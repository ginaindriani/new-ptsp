<?php

use App\Http\Controllers\Account\SettingsController;
use App\Http\Controllers\Auth\SocialiteLoginController;
use App\Http\Controllers\BackofficeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DataTamusController;
use App\Http\Controllers\Documentation\ReferencesController;
use App\Http\Controllers\LandingPageController;
use App\Http\Controllers\LayananController;
use App\Http\Controllers\Logs\AuditLogsController;
use App\Http\Controllers\Logs\SystemLogsController;
use App\Http\Controllers\Master\BeritaController;
use App\Http\Controllers\Master\BidangController;
use App\Http\Controllers\Master\DataTamuController;
use App\Http\Controllers\DataTilangController;
use App\Http\Controllers\Master\DpoController;
use App\Http\Controllers\Master\JabatanController;
use App\Http\Controllers\Master\JadwalSidangController;
use App\Http\Controllers\Master\KegiatanController;
use App\Http\Controllers\Master\LelangController;
use App\Http\Controllers\Master\OrganisasiController;
use App\Http\Controllers\Master\SatkerController;
use App\Http\Controllers\Master\MasterLayananController;
use App\Http\Controllers\Master\PengumumanController;
use App\Http\Controllers\Master\SliderController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PermohonanLayananController;
use App\Http\Controllers\RuangDiskusiController;
use App\Http\Controllers\TestingController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\FrontendController;
use App\Models\Pengumuman;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [LandingPageController::class, 'index'])->name('index');
Route::get('/layanan', [LandingPageController::class, 'index'])->name('index');

Route::get('/new', [FrontendController::class, 'index']);
Route::get('/ptsp/pilihsatker', [FrontendController::class, 'ptsp_pilih_satker']);
Route::get('/kamdal/{slug}', [FrontendController::class, 'kamdal']);
Route::post('/simpankendaraan', [FrontendController::class, 'simpankendaraan'])->name('simpankendaraan');
Route::get('/new/{slug}', [FrontendController::class, 'aksi']);
Route::get('/ptsp/{slug}', [FrontendController::class, 'ptsp']);
Route::put('/updatetamu/{unique_id}', [FrontendController::class, 'updatetamu'])->name('updatetamu');
Route::put('/updatetamusurat/{unique_id}', [FrontendController::class, 'updatetamusurat'])->name('updatetamusurat');
Route::get('/update/{slug}/{unique_id}', [FrontendController::class, 'show2']);
Route::get('/kunjungan/tilang/{unique_id}', [FrontendController::class, 'kunjungantilang']);
Route::post('/simpantamutilang', [FrontendController::class, 'simpantamutilang'])->name('simpantamutilang');
Route::get('/tilang/{slug}', [FrontendController::class, 'tilang']);
Route::get('/kunjungan/view/{unique_id}', [FrontendController::class, 'kunjunganview']);
Route::get('/kunjungan/viewptsp/{unique_id}', [FrontendController::class, 'kunjunganviewptsp']);


Route::get('/index', [LandingPageController::class, 'index'])->name('index');
Route::get('/indexlayanan', [LandingPageController::class, 'indexlayanan'])->name('indexlayanan');
Route::get('/indexinformasi', [LandingPageController::class, 'informasi'])->name('indexinformasi');
Route::get('/indexpengaduan', [LandingPageController::class, 'pengaduan'])->name('indexpengaduan');
Route::get('/satker', [LandingPageController::class, 'satker'])->name('satker');
Route::get('/scanqr', [LandingPageController::class, 'scanqr'])->name('scanqr');
Route::get('/pendaftaran', [LandingPageController::class, 'pendaftaran'])->name('pendaftaran');
Route::get('/validate/qr/{unique_key}', [LandingPageController::class, 'validateqr'])->name('validateqr');
Route::get('/informasi', [LayananController::class, 'informasi'])->name('informasi');
Route::get('/layanan', [LayananController::class, 'index'])->name('layanan');
Route::get('/satkerChild', [LandingPageController::class, 'satkerChild'])->name('satkerChild');
Route::get('/status-tamu', [LandingPageController::class, 'statusTamu'])->name('status-tamu');
Route::get('/identitas/{kode_satker}', [LayananController::class, 'identitas'])->name('identitas');
Route::post('/submit_identitas', [LayananController::class, 'submit_identitas'])->name('submit_identitas');
Route::post('/layanan/get-satker', [LayananController::class, 'getSatker'])->name('layanan.get-satker');
Route::post('/send-email', [LandingPageController::class, 'sendEmail'])->name('send-email')->middleware('throttle:1,1');

Route::middleware('auth', 'verified')->group(function () {
	// Route::get('/p', function () {
	// 	$p = Redis::incr('p');
	// 	return $p;
	// });
	Route::post('/dataUser', [DashboardController::class, 'dataUser'])->name('dataUser');
	Route::post('/dataMasukBidang', [DashboardController::class, 'dataMasukBidang'])->name('dataMasukBidang');
	Route::post('/dataKeluarBidang', [DashboardController::class, 'dataKeluarBidang'])->name('dataKeluarBidang');
	Route::get('profile', [UserController::class, 'profile'])->name('user.profile');
});

// $menu = theme()->getMenu();
// array_walk($menu, function ($val) {
//     if (isset($val['path'])) {
//         $route = Route::get($val['path'], [PagesController::class, 'index']);

//         // Exclude documentation from auth middleware
//         if (!Str::contains($val['path'], 'documentation')) {
//             $route->middleware('auth');
//         }
//     }
// });
Route::get('testing', [TestingController::class, 'index'])->name('testing');

// Documentations pages
Route::prefix('documentation')->group(function () {
    Route::get('getting-started/references', [ReferencesController::class, 'index']);
    Route::get('getting-started/changelog', [PagesController::class, 'index']);
});


Route::get('layanan/daftar', [PermohonanLayananController::class, 'daftarlayanan'])->name('permohonan.daftarLayanan');
Route::post('layanan/simpantamu', [PermohonanLayananController::class, 'simpantamu'])->name('permohonan.simpantamu');
Route::get('layanan/permohonan/system', [PermohonanLayananController::class, 'indexPublic'])->name('permohonan.indexpublic');
Route::get('layanan/permohonan/create', [PermohonanLayananController::class, 'create'])->name('permohonan.create');
Route::get('layanan/permohonan/berita/{id}', [PermohonanLayananController::class, 'berita'])->name('permohonan.berita');
Route::get('layanan/permohonan/tilang/{id}', [PermohonanLayananController::class, 'tilang'])->name('permohonan.tilang');
Route::get('layanan/permohonan/tahanan/{id}', [PermohonanLayananController::class, 'tahanan'])->name('permohonan.tahanan');
Route::get('layanan/permohonan/infotahanan/{id}', [PermohonanLayananController::class, 'infotahanan'])->name('permohonan.infotahanan');
Route::get('layanan/permohonan/perkara-pidum', [PermohonanLayananController::class, 'perkarapidum'])->name('permohonan.perkaraPidum');
Route::post('layanan/permohonan/perkara-pidum/search', [PermohonanLayananController::class, 'perkarapidum'])->name('permohonan.perkaraPidumSearch');
Route::get('layanan/permohonan/perkara-pidsus', [PermohonanLayananController::class, 'perkarapidsus'])->name('permohonan.perkaraPidsus');
Route::post('layanan/permohonan/perkara-pidsus/search', [PermohonanLayananController::class, 'perkarapidsus'])->name('permohonan.perkaraPidsusSearch');
Route::post('layanan/permohonan/chartPerkara', [PermohonanLayananController::class, 'chartPerkara'])->name('permohonan.chartPerkara');
Route::post('layanan/permohonan/store', [PermohonanLayananController::class, 'store'])->name('permohonan.store');
Route::get('layanan/permohonan/{id}/showdetail', [PermohonanLayananController::class, 'showDetail'])->name('permohonan.showdetail');
Route::get('layanan/permohonan/{id}/proses', [PermohonanLayananController::class, 'proses'])->name('permohonan.proses');
Route::get('layanan/permohonan/{id}/show', [PermohonanLayananController::class, 'show'])->name('permohonan.show');
Route::any('layanan/permohonan/modal', [PermohonanLayananController::class, 'dataModal'])->name('permohonan.dataModal');
Route::get('layanan/permohonan/{id}/downloadfile', [PermohonanLayananController::class, 'downloadfile'])->name('permohonan.downloadfile');
Route::get('layanan/permohonan/{id}/detail-map', [PermohonanLayananController::class, 'detailMap'])->name('permohonan.detail-map');
Route::post('layanan/permohonan/info-pegawai', [PermohonanLayananController::class, 'infoPegawai'])->name('permohonan.info-pegawai');
Route::get('layanan/permohonan/info-pakem', [PermohonanLayananController::class, 'infoPakem'])->name('permohonan.info-pakem');
Route::get('layanan/permohonan/info-ormas', [PermohonanLayananController::class, 'infoOrmas'])->name('permohonan.info-ormas');
Route::get('layanan/permohonan/data-geo-province/{provinceId}', [PermohonanLayananController::class, 'dataGeoProvince'])->name('permohonan.data-geo-province');

Route::get('layanan/permohonan/lelang', [PermohonanLayananController::class, 'lelang'])->name('permohonan.lelang');
Route::get('layanan/permohonan/lelang/detail/{id}', [PermohonanLayananController::class, 'detail_lelang'])->name('permohonan.detail_lelang');
Route::get('layanan/permohonan/lelang/lelang/detail/{id}', [PermohonanLayananController::class, 'detail_lelang'])->name('permohonan.detail_lelang');
Route::post('layanan/permohonan/lelang/search', [PermohonanLayananController::class, 'lelang'])->name('permohonan.lelangSearch');

Route::get('layanan/permohonan/dpo', [PermohonanLayananController::class, 'dpo'])->name('permohonan.dpo');
Route::get('layanan/permohonan/dpo/detail/{id}', [PermohonanLayananController::class, 'detail_dpo'])->name('permohonan.detail_dpo');
Route::get('layanan/permohonan/dpo/dpo/detail/{id}', [PermohonanLayananController::class, 'detail_dpo'])->name('permohonan.detail_dpo');
Route::post('layanan/permohonan/dpo/search', [PermohonanLayananController::class, 'dpo'])->name('permohonan.dpoSearch');

Route::get('layanan/permohonan/jadwalsidang', [PermohonanLayananController::class, 'jadwalsidang'])->name('permohonan.jadwalsidang');
Route::get('layanan/permohonan/jadwalsidang/detail/{id}', [PermohonanLayananController::class, 'detail_jadwalsidang'])->name('permohonan.detail_jadwalsidang');
Route::get('layanan/permohonan/jadwalsidang/jadwalsidang/detail/{id}', [PermohonanLayananController::class, 'detail_jadwalsidang'])->name('permohonan.detail_jadwalsidang');
Route::post('layanan/permohonan/jadwalsidang/search', [PermohonanLayananController::class, 'jadwalsidang'])->name('permohonan.jadwalsidangSearch');

Route::get('user/get-satker', [UserController::class, 'getSatker'])->name('user.getsatker');
Route::get('user/get-bidang', [UserController::class, 'getBidang'])->name('user.getbidang');
Route::get('user/get-jabatan', [UserController::class, 'getJabatan'])->name('user.getjabatan');
Route::get('beritadaerah/{id}', [BeritaController::class, 'beritadetail'])->name('berita.beritadetail');
Route::get('pengumuman/{id}', [PengumumanController::class, 'detail'])->name('pengumuman.pengumumandetail');
Route::get('kegiatan/{slug}', [KegiatanController::class, 'kegiatan'])->name('kegiatan.kegiatan');
Route::post('kegiatan/{slug}/search', [KegiatanController::class, 'kegiatansearch'])->name('kegiatan.kegiatansearch');
Route::get('kegiatan/{slug}/detail/{id}', [KegiatanController::class, 'detail_kegiatan'])->name('kegiatan.detail_kegiatan_search');
Route::get('kegiatan/detail/{id}', [KegiatanController::class, 'detail_kegiatan'])->name('kegiatan.detail_kegiatan');

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard/big-data', [DashboardController::class, 'getBigData'])->name('dashboard.big_data');
    Route::get('/dashboard/big-data/calculate', [DashboardController::class, 'getBigDataCalculate'])->name('dashboard.big_data_calculate');
    Route::get('/dashboard/modal-pidum/{satker}', [DashboardController::class, 'getModalPidum'])->name('dashboard.pidum');
    Route::get('/dashboard/modal-pidsus/{satker}', [DashboardController::class, 'getModalPidsus'])->name('dashboard.pidsus');
    Route::get('/dashboard/pidsus-data', [DashboardController::class, 'getDataPidsus'])->name('dashboard.pidsusdata');
    Route::get('/dashboard/pidum-data', [DashboardController::class, 'getDataPidum'])->name('dashboard.pidumdata');

    Route::get('/index', [DashboardController::class, 'index'])->name('dashboard');
    Route::prefix('layanan')->group(function () {
        Route::get('/permohonan/index', [PermohonanLayananController::class, 'index'])->name('permohonan.index');
        Route::get('/permohonan/hari-ini', [PermohonanLayananController::class, 'hariIni'])->name('permohonan.hariIni');
        Route::get('/permohonan/belum-diverifikasi', [PermohonanLayananController::class, 'belumDiverifikasi'])->name('permohonan.belumDiverifikasi');
        Route::get('/permohonan/daftar', [PermohonanLayananController::class, 'daftarPelayanan'])->name('permohonan.daftarPelayanan');
        Route::get('/permohonan/pengaduan/hari-ini', [PermohonanLayananController::class, 'pengaduanHariIni'])->name('permohonan.pengaduanHariIni');
        Route::get('/permohonan/pengaduan/belum-diverifikasi', [PermohonanLayananController::class, 'pengaduanBelumDiverifikasi'])->name('permohonan.pengaduanBelumDiverifikasi');
        Route::get('/permohonan/pengaduan/daftar', [PermohonanLayananController::class, 'pengaduanDaftarPelayanan'])->name('permohonan.pengaduanDaftarPelayanan');
        
        Route::get('/permohonan/data', [LayananController::class, 'data_layanan'])->name('permohonan.data_layanan');
        Route::delete('/permohonan/destroy/{id}', [PermohonanLayananController::class, 'destroy'])->name('permohonan.destroy');
        Route::get('/permohonan/{id}/editlayanan', [PermohonanLayananController::class, 'editLayanan'])->name('permohonan.editlayanan');
        Route::put('/permohonan/{id}/updatelayanan', [PermohonanLayananController::class, 'updateLayanan'])->name('permohonan.updatelayanan');
    });

    //backoffice
    Route::prefix('backoffice')->group(function () {
        Route::get('dashboard', [BackofficeController::class, 'dashboard'])->name('backoffice.dashboard');
        Route::get('dashboard/{id}', [BackofficeController::class, 'dashboardsatker'])->name('backoffice.dashboardsatker');
        Route::get('filterdashboard/{id}', [BackofficeController::class, 'filterdashboard'])->name('filterdashboard.dashboard');
        Route::get('layanan', [BackofficeController::class, 'layanan'])->name('backoffice.layanan');
        Route::get('informasi', [BackofficeController::class, 'informasi'])->name('backoffice.informasi');   
        Route::get('pengaduan', [BackofficeController::class, 'pengaduan'])->name('backoffice.pengaduan');   
    });


    // Account pages
    Route::get('/ruang-diskusi', [RuangDiskusiController::class, 'index'])->name('ruang-diskusi.index');
    Route::delete('/ruang-diskusi/destroy/{id}', [RuangDiskusiController::class, 'destroy'])->name('ruang-diskusi.destroy');
    Route::any('/ruang-diskusi/{id}/answer', [RuangDiskusiController::class, 'answer'])->name('ruang-diskusi.answer');
    Route::post('/ruang-diskusi/export', [RuangDiskusiController::class, 'index'])->name('user.export');

    // Data Tamu
    

    // Account pages
    Route::prefix('account')->group(function () {
        Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
        Route::put('settings', [SettingsController::class, 'update'])->name('settings.update');
        Route::put('settings/email', [SettingsController::class, 'changeEmail'])->name('settings.changeEmail');
        Route::put('settings/password', [SettingsController::class, 'changePassword'])->name('settings.changePassword');
    });
    // Logs pages
    Route::prefix('log')->name('log.')->group(function () {
        Route::resource('system', SystemLogsController::class)->only(['index', 'destroy']);
        Route::resource('system', SystemLogsController::class)->only(['index', 'destroy']);
    });
});
Route::middleware('auth', 'verified')->group(function () {
    Route::resource('users', UsersController::class);
});

Route::group(['prefix' => 'manajemen', 'middleware' => 'auth'], function () {
    Route::group(['middleware' => ['permission:manajemenuser']], function () {
        Route::post('/user/export', [UserController::class, 'index'])->name('user.export');
        Route::post('/user/ceknip', [UserController::class, 'ceknip'])->name('user.ceknip');
        Route::resource('user', UserController::class);
    });
});

Route::group(['prefix' => 'permintaan-pelayanan', 'middleware' => 'auth'], function () {
    Route::get('get-satker', [OrganisasiController::class, 'getSatker'])->name('relasiorganisasi.getsatker');
});

Route::group(['prefix' => 'master', 'middleware' => 'auth'], function () {
    Route::resource('berita', BeritaController::class);
    Route::resource('kegiatan', KegiatanController::class);
    Route::resource('dpo', DpoController::class);
    Route::resource('jadwalsidang', JadwalSidangController::class);
    Route::post('jadwalsidang/get-nip', [JadwalSidangController::class, 'getNip'])->name('jadwalsidang.getNip');
    Route::resource('datatamu', DataTamusController::class);
    Route::resource('tilang', DataTilangController::class);
    Route::resource('lelang', LelangController::class);
    Route::resource('pengumuman', PengumumanController::class);
	Route::group(['middleware' => ['permission:master']], function () {
        Route::post('/master_layanan/export', [MasterLayananController::class, 'index'])->name('master_layanan.export');
        Route::resource('master_layanan', MasterLayananController::class);
		Route::post('/satker/export', [SatkerController::class, 'index'])->name('satker.export');
		Route::resource('satker', SatkerController::class);
		Route::post('/bidang/export', [BidangController::class, 'index'])->name('bidang.export');
		Route::resource('bidang', BidangController::class);
		Route::post('/jabatan/export', [JabatanController::class, 'index'])->name('jabatan.export');
		Route::resource('jabatan', JabatanController::class);
        Route::resource('slider', SliderController::class);
        
		Route::get('get-satker', [OrganisasiController::class, 'getSatker'])->name('relasiorganisasi.getsatker');
		Route::get('get-bidang', [OrganisasiController::class, 'getBidang'])->name('relasiorganisasi.getbidang');
		Route::get('get-jabatan', [OrganisasiController::class, 'getJabatan'])->name('relasiorganisasi.getjabatan');
		Route::post('/relasiorganisasi/export', [OrganisasiController::class, 'index'])->name('relasiorganisasi.export');
		Route::resource('relasiorganisasi', OrganisasiController::class);
	});
});


/**
 * Socialite login using Google service
 * https://laravel.com/docs/8.x/socialite
 */
Route::get('/auth/redirect/{provider}', [SocialiteLoginController::class, 'redirect']);

require __DIR__ . '/auth.php';
