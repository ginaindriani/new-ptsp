<?php

use App\Http\Controllers\Api\LayananController;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PermohonanLayananController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->group(function () {
    Route::post('jenis-laporan-pengaduan', [ApiController::class, 'apiJenisLaporanPengaduan'])->name('api.jenisLaporanPengaduan');
    Route::post('jumlah-laporan-pengaduan', [ApiController::class, 'apiJumlahLaporanPengaduan'])->name('api.jenisLaporanPengaduan');
    Route::post('satker', [ApiController::class, 'apiSatker'])->name('api.satker');
    Route::post('tersangka', [ApiController::class, 'apiTersangka'])->name('api.tersangka');
    Route::post('nomor-perkara', [ApiController::class, 'nomorPerkara'])->name('api.nomorPerkara');
    Route::post('nama-pegawai', [ApiController::class, 'namaPegawai'])->name('api.namaPegawai');
    Route::post('jabatan-pegawai', [ApiController::class, 'jabatanPegawai'])->name('api.jabatanPegawai');
    Route::post('foto-pegawai', [ApiController::class, 'fotoPegawai'])->name('api.fotoPegawai');
    Route::post('simpan-sipermata', [ApiController::class, 'simpanSipermata'])->name('api.simpanSipermata');
    Route::post('simpan-lapdumas', [ApiController::class, 'simpanLapdumas'])->name('api.simpanLapdumas');
    Route::post('berita', [ApiController::class, 'berita'])->name('api.berita');
    Route::post('rutan-lapas', [ApiController::class, 'rutanLapas'])->name('api.rutanLapas');
    Route::post('barang-bukti', [ApiController::class, 'barangBukti'])->name('api.barangBukti');
    Route::post('nama-jaksa', [ApiController::class, 'namaJaksa'])->name('api.namaJaksa');
    Route::post('tempat-lahir', [ApiController::class, 'tempatLahir'])->name('api.tempatLahir');
    Route::post('tanggal-lahir', [ApiController::class, 'tanggalLahir'])->name('api.tanggalLahir');
    Route::post('jenis-kelamin', [ApiController::class, 'jenisKelamin'])->name('api.jenisKelamin');
    Route::post('jenis-tahanan', [ApiController::class, 'jenisTahanan'])->name('api.jenisTahanan');
    Route::post('ditahan-sejak', [ApiController::class, 'ditahanSejak'])->name('api.ditahanSejak');
    Route::post('tanggal-spdp', [ApiController::class, 'tanggalSpdp'])->name('api.tanggalSpdp');
    Route::post('status-tahanan', [ApiController::class, 'statusTahanan'])->name('api.statusTahanan');
    Route::post('id-tahanan', [ApiController::class, 'idTahanan'])->name('api.idTahanan');
    Route::post('nik-tahanan', [ApiController::class, 'nikTahanan'])->name('api.nikTahanan');
    Route::post('nohp-tahanan', [ApiController::class, 'nohpTahanan'])->name('api.nohpTahanan');
    Route::post('nama-saksi', [ApiController::class, 'namaSaksi'])->name('api.namaSaksi');
    Route::post('twitter', [ApiController::class, 'twitterDashboard'])->name('api.twitter');
// });

// Route::middleware('auth:api')->group(function () {
    Route::post('simpan-tamu', [PermohonanLayananController::class, 'apiStoreTamu'])->name('permohonan.simpan-tamu');
    Route::post('simpan-layanan', [PermohonanLayananController::class, 'apiStoreLayanan'])->name('permohonan.simpan-layanan');
    Route::post('update-layanan', [PermohonanLayananController::class, 'apiUpdateLayanan'])->name('permohonan.update-layanan');
// });
// Route::middleware('auth:api')->group(function () {
    Route::get('permohonan/list-by-layanan-and-nik/{id_pelayanan}/{nik}', [LayananController::class, 'listByLayanan'])->name('permohonan.list-by-layanan-and-nik');
    Route::get('permohonan/detail/{id_permohonan_pelayanan}', [LayananController::class, 'detail'])->name('permohonan.detail');
    Route::get('permohonan/master-layanan', [LayananController::class, 'masterLayanan'])->name('permohonan.master-layanan');

// });
