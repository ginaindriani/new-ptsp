<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'superadmin',
            'admin',
            'user'
        ];
        $this->createRole($roles);

        $list_permissions = [
            'manajemenuser',
            'manajemenrole',
            'master'
        ];
        $this->createPermissions($list_permissions);

        $this->createSuperadminHasPermission($list_permissions);

    }

    private function createRole($roles)
    {
        foreach ($roles as $key => $role) {
            Role::create([
                'name' => $role,
                'guard_name' => 'web',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    private function createPermissions($list_permissions)
    {
        foreach ($list_permissions as $key => $permissions) {
            Permission::create([
                'name' => $permissions,
                'guard_name' => 'web',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    private function createSuperadminHasPermission($list_permissions)
    {
        $id_role = Role::findByName('superadmin', 'web')->id;

        foreach ($list_permissions as $key => $permissions) {
            DB::table('role_has_permissions')->insert([
                [
                    'permission_id' => Permission::findByName($permissions, 'web')->id,
                    'role_id' => $id_role
                ]
            ]);
        }
    }

    private function createAdminHasPermission()
    {
        $id_role = Role::findByName('admin', 'web')->id;

        DB::table('role_has_permissions')->insert([
            [
                'permission_id' => Permission::findByName('manajemenuser', 'web')->id,
                'role_id' => $id_role
            ]
        ]);
    }
}
