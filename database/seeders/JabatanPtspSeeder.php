<?php

namespace Database\Seeders;

use App\Models\Bidang;
use App\Models\Satker;
use App\Models\Jabatan;
use App\Models\Organisasi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JabatanPtspSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            //PTSP Kejagung
            $ptspKejagung = Jabatan::whereNamaJabatan('KEPALA BAGIAN TATA USAHA UMUM DAN PIMPINAN')->first();

            if ($ptspKejagung) {
                $jabatanKejagung = Jabatan::create([
                                        'parent_id' => $ptspKejagung->id_jabatan,
                                        'nama_jabatan' => 'PTSP Kejaksaan Agung',
                                        'is_admin' => false,
                                        'eselon' => 5
                                    ]);

                $bidangKejagung = Bidang::whereNamaBidangFull('BAGIAN TATA USAHA UMUM DAN PIMPINAN > BIRO UMUM')->first();
                if ($bidangKejagung) {
                    $kejagung = Satker::where('tipe_satker', '1')->get();
                    $this->createOrganisasi($kejagung, $bidangKejagung, $jabatanKejagung);
                }
            }
            

            //PTSP Kejati
            $ptspKejati = Jabatan::whereNamaJabatan('KEPALA BAGIAN TATA USAHA')
                                    ->whereEselon(3)
                                    ->first();

            if ($ptspKejati) {
                $jabatanKejati = Jabatan::create([
                                    'parent_id' => $ptspKejati->id_jabatan,
                                    'nama_jabatan' => 'PTSP Kejaksaan Tinggi',
                                    'is_admin' => false,
                                    'eselon' => 5
                                ]);

                $bidangKejati = Bidang::whereNamaBidangFull('BAGIAN TATA USAHA > KEJAKSAAN TINGGI')->first();
                if ($bidangKejati) {
                    $kejati = Satker::where('tipe_satker', '2')->get();
                    $this->createOrganisasi($kejati, $bidangKejati, $jabatanKejati);
                }
            }


            //PTSP Kejari
            $ptspKejari = Jabatan::whereNamaJabatan('KEPALA URUSAN TATA USAHA DAN PERPUSTAKAAN')
                                    ->whereEselon(5)
                                    ->first();
            if ($ptspKejari) {
                $jabatanKejari = Jabatan::create([
                                    'parent_id' => $ptspKejari->id_jabatan,
                                    'nama_jabatan' => 'PTSP Kejaksaan Negeri',
                                    'is_admin' => false,
                                    'eselon' => 5
                                ]);

                $bidangKejari = Bidang::whereNamaBidangFull('URUSAN TATA USAHA DAN PERPUSTAKAAN > SUBBAGIAN PEMBINAAN')->first();
                if ($bidangKejari) {
                    $kejari = Satker::where('tipe_satker', '3')->get();
                    $this->createOrganisasi($kejari, $bidangKejari, $jabatanKejari);
                }
            }


            //PTSP Cabjari
            $ptspCabjari = Jabatan::whereNamaJabatan('KEPALA URUSAN PEMBINAAN')
                                    ->whereEselon(5)
                                    ->first();
            if ($ptspCabjari) {
                $jabatanCabjari = Jabatan::create([
                                    'parent_id' => $ptspCabjari->id_jabatan,
                                    'nama_jabatan' => 'PTSP Cabang Kejaksaan Negeri',
                                    'is_admin' => false,
                                    'eselon' => 5
                                ]);

                $bidangCabjari = Bidang::whereNamaBidangFull('URUSAN PEMBINAAN > CABANG KEJAKSAAN NEGERI')->first();
                if ($bidangCabjari) {
                    $cabkejari = Satker::where('tipe_satker', '4')->get();
                    $this->createOrganisasi($cabkejari, $bidangCabjari, $jabatanCabjari);
                }
            }

            DB::commit();

        } 
        catch(\Exception $ex)
        {
            DB::rollback();
            echo $ex->getMessage() . PHP_EOL;
        }
    }

    private function createOrganisasi($satker, $bidang, $jabatan) {
        foreach($satker as $key => $value) {
            Organisasi::create([
                'id_satker' => $value->id_satker,
                'id_bidang' => $bidang->id_bidang,
                'id_jabatan' => $jabatan->id_jabatan
            ]);
        }
    }
}
