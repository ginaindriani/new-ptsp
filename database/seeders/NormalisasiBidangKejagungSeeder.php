<?php

namespace Database\Seeders;

use App\Models\Bidang;
use Illuminate\Database\Seeder;
use App\Helpers\OrganisasiHelper;
use Illuminate\Support\Facades\DB;

class NormalisasiBidangKejagungSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            $bidangMain = Bidang::where('main', 1)->get();
            foreach ($bidangMain as $bidang) {
                $childBidang = OrganisasiHelper::getChildrenBidang($bidang->id_bidang);
                $nama_bidang = [];
                $nama_bidang[] = $childBidang->nama_bidang;
                foreach ($childBidang->children as $key => $child) {
                    $this->bidang($child, $nama_bidang);
                }
            }

            DB::commit();
        } 
        catch(\Exception $ex)
        {
            DB::rollback();
            echo $ex->getMessage() . PHP_EOL;
        }
    }

    private function bidang($bidang, $nama_bidang)
    {
        if ($bidang->main == 1) {
            return;
        }

        $nama_bidang[] = $bidang->nama_bidang;
        krsort($nama_bidang);
        // dd(implode(" > ",$nama_bidang));
        $bidang->nama_bidang_full = implode(" > ",$nama_bidang);
        $bidang->save();

        foreach ($bidang->children as $key => $child) {
            $this->bidang($child, $nama_bidang);
        }
    }
}
