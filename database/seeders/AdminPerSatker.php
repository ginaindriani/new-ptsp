<?php

namespace Database\Seeders;

use App\Models\Satker;
use App\Models\User;
use Hash;
use Illuminate\Database\Seeder;

class AdminPerSatker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::where('id', 1)->update(['role' => 'superadmin']);
        User::where('id', 2)->update(['role' => 'admin']);

        $satkers    = Satker::all();
        foreach ($satkers as $item) {
            $user = User::where('email',  "admin_" . $item->id_satker)
                ->first();

            if (!$user) 
                $user = new User();

            $user->nama = "ADMIN " . $item->nama_satker;
            $user->username = "admin_" . $item->id_satker;
            $user->kode_satker = $item->id_satker;
            $user->role = "admin";
            $user->email = "admin_" . $item->id_satker . "@mailinator.com";
            $user->password = Hash::make('Qwerty1234');
            $user->save();
        }
    }
}
