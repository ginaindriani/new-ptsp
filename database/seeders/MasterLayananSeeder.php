<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterLayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_layanans')->truncate();
        // Layanan Antar Barang Bukti 
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Antar Barang Bukti', 
                    'blade' => 'cetak.layanan.antar_bb', 
                    'blade_result' => 'cetak.result.antar_bb', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true}, "nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pemohon","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pemohon","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"nama_bb":{"type":"text","order":8,"title":"Nama Barang Bukti","required":true}}',
                    'image' => 'a1_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 1
            ]);

        // Layanan Antar Tilang 
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Antar Tilang', 
                    'blade' => 'cetak.layanan.antar_tilang', 
                    'blade_result' => 'cetak.result.antar_tilang', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pemohon","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pemohon","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"nama_bb":{"type":"text","order":8,"title":"Antar Tilang","required":true}}',
                    'image' => 'a2_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 2
            ]);

        // Layanan Antar Jemput Saksi Anak/ Difabel 
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Antar Jemput Saksi Anak/ Difabel', 
                    'blade' => 'cetak.layanan.antar_jemput_saksi_anakdifabel', 
                    'blade_result' => 'cetak.result.antar_jemput_saksi_anakdifabel', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pemohon","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pemohon","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"nama_saksi":{"type":"text","order":8,"title":"Nama Saksi","required":true},"alasan_jemput":{"type":"text","order":9,"title":"Alasan Jemput","required":true},"tanggal_sidang":{"type":"date","order":10,"title":"Tanggal Sidang","required":true}}',
                    'image' => 'a3_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 3
            ]);

        // Layanan Pinjam Barang Bukti 
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Pinjam Barang Bukti', 
                    'blade' => 'cetak.layanan.pinjam_bb', 
                    'blade_result' => 'cetak.result.pinjam_bb', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pemohon","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pemohon","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"nama_bb":{"type":"text","order":8,"title":"Nama Barang Bukti","required":true}}',
                    'image' => 'a4_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 4
            ]);

        // Layanan Jenguk Tahanan 
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Jenguk Tahanan', 
                    'blade' => 'cetak.layanan.jenguk_tahanan', 
                    'blade_result' => 'cetak.result.jenguk_tahanan', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'a5_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 5
            ]);

        // Layanan Penyuluhan Hukum Online
        DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 6,
                    'nama_layanan' => 'Penyuluhan Hukum Online', 
                    'blade' => 'cetak.layanan.penyuluhan_ho', 
                    'blade_result' => 'cetak.result.penyuluhan_ho', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"asal":{"type":"select","order":5,"title":"Asal","required":true},"jenis_layanan":{"type":"select","order":4,"title":"Jenis Layanan","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'a8_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 8
            ]);

        // Layanan Pendampingan Hukum 
        DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 7,
                    'nama_layanan' => 'Pendampingan Hukum', 
                    'blade' => 'cetak.layanan.pendampingan_hukum', 
                    'blade_result' => 'cetak.result.pendampingan_hukum', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pemohon","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pemohon","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"asal":{"type":"select","order":6,"title":"Asal Pemohon","required":true},"jenis_layanan":{"type":"select","order":7,"title":"Jenis Layanan","required":true},"deskripsi":{"type":"textarea","order":8,"title":"Deskripsi Singkat","required":true},"tujuan":{"type":"textarea","order":9,"title":"Tujuan Konsultasi","required":true}, "src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'a9_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 9
            ]);

        // Layanan Konsultasi Hukum 
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Konsultasi Hukum', 
                    'blade' => 'cetak.layanan.konsultasi_hukum', 
                    'blade_result' => 'cetak.result.konsultasi_hukum', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pemohon","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pemohon","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"asal":{"type":"select","order":6,"title":"Asal Pemohon","required":true},"jenis_layanan":{"type":"select","order":7,"title":"Jenis Layanan","required":true},"deskripsi":{"type":"textarea","order":8,"title":"Deskripsi Singkat","required":true},"tujuan":{"type":"textarea","order":9,"title":"Tujuan Konsultasi","required":true}, "src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa10_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 10
            ]);
            
        // Layanan Pengaduan Masyarakat 
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Pengaduan Masyarakat', 
                    'blade' => 'cetak.layanan.pengaduan', 
                    'blade_result' => 'cetak.result.pengaduan', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"asal":{"type":"select","order":6,"title":"Asal","required":true},"jenis_layanan":{"type":"select","order":7,"title":"Jenis Layanan","required":true},"no_perkara":{"type":"text","order":8,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":9,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":10,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":11,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":12,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa11_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'hyperlink',
                    'link_url' => 'https://www.lapor.go.id/instansi/kejaksaan-agung-republik-indonesia',
                    'order_by' => 11
            ]);

        // Layanan Whistle Blowing Sistem 
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Whistle Blowing Sistem', 
                    'blade' => 'cetak.layanan.wbs', 
                    'blade_result' => 'cetak.result.wbs', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa12_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 12
            ]);

        // Layanan Stop Pungli
        DB::table('master_layanans')
            ->insert([
                    'id_layanan' => '11',
                    'nama_layanan' => 'Stop Pungli', 
                    'blade' => 'cetak.layanan.stop_pungli', 
                    'blade_result' => 'cetak.result.stop_pungli', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa13_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'hyperlink',
                    'link_url' => 'javascript:void(0);',
                    'order_by' => 13
            ]);

        // Layanan Ruang Diskusi
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Ruang Diskusi', 
                    'blade' => 'cetak.layanan.ruang_diskusi', 
                    'blade_result' => 'cetak.result.ruang_diskusi', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa14_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'hyperlink',
                    'link_url' => 'https://t.me/ruangberbagi29_bot',
                    'order_by' => 14
            ]);

        // Layanan Info Perkara Pidum
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Info Perkara Pidum', 
                    'blade' => 'cetak.layanan.info_perkara_pidum', 
                    'blade_result' => 'cetak.result.info_perkara_pidum', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa15_system.jpg',
                    'tipe_layanan' => 'sistem',
                    'link_url' => 'http://213.190.4.249/kejagung/cms_ol/public/',
                    'type' => 'hyperlink',
                    'order_by' => 15
            ]);

        // Layanan Info Perkara Pidsus
        DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Info Perkara Pidsus', 
                    'blade' => 'cetak.layanan.info_perkara_pidsus', 
                    'blade_result' => 'cetak.result.info_perkara_pidsus', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa16_system.jpg',
                    'tipe_layanan' => 'sistem',
                    'link_url' => 'http://213.190.4.249/kejagung/cms_ol/public/pidsus',
                    'type' => 'hyperlink',
                    'order_by' => 16
            ]);

        // Layanan Info Kepegawaian
        DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 17,
                    'nama_layanan' => 'Info Kepegawaian', 
                    'blade' => 'cetak.layanan.info_kepegawaian', 
                    'blade_result' => 'cetak.result.info_kepegawaian', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa17_system.jpg',
                    'tipe_layanan' => 'sistem',
                    'type' => 'in_system',
                    'order_by' => 17
            ]);

            // Layanan E-Tilang
            DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'E-Tilang', 
                    'blade' => 'cetak.layanan.e_tilang', 
                    'blade_result' => 'cetak.result.e_tilang', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa18_system.jpg',
                    'tipe_layanan' => 'sistem',
                    'type' => 'hyperlink',
                    'link_url' => 'https://tilang.kejaksaan.go.id/',
                    'order_by' => 18
            ]);

            // Layanan Peta Kejaksaan R1
            DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 19,
                    'nama_layanan' => 'Peta Kejaksaan RI', 
                    'blade' => 'cetak.layanan.peta_kejaksaan_ri', 
                    'blade_result' => 'cetak.result.peta_kejaksaan_ri', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa19_system.jpg',
                    'tipe_layanan' => 'sistem',
                    'type' => 'in_system',
                    'order_by' => 19
            ]);

            // Informasi Ormas Seindonesia
            DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 20,
                    'nama_layanan' => 'Informasi Ormas Seindonesia', 
                    'blade' => 'cetak.layanan.informasi_ormas_seindonesia', 
                    'blade_result' => 'cetak.result.informasi_ormas_seindonesia', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa20_informasi.jpg',
                    'tipe_layanan' => 'sistem',
                    'type' => 'in_system',
                    'order_by' => 20
            ]);

            // Informasi Pakem Seindonesia
            DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 21,
                    'nama_layanan' => 'Informasi Pakem Seindonesia', 
                    'blade' => 'cetak.layanan.informasi_pakem_seindonesia', 
                    'blade_result' => 'cetak.result.informasi_pakem_seindonesia', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa21_informasi.jpg',
                    'tipe_layanan' => 'sistem',
                    'type' => 'in_system',
                    'order_by' => 21
            ]);

            // Informasi BNPB
            DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 22,
                    'nama_layanan' => 'Informasi BNPB', 
                    'blade' => 'cetak.layanan.informasi_bnpb', 
                    'blade_result' => 'cetak.result.informasi_bnpb', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa22_informasi.jpg',
                    'link_url' => 'javascript:void(0);',
                    'tipe_layanan' => 'sistem',
                    'type' => 'hyperlink',
                    'order_by' => 22
            ]);

            // Informasi BNPB
            DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 23,
                    'nama_layanan' => 'Layanan Pelaporan Pakem', 
                    'blade' => 'cetak.layanan.layanan_pelaporan_pakem', 
                    'blade_result' => 'cetak.result.layanan_pelaporan_pakem', 
                    'aktif' => 1, 
                    'parameter' => '{"nama":{"type":"text","order":1,"title":"Nama Pakem","required":true},"alamat":{"type":"textarea","order":2,"title":"Alamat Pakem","required":true},"peta":{"type":"text","order":3,"title":"Pick Peta","required":true},"satker":{"type":"select","order":4,"title":"Kejaksaan Pengawasan","required":true},"sk_ormas":{"type":"text","order":5,"title":"SK Pakem","required":true},"tanggal_berdiri":{"type":"date","order":6,"title":"Tanggal Berdiri","required":true},"nama_pemimpin":{"type":"text","order":7,"title":"Nama Pemimpin","required":true},"alamat_pemimpin":{"type":"textarea","order":8,"title":"Alamat Pemimpin","required":true},"src":{"max":"5000","type":"file","mimes":"jpg,jpeg,png,webp","order":9,"title":"Logo Pakem","required":false}}',
                    'image' => 'aa23_informasi.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 23
            ]);

            // satker
            DB::table('master_layanans')
            ->insert([
                    'nama_layanan' => 'Pengawasan Tahanan', 
                    'blade' => 'cetak.layanan.pengawasan_tahanan', 
                    'blade_result' => 'cetak.result.pengawasan_tahanan', 
                    'aktif' => 1, 
                    'parameter' => '{"satker":{"type":"select","order":1,"title":"Satuan Kerja Kejaksaan","required":true},"nik":{"type":"text","order":2,"title":"NIK / Identitas","required":true},"nama":{"type":"text","order":3,"title":"Nama Pelapor","required":true},"alamat":{"type":"text","order":4,"title":"Alamat Pelapor","required":true},"telp":{"type":"text","order":5,"title":"Telephone/WA","required":false},"no_perkara":{"type":"text","order":6,"title":"Nomor Perkara","required":true},"nama_terdakwa":{"type":"text","order":7,"title":"Nama Terdakwa","required":true},"jenis":{"type":"select","order":8,"title":"Jenis Pengaduan","required":true},"deskripsi":{"type":"textarea","order":9,"title":"Deskripsi Singkat","required":true},"src":{"max":"5000","type":"file","mimes":"pdf,docx,doc","order":10,"title":"Upload Dokumen Pendukung","required":false}}',
                    'image' => 'aa24_system.jpg',
                    'tipe_layanan' => 'sistem',
                    'link_url' => 'http://156.67.219.105/kejagung/tahanan/public/tahananabsen',
                    'type' => 'hyperlink',
                    'order_by' => 24
            ]);

            // Informasi BNPB
            DB::table('master_layanans')
            ->insert([
                    'id_layanan' => 25,
                    'nama_layanan' => 'Layanan Pelaporan Ormas', 
                    'blade' => 'cetak.layanan.layanan_pelaporan_ormas', 
                    'blade_result' => 'cetak.result.layanan_pelaporan_ormas', 
                    'aktif' => 1, 
                    'parameter' => '{"nama":{"type":"text","order":1,"title":"Nama Ormas","required":true},"alamat":{"type":"textarea","order":2,"title":"Alamat Ormas","required":true},"peta":{"type":"text","order":3,"title":"Pick Peta","required":true},"satker":{"type":"select","order":4,"title":"Kejaksaan Pengawasan","required":true},"sk_ormas":{"type":"text","order":5,"title":"SK Ormas","required":true},"tanggal_berdiri":{"type":"date","order":6,"title":"Tanggal Berdiri","required":true},"nama_pemimpin":{"type":"text","order":7,"title":"Nama Pemimpin","required":true},"alamat_pemimpin":{"type":"textarea","order":8,"title":"Alamat Pemimpin","required":true},"src":{"max":"5000","type":"file","mimes":"jpg,jpeg,png,webp","order":9,"title":"Logo Ormas","required":false}}',
                    'image' => 'a7_layanan.jpg',
                    'tipe_layanan' => 'layanan',
                    'type' => 'in_system',
                    'order_by' => 25
            ]);
    }
}
