<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UpdateBidangKejatiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(file_get_contents('database/unprepared/update_bidang_kejati.sql'));
        DB::unprepared(file_get_contents('database/unprepared/update_jabatan_kejati.sql'));
    }
}
