<?php

namespace Database\Seeders;

use App\Models\Bidang;
use App\Models\Satker;
use App\Models\Jabatan;
use App\Models\Organisasi;
use Illuminate\Database\Seeder;

class FungsionalKejariSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allkejari = Satker::where('tipe_satker', 3)->get();
        $parent = Bidang::where('id_bidang', 659)->first();

        foreach ($allkejari as $key => $kejari) {
            $model = Bidang::create([
                'parent_id' => $parent->id_bidang,
                'nama_bidang' => 'KELOMPOK JABATAN FUNGSIONAL',
                'nama_bidang_full' => 'KELOMPOK JABATAN FUNGSIONAL' . ($parent == null ? null : ' > ' .  $parent->nama_bidang)
            ]);

            $jbtn = Jabatan::create([
                'nama_jabatan' => 'KEPALA KELOMPOK JABATAN FUNGSIONAL',
                'eselon' => 5,
                'is_admin' => 0,
            ]);

            Organisasi::create([
                'id_satker' => $kejari->id_satker,
                'id_bidang' => $model->id_bidang,
                'id_jabatan' => $jbtn->id_jabatan,
            ]);
        }
    }
}
