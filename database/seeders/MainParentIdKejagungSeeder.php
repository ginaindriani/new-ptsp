<?php

namespace Database\Seeders;

use App\Models\Bidang;
use Illuminate\Database\Seeder;
use App\Helpers\OrganisasiHelper;
use Illuminate\Support\Facades\DB;

class MainParentIdKejagungSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultMainParentId = 28;
        DB::beginTransaction();
        try {
            $bidangUnderKejagung = Bidang::whereIn('id_bidang', [1, 2, 155, 253, 311, 374, 426, 507, 541])->get();
            foreach ($bidangUnderKejagung as $key => $value) {
                $bidangParent = Bidang::find($value->id_bidang);
                if ($bidangParent) {
                    $bidangParent->update([
                        'main_parent_id' => $this->getParentId($bidangParent->id_bidang, [1, 2, 541])
                    ]);

                    $bidangChild = OrganisasiHelper::getChildrenBidang($bidangParent->id_bidang);
                    $bidangChildFlatten = OrganisasiHelper::flatten($bidangChild->toArray());
                    if (count($bidangChildFlatten) > 0) {
                        foreach ($bidangChildFlatten as $keyChild => $valueChild) {
                            $bidangChild = Bidang::find($valueChild['id_bidang']);
                            if ($bidangChild) {
                                $bidangChild->update([
                                    'main_parent_id' => $bidangParent->main_parent_id
                                ]);
                            }
                        }
                    }
                }
            }


            $bidangUnderJambin = Bidang::whereIn('id_bidang', [3, 11, 28, 47, 65, 83, 96, 110, 122, 140, 154])->get();
            foreach ($bidangUnderJambin as $key => $value) {
                $bidangParent = Bidang::find($value->id_bidang);
                if ($bidangParent) {
                    $bidangParent->update([
                        'main_parent_id' => $this->getParentId($bidangParent->id_bidang, [11, 47, 65, 83, 96, 154])
                    ]);

                    $bidangChild = OrganisasiHelper::getChildrenBidang($bidangParent->id_bidang);
                    $bidangChildFlatten = OrganisasiHelper::flatten($bidangChild->toArray());
                    if (count($bidangChildFlatten) > 0) {
                        foreach ($bidangChildFlatten as $keyChild => $valueChild) {
                            $bidangChild = Bidang::find($valueChild['id_bidang']);
                            if ($bidangChild) {
                                $bidangChild->update([
                                    'main_parent_id' => $bidangParent->main_parent_id
                                ]);
                            }
                        }
                    }
                }
            }


            DB::commit();
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            echo $ex->getMessage() . PHP_EOL;
        }
    }

    private function getParentId($parentId, $arrayId) {
        $parentIdReturn = $parentId;
        if (in_array($parentId, $arrayId)) {
            $parentIdReturn = 28;
        }
        return $parentIdReturn;
    }
}
