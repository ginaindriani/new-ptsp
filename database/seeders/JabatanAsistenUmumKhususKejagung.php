<?php

namespace Database\Seeders;

use App\Models\Bidang;
use App\Models\Satker;
use App\Models\Jabatan;
use App\Models\Organisasi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JabatanAsistenUmumKhususKejagung extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            //Asus
            $AsusKejagung = Jabatan::whereNamaJabatan('SEKRETARIS JAKSA AGUNG MUDA BIDANG PEMBINAAN')->first();

            if ($AsusKejagung) {
                $jabatanKejagung = Jabatan::create([
                                        'parent_id' => $AsusKejagung->id_jabatan,
                                        'nama_jabatan' => 'ASISTEN KHUSUS',
                                        'is_admin' => false,
                                        'eselon' => 2
                                    ]);

                $bidangKejagung = Bidang::whereNamaBidangFull('SEKRETARIAT JAKSA AGUNG MUDA BIDANG PEMBINAAN > JAKSA AGUNG MUDA BIDANG PEMBINAAN')->first();
                if ($bidangKejagung) {
                    $kejagung = Satker::where('tipe_satker', '1')->get();
                    $this->createOrganisasi($kejagung, $bidangKejagung, $jabatanKejagung);
                }
            }
			
			//Asum
            $AsumKejagung = Jabatan::whereNamaJabatan('SEKRETARIS JAKSA AGUNG MUDA BIDANG PEMBINAAN')->first();

            if ($AsumKejagung) {
                $jabatanKejagung = Jabatan::create([
                                        'parent_id' => $AsumKejagung->id_jabatan,
                                        'nama_jabatan' => 'ASISTEN UMUM',
                                        'is_admin' => false,
                                        'eselon' => 2
                                    ]);

                $bidangKejagung = Bidang::whereNamaBidangFull('SEKRETARIAT JAKSA AGUNG MUDA BIDANG PEMBINAAN > JAKSA AGUNG MUDA BIDANG PEMBINAAN')->first();
                if ($bidangKejagung) {
                    $kejagung = Satker::where('tipe_satker', '1')->get();
                    $this->createOrganisasi($kejagung, $bidangKejagung, $jabatanKejagung);
                }
            }
            

            DB::commit();

        } 
        catch(\Exception $ex)
        {
            DB::rollback();
            echo $ex->getMessage() . PHP_EOL;
        }
    }

    private function createOrganisasi($satker, $bidang, $jabatan) {
        foreach($satker as $key => $value) {
            Organisasi::create([
                'id_satker' => $value->id_satker,
                'id_bidang' => $bidang->id_bidang,
                'id_jabatan' => $jabatan->id_jabatan
            ]);
        }
    }
}
