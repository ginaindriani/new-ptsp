<?php

namespace Database\Seeders;

use App\Models\Bidang;
use App\Models\Satker;
use App\Models\Jabatan;
use App\Models\Organisasi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class BidangSeeder extends Seeder
{
    protected $kejati;
    protected $kejari;
    protected $cabkejari;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("DELETE FROM organisasi");
        DB::statement("ALTER TABLE organisasi AUTO_INCREMENT=1");

        DB::statement("DELETE FROM bidang");
        DB::statement("ALTER TABLE bidang AUTO_INCREMENT=1");

        DB::statement("DELETE FROM jabatan");
        DB::statement("ALTER TABLE jabatan AUTO_INCREMENT=1");

        $jsonBidangKejagung = File::get(database_path('json/bidangkejagung.json'));
        $dataBidangKejagung = json_decode($jsonBidangKejagung);
        $dataBidangKejagung = collect($dataBidangKejagung);

        $jsonBidangKejati = File::get(database_path('json/bidangkejati.json'));
        $dataBidangKejati = json_decode($jsonBidangKejati);
        $dataBidangKejati = collect($dataBidangKejati);

        $jsonBidangKejari = File::get(database_path('json/bidangkejari.json'));
        $dataBidangKejari = json_decode($jsonBidangKejari);
        $dataBidangKejari = collect($dataBidangKejari);

        $jsonBidangCabkejari = File::get(database_path('json/bidangcabkejari.json'));
        $dataBidangCabkejari = json_decode($jsonBidangCabkejari);
        $dataBidangCabkejari = collect($dataBidangCabkejari);

        // dd($dataBidangCabkejari);
        foreach ($dataBidangKejagung as $data) {
            $this->createKejagung($data);
        }

        $this->kejati = Satker::where('tipe_satker', '2')->get();
        $this->kejari = Satker::where('tipe_satker', '3')->get();
        $this->cabkejari = Satker::where('tipe_satker', '4')->get();

        foreach ($dataBidangKejati as $data) {
            $this->createKejati($data);
        }

        foreach ($dataBidangKejari as $data) {
            $this->createKejari($data);
        }

        foreach ($dataBidangCabkejari as $data) {
            $this->createCabkejari($data);
        }
    }

    private function createKejagung($data, $parent = null) {
        $model = Bidang::create([
            'parent_id' => $parent == null ? null : $parent->id_bidang,
            'nama_bidang' => $data->nama,
            'nama_bidang_full' => $data->nama . ($parent == null ? null : ' > ' .  $parent->nama_bidang)
        ]);

        if ($data->nama == 'JAKSA AGUNG') {
            $jbtn = Jabatan::create([
                'nama_jabatan' => 'JAKSA AGUNG',
                'eselon' => 0,
                'is_admin' => $data->is_admin ?? 0,
            ]);
            $this->createOrganisasi(1, $model, $jbtn);

            $jbtn = Jabatan::create([
                'parent_id' => $jbtn->id_jabatan,
                'nama_jabatan' => 'WAKIL JAKSA AGUNG',
                'eselon' => 1,
                'is_admin' => $data->is_admin ?? 0,
            ]);
            $this->createOrganisasi(1, $model, $jbtn);
        }

        else if (substr($data->nama, 0, strlen('JAKSA AGUNG MUDA ')) === 'JAKSA AGUNG MUDA ') {
            $jbtn = Jabatan::create([
                'nama_jabatan' => $data->nama,
                'eselon' => $data->eselon ?? 1,
                'is_admin' => $data->is_admin ?? 0,
            ]);
            $this->createOrganisasi(1, $model, $jbtn);

        } else {
            $jbtn = Jabatan::create([
                'nama_jabatan' => 'KEPALA ' . $data->nama,
                'eselon' => $data->eselon ?? 1,
                'is_admin' => $data->is_admin ?? 0,
            ]);
            $this->createOrganisasi(1, $model, $jbtn);
        }

        if (isset($data->child)) {
            foreach ($data->child as $value) {
                $this->createKejagung($value, $model);
            }
        }
    }

    private function createKejati($data, $parent = null) {
        $model = Bidang::create([
            'parent_id' => $parent == null ? null : $parent->id_bidang,
            'nama_bidang' => $data->nama,
            'nama_bidang_full' => $data->nama . ($parent == null ? null : ' > ' .  $parent->nama_bidang)
        ]);

        if ($data->nama == 'KEJAKSAAN TINGGI') {
            $jbtn = Jabatan::create([
                'nama_jabatan' => 'KEPALA KEJAKSAAN TINGGI',
                'eselon' => $data->eselon ?? 1,
                'is_admin' => $data->is_admin ?? 0,
            ]);
            $this->createOrganisasiKejati($model, $jbtn);

            $jbtn = Jabatan::create([
                'parent_id' => $jbtn->id_jabatan,
                'nama_jabatan' => 'WAKIL KEPALA KEJAKSAAN TINGGI',
                'eselon' => $data->eselon ?? 1,
                'is_admin' => $data->is_admin ?? 0,
            ]);
            $this->createOrganisasiKejati($model, $jbtn);
        } else {
            $jbtn = Jabatan::create([
                'nama_jabatan' => 'KEPALA ' . $data->nama,
                'eselon' => $data->eselon ?? 1,
                'is_admin' => $data->is_admin ?? 0,
            ]);
            $this->createOrganisasiKejati($model, $jbtn);
        }

        if (isset($data->child)) {
            foreach ($data->child as $value) {
                $this->createKejati($value, $model);
            }
        }
    }

    private function createKejari($data, $parent = null) {
        $model = Bidang::create([
            'parent_id' => $parent == null ? null : $parent->id_bidang,
            'nama_bidang' => $data->nama,
            'nama_bidang_full' => $data->nama . ($parent == null ? null : ' > ' .  $parent->nama_bidang)
        ]);

        $jbtn = Jabatan::create([
            'nama_jabatan' => 'KEPALA ' . $data->nama,
            'eselon' => $data->eselon ?? 1,
            'is_admin' => $data->is_admin ?? 0,
        ]);
        $this->createOrganisasiKejari($model, $jbtn);

        if (isset($data->child)) {
            foreach ($data->child as $value) {
                $this->createKejari($value, $model);
            }
        }
    }

    private function createCabkejari($data, $parent = null) {
        $model = Bidang::create([
            'parent_id' => $parent == null ? null : $parent->id_bidang,
            'nama_bidang' => $data->nama,
            'nama_bidang_full' => $data->nama . ($parent == null ? null : ' > ' .  $parent->nama_bidang)
        ]);

        $jbtn = Jabatan::create([
            'nama_jabatan' => 'KEPALA ' . $data->nama,
            'eselon' => $data->eselon ?? 1,
            'is_admin' => $data->is_admin ?? 0,
        ]);
        $this->createOrganisasiCabkejari($model, $jbtn);

        if (isset($data->child)) {
            foreach ($data->child as $value) {
                $this->createCabkejari($value, $model);
            }
        }
    }

    private function createOrganisasi($satker, $bidang, $jabatan) {
        Organisasi::create([
            'id_satker' => $satker,
            'id_bidang' => $bidang->id_bidang,
            'id_jabatan' => $jabatan->id_jabatan,
        ]);
    }

    private function createOrganisasiKejati($bidang, $jabatan) {
        foreach ($this->kejati as $kejati) {
            Organisasi::create([
                'id_satker' => $kejati->id_satker,
                'id_bidang' => $bidang->id_bidang,
                'id_jabatan' => $jabatan->id_jabatan,
            ]);
        }
    }

    private function createOrganisasiKejari($bidang, $jabatan) {
        foreach ($this->kejari as $kejari) {
            Organisasi::create([
                'id_satker' => $kejari->id_satker,
                'id_bidang' => $bidang->id_bidang,
                'id_jabatan' => $jabatan->id_jabatan,
            ]);
        }
    }

    private function createOrganisasiCabkejari($bidang, $jabatan) {
        foreach ($this->cabkejari as $cabkejari) {
            Organisasi::create([
                'id_satker' => $cabkejari->id_satker,
                'id_bidang' => $bidang->id_bidang,
                'id_jabatan' => $jabatan->id_jabatan,
            ]);
        }
    }
}
