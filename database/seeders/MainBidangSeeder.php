<?php

namespace Database\Seeders;

use App\Models\Bidang;
use Illuminate\Database\Seeder;

class MainBidangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bidang::whereIn('id_bidang', [1,2,110,122,140,155,236,253,311,374,426,507,541])->update(['main'=>1]);
    }
}
