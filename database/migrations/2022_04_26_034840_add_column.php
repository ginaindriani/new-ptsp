<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tamu', function (Blueprint $table) {
            //
            $table->string('waktu_kedatangan')->nullable()->after('status');
            $table->string('tempat_lahir')->nullable()->after('status');
            $table->string('tanggal_lahir')->nullable()->after('status');
            $table->string('jenis_kelamin')->nullable()->after('status');
            $table->string('suku')->nullable()->after('status');
            $table->string('kewarganegaraan')->nullable()->after('status');
            $table->string('agama')->nullable()->after('status');
            $table->string('pendidikan')->nullable()->after('status');
            $table->string('pekerjaan')->nullable()->after('status');
            $table->string('alamat_kantor')->nullable()->after('status');
            $table->string('status_perkawinan')->nullable()->after('status');
            $table->string('kepartaian')->nullable()->after('status');
            $table->string('ormas_lainnya')->nullable()->after('status');
            $table->string('nama_pasangan')->nullable()->after('status');
            $table->string('nama_anak_anak')->nullable()->after('status');
            $table->string('nama_ayah_kandung')->nullable()->after('status');
            $table->string('alamat_ayah_kandung')->nullable()->after('status');
            $table->string('nama_ibu_kandung')->nullable()->after('status');
            $table->string('alamat_ibu_kandung')->nullable()->after('status');
            $table->string('nama_ayah_mertua')->nullable()->after('status');
            $table->string('alamat_ayah_mertua')->nullable()->after('status');
            $table->string('nama_ibu_mertua')->nullable()->after('status');
            $table->string('alamat_ibu_mertua')->nullable()->after('status');
            $table->string('nama_kenalan1')->nullable()->after('status');
            $table->string('alamat_kenalan1')->nullable()->after('status');
            $table->string('nama_kenalan2')->nullable()->after('status');
            $table->string('alamat_kenalan2')->nullable()->after('status');
            $table->string('nama_kenalan3')->nullable()->after('status');
            $table->string('alamat_kenalan3')->nullable()->after('status');
            $table->string('hobi')->nullable()->after('status');
            $table->string('kedudukan_di_masyarakat')->nullable()->after('status');
            $table->string('media_menaungi')->nullable()->after('status');
            $table->string('website_media')->nullable()->after('status');
            $table->string('nomor_kta')->nullable()->after('status');
            $table->string('nama_atasan_langsung')->nullable()->after('status');
            $table->string('no_telp_kantor')->nullable()->after('status');
            $table->string('jabatan')->nullable()->after('status');
            $table->string('foto_ktp')->nullable()->after('status');
            $table->string('foto_diri')->nullable()->after('status');
            $table->string('foto_kendaraan')->nullable()->after('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tamu', function (Blueprint $table) {
            //
            $table->dropColumn('waktu_kedatangan');
            $table->dropColumn('tempat_lahir');
            $table->dropColumn('tanggal_lahir');
            $table->dropColumn('jenis_kelamin');
            $table->dropColumn('suku');
            $table->dropColumn('kewarganegaraan');
            $table->dropColumn('agama');
            $table->dropColumn('pendidikan');
            $table->dropColumn('pekerjaan');
            $table->dropColumn('alamat_kantor');
            $table->dropColumn('status_perkawinan');
            $table->dropColumn('kepartaian');
            $table->dropColumn('ormas_lainnya');
            $table->dropColumn('nama_pasangan');
            $table->dropColumn('nama_anak_anak');
            $table->dropColumn('nama_ayah_kandung');
            $table->dropColumn('alamat_ayah_kandung');
            $table->dropColumn('nama_ibu_kandung');
            $table->dropColumn('alamat_ibu_kandung');
            $table->dropColumn('nama_ayah_mertua');
            $table->dropColumn('alamat_ayah_mertua');
            $table->dropColumn('nama_ibu_mertua');
            $table->dropColumn('alamat_ibu_mertua');
            $table->dropColumn('nama_kenalan1');
            $table->dropColumn('alamat_kenalan1');
            $table->dropColumn('nama_kenalan2');
            $table->dropColumn('alamat_kenalan2');
            $table->dropColumn('nama_kenalan3');
            $table->dropColumn('alamat_kenalan3');
            $table->dropColumn('hobi');
            $table->dropColumn('kedudukan_di_masyarakat');
            $table->dropColumn('media_menaungi');
            $table->dropColumn('website_media');
            $table->dropColumn('nomor_kta');
            $table->dropColumn('nama_atasan_langsung');
            $table->dropColumn('no_telp_kantor');
            $table->dropColumn('jabatan');
            $table->dropColumn('foto_ktp');
            $table->dropColumn('foto_diri');
            $table->dropColumn('foto_kendaraan');
        });
    }
}
