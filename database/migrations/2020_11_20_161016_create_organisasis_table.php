<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganisasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisasi', function (Blueprint $table) {
            $table->bigIncrements('id_organisasi');
            $table->unsignedBigInteger('id_satker');
            $table->unsignedBigInteger('id_bidang');
            $table->unsignedBigInteger('id_jabatan');
            $table->timestamps();

            $table->foreign('id_satker')->references('id_satker')->on('satker')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_bidang')->references('id_bidang')->on('bidang')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_jabatan')->references('id_jabatan')->on('jabatan')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organisasi', function (Blueprint $table) {
            $table->dropForeign('id_satker');
            $table->dropForeign('id_bidang');
            $table->dropForeign('id_jabatan');
        });
        Schema::dropIfExists('organisasis');
    }
}
