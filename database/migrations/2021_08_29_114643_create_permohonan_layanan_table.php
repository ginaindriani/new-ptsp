<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermohonanLayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan_layanan', function (Blueprint $table) {
            $table->uuid('id_permohonan_layanan')->primary();
            $table->integer('jenis_layanan');
            $table->date('tanggal')->nullable();
            $table->string('unique_nomor', 100)->nullable();
            $table->string('nama', 100)->nullable();
            $table->string('asal', 250)->nullable();
            $table->longText('konten');
            $table->string('file')->nullable();
            $table->string('file_signed')->nullable();
            $table->unsignedBigInteger('id_satker');
            $table->string('status', 250)->nullable();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permohonan_layanan');
    }
}
