<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewSatkerPath extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS satker_path");
        DB::statement("CREATE VIEW satker_path AS
                        WITH RECURSIVE satker_path (id, nama_satker, tipe_satker, path) AS
                        (
                        SELECT id_satker, nama_satker, tipe_satker, nama_satker as path
                            FROM satker
                            WHERE parent_id IS NULL
                        UNION ALL
                        SELECT c.id_satker, c.nama_satker, c.tipe_satker, CONCAT(sp.path, ' > ', c.nama_satker)
                            FROM satker_path AS sp JOIN satker AS c
                            ON sp.id = c.parent_id
                        )
                        SELECT * FROM satker_path
                        ORDER BY path;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS satker_path");
    }
}
