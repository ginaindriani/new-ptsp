<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrganisasiToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('id_organisasi')->after('password')->nullable();

            $table->foreign('id_organisasi')->references('id_organisasi')->on('organisasi')
                ->onUpdate('SET null')->onDelete('SET null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('id_organisasi');
            $table->dropColumn('id_organisasi');
        });
    }
}
