<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bidang', function (Blueprint $table) {
            $table->bigIncrements('id_bidang');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('nama_bidang', 250);
            $table->string('nama_bidang_full', 250);
            $table->timestamps();

            $table->foreign('parent_id')->references('id_bidang')->on('bidang')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bidang');
    }
}
