<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJabatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan', function (Blueprint $table) {
            $table->bigIncrements('id_jabatan');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('nama_jabatan', 100);
            $table->boolean('is_admin');
            $table->smallInteger('eselon');
            $table->timestamps();

            $table->foreign('parent_id')->references('id_jabatan')->on('jabatan')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jabatan');
    }
}
