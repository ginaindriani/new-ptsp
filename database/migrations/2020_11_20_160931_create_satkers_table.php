<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSatkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satker', function (Blueprint $table) {
            $table->bigIncrements('id_satker');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('nama_satker', 250);
            $table->smallInteger('tipe_satker')->comment('1: Kejagung; 2: Kejati; 3: Kejari; 4: Cabjari');
            $table->string('alamat_satker')->nullable();
            $table->string('telp_satker')->nullable();
            $table->string('website_satker')->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id_satker')->on('satker')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satker');
    }
}
