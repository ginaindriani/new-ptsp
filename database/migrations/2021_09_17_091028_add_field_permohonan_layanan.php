<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldPermohonanLayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permohonan_layanan', function (Blueprint $table) {
            $table->string('nomor_permohonan')->nullable()->after('id_permohonan_layanan');
            $table->string('nomor_surat')->nullable()->after('nomor_permohonan');
            $table->string('nomor_antrian')->nullable()->after('nomor_surat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permohonan_layanan', function (Blueprint $table) {
            $table->dropColumn('nomor_permohonan');
            $table->dropColumn('nomor_surat');
            $table->dropColumn('nomor_antrian');
        });
    }
}
