<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTujuanPenerusansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tujuan_penerusan', function (Blueprint $table) {
            $table->uuid('id_penerusan')->primary();
            $table->unsignedBigInteger('id_organisasi');
            $table->unsignedBigInteger('id_tujuan_penerusan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tujuan_penerusan');
    }
}
