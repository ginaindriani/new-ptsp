<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnJenisPengaduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permohonan_layanan', function (Blueprint $table) {
            $table->string('jenis_pengaduan')->nullable()->after('jenis_layanan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permohonan_layanan', function (Blueprint $table) {
            $table->dropColumn('jenis_pengaduan');
        });
    }
}
