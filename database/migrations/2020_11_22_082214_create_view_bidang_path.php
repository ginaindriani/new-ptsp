<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewBidangPath extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS bidang_path");
        DB::statement("CREATE VIEW bidang_path AS
                        WITH RECURSIVE bidang_path (id, nama_bidang, path, level) AS
                        (
                        SELECT id_bidang, nama_bidang, nama_bidang as path, 1 as level
                            FROM bidang
                            WHERE parent_id IS NULL
                        UNION ALL
                        SELECT c.id_bidang, c.nama_bidang, CONCAT(cp.path, ' > ', c.nama_bidang), (cp.level + 1) as level
                            FROM bidang_path AS cp JOIN bidang AS c
                            ON cp.id = c.parent_id
                        )
                        SELECT * FROM bidang_path
                        ORDER BY path;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_bidang_path');
    }
}
