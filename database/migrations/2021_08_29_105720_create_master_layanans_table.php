<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterLayanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_layanans', function (Blueprint $table) {
            $table->bigIncrements('id_layanan');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('nama_layanan', 250);
            $table->longText('parameter')->nullable();
            $table->string('blade', 100)->nullable();
            $table->string('kertas', 10)->nullable();
            $table->string('tipe_layanan', 10)->nullable();
            $table->smallInteger('aktif')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_layanans');
    }
}
