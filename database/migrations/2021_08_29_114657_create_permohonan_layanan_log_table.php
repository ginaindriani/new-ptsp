<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermohonanLayananLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan_layanan_log', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_permohonan_layanan');
            $table->unsignedBigInteger('id_organisasi');
            $table->text('komentar')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamp('checked_at')->nullable();
            $table->string('status', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permohonan_layanan_log');
    }
}
