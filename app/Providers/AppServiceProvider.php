<?php

namespace App\Providers;

use App\Core\Adapters\Theme;
use DB;
use Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $theme = theme();

        // Share theme adapter class
        View::share('theme', $theme);

        // Set demo globally
        $theme->setDemo(request()->input('demo', 'demo1'));
        // $theme->setDemo('demo2');

        $theme->initConfig();

        bootstrap()->run();

        if (isRTL()) {
            // RTL html attributes
            Theme::addHtmlAttribute('html', 'dir', 'rtl');
            Theme::addHtmlAttribute('html', 'direction', 'rtl');
            Theme::addHtmlAttribute('html', 'style', 'direction:rtl;');
        }
        $this->setCustomeValidation();

    }

    private function setCustomeValidation()
    {
        Validator::extend('idn_phone_number', function ($attribute, $value) {
            return preg_match('/^(^\+62\s?|^0)(\d{2,4}-?){2}\d{3,5}$/', $value);
        }, 'The :attribute invalid Indonesia phone number.');

        Validator::extend("idn_address", function ($attribute, $value) {
            return preg_match('/^[a-zA-Z0-9-_,.\s]*$/', $value);
        }, 'The :attribute invalid Indonesia address.');

        Validator::extend("idn_vehicle_license_plate", function ($attribute, $value) {
            return preg_match('/^[a-zA-Z0-9-_\s]*$/', $value);
        }, 'The :attribute invalid Indonesia vehicle license plate. The :attribute may only contain letters, numbers, and space.');
        Validator::extend('alpha_spaces', function ($attribute, $value) {
            return preg_match('/^[\pL\s]+$/u', $value);
        }, 'The :attribute may only contain letters and spaces.');

        Validator::extend('alpha_num_spaces', function ($attribute, $value) {
            return preg_match('/^[a-zA-Z0-9\s]*$/', $value);
        }, 'The :attribute may only contain letters, numbers and spaces.');

        Validator::extend('slug', function ($attribute, $value) {
            return preg_match('/^[a-zA-Z0-9-_]+$/', $value);
        }, 'The :attribute may only contain letters, numbers, dashes and underscores.');

        Validator::extend('check_uuid', function ($attribute, $value) {
            return preg_match('/^[a-zA-Z0-9-_]+$/', $value);
        }, 'The :attribute may only contain letters, numbers and dashes.');

        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            $user = DB::table('users')->where('uuid', $parameters)->first();
            if (empty($user)) {
                $user = DB::table('admins')->where('uuid', $parameters)->first();
            }
            return Hash::check($value, $user->password);
        }, 'The :attribute is not match with old password');
    }

}
