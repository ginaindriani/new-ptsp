<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return view('pages.users.action', compact('data'))->render();
            })
            ->addColumn('role', function($data) {
                $role = "";
                switch (strtolower($data->role)) {
                    case "superadmin":
                        $role = '<span class="badge badge-primary">' . $data->role . '</span>';
                        break;
                    case "admin":
                        $role = '<span class="badge badge-success">' . $data->role . '</span>';
                        break;
                    case "user":
                        $role = '<span class="badge badge-warning">' . $data->role . '</span>';
                        break;
                    default:
                        $role = '<span class="badge badge-secondary">' . $data->role . '</span>';
                        break;
                }
                return $role;
            })
            ->rawColumns(['action', 'role']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $role = strtolower(auth()->user()->roles->first()->name);
        $query = $model->newQuery()
                ->leftJoin('organisasi', 'organisasi.id_organisasi', '=', 'users.id_organisasi')
                ->leftJoin('satker', 'satker.id_satker', '=', 'organisasi.id_satker')
                ->leftJoin('bidang', 'bidang.id_bidang', '=', 'organisasi.id_bidang')
                ->leftJoin('jabatan', 'jabatan.id_jabatan', '=', 'organisasi.id_jabatan')
                ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                // ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
                ->when($role, function($query) use ($role) {
                        switch ($role) {
                                case "admin":
                                        if (auth()->user()->organisasi->satker->tipe_satker == 1) {
                                                return $query->where('bidang.main_parent_id', auth()->user()->organisasi->bidang->main_parent_id)->where('satker.id_satker', auth()->user()->organisasi->satker->id_satker);
                                        }
                                        return $query->where('satker.id_satker', auth()->user()->organisasi->id_satker);
                                case "user":
                                        return $query->where('users.id', auth()->user()->id);
                        }
                })
                ->where('users.id', '>', 1);

        $query = $query->select(['users.id as id', 'users.username as username', 'users.email as email', 'users.nama as nama', 'users.nik as nik', 'users.nip as nip', 
                                'satker.nama_satker as nama_satker', 'bidang.nama_bidang as nama_bidang', 'jabatan.nama_jabatan as nama_jabatan', 'users.role']);
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        return $this->builder()
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => route('user.index')
                    ])
                    ->buttons(
                        // Button::make('postExcel')->className('btn-light'),
                        Button::make('reset')->className('btn-light')
                    )
                    ->dom($domOption)
                    ->parameters([
                        'initComplete' => "function () {
                                var r = $('#user-table tfoot tr');
                                $('#user-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    var input = document.createElement('input');
                                    input.className = 'form-control form-control-sm';
                                    if(key > 0 && (key + 1) < $('#user-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty()).on('change', function () {
                                            column.search($(this).val(), false, false,true).draw();
                                        });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('nama_satker')
                    ->name('satker.nama_satker')
                    ->title('Nama Satker')
                    ->footer('Nama Satker'), 
            Column::make('nama_bidang')
                    ->name('bidang.nama_bidang')
                    ->title('Nama Bidang')
                    ->footer('Nama Bidang'), 
            Column::make('nama_jabatan')
                    ->name('jabatan.nama_jabatan')
                    ->title('Nama Jabatan')
                    ->footer('Nama Jabatan'), 
            Column::make('nama')
                    ->name('users.nama')
                    ->title('Nama')
                    ->footer('Nama'),
            Column::make('nip')
                    ->name('users.nip')
                    ->title('NIP')
                    ->footer('NIP'),
            Column::make('username')
                    ->name('suers.username')
                    ->title('Username')
                    ->footer('Username'),
            Column::make('email')
                    ->name('users.email')
                    ->title('Email')
                    ->footer('Email'), 
            Column::make('role')
                    ->name('suers.role')
                    ->title('Role')
                    ->footer('Role'),  
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(85)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)     
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
