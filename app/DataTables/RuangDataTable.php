<?php

namespace App\DataTables;

use Carbon\Carbon;
use App\Models\PermohonanLayanan;
use App\Models\PermohonanLayananLog;
use App\Helpers\OrganisasiHelper;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\DataTables\LazyDataTablesExportHandler;
use App\Models\RuangDiskusi;

class RuangDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('created_at', function($data) {
                return Carbon::parse($data->created_at)->format('Y-m-d H:i:s');
            })
            ->editColumn('message', function($data) {
                return strlen($data->message) > 50 ? substr($data->message, 0, 50) . ' . . . ' : $data->message;
            })
            ->editColumn('is_answer', function($data) {
                if ($data->is_answer) {
                    $string = '<span class="badge badge-success">Ditanggapi</span>';
                    $string .= '<div>' . Carbon::parse($data->answer_date)->format('Y-m-d H:i:s') . '</div>';
                    return $string;
                } 
                else {
                    $string = '<span class="badge badge-danger">Belum ditanggapi</span>';
                    return $string;
                }
            })
            ->editColumn('action', function($data){
                return view('pages.ruang_diskusi.action', compact('data'))->render();
            })
            ->filterColumn('created_at', function($query, $keyword) {
                $date = date('Y-m-d', strtotime($keyword));
                $query->where('ruang_diskusis.created_at', '%' . $date . '%');
            })
            ->filterColumn('is_answer', function($query, $keyword) {
                dd($keyword);
            })
            ->rawColumns(['action', 'is_answer', 'message']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(RuangDiskusi $model)
    {
        // return $model->newQuery();
        $data = $model->newQuery()
            ->select([
                'ruang_diskusis.*'
            ])->orderBy('ruang_diskusis.created_at','desc');

        // foreach (request()->all()['columns'] as $key => $value) {
        //     if ($value['data'] == 'created_at') {
        //         if (isset($value['search']['value'])) {
        //             $date = date('Y-m-d', strtotime($value['search']['value']));
        //             $data = $data->whereRaw(DB::raw("DATE(ruang_diskusis.created_at) = '$date'"));
        //         }
        //     }
        // }

        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

        return $this->builder()
            ->columns($this->getColumns())
            ->buttons(
                // Button::make('postExcel')->className('btn-light'),
                Button::make('reset')->className('btn-light')
            )
            ->postAjax([
                'url' => route('ruang-diskusi.index'),
                'data' => 'function(d) {
                    d.idLayanan = $(\'input[name=id_layanan]\').val();
                }'
            ])
            ->dom($domOption)
            ->minifiedAjax()
            ->parameters([
                'initComplete' => "function () {
                    var r = $('#ruang-diskusi-table tfoot tr');
                    $('#ruang-diskusi-table thead').append(r);
                    this.api().columns().every(function (key) {
                        var column = this;
                        var input = document.createElement('input');
                        input.className = 'form-control form-control-sm';
                        if(key > 0 && (key + 1) < $('#ruang-diskusi-table thead tr:nth-child(1) th').length) {
                            $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false,true).draw();
                                });
                        } else {
                            $('').appendTo($(column.footer()).empty());
                        }
                    });
                }"
            ])
            ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('name')
                    ->name('name')
                    ->title('Nama')
                    ->footer('Nama'),
            Column::make('email')
                    ->name('ruang_diskusis.email')
                    ->title('Email')
                    ->footer('Email'),
            Column::make('phone')
                    ->name('ruang_diskusis.phone')
                    ->title('Phone')
                    ->footer('Phone'),
            Column::make('message')
                    ->name('ruang_diskusis.message')
                    ->title('Pesan')
                    ->footer('Pesan'),
            Column::make('is_answer')
                    ->name('ruang_diskusis.is_answer')
                    ->title('Ditanggapi')
                    ->footer('Ditanggapi'),
            Column::make('created_at')
                    ->name('ruang_diskusis.created_at')
                    ->title('Tanggal')
                    ->footer('Tanggal'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(120)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Ruang_diskusi_' . date('YmdHis');
    }
}
