<?php

namespace App\DataTables;

use Carbon\Carbon;
use App\Models\PermohonanLayanan;
use App\Models\PermohonanLayananLog;
use App\Helpers\OrganisasiHelper;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\DataTables\LazyDataTablesExportHandler;
use App\Models\Satker;

class LayananDaftarDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('tanggal', function($data) {
                return Carbon::parse($data->tanggal)->format('d-m-Y');
            })
            ->editColumn('nama', function($data) {
                if($data->file!=""){
                  return  $data->nama . " <a href='{{asset('layanan/permohonan/$data->id_permohonan_layanan/downloadfile')}}'  class='btn btn-primary btn-sm' title='Download File' target='_blank'><i class='fas fa-file-pdf text-white'></i></a>";
                }else{
                  return  $data->nama ;
                }
            })
            ->editColumn('status', function($data) {
                if ( $data->status == null ) {
                    return '<span class="label label-rounded label-danger">Belum Ditanggapi</span>';
                }
                if ( $data->status == 1) {
                    return '<span class="label label-rounded label-warning">Sedang Diproses (Terkirim SIPEDE)</span>';
                } else {
                    return '<span class="label label-rounded label-success">Selesai</span>';
                }
            })
            ->editColumn('action', function($data){
                return view('pages.permohonan.action', compact('data'))->render();
            })
            ->filterColumn('tanggal', function($query, $keyword) {
                $date = date('Y-m-d', strtotime($keyword));
                $query->where('permohonan_layanan.tanggal', $date);
            })
            ->rawColumns(['action', 'status', 'tanggal', 'nama']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PermohonanLayanan $model)
    {
        // return $model->newQuery();
        $data = $model->newQuery()
            // ->where('permohonan_layanan.jenis_layanan', $this->idLayanan)
            ->join('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->join('satker', 'satker.id_satker', '=', 'permohonan_layanan.id_satker')
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'permohonan_layanan.id_permohonan_layanan',
                'permohonan_layanan.jenis_layanan',
                'permohonan_layanan.nama',
                'permohonan_layanan.tanggal',
                'permohonan_layanan.created_at',
                'permohonan_layanan.konten',
                'permohonan_layanan.id_satker',
                'master_layanans.nama_layanan',
                'satker.nama_satker',
                'permohonan_layanan.status'
            ])->orderBy('permohonan_layanan.created_at','desc');

        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $data = $data->where('permohonan_layanan.id_satker', $user->kode_satker);
        }

        if($this->tipe == '1'){
            $data = $data->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString());
        }elseif($this->tipe == '2'){
            $data = $data->where('permohonan_layanan.status', null);
        }else{

        }

        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

        return $this->builder()
            // ->setTableId('permohonan-table')
            ->columns($this->getColumns())
            ->buttons(
                // Button::make('postExcel')->className('btn-light'),
                Button::make('reset')->className('btn-light')
            )
            ->postAjax([
                'url' => route('permohonan.index'),
                'data' => 'function(d) {
                    d.idLayanan = $(\'input[name=id_layanan]\').val();
                }'
            ])
            ->dom($domOption)
            ->minifiedAjax()
            ->parameters([
                'initComplete' => "function () {
                    var r = $('#permohonan-table tfoot tr');
                    $('#permohonan-table thead').append(r);
                    this.api().columns().every(function (key) {
                        var column = this;
                        var input = document.createElement('input');
                        input.className = 'form-control form-control-sm';
                        if(key > 0 && (key + 1) < $('#permohonan-table thead tr:nth-child(1) th').length) {
                            $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false,true).draw();
                                });
                        } else {
                            $('').appendTo($(column.footer()).empty());
                        }
                    });
                }"
            ])
            ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('tanggal')
                    ->name('tanggal')
                    ->title('Tanggal')
                    ->footer('Tanggal'),
            Column::make('nama_satker')
                    ->name('permohonan_layanan.nama_satker')
                    ->title('Nama Satker')
                    ->footer('Nama Satker'),
            Column::make('nama_layanan')
                    ->name('permohonan_layanan.nama_layanan')
                    ->title('Tipe Layanan')
                    ->footer('Tipe Layanan'),
            Column::make('nama')
                    ->name('permohonan_layanan.nama')
                    ->title('Nama')
                    ->footer('Nama'),
            Column::make('status')
                    ->name('permohonan_layanan.status')
                    ->title('Status')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('Status'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(120)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Permohonan_Layanan_' . date('YmdHis');
    }
}
