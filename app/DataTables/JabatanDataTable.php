<?php

namespace App\DataTables;

use App\Models\Jabatan;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class JabatanDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return view('pages.master.jabatan.action', compact('data'))->render();
            })
            ->editColumn('eselon', function($data) {
                switch($data->eselon) {
                        case 1:
                                return "Eselon 1";
                        case 2:
                                return "Eselon 2";
                        case 3:
                                return "Eselon 3";
                        case 4:
                                return "Eselon 4";
                        case 5:
                                return "Eselon 5";
                        default:
                                return "Eselon 0";
                            
                }
            })
            ->editColumn('is_admin', function($data) {
                return $data->is_admin ? '<span class="label label-rounded label-success">Ya</span>' : '<span class="label label-rounded label-warning">Tidak</span>';
            })
            ->rawColumns(['action', 'is_admin']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Jabatan $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Jabatan $model)
    {
        $query = $model->newQuery()
                        ->leftJoin('jabatan as parentjabatan', 'parentjabatan.id_jabatan', '=', 'jabatan.parent_id')
                        ->select('jabatan.id_jabatan', 'jabatan.nama_jabatan', 'jabatan.eselon', 'jabatan.is_admin', 'parentjabatan.nama_jabatan as nama_jabatan_parent');
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        return $this->builder()
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => route('jabatan.index')
                    ])
                    ->buttons(
                        // Button::make('postExcel')->className('btn-light'),
                        Button::make('reset')->className('btn-light')
                    )
                    ->dom($domOption)
                    ->parameters([
                        "order"=> [[ 2, "asc" ]],
                        'initComplete' => "function () {
                                var r = $('#jabatan-table tfoot tr');
                                $('#jabatan-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    var input = document.createElement('input');
                                    input.className = 'form-control form-control-sm';
                                    if(key > 0 && (key + 1) < $('#jabatan-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false,true).draw();
                                            });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('nama_jabatan_parent')
                    ->name('parentjabatan.nama_jabatan')
                    ->title('Parent')
                    ->footer('Parent'),
            Column::make('nama_jabatan')
                    ->name('jabatan.nama_jabatan')
                    ->title('Nama Jabatan')
                    ->footer('Nama Jabatan'),
            Column::make('eselon')
                    ->name('jabatan.eselon')
                    ->title('Eselon')
                    ->footer('Eselon'),
            Column::make('is_admin')
                    ->name('jabatan.is_admin')
                    ->title('Is Admin')
                    ->searchable(false)
                    ->footer('Is Admin'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(85)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)     
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Jabatan_' . date('YmdHis');
    }
}
