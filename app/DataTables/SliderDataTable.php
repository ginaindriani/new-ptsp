<?php

namespace App\DataTables;

use App\Models\Sliders;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SliderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return view('pages.master.slider.action', compact('data'))->render();
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Sliders $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Sliders $model)
    {
        $query = $model->newQuery()
                        ->select('sliders.*')->orderBy('sliders.created_at','desc');;
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        return $this->builder()
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => route('slider.index')
                    ])
                    ->buttons(
                        // Button::make('postExcel')->className('btn-light'),
                        Button::make('reset')->className('btn-light')
                    )
                    ->dom($domOption)
                    ->parameters([
                        "order"=> [[ 1, "asc" ]],
                        'initComplete' => "function () {
                                var r = $('#sliders-table tfoot tr');
                                $('#sliders-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    var input = document.createElement('input');
                                    input.className = 'form-control form-control-sm';
                                    if(key > 0 && (key + 2) < $('#sliders-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false,true).draw();
                                            });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('judul_slider')
                    ->name('sliders.judul_slider')
                    ->title('Judul Slider')
                    ->footer('Judul Slider'),
            Column::make('deskripsi_slider')
                    ->name('sliders.deskripsi_slider')
                    ->title('Deskripsi Slider')
                    ->searchable(false)
                    ->footer('Deskripsi Slider'),
            Column::make('url_slider')
                    ->name('sliders.url_slider')
                    ->title('Url Slider')
                    ->searchable(false)
                    ->footer('Url Slider'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(85)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)     
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Slider_' . date('YmdHis');
    }
}
