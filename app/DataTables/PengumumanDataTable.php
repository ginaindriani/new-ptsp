<?php

namespace App\DataTables;

use Carbon\Carbon;
use App\Models\PermohonanLayanan;
use App\Models\PermohonanLayananLog;
use App\Helpers\OrganisasiHelper;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\DataTables\LazyDataTablesExportHandler;
use App\Models\Berita;
use App\Models\Dpo;
use App\Models\Pengumuman;
use App\Models\Satker;

class PengumumanDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return view('pages.master.pengumuman.action', compact('data'))->render();
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Pengumuman $model)
    {
        // return $model->newQuery();
        $data = $model->newQuery()
            ->join('satker', 'satker.id_satker', '=', 'pengumuman.id_satker')
            ->select([
                'satker.nama_satker',
                'pengumuman.*'
            ])->orderBy('pengumuman.created_at','desc');
        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $data = $data->where('pengumuman.id_satker', $user->kode_satker);
        }

        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        return $this->builder()
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => route('pengumuman.index')
                    ])
                    ->buttons(
                        Button::make('reset')->className('btn-light')
                    )
                    ->dom($domOption)
                    ->parameters([
                        "order"=> [[ 1, "asc" ]],
                        'initComplete' => "function () {
                                var r = $('#pengumuman-table tfoot tr');
                                $('#pengumuman-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    var input = document.createElement('input');
                                    input.className = 'form-control form-control-sm';
                                    if(key > 0 && (key + 2) < $('#pengumuman-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false,true).draw();
                                            });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('nama_satker')
                    ->name('satker.nama_satker')
                    ->title('Nama Satker')
                    ->footer('Nama Satker'),
            Column::make('judul_pengumuman')
                    ->name('pengumuman.judul_pengumuman')
                    ->title('Judul Pengumuman')
                    ->footer('Judul Pengumuman'),
            Column::make('deskripsi_pengumuman')
                    ->name('pengumuman.deskripsi_pengumuman')
                    ->title('Deskripsi Pengumuman')
                    ->footer('Deskripsi Pengumuman'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(120)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Permohonan_Layanan_' . date('YmdHis');
    }
}
