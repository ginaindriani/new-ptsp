<?php

namespace App\DataTables;

use App\Models\Bidang;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class BidangDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return view('pages.master.bidang.action', compact('data'))->render();
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Bidang $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Bidang $model)
    {
        $query = $model->newQuery()
                        ->leftJoin('bidang as parentbidang', 'parentbidang.id_bidang', '=', 'bidang.parent_id')
                        ->leftJoin('bidang as mainparentbidang', 'mainparentbidang.id_bidang', '=', 'bidang.main_parent_id')
                        ->select('bidang.id_bidang', 'bidang.nama_bidang', 'parentbidang.nama_bidang as nama_bidang_parent', 'mainparentbidang.nama_bidang as nama_main_parent_bidang');
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        return $this->builder()
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => route('bidang.index')
                    ])
                    ->buttons(
                        // Button::make('postExcel')->className('btn-light'),
                        Button::make('reset')->className('btn-light')
                    )
                    ->dom($domOption)
                    ->parameters([
                        "order"=> [[ 3, "asc" ]],
                        'initComplete' => "function () {
                                var r = $('#bidang-table tfoot tr');
                                $('#bidang-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    var input = document.createElement('input');
                                    input.className = 'form-control form-control-sm';
                                    if(key > 0 && (key + 1) < $('#bidang-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false,true).draw();
                                            });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('nama_main_parent_bidang')
                    ->name('mainparentbidang.nama_bidang')
                    ->title('Main Parent')
                    ->footer('Main Parent'),
            Column::make('nama_bidang_parent')
                    ->name('parentbidang.nama_bidang')
                    ->title('Parent')
                    ->footer('Parent'),
            Column::make('nama_bidang')
                    ->name('bidang.nama_bidang')
                    ->title('Nama Bidang')
                    ->footer('Nama Bidang'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(85)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)     
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Bidang_' . date('YmdHis');
    }
}
