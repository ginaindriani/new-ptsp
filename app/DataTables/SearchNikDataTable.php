<?php

namespace App\DataTables;

use App\Models\PermohonanLayanan;
use DB;
use Request;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SearchNikDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $model = datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('nik', function ($data) {
                return $data["konten"]["nik"];
            })
            ->filterColumn('tanggal', function($query, $keyword) {
                $date = date('Y-m-d', strtotime($keyword));
                $query->where('permohonan_layanan.tanggal', $date);
            })
            ->editColumn('nama', function($data) {
                if($data->file != ""){
                    $image = $data->id_permohonan_layanan;
                    return  $data->nama . "<a href='" . asset("layanan/permohonan/$image/downloadfile") . "'  class='btn btn-primary btn-sm ms-2' title='Download File' target='_blank'><i class='fas fa-file-pdf text-white'></i> File </a>";
                }else{
                    return  $data->nama ;
                }
            })
            ->editColumn('status', function($data) {
                if ( $data->status == null ) {
                    return '<span class="label label-rounded label-danger">Belum Ditanggapi</span>';
                }
                if ( $data->status == 1) {
                    return '<span class="label label-rounded label-warning">Dalam Diproses</span>';
                } else {
                    return '<span class="label label-rounded label-success">Selesai</span>';
                }
            })
            ->rawColumns(['action', 'nik', 'status', 'nama']);

        $model->addColumn('action', function($data){
            if (auth()->user()) {
                return view('pages.permohonan.action', compact('data'))->render();
            } else {
                $url = route('permohonan.showdetail', ['id' => $data->id_permohonan_layanan]) . '?id_layanan=' . $data->jenis_layanan;
                return "<a href=\"$url\" class=\"btn btn-icon btn-warning btn-sm\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"View\">
                    <i class=\"fa fa-eye text-white\"></i>
                </a>";
            }
        });

        return $model;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Dashboard $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PermohonanLayanan $model)
    {
        $query = $model->newQuery()
                        ->join('master_layanans', 'master_layanans.id_layanan', '=', 'permohonan_layanan.jenis_layanan')
                        ->join('satker', 'permohonan_layanan.id_satker', '=', 'satker.id_satker')
                        ->select([
                            'permohonan_layanan.id_permohonan_layanan', 
                            'permohonan_layanan.nama', 
                            'permohonan_layanan.status', 
                            'permohonan_layanan.tanggal', 
                            'permohonan_layanan.file', 
                            'permohonan_layanan.jenis_layanan', 
                            'permohonan_layanan.konten', 
                            'satker.nama_satker', 
                            'master_layanans.nama_layanan'
                        ]);

        // $query->whereRaw(DB::raw('permohonan_layanan.konten  REGEXP \'"nik":"([^"])*'. request()->nik .'([^"])*"\' '));
        $query->whereRaw(DB::raw("permohonan_layanan.konten LIKE '%nik\":\"" . request()->nik . "\"%'"));
        // $query->whereRaw(DB::raw("permohonan_layanan.konten LIKE %" . request()->nik . "%"));

        // return request()->nik;
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        $builder = $this->builder()
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => route('permohonan.dataModal'),
                        'data' => 'function(d) {
                            d.nik = $("[name=\'nik\']").val();
                        }'
                    ])
                    // ->buttons(
                    //     Button::make('postExcel')->className('btn-light'),
                    //     Button::make('reset')->className('btn-light')
                    // )
                    ->dom($domOption)
                    ->parameters([
                        'initComplete' => "function () {
                                var r = $('#search-nik-table tfoot tr');
                                $('#search-nik-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    var input = document.createElement('input');
                                    input.className = 'form-control form-control-sm';
                                    if(key > 0 && (key + 1) < $('#search-nik-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false,true).draw();
                                            });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);

        if (auth()->user()) {
            $builder->buttons(
                Button::make('postExcel')->className('btn-light'),
                Button::make('reset')->className('btn-light')
            );
        } else {
            $builder->buttons(
                Button::make('reset')->className('btn-light')
            );
        }

        return $builder;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('nama_satker')
                    ->name('satker.nama_satker')
                    ->title('Nama Satker')
                    ->footer('Nama Satker'),
            Column::make('nama')
                    ->name('permohonan_layanan.nama')
                    ->title('Nama')
                    ->footer('Nama'),
            Column::make('tanggal')
                    ->name('tanggal')
                    ->title('Tanggal')
                    ->footer('Tanggal'),
            Column::make('nama_layanan')
                    ->name('master_layanans.nama_layanan')
                    ->title('Layanan')
                    ->footer('Layanan'),
            Column::make('status')
                    ->name('permohonan_layanan.status')
                    ->title('Status')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('Status'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(85)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)     
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'permohonan_layanan_' . date('YmdHis');
    }
}
