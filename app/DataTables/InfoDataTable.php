<?php

namespace App\DataTables;

use Carbon\Carbon;
use App\Models\PermohonanLayanan;
use App\Models\PermohonanLayananLog;
use App\Helpers\OrganisasiHelper;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\DataTables\LazyDataTablesExportHandler;
use App\Models\MasterLayanan;
use App\Models\Satker;
use Illuminate\Support\Facades\Request;

class InfoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public function dataTable($query)
    {
        $masterLayanan = MasterLayanan::where('id_layanan', $this->id_layanan)->first();
        $datatable = datatables()
            ->eloquent($query)
            ->addIndexColumn();
        foreach ($masterLayanan->parameter as $key => $value) {
            $datatable->addColumn($key, function($data) use ($key) {
                if ($key != 'jenis_bidang') {

                    if ($key == 'satker') {
                        $satker = Satker::find($data->konten[$key]);
                        return $satker->nama_satker;
                    } else if ($key == 'src') {
                        preg_match("/^.*\.(jpg|jpeg|png|gif)$/i", $data->konten[$key], $output_array);
                        $url = asset('storage'. $this->path . $data->konten[$key]);
                        $ext = isset($output_array[1]) ? $output_array[1] : "";
                        $url = asset('storage/permohonan/' . $data->konten[$key]);
                        return !$ext ? "<a target='_blank' href='$url'>" . $data->konten[$key] . "</a>" : '<div style="width: 200px; height: 100px;"><img src="'. $url .'" class="" alt="" style="width: auto; height: 100%;"></div>';
                    } else if ($key == 'peta') {
                        $latLong = explode(',', $data->konten[$key]);
                        $lat = isset($latLong[0]) ? $latLong[0] : '';
                        $long = isset($latLong[1]) ? $latLong[1] : '';
                        if ($lat && $long) {
                            return "<button type='button' class='btn btn-info btn-sm open_location' data-lat='$lat' data-lng='$long'><i class='icon md-search' aria-hidden='true'></i>Lokasi</button>";
                        } else {
                            return '-';
                        }
                    }
                    else {
                        return $data->konten[$key];
                    }
                }
            });
            $datatable->filterColumn($key, function($query, $keyword) use ($key) {
                if ($key == 'satker') {
                    $satker = Satker::where('nama_satker', $keyword)->first();
                    $query->where("permohonan_layanan.konten", "LIKE", "%satker\":\"$satker->id_satker\"%");
                } else {
                    $query->where("permohonan_layanan.konten", "LIKE", "%$key\":\"$keyword\"%");
                }
            });
        }

        $datatable->rawColumns(['action', 'src', 'peta']);
        return $datatable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Request $request, PermohonanLayanan $model)
    {
        // return $model->newQuery();
        $data = $model->newQuery()
            ->where('permohonan_layanan.jenis_layanan', $this->id_layanan)
            ->select([
                'permohonan_layanan.id_permohonan_layanan',
                'permohonan_layanan.konten'
            ])
            ->orderBy('permohonan_layanan.created_at', 'desc');

        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";

        return $this->builder()
            // ->setTableId('permohonan-table')
            ->columns($this->getColumns())
            ->buttons(
                // Button::make('postExcel')->className('btn-light'),
                Button::make('reset')->className('btn-light')
            )
            ->postAjax([
                'url' => route('permohonan.info-pakem'),
                'data' => 'function(d) {
                    d.id_layanan = $(\'input[name=id_layanan]\').val();
                }'
            ])
            ->dom($domOption)
            ->minifiedAjax()
            ->parameters([
                'initComplete' => "function () {
                    var r = $('#permohonan-table tfoot tr');
                    $('#permohonan-table thead').append(r);
                    this.api().columns().every(function (key) {
                        var column = this;
                        var input = document.createElement('input');
                        input.className = 'form-control form-control-sm';
                        if(key > 0 && (key + 1) < $('#permohonan-table thead tr:nth-child(1) th').length) {
                            $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false,true).draw();
                                });
                        } else {
                            $('').appendTo($(column.footer()).empty());
                        }
                    });
                }"
            ])
            ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $masterLayanan = MasterLayanan::where('id_layanan', $this->id_layanan)->first();

        $columns = [
            Column::make('DT_RowIndex')
                ->title('No')
                ->orderable(false)
                ->searchable(false)
                ->footer('No')
        ];

        foreach ($masterLayanan->parameter as $key => $value) {

            if ($value['title'] != 'Jenis Bidang') {


                $columns[] = Column::make($key)
                ->name($key)
                ->title($value['title'])
                ->orderable(false)
                ->footer($value['title']);
            }

        }
        // $columns[] = Column::make('action')
        //                 ->title('Aksi')
        //                 ->footer('Aksi')
        //                 ->width(120)
        //                 ->exportable(false)
        //                 ->printable(false)
        //                 ->orderable(false)
        //                 ->searchable(false);
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Permohonan_Layanan_' . date('YmdHis');
    }
}
