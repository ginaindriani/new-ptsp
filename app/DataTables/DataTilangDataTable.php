<?php

namespace App\DataTables;

use Carbon\Carbon;
use App\Helpers\OrganisasiHelper;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\DataTables\LazyDataTablesExportHandler;
use App\Models\Berita;
use App\Models\Dpo;
use App\Models\Lelang;
use App\Models\Satker;
use App\Models\KunjunganTilang;

class DataTilangDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return view('pages.master.tilang.action', compact('data'))->render();
            })
            ->editColumn('type', function($data) {
                switch($data->type) {
                        case 1:
                                return "Tamu Biasa";
                        case 2:
                                return "Saksi";
                        case 3:
                                return "Terdakwa";
                }
            })
            ->editColumn('created_at', function($data) {
                return Carbon::parse($data->created_at)->format('Y-m-d H:i:s');
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(KunjunganTilang $model)
    {
        // return $model->newQuery();
        // dd($model);
        $data = $model->newQuery()
            ->join('satker', 'satker.id_satker', '=', 'kunjungan_tilang.id_satker')
            ->select([
                'satker.nama_satker',
                'kunjungan_tilang.*'
            ])
            ->orderBy('kunjungan_tilang.created_at','desc');
        $user = auth()->user();
        
       
        return $data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        return $this->builder()
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => route('tilang.index')
                    ])
                    ->buttons(
                        // Button::make('postExcel')->className('btn-light'),
                        Button::make('reset')->className('btn-light')
                    )
                    ->dom($domOption)
                    ->parameters([
                        "order"=> [[ 1, "asc" ]],
                        'initComplete' => "function () {
                                var r = $('#tilang-table tfoot tr');
                                $('#tilang-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    if(key > 0 && (key + 2) < $('#tilang-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false,true).draw();
                                            });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false),
            Column::make('nama_tamu')
                    ->name('kunjungan_tilang.nama_tamu')
                    ->title('Nama'),
            Column::make('nomor_berkas_tilang')
                    ->name('kunjungan_tilang.nomor_berkas_tilang')
                    ->title('No. Berkas Tilang'),
            Column::make('alamat')
                    ->name('kunjungan_tilang.alamat')
                    ->title('Alamat'),
            // Column::make('DT_RowIndex')
            //         ->title('No')
            //         ->orderable(false)
            //         ->searchable(false),
            // Column::make('nama_satker')
            //         ->name('kunjungan_tilang_detail.nama')
            //         ->title('Nama'),
            // Column::make('type')
            //         ->name('kunjungan_tilang_detail.alamat')
            //         ->title('Alamat'),
            // Column::make('nik')
            //         ->name('kunjungan_tilang_detail.no_reg_tilang')
            //         ->title('No reg tilang'),
            // Column::make('name')
            //         ->name('kunjungan_tilang_detail.denda')
            //         ->title('Denda'),
            // Column::make('email')
            //         ->name('kunjungan_tilang_detail.pasal')
            //         ->title('Pasal'),
            // Column::make('address')
            //         ->name('kunjungan_tilang_detail.tgl_sidang')
            //         ->title('Tanggal Sidang'),
            // Column::make('phone')
            //         ->name('kunjungan_tilang_detail.nama_petugas')
            //         ->title('Nama Petugas'),
            // Column::make('jenis_kendaraan')
            //         ->name('kunjungan_tilang_detail.jenis_kendaraan')
            //         ->title('Jenis Kendaraan'),
            // Column::make('created_at')
            //         ->name('kunjungan_tilang.created_at')
            //         ->title('Tanggal')
            //         ->footer('Tanggal'),
            // Column::make('action')
            //         ->title('Aksi')
            //         ->footer('Aksi')
            //         ->width(120)
            //         ->exportable(false)
            //         ->printable(false)
            //         ->orderable(false)
            //         ->searchable(false)
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Permohonan_Layanan_' . date('YmdHis');
    }
}
