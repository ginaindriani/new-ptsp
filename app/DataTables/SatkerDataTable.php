<?php

namespace App\DataTables;

use App\Models\Satker;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SatkerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return view('pages.master.satker.action', compact('data'))->render();
            })
            ->editColumn('tipe_satker', function($data) {
                switch($data->tipe_satker) {
                        case 1:
                                return "Kejaksaan Agung";
                        case 2:
                                return "Kejaksaan Tinggi";
                        case 3:
                                return "Kejaksaan Negeri";
                        case 4:
                                return "Cabang Kejaksaan Negeri";
                }
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Satker $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Satker $model)
    {
        $query = $model->newQuery()
                        ->leftJoin('satker as parentsatker', 'parentsatker.id_satker', '=', 'satker.parent_id')
                        ->select('satker.id_satker', 'satker.nama_satker', 'satker.tipe_satker', 'parentsatker.nama_satker as nama_satker_parent');
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        return $this->builder()
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => route('satker.index')
                    ])
                    ->buttons(
                        // Button::make('postExcel')->className('btn-light'),
                        Button::make('reset')->className('btn-light')
                    )
                    ->dom($domOption)
                    ->parameters([
                        "order"=> [[ 1, "asc" ]],
                        'initComplete' => "function () {
                                var r = $('#satker-table tfoot tr');
                                $('#satker-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    var input = document.createElement('input');
                                    input.className = 'form-control form-control-sm';
                                    if(key > 0 && (key + 2) < $('#satker-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false,true).draw();
                                            });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('nama_satker_parent')
                    ->name('parentsatker.nama_satker')
                    ->title('Parent')
                    ->footer('Parent'),
            Column::make('nama_satker')
                    ->name('satker.nama_satker')
                    ->title('Nama Satker')
                    ->footer('Nama Satker'),
            Column::make('tipe_satker')
                    ->name('satker.tipe_satker')
                    ->title('Tipe Satker')
                    ->searchable(false)
                    ->footer('Tipe Satker'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(85)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)     
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Satker_' . date('YmdHis');
    }
}
