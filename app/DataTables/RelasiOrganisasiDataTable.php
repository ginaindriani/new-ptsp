<?php

namespace App\DataTables;

use App\Models\Organisasi;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class RelasiOrganisasiDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $model = datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->rawColumns(['action']);

        if ($this->type == 'jabatan') {
            $model->addColumn('action', function($data){
                return view('pages.master.relasiorganisasi.jabatanaction', compact('data'))->render();
            });
        } else {
            $model->addColumn('action', function($data){
                return view('pages.master.relasiorganisasi.action', compact('data'))->render();
            });
        }

        return $model;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\RelasiOrganisasi $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Organisasi $model)
    {
        $query = $model->newQuery()
                        ->join('satker', 'satker.id_satker', '=', 'organisasi.id_satker')
                        ->join('bidang', 'bidang.id_bidang', '=', 'organisasi.id_bidang')
                        ->join('jabatan', 'jabatan.id_jabatan', '=', 'organisasi.id_jabatan')
                        ->select('organisasi.id_organisasi', 'satker.nama_satker', 'bidang.nama_bidang', 'jabatan.nama_jabatan');

        if ($this->type == 'jabatan') {
            $query->where('organisasi.id_satker', auth()->user()->organisasi->id_satker);
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $domOption = "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>
                          <'row'<'col-sm-12'tr>>
                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
        
        $builder = $this->builder()
                    ->columns($this->getColumns())
                    // ->postAjax([
                    //     'url' => route('relasiorganisasi.index')
                    // ])
                    ->buttons(
                        // Button::make('postExcel')->className('btn-light'),
                        Button::make('reset')->className('btn-light')
                    )
                    ->dom($domOption)
                    ->parameters([
                        'initComplete' => "function () {
                                var r = $('#relasiorganisasi-table tfoot tr');
                                $('#relasiorganisasi-table thead').append(r);
                                this.api().columns().every(function (key) {
                                    var column = this;
                                    var input = document.createElement('input');
                                    input.className = 'form-control form-control-sm';
                                    if(key > 0 && (key + 1) < $('#relasiorganisasi-table thead tr:nth-child(1) th').length) {
                                        $(input).appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                column.search($(this).val(), false, false,true).draw();
                                            });
                                    } else {
                                        $('').appendTo($(column.footer()).empty());
                                    }
                                });
                            }"
                    ]);

        if ($this->type != 'jabatan') {
            $builder->postAjax([
                'url' => route('relasiorganisasi.index')
            ]);
        }

        return $builder;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                    ->title('No')
                    ->orderable(false)
                    ->searchable(false)
                    ->footer('No'),
            Column::make('nama_satker')
                    ->name('satker.nama_satker')
                    ->title('Nama Satker')
                    ->footer('Nama Satker'),
            Column::make('nama_bidang')
                    ->name('bidang.nama_bidang')
                    ->title('Nama Bidang')
                    ->footer('Nama Bidang'),
            Column::make('nama_jabatan')
                    ->name('jabatan.nama_jabatan')
                    ->title('Nama Jabatan')
                    ->footer('Nama Jabatan'),
            Column::make('action')
                    ->title('Aksi')
                    ->footer('Aksi')
                    ->width(85)
                    ->exportable(false)
                    ->printable(false)
                    ->orderable(false)
                    ->searchable(false)     
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Organisasi_' . date('YmdHis');
    }
}
