<?php

namespace App\Http\Requests;

use App\Rules\Base64ImageTamu;
use Illuminate\Foundation\Http\FormRequest;

class BukutamuRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik_ktp'               => 'nullable|required_if:nik_type,1|numeric|digits:16',
            'nik_sim'               => 'nullable|required_if:nik_type,2|numeric|digits:14',
            'nik_passport'          => 'nullable|required_if:nik_type,3|alpha_num|min:6',
            'nik_type'              => 'required|numeric',
            // 'bidang' => 'required|uuid',
            // 'jabatan' => 'required|check_uuid',
            // 'perihal' => 'required|idn_address',
            'nama'                  => 'required|alpha_spaces',
            'alamat'                => 'required|idn_address',
            'telepon'               => 'required|idn_phone_number',
            'email'                 => 'nullable|email',
            'plat_kendaraan'        => 'nullable|idn_vehicle_license_plate|max:15',
            //'photo-ktp'             => 'required',
            // 'image'                 => [new Base64ImageTamu()],
            // 'image_plat_kendaraan'  => [new Base64ImageTamu()],
            'tujuan'                => 'required',
            'waktu_kedatangan'      => 'required',
            'tanggal_kedatangan'    => 'required',
            'tempat_lahir'          => 'required',
            'tanggal_lahir'         => 'required',
            'jenis_kelamin'         => 'required'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nik_ktp.required_if' => 'No KTP wajib diisi',
            'nik_ktp.numeric' => 'No KTP hanya dapat diisi dengan angka',
            'nik_ktp.digits' => 'No KTP terdiri dari 16 angka',
            'nik_sim.required_if' => 'No SIM wajib diisi',
            'nik_sim.numeric' => 'No SIM hanya dapat diisi dengan angka',
            'nik_sim.digits' => 'No SIM harus terdiri dari 14 angka',
            'nik_passport.required_if' => 'No Passport wajib diisi',
            'nik_passport.alpha_num' => 'No Passport hanya dapat diisi dengan alfabet dan angka',
            'nik_passport.min' => 'No Passport minimal terdiri dari 6 karakter',
            'nik_type.required' => 'Tipe no identitas wajib dipilih',
            'nik_type.numeric' => 'Format tipe no identitas salah',
            'bidang.required' => 'Bidang tujuan wajib dipilih',
            'bidang.uuid' => 'Format bidang tujuan salah',
            'jabatan.required' => 'Jabatan tujuan wajib dipilih',
            'jabatan.check_uuid' => 'Format jabatan tujuan salah',
            'perihal.required' => 'Keterangan datang wajib diisi',
            'perihal.idn_address' => 'Keterangan datang hanya dapat diisi dengan alfabet, angka, titik (.), koma (,) dan spasi',
            'nama.required' => 'Nama wajib diisi sesuai KTP',
            'nama.alpha_spaces' => 'Nama hanya dapat diisi dengan alfabet dan spasi',
            'alamat.required' => 'Masukan alamat sesuai KTP Anda',
            'alamat.idn_address' => 'Alamat hanya dapat diisi dengan alfabet, angka, dash (-), underscores (_), titik (.), koma (,) dan spasi',
            'telepon.required' => 'No HP / whatsappwajib diisi',
            'telepon.idn_phone_number' => 'Format no HP / whatsapp +628131234567 atau 08131234567',
            'email.email' => 'Format email salah',
            'plat_kendaraan.idn_vehicle_license_plate' => 'Plat kendaraan hanya dapat diisi dengan alfabet dalam huruf besar dan angka',
            'plat_kendaraan.max' => 'Plat kendaraan maksimal 11 karakter',
            //'photo-ktp.required'             => 'Foto KTP wajib diisi',
            // 'image.required'                 => 'Foto diri wajib diisi',
            // 'image_plat_kendaraan.required'  => 'Foto plat kendaraan wajib diisi',
            'tujuan.required' => 'Tujuan wajib diisi',
            'waktu_kedatangan.required' => 'Waktu kedatangan wajib diisi',
            'tanggal_kedatangan.required' => 'Tanggal kedatangan wajib diisi',
            'tujuan.required'                => 'Tujuan wajib diisi',
            'waktu_kedatangan.required'      => 'Waktu kedatangan wajib diisi',
            'tanggal_kedatangan.required'    => 'Tanggal kedatangan wajib diisi',
            'tempat_lahir.required'          => 'Tempat lahir wajib diisi',
            'tanggal_lahir.required'         => 'Tanggal lahir wajib diisi',
            'jenis_kelamin.required'         => 'Jenis kelamin wajib diisi'
        ];
    }
}
