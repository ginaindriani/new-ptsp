<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CheckIdentitasTamuRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik_ktp' => 'nullable|required_if:nik_type,1|numeric|digits:16',
            'nik_sim' => 'nullable|required_if:nik_type,2|numeric|digits:14',
            'nik_passport' => 'nullable|required_if:nik_type,3|alpha_num|min:6',
            'nik_type' => 'required|numeric',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nik_ktp.required_if' => 'No KTP wajib diisi',
            'nik_ktp.numeric' => 'No KTP hanya dapat diisi dengan angka',
            'nik_ktp.digits' => 'No KTP terdiri dari 16 angka',
            'nik_sim.required_if' => 'No SIM wajib diisi',
            'nik_sim.numeric' => 'No SIM hanya dapat diisi dengan angka',
            'nik_sim.digits' => 'No SIM harus terdiri dari 14 angka',
            'nik_passport.required_if' => 'No Passport wajib diisi',
            'nik_passport.alpha_num' => 'No Passport hanya dapat diisi dengan alfabet dan angka',
            'nik_passport.min' => 'No Passport minimal terdiri dari 6 karakter',
            'nik_type.required' => 'Tipe no identitas wajib dipilih',
            'nik_type.numeric' => 'Format tipe no identitas salah',
        ];
    }
}
