<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->hasAnyRole(['superadmin', 'admin']) || auth()->user()->organisasi->jabatan->is_admin) {
            return $next($request);
        }

        abort(403, 'Access denied');
    }
}
