<?php

namespace App\Http\Controllers;

use App\Models\MasterLayanan;
use App\Models\PermohonanLayanan;
use App\Models\Satker;
use Illuminate\Http\Request;
use PDF;
use QrCode;
use Storage;

set_time_limit(0);
class TestingController extends Controller
{
    public function index() {
        $permohonan = PermohonanLayanan::find('9ed30741-4288-45d1-90bf-2f81cacadea8');
        $masterLayanan = MasterLayanan::find($permohonan->jenis_layanan);
        $kontens = $permohonan->konten;
        foreach ($kontens as $key => $value) {
            if ($key == 'satker') {
                $satker = Satker::find($value);
                $kontens[$key] = $satker->nama_satker;
            } else if ($key == 'idLayanan') {
                $layanan = MasterLayanan::find($value);
                $kontens['Layanan'] = $layanan->nama_layanan;
            } else if ($key == 'src') {
                $url = asset('storage'. $this->path . $value);
                $kontens['src'] = "<a target='_blank' href='$url'>$value</a>";
            }
        }

        $qrcode = QrCode::format('png')->size(200)->generate($permohonan->id_permohonan_layanan);
        // return view('cetak.pdf.example', compact('permohonan', 'masterLayanan', 'kontens', 'qrcode'));
        $pdf = PDF::loadview('cetak.pdf.example', compact('permohonan', 'masterLayanan', 'kontens', 'qrcode'));
        $fileName =  str_replace(' ', '_', $masterLayanan->nama_layanan) . '_' .  $permohonan->nomor_permohonan . '.pdf';
        // $folderName = 'public/pdf/' . $fileName;
        // Storage::put('public/pdf/' . $fileName, $pdf->output());
        // dd(Storage::exists($folderName));
        // dd();
	    return $pdf->stream($fileName);
    }
}
