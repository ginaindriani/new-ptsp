<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MasterLayanan;
use App\Models\PermohonanLayanan;
use App\Models\Staticdatas;
use App\Models\Suku;
use DB;
use Illuminate\Http\Request;

class LayananController extends Controller
{
    public function detail($idPermohonanLayanan) {
        $url = route("permohonan.downloadfile", ['id' => $idPermohonanLayanan]);
        $permohonanLayanan = PermohonanLayanan::select([
            'permohonan_layanan.id_permohonan_layanan',
            'master_layanans.nama_layanan',
            DB::raw("'$url' as pdf_url"),
            'satker.nama_satker',
            'permohonan_layanan.nama',
            'permohonan_layanan.nomor_permohonan',
            'permohonan_layanan.nomor_surat',
            'permohonan_layanan.nomor_antrian',
            'permohonan_layanan.status',
            'permohonan_layanan.tanggal',
            'permohonan_layanan.file',
            'permohonan_layanan.jenis_layanan',
            'permohonan_layanan.konten',
            DB::raw('(
                CASE
                    WHEN permohonan_layanan.status is null THEN "Belum Ditanggapi"
                    WHEN permohonan_layanan.status = 1 THEN "Dalam Diproses"
                    ELSE "Dalam Diproses"
                END
            ) as status')
        ])
            ->where('id_permohonan_layanan', $idPermohonanLayanan)
            ->join('master_layanans', 'master_layanans.id_layanan', '=', 'permohonan_layanan.jenis_layanan')
            ->join('satker', 'permohonan_layanan.id_satker', '=', 'satker.id_satker')
            ->first();

        return response()->json([
            'error' => false,
            'datas' => $permohonanLayanan
        ], 200);
    }

    public function listByLayanan($idLayanan, $nik, Request $request) {
        $url = url('layanan/permohonan/');

        $limit = isset($request->limit) ? $request->limit : 10;
		$page = isset($request->current_page) ? $request->current_page : 1;
        $pagination = [
			"total_found" => 0,
			"limit" => (int) $limit,
			"current_page" => (int) $page,
			"total_page" => 0
		];

        $permohonanLayanan = PermohonanLayanan::select([
            'permohonan_layanan.id_permohonan_layanan',
            'master_layanans.nama_layanan',
            DB::raw("CONCAT('$url/', permohonan_layanan.id_permohonan_layanan, '/downloadfile') as pdf_url"),
            'satker.nama_satker',
            'permohonan_layanan.nama',
            'permohonan_layanan.nomor_permohonan',
            'permohonan_layanan.nomor_surat',
            'permohonan_layanan.nomor_antrian',
            'permohonan_layanan.status',
            'permohonan_layanan.tanggal',
            'permohonan_layanan.file',
            'permohonan_layanan.jenis_layanan',
            'permohonan_layanan.konten',
            DB::raw('(
                CASE
                    WHEN permohonan_layanan.status is null THEN "Belum Ditanggapi"
                    WHEN permohonan_layanan.status = 1 THEN "Dalam Diproses"
                    ELSE "Dalam Diproses"
                END
            ) as status')
        ])
            ->join('master_layanans', 'master_layanans.id_layanan', '=', 'permohonan_layanan.jenis_layanan')
            ->join('satker', 'permohonan_layanan.id_satker', '=', 'satker.id_satker')
            ->where('permohonan_layanan.jenis_layanan', $idLayanan)
            ->whereRaw(DB::raw('permohonan_layanan.konten LIKE \'%nik":"' . $nik .'"%\' '))
            ->orderBy('permohonan_layanan.created_at', 'desc');

        $countData = clone $permohonanLayanan;
        $countData = $countData->count();

        if ($page > 1) {
            $offset = ($page - 1) * $limit;
            $permohonanLayanan = $permohonanLayanan->offset($offset)->limit($limit);
        } else {
            $permohonanLayanan = $permohonanLayanan->offset(0)->limit($limit);
        }
        $permohonanLayanan = $permohonanLayanan->get();

        $pages = ($countData > 0) ? ceil($countData / $limit) : 0;
        $pagination["total_found"] = (int) $countData;
        $pagination["total_page"] = (int) $pages;

        return response()->json([
            'error' => false,
            'pagination' => $pagination,
            'datas' => $permohonanLayanan
        ], 200);
    }

    public function masterLayanan() {
        $url = asset('landingpage/images/listmenu');
        $masterlayanan = MasterLayanan::select([
            'id_layanan',
            'nama_layanan',
            'tipe_layanan',
            'aktif',
            'order_by',
            DB::raw("CONCAT('$url', '/', image) as image")
        ])
        ->orderBy('order_by')
        ->get();

        return response()->json([
            'error' => false,
            'datas' => $masterlayanan
        ], 200);
    }

    public function identitas(Request $request, $kode_satker)
    {
        $identity_type = Staticdatas::identity_type();

        $datas['identity_type'] = $identity_type;
        $datas['id_layanan'] = $kode_satker;
        $datas['jenis_kelamin_data'] = [
            (object) [
                "key"   => "pria",
                "text"  => "Pria"
            ],
            (object)[
                "key"   => "wanita",
                "text"  => "Wanita"
            ]
        ];
        $datas['status_perkawinan_data'] = [
            (object) [
                "key"   => "belum kawin",
                "text"  => "BELUM KAWIN"
            ],
            (object)[
                "key"   => "kawin",
                "text"  => "KAWIN"
            ],
            (object)[
                "key"   => "cerai hidup",
                "text"  => "CERAI HIDUP"
            ],
            (object)[
                "key"   => "cerai mati",
                "text"  => "CERAI MATI"
            ]
        ];
        $datas['agama_data'] = [
            (object) [
                "key"   => "islam",
                "text"  => "Islam"
            ],
            (object) [
                "key"   => "kristen protestan",
                "text"  => "Kristen Protestan"
            ],
            (object) [
                "key"   => "katolik",
                "text"  => "Katolik"
            ],
            (object) [
                "key"   => "hindu",
                "text"  => "Hindu"
            ],
            (object) [
                "key"   => "buddha",
                "text"  => "Buddha"
            ],
            (object) [
                "key"   => "konghucu",
                "text"  => "Konghucu"
            ],
        ];
        $datas['kewarganegaraan_data'] = [
            (object) [
                "key"   => "wni",
                "text"  => "WNI"
            ],
            (object) [
                "key"   => "wna",
                "text"  => "WNA"
            ]
        ];
        $datas['pendidikan_data'] = [
            (object) [
                "key"   => "sd",
                "text"  => "SD"
            ],
            (object) [
                "key"   => "smp",
                "text"  => "SMP"
            ],
            (object) [
                "key"   => "sma",
                "text"  => "SMA"
            ],
            (object) [
                "key"   => "d1",
                "text"  => "D1"
            ],
            (object) [
                "key"   => "d3",
                "text"  => "D3"
            ],
            (object) [
                "key"   => "s1",
                "text"  => "S1"
            ],
            (object) [
                "key"   => "s2",
                "text"  => "S2"
            ],
            (object) [
                "key"   => "s3",
                "text"  => "S3"
            ]
        ];
        $datas['suku_data'] = Suku::select("id AS key", "name AS text")->get();

        if ($request->status_tamu == 'umum') {
            $page = 'pages.layanan.identitas_umum';
            $datas['status_tamu'] = 'umum';
        } elseif ($request->status_tamu == 'saksi_terdakwa') {
            $page = 'pages.layanan.identitas_saksi_terdakwa';
            $datas['status_tamu'] = 'saksi_terdakwa';
        } elseif ($request->status_tamu == 'pers') {
            $page = 'pages.layanan.identitas_pers';
            $datas['status_tamu'] = 'pers';
        }

        return view($page, $datas);
    }


}
