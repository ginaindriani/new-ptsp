<?php

namespace App\Http\Controllers;

use App\DataTables\LayananDataTable;
use App\Http\Requests\BukutamuRequest;
use App\Http\Requests\BukutamuSaksiRequest;
use App\Http\Requests\BukutamuPersRequest;
use App\Models\MasterLayanan;
use App\Models\RuangDiskusi;
use App\Models\Staticdatas;
use App\Models\Suku;
use App\Models\Tamu;
use Illuminate\Http\Request;
use Str;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\TamuTambahan;
use Illuminate\Support\Facades\Crypt;

class LayananController extends Controller
{
    //
    public function index(Request $request)
    {
        if($request->uuid != null){
            try {
                $uuid = Crypt::decrypt($request->uuid);
                $checkUuid = Tamu::where('uuid',$uuid)->first();
                if($checkUuid == null){
                    return redirect('/pendaftaran');    
                }
            } catch (\Throwable $th) {
                return redirect('/pendaftaran');
            }
        }else{
            $uuid = null;
        }

        if($uuid){
            $data_tamu = Tamu::where('uuid',$uuid)->first();
            if(!$data_tamu){
                return redirect()->route('index');
            }
        }else{
            return redirect()->route('index');
        }

        $images = [];
        $masterLayanan = MasterLayanan::where('tipe_layanan', 'layanan')->where('aktif',1)->where('is_lapdu',null)->get();
        foreach ($masterLayanan as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.create', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            // if ($value->id_layanan == 11)
            //     $is_blank = false;
            // else
                $is_blank = true;

            $images[] = [
                'image' => asset('landingpage/images/listmenu') . '/' . str_replace(public_path('landingpage/images/listmenu/'), '', $value->image),
                'menu' => $value->tipe_layanan,
                'is_blank' => $is_blank,
                'url' => $url,
                'id_layanan' => $value->id_layanan,
                'nama_layanan' => ucwords($value->nama_layanan)
            ];
        }

        $images = json_encode($images);
        $ruangDiskusi = RuangDiskusi::where('is_answer', 1)->orderBy('created_at','desc')->limit('5')->get();
        return view('pages.layanan.index', compact('images', 'ruangDiskusi'));
    }
    public function informasi()
    {
        $images = [];
        $masterLayanan = MasterLayanan::where('tipe_layanan', '<>', 'layanan')->get();
        foreach ($masterLayanan as $key => $value) {
            if ($value->id_layanan == 19 || $value->id_layanan == 17|| $value->id_layanan == 20|| $value->id_layanan == 21 || $value->id_layanan == 13 || $value->id_layanan == 14)
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.create', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            $is_blank = true;

            $images[] = [
                'image' => asset('landingpage/images/listmenu') . '/' . str_replace(public_path('landingpage/images/listmenu/'), '', $value->image),
                'menu' => $value->tipe_layanan,
                'is_blank' => $is_blank,
                'url' => $url,
                'nama_layanan' => ucwords($value->nama_layanan)
            ];
        }

        $images = json_encode($images);
        $ruangDiskusi = RuangDiskusi::where('is_answer', 1)->orderBy('created_at','desc')->limit('5')->get();
        return view('pages.layanan.informasi', compact('images', 'ruangDiskusi'));
    }

    public function identitas(Request $request, $kode_satker)
    {
        $identity_type = Staticdatas::identity_type();

        $datas['identity_type'] = $identity_type;
        $datas['id_layanan'] = $kode_satker;
        $datas['jenis_kelamin_data'] = [
            (object) [
                "key"   => "pria",
                "text"  => "Pria"
            ],
            (object)[
                "key"   => "wanita",
                "text"  => "Wanita"
            ]
        ];
        $datas['status_perkawinan_data'] = [
            (object) [
                "key"   => "belum kawin",
                "text"  => "BELUM KAWIN"
            ],
            (object)[
                "key"   => "kawin",
                "text"  => "KAWIN"
            ],
            (object)[
                "key"   => "cerai hidup",
                "text"  => "CERAI HIDUP"
            ],
            (object)[
                "key"   => "cerai mati",
                "text"  => "CERAI MATI"
            ]
        ];
        $datas['agama_data'] = [
            (object) [
                "key"   => "islam",
                "text"  => "Islam"
            ],
            (object) [
                "key"   => "kristen protestan",
                "text"  => "Kristen Protestan"
            ],
            (object) [
                "key"   => "katolik",
                "text"  => "Katolik"
            ],
            (object) [
                "key"   => "hindu",
                "text"  => "Hindu"
            ],
            (object) [
                "key"   => "buddha",
                "text"  => "Buddha"
            ],
            (object) [
                "key"   => "konghucu",
                "text"  => "Konghucu"
            ],
        ];
        $datas['kewarganegaraan_data'] = [
            (object) [
                "key"   => "wni",
                "text"  => "WNI"
            ],
            (object) [
                "key"   => "wna",
                "text"  => "WNA"
            ]
        ];
        $datas['pendidikan_data'] = [
            (object) [
                "key"   => "sd",
                "text"  => "SD"
            ],
            (object) [
                "key"   => "smp",
                "text"  => "SMP"
            ],
            (object) [
                "key"   => "sma",
                "text"  => "SMA"
            ],
            (object) [
                "key"   => "d1",
                "text"  => "D1"
            ],
            (object) [
                "key"   => "d3",
                "text"  => "D3"
            ],
            (object) [
                "key"   => "s1",
                "text"  => "S1"
            ],
            (object) [
                "key"   => "s2",
                "text"  => "S2"
            ],
            (object) [
                "key"   => "s3",
                "text"  => "S3"
            ]
        ];
        $datas['suku_data'] = Suku::select("id AS key", "name AS text")->get();

        if ($request->status_tamu == 'umum') {
            $page = 'pages.layanan.identitas_umum';
            $datas['status_tamu'] = 'umum';
        } elseif ($request->status_tamu == 'saksi_terdakwa') {
            $page = 'pages.layanan.identitas_saksi_terdakwa';
            $datas['status_tamu'] = 'saksi_terdakwa';
        } elseif ($request->status_tamu == 'pers') {
            $page = 'pages.layanan.identitas_pers';
            $datas['status_tamu'] = 'pers';
        }

        return view($page, $datas);
    }

    public function getSatker(Request $request)
    {
        $user       = auth()->user();
        $limit      = $request->filled("limit") ? $request->input('limit') : 10;
        $limit      = $limit <= 100 ? $limit : 100;
        $query      = DB::table("satker")->selectRaw("kode_satker AS id, nama_satker AS text, tipe_satker")->where('parent_id', 28);
        if ($request->filled("keyword")) {
            $query->where("nama_satker", "like", "%" . $request->keyword . "%");
        }
        $query->orderBy("kode_satker", "asc");
        $dataQuery          = $query->paginate($limit);
        $dataResults        = $dataQuery->items();
        if ($dataQuery->currentPage() == 1 && !request()->filled("keyword")) {
            array_unshift($dataResults, (object) [
                "id"            => "28",
                "text"          => "KEJAKSAAN TINGGI SUMATERA UTARA"
            ]);
        }
        $results            = array(
            "results"       => $dataResults,
            "pagination"    => array(
                "more"      => $dataQuery->hasMorePages()
            )
        );
        return $results;
    }

    public function submit_identitas(Request $request)
    {
        // dd($request->all());
        $BukutamuRequest        = new BukutamuRequest();
        $BukutamuSaksiRequest   = new BukutamuSaksiRequest();
        $BukutamuPersRequest    = new BukutamuPersRequest();

        if($request->status_tamu == 'umum') {
            $validationRules = $BukutamuRequest->rules();
            $validationMessages = $BukutamuRequest->messages();
        } elseif($request->status_tamu == 'saksi_terdakwa') {
            $validationRules = $BukutamuSaksiRequest->rules();
            $validationMessages = $BukutamuSaksiRequest->messages();
        } elseif($request->status_tamu == 'pers') {
            $validationRules = $BukutamuPersRequest->rules();
            $validationMessages = $BukutamuPersRequest->messages();
        }

        // dd($request->id_layanan);

        $validation = Validator::make($request->all(), $validationRules, $validationMessages);
        if ($validation->fails()) {
            $errors = $validation->errors()->all();
            $old_request = $request->input();
            $request->session()->put('old-request-bukutamu', $old_request);
            $request->session()->put('errors-bukutamu', $errors);
            $request->session()->put(
                'alert-error-bukutamu',
                'Anda memiliki kesalahan input atau data belum lengkap. Silahkan ulangi lagi.'
            );
            // id layanan ini sebenarnya kode satker, salah nama variable
            return redirect(route('identitas', ['kode_satker' => $request->id_layanan, 'status_tamu' => $request->status_tamu]))->withInput($request->input());
        }
        $datas = $request->input();
        $requestBody = [
            'satker' => $datas['id_layanan'], // ini kode satker, bukan id layanan, salah nama variable
            'nik_type' => $datas['nik_type'],
            'nama' => $datas['nama'],
            'email' => $datas['email'],
            'alamat' => $datas['alamat'],
            'telepon' => $datas['telepon'],
            'plat_kendaraan' => $datas['plat_kendaraan'],
            // 'perihal' => $datas['perihal'],
            'jumlah_tamu' => 1,
            'photo' => $datas['image'],
            'photo_kendaraan' => $datas['image_plat_kendaraan'],
            'tujuan' => $datas['tujuan'],
            'waktu_kedatangan' => $datas['waktu_kedatangan'],
            'layanan_diperoleh' => $datas['id_layanan']
        ];
        $dateTimeKedatangan = Carbon::createFromFormat('m/d/Y H:i', $datas['tanggal_kedatangan'] . ' ' . $datas['waktu_kedatangan']);

        if ($datas['nik_type'] == 1) {
            $requestBody['nik'] = $datas['nik_ktp'];
        } else if ($datas['nik_type'] == 2) {
            $requestBody['nik'] = $datas['nik_sim'];
        } else if ($datas['nik_type'] == 3) {
            $requestBody['nik'] = $datas['nik_passport'];
        }
        DB::beginTransaction();
        $data_tamu = new Tamu();
        $data_tamu->uuid = (string) Str::uuid();
        $data_tamu->slug = $data_tamu->uuid;
        $data_tamu->nik_type = $datas['nik_type'];
        $data_tamu->nik = $requestBody['nik'];
        $data_tamu->name = $datas['nama'];
        $data_tamu->kode_satker = $requestBody['satker'];
        // $data_tamu->slug = $slug;
        $data_tamu->address = $datas['alamat'];
        if (!empty($datas['email'])) {
            $data_tamu->email = $datas['email'];
        }
        if (!empty($datas['plat_kendaraan'])) {
            $data_tamu->plat_kendaraan = strtoupper($datas['plat_kendaraan']);
        }
        $data_tamu->phone = $datas['telepon'];
        if (empty($check_tamu_pegawai)) {
            $data_tamu->type = 1;
        }
        $data_tamu->tujuan = $datas['tujuan'];
        // $data_tamu->layanan_diperoleh = $datas['id_layanan'];
        $data_tamu->waktu_kedatangan    = $dateTimeKedatangan;
        $data_tamu->status              = 1;
        $data_tamu->tempat_lahir        = $request->tempat_lahir;
        $data_tamu->tanggal_lahir       = $request->tanggal_lahir;
        $data_tamu->jenis_kelamin       = $request->jenis_kelamin;
        $data_tamu->suku                = $request->suku;
        $data_tamu->kewarganegaraan     = $request->kewarganegaraan;
        $data_tamu->agama               = $request->agama;
        $data_tamu->pendidikan          = $request->pendidikan;
        $data_tamu->pekerjaan           = $request->pekerjaan;
        $data_tamu->alamat_kantor       = $request->alamat_kantor;
        $data_tamu->status_perkawinan   = $request->status_perkawinan;
        $data_tamu->kepartaian          = $request->kepartaian;
        $data_tamu->ormas_lainnya       = $request->ormas_lainnya;
        $data_tamu->nama_pasangan       = $request->nama_pasangan;
        $data_tamu->nama_anak_anak      = $request->nama_anak_anak;
        $data_tamu->nama_ayah_kandung   = $request->nama_ayah_kandung;
        $data_tamu->alamat_ayah_kandung = $request->alamat_ayah_kandung;
        $data_tamu->nama_ibu_kandung    = $request->nama_ibu_kandung;
        $data_tamu->alamat_ibu_kandung  = $request->alamat_ibu_kandung;
        $data_tamu->nama_ayah_mertua    = $request->nama_ayah_mertua;
        $data_tamu->alamat_ayah_mertua  = $request->alamat_ayah_mertua;
        $data_tamu->nama_ibu_mertua     = $request->nama_ibu_mertua;
        $data_tamu->alamat_ibu_mertua   = $request->alamat_ibu_mertua;
        $data_tamu->nama_kenalan1       = $request->nama_kenalan1;
        $data_tamu->alamat_kenalan1     = $request->alamat_kenalan1;
        $data_tamu->nama_kenalan2       = $request->nama_kenalan2;
        $data_tamu->alamat_kenalan2     = $request->alamat_kenalan2;
        $data_tamu->nama_kenalan3       = $request->nama_kenalan3;
        $data_tamu->alamat_kenalan3     = $request->alamat_kenalan3;
        $data_tamu->hobi                = $request->hobi;
        $data_tamu->kedudukan_di_masyarakat = $request->kedudukan_di_masyarakat;
        $data_tamu->media_menaungi      = $request->media_menaungi;
        $data_tamu->website_media       = $request->website_media;
        $data_tamu->nomor_kta           = $request->nomor_kta;
        $data_tamu->nama_atasan_langsung = $request->nama_atasan_langsung;
        $data_tamu->no_telp_kantor      = $request->no_telp_kantor;
        $data_tamu->jabatan             = $request->jabatan;

        // simpan gambar berdasarkan file upload / img base64
        if($request['photo-ktp-64'] || $request['photo-ktp']) {
            if ($request['photo-ktp']) {
                $ktpPath        = $this->saveImage($request['photo-ktp'], $data_tamu->uuid, 'ktp');
            } else {
                $ktpPath        = $this->saveImage64($request['photo-ktp-64'], $data_tamu->uuid, 'ktp');
            }
            $data_tamu->foto_ktp = $ktpPath;
        }
        if($request->image || $request['photo-diri']) {
            if ($request['photo-diri']) {
                $diriPath       = $this->saveImage($request['photo-diri'], $data_tamu->uuid, 'diri');
            } else {
                $diriPath       = $this->saveImage64($request['image'], $data_tamu->uuid, 'diri');
            }
            $data_tamu->foto_diri = $diriPath;
        }
        if($request->image_plat_kendaraan || $request['photo-kendaraan']) {
            if ($request['photo-kendaraan']) {
                $kendaraanPath  = $this->saveImage($request['photo-kendaraan'], $data_tamu->uuid, 'kendaraan');
            } else {
                $kendaraanPath  = $this->saveImage64($request['image_plat_kendaraan'], $data_tamu->uuid, 'kendaraan');
            }
            $data_tamu->foto_kendaraan = $kendaraanPath;
        }
        $data_tamu->save();
        $request->session()->flush();

        // simpan tamu tambahan jika ada
        if($request->tamu_tambahan) {
            $tamuTambahan = $request->tamu_tambahan;
            $fieldTamuTambahan = count($request->tamu_tambahan); // jumlah tamu = jumlah field/3
            for ($i = 0; $i < $fieldTamuTambahan; $i+=3) {
                $tamuBaru = new TamuTambahan();
                $tamuBaru->id_tamu_utama    = $data_tamu->id;
                $tamuBaru->nama             = $tamuTambahan[$i]['nama'];
                $tamuBaru->no_ktp           = $tamuTambahan[$i+1]['ktp'];
                $tamuBaru->no_hp            = $tamuTambahan[$i+2]['telp'];
                $tamuBaru->save();
            }
        }
        DB::commit();
        return redirect(route('layanan', ['uuid' => $data_tamu->uuid]));
    }

    public function data_layanan(LayananDataTable $dataTable, Request $request)
    {
        return $dataTable->render('pages.layanan.data');
    }

    private function saveImage($fotoInput, $uuid, $type)
    {
        $pathDocument       = 'sumut/tamu/foto/';
        $foto              = $fotoInput;
        $fotoExtension     = $foto->getClientOriginalExtension();
        $now                = Carbon::now()->format('dmYHis');
        $fileName           = $type . '_' . $uuid . '_' . $now . '.' . $fotoExtension;
        Storage::disk('local')->put($pathDocument . $fileName, file_get_contents($foto));
        // Storage::disk('sftp')->put($pathDocument . $fileName, file_get_contents($foto));

        return $fileName;
    }

    private function saveImage64($fotoInput, $uuid, $type)
    {
        $pathDocument       = 'sumut/tamu/foto/';
        $foto              = $fotoInput;
        $now                = Carbon::now()->format('dmYHis');
        $fileName           = $type . '_' . $uuid . '_' . $now . '.png';

        $image = str_replace('data:image/png;base64,', '', $foto);
        $image = str_replace(' ', '+', $image);
        // Storage::disk('sftp')->put($pathDocument . $fileName, base64_decode($image));
        Storage::disk('local')->put($pathDocument . $fileName, base64_decode($image));

        return $fileName;
    }
}

