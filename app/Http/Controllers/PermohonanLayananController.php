<?php

namespace App\Http\Controllers;

use App\DataTables\InfoDataTable;
use App\DataTables\LayananDaftarDataTable;
use App\DataTables\LayananDataTable;
use App\DataTables\PengaduanDaftarDataTable;
use App\DataTables\SearchNikDataTable;
use App\Models\MasterLayanan;
use App\Models\Satker;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Forms\PermohonanLayananForm;
use App\Forms\PermohonanLayananEditForm;
use App\Helpers\FileHelper;
use App\Http\Requests\BukutamuRequest;
use App\Models\Berita;
use App\Models\Dpo;
use App\Models\JadwalSidang;
use App\Models\Lelang;
use App\Models\Pengumuman;
use App\Models\PerkaraTrend;
use App\Models\PermohonanLayanan;
use App\Models\Tamu;
use App\Utilities\Utilities;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Calculation\TextData\Search;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use PDF;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use stdClass;
use Str;

class PermohonanLayananController extends Controller
{
    use FormBuilderTrait;
    const KOH_TOKEN = '$2y$10$YVwGDLhBiQSrW8gbaOUmWOmULk8uUBjbm.cc8bX5dtxkt.kJIum9e';
    private $urlTrend;

    public function __construct()
    {
//        if (env('APP_ENV') == 'local')
//            $this->urlTrend = 'http://127.0.0.1:8000/api';
//        else
            $this->urlTrend = 'http://103.154.126.7/kejaksaan-trend/public/api';

        $this->path = '/permohonan/';
        $this->pathStorageFile = storage_path('app/public/upload/permohonan');
        $this->pathStorage = 'public/upload/permohonan/';
    }

    public function infoPegawai(Request $request) {
        if (!$request->ajax())
            abort(404);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-dev.kejaksaan.go.id/token/api/auth/login?email=mataelang@kejaksaan.go.id&password=m4t4el4n9*!!",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                ),
            ));
    
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            if($responCode == 200){
                $data = json_decode($response);    
                $token = $data->access_token;
                $headers = [
                    'Authorization: Bearer ' .$token,
                ];

                if ($request->search_by == "nip") {
                    // $url = 'https://mysimkari.kejaksaan.go.id/api/pegawai?nip=';
                    $url = 'https://api-dev.kejaksaan.go.id/mysimkariv2/api/v2/web/pegawai?nip=';
                    $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->search));
                } else if ($request->search_by == "nama") {
                    // $url = 'https://mysimkari.kejaksaan.go.id/api/pegawai/name?name=';
                    $url = 'https://api-dev.kejaksaan.go.id/mysimkariv2/api/v2/web/pegawai/name?name=';
                    $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->search));
                } else
                    abort(404);
        
                $page = $request->page;
                $limit = $request->limit;
                $result = json_decode($result);
                if ($result) {
                    if ($request->search_by == 'nip' && $result && $result->status == "200") {
                        $result = $result->data;
                        $table = View::make('cetak.ajax.info_pegawai', compact('result', 'request'))->render();
                    } else if ($request->search_by == 'nama' && count($result->data) > 0) {
                        $result = Utilities::getResultInfoKepegawaian($request->search, $result->data);
                        if (count($result) == 0) {
                            return response()->json([
                                'error' => true,
                                'data' => null,
                                'message' => 'Data Tidak Ditemukan'
                            ], 200);
                        }
        
                        $totalPage = (int) ceil(count($result) / $limit);
                        $dataList = [];
                        $countPage = $page > 1 ? (($page * $limit) - $limit) + 1 : $page;
                        $limitData = $page > 1 ? ($page * $limit) : $limit;
                        for ($i=$countPage; $i <= $limitData; $i++) {
                            if (isset($result[$i - 1])) {
                                $dataList[] = $result[$i - 1];
                            }
                        }
                        $result = $dataList;
                        $table = View::make('cetak.ajax.info_pegawai', compact('result', 'request', 'totalPage', 'page'))->render();
                    } else {
                        return response()->json([
                            'error' => true,
                            'data' => null,
                            'message' => 'Data Tidak Ditemukan'
                        ], 200);
                    }
                    
                    return response()->json([
                        'error' => false,
                        'data' => $table,
                        'message' => 'Proses berhasil'
                    ], 200);
                } else {
                    return response()->json([
                        'error' => true,
                        'data' => null,
                        'message' => 'Data Tidak Ditemukan'
                    ], 200);
                }

                return response()->json(json_decode($result), 200);
        

            } else {
                return response()->json([
                    'error' => true,
                    'data' => null,
                    'message' => 'Data Tidak Ditemukan'
                ], 200);
            }


        


        
        // $result = Utilities::dummyInfoPegawai();
       

        
    }

    public function detailMap($idSatker, Request $request) {
        
        if (!$request->ajax())
            abort(404);

        $yearRangeName = date('Y', strtotime('-2 years')) . " - " . date('Y');
        $datas = json_decode(Utilities::httpGetJson([], $this->urlTrend . '/data-get-detail/' . $idSatker . '?dates=' . str_replace(' ', '', $yearRangeName)));
        $jumlahTahanan = '-';
        if (isset($datas->data->satker->kode_satker)) {
            $url = 'http://103.84.193.100/fire/public/api/washan?id_kejati=' . substr($datas->data->satker->kode_satker, 0, 2) . '&id_kejari=' . substr($datas->data->satker->kode_satker, 2, 2) . '&id_cabjari=' . substr($datas->data->satker->kode_satker, 4, 2);
            $headers = [
                'Authorization: Bearer ' . self::KOH_TOKEN,
            ];
            $resultTahanan = json_decode(Utilities::httpPostJson($headers, $url));
            $jumlahTahanan = $resultTahanan && $resultTahanan->status == 200 ? $resultTahanan->total : '-';
        }

        return view('pages.permohonan.detail-map', compact('datas', 'jumlahTahanan'));
    }

    public function infoPakem(InfoDataTable $dataTable, Request $request) {
        $layananPelaporanPakem = 23;
        $masterLayanan = MasterLayanan::where('id_layanan', 21)->first();
        return $dataTable->with('id_layanan', $layananPelaporanPakem)->render($masterLayanan->blade, compact('masterLayanan'));
    }

    public function infoOrmas(InfoDataTable $dataTable, Request $request) {
        $layananPelaporanPakem = 25;
        $masterLayanan = MasterLayanan::where('id_layanan', 20)->first();

        return $dataTable->with('id_layanan', $layananPelaporanPakem)->render($masterLayanan->blade, compact('masterLayanan'));
    }

    public function index(LayananDataTable $dataTable, Request $request)
    {
        $idLayanan = $request->idLayanan;
        if (
            $idLayanan == 19
            || $idLayanan == 17
            || $idLayanan == 20
            || $idLayanan == 21
        ) {
            abort(404);
        }

        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();
        if (!$masterLayanan || $masterLayanan->type == 'hyperlink')
            return abort(404);

        return $dataTable->with('idLayanan', $idLayanan)->render('pages.permohonan.index', compact('masterLayanan'));
    }

    public function hariIni(LayananDaftarDataTable $dataTable, Request $request)
    {
        $tipe = '1';
        return $dataTable->with('tipe',$tipe)->render('pages.permohonan.pelayanan');
    }

    public function belumDiverifikasi(LayananDaftarDataTable $dataTable, Request $request)
    {
        $tipe = '2';
        return $dataTable->with('tipe',$tipe)->render('pages.permohonan.pelayanan');
    }

    public function daftarPelayanan(LayananDaftarDataTable $dataTable, Request $request)
    {
        $tipe = '3';
        return $dataTable->with('tipe',$tipe)->render('pages.permohonan.pelayanan');
    }

    public function pengaduanHariIni(PengaduanDaftarDataTable $dataTable, Request $request)
    {
        $tipe = '1';
        return $dataTable->with('tipe',$tipe)->render('pages.permohonan.pengaduan');
    }

    public function pengaduanBelumDiverifikasi(PengaduanDaftarDataTable $dataTable, Request $request)
    {
        $tipe = '2';
        return $dataTable->with('tipe',$tipe)->render('pages.permohonan.pengaduan');
    }

    public function pengaduanDaftarPelayanan(PengaduanDaftarDataTable $dataTable, Request $request)
    {
        $tipe = '3';
        return $dataTable->with('tipe',$tipe)->render('pages.permohonan.pengaduan');
    }

    

    public function indexPublic(Request $request) {
        $idLayanan = $request->idLayanan;

        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();
        if (!$masterLayanan)
            return abort(404);

        if ($masterLayanan->id_layanan == 19) {
//            dd($this->urlTrend);
            $yearRangeName = date('Y', strtotime('-2 years')) . " - " . date('Y');
            $datas = json_decode(Utilities::httpGetJson([], $this->urlTrend . '/data-satker-long-lat'));
            $provs = json_decode(Utilities::httpGetJson([], $this->urlTrend . '/data-geo-json-provs'));
            $datas = json_encode($datas->data);
            
            $provs = json_encode($provs->data);
            $urlTrend = $this->urlTrend;
            return view($masterLayanan->blade, compact('masterLayanan', 'yearRangeName', 'datas', 'provs', 'urlTrend'));
        } else if ($masterLayanan->id_layanan == 17) {
            return view($masterLayanan->blade, compact('masterLayanan'));
        } else if ($masterLayanan->id_layanan == 20) {
            return redirect()->route('permohonan.info-ormas');
        } else if ($masterLayanan->id_layanan == 21) {
            return redirect()->route('permohonan.info-pakem');
        } else if ($masterLayanan->id_layanan == 13) {
            return redirect()->route('permohonan.perkaraPidum');
        } else if ($masterLayanan->id_layanan == 14) {
            return redirect()->route('permohonan.perkaraPidsus');
        } else if ($masterLayanan->id_layanan == 42) {
            return redirect()->route('permohonan.lelang');
        } else if ($masterLayanan->id_layanan == 43) {
            return redirect()->route('permohonan.dpo');
        } else if ($masterLayanan->id_layanan == 44) {
            return redirect()->route('permohonan.jadwalsidang');
        }
        else {
            abort(404);
        }
    }

    public function create(Request $request)
    {
        
        if($request->uuid != null){
            try {
                $uuid = Crypt::decrypt($request->uuid);
                $checkUuid = Tamu::where('uuid',$uuid)->first();
                if($checkUuid == null){
                    return redirect('/pendaftaran');    
                }
            } catch (\Throwable $th) {
                return redirect('/pendaftaran');
            }
        }else{
            $uuid = null;
        }
        if($request->idLayanan == "32" || $request->idLayanan == "18" || $request->idLayanan == "26" ||  $request->idLayanan == "27" ||  $request->idLayanan == "33" || $request->idLayanan == "24" ||  $request->idLayanan == "42" ||  $request->idLayanan == "43"  ||  $request->idLayanan == "46"){
            //bypass
        }
        elseif(auth()->user() != null){
            //bypass pengcekan uuid
        }
        elseif($uuid == null){
            return redirect('/pendaftaran');
        }
        $idLayanan = $request->get('idLayanan');
        
        if($uuid != null){
            $dataTamu = Tamu::where('uuid',$uuid)->select('kode_satker','name','nik','address','phone')->first();
        }else{
            $dataTamu = null;
        }

        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();

        if (!$masterLayanan) {
            return redirect()->back()->with('error', 'Surat tidak tersedia');
        }

        if($request->idLayanan == '23' || $request->idLayanan == '25'){
            //cek jika dia pakem dan ormas satker muncul semua
            $satker = Satker::orderBy('id_satker')->get();
        }elseif(!$dataTamu){
            $satker = Satker::orderBy('id_satker')->get();
        }else{
            $satker = Satker::orderBy('id_satker')->where('kode_satker',$dataTamu->kode_satker)->get();
        }
        $datasatker = Satker::orderBy('id_satker')->get();
        if($request->idLayanan == '32' && $uuid == null){
            $beritadaerah = Berita::join('satker','satker.id_satker','=','berita.id_satker')->select('berita.id','berita.judul_berita','berita.id_satker','berita.foto','berita.tanggal','satker.nama_satker')->where('is_active',1)->orderBy('tanggal','desc')->get();
            $beritasatker = "";
            $pengumuman = Pengumuman::join('satker','satker.id_satker','=','pengumuman.id_satker')->select('pengumuman.*','satker.nama_satker')->get();
        }elseif($request->idLayanan == '32' && $uuid != null){
            $beritadaerah = Berita::join('satker','satker.id_satker','=','berita.id_satker')->select('berita.id','berita.judul_berita','berita.id_satker','berita.foto','berita.tanggal','satker.nama_satker')->where('is_active',1)->orderBy('tanggal','desc')->get();
            $beritasatker = Berita::join('satker','satker.id_satker','=','berita.id_satker')->select('berita.id','berita.judul_berita','berita.id_satker','berita.foto','berita.tanggal','satker.nama_satker')->where('berita.id_satker',$satker[0]->id_satker)->where('is_active',1)->orderBy('tanggal','desc')->get();
            $pengumuman = Pengumuman::join('satker','satker.id_satker','=','pengumuman.id_satker')->select('pengumuman.*','satker.nama_satker')->get();
        }else{
            $beritadaerah = "";
            $beritasatker = "";
            $pengumuman = Pengumuman::join('satker','satker.id_satker','=','pengumuman.id_satker')->select('pengumuman.*','satker.nama_satker')->get();
        }

        $jenisPengaduan = optionJenisPengaduan();
        $asalPengaduan = optionAsal();
        $optionPps = optionPps();
        $jenisLayanan = optionJenisLayanan($idLayanan);
        $jenisBidang = optionJenisBidang($idLayanan);

        // dd($jenisBidang);
        $parameterSurat = collect($masterLayanan->parameter)->sortBy(function($value, $key) {
            return $value['order'];
        });
        // dd($parameterSurat);

        $form = $this->form(
            PermohonanLayananForm::class,
            [
                'method'    => 'POST',
                'url'       => route('permohonan.store'),
                'autocomplete'    => 'off',
                'files' => true
            ],
            [
                'idLayanan' => $idLayanan,
                'satker' => $satker,
                'dataTamu' => $dataTamu,
                'parameterSurat' => $parameterSurat,
                'jenisPengaduan' => $jenisPengaduan,
                'asalPengaduan' => $asalPengaduan,
                'optionPps' => $optionPps,
                'jenisLayanan' => $jenisLayanan,
                'jenisBidang' => $jenisBidang
            ]

        );


        $wysiwyg = $parameterSurat->filter(function($value, $key) {
            return $value['type'] == 'wysiwyg';
        })->keys()->toArray();


        if (in_array($idLayanan,[27])) {
            //trend perkara
            $satker = Satker::all();
            $tahun = PerkaraTrend::select(DB::raw('Year(tgl_spdp) as tahun'))->groupBy(DB::raw('YEAR(tgl_spdp)'))->get();
            return view($masterLayanan->blade, compact('masterLayanan', 'form','wysiwyg','satker','tahun'));
        }elseif(in_array($idLayanan,[26])){
            //trendpengaduan
            $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            $data_pengaduan_perbulan = 
                DB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });

            $data_pengaduan = 
                DB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select([
                    'master_layanans.nama_layanan'
                    , DB::raw('count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->get();
        
                $pengaduanmcount = [];
                $pengaduanArr = [];
            
                foreach ($data_pengaduan_perbulan as $key => $value) {
                    $pengaduanmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($pengaduanmcount[$i])) {
                        $pengaduanArr[$i]['count'] = $pengaduanmcount[$i];
                    } else {
                        $pengaduanArr[$i]['count'] = 0;
                    }
                    $pengaduanArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_pengaduan_perbulan =  response()->json(array_values($pengaduanArr));
            return view($masterLayanan->blade, compact('masterLayanan', 'data_pengaduan','form', 'wysiwyg','satker','hitung_data_pengaduan_perbulan'));
        }
        else{
            return view($masterLayanan->blade, compact('masterLayanan', 'form', 'wysiwyg','datasatker','satker','beritasatker','beritadaerah','dataTamu','pengumuman'));
        }
    }

    public function berita(Request $request, $id)
    {
        
        $idLayanan = $request->get('idLayanan');
        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();

        $urlberita = 'https://ptsp.kejaksaan.go.id/api/detailnews/'.$id;
                $get = new Client([
                    'verify' => false
            ]);
        $getdata = $get->get($urlberita);
        $beritadetail = json_decode($getdata->getBody()->getContents(), true);
        $berita = $beritadetail['data'];

        return view('cetak.layanan.kejaksaan_todaydetail', compact('berita','masterLayanan'));
        
    }
           
    public function daftarLayanan(Request $request)
    {
        $satker = Satker::select('nama_satker','id_satker')->get();
        return view('cetak.layanan.daftar_layanan',compact('satker'));
    }

    public function simpantamu(Request $request)
    {
        if($request->photo == null){
            return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, pastikan anda sudah melakukan foto melalui webcam']);
        }

        

        $satker = Satker::where('id_satker',$request->satker)->select('kode_satker')->first();

        $unique_id = 'V'. date('ymd') . date('His'). rand(1, 2000);
        $unique_key= (string) Str::uuid();
        $fileName = $unique_id.'-'.time(); //generating unique file name
        preg_match('/data:image\/(.*?);/', $request->photo, $tamu_photo_extension); // extract the image extension
        $request->photo = preg_replace('/data:image\/(.*?);base64,/', '', $request->photo); // remove the type part
        $request->photo = str_replace(' ', '+', $request->photo);
        $photoName = $fileName.'.'.$tamu_photo_extension[1]; //generating unique file name
        $check_tamu = Tamu::where('nik', $request->no_identitas)->count();
        if($check_tamu >= 1){
            Tamu::where('nik', $request->no_identitas)
            ->update([
                'kode_satker' => $satker->kode_satker,
                'nik_type' => "1",
                'nik' => $request->no_identitas,
                'name' => $request->nama,
                'type' => $request->type,
                'email' => $request->email,
                'address' => $request->alamat,
                'phone' => $request->no_hp,
                'status' => "1",
                'tujuan' => $request->tujuan,
                'foto_diri' => $photoName,
                'jenis_kelamin' => $request->jenis_kelamin,
                'waktu_kedatangan' => now(),
            ]);
            $data_pegawai_ybs = Tamu::where('nik', $request->no_identitas)->select('unique_key')->first();

            $unique_keys = $data_pegawai_ybs->unique_key;
            $url = '?uuid='.Crypt::encrypt($unique_keys);
        }else{
            $data_tamu = new Tamu();
            $data_tamu->uuid = $unique_key;
            $data_tamu->unique_key = $data_tamu->uuid;
            $data_tamu->kode_satker = $satker->kode_satker;
            $data_tamu->nik_type = "1";
            $data_tamu->nik = $request->no_identitas;
            $data_tamu->name = $request->nama;
            $data_tamu->slug = $data_tamu->unique_key;
            $data_tamu->type = $request->type;
            $data_tamu->email = $request->email;
            $data_tamu->address = $request->alamat;
            $data_tamu->phone = $request->no_hp;
            $data_tamu->status = "1";
            $data_tamu->tujuan = $request->tujuan;
            $data_tamu->foto_diri = $photoName;
            $data_tamu->jenis_kelamin = $request->jenis_kelamin;
            $data_tamu->waktu_kedatangan = now();
            $data_tamu->save();

            $url = '?uuid='.Crypt::encrypt($unique_key);
        }
        
        $tokenPtsp = '6400d8d96832a21178b875de6743eac7';
        $dataPtsp = [
            'no_identitas'     => $request->no_identitas,
            'nama'     => $request->nama,
            'kode_satker'     => $satker->kode_satker,
            'jenis_kelamin'     => $request->jenis_kelamin,
            'email'     => $request->email,
            'alamat'     => $request->alamat,
            'no_hp'     => $request->no_hp,
            'photo'     => $request->photo,
            'photo_name'     => $photoName,
            'unique_key'    => $unique_key,
        ];

        $apiPtsp = 'https://bukutamu.kejaksaan.go.id/api/post-tamu-ptsp/';
        
        $ClientPtsp = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
            'verify' => false
        ]);
        
        try{
            $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                'form_params' => $dataPtsp
            ]);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            //lewati
        }
        


        return redirect(url($url));
    }

    public function tilang(Request $request, $id)
    {
        
        $idLayanan = $request->get('idLayanan');
        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();

        $getTilang = 'https://tilang.kejaksaan.go.id/info/check_berkas/';
            $ClientTilang = new Client([
                'verify' => false
            ]);
       
        try{
        $getTilangSipede = $ClientTilang->post($getTilang, [
            'form_params' => [
                'no_tilang' => $id,
                ]
            ]
        );
        }catch(\Exception $ex){
            return redirect()->back()->withInput()->with(['error' => 'Data tidak ditemukan harap Masukkan ulang data yang dicari']);
        }
        

        $resultGetTilangSipede = json_decode($getTilangSipede->getBody()->getContents(), true);
        foreach($resultGetTilangSipede as $object)
        {
            $alamat = $object['alamat'];
            $barang_bukti = $object['bb'];
            $biaya_perkara = $object['bp'];
            $denda = $object['denda'];
            $nama = $object['nama'];
            $no_briva = $object['no_briva'];
            $no_ranmor = $object['no_ranmor'];
            $no_reg_tilang = $object['no_reg_tilang'];
            $pasal = $object['pasal'];
            $subsider = $object['subsider'];
            $tgl_bayar = $object['tgl_bayar'];
            $tgl_sidang = $object['tgl_sidang'];
            $uang_titipan = $object['uang_titipan'];
            $tgl_ambil = $object['tgl_ambil'];
            $nama_petugas = $object['nama_petugas'];
            $jenis_kendaraan = $object['jenis_kendaraan'];
            $kode_satker_pn = $object['kode_satker_pn'];
            $nama_hakim = $object['nama_hakim'];
            $nama_panitera = $object['nama_panitera'];
        }
        return view('cetak.layanan.e_tilangdetail', compact('masterLayanan', 'alamat','barang_bukti' ,'biaya_perkara',
        'denda','nama','no_briva','no_ranmor','no_reg_tilang','pasal','subsider','tgl_bayar','tgl_sidang','uang_titipan','tgl_ambil',
        'nama_petugas','jenis_kendaraan','kode_satker_pn','nama_hakim','nama_panitera'));
        
    }

    public function tahanan(Request $request, $id)
    {
        
        $idLayanan = $request->get('idLayanan');
        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();

        $getTahanan = '156.67.219.105/tahanan/public/api/data-tahanan?nik='.$id;
            $ClientTahanan = new Client([
                'verify' => false
            ]);
        
        try{
            $getTahananData = $ClientTahanan->post($getTahanan);
        }catch(\Exception $ex){
            return redirect()->back()->withInput()->with(['error' => 'Data tidak ditemukan harap Masukkan ulang data yang dicari']);
        }
        

        $resultGetTahanan = json_decode($getTahananData->getBody()->getContents(), true);
        

        $dataTahanan = $resultGetTahanan['data'];
        $nama = $dataTahanan['nama'];
        $nik = $dataTahanan['nik'];
        $alamat = $dataTahanan['alamat'];
        $tempat_lahir = $dataTahanan['tempat_lahir'];
        $tanggal_lahir = $dataTahanan['tanggal_lahir'];
        $jk = $dataTahanan['jk'];
        $no_hp = $dataTahanan['no_hp'];
        $jenis_tahanan = $dataTahanan['jenis_tahanan'];
        $ditahan_sejak = $dataTahanan['ditahan_sejak'];
        $jaksa = $dataTahanan['nama_jaksa'];
        $polres = $dataTahanan['nama_polsek'];

        return view('cetak.layanan.info_tahanandetail', compact('masterLayanan', 'nama','nik','alamat','tempat_lahir',
        'tanggal_lahir','jk','no_hp','jenis_tahanan','ditahan_sejak','jaksa','polres'));
        
    }

    public function infotahanan(Request $request, $id)
    {
        
        $idLayanan = $request->get('idLayanan');
        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();

        $getTahanan = 'https://cms-publik.kejaksaan.go.id/api/ptsp/nik?nik='.$id;
            $ClientTahanan = new Client([
                'verify' => false
            ]);
        
        try{
            $getTahananData = $ClientTahanan->post($getTahanan);
        }catch(\Exception $ex){
            return redirect()->back()->withInput()->with(['error' => 'Data tidak ditemukan harap Masukkan ulang data yang dicari']);
        }
        

        $resultGetTahanan = json_decode($getTahananData->getBody()->getContents(), true);
        

        $dataTahanan = $resultGetTahanan['data'];
        if($dataTahanan == null){
            return redirect()->back()->withInput()->with(['error' => 'Data tidak ditemukan harap Masukkan ulang data yang dicari']);
        }
        $nama = $dataTahanan[0]['nama'];
        $nik = $dataTahanan[0]['nik'];
        $alamat = $dataTahanan[0]['alamat'];
        $tempat_lahir = $dataTahanan[0]['tempat_lahir'];
        $tanggal_lahir = $dataTahanan[0]['tanggal_lahir'];
        $jk = $dataTahanan[0]['jk'];
        $no_hp = $dataTahanan[0]['no_hp'];
        $ditahan_sejak = $dataTahanan[0]['ditahan_sejak'];
        $jaksa = $dataTahanan[0]['nama_jaksa'];
        $polres = $dataTahanan[0]['polres'];
        if($dataTahanan[0]['id_kejati'] == '00')
        {
            $satker = Satker::where('kode_satker','00')->first();
            $nama_satker = $satker->nama_satker;
        }elseif($dataTahanan[0]['id_cabjari'] != '00'){
            $kode_satker = $dataTahanan[0]['id_kejati'].'.'.$dataTahanan[0]['id_kejari'].'.'.$dataTahanan[0]['id_cabjari'];
            $satker = Satker::where('kode_satker',$kode_satker)->first();
            $nama_satker = $satker->nama_satker;
        }elseif($dataTahanan[0]['id_kejari'] != '00'){
            $kode_satker = $dataTahanan[0]['id_kejati'].'.'.$dataTahanan[0]['id_kejari'];
            $satker = Satker::where('kode_satker',$kode_satker)->first();
            $nama_satker = $satker->nama_satker;
        }elseif($dataTahanan[0]['id_kejati'] != '00'){
            $kode_satker = $dataTahanan[0]['id_kejati'];
            $satker = Satker::where('kode_satker',$kode_satker)->first();
            $nama_satker = $satker->nama_satker;
        }

        return view('cetak.layanan.info_tahanandetail2', compact('masterLayanan', 'nama','nik','alamat','tempat_lahir',
        'tanggal_lahir','jk','no_hp','ditahan_sejak','jaksa','polres','nama_satker'));
        
    }

    public function perkarapidum(Request $request)
    {

        $satker = $request->satker;
        $jenis_pidana = $request->jenis_pidana;
        $tahun = $request->tahun;
        $datasatker = Satker::select('kode_satker_cms','nama_satker')->get();
        if($request->satker == NULL){
            $satker = '000000';
        }

        if($request->jenis_pidana == NULL){
            $jenis_pidana = '1';
        }

        if($request->tahun == NULL){
            $tahun = date("Y");
        }

        $values = array(
            'satker' => $satker,
            'jenis_pidana' => $jenis_pidana,
            'tahun' => $tahun
        );
        $params = http_build_query($values);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://cms-publik.kejaksaan.go.id/api/p16_satjenis_trend");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         

        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($httpcode != '200'){
            $info = "PENGAMBILAN DATA PERKARA PIDUM GAGAL, SILAHKAN COBA LAGI";
            return view('cetak.layanan.information', compact('info'));
        }

        curl_close ($ch);
        $data = json_decode($output);
        
        return view('cetak.layanan.perkara_pidum', compact('data','datasatker', 'satker', 'jenis_pidana','tahun'));
    }

    public function perkarapidsus(Request $request)
    {

        $satker = $request->satker;
        $jenis = $request->jenis;
        $tahun = $request->tahun;
        $datasatker = Satker::select('kode_satker_cms','nama_satker')->get();
        if($request->satker == NULL){
            $satker = '000000';
        }

        if($request->jenis == NULL){
            $jenis = '1';
        }

        if($request->tahun == NULL){
            $tahun = date("Y");
        }

        $values = array(
            'satker' => $satker,
            'jenis' => $jenis,
            'tahun' => $tahun
        );
        $params = http_build_query($values);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://cms-publik.kejaksaan.go.id/api/p16_pidsus_satjenis_trend");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         

        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        
        if($httpcode != '200'){
            $info = "PENGAMBILAN DATA PERKARA PIDSUS GAGAL, SILAHKAN COBA LAGI";
            return view('cetak.layanan.information', compact('info'));
        }

        curl_close ($ch);
        $data = json_decode($output);

        return view('cetak.layanan.perkara_pidsus', compact('data','datasatker', 'satker', 'jenis','tahun'));
    }

    public function chartPerkara(Request $request){

        // dd($request);
        $satker = $request->satker;
        $year = $request->tahun;

        $satker = $satker == 0 ? null : str_replace(".","",$satker);

        $kategori = PerkaraTrend::select('jenis_pidana')->groupBy('jenis_pidana')->whereYear('tgl_spdp',$year)->get();

        $data = DB::table('new_data_perkaras')
        ->select(DB::raw('COUNT(jenis_pidana) as jumlah, jenis_pidana , MONTH(tgl_spdp)'))
        ->where(function($query) use ($satker){
            if ($satker != null) {
                $query->where('kode_satker','like',$satker."%");
            }
        })
        ->whereYear('tgl_spdp',$year)
        ->groupBy(DB::raw('new_data_perkaras.jenis_pidana,month(tgl_spdp)'))
        ->orderBy(DB::raw('month(tgl_spdp)'))
        ->get();

        $series = array();
        foreach ($kategori as $listKategori) {
            $dataValue = array();
            $dataSeries = new stdClass;
            $dataSeries->name = $listKategori->jenis_pidana;
            foreach ($data as $listData) {
                if($listData->jenis_pidana == $listKategori->jenis_pidana){
                    array_push($dataValue,$listData->jumlah);
                }
            }
            $dataSeries->data = $dataValue;
            array_push($series,$dataSeries);
        }


        return response()->json($series);
    }


    public function store(Request $request)
    {
        $request->validate([
            'g_token' => 'required',
        ]);

        $secretKey = env('RECAPTCHA_SECRET_KEY');
        $token = $request->g_token;
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $token . "&remoteip=" . $ip;
        $verify = file_get_contents($url);
        $response = json_decode($verify);

        if (!$response)
            return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda']);
        if ($response && !$response->success)
            return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda']);

        $idLayanan = $request->idLayanan;
        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();

        if (!$masterLayanan) {
            return redirect()->back()->with('error', 'Surat tidak tersedia');
        }

        $parameterSurat = collect($masterLayanan->parameter)
            ->sortBy(function($value, $key) {
                return $value['order'];
            });

        
        $validationRule = $parameterSurat->filter(function($item, $key) use ($parameterSurat) {
            return $item['required'];
        })->mapWithKeys(function($item, $key) {
            $rule = $item['required'] ? 'required' : '';
            if ($item['type'] == 'file') {
                $rule .= "|mimes:{$item['mimes']}|max:{$item['max']}";
            }
            return [$key => $rule];
        })->all();

        $request->validate($validationRule);

        if($request->id_layanan == '40'){
            //divalidasi 40 karena dia file uploadnya ada 3

            $file_ktp = Validator::make($request->all(), [
                'file_ktp' => ['required','mimes:pdf,docx,doc','max:2048']
            ]);
            if ($file_ktp->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda, pastikan File anda Berektensi PDF, DOCX, DOC dengan File Size dibawah 2 MB']);
            }

            $validator = Validator::make($request->all(), [
                'src' => ['required','mimes:pdf,docx,doc','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda, pastikan File anda Berektensi PDF, DOCX, DOC dengan File Size dibawah 2 MB']);
            }

            $file_lainnya = Validator::make($request->all(), [
                'file_lainnya' => ['required','mimes:pdf,docx,doc','max:2048']
            ]);
            if ($file_lainnya->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda, pastikan File anda Berektensi PDF, DOCX, DOC dengan File Size dibawah 2 MB']);
            }

        }elseif($request->src){
            $validator = Validator::make($request->all(), [
                'src' => ['required','mimes:pdf,docx,doc','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda, pastikan File anda Berektensi PDF, DOCX, DOC dengan File Size dibawah 2 MB']);
            }
        }else{
            //bypass
        }

        $satker = Satker::where('id_satker',$request->satker)->first();
        DB::beginTransaction();
        try {
            $requestData = $request->except('_token', 'submit', 'g_token', '_method');
            foreach ($parameterSurat as $key => $value) {
                if ($value['type'] == 'file') {
                    $file = $request->file($key);
					if($request->file($key) != "" || !empty($request->file($key)!="")){
                        $rand = substr(md5(microtime()),rand(0,26),9);
                        $getFileExt = $file->getClientOriginalName();
                        $fileExt = $array = explode('.', $getFileExt);
                        $extension = end($fileExt);
                        $fileName = $idLayanan . "_" . $key . "_" . $rand. "." . $extension; 
                        $destinationPath = public_path().'/storage/permohonan' ;
                        $file->move($destinationPath,$fileName);
                        $requestData[$key] = $fileName; 
					}else{
						$requestData[$key] = null;
					}
                }
            }

            $konten = jsEscape($requestData);

            $kode_satker = $satker->kode_satker;
            $counter = getCounter($idLayanan);
            $insert = new PermohonanLayanan();
            $insert->nomor_permohonan = getSuratPermohonan($counter, $idLayanan, $kode_satker);
            $insert->jenis_layanan = $idLayanan;
            $insert->tanggal = isset($requestData['tanggal']) ? $requestData['tanggal'] : date('Y-m-d');
            $insert->nama = isset($requestData['nama']) ? $requestData['nama'] : null;
            $insert->konten = $konten;
            $insert->file = isset($requestData['src']) ? $requestData['src'] : null;
            $insert->id_satker = isset($requestData['satker']) ? $requestData['satker'] : 1;
            $insert->save();
            $insert->nomor_surat = $insert->id_permohonan_layanan;
            $insert->nomor_antrian = $insert->id_permohonan_layanan;
            $insert->save();

            DB::commit();

            if($satker->id_satker == '28' || $satker->parent_id == '28' || in_array($satker->parent_id, [33, 33, 39, 42, 48, 51, 53, 53, 58])){
                $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
                $dataPtsp = [
                    'nomor_permohonan' => $insert->nomor_permohonan,
                    'jenis_layanan' => $idLayanan,
                    'tanggal' => isset($requestData['tanggal']) ? $requestData['tanggal'] : date('Y-m-d'),
                    'nama' => isset($requestData['nama']) ? $requestData['nama'] : null,
                    'konten' => $konten,
                    'file' => isset($requestData['src']) ? $requestData['src'] : null,
                    'id_satker' => isset($requestData['satker']) ? $requestData['satker'] : 1,
                    'nomor_surat' => $insert->id_permohonan_layanan,
                    'nomor_antrian' => $insert->id_permohonan_layanan
                ];
    
                $apiPtsp = 'https://ptsp-sumut.kejaksaanri.id/api/simpan-layanan';
                $ClientPtsp = new Client([
                    'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                    'verify' => false
                ]);
                // try {
                //     $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                //         'form_params' => $dataPtsp
                //     ]);
                // }catch (\GuzzleHttp\Exception\ConnectException $e) {
                //     // do nothing, the timeout exception is intended
                // }  
            }
            


            $tracingId = Str::uuid();
            if($masterLayanan->is_lapdu == '1'){
                $eventCode = 'PTSP_ADUAN';
            }else{
                $eventCode = 'PTSP_LAYANAN';
            }
            $id_satker = $satker->id_satker;
            $nama_satker = $satker->nama_satker;
            $parent_satker = $satker->parent_id;
            
            $is_processed = false;

            $summaryData = Array();
            $summaryData["tracingId"] = $tracingId;
            $summaryData["eventCode"] = $eventCode;
            $summaryData["summaryData"]["id_satker"] = $id_satker;
            $summaryData["summaryData"]["nama_satker"] = $nama_satker;
            $summaryData["summaryData"]["parent_satker"] = $parent_satker;
            $summaryData["summaryData"]["kode_satker"] = $kode_satker;
            $summaryData["summaryData"]["is_processed"] = $is_processed;
            
            $url = '43.231.129.14:8081/api/v1/realtime-event/create';
            $Client = new Client([
                'headers' => ['Content-Type' => 'application/json'],
                'body'    => json_encode($summaryData),
                'verify' => false
            ]);

            try {
                $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                DB::table('realtime_api')->insert(
                    array(
                        "tracingId" => $tracingId,
                        "eventCode" => $eventCode,
                        "summaryData" => json_encode($summaryData),
                        "created_at" => Carbon::now(),
                        "updated_at" => Carbon::now(),
                    )
                );
            } catch (\GuzzleHttp\Exception\ConnectException $e) {
                    // do nothing, the timeout exception is intended
            }  

            $eventCode = 'PTSP_PERMOHONAN_LAYANAN';
            $tracingId = Str::uuid();
            $summaryDatas["tracingId"] = $tracingId;
            $summaryDatas["eventCode"] = $eventCode;
            $summaryDatas["summaryData"]["id_satker"] = $id_satker;
            $summaryDatas["summaryData"]["nama_satker"] = $nama_satker;
            $summaryDatas["summaryData"]["parent_satker"] = $parent_satker;
            $summaryDatas["summaryData"]["kode_satker"] = $kode_satker;
            $summaryDatas["summaryData"]["is_processed"] = $is_processed;
            $summaryDatas["summaryData"]["periode"] = Carbon::now()->isoFormat('YYYY-MM-DD');;
            $summaryDatas["summaryData"]["id_jenis_layanan"] = $idLayanan;
            $summaryDatas["summaryData"]["nama_jenis_layanan"] = $masterLayanan->nama_layanan;

            try {
                $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                DB::table('realtime_api')->insert(
                    array(
                        "tracingId" => $tracingId,
                        "eventCode" => $eventCode,
                        "summaryData" => json_encode($summaryDatas),
                        "created_at" => Carbon::now(),
                        "updated_at" => Carbon::now(),
                    )
                );
            } catch (\GuzzleHttp\Exception\ConnectException $e) {
                    // do nothing, the timeout exception is intended
            }  

            if (auth()->user()) {
                return redirect()->route('permohonan.index', ['idLayanan' => $idLayanan])->with('success', 'Data Berhasil Ditambah');
            } else {
                return redirect()->route('permohonan.show', ['id' => $insert->id_permohonan_layanan])->with('success', 'Data Berhasil Ditambah');
            }
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return redirect()->back()->with('error', $ex->getMessage() . $ex->getLine());
        }
    }

    public function show($id)
    {
        $permohonan = PermohonanLayanan::find($id);
        $masterLayanan = MasterLayanan::find($permohonan->jenis_layanan);
        $konten = $permohonan->konten;

        $nomorSurat = $permohonan->nomor_permohonan;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-dev.kejaksaan.go.id/token/api/auth/login?email=sipede@kejaksaan.go.id&password=5p3d32022!!?",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if($responCode == 200){
            $data = json_decode($response);    
            $token = $data->access_token;
            $authorization = "Authorization: Bearer ".$token;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-dev.kejaksaan.go.id/sipede-prod/api/ruang-berbagi/check-status?nomor=".$nomorSurat,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                    $authorization
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if($responCode == 200){
                $data = json_decode($response);   
                $log = $data->data->suratMasukLog;
                $logStatus = 200;
            }else{
                $log = null;
                $logStatus = null;
            }
        }else{
            $log = null;
            $logStatus = null;
        }

        $kontens = [];
        foreach ($masterLayanan->parameter as $key => $value) {
            if ($key == 'satker') {
                $satker = Satker::find($konten[$key]);
                $kontens[$value['title']] = $satker->nama_satker;
            }  else if ($key == 'src' || $key == 'file_ktp' || $key == 'file_lainnya') {
                preg_match("/^.*\.(jpg|jpeg|png|gif)$/i", $konten[$key], $output_array);
                // $url = storage_path('app/public'. $this->path . $konten[$key]);
                $url = asset('/storage/permohonan/'.$konten[$key]);
                $ext = isset($output_array[1]) ? $output_array[1] : "";
                $kontens[$value['title']] = !$ext ? "<a target='_blank' href='$url'>File</a>" : '<div style="width: 200px; height: 200px;"><img src="'. $url .'" class="" alt="" style="width: auto; height: 100%;"></div>';
            }  else {
                $kontens[$value['title']] = $konten[$key];
            }
        }
        $kontens['Layanan'] = $masterLayanan->nama_layanan;

        $qrcode = QrCode::size(300)->generate($permohonan->id_permohonan_layanan);
        return view($masterLayanan->blade_result, compact('permohonan', 'masterLayanan', 'kontens', 'qrcode','log','logStatus'));
    }

    public function showdetail($id) {


        $permohonan = PermohonanLayanan::find($id);
        $masterLayanan = MasterLayanan::find($permohonan->jenis_layanan);
        $konten = $permohonan->konten;
        $nomorSurat = $permohonan->nomor_permohonan;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-dev.kejaksaan.go.id/token/api/auth/login?email=sipede@kejaksaan.go.id&password=5p3d32022!!?",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if($responCode == 200){
            $data = json_decode($response);    
            $token = $data->access_token;
            $authorization = "Authorization: Bearer ".$token;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-dev.kejaksaan.go.id/sipede-prod/api/ruang-berbagi/check-status?nomor=".$nomorSurat,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                    $authorization
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if($responCode == 200){
                $data = json_decode($response);   
                $log = $data->data->suratMasukLog;
                $logStatus = 200;
            }else{
                $log = null;
                $logStatus = null;
            }
        }else{
            $log = null;
            $logStatus = null;
        }
        
        
        $kontens = [];
        foreach ($masterLayanan->parameter as $key => $value) {
            if ($key == 'satker') {
                $satker = Satker::find($konten[$key]);
                $kontens[$value['title']] = $satker->nama_satker;
            }  else if ($key == 'src' || $key == 'file_ktp' || $key == 'file_lainnya') {
                preg_match("/^.*\.(jpg|jpeg|png|gif)$/i", $konten[$key], $output_array);
                $url = asset('storage'. $this->path . $konten[$key]);
                $ext = isset($output_array[1]) ? $output_array[1] : "";
                $kontens[$value['title']] = !$ext ? "<a target='_blank' href='$url'>File</a>" : '<div style="width: 200px; height: 200px;"><img src="'. $url .'" class="" alt="" style="width: auto; height: 100%;"></div>';
            }  else {
                $kontens[$value['title']] = $konten[$key];
            }
        }
        $kontens['Layanan'] = $masterLayanan->nama_layanan;

        $qrcode = QrCode::size(300)->generate($permohonan->id_permohonan_layanan);
        return view($masterLayanan->blade_result, compact('permohonan', 'masterLayanan', 'kontens', 'qrcode','log','logStatus'));
    }

    public function editLayanan(Request $request, $id)
    {
        $data = PermohonanLayanan::findOrFail($id);
        if (!$data) {
            return redirect()->back()->with('error', 'Surat tidak tersedia');
        }

        $idLayanan = $request->get('id_layanan');
        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();
        if (!$masterLayanan) {
            return redirect()->back()->with('error', 'Surat tidak tersedia');
        }

        $satker = Satker::orderBy('id_satker')->get();
        $jenisPengaduan = optionJenisPengaduan();
        $asalPengaduan = optionAsal();
        $optionPps = optionPps();
        $jenisLayanan = optionJenisLayanan($idLayanan);

        $parameterSurat = collect($masterLayanan->parameter)->sortBy(function($value, $key) {
            return $value['order'];
        });

        $form = $this->form(
            PermohonanLayananEditForm::class,
            [
                'method' => 'put',
                'url' => route('permohonan.updatelayanan', ['id' => $id]) . '?id_layanan=' . $idLayanan,
                'autocomplete' => 'off',
                'files' => true
            ],
            [
                'idLayanan' => $idLayanan,
                'satker' => $satker,
                'parameterSurat' => $parameterSurat,
                'jenisPengaduan' => $jenisPengaduan,
                'asalPengaduan' => $asalPengaduan,
                'optionPps' => $optionPps,
                'jenisLayanan' => $jenisLayanan,
                'data' => $data
            ]
        );

        $wysiwyg = $parameterSurat->filter(function($value, $key) {
            return $value['type'] == 'wysiwyg';
        })->keys()->toArray();

        return view($masterLayanan->blade, compact('masterLayanan', 'form', 'wysiwyg'));
    }

    public function edit($id) {

    }

    public function update(Request $request, $id)
    {
        //
    }

    public function updateLayanan(Request $request, $id)
    {
        $request->validate([
            'g_token' => 'required',
        ]);

        $secretKey = env('RECAPTCHA_SECRET_KEY');
        $token = $request->g_token;
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $token . "&remoteip=" . $ip;
        $verify = file_get_contents($url);
        $response = json_decode($verify);

        if (!$response)
            return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda']);
        if ($response && !$response->success)
            return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda']);

        $idLayanan = $request->idLayanan;
        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();

        if (!$masterLayanan) {
            return redirect()->back()->with('error', 'Surat tidak tersedia');
        }

        $parameterSurat = collect($masterLayanan->parameter)
            ->sortBy(function($value, $key) {
                return $value['order'];
            });

        $validationRule = $parameterSurat->filter(function($item, $key) use ($parameterSurat) {
            return $item['required'];
        })->mapWithKeys(function($item, $key) {
            $rule = $item['required'] ? 'required' : '';
            if ($item['type'] == 'file') {
                $rule .= "|mimes:{$item['mimes']}|max:{$item['max']}";
            }
            return [$key => $rule];
        })->all();

        $request->validate($validationRule);

        DB::beginTransaction();
        try {
            $requestData = $request->except('_token', 'submit', 'g_token', '_method');
            foreach ($parameterSurat as $key => $value) {
                if ($value['type'] == 'file') {
					if($request->file($key) != "" || !empty($request->file($key) != "")){
						$fileData = $request->file($key);
						$prefixname = $idLayanan . "_" . $key;
                        $filename = FileHelper::uploadFile($fileData, $this->path, $prefixname);
						$requestData[$key] = $filename;
					}else{
						$requestData[$key] = null;
					}
                }
            }

            if (isset($requestData['id_layanan']))
                unset($requestData['id_layanan']);

            $update = PermohonanLayanan::find($id);
            if (isset($update->konten['src']) && \Storage::disk('public')->exists($this->path . $update->konten['src']))
                \Storage::disk('public')->delete($this->path . $update->konten['src']);

            $update->jenis_layanan = $idLayanan;
            $update->tanggal = isset($requestData['tanggal']) ? $requestData['tanggal'] : date('Y-m-d');
            $update->nama = isset($requestData['nama']) ? $requestData['nama'] : null;
            $update->konten = $requestData;
            $update->file = isset($requestData['src']) ? $requestData['src'] : null;
            $update->id_satker = isset($requestData['satker']) ? $requestData['satker'] : 1;
            $update->save();

            DB::commit();
            return redirect()->route('permohonan.index', ['idLayanan' => $idLayanan])->with('success', 'Data Berhasil Ditambah');
        } catch(\Exception $ex) {
            DB::rollback();
            return redirect()->back()->with('error', $ex->getMessage() . $ex->getLine());
        }
    }

    public function downloadfile(Request $request, $id) {
        $permohonan = PermohonanLayanan::find($id);
        $masterLayanan = MasterLayanan::find($permohonan->jenis_layanan);
        $kontens = $permohonan->konten;
        foreach ($kontens as $key => $value) {
            if ($key == 'satker') {
                $satker = Satker::find($value);
                $kontens[$key] = $satker->nama_satker;
            } else if ($key == 'idLayanan') {
                $layanan = MasterLayanan::find($value);
                $kontens['Layanan'] = $layanan->nama_layanan;
            } else if ($key == 'src') {
                // $url = asset('storage'. $this->path . $value);
                $url = asset('/storage/permohonan/'.$value);
                $kontens['src'] = "<a target='_blank' href='$url'>$value</a>";
            } else if ($key == 'file_ktp') {
                $url = asset('/storage/permohonan/'.$value);
                $kontens['file_ktp'] = "<a target='_blank' href='$url'>$value</a>";
            }  else if ($key == 'file_lainnya') {
                $url = asset('/storage/permohonan/'.$value);
                $kontens['file_lainnya'] = "<a target='_blank' href='$url'>$value</a>";
            }
        }

        $nomorSurat = $permohonan->nomor_permohonan;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-dev.kejaksaan.go.id/token/api/auth/login?email=sipede@kejaksaan.go.id&password=5p3d32022!!?",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if($responCode == 200){
            $data = json_decode($response);    
            $token = $data->access_token;
            $authorization = "Authorization: Bearer ".$token;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-dev.kejaksaan.go.id/sipede-prod/api/ruang-berbagi/check-status?nomor=".$nomorSurat,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                    $authorization
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if($responCode == 200){
                $data = json_decode($response);   
                $log = $data->data->suratMasukLog;
                $logStatus = 200;
            }else{
                $log = null;
                $logStatus = null;
            }
        }else{
            $log = null;
            $logStatus = null;
        }
        

        $qrcode = QrCode::format('png')->size(200)->generate($permohonan->id_permohonan_layanan);
        if ($request->view)
            return view('cetak.pdf.example', compact('permohonan', 'masterLayanan', 'kontens', 'qrcode','log','logStatus'));
        else {
            $pdf = PDF::loadview('cetak.pdf.example', compact('permohonan', 'masterLayanan', 'kontens', 'qrcode','log','logStatus'));
	        return $pdf->stream($masterLayanan->nama_layanan . '_' .  $permohonan->nomor_permohonan . '.pdf');
        }
    }

    public function destroy($id)
    {
        $permohonan = PermohonanLayanan::find($id);
        if (!$permohonan) {
            return redirect()->back()->with('error', 'Surat tidak tersedia');
        }
        $permohonan->delete();
        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
        // return redirect()->route('permohonan.index', ['idLayanan' => $permohonan->jenis_layanan])->with('success', 'Data Berhasil Dihapus');
    }

    public function dataModal(SearchNikDataTable $dataTable, Request $request) {
        if (!$request->ajax())
            abort(404);

        $idLayanan = 1;
        $masterLayanan = MasterLayanan::where('id_layanan', $idLayanan)->first();
        $nik = $request->nik;
        return $dataTable->with('idLayanan', $idLayanan)->render('pages.permohonan.modal-data', compact('masterLayanan', 'nik'));
    }

    public function dataGeoProvince($provId, Request $request) {
        $datas = json_decode(Utilities::httpGetJson([], $this->urlTrend . '/data-geo-province/' . $provId . '?dates=' . $request->dates));
        return $datas;
    }

    public function proses($id)
    {   
        try {
            $permohonan = PermohonanLayanan::find($id);
            $masterLayanan = MasterLayanan::find($permohonan->jenis_layanan);
            $masterLayanans = $masterLayanan->first();;
            $kontens = $permohonan->konten;
            foreach ($kontens as $key => $value) {
                if ($key == 'satker') {
                    $satker = Satker::find($value);
                    $kontens[$key] = $satker->nama_satker;
                } else if ($key == 'idLayanan') {
                    $layanan = MasterLayanan::find($value);
                    $kontens['Layanan'] = $layanan->nama_layanan;
                } else if ($key == 'src') {
                    $url = asset('storage'. $this->path . $value);
                    $kontens['src'] = "<a target='_blank' href='$url'>$value</a>";
                } else if ($key == 'file_ktp') {
                    $url = asset('storage'. $this->path . $value);
                    $kontens['file_ktp'] = "<a target='_blank' href='$url'>$value</a>";
                }  else if ($key == 'file_lainnya') {
                    $url = asset('storage'. $this->path . $value);
                    $kontens['file_lainnya'] = "<a target='_blank' href='$url'>$value</a>";
                }
            }

            $nomorSurat = $permohonan->nomor_permohonan;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-dev.kejaksaan.go.id/token/api/auth/login?email=sipede@kejaksaan.go.id&password=5p3d32022!!?",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            if($responCode == 200){
                $data = json_decode($response);    
                $token = $data->access_token;
                $authorization = "Authorization: Bearer ".$token;
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api-dev.kejaksaan.go.id/sipede-prod/api/ruang-berbagi/check-status?nomor=".$nomorSurat,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_HTTPHEADER => array(
                        "accept: */*",
                        "accept-language: en-US,en;q=0.8",
                        "content-type: application/json",
                        $authorization
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                if($responCode == 200){
                    $data = json_decode($response);   
                    $log = $data->data->suratMasukLog;
                    $logStatus = 200;
                }else{
                    $log = null;
                    $logStatus = null;
                }
            }else{
                $log = null;
                $logStatus = null;
            }
            

            $qrcode = QrCode::format('png')->size(200)->generate($permohonan->id_permohonan_layanan);
            $pdf = PDF::loadview('cetak.pdf.example', compact('permohonan', 'masterLayanan', 'kontens', 'qrcode','log','logStatus'));
            $nameFile = $this->pathStorage.'/'.$masterLayanan->id_layanan . '_' .  $id . '.pdf';
            Storage::put($this->pathStorage.'/'.$masterLayanan->id_layanan . '_' .  $id . '.pdf', $pdf->output());
            $permohonan->status = 1;
            $permohonan->file = $masterLayanan->id_layanan . '_' .  $id . '.pdf';
            $permohonan->save();

            $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
            $dataPtsp = [
                'nomor_permohonan' => $permohonan->nomor_permohonan,
            ];

            $apiPtsp = 'https://ptsp-sumut.kejaksaanri.id/api/update-layanan';
            $ClientPtsp = new Client([
                'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                'verify' => false
            ]);
            // try {
            //     $postApiPtsp = $ClientPtsp->post($apiPtsp, [
            //         'form_params' => $dataPtsp
            //     ]);
            // }catch (\GuzzleHttp\Exception\ConnectException $e) {
            //     // do nothing, the timeout exception is intended
            // }  

            $satker = Satker::where('id_satker',$permohonan->id_satker)->first();
            if($permohonan->jenis_layanan == '29'){
                
                
                if($satker->wilayah == 'I'){
                    $wilayah = 'WILAYAH I';
                }elseif($satker->wilayah == 'II'){
                    $wilayah = 'WILAYAH II';
                }elseif($satker->wilayah == 'III'){
                    $wilayah = 'WILAYAH III';
                }else{
                    $wilayah = 'WILAYAH II';
                }

                $dataSipermata = [
                    'tanggal' => $permohonan->tanggal,
                    'nomor' => $permohonan->nomor_permohonan,
                    'wilayah' => $wilayah,
                    'asal' => $permohonan->nama,
                    'hal' => strtoupper("Pengaduan Laporan Mafia Tanah dengan terlapor ".$permohonan->konten['nama_terlapor']." pada satuan kerja".$satker->nama_satker),
                    'id_satker' => $permohonan->id_satker,
                    'tgl_agenda' => $permohonan->tanggal,
                    'file'      => $permohonan->file,
                    // 'file' => fopen($this->pathStorageFile.'/'.$permohonan->file, 'r')
                ];

                $file = fopen($this->pathStorageFile.'/'.$permohonan->file, 'r');
                
                $url = 'http://sipermata.kejaksaanri.id/api/post-lapdumas';
                $tokenSipermata = '6LcOWzIUAAAAAPAUXSVJdO94Uzzx2-CBmb-3JVvH';
        
                $responseSipermata = Http::attach(
                    'file', fopen($this->pathStorageFile.'/'.$permohonan->file, 'r') //file_get_contents($this->pathStorageFile.'/'.$permohonan->file), $permohonan->file
                )->withToken($tokenSipermata)->post('http://sipermata.kejaksaanri.id/api/post-lapdumas',
                    $dataSipermata
                );

            }

            $permohonan = PermohonanLayanan::find($id);
            $data = [
                'id_satker' => $satker->id_satker,
                'nomor'     => $permohonan->nomor_permohonan,
                'tanggal'   => $permohonan->tanggal,
                'asal'      => $kontens['nama'],
                'hal'       => $masterLayanan->nama_layanan,
                'file'      => $permohonan->file,
            ];
            if (!$data) {
                return response()->json([
                    'status'    => '423',
                    'ket'       => 'Data tidak tersedia'
                ]);
            }
            $getToken = 'https://api-dev.kejaksaan.go.id/token/api/auth/login?email=sipede@kejaksaan.go.id&password=5p3d32022!!?';
                $ClientToken = new Client([
                    'verify' => false
                ]);
            $getTokenSipede = $ClientToken->post($getToken);
            $resultGetTokenSipede = json_decode($getTokenSipede->getBody()->getContents(), true);
            $token = $resultGetTokenSipede["access_token"];

            $file = fopen($this->pathStorageFile.'/'.$permohonan->file, 'r');
           
			$response = Http::attach(
                'file', fopen($this->pathStorageFile.'/'.$permohonan->file, 'r') //file_get_contents($this->pathStorageFile.'/'.$permohonan->file), $permohonan->file
            )->withToken($token)->post('https://api-dev.kejaksaan.go.id/sipede-prod/api/ruang-berbagi/create',
                $data
            );

            $tracingId = Str::uuid();
            if($masterLayanans->is_lapdu == '1'){
                $eventCode = 'PTSP_ADUAN';
            }else{
                $eventCode = 'PTSP_LAYANAN';
            }
            $id_satker = $satker->id_satker;
            $nama_satker = $satker->nama_satker;
            $parent_satker = $satker->parent_id;
            $kode_satker = $satker->kode_satker;
            $is_processed = true;

            $summaryData = Array();
            $summaryData["tracingId"] = $tracingId;
            $summaryData["eventCode"] = $eventCode;
            $summaryData["summaryData"]["id_satker"] = $id_satker;
            $summaryData["summaryData"]["nama_satker"] = $nama_satker;
            $summaryData["summaryData"]["parent_satker"] = $parent_satker;
            $summaryData["summaryData"]["kode_satker"] = $kode_satker;
            $summaryData["summaryData"]["is_processed"] = $is_processed;
            
            $url = '43.231.129.14:8081/api/v1/realtime-event/create';
            $Client = new Client([
                'headers' => ['Content-Type' => 'application/json'],
                'body'    => json_encode($summaryData),
                'verify' => false
            ]);

            try {
                $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                DB::table('realtime_api')->insert(
                    array(
                        "tracingId" => $tracingId,
                        "eventCode" => $eventCode,
                        "summaryData" => json_encode($summaryData),
                        "created_at" => Carbon::now(),
                        "updated_at" => Carbon::now(),
                    )
                );
            } catch (\GuzzleHttp\Exception\ConnectException $e) {
                    // do nothing, the timeout exception is intended
            }  
            return $response->body();
        }
        catch (\Exception $ex) {
            $data = [
                'status'    => '500',
                'ket'       => $ex->getMessage()
            ];
        }
        return $data;
    }

    public function apiStoreTamu(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        // $BukutamuRequest        = new BukutamuRequest();
        $data_tamu = new Tamu();
        $data_tamu->uuid = $request->uuid;
        $data_tamu->slug = $request->uuid;
        $data_tamu->nik_type = $request->nik_type;
        $data_tamu->nik = $request->nik;
        $data_tamu->name = $request->name;
        $data_tamu->kode_satker = $request->kode_satker;
        $data_tamu->type = $request->type;
        $data_tamu->email = $request->email;
        $data_tamu->address = $request->address;
        $data_tamu->phone = $request->phone;
        $data_tamu->plat_kendaraan = $request->plat_kendaraan;
        $data_tamu->status = $request->status;
        $data_tamu->tujuan = $request->tujuan;
        $data_tamu->foto_kendaraan = $request->foto_kendaraan;
        $data_tamu->foto_diri = $request->foto_diri;
        $data_tamu->jenis_kelamin = $request->jenis_kelamin;
        $data_tamu->unique_key = $request->unique_key;
        $data_tamu->waktu_kedatangan = now();
        $data_tamu->save();


        

        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
            ),
            200
        );

        

    }

    public function apiStoreLayanan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        // return $request;
        $insert = new PermohonanLayanan();
        $insert->nomor_permohonan = $request->nomor_permohonan;
        $insert->jenis_layanan = $request->jenis_layanan;
        $insert->tanggal = $request->tanggal;
        $insert->nama = $request->nama;
        $insert->konten = $request->konten;
        $insert->file = $request->file;
        $insert->id_satker = $request->id_satker;
        $insert->nomor_surat = $request->nomor_surat;
        $insert->nomor_antrian = $request->nomor_antrian;
        $insert->save();

        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
            ),
            200
        );
    }

    public function apiUpdateLayanan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $id = $request->nomor_permohonan;
        $permohonan = PermohonanLayanan::where('nomor_permohonan',$id)->first();
        $permohonan->status = 1;
        $permohonan->save();
        if($permohonan == null){
            return response()->json(
                array(
                    'code' => 200,
                    'response' => 'success',
                    'message' => 'Success',
                ),
                200
            );
        }elseif(!$permohonan){
            return response()->json(
                array(
                    'code' => 200,
                    'response' => 'success',
                    'message' => 'Success',
                ),
                200
            );
        }elseif($permohonan->status == '1'){
            return response()->json(
                array(
                    'code' => 200,
                    'response' => 'success',
                    'message' => 'Success',
                ),
                200
            );
        }
        $masterLayanan = MasterLayanan::find($permohonan->jenis_layanan);
        $masterLayanans = $masterLayanan->first();;
        $kontens = $permohonan->konten;
        $nomorSurat = $permohonan->nomor_permohonan;

        foreach ($kontens as $key => $value) {
            if ($key == 'satker') {
                $satker = Satker::find($value);
                $kontens[$key] = $satker->nama_satker;
            } else if ($key == 'idLayanan') {
                $layanan = MasterLayanan::find($value);
                $kontens['Layanan'] = $layanan->nama_layanan;
            } else if ($key == 'src') {
                $url = asset('storage'. $this->path . $value);
                $kontens['src'] = "<a target='_blank' href='$url'>$value</a>";
            } else if ($key == 'file_ktp') {
                $url = asset('storage'. $this->path . $value);
                $kontens['file_ktp'] = "<a target='_blank' href='$url'>$value</a>";
            }  else if ($key == 'file_lainnya') {
                $url = asset('storage'. $this->path . $value);
                $kontens['file_lainnya'] = "<a target='_blank' href='$url'>$value</a>";
            }
        }

        $nomorSurat = $permohonan->nomor_permohonan;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-dev.kejaksaan.go.id/token/api/auth/login?email=sipede@kejaksaan.go.id&password=5p3d32022!!?",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if($responCode == 200){
            $data = json_decode($response);    
            $token = $data->access_token;
            $authorization = "Authorization: Bearer ".$token;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api-dev.kejaksaan.go.id/sipede-prod/api/ruang-berbagi/check-status?nomor=".$nomorSurat,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                    $authorization
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if($responCode == 200){
                $data = json_decode($response);   
                $log = $data->data->suratMasukLog;
                $logStatus = 200;
            }else{
                $log = null;
                $logStatus = null;
            }
        }else{
            $log = null;
            $logStatus = null;
        }
        
        $qrcode = QrCode::format('png')->size(200)->generate($permohonan->id_permohonan_layanan);
        $pdf = PDF::loadview('cetak.pdf.example', compact('permohonan', 'masterLayanan', 'kontens', 'qrcode','log','logStatus'));
        $nameFile = $this->pathStorage.'/'.$masterLayanan->id_layanan . '_' .  $id . '.pdf';
        Storage::put($this->pathStorage.'/'.$masterLayanan->id_layanan . '_' .  $id . '.pdf', $pdf->output());
        $permohonan->status = 1;
        $permohonan->file = $masterLayanan->id_layanan . '_' .  $id . '.pdf';
        $permohonan->save();

        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
            ),
            200
        );
    }

    public function lelang(Request $request)
    {
        if(!$request){
            $lelang = Lelang::join('satker','lelang.id_satker','=','satker.id_satker')->select('lelang.*','satker.nama_satker')->get();
        }
        elseif($request->satker == NULL && $request->status_laku == NULL){
            $lelang = Lelang::join('satker','lelang.id_satker','=','satker.id_satker')->select('lelang.*','satker.nama_satker')->get();
        }
        elseif($request->satker == NULL && $request->status_laku != NULL){
            $lelang = Lelang::join('satker','lelang.id_satker','=','satker.id_satker')->select('lelang.*','satker.nama_satker')->where('lelang.status_laku', $request->status_laku)->get();
        }
        elseif($request->satker != NULL && $request->status_laku != NULL){
            $lelang = Lelang::join('satker','lelang.id_satker','=','satker.id_satker')->select('lelang.*','satker.nama_satker')->where('lelang.id_satker', $request->satker)->where('lelang.status_laku', $request->status_laku)->get();
        }
        else{
            $lelang = Lelang::join('satker','lelang.id_satker','=','satker.id_satker')->select('lelang.*','satker.nama_satker')->where('lelang.id_satker', $request->satker)->get();
        }
        $datasatker = Satker::get();
        return view("cetak.layanan.informasi_lelang", compact('lelang','datasatker'));
    }

    public function kegiatansearch(Request $request)
    {
        if ($request->satker == null){
            return $this->lelang();
        }else{
            $lelang = Lelang::join('satker','lelang.id_satker','=','satker.id_satker')->select('lelang.*','satker.nama_satker')->get();
            $datasatker = Satker::get();
            return view("cetak.layanan.informasi_lelang", compact('lelang','datasatker'));
        }
        
    }

    public function detail_lelang($slug)
    {
        $kegiatan = "";
        return response($kegiatan);
    }

    public function dpo(Request $request)
    {
        if(!$request){
            $dpo = Dpo::join('satker','dpo.id_satker','=','satker.id_satker')->select('dpo.*','satker.nama_satker')->get();
        }
        elseif($request->satker == NULL && $request->status_ditemukan == NULL){
            $dpo = Dpo::join('satker','dpo.id_satker','=','satker.id_satker')->select('dpo.*','satker.nama_satker')->get();
        }
        elseif($request->satker == NULL && $request->status_ditemukan != NULL){
            $dpo = Dpo::join('satker','dpo.id_satker','=','satker.id_satker')->select('dpo.*','satker.nama_satker')->where('dpo.status_ditemukan', $request->status_ditemukan)->get();
        }
        elseif($request->satker != NULL && $request->status_ditemukan != NULL){
            $dpo = Dpo::join('satker','dpo.id_satker','=','satker.id_satker')->select('dpo.*','satker.nama_satker')->where('dpo.id_satker', $request->satker)->where('dpo.status_ditemukan', $request->status_ditemukan)->get();
        }
        else{
            $dpo = Dpo::join('satker','dpo.id_satker','=','satker.id_satker')->select('dpo.*','satker.nama_satker')->where('dpo.id_satker', $request->satker)->get();
        }
        $datasatker = Satker::get();
        return view("cetak.layanan.informasi_dpo", compact('dpo','datasatker'));
    }

    public function jadwalsidang(Request $request)
    {

        if($request->tanggal == null){
            $tanggal = Carbon::today()->toDateString();
        }else{
            $tanggal = $request->tanggal;
        }
        
        if(!$request){
            $jadwalsidang = JadwalSidang::join('satker','jadwal_sidang.id_satker','=','satker.id_satker')->where('tanggal_sidang',$tanggal)->select('jadwal_sidang.*','satker.nama_satker')->get();
        }
        elseif($request->satker == NULL){
            $jadwalsidang = JadwalSidang::join('satker','jadwal_sidang.id_satker','=','satker.id_satker')->where('tanggal_sidang',$tanggal)->select('jadwal_sidang.*','satker.nama_satker')->get();
        }
        elseif($request->satker != NULL){
            $jadwalsidang = JadwalSidang::join('satker','jadwal_sidang.id_satker','=','satker.id_satker')->where('tanggal_sidang',$tanggal)->where('jadwal_sidang.id_satker', $request->satker)->select('jadwal_sidang.*','satker.nama_satker')->get();
        }
        else{
            $jadwalsidang = JadwalSidang::join('satker','jadwal_sidang.id_satker','=','satker.id_satker')->where('tanggal_sidang',$tanggal)->select('jadwal_sidang.*','satker.nama_satker')->get();
        }
        $datasatker = Satker::get();
        return view("cetak.layanan.informasi_jadwalsidang", compact('jadwalsidang','datasatker','tanggal'));
    }


    public function detail_dpo($slug)
    {
        $kegiatan = "";
        return response($kegiatan);
    }
}

