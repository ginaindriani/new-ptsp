<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $secretKey = env('RECAPTCHA_SITE_KEY');
        return view('auth.login', compact('secretKey'));
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {

        $request->validate([
            'g_token' => 'required',
        ], [
            'g_token.required' => 'Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda'
        ]);
        
        $secretKey = env('RECAPTCHA_SECRET_KEY');
        $token = $request->g_token;
        $ip = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $token . "&remoteip=" . $ip;
        $verify = file_get_contents($url);
        $response = json_decode($verify);

        if (!$response) 
            return response()->json([
                'errors' => [
                    'g_token' => ["Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda"]
                ], 'message' => 'The given data was invalid.'
            ], 422);    
            
        if ($response && !$response->success) {
            return response()->json([
                'errors' => [
                    'g_token' => ["Terdeteksi aktifitas mencurigakan, silahkan isi ulang form anda"]
                ], 'message' => 'The given data was invalid.'
            ], 422);   
        }

        $request->authenticate();

        
        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
