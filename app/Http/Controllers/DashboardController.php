<?php

namespace App\Http\Controllers;

use App\Models\MasterLayanan;
use App\Models\PermohonanLayanan;
use App\Models\Satker;
use App\Models\Tamu;
use App\Utilities\Utilities;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use stdClass;
use Yajra\DataTables\Facades\DataTables;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $masterLayanan = [];
        $masterLayanans = MasterLayanan::where('aktif', 1)->get();
        foreach ($masterLayanans as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.index', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            $value->url_target = $url;

            if ($value->id_layanan == 11 || $value->id_layanan == 23)
                $value_is_blank = false;
            else
                $value_is_blank = true;

            $masterLayanan[] = $value;
        }
        return view("pages.index", compact('masterLayanan'));
    }

    public function getBigData()
    {
        $urlTotalPegawai = 'https://absensi.kejaksaan.go.id/absen/api/get_pegawai_mataelang_dan_satker_turunan/00';

        $satker = Satker::select('nama_satker','id_satker')->get();
        $totalOrmas = PermohonanLayanan::where('jenis_layanan', 25)->count();
        $totalPakem = PermohonanLayanan::where('jenis_layanan', 23)->count();
        $totalTamu  = Tamu::leftJoin('tamu_tambahan', 'tamu_tambahan.id_tamu_utama', '=', 'tamu.id')->count();
        $totalPegawai = json_decode($this->httpGetJsonCustom([], $urlTotalPegawai))->total;
        $totalPidum = DB::connection("dbtrend_analysis")
            ->table('perkara_trends')
            ->join('satker_long_lats', 'satker_long_lats.kode_satker', '=', 'perkara_trends.kode_satker')
            ->count();
        $totalPidsus = DB::connection("dbtrend_analysis")
            ->table('perkara_trend_pidsuses')
            ->join('satker_long_lats', 'satker_long_lats.kode_satker', '=', 'perkara_trend_pidsuses.kode_satker')
            ->count();
        return view("pages.bigdata.bigdata", compact(
            'totalOrmas',
            'satker',
            'totalPakem',
            'totalTamu',
            'totalPegawai',
            'totalPidum',
            'totalPidsus')
        );
    }

    public function getBigDataCalculate(Request $request){

        $id_satker = $request->satker;

        if ($id_satker == 0) {

            $urlTotalPegawai = 'https://absensi.kejaksaan.go.id/absen/api/get_pegawai_mataelang_dan_satker_turunan/00';

            $satker = Satker::select('nama_satker','id_satker')->get();
            $totalOrmas = PermohonanLayanan::where('jenis_layanan', 25)->count();
            $totalPakem = PermohonanLayanan::where('jenis_layanan', 23)->count();
            $totalTamu  = Tamu::leftJoin('tamu_tambahan', 'tamu_tambahan.id_tamu_utama', '=', 'tamu.id')->count();
            $totalPegawai = json_decode($this->httpGetJsonCustom([], $urlTotalPegawai))->total;
            $totalPidum = DB::connection("dbtrend_analysis")
                ->table('perkara_trends')
                ->join('satker_long_lats', 'satker_long_lats.kode_satker', '=', 'perkara_trends.kode_satker')
                ->count();
            $totalPidsus = DB::connection("dbtrend_analysis")
                ->table('perkara_trend_pidsuses')
                ->join('satker_long_lats', 'satker_long_lats.kode_satker', '=', 'perkara_trend_pidsuses.kode_satker')
                ->count();

        }else{

            $satker = Satker::where('id_satker',$id_satker)->first();
            $kode_satker = $satker->kode_satker;
            // dd($satker->kode_satker);

            $urlTotalPegawai = 'https://absensi.kejaksaan.go.id/absen/api/get_pegawai_mataelang_dan_satker_turunan/'. $kode_satker;

            $satker = Satker::select('nama_satker','kode_satker')->get();
            $totalOrmas = PermohonanLayanan::where('jenis_layanan', 25)->where('id_satker',$id_satker)->count();
            $totalPakem = PermohonanLayanan::where('jenis_layanan', 23)->where('id_satker',$id_satker)->count();

            $totalTamu  = Tamu::leftJoin('tamu_tambahan', 'tamu_tambahan.id_tamu_utama', '=', 'tamu.id')->where('kode_satker',$kode_satker)->count();

            $totalPegawai = json_decode($this->httpGetJsonCustom([], $urlTotalPegawai))->total;

            $totalPidum = DB::connection("dbtrend_analysis")
                        ->table('perkara_trends')
                        ->join('satker_long_lats', 'satker_long_lats.kode_satker', '=', 'perkara_trends.kode_satker')
                        ->where('satker_long_lats.kode_satker_pegawai', 'like', $kode_satker.'%')
                        ->count();

            $totalPidsus = DB::connection("dbtrend_analysis")
                        ->table('perkara_trend_pidsuses')
                        ->join('satker_long_lats', 'satker_long_lats.kode_satker', '=', 'perkara_trend_pidsuses.kode_satker')
                        ->where('satker_long_lats.kode_satker_pegawai', 'like', $kode_satker.'%')
                        ->count();
        }

        $data = new stdClass;
        $data->totalOrmas = $totalOrmas;
        $data->satker = $satker;
        $data->totalPakem = $totalPakem;
        $data->totalTamu = $totalTamu;
        $data->totalPegawai = $totalPegawai;
        $data->totalPidum = $totalPidum;
        $data->totalPidsus = $totalPidsus;

        return response()->json($data, 200);
    }


    public function getModalPidum($satker) {
        if ($satker == 0) {

            $datas =  DB::connection("dbtrend_analysis")
            ->table('satker_long_lats')
            ->select([
                'satker_long_lats.satker',
                'satker_long_lats.kode_satker',
                DB::raw('SUM(IF(perkara_trends.id is null, 0, 1)) as total')
                ])
                ->leftjoin('perkara_trends', 'satker_long_lats.kode_satker', '=', 'perkara_trends.kode_satker')
                ->groupBy('satker_long_lats.kode_satker')
                ->get();

        }else{

            $dataSatker = Satker::where('id_satker',$satker)->first();
            $kode_satker = $dataSatker->kode_satker;

            $datas =  DB::connection("dbtrend_analysis")
            ->table('satker_long_lats')
            ->select([
                'satker_long_lats.satker',
                'satker_long_lats.kode_satker',
                DB::raw('SUM(IF(perkara_trends.id is null, 0, 1)) as total')
                ])
                ->leftjoin('perkara_trends', 'satker_long_lats.kode_satker', '=', 'perkara_trends.kode_satker')
                ->where('satker_long_lats.kode_satker_pegawai', 'like', $kode_satker.'%')
                ->groupBy('satker_long_lats.kode_satker')
                ->get();

        }

        return view('ajax.modal_pidum', compact('datas'));
    }

    public function getModalPidsus($satker) {

        if ($satker == 0) {

            $datas =  DB::connection("dbtrend_analysis")
            ->table('satker_long_lats')
            ->select([
                'satker_long_lats.satker',
                'satker_long_lats.kode_satker',
                DB::raw('SUM(IF(perkara_trend_pidsuses.id is null, 0, 1)) as total')
            ])
            ->leftjoin('perkara_trend_pidsuses', 'satker_long_lats.kode_satker', '=', 'perkara_trend_pidsuses.kode_satker')
            ->groupBy('satker_long_lats.kode_satker')
            ->get();

        }else{

            $dataSatker = Satker::where('id_satker',$satker)->first();
            $kode_satker = $dataSatker->kode_satker;

            $datas =  DB::connection("dbtrend_analysis")
            ->table('satker_long_lats')
            ->select([
                'satker_long_lats.satker',
                'satker_long_lats.kode_satker',
                DB::raw('SUM(IF(perkara_trend_pidsuses.id is null, 0, 1)) as total')
            ])
            ->leftjoin('perkara_trend_pidsuses', 'satker_long_lats.kode_satker', '=', 'perkara_trend_pidsuses.kode_satker')
            ->where('satker_long_lats.kode_satker_pegawai', 'like', $kode_satker.'%')
            ->groupBy('satker_long_lats.kode_satker')
            ->get();

        }
        return view('ajax.modal_pidsus', compact('datas'));
    }

    public function getDataPidsus(Request $request) {
        $selectList = [
            'perkara_trend_pidsuses.id_berkas',
            'perkara_trend_pidsuses.no_spdp',
            'perkara_trend_pidsuses.id_perkara',
            'perkara_trend_pidsuses.nama_tsk',
            'perkara_trend_pidsuses.tgl_spdp',
            'perkara_trend_pidsuses.jenis_pidana',
            'perkara_trend_pidsuses.jenis_perkara',
            'satker_long_lats.satker'
        ];
        $prosesPerkara = Utilities::dataProcessPerkara();
        foreach ($prosesPerkara as $key => $value) {
            $selectList[] = DB::raw("perkara_trend_detail_pidsuses.$value");
        }
        $reversed = array_reverse($prosesPerkara);

        $mainQuery  = DB::connection("dbtrend_analysis")
            ->table('perkara_trend_pidsuses')
            ->select($selectList)
                ->join('satker_long_lats', 'satker_long_lats.kode_satker', '=', 'perkara_trend_pidsuses.kode_satker')
                ->join('perkara_trend_detail_pidsuses', 'perkara_trend_detail_pidsuses.id_perkara_pidsus', '=', 'perkara_trend_pidsuses.id')
                ->where('satker_long_lats.kode_satker_pegawai', 'like', '02%');

        if ($request->kode_satker)
            $mainQuery = $mainQuery->where('satker_long_lats.kode_satker', $request->kode_satker);

        return DataTables::of($mainQuery)
            ->filterColumn('tahap_berkas', function($query, $keyword) use ($reversed) {
                $sql = "";
                foreach ($reversed as $key => $value) {
                    if (strpos(strtolower($value), strtolower($keyword)) !== false) {
                        if (!$sql) {
                            $sql .= " perkara_trend_detail_pidsuses.$value IS NOT NULL ";
                        } else {
                            $sql .= " OR perkara_trend_detail_pidsuses.$value IS NOT NULL ";
                        }
                    }
                }
                $query->whereRaw(DB::raw($sql));
            })
            ->orderColumn('tahap_berkas', function ($query, $order) {
                $query->orderBy(DB::raw("YEAR(perkara_trend_detail_pidsuses.tgl_spdp)"), $order);
            })
            ->editColumn('tahap_berkas', function ($model) use ($reversed) {
                foreach ($reversed as $key => $value) {
                    if ($model->$value) {
                        return str_replace('_',' ',ucwords($key));
                    }
                }
                return '-';
            })
            ->make(true);
    }

    public function getDataPidum(Request $request) {
        $selectList = [
            'perkara_trends.id_berkas',
            'perkara_trends.no_spdp',
            'perkara_trends.id_perkara',
            'perkara_trends.nama_tsk',
            'perkara_trends.tgl_spdp',
            'perkara_trends.jenis_pidana',
            'perkara_trends.jenis_perkara',
            'satker_long_lats.satker'
        ];
        $prosesPerkara = Utilities::dataProcessPerkara();
        foreach ($prosesPerkara as $key => $value) {
            $selectList[] = DB::raw("perkara_trend_details.$value");
        }
        $reversed = array_reverse($prosesPerkara);

        $mainQuery  = DB::connection("dbtrend_analysis")
            ->table('perkara_trends')
            ->select($selectList)
                ->join('satker_long_lats', 'satker_long_lats.kode_satker', '=', 'perkara_trends.kode_satker')
                ->join('perkara_trend_details', 'perkara_trend_details.id_perkara_trend', '=', 'perkara_trends.id')
                ->where('satker_long_lats.kode_satker_pegawai', 'like', '02%');

        if ($request->kode_satker)
            $mainQuery = $mainQuery->where('satker_long_lats.kode_satker', $request->kode_satker);

        return DataTables::of($mainQuery)
            ->filterColumn('tahap_berkas', function($query, $keyword) use ($reversed) {
                $sql = "";
                foreach ($reversed as $key => $value) {
                    if (strpos(strtolower($value), strtolower($keyword)) !== false) {
                        if (!$sql) {
                            $sql .= " perkara_trend_details.$value IS NOT NULL ";
                        } else {
                            $sql .= " OR perkara_trend_details.$value IS NOT NULL ";
                        }
                    }
                }
                $query->whereRaw(DB::raw($sql));
            })
            ->orderColumn('tahap_berkas', function ($query, $order) {
                $query->orderBy(DB::raw("YEAR(perkara_trend_details.tgl_spdp)"), $order);
            })
            ->editColumn('tahap_berkas', function ($model) use ($reversed) {
                foreach ($reversed as $key => $value) {
                    if ($model->$value) {
                        return str_replace('_',' ',ucwords($key));
                    }
                }
                return '-';
            })
            ->make(true);
    }

    private static function httpGetJsonCustom($headers, $url)
    {
        set_time_limit(0);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
