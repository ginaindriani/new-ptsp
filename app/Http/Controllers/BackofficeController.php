<?php

namespace App\Http\Controllers;

use App\DataTables\BeritaDataTable;
use App\Models\Berita;
use App\Models\MasterLayanan;
use App\Models\PermohonanLayanan;
use App\Models\Satker;
use App\Models\Tamu;
use App\Utilities\Utilities;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB as FacadesDB;
use stdClass;
use Yajra\DataTables\Facades\DataTables;

class BackofficeController extends Controller
{
    //
    public function index()
    {
        $masterLayanan = [];
        $masterLayanans = MasterLayanan::where('aktif', 1)->get();
        foreach ($masterLayanans as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.index', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            $value->url_target = $url;

            if ($value->id_layanan == 11 || $value->id_layanan == 23)
                $value_is_blank = false;
            else
                $value_is_blank = true;

            $masterLayanan[] = $value;
        }
        return view("pages.index", compact('masterLayanan'));
    }

    public function dashboard()
    {
        $datasatker = Satker::get();
        if(auth()->user()->kode_satker == null){
            $total_pelayanan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            $total_pelayanan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString())
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            $total_pelayanan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->where('permohonan_layanan.status', null)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            

            $total_pengaduan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pengaduan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString())
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            $total_pengaduan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->where('permohonan_layanan.status', null)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            $data_layanan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->select([
                    'master_layanans.nama_layanan'
                    , FacadesDB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();
            
            $data_pengaduan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select([
                    'master_layanans.nama_layanan'
                    , FacadesDB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();


            $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            $data_layanan_perbulan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
        
                $layananmcount = [];
                $layananArr = [];
            
                foreach ($data_layanan_perbulan as $key => $value) {
                    $layananmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($layananmcount[$i])) {
                        $layananArr[$i]['count'] = $layananmcount[$i];
                    } else {
                        $layananArr[$i]['count'] = 0;
                    }
                    $layananArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_layanan_perbulan =  response()->json(array_values($layananArr));

            $data_pengaduan_perbulan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
        
                $pengaduanmcount = [];
                $pengaduanArr = [];
            
                foreach ($data_pengaduan_perbulan as $key => $value) {
                    $pengaduanmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($pengaduanmcount[$i])) {
                        $pengaduanArr[$i]['count'] = $pengaduanmcount[$i];
                    } else {
                        $pengaduanArr[$i]['count'] = 0;
                    }
                    $pengaduanArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_pengaduan_perbulan =  response()->json(array_values($pengaduanArr));

            $top_10_satker_data_layanan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('jumlah','desc')
            ->limit(10)
            ->get();

            $top_10_satker_data_pengaduan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('jumlah','desc')
            ->limit(10)
            ->get();

            $top_10_satker_persentase_layanan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('hitung','desc')
            ->limit(10)
            ->get();

            $top_10_satker_persentase_pengaduan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('percentage','desc')
            ->limit(10)
            ->get();
        }else{
            $total_pelayanan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            $total_pelayanan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString())
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            $total_pelayanan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->where('permohonan_layanan.status', null)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            $total_pengaduan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pengaduan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString())
                ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());
    
            $total_pengaduan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.status', null)
                ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $data_layanan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
                ->select([
                    'master_layanans.nama_layanan'
                    , FacadesDB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();
            
            $data_pengaduan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
                ->select([
                    'master_layanans.nama_layanan'
                    , FacadesDB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();


            $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            $data_layanan_perbulan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
        
                $layananmcount = [];
                $layananArr = [];
            
                foreach ($data_layanan_perbulan as $key => $value) {
                    $layananmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($layananmcount[$i])) {
                        $layananArr[$i]['count'] = $layananmcount[$i];
                    } else {
                        $layananArr[$i]['count'] = 0;
                    }
                    $layananArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_layanan_perbulan =  response()->json(array_values($layananArr));

            $data_pengaduan_perbulan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
        
                $pengaduanmcount = [];
                $pengaduanArr = [];
            
                foreach ($data_pengaduan_perbulan as $key => $value) {
                    $pengaduanmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($pengaduanmcount[$i])) {
                        $pengaduanArr[$i]['count'] = $pengaduanmcount[$i];
                    } else {
                        $pengaduanArr[$i]['count'] = 0;
                    }
                    $pengaduanArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_pengaduan_perbulan =  response()->json(array_values($pengaduanArr));

            $top_10_satker_data_layanan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('jumlah','desc')
            ->limit(10)
            ->get();

            $top_10_satker_data_pengaduan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->where('master_layanans.is_lapdu',1)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('jumlah','desc')
            ->limit(10)
            ->get();

            $top_10_satker_persentase_layanan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('hitung','desc')
            ->limit(10)
            ->get();

            $top_10_satker_persentase_pengaduan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->where('master_layanans.is_lapdu',1)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('percentage','desc')
            ->limit(10)
            ->get();
        }
        


        return view("pages.backoffice.dashboard", compact('total_pelayanan','total_pelayanan_hari_ini','total_pelayanan_belum','total_pengaduan','total_pengaduan_hari_ini','total_pengaduan_belum','data_pengaduan','data_layanan','hitung_data_pengaduan_perbulan','hitung_data_layanan_perbulan','top_10_satker_data_layanan','top_10_satker_data_pengaduan','top_10_satker_persentase_layanan','top_10_satker_persentase_pengaduan','datasatker'));
    }

    public function dashboardsatker($id)
    {
        $total_pelayanan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

        $total_pelayanan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->where('permohonan_layanan.created_at', Carbon::now())
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

        $total_pelayanan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->where('permohonan_layanan.status', null)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

        $total_pengaduan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

        $total_pengaduan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString())
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

        $total_pengaduan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->where('permohonan_layanan.status', null)
            ->where('permohonan_layanan.id_satker',auth()->user()->kode_satker)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

        $data_layanan = 
            FacadesDB::table('master_layanans')
            ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->where('permohonan_layanan.id_satker',$id)
            ->select([
                'master_layanans.nama_layanan'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('master_layanans.nama_layanan')
            ->orderBy('master_layanans.order_by','asc')
            ->orderBy('percentage','asc')
            ->get();
        
        $data_pengaduan = 
            FacadesDB::table('master_layanans')
            ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->where('permohonan_layanan.id_satker',$id)
            ->select([
                'master_layanans.nama_layanan'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('master_layanans.nama_layanan')
            ->orderBy('master_layanans.order_by','asc')
            ->orderBy('percentage','asc')
            ->get();


        $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $data_layanan_perbulan = 
            FacadesDB::table('master_layanans')
            ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->where('permohonan_layanan.id_satker',$id)
            ->select('permohonan_layanan.created_at as created_at')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('m');
            });
    
            $layananmcount = [];
            $layananArr = [];
        
            foreach ($data_layanan_perbulan as $key => $value) {
                $layananmcount[(int)$key] = count($value);
            }
        
            for ($i = 1; $i <= 12; $i++) {
                if (!empty($layananmcount[$i])) {
                    $layananArr[$i]['count'] = $layananmcount[$i];
                } else {
                    $layananArr[$i]['count'] = 0;
                }
                $layananArr[$i]['month'] = $month[$i - 1];
            }

        $hitung_data_layanan_perbulan =  response()->json(array_values($layananArr));

        $data_pengaduan_perbulan = 
            FacadesDB::table('master_layanans')
            ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->where('permohonan_layanan.id_satker',$id)
            ->select('permohonan_layanan.created_at as created_at')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('m');
            });
    
            $pengaduanmcount = [];
            $pengaduanArr = [];
        
            foreach ($data_pengaduan_perbulan as $key => $value) {
                $pengaduanmcount[(int)$key] = count($value);
            }
        
            for ($i = 1; $i <= 12; $i++) {
                if (!empty($pengaduanmcount[$i])) {
                    $pengaduanArr[$i]['count'] = $pengaduanmcount[$i];
                } else {
                    $pengaduanArr[$i]['count'] = 0;
                }
                $pengaduanArr[$i]['month'] = $month[$i - 1];
            }

        $hitung_data_pengaduan_perbulan =  response()->json(array_values($pengaduanArr));

        $top_10_satker_data_layanan = 
        FacadesDB::table('satker')
        ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
        ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
        ->where('master_layanans.tipe_layanan','layanan')
        ->where('master_layanans.aktif',1)
        ->where('permohonan_layanan.id_satker',$id)
        ->where('master_layanans.is_lapdu',null)
        ->select([
            'satker.nama_satker'
            , FacadesDB::raw('
                count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                ')
        ])
        ->groupBy('satker.nama_satker')
        ->orderBy('jumlah','desc')
        ->limit(10)
        ->get();

        $top_10_satker_data_pengaduan = 
        FacadesDB::table('satker')
        ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
        ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
        ->where('master_layanans.tipe_layanan','layanan')
        ->where('master_layanans.aktif',1)
        ->where('permohonan_layanan.id_satker',$id)
        ->where('master_layanans.is_lapdu',1)
        ->select([
            'satker.nama_satker'
            , FacadesDB::raw('
                count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                ')
        ])
        ->groupBy('satker.nama_satker')
        ->orderBy('jumlah','desc')
        ->limit(10)
        ->get();

        $top_10_satker_persentase_layanan = 
        FacadesDB::table('satker')
        ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
        ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
        ->where('master_layanans.tipe_layanan','layanan')
        ->where('master_layanans.aktif',1)
        ->where('permohonan_layanan.id_satker',$id)
        ->where('master_layanans.is_lapdu',null)
        ->select([
            'satker.nama_satker'
            , FacadesDB::raw('
                count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                ')
        ])
        ->groupBy('satker.nama_satker')
        ->orderBy('hitung','desc')
        ->limit(10)
        ->get();

        $top_10_satker_persentase_pengaduan = 
        FacadesDB::table('satker')
        ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
        ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
        ->where('master_layanans.tipe_layanan','layanan')
        ->where('master_layanans.aktif',1)
        ->where('permohonan_layanan.id_satker',$id)
        ->where('master_layanans.is_lapdu',1)
        ->select([
            'satker.nama_satker'
            , FacadesDB::raw('
                count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                ')
        ])
        ->groupBy('satker.nama_satker')
        ->orderBy('percentage','desc')
        ->limit(10)
        ->get();
        


        return view("pages.backoffice.dashboardsatker", compact('total_pelayanan', 'total_pelayanan_hari_ini','total_pelayanan_belum', 'total_pengaduan','total_pengaduan_hari_ini','total_pengaduan_belum','data_pengaduan','data_layanan','hitung_data_pengaduan_perbulan','hitung_data_layanan_perbulan','top_10_satker_data_layanan','top_10_satker_data_pengaduan','top_10_satker_persentase_layanan','top_10_satker_persentase_pengaduan'));
    }

    public function filterdashboard($tempid)
    {
        if($tempid == 0){
            $total_pelayanan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->select('permohonan_layanan.id_permohonan_layanan')
            ->get());

            $total_pelayanan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->where('permohonan_layanan.created_at', Carbon::now())
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pelayanan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->where('permohonan_layanan.status', null)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pengaduan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pengaduan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString())
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pengaduan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.status', null)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());
            

            $data_layanan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->select([
                    'master_layanans.nama_layanan'
                    , FacadesDB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();
            
            $data_pengaduan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select([
                    'master_layanans.nama_layanan'
                    , FacadesDB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();


            $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            $data_layanan_perbulan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
        
                $layananmcount = [];
                $layananArr = [];
            
                foreach ($data_layanan_perbulan as $key => $value) {
                    $layananmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($layananmcount[$i])) {
                        $layananArr[$i]['count'] = $layananmcount[$i];
                    } else {
                        $layananArr[$i]['count'] = 0;
                    }
                    $layananArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_layanan_perbulan =  response()->json(array_values($layananArr));

            $data_pengaduan_perbulan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
        
                $pengaduanmcount = [];
                $pengaduanArr = [];
            
                foreach ($data_pengaduan_perbulan as $key => $value) {
                    $pengaduanmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($pengaduanmcount[$i])) {
                        $pengaduanArr[$i]['count'] = $pengaduanmcount[$i];
                    } else {
                        $pengaduanArr[$i]['count'] = 0;
                    }
                    $pengaduanArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_pengaduan_perbulan =  response()->json(array_values($pengaduanArr));

            $top_10_satker_data_layanan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('jumlah','desc')
            ->limit(10)
            ->get();

            $top_10_satker_data_pengaduan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('jumlah','desc')
            ->limit(10)
            ->get();

            $top_10_satker_persentase_layanan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('hitung','desc')
            ->limit(10)
            ->get();

            $top_10_satker_persentase_pengaduan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('master_layanans.is_lapdu',1)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('percentage','desc')
            ->limit(10)
            ->get();
        }else{

            $total_pelayanan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pelayanan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString())
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pelayanan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->where('permohonan_layanan.status', null)
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pengaduan = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pengaduan_hari_ini = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->whereDate('permohonan_layanan.created_at', Carbon::today()->toDateString())
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());

            $total_pengaduan_belum = count(MasterLayanan::join('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.status', null)
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select('permohonan_layanan.id_permohonan_layanan')
                ->get());
            
            $data_layanan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select([
                    'master_layanans.nama_layanan'
                    , FacadesDB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();
            
            $data_pengaduan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select([
                    'master_layanans.nama_layanan'
                    , FacadesDB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();


            $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            $data_layanan_perbulan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
        
                $layananmcount = [];
                $layananArr = [];
            
                foreach ($data_layanan_perbulan as $key => $value) {
                    $layananmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($layananmcount[$i])) {
                        $layananArr[$i]['count'] = $layananmcount[$i];
                    } else {
                        $layananArr[$i]['count'] = 0;
                    }
                    $layananArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_layanan_perbulan =  response()->json(array_values($layananArr));

            $data_pengaduan_perbulan = 
                FacadesDB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->where('permohonan_layanan.id_satker',$tempid)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });
        
                $pengaduanmcount = [];
                $pengaduanArr = [];
            
                foreach ($data_pengaduan_perbulan as $key => $value) {
                    $pengaduanmcount[(int)$key] = count($value);
                }
            
                for ($i = 1; $i <= 12; $i++) {
                    if (!empty($pengaduanmcount[$i])) {
                        $pengaduanArr[$i]['count'] = $pengaduanmcount[$i];
                    } else {
                        $pengaduanArr[$i]['count'] = 0;
                    }
                    $pengaduanArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_pengaduan_perbulan =  response()->json(array_values($pengaduanArr));

            $top_10_satker_data_layanan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('permohonan_layanan.id_satker',$tempid)
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('jumlah','desc')
            ->limit(10)
            ->get();

            $top_10_satker_data_pengaduan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('permohonan_layanan.id_satker',$tempid)
            ->where('master_layanans.is_lapdu',1)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('jumlah','desc')
            ->limit(10)
            ->get();

            $top_10_satker_persentase_layanan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('permohonan_layanan.id_satker',$tempid)
            ->where('master_layanans.is_lapdu',null)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('hitung','desc')
            ->limit(10)
            ->get();

            $top_10_satker_persentase_pengaduan = 
            FacadesDB::table('satker')
            ->leftjoin('permohonan_layanan','satker.id_satker','=','permohonan_layanan.id_satker')
            ->leftjoin('master_layanans','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
            ->where('master_layanans.tipe_layanan','layanan')
            ->where('master_layanans.aktif',1)
            ->where('permohonan_layanan.id_satker',$tempid)
            ->where('master_layanans.is_lapdu',1)
            ->select([
                'satker.nama_satker'
                , FacadesDB::raw('
                    count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belumdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudahdiproses,
                    count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah,
                    concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage,
                    count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS hitung
                    ')
            ])
            ->groupBy('satker.nama_satker')
            ->orderBy('percentage','desc')
            ->limit(10)
            ->get();
        }
        


        return view("pages.backoffice.filterdashboard", compact('total_pelayanan','total_pelayanan_hari_ini','total_pelayanan_belum','total_pengaduan','total_pengaduan_hari_ini','total_pengaduan_belum', 'data_pengaduan','data_layanan','hitung_data_pengaduan_perbulan','hitung_data_layanan_perbulan','top_10_satker_data_layanan','top_10_satker_data_pengaduan','top_10_satker_persentase_layanan','top_10_satker_persentase_pengaduan', 'tempid'));
    }

    public function layanan(){
        $masterLayanan = [];
        $masterLayanans = MasterLayanan::where('aktif', 1)->where('tipe_layanan','layanan')->where('is_lapdu',null)->orderBy('order_by','asc')->get();
        foreach ($masterLayanans as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.index', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            $value->url_target = $url;

            if ($value->id_layanan == 11 || $value->id_layanan == 23)
                $value_is_blank = false;
            else
                $value_is_blank = true;

            $masterLayanan[] = $value;
        }
        return view("pages.backoffice.menulayanan", compact('masterLayanan'));
    }

    public function informasi(){
        $masterLayanan = [];
        $masterLayanans = MasterLayanan::where('aktif', 1)->where('tipe_layanan','sistem')->where('is_lapdu',null)->orderBy('order_by','asc')->get();
        foreach ($masterLayanans as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.index', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            $value->url_target = $url;

            if ($value->id_layanan == 11 || $value->id_layanan == 23)
                $value_is_blank = false;
            else
                $value_is_blank = true;

            $masterLayanan[] = $value;
        }
        return view("pages.backoffice.menuinformasi", compact('masterLayanan'));
    }

    public function pengaduan(){
        $masterLayanan = [];
        $masterLayanans = MasterLayanan::where('aktif', 1)->where('tipe_layanan','layanan')->where('is_lapdu',1)->orderBy('order_by','asc')->get();
        foreach ($masterLayanans as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.index', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            $value->url_target = $url;

            if ($value->id_layanan == 11 || $value->id_layanan == 23)
                $value_is_blank = false;
            else
                $value_is_blank = true;

            $masterLayanan[] = $value;
        }
        return view("pages.backoffice.menupengaduan", compact('masterLayanan'));
    }
    
}
