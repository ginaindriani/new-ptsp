<?php

namespace App\Http\Controllers;

use App\DataTables\DataTamuDataTable;
use App\Models\Satker;
use App\Models\Tamu;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Str;

class DataTamusController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(DataTilangDataTable $dataTable, Request $request)
    {
        // dd($dataTable);
        return $dataTable->render('pages.master.datatamu.index');
    }

    public function create()
    {
        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
        }else{
            $satker = Satker::get();
        }

        return view("pages.master.datatamu.create", compact('satker'));
    }

    public function store(Request $request)
    {
        if($request->photo == null){
            return redirect()->back()->withInput()->with(['error' => 'Terdeteksi aktifitas mencurigakan, pastikan anda sudah melakukan foto melalui webcam']);
        }

        

        $satker = Satker::where('id_satker',$request->satker)->select('kode_satker')->first();

        $unique_id = 'V'. date('ymd') . date('His'). rand(1, 2000);
        $unique_key= (string) Str::uuid();
        $fileName = $unique_id.'-'.time(); //generating unique file name
        preg_match('/data:image\/(.*?);/', $request->photo, $tamu_photo_extension); // extract the image extension
        $request->photo = preg_replace('/data:image\/(.*?);base64,/', '', $request->photo); // remove the type part
        $request->photo = str_replace(' ', '+', $request->photo);
        $photoName = $fileName.'.'.$tamu_photo_extension[1]; //generating unique file name
        $check_tamu = Tamu::where('nik', $request->no_identitas)->count();
        if($check_tamu >= 1){
            Tamu::where('nik', $request->no_identitas)
            ->update([
                'kode_satker' => $satker->kode_satker,
                'nik_type' => "1",
                'nik' => $request->no_identitas,
                'name' => $request->nama,
                'type' => $request->type,
                'email' => $request->email,
                'address' => $request->alamat,
                'phone' => $request->no_hp,
                'status' => "1",
                'tujuan' => $request->tujuan,
                'foto_diri' => $photoName,
                'jenis_kelamin' => $request->jenis_kelamin,
                'waktu_kedatangan' => now(),
            ]);
            $data_pegawai_ybs = Tamu::where('nik', $request->no_identitas)->select('unique_key')->first();

            $unique_keys = $data_pegawai_ybs->unique_key;
            $url = '?uuid='.Crypt::encrypt($unique_keys);
        }else{
            $data_tamu = new Tamu();
            $data_tamu->uuid = $unique_key;
            $data_tamu->unique_key = $data_tamu->uuid;
            $data_tamu->kode_satker = $satker->kode_satker;
            $data_tamu->nik_type = "1";
            $data_tamu->nik = $request->no_identitas;
            $data_tamu->name = $request->nama;
            $data_tamu->slug = $data_tamu->unique_key;
            $data_tamu->type = $request->type;
            $data_tamu->email = $request->email;
            $data_tamu->address = $request->alamat;
            $data_tamu->phone = $request->no_hp;
            $data_tamu->status = "1";
            $data_tamu->tujuan = $request->tujuan;
            $data_tamu->foto_diri = $photoName;
            $data_tamu->jenis_kelamin = $request->jenis_kelamin;
            $data_tamu->waktu_kedatangan = now();
            $data_tamu->save();

            $url = '?uuid='.Crypt::encrypt($unique_key);
        }
        
        $tokenPtsp = '6400d8d96832a21178b875de6743eac7';
        $dataPtsp = [
            'no_identitas'     => $request->no_identitas,
            'nama'     => $request->nama,
            'jenis_kelamin'     => $request->jenis_kelamin,
            'kode_satker'     => $satker->kode_satker,
            'email'     => $request->email,
            'alamat'     => $request->alamat,
            'no_hp'     => $request->no_hp,
            'photo'     => $request->photo,
            'photo_name'     => $photoName,
            'unique_key'    => $unique_key,
        ];

        $apiPtsp = 'https://bukutamu.kejaksaan.go.id/api/post-tamu-ptsp/';
        
        $ClientPtsp = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
            'verify' => false
        ]);
        
        try{
            $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                'form_params' => $dataPtsp
            ]);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            //lewati
        }

        return  redirect()->route('datatamu.index')->with(["success" => "Data berhasil ditambah"]);
    }
}
