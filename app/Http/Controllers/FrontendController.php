<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\Kunjungan;
use App\Models\Organisasi;
use App\Models\Satker;
use App\Models\Tamu;
use App\Models\Pegawai;
use App\Models\SurveyResponse;
use App\Models\Bidang;
use Illuminate\Http\Request;
use App\Services\Sipede\SatkerService;
use Illuminate\Support\Facades\DB;
use App\Traits\Uuid;
use Illuminate\Support\Carbon;
use App\Helpers\ResponseFormatter;
use App\Models\KunjunganTilang;
use App\Models\KunjunganTilangDetail;
use Illuminate\Support\Facades\File;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use app\Models\Staticdatas;
use App\Models\TamuPengikut;
use App\RealTimeApi;
use App\Services\Survey\SurveyService;
use RealRashid\SweetAlert\Facades\Alert;
use Barryvdh\DomPDF\Facade\Pdf;
use Exception;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;

class FrontendController extends Controller
{

    public function __construct()
    {
        ini_set('memory_limit', '4096M');  //4GB
        ini_set('max_execution_time', 300); //5 minutes
    }
    
    public function index()
    {
        $satker = Satker::all();

        return view('frontend.index', compact('satker'));
    }

    public function ptsp_pilih_satker()
    {
        $satker = Satker::all();
        return view('frontend.ptspsatker', compact('satker'));
    }

    public function indexadmin()
    {
        $satker = Satker::all();

        return view('frontend.home', compact('satker'));
    }

    public function hasilsurvey()
    {
        $satker = Satker::all();

        return view('frontend.hasilsurvey', compact('satker'));
    }

    public function hasilsurveysatker(Request $request, $kode_satker)
    {

       
        $selected = DB::select("select s.question, 
            COUNT(CASE WHEN s.response = 'Sangat Kurang' then 1 ELSE NULL END) as 'sangat_kurang',
            COUNT(CASE WHEN s.response = 'Kurang' then 1 ELSE NULL END) as 'kurang',
            COUNT(CASE WHEN s.response = 'Cukup' then 1 ELSE NULL END) as 'cukup',
            COUNT(CASE WHEN s.response = 'Baik' then 1 ELSE NULL END) as 'baik',
            COUNT(CASE WHEN s.response = 'Sangat Baik' then 1 ELSE NULL END) as 'sangat_baik'
            from survey_response s
            join kunjungan k 
            on s.kunjungan_id = k.id
            join satker satker
            on k.id_satker = satker.id_satker
            where satker.kode_satker = $kode_satker
        ");

        $selected_table = DB::select("select s.question,
            COUNT(CASE WHEN s.response = 'Sangat Kurang' then 1 ELSE NULL END) as 'sangat_kurang',
            COUNT(CASE WHEN s.response = 'Kurang' then 1 ELSE NULL END) as 'kurang',
            COUNT(CASE WHEN s.response = 'Cukup' then 1 ELSE NULL END) as 'cukup',
            COUNT(CASE WHEN s.response = 'Baik' then 1 ELSE NULL END) as 'baik',
            COUNT(CASE WHEN s.response = 'Sangat Baik' then 1 ELSE NULL END) as 'sangat_baik'
            from survey_response s
            join kunjungan k 
            on s.kunjungan_id = k.id
            join satker satker
            on k.id_satker = satker.id_satker
            where satker.kode_satker = $kode_satker
            GROUP BY s.question");
        

        $jumlah_jawaban = $selected[0]->sangat_kurang + $selected[0]->kurang + $selected[0]->cukup + $selected[0]->baik + $selected[0]->sangat_baik;

        $agregasi_sangat_kurang = $selected[0]->sangat_kurang * 0;
        $agregasi_kurang = $selected[0]->kurang * 0.25;
        $agregasi_cukup = $selected[0]->cukup * 0.5;
        $agregasi_baik = $selected[0]->baik * 0.75;
        $agregasi_sangat_baik = $selected[0]->sangat_baik * 1;

        $total_agregasi = $agregasi_sangat_kurang + $agregasi_kurang + $agregasi_cukup + $agregasi_baik + $agregasi_sangat_baik;

        
        $hasil_indeks = ($total_agregasi / $jumlah_jawaban) * 100;

        return view('frontend.hasilsurveysatker', compact('selected','hasil_indeks','selected_table'));
    }

    public function aksi(Request $request, $slug)
    {
        $satker =  DB::table('satker as s')
        ->select('s.id_satker as id_satker','s.nama_satker as nama_satker', 's.slug as slug', 's.tipe_satker as tipe_satker')
        ->where('s.slug', $slug)
        ->first();

        // return $satker;
        return view('frontend.aksi', compact('satker'));
    }

    public function aksiadmin(Request $request, $slug)
    {
        $satker =  DB::table('satker as s')
        ->select('s.id_satker as id_satker','s.nama_satker as nama_satker', 's.slug as slug')
        ->where('s.slug', $slug)
        ->first();

        return view('frontend.aksiadmin', compact('satker'));
    }

    public function ptsp(Request $request, $slug)
    {
        $satker =  DB::table('satker as s')
        ->select('s.id_satker as id_satker','s.kode_satker','s.nama_satker as nama_satker','s.slug as slug')
        ->where('s.slug', $slug)
        ->first();

        $data =  DB::table('satker as s')
        ->join('kunjungan as k','k.id_satker','=','s.id_satker')
        ->select('s.id_satker as id_satker','s.nama_satker as nama_satker','k.unique_id as unique_id' ,'s.slug as slug', 'k.photo_kendaraan as photo_kendaraan','k.created_at as jam_datang')
        ->where('s.kode_satker', $satker->kode_satker)
        ->where('k.id_tamu', null)
        ->whereDate('k.created_at', Carbon::today())
        ->get();

        return view('frontend.ptsp', compact('satker','data'));
    }

    public function kamdal(Request $request, $slug)
    {
        $satker =  DB::table('satker as s')
        ->select('s.id_satker as id_satker','s.nama_satker as nama_satker', 's.slug as slug')
        ->where('s.slug', $slug)
        ->first();
        
        return view('frontend.kamdal', compact('satker'));
    }

    public function tilang(Request $request, $slug)
    {
        $satker =  DB::table('satker as s')
        ->select('s.id_satker as id_satker','s.nama_satker as nama_satker', 's.slug as slug')
        ->where('s.slug', $slug)
        ->first();
        
        return view('frontend.tilang', compact('satker'));
    }

    public function kamdalbelakang(Request $request, $slug)
    {
        $satker =  DB::table('satker as s')
        ->select('s.id_satker as id_satker','s.nama_satker as nama_satker', 's.slug as slug')
        ->where('s.slug', $slug)
        ->first();
        
        return view('frontend.kamdalbelakang', compact('satker'));
    }

    public function show(Request $request, $slug)
    {
        $data_tujuan = DB::table('satker as s')
        ->join('pegawai as p','s.kode_satker','=','p.kode_satker')
        ->select(DB::raw("CONCAT(p.jabatan,' (',p.name,')') AS tujuan"), 'p.id as id_pegawai')
        ->where('s.slug', $slug)
        ->where('p.status_pegawai', '1')
        ->groupBy('tujuan')
        ->get();

        $data =  DB::table('satker as s')
        ->join('organisasi as o','s.id_satker','=','o.id_satker')
        ->select('s.id_satker as id_satker', 's.slug as slug', 's.kode_satker as kode_satker', 'o.id_organisasi as id_organisasi','s.nama_satker as nama_satker')
        ->where('s.slug', $slug)
        ->get();

        Alert::warning('Form Pendaftaran Tamu', 'Pastikan inputan terisi semua');

        return view('frontend.proses', compact('data','data_tujuan'));
    }

    public function ptsp_proses(Request $request, $slug)
    {
        $data_tujuan = DB::table('satker as s')
        ->join('pegawai as p','s.kode_satker','=','p.kode_satker')
        ->select(DB::raw("CONCAT(p.jabatan,' (',p.name,')') AS tujuan"), 'p.id as id_pegawai')
        ->where('s.slug', $slug)
        ->groupBy('tujuan')
        ->get();

        $data =  DB::table('satker as s')
        ->join('organisasi as o','s.id_satker','=','o.id_satker')
        ->select('s.id_satker as id_satker', 's.slug as slug', 's.kode_satker as kode_satker', 'o.id_organisasi as id_organisasi','s.nama_satker as nama_satker')
        ->where('s.slug', $slug)
        ->get();

        Alert::warning('Form Pendaftaran Tamu', 'Pastikan inputan terisi semua');

        return view('frontend.ptspsatkerproses', compact('data','data_tujuan'));
    }

    public function show2(Request $request, $slug ,$unique_id)
    {
        $unique = DB::table('kunjungan as k')
        ->where('k.unique_id', $unique_id)
        ->orderBy('k.created_at','desc')
        ->select('k.id as id_kunjungan','k.unique_id as unique_id','k.photo_kendaraan as photo_kendaraan')
        // ->where('p.status_pegawai', '1')
        ->first();

        $data_tujuan = DB::table('satker as s')
        ->join('pegawai as p','s.kode_satker','=','p.kode_satker')
        ->select(DB::raw("CONCAT(p.jabatan,' (',p.name,')') AS tujuan"), 'p.id as id_pegawai')
        ->where('s.slug', $slug)
        ->where('p.status_pegawai', '1')
        ->groupBy('tujuan')
        ->get();

        $data =  DB::table('satker as s')
        ->join('organisasi as o','s.id_satker','=','o.id_satker')
        ->select('s.id_satker as id_satker', 's.slug as slug', 's.kode_satker as kode_satker', 'o.id_organisasi as id_organisasi','s.nama_satker as nama_satker')
        ->where('s.slug', $slug)
        ->get();

        Alert::warning('Form Pendaftaran Tamu', 'Pastikan inputan terisi semua');

        return view('frontend.antarsurat', compact('data','unique','data_tujuan'));
    }

    public function store(Request $request)
    {

        $slug = $request->slug;

        return view('frontend.proses',[$slug]);
    }

    public function searchnik(Request $request)
    {

        $nik = $request->input('nik');

        if($nik)
        {
            $datatamu=DB::table('tamu')->where('no_identitas','=',$nik)->first();
            if($datatamu)
            {
                return ResponseFormatter::success(
                    $datatamu,
                    'Data Tamu Berhasil Diambil'
                );
            }
            else
            {
                return ResponseFormatter::error(
                    null,
                    'Data Tamu Tidak Ada',
                    404
                );
            }

            return ResponseFormatter::success(
                $datatamu,
                'Data Pertanyaan Berhasil Diambil'
            );
        }

    }

    function day_idn($day) {
        $hari = array (
            0 => 'Minggu',
            1 => 'Senin',
            2 => 'Selasa',
            3 => 'Rabu',
            4 => 'Kamis',
            5 => 'Jumat',
            6 => 'Sabtu'
        );
        return $hari[$day];
    }

    function month_idn($month) {
        $bulan = array (
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        );
        return $bulan[$month];
    }

    
    public function simpankendaraan(Request $request)
    {

        if( !empty($request['photo']) ){
            $fileDirectory = public_path('img').'/foto/'; // directory of uploaded file
            if (! File::isDirectory($fileDirectory)) {
                File::makeDirectory($fileDirectory, 0755, true);
            }

            $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
            $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
            $check_total_guest = Kunjungan::whereBetween(
                'created_at', [$check_total_guest_from, $check_total_guest_to]
            )
            ->where('id_satker', $request["id_satker"])
            ->get();
            $total_today_guest = count($check_total_guest);
            $total_today_guest = $total_today_guest + 1;
            $pengunjung_ke = '001';
            if($total_today_guest < 10) {
                $pengunjung_ke = '00'.$total_today_guest;
            } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                $pengunjung_ke = '0'.$total_today_guest;
            } else if($total_today_guest >= 100) {
                $pengunjung_ke = $total_today_guest;
            }

            $unique_id = 'V'. date('ymd') . date('His') . $pengunjung_ke . rand(1, 99);

            $fileName = $unique_id.'-'.time(); //generating unique file name

            $photoName = '';
            if( !empty($request['photo']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                $request['photo'] = str_replace(' ', '+', $request['photo']);
                $photoName = $fileName.'_vehicle.'.$tamu_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
            }

            Kunjungan::create([
                "id_satker" => $request["id_satker"],
                "no_kendaraan" => $request["no_kendaraan"],
                "photo_kendaraan" => 'img/foto/'.$photoName,
                "unique_id" => $unique_id,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now()
            ]);

            //pasang notif tersimpan
            Alert::success('Data Berhasil Tersimpan', 'Arahkan tamu mengunjungi PTSP untuk step selanjutnya');
            return back();
        }
        else{
            
            Alert::warning('Data Tidak Berhasil Tersimpan', 'Pastikan anda telah mengisi foto kendaraan');
            return back();
        }
        return back();
        
    }

    public function simpantamutilang(Request $request)
    {

        if( !empty($request['photo']) ){

            try {
                $url = 'https://tilang.kejaksaan.go.id/info/check_berkas/';
                $Client = new Client([
                    'form_params' => [
                        'no_tilang' => $request["nomor_berkas"]
                    ],
                    'verify' => false
                ]);

                $response = $Client->post($url);
                if($response->getStatusCode() == '200'){
                    $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
                    $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
                    $check_total_guest = Kunjungan::whereBetween(
                        'created_at', [$check_total_guest_from, $check_total_guest_to]
                    )
                    ->where('id_satker', $request["id_satker"])
                    ->get();
                    $total_today_guest = count($check_total_guest);
                    $total_today_guest = $total_today_guest + 1;
                    $pengunjung_ke = '001';
                    if($total_today_guest < 10) {
                        $pengunjung_ke = '00'.$total_today_guest;
                    } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                        $pengunjung_ke = '0'.$total_today_guest;
                    } else if($total_today_guest >= 100) {
                        $pengunjung_ke = $total_today_guest;
                    }

                    $unique_id = 'V'. date('ymd') . date('His') . $pengunjung_ke . rand(1, 99);

                    $fileName = $unique_id.'-'.time(); //generating unique file name

                    $photoName = '';
                    if( !empty($request['photo']) ) {
                        preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                        $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                        $request['photo'] = str_replace(' ', '+', $request['photo']);
                        $photoName = $fileName.'_vehicle.'.$tamu_photo_extension[1]; //generating unique file name
                        File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
                    }

                    KunjunganTilang::create([
                        "id_satker" => $request["id_satker"],
                        "guest_no" => $pengunjung_ke,
                        "nama_tamu" => $request["nama_tamu"],
                        "alamat" => $request["alamat"],
                        "nomor_berkas_tilang" => $request["nomor_berkas"],
                        "photo" => 'img/foto/'.$photoName,
                        "unique_id" => $unique_id,
                        'created_at'        => Carbon::now(),
                        'updated_at'        => Carbon::now()
                    ]);


                    $kunjungan_baru = KunjunganTilang::select('*')
                    ->where('nama_tamu', $request["nama_tamu"])
                    ->orderByRaw('id DESC')
                    ->first();

                    //generate QR 
                    $qr_image = 'img/qr/'.$fileName.'.png';
                    QrCode::format('png')
                    ->size(300)
                    ->margin(3)
                    ->generate($unique_id, '../public/'.$qr_image);
                    
                    //update QR
                    KunjunganTilang::where('id', $kunjungan_baru['id'])->update(
                        array(
                            'qr_image' => $qr_image
                        )
                    );

                    $result = json_decode($response->getBody()->getContents(), true);


                    foreach($result as $result){
                    KunjunganTilangDetail::insert(
                        [
                            'id' => $result['id'],
                            'alamat' => $result['alamat'],
                            'bb' => $result['bb'],
                            'bp' => $result['bp'],
                            'denda' => $result['denda'],
                            'form' => $result['form'],
                            'kode_ins' => $result['kode_ins'],
                            'nama' => $result['nama'],
                            'no_briva' => $result['no_briva'],
                            'no_ranmor' => $result['no_ranmor'],
                            'no_reg_tilang' => $result['no_reg_tilang'],
                            'pasal' => $result['pasal'],
                            'subsider' => $result['subsider'],
                            'tgl_bayar' => $result['tgl_bayar'],
                            'tgl_sidang' => $result['tgl_sidang'],
                            'uang_titipan' => $result['uang_titipan'],
                            'tgl_ambil' => $result['tgl_ambil'],
                            'nama_petugas' => $result['nama_petugas'],
                            'jenis_kendaraan' => $result['jenis_kendaraan'],
                            'nama_hakim' => $result['nama_hakim'],
                            'nama_panitera' => $result['nama_panitera'],
                            'kunjungan_tilang_id' =>  $kunjungan_baru['id']
                            ]
                        );
                    }
                        //pasang notif tersimpan
                    Alert::success('Data Berhasil Tersimpan', 'Silahkan kunjungi Loket Tilang pada satuan kerja yang anda tuju');
                    return redirect(url('/kunjungan/tilang',$unique_id));

                }if($response->getStatusCode() == '400'){
                    Alert::warning('Data Tidak Berhasil Tersimpan', 'Pastikan Nomor Berkas Tilang benar');
                    return back();
                }else{
                    Alert::warning('Data Tidak Berhasil Tersimpan', 'Pastikan Nomor Berkas Tilang benar');
                    return back();
                }
            
            }catch(Exception $e){$e ='error';}   
        }
        else{
            
            Alert::warning('Data Tidak Berhasil Tersimpan', 'Pastikan anda telah mengisi foto dan memastikan inputan terisi semua dengan benar');
            return back();
        }
        return back();
        
    }

    



    public function simpantamu(Request $request)
    {

        $tanggal_hari_ini = Carbon::today()->toDateString();

        // $token = "KLMFTY42H4P3RsMG37gm51yvoA5iLxG7PBd3Nrqtc6b839kwb9";
        // $myArr = array();
        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => 'https://app.ruangwa.id/api/info',
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => '',
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 1,
        //     CURLOPT_FOLLOWLOCATION => true,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => 'POST',
        //     CURLOPT_POSTFIELDS => 'token='.$token.'&username=kejaksaandev',
        // ));
        // $response = curl_exec($curl);
        // $responWa = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // curl_close($curl);
        // if($responWa == 200){
        //     $data_token = json_decode($response);    
        //     $myArr = array();
        //     $checkNumber =  $data_token->devices;
        //     foreach($checkNumber as $cn){
        //         $key_wa = $cn->token;
        //         array_push($myArr, $key_wa);
        //         shuffle($myArr);
        //         $number_key = $myArr[0]; 
        //     }   
        // }
        $data_pegawai = Pegawai::where("id",$request["id_pegawai"])->first();
        $satker = Satker::where('id_satker',$request["id_satker"])->select('nama_satker','parent_id','kode_satker')->first();

        

        
        // $url = 'http://43.231.129.16/absen/api/absen/cek_absen/'.$data_pegawai->nip;
        
        // $Client = new Client(['headers' => [
        //     'x-api-version' => '2',
        //     'Accept'        => 'application/json'
        // ]]);
        // $response = $Client->get($url);
        // $result_api = json_decode($response->getBody()->getContents(), true);

        // if($result_api['status_code'] != 200){
        //     return view('frontend.tidak-tersedia');
        // }

        if( $request['status'] == 9 and !empty($request['photo']) ){
            $check_tamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->count();
            $check_tamu_pegawai = Pegawai::whereRaw('status = 1')
            ->where('nik', $request['tamu_no_identitas'])
            ->first();

            $jumlah_tamu = $request["jumlah_tamu"];

            if($jumlah_tamu == null){
                $jumlah_tamu_new = 1;
            }else{
                $jumlah_tamu_x = $request["jumlah_tamu"];
                $jumlah_tamu_new = $jumlah_tamu_x + 1;
            }

            if(empty($check_tamu_pegawai)) {
                $tipe_tamu = $request["tamu_tipe_tamu"];
                
            }else{
                $tipe_tamu = 7;
            }

            if($check_tamu >= 1){
                Tamu::where('no_identitas', $request->tamu_no_identitas)
                ->update([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "no_hp" => $request["tamu_no_hp"],
                    "is_anak" => '2',
                    "tipe_tamu" => $tipe_tamu,
                    'updated_at'        => Carbon::now()
                ]);
            }else {
                Tamu::create([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "is_anak" => '2',
                    "status_blacklist" => '5',
                    "tipe_tamu" => $tipe_tamu,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);
            }
            //pengambilan data tamu yang sudah ada di db
            $datatamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();

            //file directory
            $slugnama = str_replace(" ","-",$request["tamu_nama"]);
            $fileName = $request["tamu_tipe_identitas"].$request["tamu_no_identitas"].$slugnama.'-'.time(); //generating unique file name
            $fileDirectory = public_path('img').'/foto/'; // directory of uploaded file
            if (! File::isDirectory($fileDirectory)) {
                File::makeDirectory($fileDirectory, 0755, true);
            }

            $qrCodeDirectory = public_path('img').'/qr/'; // directory of qrcode file
            if (! File::isDirectory($qrCodeDirectory)) {
                File::makeDirectory($qrCodeDirectory, 0755, true);
            }

            //upload foto tamu
            $photoName = '';
            if( !empty($request['photo']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                $request['photo'] = str_replace(' ', '+', $request['photo']);
                $photoName = $fileName.'_photo.'.$tamu_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
            }

            //upload foto kendaraan
            $vehiclePhotoName = '';
            if( !empty($request['photo_kendaraan']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo_kendaraan'], $vehicle_photo_extension); // extract the image extension
                $request['photo_kendaraan'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo_kendaraan']); // remove the type part
                $request['photo_kendaraan'] = str_replace(' ', '+', $request['photo_kendaraan']);
                $vehiclePhotoName = $fileName.'_vehicle.'.$vehicle_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$vehiclePhotoName, base64_decode($request['photo_kendaraan']));
            }

            //nomor antrian
            $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
            $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
            $check_total_guest = Kunjungan::whereBetween(
                'created_at', [$check_total_guest_from, $check_total_guest_to]
            )
            ->where('id_satker', $request["id_satker"])
            ->get();
            $total_today_guest = count($check_total_guest);
            $total_today_guest = $total_today_guest + 1;
            $pengunjung_ke = '001';
            if($total_today_guest < 10) {
                $pengunjung_ke = '00'.$total_today_guest;
            } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                $pengunjung_ke = '0'.$total_today_guest;
            } else if($total_today_guest >= 100) {
                $pengunjung_ke = $total_today_guest;
            }
            $unique_id = 'V'. date('ymd') . date('His') . $pengunjung_ke . rand(1, 99);

            //QR Expired TIME
            $qr_expired_time = date('Y-m-d H:i:s', strtotime('+24 hours'));

            if($datatamu->status_blacklist == 1) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu blacklist',
                        'datas' => []
                    ),
                    400
                );
            }
            elseif($datatamu->status_blacklist == 2) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu ancaman',
                        'datas' => []
                    ),
                    400
                );
            }else{
                $pegawai = Pegawai::where('id',$request["id_pegawai"])->select('name','jabatan')->first();
                if($request["nama_tahanan2"] != null){
                    $nama_tahanan = $request["nama_tahanan2"];
                }else{
                    $nama_tahanan = $request["nama_tahanan"];
                }
                Kunjungan::create([
                    "id_tamu" => $datatamu->id_tamu,
                    "id_satker" => $request["id_satker"],
                    "kode_satker" => $satker->kode_satker,
                    "id_organisasi" => $request["id_organisasi"],
                    "id_pegawai" => $request["id_pegawai"],
                    "no_berkas" => $request["no_berkas"],
                    "detail" => $request["kunjungan_detail"],
                    "guest_no" => $pengunjung_ke,
                    "tanggal_expired" => $qr_expired_time,
                    "photo" => 'img/foto/'.$photoName,
                    "photo_kendaraan" => 'img/foto/'.$vehiclePhotoName,
                    "qr_image" => "img/qr/",
                    "jumlah_tamu" => $jumlah_tamu_new,
                    "status" => '9',
                    "tipe_tamu" => $tipe_tamu,
                    "unique_id" => $unique_id,
                    "nama" => $request["tamu_nama"],
                    "tipe_pelayanan" => $request["status"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "no_kendaraan" => $request["no_kendaraan"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "barang_bawaan" => $request["barang_bawaan"],
                    "barang_ditinggal" => $request["barang_ditinggal"],
                    "nama_tahanan" => $nama_tahanan,
                    "no_hp" => $request["tamu_no_hp"],
                    "name" => $pegawai->name,
                    "jabatan" => $pegawai->jabatan,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);

                //cari id untuk Update QR
                $kunjungan_baru = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                //generate QR 
                $qr_image = 'img/qr/'.$fileName.'.png';
                QrCode::format('png')
                ->size(300)
                ->margin(3)
                ->generate($unique_id, '../public/'.$qr_image);
                
                //update QR
                Kunjungan::where('id', $kunjungan_baru['id'])->update(
                    array(
                        'qr_image' => $qr_image
                    )
                );

                foreach($request->input('nama_pengikut') as $key => $value) {
                    TamuPengikut::create([
                        'nama_pengikut'=>$value,
                        'id_kunjungan'=> $kunjungan_baru['id'],
                        'urutan_tamu'=> $key+1
                    ]);
                }

                foreach($request->input('jenis_kelamin_pengikut') as $key => $value) {
                    TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                    ->where('urutan_tamu',$key+1)
                    ->update(['jenis_kelamin'=>$value]);
                }

                foreach($request->input('is_anak') as $key => $value) {
                    TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                    ->where('urutan_tamu',$key+1)
                    ->update(['is_anak'=>$value]);
                }
            
            }

            $data_personal = 
            DB::table('kunjungan as k')
            ->join('tamu as t','k.id_tamu','=','t.id_tamu')
            ->join('pegawai as p','p.id','=','k.id_pegawai')
            ->join('satker as s','s.id_satker','=','k.id_satker')
            ->select('k.id as kunjungan_id','s.nama_satker as nama_satker_kunjungan', 's.slug as slug_satker', 'k.in as waktu_checkin', 'k.created_at as tanggal_kunjungan','p.nip as nip_pegawai', 'k.id_tamu as id_tamu','k.unique_id as unique_id', 'k.jumlah_tamu as jumlah_tamu',DB::raw('CASE WHEN k.tipe_tamu = 1 THEN "TAMU BIASA" WHEN k.tipe_tamu = 2 THEN "SAKSI" WHEN k.tipe_tamu = 3 THEN "TERSANGKA/TERDAKWA" WHEN k.tipe_tamu = 4 THEN "AHLI" WHEN k.tipe_tamu = 5 THEN "TAMU VVIP" ELSE "TIDAK DIKETAHUI" END as tipe_tamu'), DB::raw('CASE WHEN t.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'), 't.nama as nama_tamu',  't.no_identitas as ktp_tamu', 't.no_polisi as nomor_kendaraan_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo) as foto_tamu'), DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo_kendaraan) as foto_kendaraan'), 'k.in as jam_checkin', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
            ->orderBy('k.created_at','desc')
            ->where('k.unique_id', $unique_id)
            ->first();

            $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
            $dataPtsp = [
                'uuid'  => $datatamu->uuid,
                'nik_type'  => '1',
                'nik'     => $request["tamu_no_identitas"],
                'name'   => $request["tamu_nama"],
                'kode_satker'      => $satker->kode_satker,
                'type'       => '1',
                'email'      => $request["tamu_email"],
                'address'      => $request["tamu_alamat"],
                'phone'      => $request["tamu_no_hp"],
                'plat_kendaraan'      => $request["tamu_no_polisi"],
                'status'      => "1",
                'tujuan'      => $request["kunjungan_detail"],
                'foto_kendaraan'      => 'img/foto/'.$vehiclePhotoName,
                'foto_diri'      => 'img/foto/'.$photoName,
                'jenis_kelamin'      => $request["jenis_kelamin"],
                'unique_key'      => $unique_id,
            ];

            // return $dataPtsp;

            $apiPtsp = 'https://pelayanan.kejaksaan.go.id/api/simpan-tamu';
            $ClientPtsp = new Client([
                'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                'verify' => false
            ]);
            // return $dataPtsp;
            $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                'form_params' => $dataPtsp
            ]);

            $crypt = "https://pelayanan.kejaksaan.go.id/?uuid=".Crypt::encrypt($datatamu->uuid);
            
            $url = 
            DB::table('kunjungan as k')
            ->join('satker as s','s.id_satker','=','k.id_satker')
            ->join('tamu as t','t.id_tamu','=','k.id_tamu')
            ->select( DB::raw("CONCAT('https://pelayanan.kejaksaan.go.id/?uuid=',t.uuid) AS url_survey"))
            ->orderBy('k.created_at','desc')
            ->where('k.unique_id', $unique_id)
            ->first();

            $numberKeys = array(
                'helpdesk1',
                'helpdesk2',
                'helpdesk4'
                
            );
    
            $randomNumber = array_rand($numberKeys);
            $numberKey = $numberKeys[$randomNumber];
    
            
            $number_tamu = $request['no_hp'];
            $file_tamu =  "https://bukutamu.kejaksaan.go.id/".$qr_image;
            $caption_tamu = "Selamat datang di Pelayanan Terpadu Kejaksaan Republik Indonesia"."\n\nHalo ".$request->nama."\n\nTerimakasih telah melakukan pendaftaran pelayanan pada sistem Pelayanan Terpadu Kejaksaan\n\n Simpan QR Code yang dikirimkan oleh Whatsapp ini sebagai tanda pengenal anda pada sistem Pelayanan Terpadu Kejaksaan";;
            // $isi_post_tamu = 'token='.$number_key.'&number='.$number_tamu.'&file='.$file_tamu.'&caption='.$caption_tamu;
            $isi_post_tamu = [
                'sessions'=> $numberKey,
                'target'=> $number_tamu,
                'message'=> $caption_tamu,
                'url'=> $file_tamu
            ];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $isi_post_tamu,
            ));
    
            try {
                $lapor_tamu_kunjungan = curl_exec($curl);
            } catch (Exception $e) {
                    // do nothing, the timeout exception is intended
            }
            curl_close($curl);

            return view('frontend.direct', compact('url','crypt'));

        }

        elseif( !empty($request['photo']) ){
            //pengecekan tamu sudah ada di db atau belum
            $check_tamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->count();
            // $check_tamu_data = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();
            $check_tamu_pegawai = Pegawai::whereRaw('status = 1')
            ->where('nik', $request['tamu_no_identitas'])
            ->first();


            $jumlah_tamu = $request["jumlah_tamu"];

            if($jumlah_tamu == null){
                $jumlah_tamu_new = 1;
            }else{
                $jumlah_tamu_x = $request["jumlah_tamu"];
                $jumlah_tamu_new = $jumlah_tamu_x + 1;
            }

            if(empty($check_tamu_pegawai)) {
                $tipe_tamu = $request["tamu_tipe_tamu"];
                
            }else{
                $tipe_tamu = 7;
            }

            if($check_tamu >= 1){
                Tamu::where('no_identitas', $request->tamu_no_identitas)
                ->update([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "no_hp" => $request["tamu_no_hp"],
                    "is_anak" => '2',
                    "tipe_tamu" => $tipe_tamu,
                    'updated_at'        => Carbon::now()
                ]);
            }else {
                Tamu::create([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "is_anak" => '2',
                    "status_blacklist" => '5',
                    "tipe_tamu" => $tipe_tamu,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);
            }
            //pengambilan data tamu yang sudah ada di db
            $datatamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();

            //file directory
            $slugnama = str_replace(" ","-",$request["tamu_nama"]);
            $fileName = $request["tamu_tipe_identitas"].$request["tamu_no_identitas"].$slugnama.'-'.time(); //generating unique file name
            $fileDirectory = public_path('img').'/foto/'; // directory of uploaded file
            if (! File::isDirectory($fileDirectory)) {
                File::makeDirectory($fileDirectory, 0755, true);
            }

            $qrCodeDirectory = public_path('img').'/qr/'; // directory of qrcode file
            if (! File::isDirectory($qrCodeDirectory)) {
                File::makeDirectory($qrCodeDirectory, 0755, true);
            }

            //upload foto tamu
            $photoName = '';
            if( !empty($request['photo']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                $request['photo'] = str_replace(' ', '+', $request['photo']);
                $photoName = $fileName.'_photo.'.$tamu_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
            }

            //upload foto kendaraan
            $vehiclePhotoName = '';
            if( !empty($request['photo_kendaraan']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo_kendaraan'], $vehicle_photo_extension); // extract the image extension
                $request['photo_kendaraan'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo_kendaraan']); // remove the type part
                $request['photo_kendaraan'] = str_replace(' ', '+', $request['photo_kendaraan']);
                $vehiclePhotoName = $fileName.'_vehicle.'.$vehicle_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$vehiclePhotoName, base64_decode($request['photo_kendaraan']));
            }

            //nomor antrian
            $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
            $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
            $check_total_guest = Kunjungan::whereBetween(
                'created_at', [$check_total_guest_from, $check_total_guest_to]
            )
            ->where('id_satker', $request["id_satker"])
            ->get();
            $total_today_guest = count($check_total_guest);
            $total_today_guest = $total_today_guest + 1;
            $pengunjung_ke = '001';
            if($total_today_guest < 10) {
                $pengunjung_ke = '00'.$total_today_guest;
            } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                $pengunjung_ke = '0'.$total_today_guest;
            } else if($total_today_guest >= 100) {
                $pengunjung_ke = $total_today_guest;
            }
            $unique_id = 'V'. date('ymd') . date('His') . $pengunjung_ke . rand(1, 99);
            

            //QR Expired TIME
            $qr_expired_time = date('Y-m-d H:i:s', strtotime('+24 hours'));

            $massgate_id = $unique_id;

            $requestMassgateBody = [
                'visitor_id' => $massgate_id,
                'full_name' => $request['tamu_nama'],
                'nik_number' => $request['tamu_no_identitas'],
                'phone_no' => $request['tamu_no_hp'],
                'visit_purpose' => $request['kunjungan_detail'],
                'address' => $request['tamu_alamat'],
                'qr_code_data' => $unique_id,
                'enrolled_time' => date('Y-m-d H:i:s'),
                'qr_expiry_time' => $qr_expired_time,
                'category' => '1',
            ];


            if($datatamu->status_blacklist == 1) {
                $requestMassgateBody['category'] = 'Blacklist';
            }

            if($datatamu->status_blacklist == 2) {
                $requestMassgateBody['category'] = 'Threat';
            }


            
            $selected_satker = Satker::where('id_satker', $request['id_satker'])->first();
            if($selected_satker->kode_satker == '00'){
                $url_massgate = 'http://172.16.3.107/massgate/public/visitor_enrolment';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($requestMassgateBody),
                    'verify' => false
                ]);
                try {
                    $post_massgate = $Client->post($url_massgate, ['timeout' => 5]);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
            }

            
           


            if($datatamu->status_blacklist == 1) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu blacklist',
                        'datas' => []
                    ),
                    400
                );
            }
            elseif($datatamu->status_blacklist == 2) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu ancaman',
                        'datas' => []
                    ),
                    400
                );
            }else{
                $pegawai = Pegawai::where('id',$request["id_pegawai"])->select('name','jabatan')->first();
                if($request["nama_tahanan2"] != null){
                    $nama_tahanan = $request["nama_tahanan2"];
                }else{
                    $nama_tahanan = $request["nama_tahanan"];
                }
                Kunjungan::create([
                    "id_tamu" => $datatamu->id_tamu,
                    "id_satker" => $request["id_satker"],
                    "id_organisasi" => $request["id_organisasi"],
                    "id_pegawai" => $request["id_pegawai"],
                    "detail" => $request["kunjungan_detail"],
                    "no_berkas" => $request["no_berkas"],
                    "tipe_pelayanan" => $request["status"],
                    "guest_no" => $pengunjung_ke,
                    "tanggal_expired" => $qr_expired_time,
                    "photo" => 'img/foto/'.$photoName,
                    "photo_kendaraan" => 'img/foto/'.$vehiclePhotoName,
                    "qr_image" => "img/qr/",
                    "jumlah_tamu" => $jumlah_tamu_new,
                    "tipe_tamu" => $tipe_tamu,
                    "unique_id" => $unique_id,
                    "massgate_id" => $massgate_id,
                    "barang_bawaan" => $request["barang_bawaan"],
                    "barang_ditinggal" => $request["barang_ditinggal"],
                    "nama_tahanan" => $nama_tahanan,
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "no_kendaraan" => $request["no_kendaraan"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "name" => $pegawai->name,
                    "jabatan" => $pegawai->jabatan,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);

                //cari id untuk Update QR
                $kunjungan_baru = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                //generate QR 
                $qr_image = 'img/qr/'.$fileName.'.png';
                QrCode::format('png')
                ->size(300)
                ->margin(3)
                ->generate($unique_id, '../public/'.$qr_image);
                
                //update QR
                Kunjungan::where('id', $kunjungan_baru['id'])->update(
                    array(
                        'qr_image' => $qr_image
                    )
                );
                if($request['nama_pengikut'][0] != null){
                    foreach($request->input('nama_pengikut') as $key => $value) {
                        TamuPengikut::create([
                            'nama_pengikut'=>$value,
                            'id_kunjungan'=> $kunjungan_baru['id'],
                            'urutan_tamu'=> $key+1
                        ]);
                    }
    
                    foreach($request->input('jenis_kelamin_pengikut') as $key => $value) {
                        TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                        ->where('urutan_tamu',$key+1)
                        ->update(['jenis_kelamin'=>$value]);
                    }
    
                    foreach($request->input('is_anak') as $key => $value) {
                        TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                        ->where('urutan_tamu',$key+1)
                        ->update(['is_anak'=>$value]);
                    }
                }
                

                

                //create data for WA

                $tipe_nik = '';
                if($request["tamu_tipe_identitas"] == '1'){$tipe_nik = 'KTP';}
                elseif($request["tamu_tipe_identitas"] == '2'){$tipe_nik = 'SIM';}
                else{$tipe_nik = 'Passport';}
                
                $selected_bidang = "Kejaksaan";

                $selected_jabatan = 
                DB::table('pegawai as p')
                ->select('p.jabatan as nama_jabatan')
                ->where('p.id', $request['id_pegawai'])
                ->first();


                
                
                $nama_pegawai_tujuan = $data_pegawai->name;
                $nama_jabatan_tujuan = $selected_jabatan->nama_jabatan;
                $nama_satker_tujuan = $selected_satker->nama_satker;
                // $nama_bidang_tujuan = $selected_bidang->nama_bidang;
                $nama_bidang_tujuan = $selected_bidang;

                $whatsapp_pegawai = $data_pegawai->no_hp;
                if (substr($whatsapp_pegawai, 0, 1) === '0') {
                    $whatsapp_pegawai = '62'.substr($whatsapp_pegawai, 1);
                } else if (substr($whatsapp_pegawai, 0, 1) === '+') {
                    $whatsapp_pegawai = substr($whatsapp_pegawai, 1);
                }

                $data_kunjungan = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                

                // INPUT 
                
                $numberKeys = array(
                    'helpdesk1',
                    'helpdesk2',
                    'helpdesk4'
                    
                );

                $randomNumber = array_rand($numberKeys);
                $numberKey = $numberKeys[$randomNumber];

                $number = $whatsapp_pegawai;
                $file = "https://bukutamu.kejaksaan.go.id/".$data_kunjungan->photo;
                $caption = "Salam sehat dan sukses selalu bapak/ibu ".$nama_pegawai_tujuan."\n\nMohon izin sebelumnya whatsapp ini memberikan notifikasi ke bapak/ibu tentang tamu yang akan menemui bapak/ibu\n\nBerikut biodata tamu yang ingin menemui bapak/ibu\n\nNama : ".$request['tamu_nama']."\nDengan Keperluan : ".$data_kunjungan->detail."\n\nPesan ini langsung tergenerate dari Aplikasi Bukutamu Kejaksaan\n\nTerimakasih Bapak/Ibu";
                // $isi_post = 'token='.$number_key.'&number='.$number.'&file='.$file.'&caption='.$caption;
                $isi_post = [
                    'sessions'=> $numberKey,
                    'target'=> $number,
                    'message'=> $caption,
                    'url'=> $file
                ];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 1,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $isi_post,
                ));
                try {
                    $lapor_tamu_kunjungan = curl_exec($curl);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($curl);
                

                $data_kunjungan = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                $number_tamu = $request['tamu_no_hp'];
                $file_tamu =  "https://bukutamu.kejaksaan.go.id/".$data_kunjungan->qr_image;
                $caption_tamu = "Selamat datang di ".$nama_satker_tujuan."\nHalo ".$request['tamu_nama']."\n\nAnda telah melakukan permintaan untuk bertemu dengan : ".$nama_pegawai_tujuan." jabatan (".$nama_jabatan_tujuan.") dengan keperluan ".$data_kunjungan->detail."\n\nNo Antrian Anda : ".$data_kunjungan->guest_no."\n\nPesan ini dari Aplikasi Bukutamu Kejaksaan\nTerimakasih";
                // $isi_post_tamu = 'token='.$number_key.'&number='.$number_tamu.'&file='.$file_tamu.'&caption='.$caption_tamu;
                $isi_post_tamu = [
                    'sessions'=> $numberKey,
                    'target'=> $number_tamu,
                    'message'=> $caption_tamu,
                    'url'=> $file_tamu
                ];
                
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 1,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $isi_post_tamu,
                ));
                try {
                    $lapor_tamu_kunjungan = curl_exec($curl);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($curl);

                $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
                $dataPtsp = [
                    'nik_type'  => '1',
                    'uuid'  => $datatamu->uuid,
                    'nik'     => $request["tamu_no_identitas"],
                    'name'   => $request["tamu_nama"],
                    'kode_satker'      => $satker->kode_satker,
                    'type'       => '1',
                    'email'      => $request["tamu_email"],
                    'address'      => $request["tamu_alamat"],
                    'phone'      => $request["tamu_no_hp"],
                    'plat_kendaraan'      => $request["tamu_no_polisi"],
                    'status'      => "1",
                    'tujuan'      => $request["kunjungan_detail"],
                    'foto_kendaraan'      => 'img/foto/'.$vehiclePhotoName,
                    'foto_diri'      => 'img/foto/'.$photoName,
                    'jenis_kelamin'      => $request["jenis_kelamin"],
                    'unique_key'      => $unique_id,
                ];

                // return $dataPtsp;

                $apiPtsp = 'https://pelayanan.kejaksaan.go.id/api/simpan-tamu';
                $ClientPtsp = new Client([
                    'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                    'verify' => false
                ]);
                // return $dataPtsp;
                $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                    'form_params' => $dataPtsp
                ]);

                // $apiPtsp = 'https://ptsp-sumut.kejaksaanri.id/api/simpan-tamu';
                // $ClientPtsp = new Client([
                //     'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                //     'verify' => false
                // ]);
                // return $dataPtsp;
                // $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                //     'form_params' => $dataPtsp
                // ]);

                // return $response_body = json_decode($postApiPtsp->getBody());


                $data_return = [
                    'data_tamu' => $datatamu,
                    'data_kunjungan' => $kunjungan_baru,
                    'nama_pegawai_tujuan' => $nama_pegawai_tujuan,
                    'nama_jabatan_tujuan' => $nama_jabatan_tujuan,
                    'nama_satker_tujuan' => $nama_satker_tujuan,
                    'nama_bidang_tujuan' => $nama_bidang_tujuan,
                ];

                
                $tanggal_hari_ini = Carbon::today()->toDateString();

                $tracingId = Str::uuid();
                $eventCode = 'TAMU_KNJGN';
                $id_satker = $request["id_satker"];
                $nama_satker = $satker->nama_satker;
                $parent_satker = $satker->parent_id;
                $kode_satker = $satker->kode_satker;
                $tanggal = $tanggal_hari_ini;
                $bulan = substr($tanggal_hari_ini,0,7);
                $total_kendaraan = 0;

                $summaryData = Array();
                $summaryData["tracingId"] = $tracingId;
                $summaryData["eventCode"] = $eventCode;
                $summaryData["summaryData"]["id_satker"] = $id_satker;
                $summaryData["summaryData"]["nama_satker"] = $nama_satker;
                $summaryData["summaryData"]["parent_satker"] = $parent_satker;
                $summaryData["summaryData"]["kode_satker"] = $kode_satker;
                $summaryData["summaryData"]["tanggal"] = $tanggal;
                $summaryData["summaryData"]["bulan"] = $bulan;
                $summaryData["summaryData"]["total_kendaraan"] = $total_kendaraan;
                
                $url = '43.231.129.14:8081/api/v1/realtime-event/create';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($summaryData),
                    'verify' => false
                ]);

                try {
                    $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                    DB::table('realtime_api')->insert(
                        array(
                            "tracingId" => $tracingId,
                            "eventCode" => $eventCode,
                            "summaryData" => json_encode($summaryData),
                            "created_at" => Carbon::now(),
                            "updated_at" => Carbon::now(),
                        )
                   );
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }  
                
                
                $tracingId2 = Str::uuid();
                $eventCode2 = 'TAMU_KNJGN_TAMU';
                $nama_satker = $satker->nama_satker;
                $parent_satker = $satker->parent_id;
                $kode_satker = $satker->kode_satker;
                $tanggal = $tanggal_hari_ini;
                $bulan = substr($tanggal_hari_ini,0,7);
                $total_kendaraan = 0;
                $master_tipe_tamu = DB::table('master_tipe_tamu')->where('id',$tipe_tamu)->first();
                $cari_anak = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('is_anak','1')->get());
                $cari_dewasa = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('is_anak','2')->get()) + 1;
                $cari_laki = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('jenis_kelamin','1')->get()) + count(Kunjungan::join('tamu','tamu.id_tamu','kunjungan.id_tamu')->where('kunjungan.id',$kunjungan_baru['id'])->where('tamu.jenis_kelamin','1')->get());
                $cari_perempuan = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('jenis_kelamin','2')->get()) + count(Kunjungan::join('tamu','tamu.id_tamu','kunjungan.id_tamu')->where('kunjungan.id',$kunjungan_baru['id'])->where('tamu.jenis_kelamin','2')->get());


                $summaryData2 = Array();
                $summaryData2["tracingId"] = $tracingId2;
                $summaryData2["eventCode"] = $eventCode2;
                $summaryData2["summaryData"]["id_satker"] = $id_satker;
                $summaryData2["summaryData"]["nama_satker"] = $nama_satker;
                $summaryData2["summaryData"]["parent_satker"] = $parent_satker;
                $summaryData2["summaryData"]["kode_satker"] = $kode_satker;
                $summaryData2["summaryData"]["tanggal"] = $tanggal;
                $summaryData2["summaryData"]["bulan"] = $bulan;
                $summaryData2["summaryData"]["id_jenis_tamu"] = $tipe_tamu;
                $summaryData2["summaryData"]["nama_jenis_tamu"] = $master_tipe_tamu->nama;
                $summaryData2["summaryData"]["total_tamu"] = $jumlah_tamu_new;
                $summaryData2["summaryData"]["total_anak"] = $cari_anak;
                $summaryData2["summaryData"]["total_dewasa"] = $cari_dewasa;
                $summaryData2["summaryData"]["total_laki"] = $cari_laki;
                $summaryData2["summaryData"]["total_perempuan"] = $cari_perempuan;
                
                $url = '43.231.129.14:8081/api/v1/realtime-event/create';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($summaryData2),
                    'verify' => false
                ]);

                try {
                    $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                    DB::table('realtime_api')->insert(
                        array(
                            "tracingId" => $tracingId2,
                            "eventCode" => $eventCode2,
                            "summaryData" => json_encode($summaryData2),
                            "created_at" => Carbon::now(),
                            "updated_at" => Carbon::now(),
                        )
                   );
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }   
                
                $tokenekin = '$2y$10$K0xXdngm7c0GvChV0O5q1eA85juNQ3S3NZ1wmZXFJazOnffWnbICm';
                // $url = 'https://mysimkari.kejaksaan.go.id/api/ekinerja';
                $url = '172.16.2.101/api/ekinerja';
                $headers = array('Authorization: Bearer '.$tokenekin, 'Content-Type: multipart/form-data');

                $data_personal = 
                DB::table('kunjungan as k')
                ->join('tamu as t','k.id_tamu','=','t.id_tamu')
                ->join('pegawai as p','p.id','=','k.id_pegawai')
                ->join('satker as s','s.id_satker','=','k.id_satker')
                ->select('k.id as kunjungan_id','s.nama_satker as nama_satker_kunjungan', 's.slug as slug_satker', 'k.in as waktu_checkin', 'k.created_at as tanggal_kunjungan','p.nip as nip_pegawai', 'k.id_tamu as id_tamu','k.unique_id as unique_id', 'k.jumlah_tamu as jumlah_tamu',DB::raw('CASE WHEN k.tipe_tamu = 1 THEN "TAMU BIASA" WHEN k.tipe_tamu = 2 THEN "SAKSI" WHEN k.tipe_tamu = 3 THEN "TERSANGKA/TERDAKWA" WHEN k.tipe_tamu = 4 THEN "AHLI" WHEN k.tipe_tamu = 5 THEN "TAMU VVIP" ELSE "TIDAK DIKETAHUI" END as tipe_tamu'), DB::raw('CASE WHEN t.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'), 't.nama as nama_tamu',  't.no_identitas as ktp_tamu', 't.no_polisi as nomor_kendaraan_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo) as foto_tamu'), DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo_kendaraan) as foto_kendaraan'), 'k.in as jam_checkin', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
                ->orderBy('k.created_at','desc')
                ->where('k.unique_id', $unique_id)
                ->first();
                $post = [
                    'nip' => $data_personal->nip_pegawai,
                    'sasaran_kinerja' => 'Penerimaan Tamu ('.$data_personal->tipe_tamu.')',
                    'desc_kinerja'   => 'Penerimaan Tamu ('.$data_personal->tipe_tamu.') Atas Nama '.$data_personal->nama_tamu.' pada tanggal '.$data_personal->tanggal_kunjungan.' Pada Satuan Kerja '.$data_personal->nama_satker_kunjungan,
                    'lokasi_kegiatan' => $data_personal->nama_satker_kunjungan,
                    'point' => 1,
                    'nama_app' => 'BUKU TAMU',
                    'url_file' => $data_personal->foto_tamu,
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS,$post); 
                try {
                   $response = curl_exec($ch);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($ch);   
            }
            // $this->kunjungan($unique_id);
            Alert::success('Berhasil menyimpan data', 'Silahkan lakukan checkin');
            return redirect(url('/kunjungan/view',$unique_id));
            
        }else{
            Alert::warning('Data Tidak Berhasil Tersimpan', 'Pastikan anda telah mengisi foto');
            return back();
            
        }
        
    }
    


    public function simpandataptsp(Request $request)
    {

        $token = "KLMFTY42H4P3RsMG37gm51yvoA5iLxG7PBd3Nrqtc6b839kwb9";
        $satker = Satker::where('id_satker',$request["id_satker"])->select('kode_satker')->first();

        $data_pegawai = Pegawai::where("id",$request["id_pegawai"])->first();
        if( !empty($request['photo']) ){
            //pengecekan tamu sudah ada di db atau belum
            $check_tamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->count();
            // $check_tamu_data = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();
 

            $jumlah_tamu = $request["jumlah_tamu"];

            if($jumlah_tamu == null){
                $jumlah_tamu_new = 1;
            }else{
                $jumlah_tamu_x = $request["jumlah_tamu"];
                $jumlah_tamu_new = $jumlah_tamu_x + 1;
            }

            if($check_tamu >= 1){
                Tamu::where('no_identitas', $request->tamu_no_identitas)
                ->update([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "no_hp" => $request["tamu_no_hp"],
                    "is_anak" => '2',
                    "tipe_tamu" => 1,
                    'updated_at'        => Carbon::now()
                ]);
            }else {
                Tamu::create([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "is_anak" => '2',
                    "status_blacklist" => '5',
                    "tipe_tamu" => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);
            }
            //pengambilan data tamu yang sudah ada di db
            $datatamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();

            //file directory
            $slugnama = str_replace(" ","-",$request["tamu_nama"]);
            $fileName = $request["tamu_tipe_identitas"].$request["tamu_no_identitas"].$slugnama.'-'.time(); //generating unique file name
            $fileDirectory = public_path('img').'/foto/'; // directory of uploaded file
            if (! File::isDirectory($fileDirectory)) {
                File::makeDirectory($fileDirectory, 0755, true);
            }

            $qrCodeDirectory = public_path('img').'/qr/'; // directory of qrcode file
            if (! File::isDirectory($qrCodeDirectory)) {
                File::makeDirectory($qrCodeDirectory, 0755, true);
            }

            //upload foto tamu
            $photoName = '';
            if( !empty($request['photo']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                $request['photo'] = str_replace(' ', '+', $request['photo']);
                $photoName = $fileName.'_photo.'.$tamu_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
            }

            //upload foto kendaraan
            $vehiclePhotoName = '';
            if( !empty($request['photo_kendaraan']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo_kendaraan'], $vehicle_photo_extension); // extract the image extension
                $request['photo_kendaraan'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo_kendaraan']); // remove the type part
                $request['photo_kendaraan'] = str_replace(' ', '+', $request['photo_kendaraan']);
                $vehiclePhotoName = $fileName.'_vehicle.'.$vehicle_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$vehiclePhotoName, base64_decode($request['photo_kendaraan']));
            }

            //nomor antrian
            $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
            $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
            $check_total_guest = Kunjungan::whereBetween(
                'created_at', [$check_total_guest_from, $check_total_guest_to]
            )
            ->where('id_satker', $request["id_satker"])
            ->get();
            $total_today_guest = count($check_total_guest);
            $total_today_guest = $total_today_guest + 1;
            $pengunjung_ke = '001';
            if($total_today_guest < 10) {
                $pengunjung_ke = '00'.$total_today_guest;
            } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                $pengunjung_ke = '0'.$total_today_guest;
            } else if($total_today_guest >= 100) {
                $pengunjung_ke = $total_today_guest;
            }
            $unique_id = 'V'. date('ymd') . date('His') . $pengunjung_ke . rand(1, 99);
            

            //QR Expired TIME
            $qr_expired_time = date('Y-m-d H:i:s', strtotime('+24 hours'));


            $massgate_id = $unique_id;

            $requestMassgateBody = [
                'visitor_id' => $massgate_id,
                'full_name' => $request['tamu_nama'],
                'nik_number' => $request['tamu_no_identitas'],
                'phone_no' => $request['tamu_no_hp'],
                'visit_purpose' => $request['kunjungan_detail'],
                'address' => $request['tamu_alamat'],
                'qr_code_data' => $unique_id,
                'enrolled_time' => date('Y-m-d H:i:s'),
                'qr_expiry_time' => $qr_expired_time,
                'category' => '1',
            ];


            if($datatamu->status_blacklist == 1) {
                $requestMassgateBody['category'] = 'Blacklist';
            }

            if($datatamu->status_blacklist == 2) {
                $requestMassgateBody['category'] = 'Threat';
            }

            $selected_satker = Satker::where('id_satker', $request['id_satker'])->first();

            if($request["nama_tahanan2"] != null){
                $nama_tahanan = $request["nama_tahanan2"];
            }else{
                $nama_tahanan = $request["nama_tahanan"];
            }
            if($datatamu->status_blacklist == 1) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu blacklist',
                        'datas' => []
                    ),
                    400
                );
            }
            elseif($datatamu->status_blacklist == 2) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu ancaman',
                        'datas' => []
                    ),
                    400
                );
            }else{
                $pegawai = Pegawai::where('id',$request["id_pegawai"])->select('name','jabatan')->first();
                Kunjungan::create([
                    "id_tamu" => $datatamu->id_tamu,
                    "id_satker" => $request["id_satker"],
                    "id_organisasi" => $request["id_organisasi"],
                    "id_pegawai" => $request["id_pegawai"],
                    "detail" => $request["kunjungan_detail"],
                    "tipe_pelayanan" => $request["status"],
                    "no_berkas" => $request["no_berkas"],
                    "guest_no" => $pengunjung_ke,
                    "tanggal_expired" => $qr_expired_time,
                    "photo" => 'img/foto/'.$photoName,
                    "photo_kendaraan" => 'img/foto/'.$vehiclePhotoName,
                    "qr_image" => "img/qr/",
                    "jumlah_tamu" => $jumlah_tamu_new,
                    "tipe_tamu" => 1,
                    "unique_id" => $unique_id,
                    "massgate_id" => $massgate_id,
                    "nama" => $request["tamu_nama"],
                    "barang_bawaan" => $request["barang_bawaan"],
                    "barang_ditinggal" => $request["barang_ditinggal"],
                    "nama_tahanan" => $nama_tahanan,
                    "no_identitas" => $request["tamu_no_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "no_kendaraan" => $request["no_kendaraan"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "name" => $pegawai->name,
                    "jabatan" => $pegawai->jabatan,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);

                //cari id untuk Update QR
                $kunjungan_baru = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                //generate QR 
                $qr_image = 'img/qr/'.$fileName.'.png';
                QrCode::format('png')
                ->size(300)
                ->margin(3)
                ->generate($unique_id, '../public/'.$qr_image);
                
                //update QR
                Kunjungan::where('id', $kunjungan_baru['id'])->update(
                    array(
                        'qr_image' => $qr_image
                    )
                );
                if($request['nama_pengikut'][0] != null){
                    foreach($request->input('nama_pengikut') as $key => $value) {
                        TamuPengikut::create([
                            'nama_pengikut'=>$value,
                            'id_kunjungan'=> $kunjungan_baru['id'],
                            'urutan_tamu'=> $key+1
                        ]);
                    }
    
                    foreach($request->input('jenis_kelamin_pengikut') as $key => $value) {
                        TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                        ->where('urutan_tamu',$key+1)
                        ->update(['jenis_kelamin'=>$value]);
                    }
    
                    foreach($request->input('is_anak') as $key => $value) {
                        TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                        ->where('urutan_tamu',$key+1)
                        ->update(['is_anak'=>$value]);
                    }
                }
                

                

                //create data for WA

                $tipe_nik = '';
                if($request["tamu_tipe_identitas"] == '1'){$tipe_nik = 'KTP';}
                elseif($request["tamu_tipe_identitas"] == '2'){$tipe_nik = 'SIM';}
                else{$tipe_nik = 'Passport';}
                
                $selected_bidang = "Kejaksaan";
                $nama_satker_tujuan = $selected_satker->nama_satker;


                $data_kunjungan = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                //INPUT 


                $data_kunjungan = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                $numberKeys = array(
                    'helpdesk1',
                    'helpdesk2',
                    'helpdesk4'
                    
                );

                $randomNumber = array_rand($numberKeys);
                $numberKey = $numberKeys[$randomNumber];

                $number_tamu = $request['tamu_no_hp'];
                $file_tamu =  "https://bukutamu.kejaksaan.go.id/".$data_kunjungan->qr_image;
                $caption_tamu = "Selamat datang di ".$nama_satker_tujuan."\nHalo ".$request['tamu_nama']."\n\Terimakasih telah melakukan pendaftaran pelayanan pada sistem PTSP";
                // $isi_post_tamu = 'token='.$number_key.'&number='.$number_tamu.'&file='.$file_tamu.'&caption='.$caption_tamu;
                $isi_post_tamu = [
                    'sessions'=> $numberKey,
                    'target'=> $number_tamu,
                    'message'=> $caption_tamu,
                    'url'=> $file_tamu
                ];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 1,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $isi_post_tamu,
                ));
                try {
                    $lapor_tamu_kunjungan = curl_exec($curl);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($curl);
                


                if ($request["tamu_no_polisi"] == null){
                    $nopol = "-";
                }else{
                    $nopol = $request["tamu_no_polisi"];
                }
                $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
                $dataPtsp = [
                    'nik_type'  => 1,
                    'uuid'  => $datatamu->uuid,
                    'nik'     => $request["tamu_no_identitas"],
                    'name'   => $request["tamu_nama"],
                    'kode_satker'      => $satker->kode_satker,
                    'type'       => 1,
                    'email'      => $request["tamu_email"],
                    'address'      => $request["tamu_alamat"],
                    'phone'      => $request["tamu_no_hp"],
                    'plat_kendaraan'      => $nopol,
                    'status'      => "1",
                    'tujuan'      => $request["kunjungan_detail"],
                    'foto_kendaraan'      => 'img/foto/'.$vehiclePhotoName,
                    'foto_diri'      => 'img/foto/'.$photoName,
                    'jenis_kelamin'      => $request["jenis_kelamin"],
                    'unique_key'      => $unique_id,
                ];

                $apiPtsp = 'https://pelayanan.kejaksaan.go.id/api/simpan-tamu';
                $ClientPtsp = new Client([
                    'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                    'verify' => false
                ]);
                $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                    'form_params' => $dataPtsp
                ]);

                // $apiPtsp = 'https://ptsp-sumut.kejaksaanri.id/api/simpan-tamu';
                // $ClientPtsp = new Client([
                //     'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                //     'verify' => false
                // ]);
                // $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                //     'form_params' => $dataPtsp
                // ]);


                $data_return = [
                    'data_tamu' => $datatamu,
                    'data_kunjungan' => $kunjungan_baru,
                    'nama_satker_tujuan' => $nama_satker_tujuan,
                ];

            }
            // $this->kunjungan($unique_id);
            Alert::success('Berhasil menyimpan data', 'Silahkan lakukan checkin');
            return redirect(url('/kunjungan/viewptsp',$unique_id));
            
        }else{
            Alert::warning('Data Tidak Berhasil Tersimpan', 'Pastikan anda telah mengisi foto');
            return back();
            
        }
        
    }

    public function updatetamusurat(Request $request, $unique_id)
    {
        $data_pegawai = Pegawai::where("id",$request["id_pegawai"])->first();
        
        $satker = Satker::where('id_satker',$request["id_satker"])->select('kode_satker')->first();
        
        // $url = 'http://43.231.129.16/absen/api/absen/cek_absen/'.$data_pegawai->nip;
        
        // $Client = new Client(['headers' => [
        //     'x-api-version' => '2',
        //     'Accept'        => 'application/json'
        // ]]);
        // $response = $Client->get($url);
        // $result_api = json_decode($response->getBody()->getContents(), true);

        // if($result_api['status_code'] != 200){
        //     return view('frontend.tidak-tersedia');
        // }
        
        if( $request['status'] == 9 and !empty($request['photo']) ){
            $check_tamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->count();
            // $check_tamu_data = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();
            $check_tamu_pegawai = Pegawai::whereRaw('status = 1')
            ->where('nik', $request['tamu_no_identitas'])
            ->first();

            $jumlah_tamu = $request["jumlah_tamu"];

            if($jumlah_tamu == null){
                $jumlah_tamu_new = 1;
            }else{
                $jumlah_tamu_x = $request["jumlah_tamu"];
                $jumlah_tamu_new = $jumlah_tamu_x + 1;
            }

            if(empty($check_tamu_pegawai)) {
                $tipe_tamu = $request["tamu_tipe_tamu"];
                
            }else{
                $tipe_tamu = 7;
            }

            if($check_tamu >= 1){
                Tamu::where('no_identitas', $request->tamu_no_identitas)
                ->update([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "no_hp" => $request["tamu_no_hp"],
                    "is_anak" => '2',
                    "tipe_tamu" => $tipe_tamu,
                    'updated_at'        => Carbon::now()
                ]);
            }else {
                Tamu::create([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "is_anak" => '2',
                    "status_blacklist" => '5',
                    "tipe_tamu" => $tipe_tamu,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);
            }
            //pengambilan data tamu yang sudah ada di db
            $datatamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();

            //file directory
            $slugnama = str_replace(" ","-",$request["tamu_nama"]);
            $fileName = $request["tamu_tipe_identitas"].$request["tamu_no_identitas"].$slugnama.'-'.time(); //generating unique file name
            $fileDirectory = public_path('img').'/foto/'; // directory of uploaded file
            if (! File::isDirectory($fileDirectory)) {
                File::makeDirectory($fileDirectory, 0755, true);
            }

            $qrCodeDirectory = public_path('img').'/qr/'; // directory of qrcode file
            if (! File::isDirectory($qrCodeDirectory)) {
                File::makeDirectory($qrCodeDirectory, 0755, true);
            }

            //upload foto tamu
            $photoName = '';
            if( !empty($request['photo']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                $request['photo'] = str_replace(' ', '+', $request['photo']);
                $photoName = $fileName.'_photo.'.$tamu_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
            }

            //upload foto kendaraan
            $vehiclePhotoName = '';
            if( !empty($request['photo_kendaraan']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo_kendaraan'], $vehicle_photo_extension); // extract the image extension
                $request['photo_kendaraan'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo_kendaraan']); // remove the type part
                $request['photo_kendaraan'] = str_replace(' ', '+', $request['photo_kendaraan']);
                $vehiclePhotoName = $fileName.'_vehicle.'.$vehicle_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$vehiclePhotoName, base64_decode($request['photo_kendaraan']));
            }

            //nomor antrian
            $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
            $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
            $check_total_guest = Kunjungan::whereBetween(
                'created_at', [$check_total_guest_from, $check_total_guest_to]
            )
            ->where('id_satker', $request["id_satker"])
            ->get();
            $total_today_guest = count($check_total_guest);
            $total_today_guest = $total_today_guest + 1;
            $pengunjung_ke = '001';
            if($total_today_guest < 10) {
                $pengunjung_ke = '00'.$total_today_guest;
            } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                $pengunjung_ke = '0'.$total_today_guest;
            } else if($total_today_guest >= 100) {
                $pengunjung_ke = $total_today_guest;
            }


            //QR Expired TIME
            $qr_expired_time = date('Y-m-d H:i:s', strtotime('+24 hours'));

            if($datatamu->status_blacklist == 1) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu blacklist',
                        'datas' => []
                    ),
                    400
                );
            }
            elseif($datatamu->status_blacklist == 2) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu ancaman',
                        'datas' => []
                    ),
                    400
                );
            }else{
                $satker = Satker::where('id_satker',$request["id_satker"])->select('kode_satker');
                $pegawai = Pegawai::where('id',$request["id_pegawai"])->select('name','jabatan')->first();
                if($request["nama_tahanan2"] != null){
                    $nama_tahanan = $request["nama_tahanan2"];
                }else{
                    $nama_tahanan = $request["nama_tahanan"];
                }
                Kunjungan::where('unique_id', $unique_id)
                ->update([
                    "id_tamu" => $datatamu->id_tamu,
                    "id_organisasi" => $request["id_organisasi"],
                    "id_pegawai" => $request["id_pegawai"],
                    "id_satker" => $request["id_satker"],
                    "kode_satker" => $satker->kode_satker,
                    "no_berkas" => $request["no_berkas"],
                    "barang_bawaan" => $request["barang_bawaan"],
                    "barang_ditinggal" => $request["barang_ditinggal"],
                    "nama_tahanan" => $nama_tahanan,
                    "detail" => $request["kunjungan_detail"],
                    "guest_no" => $pengunjung_ke,
                    "tanggal_expired" => $qr_expired_time,
                    "photo" => 'img/foto/'.$photoName,
                    "qr_image" => "img/qr/",
                    "status" => '9',
                    "jumlah_tamu" => $jumlah_tamu_new,
                    "tipe_tamu" => $tipe_tamu,
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "no_kendaraan" => $request["no_kendaraan"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    // "name" => $pegawai->name,
                    // "jabatan" => $pegawai->jabatan,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);

                //cari id untuk Update QR
                $kunjungan_baru = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                //generate QR 
                $qr_image = 'img/qr/'.$fileName.'.png';
                QrCode::format('png')
                ->size(300)
                ->margin(3)
                ->generate($unique_id, '../public/'.$qr_image);
                
                //update QR
                Kunjungan::where('id', $kunjungan_baru['id'])->update(
                    array(
                        'qr_image' => $qr_image
                    )
                );

                if($request['nama_pengikut'][0] != null){
                    foreach($request->input('nama_pengikut') as $key => $value) {
                        TamuPengikut::create([
                            'nama_pengikut'=>$value,
                            'id_kunjungan'=> $kunjungan_baru['id'],
                            'urutan_tamu'=> $key+1
                        ]);
                    }
    
                    foreach($request->input('jenis_kelamin_pengikut') as $key => $value) {
                        TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                        ->where('urutan_tamu',$key+1)
                        ->update(['jenis_kelamin'=>$value]);
                    }
    
                    foreach($request->input('is_anak') as $key => $value) {
                        TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                        ->where('urutan_tamu',$key+1)
                        ->update(['is_anak'=>$value]);
                    }
                }
            
            }

            $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
            $dataPtsp = [
                'uuid'  => $datatamu->uuid,
                'nik_type'  => '1',
                'nik'     => $request["tamu_no_identitas"],
                'name'   => $request["tamu_nama"],
                'kode_satker'      => $satker->kode_satker,
                'type'       => '1',
                'email'      => $request["tamu_email"],
                'address'      => $request["tamu_alamat"],
                'phone'      => $request["tamu_no_hp"],
                'plat_kendaraan'      => $request["tamu_no_polisi"],
                'status'      => "1",
                'tujuan'      => $request["kunjungan_detail"],
                'foto_kendaraan'      => 'img/foto/'.$vehiclePhotoName,
                'foto_diri'      => 'img/foto/'.$photoName,
                'jenis_kelamin'      => $request["jenis_kelamin"],
                'unique_key'      => $unique_id,
            ];

            // return $dataPtsp;

            $apiPtsp = 'https://pelayanan.kejaksaan.go.id/api/simpan-tamu';
            $ClientPtsp = new Client([
                'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                'verify' => false
            ]);
            // return $dataPtsp;
            $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                'form_params' => $dataPtsp
            ]);
                
              
            
            $crypt = "https://pelayanan.kejaksaan.go.id/?uuid=".Crypt::encrypt($datatamu->uuid);
            
            $url = 
            DB::table('kunjungan as k')
            ->join('satker as s','s.id_satker','=','k.id_satker')
            ->join('tamu as t','t.id_tamu','=','k.id_tamu')
            ->select( DB::raw("CONCAT('https://pelayanan.kejaksaan.go.id/?uuid=',t.uuid) AS url_survey"))
            ->orderBy('k.created_at','desc')
            ->where('k.unique_id', $unique_id)
            ->first();



            $data_personal = 
            DB::table('kunjungan as k')
            ->join('tamu as t','k.id_tamu','=','t.id_tamu')
            ->join('pegawai as p','p.id','=','k.id_pegawai')
            ->join('satker as s','s.id_satker','=','k.id_satker')
            ->select('k.id as kunjungan_id','s.nama_satker as nama_satker_kunjungan', 's.slug as slug_satker', 'k.in as waktu_checkin', 'k.created_at as tanggal_kunjungan','p.nip as nip_pegawai', 'k.id_tamu as id_tamu','k.unique_id as unique_id', 'k.jumlah_tamu as jumlah_tamu',DB::raw('CASE WHEN k.tipe_tamu = 1 THEN "TAMU BIASA" WHEN k.tipe_tamu = 2 THEN "SAKSI" WHEN k.tipe_tamu = 3 THEN "TERSANGKA/TERDAKWA" WHEN k.tipe_tamu = 4 THEN "AHLI" WHEN k.tipe_tamu = 5 THEN "TAMU VVIP" ELSE "TIDAK DIKETAHUI" END as tipe_tamu'), DB::raw('CASE WHEN t.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'), 't.nama as nama_tamu',  't.no_identitas as ktp_tamu', 't.no_polisi as nomor_kendaraan_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo) as foto_tamu'), DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo_kendaraan) as foto_kendaraan'), 'k.in as jam_checkin', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
            ->orderBy('k.created_at','desc')
            ->where('k.unique_id', $unique_id)
            ->first();


            $tokenekin = ('$2y$10$K0xXdngm7c0GvChV0O5q1eA85juNQ3S3NZ1wmZXFJazOnffWnbICm');
            // $url = ('https://mysimkari.kejaksaan.go.id/api/ekinerja');
            $url = '172.16.2.101/api/ekinerja';
            $headers = array('Authorization: Bearer '.$tokenekin, 'Content-Type: multipart/form-data');

            $post = [
                'nip' => $data_personal->nip_pegawai,
                'sasaran_kinerja' => 'Penerimaan Tamu ('.$data_personal->tipe_tamu.')',
                'desc_kinerja'   => 'Penerimaan Tamu ('.$data_personal->tipe_tamu.') Atas Nama '.$data_personal->nama_tamu.' pada tanggal '.$data_personal->tanggal_kunjungan.' Pada Satuan Kerja '.$data_personal->nama_satker_kunjungan,
                'lokasi_kegiatan' => $data_personal->nama_satker_kunjungan,
                'point' => 1,
                'nama_app' => 'BUKU TAMU',
                'url_file' => $data_personal->foto_tamu,
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$post); 
            try {
                $response = curl_exec($ch);
            } catch (Exception $e) {
                    // do nothing, the timeout exception is intended
            }
            curl_close($ch);

            $numberKeys = array(
                'helpdesk1',
                'helpdesk2',
                'helpdesk4'
                
            );
    
            $randomNumber = array_rand($numberKeys);
            $numberKey = $numberKeys[$randomNumber];
    
            
            $number_tamu = $request['no_hp'];
            $file_tamu =  "https://bukutamu.kejaksaan.go.id/".$qr_image;
            $caption_tamu = "Selamat datang di Pelayanan Terpadu Kejaksaan Republik Indonesia"."\n\nHalo ".$request->nama."\n\nTerimakasih telah melakukan pendaftaran pelayanan pada sistem Pelayanan Terpadu Kejaksaan\n\n Simpan QR Code yang dikirimkan oleh Whatsapp ini sebagai tanda pengenal anda pada sistem Pelayanan Terpadu Kejaksaan";;
            // $isi_post_tamu = 'token='.$number_key.'&number='.$number_tamu.'&file='.$file_tamu.'&caption='.$caption_tamu;
            $isi_post_tamu = [
                'sessions'=> $numberKey,
                'target'=> $number_tamu,
                'message'=> $caption_tamu,
                'url'=> $file_tamu
            ];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $isi_post_tamu,
            ));
    
            try {
                $lapor_tamu_kunjungan = curl_exec($curl);
            } catch (Exception $e) {
                    // do nothing, the timeout exception is intended
            }
            curl_close($curl);


            return view('frontend.direct', compact('url','crypt'));

        }

        elseif( !empty($request['photo']) ){
            //pengecekan tamu sudah ada di db atau belum
            $check_tamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->count();
            // $check_tamu_data = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();
            $check_tamu_pegawai = Pegawai::whereRaw('status = 1')
            ->where('nik', $request['tamu_no_identitas'])
            ->first();

            $jumlah_tamu = $request["jumlah_tamu"];

            if($jumlah_tamu == null){
                $jumlah_tamu_new = 1;
            }else{
                $jumlah_tamu_x = $request["jumlah_tamu"];
                $jumlah_tamu_new = $jumlah_tamu_x + 1;
            }

            if(empty($check_tamu_pegawai)) {
                $tipe_tamu = $request["tamu_tipe_tamu"];
                
            }else{
                $tipe_tamu = 7;
            }

            if($check_tamu >= 1){
                Tamu::where('no_identitas', $request->tamu_no_identitas)
                ->update([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "tipe_tamu" => $tipe_tamu,
                    "is_anak" => '2',
                    'updated_at'        => Carbon::now()
                ]);
            }else {
                Tamu::create([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "status_blacklist" => "5",
                    "tipe_tamu" => $tipe_tamu,
                    "is_anak" => '2',
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);
            }
            //pengambilan data tamu yang sudah ada di db
            $datatamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();

            //file directory
            $slugnama = str_replace(" ","-",$request["tamu_nama"]);
            $fileName = $request["tamu_tipe_identitas"].$request["tamu_no_identitas"].$slugnama.'-'.time(); //generating unique file name
            $fileDirectory = public_path('img').'/foto/'; // directory of uploaded file
            if (! File::isDirectory($fileDirectory)) {
                File::makeDirectory($fileDirectory, 0755, true);
            }

            $qrCodeDirectory = public_path('img').'/qr/'; // directory of qrcode file
            if (! File::isDirectory($qrCodeDirectory)) {
                File::makeDirectory($qrCodeDirectory, 0755, true);
            }

            //upload foto tamu
            $photoName = '';
            if( !empty($request['photo']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                $request['photo'] = str_replace(' ', '+', $request['photo']);
                $photoName = $fileName.'_photo.'.$tamu_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
            }

            //upload foto kendaraan
            $vehiclePhotoName = '';
            if( !empty($request['photo_kendaraan']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo_kendaraan'], $vehicle_photo_extension); // extract the image extension
                $request['photo_kendaraan'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo_kendaraan']); // remove the type part
                $request['photo_kendaraan'] = str_replace(' ', '+', $request['photo_kendaraan']);
                $vehiclePhotoName = $fileName.'_vehicle.'.$vehicle_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$vehiclePhotoName, base64_decode($request['photo_kendaraan']));
            }

            //nomor antrian
            $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
            $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
            $check_total_guest = Kunjungan::whereBetween(
                'created_at', [$check_total_guest_from, $check_total_guest_to]
            )
            ->where('id_satker', $request["id_satker"])
            ->get();
            $total_today_guest = count($check_total_guest);
            $total_today_guest = $total_today_guest + 1;
            $pengunjung_ke = '001';
            if($total_today_guest < 10) {
                $pengunjung_ke = '00'.$total_today_guest;
            } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                $pengunjung_ke = '0'.$total_today_guest;
            } else if($total_today_guest >= 100) {
                $pengunjung_ke = $total_today_guest;
            }
            

            //QR Expired TIME
            $qr_expired_time = date('Y-m-d H:i:s', strtotime('+24 hours'));


            //massgate
        
            $massgate_id = $unique_id;
            

            $requestMassgateBody = [
                'visitor_id' => $massgate_id,
                'full_name' => $request['tamu_nama'],
                'nik_number' => $request['tamu_no_identitas'],
                'phone_no' => $request['tamu_no_hp'],
                'visit_purpose' => $request['kunjungan_detail'],
                'address' => $request['tamu_alamat'],
                'qr_code_data' => $unique_id,
                'enrolled_time' => date('Y-m-d H:i:s'),
                'qr_expiry_time' => $qr_expired_time,
                'category' => '1',
            ];


            if($datatamu->status_blacklist == 1) {
                $requestMassgateBody['category'] = 'Blacklist';
            }

            if($datatamu->status_blacklist == 2) {
                $requestMassgateBody['category'] = 'Threat';
            }

            $selected_satker = Satker::where('id_satker', $request['id_satker'])->first();
            if($selected_satker->kode_satker == '00'){
                $url_massgate = 'http://172.16.3.107/massgate/public/visitor_enrolment';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($requestMassgateBody),
                    'verify' => false
                ]);
                try {
                    $post_massgate = $Client->post($url_massgate, ['timeout' => 5]);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
            }
         
            if($datatamu->status_blacklist == 1) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu blacklist',
                        'datas' => []
                    ),
                    400
                );
            }
            elseif($datatamu->status_blacklist == 2) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu ancaman',
                        'datas' => []
                    ),
                    400
                );
            }else{
                $pegawai = Pegawai::where('id',$request["id_pegawai"])->select('name','jabatan')->first();
                if($request["nama_tahanan2"] != null){
                    $nama_tahanan = $request["nama_tahanan2"];
                }else{
                    $nama_tahanan = $request["nama_tahanan"];
                }
                Kunjungan::where('unique_id', $unique_id)
                ->update([
                    "id_tamu" => $datatamu->id_tamu,
                    "id_organisasi" => $request["id_organisasi"],
                    "id_pegawai" => $request["id_pegawai"],
                    "id_satker" => $request["id_satker"],
                    "no_berkas" => $request["no_berkas"],
                    "detail" => $request["kunjungan_detail"],
                    "guest_no" => $pengunjung_ke,
                    "tanggal_expired" => $qr_expired_time,
                    "barang_bawaan" => $request["barang_bawaan"],
                    "barang_ditinggal" => $request["barang_ditinggal"],
                    "nama_tahanan" => $nama_tahanan,
                    "photo" => 'img/foto/'.$photoName,
                    "qr_image" => "img/qr/",
                    "jumlah_tamu" => $jumlah_tamu_new,
                    "tipe_tamu" => $tipe_tamu,
                    "unique_id" => $unique_id,
                    "massgate_id" => $massgate_id,
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "no_kendaraan" => $request["no_kendaraan"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    // "name" => $pegawai->name,
                    // "jabatan" => $pegawai->jabatan,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);

                //cari id untuk Update QR
                $kunjungan_baru = Kunjungan::select('*')
                ->where('id', $request['id_kunjungan'])
                ->orderByRaw('id DESC')
                ->first();

                //generate QR 
                $qr_image = 'img/qr/'.$fileName.'.png';
                QrCode::format('png')
                ->size(300)
                ->margin(3)
                ->generate($unique_id, '../public/'.$qr_image);
                
                //update QR
                Kunjungan::where('id', $kunjungan_baru['id'])->update(
                    array(
                        'qr_image' => $qr_image
                    )
                );

                foreach($request->input('nama_pengikut') as $key => $value) {
                    TamuPengikut::create([
                        'nama_pengikut'=>$value,
                        'id_kunjungan'=> $kunjungan_baru['id'],
                        'urutan_tamu'=> $key+1
                    ]);
                }

                foreach($request->input('jenis_kelamin_pengikut') as $key => $value) {
                    TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                    ->where('urutan_tamu',$key+1)
                    ->update(['jenis_kelamin'=>$value]);
                }

                foreach($request->input('is_anak') as $key => $value) {
                    TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                    ->where('urutan_tamu',$key+1)
                    ->update(['is_anak'=>$value]);
                }

                

                //create data for WA

                $tipe_nik = '';
                if($request["tamu_tipe_identitas"] == '1'){$tipe_nik = 'KTP';}
                elseif($request["tamu_tipe_identitas"] == '2'){$tipe_nik = 'SIM';}
                else{$tipe_nik = 'Passport';}
                
                $selected_pegawai = Pegawai::where('id', $request['id_pegawai'])->first();
                
                $selected_bidang = "Kejaksaan";

                $selected_jabatan = 
                DB::table('pegawai as p')
                ->select('p.jabatan as nama_jabatan')
                ->where('p.id', $request['id_pegawai'])
                ->first();

                
                
                // $nama_pegawai_tujuan = $selected_pegawai->name;
                // $nama_jabatan_tujuan = $selected_jabatan->nama_jabatan;
                $nama_satker_tujuan = $selected_satker->nama_satker;
                $nama_bidang_tujuan = $selected_bidang;

                
                $data_kunjungan = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                $numberKeys = array(
                    'helpdesk1',
                    'helpdesk2',
                    'helpdesk4'
                    
                );

                $randomNumber = array_rand($numberKeys);
                $numberKey = $numberKeys[$randomNumber];

                // $number = $selected_pegawai->no_hp;
                $file = "https://bukutamu.kejaksaan.go.id/".$data_kunjungan->photo;
                // $caption = "Salam sehat dan sukses selalu bapak/ibu ".$nama_pegawai_tujuan."\n\nMohon izin sebelumnya whatsapp ini memberikan notifikasi ke bapak/ibu tentang tamu yang akan menemui bapak/ibu\n\nBerikut biodata tamu yang ingin menemui bapak/ibu\n\nNama : ".$request['tamu_nama']."\nDengan Keperluan : ".$data_kunjungan->detail."\n\nPesan ini langsung tergenerate dari Aplikasi Bukutamu Kejaksaan\n\nTerimakasih Bapak/Ibu";
                // $isi_post = 'token='.$number_key.'&number='.$number.'&file='.$file.'&caption='.$caption;
                $isi_post = [
                    'sessions'=> $numberKey,
                    // 'target'=> $number,
                    // 'message'=> $caption,
                    'url'=> $file
                ];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 1,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $isi_post,
                ));
                try {
                    $lapor_tamu_kunjungan = curl_exec($curl);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($curl);
                

                $data_kunjungan = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                $number_tamu = $request['tamu_no_hp'];
                $file_tamu =  "https://bukutamu.kejaksaan.go.id/".$data_kunjungan->qr_image;
                $caption_tamu = "Selamat datang di ".$nama_satker_tujuan."\nHalo ".$request['tamu_nama']."\n\nAnda telah melakukan permintaan untuk ".$data_kunjungan->detail."\n\nNo Antrian Anda : ".$data_kunjungan->guest_no."\n\nPesan ini dari Aplikasi Bukutamu Kejaksaan\nTerimakasih";
                // $isi_post_tamu = 'token='.$number_key.'&number='.$number_tamu.'&file='.$file_tamu.'&caption='.$caption_tamu;
                $isi_post_tamu = [
                    'sessions'=> $numberKey,
                    'target'=> $number_tamu,
                    'message'=> $caption_tamu,
                    'url'=> $file_tamu
                ];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 1,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $isi_post_tamu,
                ));
                try {
                    $lapor_tamu_kunjungan = curl_exec($curl);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($curl);

                

                 
                $satker = Satker::where('id_satker',$request["id_satker"])->select('nama_satker','parent_id','kode_satker')->first();
                $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
                $dataPtsp = [
                    'nik_type'  => $tipe_tamu,
                    'uuid'  => $datatamu->uuid,
                    'nik'     => $request["tamu_no_identitas"],
                    'name'   => $request["tamu_nama"],
                    'kode_satker'      => $satker->kode_satker,
                    'type'       => $tipe_tamu,
                    'email'      => $request["tamu_email"],
                    'address'      => $request["tamu_alamat"],
                    'phone'      => $request["tamu_no_hp"],
                    'plat_kendaraan'      => $request["tamu_no_polisi"],
                    'status'      => "1",
                    'tujuan'      => $request["kunjungan_detail"],
                    'foto_kendaraan'      => 'img/foto/'.$vehiclePhotoName,
                    'foto_diri'      => 'img/foto/'.$photoName,
                    'jenis_kelamin'      => $request["jenis_kelamin"],
                    'unique_key'      => $unique_id,
                ];

                // return $dataPtsp;

                $apiPtsp = 'https://pelayanan.kejaksaan.go.id/api/simpan-tamu';
                $ClientPtsp = new Client([
                    'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                    'verify' => false
                ]);
                $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                    'form_params' => $dataPtsp
                ]);

                // $apiPtsp = 'https://ptsp-sumut.kejaksaanri.id/api/simpan-tamu';
                // $ClientPtsp = new Client([
                //     'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                //     'verify' => false
                // ]);
                // $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                //     'form_params' => $dataPtsp
                // ]);


                $data_return = [
                    'data_tamu' => $datatamu,
                    'data_kunjungan' => $kunjungan_baru,
                    // 'nama_pegawai_tujuan' => $nama_pegawai_tujuan,
                    // 'nama_jabatan_tujuan' => $nama_jabatan_tujuan,
                    'nama_satker_tujuan' => $nama_satker_tujuan,
                    'nama_bidang_tujuan' => $nama_bidang_tujuan,
                ];

                $tanggal_hari_ini = Carbon::today()->toDateString();

                $tracingId = Str::uuid();
                $eventCode = 'TAMU_KNJGN';
                $id_satker = $request["id_satker"];
                $nama_satker = $satker->nama_satker;
                $parent_satker = $satker->parent_id;
                $kode_satker = $satker->kode_satker;
                $tanggal = $tanggal_hari_ini;
                $bulan = substr($tanggal_hari_ini,0,7);
                $total_kendaraan = 1;

                $summaryData = Array();
                $summaryData["tracingId"] = $tracingId;
                $summaryData["eventCode"] = $eventCode;
                $summaryData["summaryData"]["id_satker"] = $id_satker;
                $summaryData["summaryData"]["nama_satker"] = $nama_satker;
                $summaryData["summaryData"]["parent_satker"] = $parent_satker;
                $summaryData["summaryData"]["kode_satker"] = $kode_satker;
                $summaryData["summaryData"]["tanggal"] = $tanggal;
                $summaryData["summaryData"]["bulan"] = $bulan;
                $summaryData["summaryData"]["total_kendaraan"] = $total_kendaraan;
                
                $url = '43.231.129.14:8081/api/v1/realtime-event/create';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($summaryData),
                    'verify' => false
                ]);

                try {
                    $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                    DB::table('realtime_api')->insert(
                        array(
                            "tracingId" => $tracingId,
                            "eventCode" => $eventCode,
                            "summaryData" => json_encode($summaryData),
                            "created_at" => Carbon::now(),
                            "updated_at" => Carbon::now(),
                        )
                   );
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }    

                $tracingId2 = Str::uuid();
                $eventCode2 = 'TAMU_KNJGN_TAMU';
                $nama_satker = $satker->nama_satker;
                $parent_satker = $satker->parent_id;
                $kode_satker = $satker->kode_satker;
                $tanggal = $tanggal_hari_ini;
                $bulan = substr($tanggal_hari_ini,0,7);
                $total_kendaraan = 1;
                $master_tipe_tamu = DB::table('master_tipe_tamu')->where('id',$tipe_tamu)->first();
                $cari_anak = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('is_anak','1')->get());
                $cari_dewasa = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('is_anak','2')->get()) + 1;
                $cari_laki = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('jenis_kelamin','1')->get()) + count(Kunjungan::join('tamu','tamu.id_tamu','kunjungan.id_tamu')->where('kunjungan.id',$kunjungan_baru['id'])->where('tamu.jenis_kelamin','1')->get());
                $cari_perempuan = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('jenis_kelamin','2')->get()) + count(Kunjungan::join('tamu','tamu.id_tamu','kunjungan.id_tamu')->where('kunjungan.id',$kunjungan_baru['id'])->where('tamu.jenis_kelamin','2')->get());


                $summaryData2 = Array();
                $summaryData2["tracingId"] = $tracingId2;
                $summaryData2["eventCode"] = $eventCode2;
                $summaryData2["summaryData"]["id_satker"] = $id_satker;
                $summaryData2["summaryData"]["nama_satker"] = $nama_satker;
                $summaryData2["summaryData"]["parent_satker"] = $parent_satker;
                $summaryData2["summaryData"]["kode_satker"] = $kode_satker;
                $summaryData2["summaryData"]["tanggal"] = $tanggal;
                $summaryData2["summaryData"]["bulan"] = $bulan;
                $summaryData2["summaryData"]["id_jenis_tamu"] = $tipe_tamu;
                $summaryData2["summaryData"]["nama_jenis_tamu"] = $master_tipe_tamu->nama;
                $summaryData2["summaryData"]["total_tamu"] = $jumlah_tamu_new;
                $summaryData2["summaryData"]["total_anak"] = $cari_anak;
                $summaryData2["summaryData"]["total_dewasa"] = $cari_dewasa;
                $summaryData2["summaryData"]["total_laki"] = $cari_laki;
                $summaryData2["summaryData"]["total_perempuan"] = $cari_perempuan;
                
                $url = '43.231.129.14:8081/api/v1/realtime-event/create';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($summaryData2),
                    'verify' => false
                ]);

                try {
                    $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                    DB::table('realtime_api')->insert(
                        array(
                            "tracingId" => $tracingId2,
                            "eventCode" => $eventCode2,
                            "summaryData" => json_encode($summaryData2),
                            "created_at" => Carbon::now(),
                            "updated_at" => Carbon::now(),
                        )
                   );
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }  

            }
            // $this->kunjungan($unique_id);
            Alert::success('Berhasil menyimpan data', 'Silahkan lakukan checkin');
            return redirect(url('/kunjungan/view',$unique_id));
        }else{
            Alert::warning('Data Tidak Berhasil Tersimpan', 'Pastikan anda telah mengisi foto');
            return back();
        }
    }

    public function updatetamu(Request $request, $unique_id)
    {
        $data_pegawai = Pegawai::where("id",$request["id_pegawai"])->first();
        
        $satker = Satker::where('id_satker',$request["id_satker"])->select('kode_satker')->first();
        
        // $url = 'http://43.231.129.16/absen/api/absen/cek_absen/'.$data_pegawai->nip;
        
        // $Client = new Client(['headers' => [
        //     'x-api-version' => '2',
        //     'Accept'        => 'application/json'
        // ]]);
        // $response = $Client->get($url);
        // $result_api = json_decode($response->getBody()->getContents(), true);

        // if($result_api['status_code'] != 200){
        //     return view('frontend.tidak-tersedia');
        // }
        
        if( $request['status'] == 9 and !empty($request['photo']) ){
            $check_tamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->count();
            // $check_tamu_data = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();
            $check_tamu_pegawai = Pegawai::whereRaw('status = 1')
            ->where('nik', $request['tamu_no_identitas'])
            ->first();

            $jumlah_tamu = $request["jumlah_tamu"];

            if($jumlah_tamu == null){
                $jumlah_tamu_new = 1;
            }else{
                $jumlah_tamu_x = $request["jumlah_tamu"];
                $jumlah_tamu_new = $jumlah_tamu_x + 1;
            }

            if(empty($check_tamu_pegawai)) {
                $tipe_tamu = $request["tamu_tipe_tamu"];
                
            }else{
                $tipe_tamu = 7;
            }

            if($check_tamu >= 1){
                Tamu::where('no_identitas', $request->tamu_no_identitas)
                ->update([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "no_hp" => $request["tamu_no_hp"],
                    "is_anak" => '2',
                    "tipe_tamu" => $tipe_tamu,
                    'updated_at'        => Carbon::now()
                ]);
            }else {
                Tamu::create([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "is_anak" => '2',
                    "status_blacklist" => '5',
                    "tipe_tamu" => $tipe_tamu,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);
            }
            //pengambilan data tamu yang sudah ada di db
            $datatamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();

            //file directory
            $slugnama = str_replace(" ","-",$request["tamu_nama"]);
            $fileName = $request["tamu_tipe_identitas"].$request["tamu_no_identitas"].$slugnama.'-'.time(); //generating unique file name
            $fileDirectory = public_path('img').'/foto/'; // directory of uploaded file
            if (! File::isDirectory($fileDirectory)) {
                File::makeDirectory($fileDirectory, 0755, true);
            }

            $qrCodeDirectory = public_path('img').'/qr/'; // directory of qrcode file
            if (! File::isDirectory($qrCodeDirectory)) {
                File::makeDirectory($qrCodeDirectory, 0755, true);
            }

            //upload foto tamu
            $photoName = '';
            if( !empty($request['photo']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                $request['photo'] = str_replace(' ', '+', $request['photo']);
                $photoName = $fileName.'_photo.'.$tamu_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
            }

            //upload foto kendaraan
            $vehiclePhotoName = '';
            if( !empty($request['photo_kendaraan']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo_kendaraan'], $vehicle_photo_extension); // extract the image extension
                $request['photo_kendaraan'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo_kendaraan']); // remove the type part
                $request['photo_kendaraan'] = str_replace(' ', '+', $request['photo_kendaraan']);
                $vehiclePhotoName = $fileName.'_vehicle.'.$vehicle_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$vehiclePhotoName, base64_decode($request['photo_kendaraan']));
            }

            //nomor antrian
            $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
            $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
            $check_total_guest = Kunjungan::whereBetween(
                'created_at', [$check_total_guest_from, $check_total_guest_to]
            )
            ->where('id_satker', $request["id_satker"])
            ->get();
            $total_today_guest = count($check_total_guest);
            $total_today_guest = $total_today_guest + 1;
            $pengunjung_ke = '001';
            if($total_today_guest < 10) {
                $pengunjung_ke = '00'.$total_today_guest;
            } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                $pengunjung_ke = '0'.$total_today_guest;
            } else if($total_today_guest >= 100) {
                $pengunjung_ke = $total_today_guest;
            }


            //QR Expired TIME
            $qr_expired_time = date('Y-m-d H:i:s', strtotime('+24 hours'));

            if($datatamu->status_blacklist == 1) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu blacklist',
                        'datas' => []
                    ),
                    400
                );
            }
            elseif($datatamu->status_blacklist == 2) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu ancaman',
                        'datas' => []
                    ),
                    400
                );
            }else{
                $satker = Satker::where('id_satker',$request["id_satker"])->select('kode_satker');
                $pegawai = Pegawai::where('id',$request["id_pegawai"])->select('name','jabatan')->first();
                if($request["nama_tahanan2"] != null){
                    $nama_tahanan = $request["nama_tahanan2"];
                }else{
                    $nama_tahanan = $request["nama_tahanan"];
                }
                Kunjungan::where('unique_id', $unique_id)
                ->update([
                    "id_tamu" => $datatamu->id_tamu,
                    "id_organisasi" => $request["id_organisasi"],
                    "id_pegawai" => $request["id_pegawai"],
                    "id_satker" => $request["id_satker"],
                    "kode_satker" => $satker->kode_satker,
                    "no_berkas" => $request["no_berkas"],
                    "barang_bawaan" => $request["barang_bawaan"],
                    "barang_ditinggal" => $request["barang_ditinggal"],
                    "nama_tahanan" => $nama_tahanan,
                    "detail" => $request["kunjungan_detail"],
                    "guest_no" => $pengunjung_ke,
                    "tanggal_expired" => $qr_expired_time,
                    "photo" => 'img/foto/'.$photoName,
                    "qr_image" => "img/qr/",
                    "status" => '9',
                    "jumlah_tamu" => $jumlah_tamu_new,
                    "tipe_tamu" => $tipe_tamu,
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "no_kendaraan" => $request["no_kendaraan"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "name" => $pegawai->name,
                    "jabatan" => $pegawai->jabatan,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);

                //cari id untuk Update QR
                $kunjungan_baru = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                //generate QR 
                $qr_image = 'img/qr/'.$fileName.'.png';
                QrCode::format('png')
                ->size(300)
                ->margin(3)
                ->generate($unique_id, '../public/'.$qr_image);
                
                //update QR
                Kunjungan::where('id', $kunjungan_baru['id'])->update(
                    array(
                        'qr_image' => $qr_image
                    )
                );

                if($request['nama_pengikut'][0] != null){
                    foreach($request->input('nama_pengikut') as $key => $value) {
                        TamuPengikut::create([
                            'nama_pengikut'=>$value,
                            'id_kunjungan'=> $kunjungan_baru['id'],
                            'urutan_tamu'=> $key+1
                        ]);
                    }
    
                    foreach($request->input('jenis_kelamin_pengikut') as $key => $value) {
                        TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                        ->where('urutan_tamu',$key+1)
                        ->update(['jenis_kelamin'=>$value]);
                    }
    
                    foreach($request->input('is_anak') as $key => $value) {
                        TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                        ->where('urutan_tamu',$key+1)
                        ->update(['is_anak'=>$value]);
                    }
                }
            
            }

            $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
            $dataPtsp = [
                'uuid'  => $datatamu->uuid,
                'nik_type'  => '1',
                'nik'     => $request["tamu_no_identitas"],
                'name'   => $request["tamu_nama"],
                'kode_satker'      => $satker->kode_satker,
                'type'       => '1',
                'email'      => $request["tamu_email"],
                'address'      => $request["tamu_alamat"],
                'phone'      => $request["tamu_no_hp"],
                'plat_kendaraan'      => $request["tamu_no_polisi"],
                'status'      => "1",
                'tujuan'      => $request["kunjungan_detail"],
                'foto_kendaraan'      => 'img/foto/'.$vehiclePhotoName,
                'foto_diri'      => 'img/foto/'.$photoName,
                'jenis_kelamin'      => $request["jenis_kelamin"],
                'unique_key'      => $unique_id,
            ];

            // return $dataPtsp;

            $apiPtsp = 'https://pelayanan.kejaksaan.go.id/api/simpan-tamu';
            $ClientPtsp = new Client([
                'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                'verify' => false
            ]);
            // return $dataPtsp;
            $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                'form_params' => $dataPtsp
            ]);
                
              
            
            $crypt = "https://pelayanan.kejaksaan.go.id/?uuid=".Crypt::encrypt($datatamu->uuid);
            
            $url = 
            DB::table('kunjungan as k')
            ->join('satker as s','s.id_satker','=','k.id_satker')
            ->join('tamu as t','t.id_tamu','=','k.id_tamu')
            ->select( DB::raw("CONCAT('https://pelayanan.kejaksaan.go.id/?uuid=',t.uuid) AS url_survey"))
            ->orderBy('k.created_at','desc')
            ->where('k.unique_id', $unique_id)
            ->first();



            $data_personal = 
            DB::table('kunjungan as k')
            ->join('tamu as t','k.id_tamu','=','t.id_tamu')
            ->join('pegawai as p','p.id','=','k.id_pegawai')
            ->join('satker as s','s.id_satker','=','k.id_satker')
            ->select('k.id as kunjungan_id','s.nama_satker as nama_satker_kunjungan', 's.slug as slug_satker', 'k.in as waktu_checkin', 'k.created_at as tanggal_kunjungan','p.nip as nip_pegawai', 'k.id_tamu as id_tamu','k.unique_id as unique_id', 'k.jumlah_tamu as jumlah_tamu',DB::raw('CASE WHEN k.tipe_tamu = 1 THEN "TAMU BIASA" WHEN k.tipe_tamu = 2 THEN "SAKSI" WHEN k.tipe_tamu = 3 THEN "TERSANGKA/TERDAKWA" WHEN k.tipe_tamu = 4 THEN "AHLI" WHEN k.tipe_tamu = 5 THEN "TAMU VVIP" ELSE "TIDAK DIKETAHUI" END as tipe_tamu'), DB::raw('CASE WHEN t.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'), 't.nama as nama_tamu',  't.no_identitas as ktp_tamu', 't.no_polisi as nomor_kendaraan_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo) as foto_tamu'), DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo_kendaraan) as foto_kendaraan'), 'k.in as jam_checkin', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
            ->orderBy('k.created_at','desc')
            ->where('k.unique_id', $unique_id)
            ->first();


            $tokenekin = ('$2y$10$K0xXdngm7c0GvChV0O5q1eA85juNQ3S3NZ1wmZXFJazOnffWnbICm');
            // $url = ('https://mysimkari.kejaksaan.go.id/api/ekinerja');
            $url = '172.16.2.101/api/ekinerja';
            $headers = array('Authorization: Bearer '.$tokenekin, 'Content-Type: multipart/form-data');

            $post = [
                'nip' => $data_personal->nip_pegawai,
                'sasaran_kinerja' => 'Penerimaan Tamu ('.$data_personal->tipe_tamu.')',
                'desc_kinerja'   => 'Penerimaan Tamu ('.$data_personal->tipe_tamu.') Atas Nama '.$data_personal->nama_tamu.' pada tanggal '.$data_personal->tanggal_kunjungan.' Pada Satuan Kerja '.$data_personal->nama_satker_kunjungan,
                'lokasi_kegiatan' => $data_personal->nama_satker_kunjungan,
                'point' => 1,
                'nama_app' => 'BUKU TAMU',
                'url_file' => $data_personal->foto_tamu,
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$post); 
            try {
                $response = curl_exec($ch);
            } catch (Exception $e) {
                    // do nothing, the timeout exception is intended
            }
            curl_close($ch);

            $numberKeys = array(
                'helpdesk1',
                'helpdesk2',
                'helpdesk4'
                
            );
    
            $randomNumber = array_rand($numberKeys);
            $numberKey = $numberKeys[$randomNumber];
    
            
            $number_tamu = $request['no_hp'];
            $file_tamu =  "https://bukutamu.kejaksaan.go.id/".$qr_image;
            $caption_tamu = "Selamat datang di Pelayanan Terpadu Kejaksaan Republik Indonesia"."\n\nHalo ".$request->nama."\n\nTerimakasih telah melakukan pendaftaran pelayanan pada sistem Pelayanan Terpadu Kejaksaan\n\n Simpan QR Code yang dikirimkan oleh Whatsapp ini sebagai tanda pengenal anda pada sistem Pelayanan Terpadu Kejaksaan";;
            // $isi_post_tamu = 'token='.$number_key.'&number='.$number_tamu.'&file='.$file_tamu.'&caption='.$caption_tamu;
            $isi_post_tamu = [
                'sessions'=> $numberKey,
                'target'=> $number_tamu,
                'message'=> $caption_tamu,
                'url'=> $file_tamu
            ];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $isi_post_tamu,
            ));
    
            try {
                $lapor_tamu_kunjungan = curl_exec($curl);
            } catch (Exception $e) {
                    // do nothing, the timeout exception is intended
            }
            curl_close($curl);


            return view('frontend.direct', compact('url','crypt'));

        }

        elseif( !empty($request['photo']) ){
            //pengecekan tamu sudah ada di db atau belum
            $check_tamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->count();
            // $check_tamu_data = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();
            $check_tamu_pegawai = Pegawai::whereRaw('status = 1')
            ->where('nik', $request['tamu_no_identitas'])
            ->first();

            $jumlah_tamu = $request["jumlah_tamu"];

            if($jumlah_tamu == null){
                $jumlah_tamu_new = 1;
            }else{
                $jumlah_tamu_x = $request["jumlah_tamu"];
                $jumlah_tamu_new = $jumlah_tamu_x + 1;
            }

            if(empty($check_tamu_pegawai)) {
                $tipe_tamu = $request["tamu_tipe_tamu"];
                
            }else{
                $tipe_tamu = 7;
            }

            if($check_tamu >= 1){
                Tamu::where('no_identitas', $request->tamu_no_identitas)
                ->update([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "tipe_tamu" => $tipe_tamu,
                    "is_anak" => '2',
                    'updated_at'        => Carbon::now()
                ]);
            }else {
                Tamu::create([
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "tipe_identitas" => $request["tamu_tipe_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "email" => $request["tamu_email"],
                    "jenis_kelamin" => $request["jenis_kelamin"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "status_blacklist" => "5",
                    "tipe_tamu" => $tipe_tamu,
                    "is_anak" => '2',
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);
            }
            //pengambilan data tamu yang sudah ada di db
            $datatamu = Tamu::where('no_identitas', $request->tamu_no_identitas)->first();

            //file directory
            $slugnama = str_replace(" ","-",$request["tamu_nama"]);
            $fileName = $request["tamu_tipe_identitas"].$request["tamu_no_identitas"].$slugnama.'-'.time(); //generating unique file name
            $fileDirectory = public_path('img').'/foto/'; // directory of uploaded file
            if (! File::isDirectory($fileDirectory)) {
                File::makeDirectory($fileDirectory, 0755, true);
            }

            $qrCodeDirectory = public_path('img').'/qr/'; // directory of qrcode file
            if (! File::isDirectory($qrCodeDirectory)) {
                File::makeDirectory($qrCodeDirectory, 0755, true);
            }

            //upload foto tamu
            $photoName = '';
            if( !empty($request['photo']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo'], $tamu_photo_extension); // extract the image extension
                $request['photo'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo']); // remove the type part
                $request['photo'] = str_replace(' ', '+', $request['photo']);
                $photoName = $fileName.'_photo.'.$tamu_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$photoName, base64_decode($request['photo']));
            }

            //upload foto kendaraan
            $vehiclePhotoName = '';
            if( !empty($request['photo_kendaraan']) ) {
                preg_match('/data:image\/(.*?);/', $request['photo_kendaraan'], $vehicle_photo_extension); // extract the image extension
                $request['photo_kendaraan'] = preg_replace('/data:image\/(.*?);base64,/', '', $request['photo_kendaraan']); // remove the type part
                $request['photo_kendaraan'] = str_replace(' ', '+', $request['photo_kendaraan']);
                $vehiclePhotoName = $fileName.'_vehicle.'.$vehicle_photo_extension[1]; //generating unique file name
                File::put(public_path('img').'/foto/'.$vehiclePhotoName, base64_decode($request['photo_kendaraan']));
            }

            //nomor antrian
            $check_total_guest_from = date('Y-m-d'.' '.'00:00:00');
            $check_total_guest_to = date('Y-m-d'.' '.'23:59:59');
            $check_total_guest = Kunjungan::whereBetween(
                'created_at', [$check_total_guest_from, $check_total_guest_to]
            )
            ->where('id_satker', $request["id_satker"])
            ->get();
            $total_today_guest = count($check_total_guest);
            $total_today_guest = $total_today_guest + 1;
            $pengunjung_ke = '001';
            if($total_today_guest < 10) {
                $pengunjung_ke = '00'.$total_today_guest;
            } else if($total_today_guest >= 10 && $total_today_guest < 100) {
                $pengunjung_ke = '0'.$total_today_guest;
            } else if($total_today_guest >= 100) {
                $pengunjung_ke = $total_today_guest;
            }
            

            //QR Expired TIME
            $qr_expired_time = date('Y-m-d H:i:s', strtotime('+24 hours'));


            //massgate
        
            $massgate_id = $unique_id;
            

            $requestMassgateBody = [
                'visitor_id' => $massgate_id,
                'full_name' => $request['tamu_nama'],
                'nik_number' => $request['tamu_no_identitas'],
                'phone_no' => $request['tamu_no_hp'],
                'visit_purpose' => $request['kunjungan_detail'],
                'address' => $request['tamu_alamat'],
                'qr_code_data' => $unique_id,
                'enrolled_time' => date('Y-m-d H:i:s'),
                'qr_expiry_time' => $qr_expired_time,
                'category' => '1',
            ];


            if($datatamu->status_blacklist == 1) {
                $requestMassgateBody['category'] = 'Blacklist';
            }

            if($datatamu->status_blacklist == 2) {
                $requestMassgateBody['category'] = 'Threat';
            }

            $selected_satker = Satker::where('id_satker', $request['id_satker'])->first();
            if($selected_satker->kode_satker == '00'){
                $url_massgate = 'http://172.16.3.107/massgate/public/visitor_enrolment';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($requestMassgateBody),
                    'verify' => false
                ]);
                try {
                    $post_massgate = $Client->post($url_massgate, ['timeout' => 5]);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
            }
         
            if($datatamu->status_blacklist == 1) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu blacklist',
                        'datas' => []
                    ),
                    400
                );
            }
            elseif($datatamu->status_blacklist == 2) {
                return response()->json(
                    array(
                        'code' => 400,
                        'response' => 'failed',
                        'message' => 'tamu ancaman',
                        'datas' => []
                    ),
                    400
                );
            }else{
                $pegawai = Pegawai::where('id',$request["id_pegawai"])->select('name','jabatan')->first();
                if($request["nama_tahanan2"] != null){
                    $nama_tahanan = $request["nama_tahanan2"];
                }else{
                    $nama_tahanan = $request["nama_tahanan"];
                }
                Kunjungan::where('unique_id', $unique_id)
                ->update([
                    "id_tamu" => $datatamu->id_tamu,
                    "id_organisasi" => $request["id_organisasi"],
                    "id_pegawai" => $request["id_pegawai"],
                    "id_satker" => $request["id_satker"],
                    "no_berkas" => $request["no_berkas"],
                    "detail" => $request["kunjungan_detail"],
                    "guest_no" => $pengunjung_ke,
                    "tanggal_expired" => $qr_expired_time,
                    "barang_bawaan" => $request["barang_bawaan"],
                    "barang_ditinggal" => $request["barang_ditinggal"],
                    "nama_tahanan" => $nama_tahanan,
                    "photo" => 'img/foto/'.$photoName,
                    "qr_image" => "img/qr/",
                    "jumlah_tamu" => $jumlah_tamu_new,
                    "tipe_tamu" => $tipe_tamu,
                    "unique_id" => $unique_id,
                    "massgate_id" => $massgate_id,
                    "nama" => $request["tamu_nama"],
                    "no_identitas" => $request["tamu_no_identitas"],
                    "no_polisi" => $request["tamu_no_polisi"],
                    "no_kendaraan" => $request["no_kendaraan"],
                    "email" => $request["tamu_email"],
                    "alamat" => $request["tamu_alamat"],
                    "no_hp" => $request["tamu_no_hp"],
                    "name" => $pegawai->name,
                    "jabatan" => $pegawai->jabatan,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now()
                ]);

                //cari id untuk Update QR
                $kunjungan_baru = Kunjungan::select('*')
                ->where('id', $request['id_kunjungan'])
                ->orderByRaw('id DESC')
                ->first();

                //generate QR 
                $qr_image = 'img/qr/'.$fileName.'.png';
                QrCode::format('png')
                ->size(300)
                ->margin(3)
                ->generate($unique_id, '../public/'.$qr_image);
                
                //update QR
                Kunjungan::where('id', $kunjungan_baru['id'])->update(
                    array(
                        'qr_image' => $qr_image
                    )
                );

                foreach($request->input('nama_pengikut') as $key => $value) {
                    TamuPengikut::create([
                        'nama_pengikut'=>$value,
                        'id_kunjungan'=> $kunjungan_baru['id'],
                        'urutan_tamu'=> $key+1
                    ]);
                }

                foreach($request->input('jenis_kelamin_pengikut') as $key => $value) {
                    TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                    ->where('urutan_tamu',$key+1)
                    ->update(['jenis_kelamin'=>$value]);
                }

                foreach($request->input('is_anak') as $key => $value) {
                    TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])
                    ->where('urutan_tamu',$key+1)
                    ->update(['is_anak'=>$value]);
                }

                

                //create data for WA

                $tipe_nik = '';
                if($request["tamu_tipe_identitas"] == '1'){$tipe_nik = 'KTP';}
                elseif($request["tamu_tipe_identitas"] == '2'){$tipe_nik = 'SIM';}
                else{$tipe_nik = 'Passport';}
                
                $selected_pegawai = Pegawai::where('id', $request['id_pegawai'])->first();
                
                $selected_bidang = "Kejaksaan";

                $selected_jabatan = 
                DB::table('pegawai as p')
                ->select('p.jabatan as nama_jabatan')
                ->where('p.id', $request['id_pegawai'])
                ->first();

                
                
                $nama_pegawai_tujuan = $selected_pegawai->name;
                $nama_jabatan_tujuan = $selected_jabatan->nama_jabatan;
                $nama_satker_tujuan = $selected_satker->nama_satker;
                $nama_bidang_tujuan = $selected_bidang;

                
                $data_kunjungan = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                $numberKeys = array(
                    'helpdesk1',
                    'helpdesk2',
                    'helpdesk4'
                    
                );

                $randomNumber = array_rand($numberKeys);
                $numberKey = $numberKeys[$randomNumber];

                $number = $selected_pegawai->no_hp;
                $file = "https://bukutamu.kejaksaan.go.id/".$data_kunjungan->photo;
                $caption = "Salam sehat dan sukses selalu bapak/ibu ".$nama_pegawai_tujuan."\n\nMohon izin sebelumnya whatsapp ini memberikan notifikasi ke bapak/ibu tentang tamu yang akan menemui bapak/ibu\n\nBerikut biodata tamu yang ingin menemui bapak/ibu\n\nNama : ".$request['tamu_nama']."\nDengan Keperluan : ".$data_kunjungan->detail."\n\nPesan ini langsung tergenerate dari Aplikasi Bukutamu Kejaksaan\n\nTerimakasih Bapak/Ibu";
                // $isi_post = 'token='.$number_key.'&number='.$number.'&file='.$file.'&caption='.$caption;
                $isi_post = [
                    'sessions'=> $numberKey,
                    'target'=> $number,
                    'message'=> $caption,
                    'url'=> $file
                ];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 1,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $isi_post,
                ));
                try {
                    $lapor_tamu_kunjungan = curl_exec($curl);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($curl);
                

                $data_kunjungan = Kunjungan::select('*')
                ->where('id_tamu', $datatamu->id_tamu)
                ->orderByRaw('id DESC')
                ->first();

                $number_tamu = $request['tamu_no_hp'];
                $file_tamu =  "https://bukutamu.kejaksaan.go.id/".$data_kunjungan->qr_image;
                $caption_tamu = "Selamat datang di ".$nama_satker_tujuan."\nHalo ".$request['tamu_nama']."\n\nAnda telah melakukan permintaan untuk bertemu dengan : ".$nama_pegawai_tujuan." jabatan (".$nama_jabatan_tujuan.") dengan keperluan ".$data_kunjungan->detail."\n\nNo Antrian Anda : ".$data_kunjungan->guest_no."\n\nPesan ini dari Aplikasi Bukutamu Kejaksaan\nTerimakasih";
                // $isi_post_tamu = 'token='.$number_key.'&number='.$number_tamu.'&file='.$file_tamu.'&caption='.$caption_tamu;
                $isi_post_tamu = [
                    'sessions'=> $numberKey,
                    'target'=> $number_tamu,
                    'message'=> $caption_tamu,
                    'url'=> $file_tamu
                ];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 1,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $isi_post_tamu,
                ));
                try {
                    $lapor_tamu_kunjungan = curl_exec($curl);
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }
                curl_close($curl);

                

                 
                $satker = Satker::where('id_satker',$request["id_satker"])->select('nama_satker','parent_id','kode_satker')->first();
                $tokenPtsp = '35518ecc-b4a2-428c-813b-8595f1bb1df5';
                $dataPtsp = [
                    'nik_type'  => $tipe_tamu,
                    'uuid'  => $datatamu->uuid,
                    'nik'     => $request["tamu_no_identitas"],
                    'name'   => $request["tamu_nama"],
                    'kode_satker'      => $satker->kode_satker,
                    'type'       => $tipe_tamu,
                    'email'      => $request["tamu_email"],
                    'address'      => $request["tamu_alamat"],
                    'phone'      => $request["tamu_no_hp"],
                    'plat_kendaraan'      => $request["tamu_no_polisi"],
                    'status'      => "1",
                    'tujuan'      => $request["kunjungan_detail"],
                    'foto_kendaraan'      => 'img/foto/'.$vehiclePhotoName,
                    'foto_diri'      => 'img/foto/'.$photoName,
                    'jenis_kelamin'      => $request["jenis_kelamin"],
                    'unique_key'      => $unique_id,
                ];

                // return $dataPtsp;

                $apiPtsp = 'https://pelayanan.kejaksaan.go.id/api/simpan-tamu';
                $ClientPtsp = new Client([
                    'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                    'verify' => false
                ]);
                $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                    'form_params' => $dataPtsp
                ]);

                // $apiPtsp = 'https://ptsp-sumut.kejaksaanri.id/api/simpan-tamu';
                // $ClientPtsp = new Client([
                //     'headers' => ['Authorization' => 'Bearer ' . $tokenPtsp],
                //     'verify' => false
                // ]);
                // $postApiPtsp = $ClientPtsp->post($apiPtsp, [
                //     'form_params' => $dataPtsp
                // ]);


                $data_return = [
                    'data_tamu' => $datatamu,
                    'data_kunjungan' => $kunjungan_baru,
                    'nama_pegawai_tujuan' => $nama_pegawai_tujuan,
                    'nama_jabatan_tujuan' => $nama_jabatan_tujuan,
                    'nama_satker_tujuan' => $nama_satker_tujuan,
                    'nama_bidang_tujuan' => $nama_bidang_tujuan,
                ];

                $tanggal_hari_ini = Carbon::today()->toDateString();

                $tracingId = Str::uuid();
                $eventCode = 'TAMU_KNJGN';
                $id_satker = $request["id_satker"];
                $nama_satker = $satker->nama_satker;
                $parent_satker = $satker->parent_id;
                $kode_satker = $satker->kode_satker;
                $tanggal = $tanggal_hari_ini;
                $bulan = substr($tanggal_hari_ini,0,7);
                $total_kendaraan = 1;

                $summaryData = Array();
                $summaryData["tracingId"] = $tracingId;
                $summaryData["eventCode"] = $eventCode;
                $summaryData["summaryData"]["id_satker"] = $id_satker;
                $summaryData["summaryData"]["nama_satker"] = $nama_satker;
                $summaryData["summaryData"]["parent_satker"] = $parent_satker;
                $summaryData["summaryData"]["kode_satker"] = $kode_satker;
                $summaryData["summaryData"]["tanggal"] = $tanggal;
                $summaryData["summaryData"]["bulan"] = $bulan;
                $summaryData["summaryData"]["total_kendaraan"] = $total_kendaraan;
                
                $url = '43.231.129.14:8081/api/v1/realtime-event/create';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($summaryData),
                    'verify' => false
                ]);

                try {
                    $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                    DB::table('realtime_api')->insert(
                        array(
                            "tracingId" => $tracingId,
                            "eventCode" => $eventCode,
                            "summaryData" => json_encode($summaryData),
                            "created_at" => Carbon::now(),
                            "updated_at" => Carbon::now(),
                        )
                   );
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }    

                $tracingId2 = Str::uuid();
                $eventCode2 = 'TAMU_KNJGN_TAMU';
                $nama_satker = $satker->nama_satker;
                $parent_satker = $satker->parent_id;
                $kode_satker = $satker->kode_satker;
                $tanggal = $tanggal_hari_ini;
                $bulan = substr($tanggal_hari_ini,0,7);
                $total_kendaraan = 1;
                $master_tipe_tamu = DB::table('master_tipe_tamu')->where('id',$tipe_tamu)->first();
                $cari_anak = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('is_anak','1')->get());
                $cari_dewasa = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('is_anak','2')->get()) + 1;
                $cari_laki = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('jenis_kelamin','1')->get()) + count(Kunjungan::join('tamu','tamu.id_tamu','kunjungan.id_tamu')->where('kunjungan.id',$kunjungan_baru['id'])->where('tamu.jenis_kelamin','1')->get());
                $cari_perempuan = count(TamuPengikut::where('id_kunjungan',$kunjungan_baru['id'])->where('jenis_kelamin','2')->get()) + count(Kunjungan::join('tamu','tamu.id_tamu','kunjungan.id_tamu')->where('kunjungan.id',$kunjungan_baru['id'])->where('tamu.jenis_kelamin','2')->get());


                $summaryData2 = Array();
                $summaryData2["tracingId"] = $tracingId2;
                $summaryData2["eventCode"] = $eventCode2;
                $summaryData2["summaryData"]["id_satker"] = $id_satker;
                $summaryData2["summaryData"]["nama_satker"] = $nama_satker;
                $summaryData2["summaryData"]["parent_satker"] = $parent_satker;
                $summaryData2["summaryData"]["kode_satker"] = $kode_satker;
                $summaryData2["summaryData"]["tanggal"] = $tanggal;
                $summaryData2["summaryData"]["bulan"] = $bulan;
                $summaryData2["summaryData"]["id_jenis_tamu"] = $tipe_tamu;
                $summaryData2["summaryData"]["nama_jenis_tamu"] = $master_tipe_tamu->nama;
                $summaryData2["summaryData"]["total_tamu"] = $jumlah_tamu_new;
                $summaryData2["summaryData"]["total_anak"] = $cari_anak;
                $summaryData2["summaryData"]["total_dewasa"] = $cari_dewasa;
                $summaryData2["summaryData"]["total_laki"] = $cari_laki;
                $summaryData2["summaryData"]["total_perempuan"] = $cari_perempuan;
                
                $url = '43.231.129.14:8081/api/v1/realtime-event/create';
                $Client = new Client([
                    'headers' => ['Content-Type' => 'application/json'],
                    'body'    => json_encode($summaryData2),
                    'verify' => false
                ]);

                try {
                    $postRealtimeData = $Client->post($url, ['timeout' => 5]);
                    DB::table('realtime_api')->insert(
                        array(
                            "tracingId" => $tracingId2,
                            "eventCode" => $eventCode2,
                            "summaryData" => json_encode($summaryData2),
                            "created_at" => Carbon::now(),
                            "updated_at" => Carbon::now(),
                        )
                   );
                } catch (Exception $e) {
                        // do nothing, the timeout exception is intended
                }  

            }
            // $this->kunjungan($unique_id);
            Alert::success('Berhasil menyimpan data', 'Silahkan lakukan checkin');
            return redirect(url('/kunjungan/view',$unique_id));
        }else{
            Alert::warning('Data Tidak Berhasil Tersimpan', 'Pastikan anda telah mengisi foto');
            return back();
        }
    }

    public function kunjungan($unique_id){
        $data_personal = 
        DB::table('kunjungan as k')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->select('s.nama_satker as nama_satker', 's.slug as slug_satker', 'k.unique_id as unique_id', 'k.jumlah_tamu as jumlah_tamu','k.tipe_tamu as tipe_tamu','s.slug as slug' ,'t.jenis_kelamin as jenis_kelamin', 't.nama as nama_tamu', 'k.qr_image as qr_image', 'k.tanggal_expired as valid_sampai', 't.no_identitas as ktp_tamu', 't.no_polisi as plat_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', 'k.photo as foto_diri', 'k.photo_kendaraan as foto_kendaraan', 'k.created_at as tanggal_berkunjung', 'k.in as jam_masuk', 'k.out as jam_keluar',  'p.jabatan as jabatan_tujuan', 'p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();

        $kunjungan = DB::table('kunjungan as k')
        ->select('k.id_tamu as id_tamu','k.id as kunjungan_id')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->orderBy('k.id','desc')
        ->first();

        $riwayat_kunjungan = 
        DB::table('kunjungan as k')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','k.id_satker','=','s.id_satker')
        ->select('k.id as kunjungan_id','k.id_tamu as id_tamu','s.nama_satker as nama_satker','p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.created_at as tanggal_kunjungan','k.in as jam_masuk', 'k.out as jam_keluar')
        ->where('k.id_tamu', $kunjungan->id_tamu)
        ->orderBy('k.id','desc')
        ->get();

        $pengikut = 
        DB::table('kunjungan as k') 
        ->join('tamu_pengikut as tp','tp.id_kunjungan','=','k.id')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->get();


        return view('frontend.kunjungan', compact('data_personal','riwayat_kunjungan','pengikut'));
    }


    public function regist($unique_id){
        $data_personal = 
        DB::table('kunjungan as k')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','s.kode_satker','=','p.kode_satker')
        ->select('s.nama_satker as nama_satker', 'k.unique_id as unique_id', 's.slug as slug_satker', 'k.jumlah_tamu as jumlah_tamu','k.tipe_tamu as tipe_tamu','s.slug as slug' ,'t.jenis_kelamin as jenis_kelamin', 't.nama as nama_tamu', 'k.qr_image as qr_image', 'k.tanggal_expired as valid_sampai', 't.no_identitas as ktp_tamu', 't.no_polisi as plat_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', 'k.photo as foto_diri', 'k.photo_kendaraan as foto_kendaraan', 'k.created_at as tanggal_berkunjung', 'k.in as jam_masuk', 'k.out as jam_keluar',  'p.jabatan as jabatan_tujuan', 'p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();

        $kunjungan = DB::table('kunjungan as k')
        ->select('k.id_tamu as id_tamu','k.id as kunjungan_id')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->orderBy('k.id','desc')
        ->first();

        $riwayat_kunjungan = 
        DB::table('kunjungan as k') 
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','k.id_satker','=','s.id_satker')
        ->select('k.id as kunjungan_id','k.id_tamu as id_tamu','s.nama_satker as nama_satker','p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.created_at as tanggal_kunjungan','k.in as jam_masuk', 'k.out as jam_keluar')
        ->where('k.id_tamu', $kunjungan->id_tamu)
        ->orderBy('k.id','desc')
        ->get();

        $pengikut = 
        DB::table('kunjungan as k') 
        ->join('tamu_pengikut as tp','tp.id_kunjungan','=','k.id')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->get();


        return view('frontend.kunjunganregist', compact('data_personal','riwayat_kunjungan','pengikut'));
    }

    public function kunjunganview($unique_id){
        $data_personal = 
        DB::table('kunjungan as k')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->select('s.nama_satker as nama_satker', 'k.unique_id as unique_id', 's.slug as slug_satker', 'k.jumlah_tamu as jumlah_tamu','k.tipe_tamu as tipe_tamu','s.slug as slug' ,'t.jenis_kelamin as jenis_kelamin', 't.nama as nama_tamu', 'k.qr_image as qr_image', 'k.tanggal_expired as valid_sampai', 't.no_identitas as ktp_tamu', 't.no_polisi as plat_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', 'k.photo as foto_diri', 'k.photo_kendaraan as foto_kendaraan', 'k.created_at as tanggal_berkunjung', 'k.in as jam_masuk', 'k.out as jam_keluar',  'p.jabatan as jabatan_tujuan', 'p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();

        $kunjungan = DB::table('kunjungan as k')
        ->select('k.id_tamu as id_tamu','k.id as kunjungan_id')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->orderBy('k.id','desc')
        ->first();

        $riwayat_kunjungan = 
        DB::table('kunjungan as k') 
        ->join('satker as s','k.id_satker','=','s.id_satker')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->select('k.id as kunjungan_id','k.id_tamu as id_tamu','s.nama_satker as nama_satker','p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.created_at as tanggal_kunjungan','k.in as jam_masuk', 'k.out as jam_keluar')
        ->where('k.id_tamu', $kunjungan->id_tamu)
        ->orderBy('k.id','desc')
        ->get();

        $pengikut = 
        DB::table('kunjungan as k') 
        ->join('tamu_pengikut as tp','tp.id_kunjungan','=','k.id')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->get();


        return view('frontend.kunjunganview', compact('data_personal','riwayat_kunjungan','pengikut'));
    }

    public function kunjunganviewptsp($unique_id){
        $data_personal = 
        DB::table('kunjungan as k')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->select('s.nama_satker as nama_satker', 'k.unique_id as unique_id', 's.slug as slug_satker', 'k.jumlah_tamu as jumlah_tamu','k.tipe_tamu as tipe_tamu','s.slug as slug' ,'t.jenis_kelamin as jenis_kelamin', 't.nama as nama_tamu', 'k.qr_image as qr_image', 'k.tanggal_expired as valid_sampai', 't.no_identitas as ktp_tamu', 't.no_polisi as plat_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', 'k.photo as foto_diri', 'k.photo_kendaraan as foto_kendaraan', 'k.created_at as tanggal_berkunjung', 'k.in as jam_masuk', 'k.out as jam_keluar', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();
        

        $kunjungan = DB::table('kunjungan as k')
        ->select('k.id_tamu as id_tamu','k.id as kunjungan_id')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->orderBy('k.id','desc')
        ->first();

        $riwayat_kunjungan = 
        DB::table('kunjungan as k') 
        ->join('satker as s','k.id_satker','=','s.id_satker')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->select('k.id as kunjungan_id','k.id_tamu as id_tamu','s.nama_satker as nama_satker','p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.created_at as tanggal_kunjungan','k.in as jam_masuk', 'k.out as jam_keluar')
        ->where('k.id_tamu', $kunjungan->id_tamu)
        ->orderBy('k.id','desc')
        ->get();

        $pengikut = 
        DB::table('kunjungan as k') 
        ->join('tamu_pengikut as tp','tp.id_kunjungan','=','k.id')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->get();


        return view('frontend.kunjunganviewptsp', compact('data_personal','riwayat_kunjungan','pengikut'));
    }


    public function kunjungantilang($unique_id){

        $data_personal = 
        DB::table('kunjungan_tilang as k')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->select('k.id as id','k.created_at as created_at','k.unique_id as unique_id','s.nama_satker as nama_satker','k.nama_tamu as nama_tamu','k.qr_image as qr_image','k.guest_no as guest_no','k.photo as photo')
        ->where('k.unique_id', $unique_id)
        ->first();


        $data_tilang = 
        DB::table('kunjungan_tilang_detail as k')
        ->select('*')
        ->where('k.kunjungan_tilang_id', $data_personal->id)
        ->first();

       

        return view('frontend.kunjungantilang', compact('data_personal','data_tilang'));
    }

    public function kunjungantilang2($unique_id){

        $data_personal = 
        DB::table('kunjungan_tilang as k')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->select('*')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();

        return view('frontend.kunjungantilang2', compact('data_personal'));
    }

    public function kunjunganqr($unique_id){
        $data_personal = 
        DB::table('kunjungan as k')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->select('s.nama_satker as nama_satker', 'k.unique_id as unique_id', 's.slug as slug_satker',  'k.jumlah_tamu as jumlah_tamu','k.tipe_tamu as tipe_tamu','s.slug as slug' ,'t.jenis_kelamin as jenis_kelamin', 't.nama as nama_tamu', 'k.qr_image as qr_image', 'k.tanggal_expired as valid_sampai', 't.no_identitas as ktp_tamu', 't.no_polisi as plat_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', 'k.photo as foto_diri', 'k.photo_kendaraan as foto_kendaraan', 'k.created_at as tanggal_berkunjung', 'k.in as jam_masuk', 'k.out as jam_keluar',  'p.jabatan as jabatan_tujuan', 'p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();

        $kunjungan = DB::table('kunjungan as k')
        ->select('k.id_tamu as id_tamu','k.id as kunjungan_id')
        ->where('k.unique_id', $unique_id)
        ->orderBy('k.id','desc')
        ->first();

        $riwayat_kunjungan = 
        DB::table('kunjungan as k')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','k.id_satker','=','s.id_satker')
        ->select('k.id as kunjungan_id','k.id_tamu as id_tamu','s.nama_satker as nama_satker','p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.created_at as tanggal_kunjungan','k.in as jam_masuk', 'k.out as jam_keluar')
        ->where('k.id_tamu', $kunjungan->id_tamu)
        ->orderBy('k.id','desc')
        ->get();

        $pengikut = 
        DB::table('kunjungan as k') 
        ->join('tamu_pengikut as tp','tp.id_kunjungan','=','k.id')
        ->where('k.unique_id', $unique_id)
        ->orderBy('k.created_at','desc')
        ->get();


        return view('frontend.kunjunganqr', compact('data_personal','riwayat_kunjungan','pengikut'));
    }

    public function get_survey($kunjungan_id)
    {
        
     
        $data_survey = DB::table('survey_response')
        ->select('*')
        ->where('kunjungan_id', $kunjungan_id)
        ->get();


        
        return $data_survey;
    }

    public function qr_reader()
    {
        return view('frontend.qr-reader');
    }

    public function in_out($unique_id){
        $query_kunjungan = Kunjungan::where('unique_id', $unique_id)
        //status 1 masuk
        ->whereRaw('status != 2')
        //status 2  ditolak
        //status 3 keluar
        ->whereRaw('status != 4')
        ->orderBy('created_at','desc')
        //status 4 diblacklist
        ->first();

        if( empty($query_kunjungan) ) {
            return view('frontend.notfound');
        }

        if(date('Y-m-d H:i', strtotime($query_kunjungan->tanggal_expired)) < date('Y-m-d H:i')){
            return view('frontend.expired');
        }
        
        if(
            $query_kunjungan->in === null &&
            $query_kunjungan->out === null
        ) {
            Kunjungan::where('unique_id', $query_kunjungan->unique_id)->update([
                'in' => date('Y-m-d H:i:s'),
                'status' => 1
                
            ]);

            return redirect(url('/kunjungan/qr',$unique_id));

        }


        if ( $query_kunjungan->in !== null && $query_kunjungan->out === null ) {
            Kunjungan::where('unique_id', $query_kunjungan->unique_id)->update([
                'out' => date('Y-m-d H:i:s'),
                'status' => 3
            ]);


            $data_personal = 
            DB::table('kunjungan as k')
            ->join('satker as s','s.id_satker','=','k.id_satker')
            ->join('tamu as t','k.id_tamu','=','t.id_tamu')
            ->select('s.nama_satker as nama_satker', 's.slug as slug_satker','t.nama as nama_tamu',  't.no_hp as no_hp_tamu', DB::raw("CONCAT('survey/',s.slug,'/',t.no_identitas,'/',k.unique_id) AS url_survey"))
            ->orderBy('k.created_at','desc')
            ->where('k.unique_id', $unique_id)
            ->first();

            $url_survey = 'https://survey.kejaksaan.go.id/'.$data_personal->url_survey;

            $numberKeys = array(
                'helpdesk1',
                'helpdesk2',
                'helpdesk4'
                
            );
    
            $randomNumber = array_rand($numberKeys);
            $numberKey = $numberKeys[$randomNumber];
            
            $number_tamu = $data_personal->no_hp_tamu;
            $contenttext =  "Halo ".$data_personal->nama_tamu."\n\nTerimakasih telah melakukan kunjungan pada satuan kerja kami. \n\n Untuk meningkatkan pelayanan kami, anda dapat melakukan survey dari pelayanan yang kami berikan melalui link dibawah ini \n\nPesan ini dari Aplikasi Bukutamu Kejaksaan\nTerimakasih";
            $footertext = "KLIK TOMBOL DIBAWAH INI UNTUK GENERATE LINK SURVEY";
            $buttonid = $url_survey;
            $buttontext = $url_survey;
            $isi_post_tamu = [
                'sessions'=> $numberKey,
                'target'=> $number_tamu,
                'message'=> $contenttext,
                'textFooter' => 'Footer',
                'urlButton'=> 'SURVEY KEPUASAN',
                'responUrl' => $url_survey
            ];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $isi_post_tamu,
            ));
            try {
                $lapor_tamu_kunjungan = curl_exec($curl);
            } catch (Exception $e) {
                    // do nothing, the timeout exception is intended
            }
            curl_close($curl);

            
            
            return Redirect::to($url_survey);

        }
        return view('frontend.thankyou');
    }

    public function api_survey($satker, $nik, Request $request)
    {
        $token = $request->header('Authorization');


        
        // if($token !== 'Bearer '.env('APP_API_TOKEN')) {
        //     return response()->json(
        //         array(
        //             'code' => 403,
        //             'response' => 'failed',
        //             'message' => 'Invalid token',
        //             'datas' => [],
        //         ),
        //         403
        //     );
        // }

        if(empty($request->input('payload'))) {
            return response()->json(
                array(
                    'code' => 400,
                    'response' => 'failed',
                    'message' => 'Invalid data',
                    'datas' => [],
                ),
                400
            );
        }

        $get_tamu = Tamu::where('no_identitas', $nik)->first();

        if(empty($get_tamu)) {
            return response()->json(
                array(
                    'code' => 404,
                    'response' => 'failed',
                    'message' => 'Tamu tidak ditemukan',
                    'datas' => [],
                ),
                404
            );
        }

        $from = date('Y-m-d'.' '.'00:00:00');
        $to = date('Y-m-d'.' '.'23:59:59');
        $get_tamu_kunjungan = Kunjungan::whereBetween('created_at', [$from, $to])
            ->where('id_tamu', $get_tamu->id_tamu)
            ->orderByRaw('id DESC')
            ->first();

        if(empty($get_tamu_kunjungan)) {
            return response()->json(
                array(
                    'code' => 404,
                    'response' => 'failed',
                    'message' => 'Tamu tidak melakukan kunjungan hari ini',
                    'datas' => [],
                ),
                404
            );
        }

        if($get_tamu_kunjungan->in == null) {
            return response()->json(
                array(
                    'code' => 400,
                    'response' => 'failed',
                    'message' => 'Tamu belum melakukan checkin',
                    'datas' => [],
                ),
                400
            );
        }


        
        $payload = $request->input('payload');

        $tracingId2 = Str::uuid();
        $eventCode2 = 'TAMU_SURVEY';
        $id_satker = $get_tamu_kunjungan->id_satker;
        $satker = Satker::where('id_satker',$id_satker)->first();
        $nama_satker = $satker->nama_satker;
        $parent_satker = $satker->parent_id;
        $kode_satker = $satker->kode_satker;

        $summaryData2 = Array();
        $summaryData2["tracingId"] = $tracingId2;
        $summaryData2["eventCode"] = $eventCode2;
        $summaryData2["summaryData"]["id_satker"] = $id_satker;
        $summaryData2["summaryData"]["nama_satker"] = $nama_satker;
        $summaryData2["summaryData"]["parent_satker"] = $parent_satker;
        $summaryData2["summaryData"]["kode_satker"] = $kode_satker;
        $summaryData2["summaryData"]["question_answer"] = $payload;
        
        $url = '43.231.129.14:8081/api/v1/realtime-event/create';
        $Client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode($summaryData2),
            'verify' => false
        ]);

        try {
            $postRealtimeData = $Client->post($url, ['timeout' => 5]);
            DB::table('realtime_api')->insert(
                array(
                    "tracingId" => $tracingId2,
                    "eventCode" => $eventCode2,
                    "summaryData" => json_encode($summaryData2),
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now(),
                )
            );
        } catch (Exception $e) {
                // do nothing, the timeout exception is intended
        }    

        foreach($payload as $insert) {
            $response = new SurveyResponse();
            // $response->uuid = (string) Str::uuid();
            $response->identity_no = $get_tamu->no_identitas;
            $response->organisasi_id = $get_tamu_kunjungan->id_organisasi;
            $response->kunjungan_id = $get_tamu_kunjungan->id;
            $response->question = $insert['question'];
            $response->response = $insert['answer'];
            $response->status = 1;
            $response->save();
        }

        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
                'datas' => array(
                    'unique_kunjungan' => $get_tamu_kunjungan->unique_id,
                ),
            ),
            200
        );
    }

    public function api_checkin($unique_id, Request $request)
    {
        // $token = $request->header('Authorization');

        // if($token !== 'Bearer '.env('APP_API_TOKEN')) {
        //     return response()->json(
        //         array(
        //             'code' => 403,
        //             'response' => 'failed',
        //             'message' => 'Invalid token',
        //             'datas' => [],
        //         ),
        //         403
        //     );
        // }

        $data_personal = 
        DB::table('kunjungan as k')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->select('k.id as kunjungan_id','s.nama_satker as nama_satker_kunjungan', 's.slug as slug_satker', 'k.in as waktu_checkin', 'k.created_at as tanggal_kunjungan','p.nip as nip_pegawai', 'k.id_tamu as id_tamu','k.unique_id as unique_id', 'k.jumlah_tamu as jumlah_tamu',DB::raw('CASE WHEN k.tipe_tamu = 1 THEN "TAMU BIASA" WHEN k.tipe_tamu = 2 THEN "SAKSI" WHEN k.tipe_tamu = 3 THEN "TERSANGKA/TERDAKWA" WHEN k.tipe_tamu = 4 THEN "AHLI" WHEN k.tipe_tamu = 5 THEN "TAMU VVIP" ELSE "TIDAK DIKETAHUI" END as tipe_tamu'), DB::raw('CASE WHEN t.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'), 't.nama as nama_tamu',  't.no_identitas as ktp_tamu', 't.no_polisi as nomor_kendaraan_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo) as foto_tamu'), DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo_kendaraan) as foto_kendaraan'), 'k.in as jam_checkin', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();

        $pengikut = 
        DB::table('kunjungan as k') 
        ->join('tamu_pengikut as tp','tp.id_kunjungan','=','k.id')
        ->select('tp.nama_pengikut as nama_pengikut',DB::raw('CASE WHEN tp.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'),DB::raw('CASE WHEN tp.is_anak = 1 THEN "Anak-anak" ELSE "Dewasa" END as tipe_pengikut'))
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->get();

        $riwayat_kunjungan = 
        DB::table('kunjungan as k')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','k.id_satker','=','s.id_satker')
        ->select('s.nama_satker as nama_satker','p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.created_at as tanggal_kunjungan')
        ->where('k.id_tamu', $data_personal->id_tamu)
        ->orderBy('k.id','desc')
        ->get();

        $tokenekin = ('$2y$10$K0xXdngm7c0GvChV0O5q1eA85juNQ3S3NZ1wmZXFJazOnffWnbICm');
        // $url = ('https://mysimkari.kejaksaan.go.id/api/ekinerja');
        $url = '172.16.2.101/api/ekinerja';
        $headers = array('Authorization: Bearer '.$tokenekin, 'Content-Type: multipart/form-data');

        $post = [
            'nip' => $data_personal->nip_pegawai,
            'sasaran_kinerja' => 'Penerimaan Tamu ('.$data_personal->tipe_tamu.')',
            'desc_kinerja'   => 'Penerimaan Tamu ('.$data_personal->tipe_tamu.') Atas Nama '.$data_personal->nama_tamu.' pada tanggal '.$data_personal->tanggal_kunjungan.' Pada Satuan Kerja '.$data_personal->nama_satker_kunjungan,
            'lokasi_kegiatan' => $data_personal->nama_satker_kunjungan,
            'point' => 1,
            'nama_app' => 'BUKU TAMU',
            'url_file' => $data_personal->foto_tamu,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
        $response = curl_exec($ch);

        curl_close($ch);

        Kunjungan::where('unique_id', $unique_id)
        ->update([
            "status" => 7,
        ]);

        if($data_personal->waktu_checkin == null){
            Kunjungan::where('unique_id', $unique_id)
            ->update([
                'in' => date('Y-m-d H:i:s')
            ]);
        }



        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
                'datas' => array(
                    'data' => $data_personal,
                    'pengikut' => $pengikut,
                    'riwayat_kunjungan' => $riwayat_kunjungan,
                ),
            ),
            200
        );
        
    }

    public function api_gettamu($nip, Request $request)
    {
        // $token = $request->header('Authorization');

        // if($token !== 'Bearer '.env('APP_API_TOKEN')) {
        //     return response()->json(
        //         array(
        //             'code' => 403,
        //             'response' => 'failed',
        //             'message' => 'Invalid token',
        //             'datas' => [],
        //         ),
        //         403
        //     );
        // }

        $count_tamu = 
        DB::table('kunjungan as k')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->select('k.unique_id as unique_id')
        ->where('p.nip', $nip)
        ->get();

        $total_kunjungan = count($count_tamu);

        $data_personal = 
        DB::table('kunjungan as k')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','k.id_satker','=','s.id_satker')
        ->select('s.nama_satker as nama_satker_kunjungan','k.id as id_kunjungan','k.unique_id as unique_id', 't.nama as nama_tamu', 't.no_identitas as ktp_tamu', DB::raw('CASE WHEN t.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'), 'k.created_at as tanggal_kunjungan',   'k.in as waktu_checkin', 'k.out as waktu_checkout',   'k.jumlah_tamu as jumlah_tamu',DB::raw('CASE WHEN k.tipe_tamu = 1 THEN "TAMU BIASA" WHEN k.tipe_tamu = 2 THEN "SAKSI" WHEN k.tipe_tamu = 3 THEN "TERSANGKA/TERDAKWA" WHEN k.tipe_tamu = 4 THEN "AHLI" WHEN k.tipe_tamu = 5 THEN "TAMU VVIP" ELSE "TIDAK DIKETAHUI" END as tipe_tamu'), 't.no_polisi as nomor_kendaraan_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo) as foto_tamu'), DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo_kendaraan) as foto_kendaraan'), 'k.detail as keterangan_tujuan')
        ->where('p.nip', $nip)
        ->orderBy('k.created_at','desc')
        ->get();
          
        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
                'datas' => array(
                    'total_kunjungan' => $total_kunjungan,
                    'data' => $data_personal,
                ),
            ),
            200
        );
        
    }

    public function api_getdataizin(Request $request)
    {
        $token = $request->header('Authorization');

        if($token !== 'Bearer '.env('APP_API_TOKEN')) {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $get_data_izin = 
        DB::table('data_izin as d')
        ->select('*')
        ->get();
          
        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
                'datas' => array(
                    'data' => $get_data_izin
                ),
            ),
            200
        );
        
    }


    public function api_tamu_ptsp(Request $request)
    {
        $token = $request->header('Authorization');

        
        // if($token !== 'Bearer '.env('APP_API_TOKEN')) {
        //     return response()->json(
        //         array(
        //             'code' => 403,
        //             'response' => 'failed',
        //             'message' => 'Invalid token',
        //             'datas' => [],
        //         ),
        //         403
        //     );
        // }

        $qr_expired_time = date('Y-m-d H:i:s', strtotime('+24 hours'));
        $check_tamu = Tamu::where('no_identitas', $request->no_identitas)->count();
        $check_tamu_data = Tamu::where('no_identitas', $request->no_identitas)->first();
        if($check_tamu >= 1){
            File::put(public_path('img').'/foto/'.$request->photo_name, base64_decode($request->photo));
            Tamu::where('no_identitas', $request->no_identitas)
            ->update([
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'no_hp' => $request->no_hp,    
                'unique_key' => $request->unique_key,    
                'latest_photo' => $request->photo_name,    
                'is_anak' => '2',
                'updated_at'        => Carbon::now()
            ]);

            Kunjungan::create([
                "id_tamu" => $check_tamu_data->id_tamu,
                "id_satker" => $request->id_satker,
                "kode_satker" => $request->kode_satker,
                "id_organisasi" => $request["id_organisasi"],
                "id_pegawai" => 'tamu-tidak-mengunjungi-siapapun',
                "detail" => 'Menggunakan Layanan',
                "tanggal_expired" => $qr_expired_time,
                "photo" => 'img/foto/'.$request->photo_name,
                "qr_image" => "img/qr/",
                "status" => '9',
                "tipe_tamu" => '1',
                "unique_id" => $request->unique_key,
                "nama" => $request->nama,
                "no_identitas" => $request->no_identitas,
                "email" => $request->email,
                "alamat" => $request->alamat,
                "no_hp" => $request->no_hp,
                "name" => '-',
                "jabatan" => '-',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now()
            ]);
        }else{
            File::put(public_path('img').'/foto/'.$request->photo_name, base64_decode($request->photo));
            $data_tamu = new Tamu();
            $data_tamu->tipe_identitas = "1";
            $data_tamu->no_identitas = $request->no_identitas;
            $data_tamu->nama = $request->nama;
            $data_tamu->jenis_kelamin = $request->jenis_kelamin;
            $data_tamu->email = $request->email;
            $data_tamu->alamat = $request->alamat;
            $data_tamu->no_hp = $request->no_hp;
            $data_tamu->unique_key = $request->unique_key;
            $data_tamu->latest_photo = $request->photo_name;
            $data_tamu->is_anak = "2";
            $data_tamu->tipe_tamu = "8";
            $data_tamu->status_blacklist = "5";
            $data_tamu->save();

            Kunjungan::create([
                "id_tamu" => $data_tamu->id_tamu,
                "id_satker" => $request->id_satker,
                "kode_satker" => $request->kode_satker,
                "id_organisasi" => $request["id_organisasi"],
                "id_pegawai" => 'tamu-tidak-mengunjungi-siapapun',
                "detail" => 'Menggunakan Layanan',
                "tanggal_expired" => $qr_expired_time,
                "photo" => 'img/foto/'.$request->photo_name,
                "qr_image" => "img/qr/",
                "status" => '9',
                "tipe_tamu" => '1',
                "unique_id" => $request->unique_key,
                "tipe_pelayanan" => '9',
                "nama" => $request->nama,
                "no_identitas" => $request->no_identitas,
                "email" => $request->email,
                "alamat" => $request->alamat,
                "no_hp" => $request->no_hp,
                "name" => '-',
                "jabatan" => '-',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now()
            ]);
        }

        

        
        //generate QR 
        $qr_image = 'img/qr/'.$request->unique_key.'.png';
        QrCode::format('png')
        ->size(300)
        ->margin(3)
        ->generate($request->unique_key, '../public/'.$qr_image);


        $numberKeys = array(
            'helpdesk1',
            'helpdesk2',
            'helpdesk4'
            
        );

        $randomNumber = array_rand($numberKeys);
        $numberKey = $numberKeys[$randomNumber];

        
        $number_tamu = $request['no_hp'];
        $file_tamu =  "https://bukutamu.kejaksaan.go.id/".$qr_image;
        $caption_tamu = "Selamat datang di Pelayanan Terpadu Kejaksaan Republik Indonesia"."\n\nHalo ".$request->nama."\n\nTerimakasih telah melakukan pendaftaran pelayanan pada sistem Pelayanan Terpadu Kejaksaan\n\n Simpan QR Code yang dikirimkan oleh Whatsapp ini sebagai tanda pengenal anda pada sistem Pelayanan Terpadu Kejaksaan";;
        // $isi_post_tamu = 'token='.$number_key.'&number='.$number_tamu.'&file='.$file_tamu.'&caption='.$caption_tamu;
        $isi_post_tamu = [
            'sessions'=> $numberKey,
            'target'=> $number_tamu,
            'message'=> $caption_tamu,
            'url'=> $file_tamu
        ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://waotp.kejaksaanri.id/api/sendmedia',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 1,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $isi_post_tamu,
        ));

        try {
            $lapor_tamu_kunjungan = curl_exec($curl);
        } catch (Exception $e) {
                // do nothing, the timeout exception is intended
        }
        curl_close($curl);

        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
                'datas' => array(
                ),
            ),
            200
        );
        
    }

    public function api_gettamu_detail($unique_id, Request $request)
    {
        // $token = $request->header('Authorization');

        // if($token !== 'Bearer '.env('APP_API_TOKEN')) {
        //     return response()->json(
        //         array(
        //             'code' => 403,
        //             'response' => 'failed',
        //             'message' => 'Invalid token',
        //             'datas' => [],
        //         ),
        //         403
        //     );
        // }

        $data_personal = 
        DB::table('kunjungan as k')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->select('k.id as kunjungan_id','s.nama_satker as nama_satker_kunjungan','k.out as waktu_checkout', 's.slug as slug_satker', 'k.in as waktu_checkin', 'k.created_at as tanggal_kunjungan','p.nip as nip_pegawai', 'k.id_tamu as id_tamu','k.unique_id as unique_id', 'k.jumlah_tamu as jumlah_tamu',DB::raw('CASE WHEN k.tipe_tamu = 1 THEN "TAMU BIASA" WHEN k.tipe_tamu = 2 THEN "SAKSI" WHEN k.tipe_tamu = 3 THEN "TERSANGKA/TERDAKWA" WHEN k.tipe_tamu = 4 THEN "AHLI" WHEN k.tipe_tamu = 5 THEN "TAMU VVIP" ELSE "TIDAK DIKETAHUI" END as tipe_tamu'), DB::raw('CASE WHEN t.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'), 't.nama as nama_tamu',  't.no_identitas as ktp_tamu', 't.no_polisi as nomor_kendaraan_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo) as foto_tamu'), DB::raw('CONCAT("https://bukutamu.kejaksaan.go.id/",k.photo_kendaraan) as foto_kendaraan'),  'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();

        $pengikut = 
        DB::table('kunjungan as k') 
        ->join('tamu_pengikut as tp','tp.id_kunjungan','=','k.id')
        ->select('tp.nama_pengikut as nama_pengikut',DB::raw('CASE WHEN tp.jenis_kelamin = 1 THEN "Laki-laki" ELSE "Perempuan" END as jenis_kelamin'),DB::raw('CASE WHEN tp.is_anak = 1 THEN "Anak-anak" ELSE "Dewasa" END as tipe_pengikut'))
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->get();

        $riwayat_kunjungan = 
        DB::table('kunjungan as k')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->join('satker as s','k.id_satker','=','s.id_satker')
        ->select('s.nama_satker as nama_satker','p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.created_at as tanggal_kunjungan')
        ->where('k.id_tamu', $data_personal->id_tamu)
        ->orderBy('k.id','desc')
        ->get();
          
        return response()->json(
            array(
                'code' => 200,
                'response' => 'success',
                'message' => 'Success',
                'datas' => array(
                    'data' => $data_personal,
                    'pengikut' => $pengikut,
                    'riwayat_kunjungan' => $riwayat_kunjungan,
                ),
            ),
            200
        );
        
    }

    public function konfirmasi($unique_id){

        $data = Kunjungan::where('unique_id',$unique_id)->first();
        return view('frontend.konfirmasi', compact('data'));

    }

    public function update_konfirmasi(Request $request, $unique_id){

        $data = 
        DB::table('kunjungan as k')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->join('tamu as t','k.id_tamu','=','t.id_tamu')
        ->join('pegawai as p','p.id','=','k.id_pegawai')
        ->select('s.nama_satker as nama_satker', 'k.status as status' , 'k.reject_reason as reject_reason','t.nama as nama_tamu', 'k.qr_image as qr_image', 'k.tanggal_expired as valid_sampai', 't.no_identitas as ktp_tamu', 't.no_polisi as plat_tamu', 't.no_hp as no_hp_tamu', 't.email as email_tamu', 't.alamat as alamat_tamu', 'k.photo as foto_diri', 'k.photo_kendaraan as foto_kendaraan', 'k.created_at as tanggal_berkunjung', 'k.in as jam_masuk', 'k.out as jam_keluar',  'p.jabatan as jabatan_tujuan', 'p.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();

        $bidang_tujuan = "Kejaksaan";


         return view('frontend.thankyou-admin');
        
    }

    public function download_pdf($unique_id){

        $data = 
        DB::table('kunjungan as k')
        ->join('satker as s','s.id_satker','=','k.id_satker')
        ->select('s.nama_satker as nama_satker', 'k.unique_id as unique_id', 'k.jumlah_tamu as jumlah_tamu','k.tipe_tamu as tipe_tamu','s.slug as slug' , 'k.nama as nama_tamu', 'k.qr_image as qr_image', 'k.tanggal_expired as valid_sampai', 'k.no_identitas as ktp_tamu', 'k.no_polisi as plat_tamu', 'k.no_hp as no_hp_tamu', 'k.email as email_tamu', 'k.alamat as alamat_tamu', 'k.photo as foto_diri', 'k.photo_kendaraan as foto_kendaraan', 'k.created_at as tanggal_berkunjung', 'k.in as jam_masuk', 'k.out as jam_keluar', 'k.jabatan_pegawai as bidang_tujuan', 'k.jabatan_pegawai as jabatan_tujuan', 'k.name as nama_tujuan', 'k.detail as keterangan_tujuan','k.guest_no as nomor_antrian')
        ->orderBy('k.created_at','desc')
        ->where('k.unique_id', $unique_id)
        ->first();
        // $pdf = PDF::loadview('frontend.cetak');
        // // ->setPaper('a4', 'landscape');
        // return $pdf->download('laporan-pegawai-pdf');

        // $pdf_name = 'KunjunganGatePass'.$unique_id;

        // $pdf = PDF::loadView('frontend.cetak', compact('data'))
        //     ->setPaper('a4', 'landscape')
        //     ->save('../public/pdf/'.$pdf_name.'.pdf');
        // return $pdf->download($pdf_name . '.pdf');

        return view('frontend.cetak', compact('data'));
    }
   
}
