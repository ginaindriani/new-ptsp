<?php

namespace App\Http\Controllers;

use App\DataTables\DataTilangDataTable;
use App\DataTables\DpoDataTable;
use App\DataTables\LelangDataTable;
use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\Dpo;
use App\Models\KunjunganTilang;
use App\Models\Satker;
use App\Models\Sliders;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class DataTilangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(DataTilangDataTable $dataTable, Request $request)
    {
        return $dataTable->render('pages.master.tilang.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
        }else{
            $satker = Satker::get();
        }

        return view("pages.master.tilang.create", compact('satker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        

        $this->validate($request, [
            'id_satker' => 'required',
            'nama_barang_lelang' => 'required',
            'deskripsi_barang' => 'required',
            'harga_barang' => 'required',
            'status_laku' => 'required',
            'foto' => 'required',
        ]);

        $nama_barang_lelang = jsEscape($request->nama_barang_lelang);
        $deskripsi_barang = jsEscape($request->deskripsi_barang);
        $harga_barang = jsEscape($request->harga_barang);

        if( !empty($request->foto) ) {
            $validator = Validator::make($request->all(), [
                'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }

            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "lelang_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/lelang/images' ;
            $file->move($destinationPath,$fileName);
        }

        $lelang = Lelang::create([
            'id_satker'  => $request->id_satker,
            'nama_barang_lelang'  => $nama_barang_lelang,
            'tanggal_lelang'  => $request->tanggal_lelang,
            'link_lelang'  => $request->link_lelang,
            'deskripsi_barang' => $deskripsi_barang,
            'harga_barang' => $harga_barang,
            'status_laku' => $request->status_laku,
            'foto' => 'lelang/images/'.$fileName,
            'is_active' => '1',
        ]);
        
        return  redirect()->route('tilang.index')->with(["success" => "Data berhasil ditambah"]);
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function edit($id)
    {

        $user = auth()->user();
        
        $satker = Satker::get();
        

        $tilang = KunjunganTilang::findOrFail($id);
        $selfsatker = Satker::where('id_satker',$lelang->id_satker)->first();
        return view('pages.master.tilang.edit', compact('tilang','satker','selfsatker'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nama_barang_lelang = jsEscape($request->nama_barang_lelang);
        $deskripsi_barang = jsEscape($request->deskripsi_barang);
        $harga_barang = jsEscape($request->harga_barang);

        $lelang = Lelang::findOrFail($id);
        if( !empty($request->foto) ) {
            $validator = Validator::make($request->all(), [
                'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "lelang_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/lelang/images' ;
            $file->move($destinationPath,$fileName);

            $lelang->update([
                'id_satker'  => $request->id_satker,
                'nama_barang_lelang'  => $nama_barang_lelang,
                'deskripsi_barang' => $deskripsi_barang,
                'tanggal_lelang'  => $request->tanggal_lelang,
                'link_lelang'  => $request->link_lelang,
                'harga_barang' => $harga_barang,
                'status_laku' => $request->status_laku,
                'foto' => 'lelang/images/'.$fileName,
                'is_active' => '1',
            ]);
        }else{
            $lelang->update([
                'id_satker'  => $request->id_satker,
                'nama_barang_lelang'  => $nama_barang_lelang,
                'deskripsi_barang' => $deskripsi_barang,
                'harga_barang' => $harga_barang,
                'status_laku' => $request->status_laku,
                'is_active' => '1',
            ]);
        }
        return  redirect()->route('tilang.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lelang = Lelang::findOrFail($id);
        $lelang->delete();
        return  redirect()->route('tilang.index')->with(["success" => "Data berhasil dihapus"]);
    }

}
