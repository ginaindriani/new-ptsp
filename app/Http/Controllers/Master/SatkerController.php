<?php

namespace App\Http\Controllers\Master;

use App\DataTables\SatkerDataTable;
use App\Http\Controllers\Controller;
use App\Models\Satker;
use Illuminate\Http\Request;

class SatkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(SatkerDataTable $dataTable)
    {
        return $dataTable->render('pages.master.satker.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $satker = $this->listOptionSatker();
        $tipeSatker = $this->tipeSatker();
        return view('pages.master.satker.create', compact('satker', 'tipeSatker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_satker' => 'required',
            'tipe_satker' => 'required'
        ]);

        $satker = Satker::firstOrCreate([
            'nama_satker'  => $request->nama_satker,
            'parent_id' => $request->parent_id,
            'tipe_satker' => $request->tipe_satker,
            'alamat_satker' => $request->alamat_satker,
            'telp_satker' => $request->telp_satker,
            'website_satker' => $request->website_satker,
        ]);

        return  redirect()->route('satker.index')->with(["success" => "Data berhasil ditambah"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Satker::findOrFail($id);
        $satker = $this->listOptionSatker();

        $tipeSatker = $this->tipeSatker();

        return view('pages.master.satker.edit', compact('data', 'satker', 'tipeSatker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_satker' => 'required',
            'tipe_satker' => 'required'
        ]);
        
        $satker = Satker::findOrFail($id);

        $satker->update([
            'nama_satker'  => $request->nama_satker,
            'parent_id' => $request->parent_id,
            'tipe_satker' => $request->tipe_satker,
            'alamat_satker' => $request->alamat_satker,
            'telp_satker' => $request->telp_satker,
            'website_satker' => $request->website_satker,
        ]);

        return  redirect()->route('satker.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $satker = Satker::findOrFail($id);
        $satker->delete();
        return  redirect()->route('satker.index')->with(["success" => "Data berhasil dihapus"]);
    }

    private function listOptionSatker($id = null)
    {
        $satker = Satker::where('tipe_satker', '!=', '4')
                        ->select('id_satker', 'nama_satker');
        if ($id != null) {
            $satker = $satker->where('id_satker', '!=', $id);
        }
        $satker = $satker->order()
                            ->get();
        $satker->prepend($this->prependSatker());

        return $satker;
    }
    
    private function prependSatker()
    {
        $prepend = collect([
            "id_satker" => "",
            "nama_satker" => "--Pilih Satker--",
            "label_type_satker" => null
        ]);

        return $prepend;
    }

    private function tipeSatker()
    {
        $tipeSatker = [
            "1" => "Kejaksaan Agung",
            "2" => "Kejaksaan Tinggi",
            "3" => "Kejaksaan Negeri",
            "4" => "Cabang Kejaksaan Negeri"
        ];

        return $tipeSatker;
    }

    public function profile()
    {
        $data = Satker::findOrFail(auth()->user()->organisasi->id_satker);

        return view('master.satker.profile', compact('data'));
    }

    public function profile_update(Request $request, $id)
    {
        $satker = Satker::findOrFail($id);

        $satker->update([
            'alamat_satker' => $request->alamat_satker,
            'telp_satker' => $request->telp_satker,
            'website_satker' => $request->website_satker,			
            'city' => $request->city,
        ]);

        return  redirect()->route('satker.profile')->with(["success" => "Data berhasil diubah"]);
    }
}
