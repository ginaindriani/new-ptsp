<?php

namespace App\Http\Controllers\Master;

use App\DataTables\BeritaDataTable;
use App\DataTables\DpoDataTable;
use App\DataTables\PengumumanDataTable;
use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\Dpo;
use App\Models\Pengumuman;
use App\Models\Satker;
use App\Models\Sliders;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(PengumumanDataTable $dataTable, Request $request)
    {
        return $dataTable->render('pages.master.pengumuman.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
        }else{
            $satker = Satker::get();
        }

        return view("pages.master.pengumuman.create", compact('satker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'id_satker' => 'required',
            'judul_pengumuman' => 'required',
            'deskripsi_pengumuman' => 'required',
            'foto' => 'required',
        ]);

        $judul_pengumuman = jsEscape($request->judul_pengumuman);
        $deskripsi_pengumuman = jsEscape($request->deskripsi_pengumuman);

        if( !empty($request->foto) ) {
            $validator = Validator::make($request->all(), [
                'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }

            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "pengumuman" . $rand. "." . $extension; 
            $destinationPath = public_path().'/pengumuman/images' ;
            $file->move($destinationPath,$fileName);
        }

        $pengumuman = Pengumuman::create([
            'id_satker'  => $request->id_satker,
            'judul_pengumuman'  => $judul_pengumuman,
            'deskripsi_pengumuman' => $deskripsi_pengumuman,
            'tanggal'  => $request->tanggal,
            'foto' => 'pengumuman/images/'.$fileName,
            'is_active' => '1',
        ]);
        
        return  redirect()->route('pengumuman.index')->with(["success" => "Data berhasil ditambah"]);
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function edit($id)
    {

        $user = auth()->user();
        
        $satker = Satker::get();
        

        $pengumuman = Pengumuman::findOrFail($id);
        $selfsatker = Satker::where('id_satker',$pengumuman->id_satker)->first();
        return view('pages.master.pengumuman.edit', compact('pengumuman','satker','selfsatker'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $judul_pengumuman = jsEscape($request->judul_pengumuman);
        $deskripsi_pengumuman = jsEscape($request->deskripsi_pengumuman);

        $pengumuman = Pengumuman::findOrFail($id);
        if( !empty($request->foto) ) {
            $validator = Validator::make($request->all(), [
                'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "pengumuman_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/pengumuman/images' ;
            $file->move($destinationPath,$fileName);

            $pengumuman->update([
                'id_satker'  => $request->id_satker,
                'judul_pengumuman'  => $judul_pengumuman,
                'deskripsi_pengumuman' => $deskripsi_pengumuman,
                'tanggal'  => $request->tanggal,
                'foto' => 'pengumuman/images/'.$fileName,
                'is_active' => '1',
            ]);
        }else{
            $pengumuman->update([
                'id_satker'  => $request->id_satker,
                'judul_pengumuman'  => $judul_pengumuman,
                'tanggal'  => $request->tanggal,
                'deskripsi_pengumuman' => $deskripsi_pengumuman,
                'is_active' => '1',
            ]);
        }
        return  redirect()->route('pengumuman.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengumuman = Pengumuman::findOrFail($id);
        $pengumuman->delete();
        return  redirect()->route('pengumuman.index')->with(["success" => "Data berhasil dihapus"]);
    }

    public function detail($id)
    {
        $pengumuman = Pengumuman::join('satker','pengumuman.id_satker','=','satker.id_satker')->findOrFail($id);
        return view("cetak.layanan.pengumumandetail", compact('pengumuman'));
    }

}
