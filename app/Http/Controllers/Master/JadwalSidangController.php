<?php

namespace App\Http\Controllers\Master;

use App\DataTables\BeritaDataTable;
use App\DataTables\DpoDataTable;
use App\DataTables\JadwalSidangDataTable;
use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\Dpo;
use App\Models\JadwalSidang;
use App\Models\JadwalSidangJaksa;
use App\Models\JadwalSidangTerdakwa;
use App\Models\Pegawai;
use App\Models\Satker;
use App\Models\Sliders;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use PhpParser\Node\Expr\AssignOp\Concat;

class JadwalSidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(JadwalSidangDataTable $dataTable, Request $request)
    {
        return $dataTable->render('pages.master.jadwalsidang.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
            $pegawai = Pegawai::where('kode_satker',$satker[0]->kode_satker)->select('nip','nama')->get();
            $stepSidang = DB::table('master_step_sidang')->get();
        }else{
            $satker = Satker::get();
            $pegawai = Pegawai::select('nip','nama')->get();
            $stepSidang = DB::table('master_step_sidang')->get();
        }

        return view("pages.master.jadwalsidang.create", compact('satker','pegawai','stepSidang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request;

        $this->validate($request, [
            'id_satker' => 'required',
            'perkara' => 'required',
            'tahapan_sidang' => 'required',
            'pasal' => 'required',
            'daftar_terdakwa' => 'required',
            'tanggal_sidang' => 'required',
            'daftar_jaksa' => 'required'
        ]);

        $pasal = jsEscape($request->pasal);
        $daftar_terdakwa = jsEscape($request->daftar_terdakwa);
        $perkara = jsEscape($request->perkara);
        $daftar_jaksa = jsEscape($request->daftar_jaksa);
        $keterangan = jsEscape($request->keterangan);

        $jadwalSidang = JadwalSidang::create([
            'id_satker'  => $request->id_satker,
            'perkara' => $perkara,
            'tahap_sidang' => $request->tahapan_sidang,
            'tanggal_sidang' => $request->tanggal_sidang,
            'pasal' => $pasal,
            'keterangan' => $keterangan,
        ]);

        $list_nama_terdakwa = "";
        foreach($daftar_terdakwa as $dt){
            JadwalSidangTerdakwa::create([
                'id_jadwal_sidang'  => $jadwalSidang->id,
                'nama_terdakwa'  => $dt,
            ]);
            if($list_nama_terdakwa == ""){
                $list_nama_terdakwa = $dt;
            }else{
                $list_nama_terdakwa = $list_nama_terdakwa.", ". $dt;
            }
            
        }

        $list_nama_jaksa = "";
        foreach($daftar_jaksa as $dj){
            $pegawai = Pegawai::where('nip',$dj)->select('nama')->first();
            JadwalSidangJaksa::create([
                'id_jadwal_sidang'  => $jadwalSidang->id,
                'nip'  => $dj,
                'nama_jaksa'  => $pegawai->nama,
            ]);

            if($list_nama_jaksa == ""){
                $list_nama_jaksa = $pegawai->nama;
            }else{
                $list_nama_jaksa = $list_nama_jaksa.", ". $pegawai->nama;
            }
        }
        
        $jadwalSidang->update([
            'daftar_jaksa'  => $list_nama_jaksa,
            'daftar_terdakwa'  => $list_nama_terdakwa,
        ]);
        
        return  redirect()->route('jadwalsidang.index')->with(["success" => "Data berhasil ditambah"]);
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function edit($id)
    {

        $user = auth()->user();
        $jadwalsidang = JadwalSidang::findOrFail($id);
        $selfsatker = Satker::where('id_satker',$jadwalsidang->id_satker)->first();

        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
            $pegawai = Pegawai::where('kode_satker',$user->kode_satker)->select('nip','nama')->get();
            $stepSidang = DB::table('master_step_sidang')->get();
        }else{
            $satker = Satker::get();
            $pegawai = Pegawai::select('nip','nama')->get();
            $stepSidang = DB::table('master_step_sidang')->get();
        }

        return view('pages.master.jadwalsidang.edit', compact('jadwalsidang','satker','selfsatker','pegawai','stepSidang'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // return $request;

        $pasal = jsEscape($request->pasal);
        $daftar_terdakwa = jsEscape($request->daftar_terdakwa);
        $perkara = jsEscape($request->perkara);
        $daftar_jaksa = jsEscape($request->daftar_jaksa);
        $keterangan = jsEscape($request->keterangan);

        $jadwalSidang = JadwalSidang::findOrFail($id);
       
        $jadwalSidang->update([
            'id_satker'  => $request->id_satker,
            'perkara' => $perkara,
            'tahap_sidang' => $request->tahapan_sidang,
            'tanggal_sidang' => $request->tanggal_sidang,
            'pasal' => $pasal,
            'keterangan' => $keterangan,
        ]);

        if($daftar_terdakwa != [null]){
            DB::table('jadwal_sidang_terdakwa')->where('id_jadwal_sidang', $jadwalSidang->id)->delete();
            $list_nama_terdakwa = "";
            foreach($daftar_terdakwa as $dt){
                JadwalSidangTerdakwa::create([
                    'id_jadwal_sidang'  => $jadwalSidang->id,
                    'nama_terdakwa'  => $dt,
                ]);
                if($list_nama_terdakwa == ""){
                    $list_nama_terdakwa = $dt;
                }else{
                    $list_nama_terdakwa = $list_nama_terdakwa.", ". $dt;
                }

                $jadwalSidang->update([
                    'daftar_terdakwa'  => $list_nama_terdakwa,
                ]);
                
            } 
        }

        if($daftar_jaksa != [null]){
            DB::table('jadwal_sidang_jaksa')->where('id_jadwal_sidang', $jadwalSidang->id)->delete();
            $list_nama_jaksa = "";
            foreach($daftar_jaksa as $dj){
                $pegawai = Pegawai::where('nip',$dj)->select('nama')->first();
                JadwalSidangJaksa::create([
                    'id_jadwal_sidang'  => $jadwalSidang->id,
                    'nip'  => $dj,
                    'nama_jaksa'  => $pegawai->nama,
                ]);

                if($list_nama_jaksa == ""){
                    $list_nama_jaksa = $pegawai->nama;
                }else{
                    $list_nama_jaksa = $list_nama_jaksa.", ". $pegawai->nama;
                }
            }
            $jadwalSidang->update([
                'daftar_jaksa'  => $list_nama_jaksa,
            ]);
        }
        
        
        return  redirect()->route('jadwalsidang.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jadwalsidang = JadwalSidang::findOrFail($id);
        $jadwalsidang->delete();
        return  redirect()->route('jadwalsidang.index')->with(["success" => "Data berhasil dihapus"]);
    }

    // public function getNip(Request $request)
    // {

    //     $limit = $request->filled("limit") ? $request->input('limit') : 10;
    //     $limit = $limit <= 100 ? $limit : 100;
    //     $query = DB::table("pegawais")
    //         ->select([
    //             DB::raw('pegawais.nip as id'),
    //             DB::raw('CONCAT(pegawais.nama, " ", pegawais.nip) as text')]);
        
    //     if ($request->filled("keyword")) {
    //         $query->where(function ($q) use ($request) {
    //             $q->where("pegawais.nip", "like", "%" . $request->keyword . "%")
    //                 ->orWhere("pegawais.nama", "like", "%" . $request->keyword . "%");
    //         });
    //     }

    //     $query          = $query->orderBy("pegawais.nama", "asc");
    //     $query          = $query->orderBy("pegawais.nip", "asc");
    //     $dataQuery      = $query->paginate($limit);
    //     $dataResults    = $dataQuery->items();
    //     $results = array(
    //         "results" => $dataResults,
    //         "pagination" => array(
    //             "more" => $dataQuery->hasMorePages()
    //         )
    //     );
    //     return $results;
    // }

    public function getNip(Request $request)
    {

        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
            $pegawai = Pegawai::where('kode_satker',$satker[0]->kode_satker)->select('nip','nama')->get();
        }else{
            $pegawai = Pegawai::select('nip','nama')->get();
        }
        return $pegawai;
    }
}
