<?php

namespace App\Http\Controllers\Master;

use App\DataTables\BidangDataTable;
use App\Http\Controllers\Controller;
use App\Models\Bidang;
use Illuminate\Http\Request;

class BidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(BidangDataTable $dataTable)
    {
        return $dataTable->render('pages.master.bidang.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bidang = $this->listOptionBidang();
        $mainBidang = $this->listOptionMainBidang();
        return view('pages.master.bidang.create', compact('bidang', 'mainBidang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_bidang' => 'required',
            'nama_bidang_full' => 'required',
        ]);

        $bidang = Bidang::firstOrCreate([
            'nama_bidang'  => $request->nama_bidang,
            'nama_bidang_full'  => $request->nama_bidang_full,
            'parent_id' => $request->parent_id,
            'main_parent_id' => $request->main_parent_id
        ]);

        return  redirect()->route('bidang.index')->with(["success" => "Data berhasil ditambah"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataBidang = Bidang::findOrFail($id);
        $bidang = $this->listOptionBidang();
        $mainBidang = $this->listOptionMainBidang();

        return view('pages.master.bidang.edit', compact('dataBidang', 'bidang', 'mainBidang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_bidang' => 'required'
        ]);
        
        $bidang = Bidang::findOrFail($id);

        $bidang->update([
            'nama_bidang'  => $request->nama_bidang,
            'parent_id' => $request->parent_id,
            'main_parent_id' => $request->main_parent_id
        ]);

        return  redirect()->route('bidang.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bidang = Bidang::findOrFail($id);
        $bidang->delete();
        return  redirect()->route('bidang.index')->with(["success" => "Data berhasil dihapus"]);
    }

    private function listOptionBidang($id = null)
    {
        $bidang = Bidang::select('id_bidang', 'nama_bidang');
        if ($id != null) {
            $bidang = $bidang->where('id_bidang', '!=', $id);
        }
        $bidang = $bidang->order()
                            ->get();
        $bidang->prepend($this->prependBidang());

        return $bidang;
    }

    private function listOptionMainBidang()
    {
        $bidang = Bidang::select('id_bidang', 'nama_bidang')
                            ->whereIn('parent_id', [1, 2])
                            ->whereNotIn('id_bidang', [541, 154, 96, 83, 65, 47, 11, 2])
                            ->get();
        $bidang->prepend($this->prependBidang());

        return $bidang;
    }
    
    private function prependBidang()
    {
        $prepend = collect([
            "id_bidang" => "",
            "nama_bidang" => "--Pilih Bidang--"
        ]);

        return $prepend;
    }
}
