<?php

namespace App\Http\Controllers\Master;

use App\DataTables\RelasiOrganisasiDataTable;
use App\Http\Controllers\Controller;
use App\Models\Bidang;
use App\Models\Jabatan;
use App\Models\Organisasi;
use App\Models\Satker;
use Illuminate\Http\Request;

class OrganisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(RelasiOrganisasiDataTable $dataTable)
    {
        return $dataTable->render('pages.master.relasiorganisasi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $satker = $this->listOptionSatker();
        $bidang = $this->listOptionBidang();
        $jabatan = $this->listOptionJabatan();
        return view('pages.master.relasiorganisasi.create', compact('satker', 'bidang', 'jabatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_satker' => 'required',
            'id_bidang' => 'required',
            'id_jabatan.*' => 'required'
        ]);

        $idSatker = $request->id_satker;
        $idBidang = $request->id_bidang;
        $idJabatanArr = $request->id_jabatan;
        $kodePejabat = $request->kode_pejabat;

        foreach ($idJabatanArr as $key => $idJabatan) {
            $checkData = Organisasi::whereIdSatker($idSatker)
                                    ->whereIdBidang($idBidang)
                                    ->whereIdJabatan($idJabatan)
                                    ->first();
            
            if (!$checkData) {
                Organisasi::create([
                    'id_satker'  => $idSatker,
                    'id_bidang' => $idBidang,
                    'id_jabatan' => $idJabatan,
                    // 'kode_pejabat' => $kodePejabat
                ]);
            }
        }

        return  redirect()->route('relasiorganisasi.index')->with(["success" => "Data berhasil ditambah"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $satker = $this->listOptionSatker();
        $bidang = $this->listOptionBidang();
        $jabatan = $this->listOptionJabatan();
        $dataOrganisasi = Organisasi::with('satker', 'bidang', 'jabatan')
                                    ->findOrFail($id);

        return view('pages.master.relasiorganisasi.edit', compact('dataOrganisasi', 'satker', 'bidang', 'jabatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id_satker' => 'required',
            'id_bidang' => 'required',
            'id_jabatan' => 'required'
        ]);

        $organisasi = Organisasi::findOrFail($id);

        $idSatker = $request->id_satker;
        $idBidang = $request->id_bidang;
        $idJabatan = $request->id_jabatan;
        $kodePejabat = $request->kode_pejabat;

        $checkData = Organisasi::whereIdSatker($idSatker)
                                ->whereIdBidang($idBidang)
                                ->whereIdJabatan($idJabatan)
                                ->first();

        // if ($checkData) {
        //     $checkData->update([
        //         'kode_pejabat' => $kodePejabat
        //     ]);

        //     return  redirect()->route('relasiorganisasi.index')->with(["success" => "Data berhasil diubah"]);
        // }

        $organisasi->update([
            'id_satker'  => $idSatker,
            'id_bidang' => $idBidang,
            'id_jabatan' => $idJabatan,
            // 'kode_pejabat' => $kodePejabat
        ]);

        return  redirect()->route('relasiorganisasi.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organisasi = Organisasi::findOrFail($id);
        $organisasi->delete();
        return  redirect()->route('relasiorganisasi.index')->with(["success" => "Data berhasil dihapus"]);
    }

    public function getSatker()
    {
        $satker = Satker::where('nama_satker', 'LIKE', '%' .request()->term. '%')
                        ->order()
                        ->get(['id_satker as id', 'nama_satker as text']);

        return ['results' => $satker];
    }

    public function getBidang()
    {
        $bidang = Bidang::where('nama_bidang', 'LIKE', '%' .request()->term. '%')
                        ->order()
                        ->get(['id_bidang as id', 'nama_bidang as text']);

        return ['results' => $bidang];
    }

    public function getJabatan()
    {
        $jabatan = Jabatan::where('nama_jabatan', 'LIKE', '%' .request()->term. '%')
                        ->order()
                        ->get(['id_jabatan as id', 'nama_jabatan as text']);

        return ['results' => $jabatan];
    }

    public function jabatanIndex(RelasiOrganisasiDataTable $dataTable)
    {
        return $dataTable->with('type', 'jabatan')->render('master.relasiorganisasi.jabatanindex');
    }

    public function jabatanEdit($id)
    {
        $dataOrganisasi = Organisasi::with('satker', 'bidang', 'jabatan')
                                    ->findOrFail($id);

        return view('master.relasiorganisasi.jabatanedit', compact('dataOrganisasi'));
    }

    public function jabatanUpdate(Request $request, $id)
    {

        $organisasi = Organisasi::findOrFail($id);
        $kodePejabat = $request->kode_pejabat;

        $organisasi->update([
            'kode_pejabat' => $kodePejabat
        ]);

        return  redirect()->route('kdjabatan.index')->with(["success" => "Data berhasil diubah"]);
    }

    private function listOptionSatker($id = null)
    {
        $satker = Satker::where('tipe_satker', '!=', '4')
                        ->select('id_satker', 'nama_satker');
        if ($id != null) {
            $satker = $satker->where('id_satker', '!=', $id);
        }
        $satker = $satker->order()
                            ->get();
        $satker->prepend($this->prependSatker());

        return $satker;
    }

    private function listOptionBidang($id = null)
    {
        $bidang = Bidang::select('id_bidang', 'nama_bidang');
        if ($id != null) {
            $bidang = $bidang->where('id_bidang', '!=', $id);
        }
        $bidang = $bidang->order()
                            ->get();
        $bidang->prepend($this->prependBidang());

        return $bidang;
    }
    
    private function prependSatker()
    {
        $prepend = collect([
            "id_satker" => "",
            "nama_satker" => "--Pilih Satker--",
            "label_type_satker" => null
        ]);

        return $prepend;
    }
    
    private function prependBidang()
    {
        $prepend = collect([
            "id_bidang" => "",
            "nama_bidang" => "--Pilih Bidang--"
        ]);

        return $prepend;
    }

    private function listOptionJabatan($id = null)
    {
        $jabatan = Jabatan::select('id_jabatan', 'nama_jabatan');
        if ($id != null) {
            $jabatan = $jabatan->where('id_jabatan', '!=', $id);
        }
        $jabatan = $jabatan->order()
                            ->get();

        $prepend = collect([
            "id_jabatan" => "",
            "nama_jabatan" => "--Pilih Jabatan--",
            "label_type_jabatan" => null
        ]);

        $jabatan->prepend($prepend);

        return $jabatan;
    }
}
