<?php

namespace App\Http\Controllers\Master;

use App\DataTables\JabatanDataTable;
use App\Http\Controllers\Controller;
use App\Models\Jabatan;
use Illuminate\Http\Request;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(JabatanDataTable $dataTable)
    {
        return $dataTable->render('pages.master.jabatan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jabatan = $this->listOptionJabatan();
        $tipeJabatan = $this->tipeJabatan();
        return view('pages.master.jabatan.create', compact('jabatan', 'tipeJabatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_jabatan' => 'required',
            'eselon' => 'required',
            'is_admin' => 'required'
        ]);

        $jabatan = Jabatan::firstOrCreate([
            'nama_jabatan'  => $request->nama_jabatan,
            'parent_id' => $request->parent_id,
            'eselon' => $request->eselon,
            'is_admin'  => $request->is_admin
        ]);

        return  redirect()->route('jabatan.index')->with(["success" => "Data berhasil ditambah"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataJabatan = Jabatan::findOrFail($id);
        $jabatan = $this->listOptionJabatan($dataJabatan->id_jabatan);
        $tipeJabatan = $this->tipeJabatan();
        $role = strtolower(auth()->user()->roles->first()->name);

        return view('pages.master.jabatan.edit', compact('dataJabatan', 'jabatan', 'tipeJabatan', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_jabatan' => 'required',
            'eselon' => 'required',
            'is_admin' => 'required'
        ]);
        
        $jabatan = Jabatan::findOrFail($id);

        $jabatan->update([
            'nama_jabatan'  => $request->nama_jabatan,
            'parent_id' => $request->parent_id,
            'eselon' => $request->eselon,
            'is_admin'  => $request->is_admin
        ]);

        return  redirect()->route('jabatan.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jabatan = Jabatan::findOrFail($id);
        $jabatan->delete();
        return  redirect()->route('jabatan.index')->with(["success" => "Data berhasil dihapus"]);
    }

    private function listOptionJabatan($id = null)
    {
        $jabatan = Jabatan::select('id_jabatan', 'nama_jabatan');
        if ($id != null) {
            $jabatan = $jabatan->where('id_jabatan', '!=', $id);
        }
        $jabatan = $jabatan->order()
                            ->get();

        $prepend = collect([
            "id_jabatan" => "",
            "nama_jabatan" => "--Pilih Jabatan--",
            "label_type_jabatan" => null
        ]);

        $jabatan->prepend($prepend);

        return $jabatan;
    }

    private function tipeJabatan()
    {
        $tipeJabatan = [
            "1" => "ESELON 1",
            "2" => "ESELON 2",
            "3" => "ESELON 3",
            "4" => "ESELON 4",
            "5" => "ESELON 5"
        ];

        return $tipeJabatan;
    }
}
