<?php

namespace App\Http\Controllers\Master;

use App\DataTables\BeritaDataTable;
use App\DataTables\DpoDataTable;
use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\Dpo;
use App\Models\Satker;
use App\Models\Sliders;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class DpoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(DpoDataTable $dataTable, Request $request)
    {
        return $dataTable->render('pages.master.dpo.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
        }else{
            $satker = Satker::get();
        }

        return view("pages.master.dpo.create", compact('satker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'id_satker' => 'required',
            'nama_dpo' => 'required',
            'perkara' => 'required',
            'pasal' => 'required',
            'alasan_dpo' => 'required',
            'terakhir_dilihat' => 'required',
            'foto' => 'required',
        ]);

        $nama_dpo = jsEscape($request->nama_dpo);
        $perkara = jsEscape($request->perkara);
        $pasal = jsEscape($request->pasal);
        $alasan_dpo = jsEscape($request->alasan_dpo);
        $terakhir_dilihat = jsEscape($request->terakhir_dilihat);

        if( !empty($request->foto) ) {
            $validator = Validator::make($request->all(), [
                'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "dpo_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/dpo/images' ;
            $file->move($destinationPath,$fileName);
        }

        $dpo = Dpo::create([
            'id_satker'  => $request->id_satker,
            'nama_dpo'  => $nama_dpo,
            'perkara' => $perkara,
            'pasal' => $pasal,
            'alasan_dpo' => $alasan_dpo,
            'terakhir_dilihat' => $terakhir_dilihat,
            'foto' => 'dpo/images/'.$fileName,
            'is_active' => '1',
        ]);
        
        return  redirect()->route('dpo.index')->with(["success" => "Data berhasil ditambah"]);
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function edit($id)
    {

        $user = auth()->user();
        
        $satker = Satker::get();
        

        $dpo = Dpo::findOrFail($id);
        $selfsatker = Satker::where('id_satker',$dpo->id_satker)->first();
        return view('pages.master.dpo.edit', compact('dpo','satker','selfsatker'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $nama_dpo = jsEscape($request->nama_dpo);
        $perkara = jsEscape($request->perkara);
        $pasal = jsEscape($request->pasal);
        $alasan_dpo = jsEscape($request->alasan_dpo);
        $terakhir_dilihat = jsEscape($request->terakhir_dilihat);

        $dpo = Dpo::findOrFail($id);
        if( !empty($request->foto) ) {
            $validator = Validator::make($request->all(), [
                'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "dpo_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/dpo/images' ;
            $file->move($destinationPath,$fileName);

            $dpo->update([
                'id_satker'  => $request->id_satker,
                'nama_dpo'  => $nama_dpo,
                'perkara' => $perkara,
                'pasal' => $pasal,
                'alasan_dpo' => $alasan_dpo,
                'terakhir_dilihat' => $terakhir_dilihat,
                'foto' => 'dpo/images/'.$fileName,
                'is_active' => '1',
            ]);
        }else{
            $dpo->update([
                'id_satker'  => $request->id_satker,
                'nama_dpo'  => $nama_dpo,
                'perkara' => $perkara,
                'pasal' => $pasal,
                'alasan_dpo' => $alasan_dpo,
                'terakhir_dilihat' => $terakhir_dilihat,
                'is_active' => '1',
            ]);
        }
        return  redirect()->route('dpo.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dpo = Dpo::findOrFail($id);
        $dpo->delete();
        return  redirect()->route('dpo.index')->with(["success" => "Data berhasil dihapus"]);
    }

}
