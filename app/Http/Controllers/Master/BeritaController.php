<?php

namespace App\Http\Controllers\Master;

use App\DataTables\BeritaDataTable;
use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\Satker;
use App\Models\Sliders;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(BeritaDataTable $dataTable, Request $request)
    {
        return $dataTable->render('pages.master.berita.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
        }else{
            $satker = Satker::get();
        }

        return view("pages.master.berita.create", compact('satker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'id_satker' => 'required',
            'judul_berita' => 'required',
            'deskripsi_berita' => 'required',
            'foto' => 'required',
        ]);

        $judul_berita = jsEscape($request->judul_berita);
        $deskripsi_berita = jsEscape($request->deskripsi_berita);

        $validator = Validator::make($request->all(), [
            'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
        ]);
        if ($validator->fails()) 
        {
            return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
        }

        if( !empty($request->foto) ) {
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "berita_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/berita/images' ;
            $file->move($destinationPath,$fileName);
        }

        $berita = Berita::create([
            'id_satker'  => $request->id_satker,
            'judul_berita'  => $judul_berita,
            'deskripsi_berita' => $deskripsi_berita,
            'tanggal' => Carbon::now(),
            'foto' => 'berita/images/'.$fileName,
            'is_active' => '1',
        ]);
        
        return  redirect()->route('berita.index')->with(["success" => "Data berhasil ditambah"]);
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function edit($id)
    {

        $user = auth()->user();
        
        $satker = Satker::get();
        

        $berita = Berita::findOrFail($id);
        $selfsatker = Satker::where('id_satker',$berita->id_satker)->first();
        return view('pages.master.berita.edit', compact('berita','satker','selfsatker'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $judul_berita = jsEscape($request->judul_berita);
        $deskripsi_berita = jsEscape($request->deskripsi_berita);

        $berita = Berita::findOrFail($id);
        if( !empty($request->foto) ) {
            $validator = Validator::make($request->all(), [
                'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "berita_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/berita/images' ;
            $file->move($destinationPath,$fileName);

            $berita->update([
                'id_satker'  => $request->id_satker,
                'judul_berita'  => $judul_berita,
                'deskripsi_berita' => $deskripsi_berita,
                'tanggal' => Carbon::now(),
                'foto' => 'berita/images/'.$fileName,
                'is_active' => '1',
            ]);
        }else{
            $berita->update([
                'id_satker'  => $request->id_satker,
                'judul_berita'  => $judul_berita,
                'deskripsi_berita' => $deskripsi_berita,
                'tanggal' => Carbon::now(),
                'is_active' => '1',
            ]);
        }

        return  redirect()->route('berita.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::findOrFail($id);
        $berita->delete();
        return  redirect()->route('berita.index')->with(["success" => "Data berhasil dihapus"]);
    }

    public function beritadetail($id)
    {
        $berita = Berita::findOrFail($id);
        $satker = Satker::where('id_satker',$berita['id_satker'])->select('nama_satker')->first();
        return view("cetak.layanan.kejaksaan_todaydetail_daerah", compact('berita','satker'));
    }

}
