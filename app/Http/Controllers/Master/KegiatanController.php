<?php

namespace App\Http\Controllers\Master;

use App\DataTables\BeritaDataTable;
use App\DataTables\KegiatanDataTable;
use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\Kegiatan;
use App\Models\Satker;
use App\Models\Sliders;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(KegiatanDataTable $dataTable, Request $request)
    {
        return $dataTable->render('pages.master.kegiatan.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        $user = auth()->user();
        
        if ($user->role == 'admin') {
            $satker = Satker::where('id_satker',$user->kode_satker)->get();
        }else{
            $satker = Satker::get();
        }

        $master_kegiatan = DB::table('master_kegiatan')->where('is_active',1)->get();

        return view("pages.master.kegiatan.create", compact('satker','master_kegiatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_satker' => 'required',
            'id_kegiatan' => 'required',
            'judul' => 'required',
            'tanggal' => 'required',
            'deskripsi_kegiatan' => 'required',
            'foto' => 'required',
        ]);

        $judul = jsEscape($request->judul);
        $deskripsi_kegiatan = jsEscape($request->deskripsi_kegiatan);

        $validator = Validator::make($request->all(), [
            'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
        ]);
        if ($validator->fails()) 
        {
            return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
        }

        if( !empty($request->foto) ) {
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "kegiatan_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/kegiatan/images' ;
            $file->move($destinationPath,$fileName);
        }

        $berita = Kegiatan::create([
            'id_satker'  => $request->id_satker,
            'id_kegiatan'  => $request->id_kegiatan,
            'judul'  => $judul,
            'tanggal'  => $request->tanggal,
            'deskripsi_kegiatan' => $deskripsi_kegiatan,
            'foto' => 'kegiatan/images/'.$fileName,
            'is_active' => '1'
        ]);
        
        return  redirect()->route('kegiatan.index')->with(["success" => "Data berhasil ditambah"]);
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    public function edit($id)
    {

        $user = auth()->user();
        
        $satker = Satker::get();
        

        $kegiatan = Kegiatan::findOrFail($id);
        $selfsatker = Satker::where('id_satker',$kegiatan->id_satker)->first();
        
        $master_kegiatan = DB::table('master_kegiatan')->where('is_active',1)->get();
        $self_master_kegiatan = DB::table('master_kegiatan')->where('id',$kegiatan->id_kegiatan)->first();

        return view('pages.master.kegiatan.edit', compact('kegiatan','satker','selfsatker','self_master_kegiatan','master_kegiatan'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $judul = jsEscape($request->judul);
        $deskripsi_kegiatan = jsEscape($request->deskripsi_kegiatan);

        $kegiatan = Kegiatan::findOrFail($id);
        if( !empty($request->foto) ) {
            $validator = Validator::make($request->all(), [
                'foto' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('foto') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "kegiatan_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/kegiatan/images' ;
            $file->move($destinationPath,$fileName);

        
            $kegiatan->update([
                'id_satker'  => $request->id_satker,
                'id_kegiatan'  => $request->id_kegiatan,
                'judul'  => $judul,
                'tanggal'  => $request->tanggal,
                'deskripsi_kegiatan' => $deskripsi_kegiatan,
                'foto' => 'kegiatan/images/'.$fileName,
                'is_active' => '1'
            ]);
        }else{
            $kegiatan->update([
                'id_satker'  => $request->id_satker,
                'id_kegiatan'  => $request->id_kegiatan,
                'judul'  => $judul,
                'tanggal'  => $request->tanggal,
                'deskripsi_kegiatan' => $deskripsi_kegiatan,
                'is_active' => '1'
            ]);
        }

        return  redirect()->route('kegiatan.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kegiatan = Kegiatan::findOrFail($id);
        $kegiatan->delete();
        return  redirect()->route('kegiatan.index')->with(["success" => "Data berhasil dihapus"]);
    }

    public function kegiatan($slug)
    {
        $kegiatan = Kegiatan::join('satker','kegiatan.id_satker','=','satker.id_satker')->join('master_kegiatan','master_kegiatan.id','=','kegiatan.id_kegiatan')->where('master_kegiatan.slug',$slug)->select('kegiatan.*','master_kegiatan.judul_kegiatan','satker.nama_satker','master_kegiatan.judul_kegiatan')->get();
        $datasatker = Satker::get();
        $judul_kegiatan = DB::table('master_kegiatan')->where('slug',$slug)->select('judul_kegiatan','slug')->first();
        return view("cetak.layanan.kegiatan", compact('kegiatan','datasatker','judul_kegiatan'));
    }

    public function kegiatansearch($slug, Request $request)
    {
        if ($request->satker == null){
            return $this->kegiatan($slug);
        }else{
            $kegiatan = Kegiatan::join('satker','kegiatan.id_satker','=','satker.id_satker')->join('master_kegiatan','master_kegiatan.id','=','kegiatan.id_kegiatan')->where('master_kegiatan.slug',$slug)->where('kegiatan.id_satker', $request->satker)->select('kegiatan.*','master_kegiatan.judul_kegiatan','satker.nama_satker','master_kegiatan.judul_kegiatan')->get();
            $datasatker = Satker::get();
            $judul_kegiatan = DB::table('master_kegiatan')->where('slug',$slug)->select('judul_kegiatan','slug')->first();
            return view("cetak.layanan.kegiatan", compact('kegiatan','datasatker','judul_kegiatan'));
        }
        
    }

    public function detail_kegiatan($slug)
    {
        $kegiatan = "";
        return response($kegiatan);
    }

}
