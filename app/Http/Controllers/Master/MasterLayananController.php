<?php

namespace App\Http\Controllers\Master;

use App\DataTables\MasterLayananDataTable;
use App\Http\Controllers\Controller;
use App\Models\MasterLayanan;
use App\Models\PermohonanLayanan;
use Illuminate\Http\Request;

class MasterLayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(MasterLayananDataTable $dataTable)
    {
        return $dataTable->render('pages.master.masterlayanan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master.masterlayanan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_layanan' => 'required',
            'blade' => 'required',
            'blade_result' => 'required',
            'tipe_layanan' => 'required',
            'type' => 'required',
            'aktif' => 'required',
            'order_by' => 'required'
        ]);

        if (!json_decode($request->parameter)){
            return  redirect()->back()->withInput()->with(["error" => "Format json tidak sesuai."]);    
        }

        MasterLayanan::firstOrCreate([
            'nama_layanan'  => $request->nama_layanan,
            'parameter' => json_decode($request->parameter, true),
            'blade' => $request->blade,
            'blade_result' => $request->blade_result,
            'tipe_layanan' => $request->tipe_layanan,
            'type' => $request->type,
            'order_by' => $request->order_by,
        ]);

        return  redirect()->route('master_layanan.index')->with(["success" => "Data berhasil ditambah"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MasterLayanan::findOrFail($id);
        return view('pages.master.masterlayanan.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_layanan' => 'required',
            'blade' => 'required',
            'blade_result' => 'required',
            'tipe_layanan' => 'required',
            'type' => 'required',
            'aktif' => 'required',
            'order_by' => 'required'
        ]);

        $update = MasterLayanan::findOrFail($id);

        if (!json_decode($request->parameter)){
            return  redirect()->back()->with(["error" => "Format json tidak sesuai."]);    
        }

        $update->update([
            'nama_layanan'  => $request->nama_layanan,
            'parameter' => json_decode($request->parameter),
            'blade' => $request->blade,
            'blade_result' => $request->blade_result,
            'tipe_layanan' => $request->tipe_layanan,
            'type' => $request->type,
            'order_by' => $request->order_by,
        ]);

        return  redirect()->route('master_layanan.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MasterLayanan::findOrFail($id);
        if($data){
            $cek = PermohonanLayanan::where('jenis_layanan', $id)->count();
            if($cek>0){
                return  redirect()->route('master_layanan.index')->with(["error" => "Data gagal dihapus"]);
            }
        }
        $data->delete();
        return  redirect()->route('master_layanan.index')->with(["success" => "Data berhasil dihapus"]);
    }

    private function listOptionSatker($id = null)
    {
        $satker = Satker::where('tipe_satker', '!=', '4')
                        ->select('id_satker', 'nama_satker');
        if ($id != null) {
            $satker = $satker->where('id_satker', '!=', $id);
        }
        $satker = $satker->order()
                            ->get();
        $satker->prepend($this->prependSatker());

        return $satker;
    }
    
    private function prependSatker()
    {
        $prepend = collect([
            "id_satker" => "",
            "nama_satker" => "--Pilih Satker--",
            "label_type_satker" => null
        ]);

        return $prepend;
    }

    private function tipeSatker()
    {
        $tipeSatker = [
            "1" => "Kejaksaan Agung",
            "2" => "Kejaksaan Tinggi",
            "3" => "Kejaksaan Negeri",
            "4" => "Cabang Kejaksaan Negeri"
        ];

        return $tipeSatker;
    }

    public function profile()
    {
        $data = Satker::findOrFail(auth()->user()->organisasi->id_satker);

        return view('master.masterlayanan.profile', compact('data'));
    }

    public function profile_update(Request $request, $id)
    {
        $satker = Satker::findOrFail($id);

        $satker->update([
            'alamat_satker' => $request->alamat_satker,
            'telp_satker' => $request->telp_satker,
            'website_satker' => $request->website_satker,			
            'city' => $request->city,
        ]);

        return  redirect()->route('master_layanan.profile')->with(["success" => "Data berhasil diubah"]);
    }
}
