<?php

namespace App\Http\Controllers\Master;

use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Sliders;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        ini_set('max_execution_time', 600); 
        ini_set('memory_limit','2048M');
    }
    
    public function index(SliderDataTable $dataTable)
    {
        return $dataTable->render('pages.master.slider.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.master.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'tipe_slider' => 'required',
            'judul_slider' => 'required',
            'deskripsi_slider' => 'required',
            'gambar' => 'required',
        ]);

        $tipe_slider = jsEscape($request->tipe_slider);
        $judul_slider = jsEscape($request->judul_slider);
        $deskripsi_slider = jsEscape($request->deskripsi_slider);
        $url_slider = jsEscape($request->url_slider);
        

        $validator = Validator::make($request->all(), [
            'gambar' => ['required','mimes:png,jpg,jpeg','max:2048']
        ]);
        if ($validator->fails()) 
        {
            return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
        }

        $fileName = $request->judul_slider.'-'.time();
        $photoName = '';
        if( !empty($request->gambar) ) {
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('gambar') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "landingpage_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/landingpage/images' ;
            $file->move($destinationPath,$fileName);
        }

        $slider = Sliders::firstOrCreate([
            'tipe_slider'  => $tipe_slider,
            'judul_slider'  => $judul_slider,
            'deskripsi_slider' => $deskripsi_slider,
            'url_slider' => $url_slider,
            'gambar' => 'landingpage/images/'.$fileName,
            'is_active' => '1',
        ]);

        return  redirect()->route('slider.index')->with(["success" => "Data berhasil ditambah"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Sliders::findOrFail($id);
        return view('pages.master.slider.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tipe_slider' => 'required',
            'judul_slider' => 'required',
            'deskripsi_slider' => 'required',
        ]);

        $tipe_slider = jsEscape($request->tipe_slider);
        $judul_slider = jsEscape($request->judul_slider);
        $deskripsi_slider = jsEscape($request->deskripsi_slider);
        $url_slider = jsEscape($request->url_slider);
        

        $slider = Sliders::findOrFail($id);

        if( !empty($request->gambar) ) {

            $validator = Validator::make($request->all(), [
                'gambar' => ['required','mimes:png,jpg,jpeg','max:2048']
            ]);
            if ($validator->fails()) 
            {
                return redirect()->back()->withInput()->with(['error' => 'Cek kembali gambar anda']);
            }
            $rand = substr(md5(microtime()),rand(0,26),9);
            $file = $request->file('gambar') ;
            $getFileExt = $file->getClientOriginalName();
            $fileExt = $array = explode('.', $getFileExt);
            $extension = end($fileExt);
            $fileName = "landingpage_" . $rand. "." . $extension; 
            $destinationPath = public_path().'/landingpage/images' ;
            $file->move($destinationPath,$fileName);

            $slider->update([
                'tipe_slider'  => $tipe_slider,
                'judul_slider'  => $judul_slider,
                'deskripsi_slider' => $deskripsi_slider,
                'url_slider' => $url_slider,
                'gambar' => 'landingpage/images/'.$fileName,
                'is_active' => $request->is_active,
            ]);
        }else{
            $slider->update([
                'tipe_slider'  => $tipe_slider,
                'judul_slider'  => $judul_slider,
                'deskripsi_slider' => $deskripsi_slider,
                'url_slider' => $url_slider,
                'is_active' => $request->is_active,
            ]);
        }

        return  redirect()->route('slider.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Sliders::findOrFail($id);
        $slider->delete();
        return  redirect()->route('slider.index')->with(["success" => "Data berhasil dihapus"]);
    }

}
