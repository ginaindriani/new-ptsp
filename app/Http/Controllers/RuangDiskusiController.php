<?php

namespace App\Http\Controllers;

use App\DataTables\RuangDataTable;
use App\Models\RuangDiskusi;
use Illuminate\Http\Request;

class RuangDiskusiController extends Controller
{
    public function index(RuangDataTable $dataTable, Request $request) {
        return $dataTable->render('pages.ruang_diskusi.index');
    }

    public function destroy($id) {
        $ruangDiskusi = RuangDiskusi::find($id);
        if (!$ruangDiskusi) {
            return redirect()->back()->with('error', 'Data tidak tersedia');
        }
        $ruangDiskusi->delete();
        return redirect()->route('ruang-diskusi.index')->with('success', 'Data Berhasil Dihapus');
    }

    public function answer($id, Request $request) {
        if ($request->ajax()) {
            $ruangDiskusi = RuangDiskusi::find($id);
            if ($request->isMethod('get')) {
                return view('pages.ruang_diskusi.answer', compact('ruangDiskusi'));
            } else {
                if ($ruangDiskusi && $request->answer) {
                    $ruangDiskusi->answer = $request->answer;
                    $ruangDiskusi->is_answer = 1;
                    $ruangDiskusi->answer_date = date('Y-m-d H:i:s');
                    $ruangDiskusi->save();

                    return response()->json([
                        'error' => false,
                        'message' => 'Data berhasil disimpan'
                    ], 200);
                }
                else {
                    $ruangDiskusi->answer = NULL;
                    $ruangDiskusi->is_answer = 0;
                    $ruangDiskusi->answer_date = NULL;
                    $ruangDiskusi->save();

                    return response()->json([
                        'error' => true,
                        'message' => 'Belum ada jawaban'
                    ], 200);
                }
            }
        }
        abort('404');
    }
}
