<?php

namespace App\Http\Controllers;

use App\DataTables\InfoDataTable;
use App\DataTables\LayananDaftarDataTable;
use App\DataTables\LayananDataTable;
use App\DataTables\PengaduanDaftarDataTable;
use App\DataTables\SearchNikDataTable;
use App\Models\MasterLayanan;
use App\Models\Satker;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Forms\PermohonanLayananForm;
use App\Forms\PermohonanLayananEditForm;
use App\Helpers\FileHelper;
use App\Http\Requests\BukutamuRequest;
use App\Models\Berita;
use App\Models\Dpo;
use App\Models\JadwalSidang;
use App\Models\Lelang;
use App\Models\Pegawai;
use App\Models\PerkaraTrend;
use App\Models\PermohonanLayanan;
use GuzzleHttp\Client as GuzzleHttpClient;
use App\Models\Tamu;
use App\Utilities\Utilities;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Calculation\TextData\Search;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use PDF;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use stdClass;
use Str;

class ApiController extends Controller
{
    use FormBuilderTrait;
    const KOH_TOKEN = '$2y$10$YVwGDLhBiQSrW8gbaOUmWOmULk8uUBjbm.cc8bX5dtxkt.kJIum9e';
    const CMS_PUBLIK_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI4MEk1NGVJTTNDWWw5MHR5UVR6VXZheU1XT2hEdFBpQyIsImlhdCI6MTY2MjYxNDg4MywiZXhwIjoxNjYyNjE1NzgzLCJuYmYiOjE2NjI2MTQ4ODMsImp0aSI6IkJFZU9tbnBmZ200ZGs4ZDciLCJzdWIiOjEyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3IiwiaWQiOjEyLCJuYW1lIjpudWxsLCJlbWFpbCI6InBpZHVtQGtlamFrc2Fhbi5nby5pZCJ9.5tZyRvreR6h4dBplm0F854pFMdZoxjaQFXr9HX5Tw5A';
    private $urlTrend;

    public function __construct()
    {
        $this->urlTrend = 'http://103.154.126.7/kejaksaan-trend/public/api';
        $this->path = '/permohonan/';
        $this->pathStorageFile = storage_path('app/public/upload/permohonan');
        $this->pathStorage = 'public/upload/permohonan/';
    }

    public function apiJenisLaporanPengaduan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $data = MasterLayanan::where('is_lapdu',1)->select('nama_layanan as jenis_layanan_pengaduan')->get();

        return response()->json(
            array(
                'code' => 200,
                'message' => 'Success',
                'response' => $data,
            ),
            200
        );
    }

    public function apiJumlahLaporanPengaduan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $data = DB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select([
                    'master_layanans.nama_layanan as jenis_layanan_pengaduan'
                    , DB::raw('
                        count( CASE WHEN permohonan_layanan.status is null THEN 1 END) AS belum_diproses,
                        count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END) AS sudah_diproses,
                        count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) AS jumlah_keseluruhan,
                        concat(round(( count( CASE WHEN permohonan_layanan.status = 1 THEN 1 END)/count( CASE WHEN permohonan_layanan.status = 1 or permohonan_layanan.status is null THEN 1 END) * 100 ),0),"%") AS percentage
                        ')
                ])
                ->groupBy('master_layanans.nama_layanan')
                ->orderBy('master_layanans.order_by','asc')
                ->orderBy('percentage','asc')
                ->get();


        return response()->json(
            array(
                'code' => 200,
                'message' => 'Success',
                'response' => $data,
            ),
            200
        );
    }

    public function apiSatker(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $data = Satker::get();

        return response()->json(
            array(
                'code' => 200,
                'message' => 'Success',
                'response' => $data,
            ),
            200
        );
    }

    public function apiTersangka(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }


        $satkers = Satker::where('kode_satker_cms',$request->satker)->select('nama_satker as nama_satker')->first();
        $satker = $satkers->nama_satker;
        $data_tersangka = array();

        $jenis_perkara_pidum = ['1','2','3','4'];
        foreach($jenis_perkara_pidum as $jpp){
            $values = array(
                'satker' => $request->satker,
                'jenis_pidana' => $jpp,
                'tahun' => $request->tahun
            );
            $params = http_build_query($values);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://cms-publik.kejaksaan.go.id/api/p16_satjenis_trend");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($httpcode != '200'){
                $info = "PENGAMBILAN DATA PERKARA PIDUM GAGAL, SILAHKAN COBA LAGI";
                return response()->json(
                    array(
                        'code' => 500,
                        'message' => 'Gagal',
                        'response' => 'Data gagal diambil terjadi kesalahan pada server',
                    ),
                    500
                );
            }
            curl_close ($ch);
            $data = json_decode($output);
            foreach($data as $o){
                $json = array(
                    'nama_tsk' => $o->nama_tsk,
                    'jenis_pidana' => $o->jenis_pidana,
                    'jenis_perkara' => $o->jenis_perkara,
                );
    
                $data_tersangka[] = array_push($data_tersangka, $json);
            }
        }

        $jenis_perkara_pidsus = ['1','2','3'];
        foreach($jenis_perkara_pidsus as $jpp){
            $values = array(
                'satker' => $request->satker,
                'jenis' => $jpp,
                'tahun' => $request->tahun
            );
            $params = http_build_query($values);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://cms-publik.kejaksaan.go.id/api/p16_pidsus_satjenis_trend");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($httpcode != '200'){
                $info = "PENGAMBILAN DATA PERKARA PIDUM GAGAL, SILAHKAN COBA LAGI";
                return response()->json(
                    array(
                        'code' => 500,
                        'message' => 'Gagal',
                        'response' => 'Data gagal diambil terjadi kesalahan pada server',
                    ),
                    500
                );
            }
            curl_close ($ch);
            $data = json_decode($output);
            foreach($data as $o){
                $json = array(
                    'nama_tsk' => $o->nama_tsk,
                    'jenis_pidana' => "Tindak Pidana Korupsi",
                    'jenis_perkara' => $o->jenis_pidana,
                );
    
                $data_tersangka[] = array_push($data_tersangka, $json);
            }
        }
        

        return response()->json(
            array(
                'code' => 200,
                'message' => 'Success',
                'satuan_kerja' => $satker,
                'data_tersangka' => $data_tersangka,
                'response' => $data,
            ),
            200
        );
    }

    public function nomorPerkara(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }


        $satkers = Satker::where('kode_satker_cms',$request->satker)->select('nama_satker as nama_satker')->first();
        $satker = $satkers->nama_satker;
        $data_tersangka = array();

        $jenis_perkara_pidum = ['1','2','3','4'];
        foreach($jenis_perkara_pidum as $jpp){
            $values = array(
                'satker' => $request->satker,
                'jenis_pidana' => $jpp,
                'tahun' => $request->tahun
            );
            $params = http_build_query($values);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://cms-publik.kejaksaan.go.id/api/p16_satjenis_trend");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($httpcode != '200'){
                $info = "PENGAMBILAN DATA PERKARA PIDUM GAGAL, SILAHKAN COBA LAGI";
                return response()->json(
                    array(
                        'code' => 500,
                        'message' => 'Gagal',
                        'response' => 'Data gagal diambil terjadi kesalahan pada server',
                    ),
                    500
                );
            }
            curl_close ($ch);
            $data = json_decode($output);
            foreach($data as $o){
                $json = array(
                    'nomor_perkara' => $o->id_perkara,
                    'nomor_spdp' => $o->no_spdp,
                    'tgl_spdp' => $o->tgl_spdp,
                    'nama_tsk' => $o->nama_tsk,
                    'jenis_pidana' => $o->jenis_pidana,
                    'jenis_perkara' => $o->jenis_perkara,
                );
    
                $data_tersangka[] = array_push($data_tersangka, $json);
            }
        }

        $jenis_perkara_pidsus = ['1','2','3'];
        foreach($jenis_perkara_pidsus as $jpp){
            $values = array(
                'satker' => $request->satker,
                'jenis' => $jpp,
                'tahun' => $request->tahun
            );
            $params = http_build_query($values);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://cms-publik.kejaksaan.go.id/api/p16_pidsus_satjenis_trend");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($httpcode != '200'){
                $info = "PENGAMBILAN DATA PERKARA PIDUM GAGAL, SILAHKAN COBA LAGI";
                return response()->json(
                    array(
                        'code' => 500,
                        'message' => 'Gagal',
                        'response' => 'Data gagal diambil terjadi kesalahan pada server',
                    ),
                    500
                );
            }
            curl_close ($ch);
            $data = json_decode($output);
            foreach($data as $o){
                $json = array(
                    'nomor_perkara' => $o->id_spdp,
                    'nomor_spdp' => $o->no_spdp,
                    'tgl_spdp' => $o->tgl_spdp,
                    'nama_tsk' => $o->nama_tsk,
                    'jenis_pidana' => "Tindak Pidana Korupsi",
                    'jenis_perkara' => $o->jenis_pidana,
                );
    
                $data_tersangka[] = array_push($data_tersangka, $json);
            }
        }
        

        return response()->json(
            array(
                'code' => 200,
                'message' => 'Success',
                'satuan_kerja' => $satker,
                'data_tersangka' => $data_tersangka,
                'response' => $data,
            ),
            200
        );
    }

    public function namaPegawai(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $pegawai = Pegawai::Where('nama', 'like', '%' . $request->nama_pegawai . '%')->get();

        return response()->json(
            array(
                'code' => 200,
                'message' => 'Success',
                'data_pegawai' => $pegawai,
            ),
            200
        );
    }

    public function jabatanPegawai(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $pegawai = Pegawai::Where('jabatan', 'like', '%' . $request->jabatan_pegawai . '%')->get();

        return response()->json(
            array(
                'code' => 200,
                'message' => 'Success',
                'data_pegawai' => $pegawai,
            ),
            200
        );
    }

    public function fotoPegawai(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::KOH_TOKEN,
        ];
        $url = 'https://mysimkari.kejaksaan.go.id/api/pegawai?nip=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->nip));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'foto_pegawai' => $result->data->foto,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function simpanSipermata(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        
        $dataSipermata = [
            'tanggal' => $request->tanggal,
            'nomor' => $request->nomor,
            'wilayah' => $request->wilayah,
            'asal' => $request->asal,
            'hal' => $request->hal,
            'id_satker' => $request->id_satker,
            'tgl_agenda' => $request->tgl_agenda,
            'file'      => $request->file,
            // 'file' => fopen($this->pathStorageFile.'/'.$permohonan->file, 'r')
        ];

        $apiSipermata = 'http://sipermata.kejaksaanri.id/api/post-lapdumas';
        $tokenSipermata = '6LcOWzIUAAAAAPAUXSVJdO94Uzzx2-CBmb-3JVvH';

        $ClientSipermata = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $tokenSipermata],
            'verify' => false
        ]);
        
        try {
            $postApiSipermata = $ClientSipermata->post($apiSipermata, [
                'form_params' => $dataSipermata
            ]);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
                // do nothing, the timeout exception is intended
        }

        return response()->json(
            array(
                'code' => 200,
                'response' => 'Success',
                'message' => 'Data terkirim ke Sipermata',
                'datas' => [],
            ),
            200
        );

    }


    public function simpanLapdumas(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $cek = count(MasterLayanan::where('id_layanan','=',$request->jenis_layanan)->where('is_lapdu','=','1')->get());

        if($cek == '0'){
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Layanan tidak termasuk Lapdumas',
                    'datas' => [],
                ),
                403
            );
        }
        $insert = new PermohonanLayanan();
        $insert->nomor_permohonan = $request->nomor_permohonan;
        $insert->jenis_layanan = $request->jenis_layanan;
        $insert->tanggal = $request->tanggal;
        $insert->nama = $request->nama;
        $insert->konten = $request->konten;
        $insert->file = $request->file;
        $insert->id_satker = $request->id_satker;
        $insert->save();

        return response()->json(
            array(
                'code' => 200,
                'response' => 'Success',
                'message' => 'Data terkirim ke Lapdumas',
                'datas' => [],
            ),
            200
        );

    }

    public function berita(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        // try {
        //     $urlberita = 'https://ptsp.kejaksaan.go.id/api/listnews';
        //         $get = new GuzzleHttpClient([
        //             'verify' => false
        //     ]);
        //     $getdata = $get->get($urlberita);
        //     $beritadetail = json_decode($getdata->getBody()->getContents(), true);
        //     $berita = $beritadetail['data'];
        // }catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $berita = [];
        // }
        // $getdata = $get->get($urlberita);
        // $beritadetail = json_decode($getdata->getBody()->getContents(), true);
        // $berita = $beritadetail['data'];

        return response()->json(
            array(
                'code' => 200,
                'response' => 'Success',
                'message' => 'Daftar Berita',
                'datas' => $berita,
            ),
            200
        );


    }

    public function rutanLapas(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }
        $data = DB::table('master_rutan_lapas')->get();
        return response()->json(
            array(
                'code' => 200,
                'response' => 'Success',
                'message' => 'Berhasil mengambil data',
                'datas' => $data,
            ),
            200
        );
    }

    public function barangBukti(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/barbuk?nama=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->nama_barbuk));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }


    public function namaJaksa(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/jpu?nama=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->nama));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function tempatLahir(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/tempat-lahir?nama=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->nama_tempat_lahir));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function tanggalLahir(Request $request)
    {
        
        $token = $request->header('Authorization');
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/tgl-lahir?tgl_start='.rawurlencode($request->tgl_start).'&tgl_end='.rawurlencode($request->tgl_end);
        $result = Utilities::httpPostJson($headers, $url);
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function jenisKelamin(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/jk?jk=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->jenis_kelamin));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function jenisTahanan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/jenis-tahanan?jenis=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->jenis));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function ditahanSejak(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/ditahan-sejak?ditahan_sejak=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->tanggal));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function tanggalSpdp(Request $request)
    {
        
        
        $token = $request->header('Authorization');
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/tgl-spdp?tgl_start='.rawurlencode($request->tgl_start).'&tgl_end='.rawurlencode($request->tgl_end);
        $result = Utilities::httpPostJson($headers, $url);
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function statusTahanan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        if($request->status == 'Ditahan'){
            $jenis = 'kota';
        }else{
            $jenis = $request->status;
        }
        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/jenis-tahanan?jenis=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($jenis));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function idTahanan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/id?id=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->id_tahanan));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function nikTahanan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/nik?nik=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->nik_tahanan));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function nohpTahanan(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $headers = [
            'Authorization: Bearer ' . self::CMS_PUBLIK_TOKEN,
        ];
        $url = 'https://cms-publik.kejaksaan.go.id/api/ptsp/no-hp?no_hp=';
        $result = Utilities::httpPostJson($headers, $url . rawurlencode($request->no_hp));
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result->data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function namaSaksi(Request $request)
    {
        
        $token = $request->header('Authorization');
        
        if($token !== 'Bearer 35518ecc-b4a2-428c-813b-8595f1bb1df5') {
            return response()->json(
                array(
                    'code' => 403,
                    'response' => 'failed',
                    'message' => 'Invalid token',
                    'datas' => [],
                ),
                403
            );
        }

        $data = ['id_spdp : 010000202004078', 
        'id_berkas : 010000202005066',
        'no_spdp : PDP-02/WBC.01/PPNS/2020',
        'nama_tsk : SADLI BIN MAHMUD',
        'tgl_spdp : 2020-03-30',
        'nama_satker : KEJAKSAAN TINGGI ACEH ',
        'jenis_pidana : Tindak Pidana Kepabeanan, Cukai & TPPU',
        'warganegara : WNI'];
            
        
       
        if ($request->nama == "SADLI BIN MAHMUD") {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $data,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }

    public function twitterDashboard(Request $request)
    {
        
        $headers = [
            'Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAJkKhgEAAAAAjbCxw2lyGq9iY1JCGKFCv46oUaQ%3DulzveSrKDS40xVpFDHcFPdgnLgmYdH04CyNScrnccuKY72CyjG',
        ];
        $url = 'https://api.twitter.com/2/tweets/search/recent?max_results=100&query=kejaksaan';
        $result = Utilities::httpGetJson($headers, $url);
        $result = json_decode($result);
        

        if ($result) {
            // $result = $result;
            return response()->json(
                array(
                    'code' => 200,
                    'message' => 'Success',
                    'data' => $result,
                ),
                200
            );

        } else {
            return response()->json([
                'error' => true,
                'data' => null,
                'message' => 'Data Tidak Ditemukan'
            ], 200);
        }
        
    }


    
    


}

