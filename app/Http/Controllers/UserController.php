<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Helpers\FileHelper;
use App\Models\Jabatan;
use App\Models\Organisasi;
use App\Models\Satker;
use App\Models\User;
use Http;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
		//path disk storage
        //$this->path = 'upload/user/';
		
		//path anoter server storage
        $this->path = '/user/';
        $this->pathStorage = storage_path('app/public/upload/user');
        $this->hostKejagung = config('constants.kejagung_api');
        $this->authKejagung = config('constants.kejagung_token');
		$this->hostMediaServer = config('constants.host_media');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('pages.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = $this->getRole();
        $satker = $this->getSatker();
        return view('pages.users.create', compact('role', 'satker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'nik' => 'required',
            'nip' => 'required',
            'pangkat' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'role' => 'required',
            'id_satker' => 'required',
            'id_bidang' => 'required',
            'id_jabatan' => 'required'
        ]);

        $organisasi = Organisasi::whereIdSatker($request->id_satker)
                                    ->whereIdBidang($request->id_bidang)
                                    ->whereIdJabatan($request->id_jabatan)
                                    ->first();

        if (!$organisasi) {
            return redirect()->back()->with(["error" => "Data organisasi tidak ditemukan"]);
        }

        if ($request->has('photo') && $request->photo != '') {
            $prefixname = "avatar_".$request->nip;
            $filename = FileHelper::uploadFileBase64($request->photo, 'upload/user/', $prefixname);
        }

        $user = User::firstOrCreate([
            'nama' => $request->nama,
            'nik' => $request->nik,
            'nip' => $request->nip,
            'pangkat' => $request->pangkat,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'id_organisasi' => $organisasi->id_organisasi,
            'avatar' => $filename ?? 'icon-user-default.png'
        ]);

        $user->syncRoles($request->role);

        return  redirect()->route('user.index')->with(["success" => "Data berhasil ditambah"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('organisasi.satker', 'organisasi.bidang', 'organisasi.jabatan')->findOrFail($id);
        $role = $this->getRole();
        $satker = $this->getSatker();

        return view('pages.users.edit', compact('user', 'role', 'satker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'nik' => 'required',
            'nip' => 'required',
            'pangkat' => 'required',
            'username' => 'required|unique:users,username,'.$id,
            'email' => 'required|unique:users,email,'.$id,
            'role' => 'required',
            'id_satker' => 'required',
            'id_bidang' => 'required',
            'id_jabatan' => 'required'
        ]);

        $organisasi = Organisasi::whereIdSatker($request->id_satker)
                                    ->whereIdBidang($request->id_bidang)
                                    ->whereIdJabatan($request->id_jabatan)
                                    ->first();

        if (!$organisasi) {
            return redirect()->back()->with(["error" => "Data organisasi tidak ditemukan"]);
        }
		
		$user = User::findOrFail($id);
		
		if ($request->has('photo') && $request->photo != '') {
            $prefixname = "avatar_".$request->nip;
            $filename = FileHelper::uploadFileBase64($request->photo, 'upload/user/', $prefixname);

			$user->update([
				'nama' => $request->nama,
				'nik' => $request->nik,
				'nip' => $request->nip,
				'pangkat' => $request->pangkat,
				'username' => $request->username,
				'email' => $request->email,
				'password' => empty($request->password) ? $user->password : bcrypt($request->password),
				'id_organisasi' => $organisasi->id_organisasi,
				'avatar' => $filename ?? 'icon-user-default.png'
			]);

			$user->syncRoles($request->role);
        }else{
			$user->update([
				'nama' => $request->nama,
				'nik' => $request->nik,
				'nip' => $request->nip,
				'pangkat' => $request->pangkat,
				'username' => $request->username,
				'email' => $request->email,
				'password' => empty($request->password) ? $user->password : bcrypt($request->password),
				'id_organisasi' => $organisasi->id_organisasi
			]);

			$user->syncRoles($request->role);
		}

        

        return  redirect()->route('user.index')->with(["success" => "Data berhasil diubah"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return  redirect()->route('user.index')->with(["success" => "Data berhasil dihapus"]);
    }

    public function getSatker()
    {
        if (auth()->user())
            $role = strtolower(auth()->user()->roles->first()->name);
        else 
            $role = "";

        $satker = Organisasi::with('satker')
                            ->whereHas('satker', function($q) {
                                $q->where('nama_satker', 'LIKE', "%".request()->term. "%");
                            })
                            ->when($role, function ($query, $role) {
                                if ($role == 'admin') {
                                    return $query->where('id_satker', auth()->user()->organisasi->id_satker);
                                }
                            })
                            ->groupBy('id_satker')
                            ->order()
                            ->get();
        $dataSatker = $satker->map(function($item, $key) {
                            $data['id'] = $item->satker->id_satker;
                            $data['text'] = $item->satker->nama_satker;
                            return $data;
                        });

        return collect($dataSatker);
    }

    public function getBidang()
    {
		
		$satker=Satker::where('id_satker', request()->id_satker)->first();
		if($satker->tipe_satker==1 && auth()->user()->organisasi->jabatan->is_admin){
			$bidang = Organisasi::with('bidang')
                            ->whereHas('bidang', function($q) {
                                $q->where('nama_bidang', 'LIKE', '%' .request()->term. '%')
								  ->where('main_parent_id', auth()->user()->organisasi->bidang->main_parent_id);
                            })
                            ->where('id_satker', request()->id_satker)
                            ->order()
                            ->groupBy('id_bidang')
                            ->get();

		}else{
			$bidang = Organisasi::with('bidang')
                            ->whereHas('bidang', function($q) {
                                $q->where('nama_bidang', 'LIKE', '%' .request()->term. '%');
                            })
                            ->where('id_satker', request()->id_satker)
                            ->order()
                            ->groupBy('id_bidang')
                            ->get();

		}
        
        $dataBidang = $bidang->map(function($item, $key) {
                            $data['id'] = $item->bidang->id_bidang;
                            $data['text'] = $item->bidang->nama_bidang_full;
                            return $data;
                        });

        return collect($dataBidang);
    }

    public function getJabatan()
    {
        $jabatan = Organisasi::with('jabatan')
                            ->whereHas('jabatan', function($q) {
                                $q->where('nama_jabatan', 'LIKE', '%' .request()->term. '%');
                            })
                            ->where('id_bidang', request()->id_bidang)
                            ->order()
                            ->groupBy('id_jabatan')
                            ->get();
        $dataJabatan = $jabatan->map(function($item, $key) {
                            $data['id'] = $item->jabatan->id_jabatan;
                            $data['text'] = $item->jabatan->nama_jabatan;
                            return $data;
                        });

        return collect($dataJabatan);
    }

    public function profile()
    {
		$host_media = $this->hostMediaServer;
        return view('profile', compact('host_media'));
    }

    public function ceknip(Request $request)
    {
        try {
            $nip = $request->nip;
            if (!$nip) {
                return response()->json([
                    'status' => '422',
                    'ket' => 'NIP tidak boleh kosong'
                ]);
            }

            $response = Http::withToken('$2y$10$YVwGDLhBiQSrW8gbaOUmWOmULk8uUBjbm.cc8bX5dtxkt.kJIum9e')->post('https://mysimkari.kejaksaan.go.id/api/pegawai?nip=' . $nip);

            if ($response->successful()) {
                $result = json_decode($response->body());
                if ($result && $result->status == "200") {
                    $data = collect();
                    foreach ($result->data as $key => $value) {
                        $data[$key] = $value;
                    }
                    $data->prepend('200', 'status');
                }
                else {
                    $data = [
                        'status'    => '404',
                        'ket'       => $result
                    ];
                }
            }
            else {
                $data = [
                    'status'    => (string) $response->status(),
                    'ket'       => 'Unsuccessfull Request'
                ];
            }
        }
        catch (\Exception $ex) {
            $data = [
                'status'    => '500',
                'ket'       => $ex->getMessage()
            ];
        }
        return $data;
    }

    private function getRole() {
        $roleUser = strtolower(auth()->user()->roles->first()->name);
        switch ($roleUser) {
            case 'superadmin':
                return Role::orderBy('name', 'ASC')->get();
            case 'admin':
                return Role::whereNotIn('name', ['superadmin'])->orderBy('name', 'ASC')->get();
            default:
                return Role::whereNotIn('name', ['superadmin', 'admin'])->orderBy('name', 'ASC')->get();
        }
    }

    private function syncIsAdminJabatan($idJabatan, $role) {
        $jabatan = Jabatan::find($idJabatan);
        switch($role) {
            case 'superadmin':
            case 'admin':
                if  (!$jabatan->is_admin) {
                    $jabatan->update([
                        'is_admin' => true
                    ]);
                }
                break;
            default:
                if  ($jabatan->is_admin) {
                    $jabatan->update([
                        'is_admin' => false
                    ]);
                }
                break;
        }
    }
}
