<?php

namespace App\Http\Controllers;

use App\Models\Berita;
use App\Models\Lelang;
use App\Models\MasterLayanan;
use App\Models\RuangDiskusi;
use App\Models\Satker;
use App\Models\SendEmailForm;
use App\Models\Sliders;
use App\Models\Suku;
use App\Models\Tamu;
use Carbon\Carbon;
use DirectoryIterator;
use Facade\FlareClient\Http\Client;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class LandingPageController extends Controller
{
    public function index(Request $request)
    {
        
        if($request->uuid != null){
            try {
                $uuid = Crypt::decrypt($request->uuid);
                $checkUuid = Tamu::where('uuid',$uuid)->first();
                if($checkUuid == null){
                    return redirect('/pendaftaran');    
                }
            } catch (\Throwable $th) {
                return redirect('/pendaftaran');
            }
        }else{
            $uuid = null;
        }
        
       
        $images = [];
        $menuLayanan = [];
        $menuInformasi = [];
        $menuPengaduan = [];
        $masterLayanan = MasterLayanan::where('tipe_layanan', 'layanan')->where('aktif',1)->where('is_lapdu',null)->orderBy('order_by','asc')->get();
        foreach ($masterLayanan as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.create', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

         
            $is_blank = true;

            $images[] = [
                'image' => asset('landingpage/images/listmenu') . '/' . str_replace(public_path('landingpage/images/listmenu/'), '', $value->image),
                'menu' => $value->tipe_layanan,
                'is_blank' => $is_blank,
                'url' => $url,
                'id_layanan' => $value->id_layanan,
                'nama_layanan' => ucwords($value->nama_layanan)
            ];
            $menuLayanan[] = [
                'url' => $url,
                'nama' => ucwords($value->nama_layanan)
            ];
        }

        $masterPengaduan = MasterLayanan::where('tipe_layanan', 'layanan')->where('aktif',1)->where('is_lapdu',1)->orderBy('order_by','asc')->get();
        foreach ($masterPengaduan as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.create', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

  
                $is_blank = true;

            $menuPengaduan[] = [
                'url' => $url,
                'nama' => ucwords($value->nama_layanan)
            ];
        }

        

        $msInformasi = MasterLayanan::where('tipe_layanan', '<>', 'layanan')->where('aktif',1)->where('is_lapdu',null)->orderBy('order_by','asc')->get();
        foreach ($msInformasi as $key => $value) {
            if ($value->id_layanan == 19 || $value->id_layanan == 17|| $value->id_layanan == 20|| $value->id_layanan == 21 || $value->id_layanan == 13 || $value->id_layanan == 14 || $value->id_layanan == 42 || $value->id_layanan == 43 || $value->id_layanan == 44)
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.create', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            $is_blank = true;

            $menuInformasi[] = [
                'url' => $url,
                'nama' => ucwords($value->nama_layanan)
            ];
        }

        if($uuid != null){
            $datatamu = Tamu::where('uuid',$uuid)->first();
            $satker = Satker::where('kode_satker',$datatamu->kode_satker)->select('nama_satker','id_satker')->first();
            
        }else{
            $datatamu = '';
            $satker = '';
        }


        $images = json_encode($images);
        $ruangDiskusi = RuangDiskusi::where('is_answer', 1)->orderBy('created_at','desc')->limit('5')->get();

        // try {
        //     $urlberita = 'https://ptsp.kejaksaan.go.id/api/listnews';
        //         $get = new GuzzleHttpClient([
        //             'verify' => false
        //     ]);
        //     $getdata = $get->get($urlberita);
        //     // return $statusCode = $urlberita->getStatusCode();
        //     $beritadetail = json_decode($getdata->getBody()->getContents(), true);
        //     $berita = $beritadetail['data'];
        // }catch (\GuzzleHttp\Exception\BadResponseException $e) {
        $berita = [];
        // }

        $beritadaerah = Berita::join('satker','satker.id_satker','=','berita.id_satker')->select('berita.id', 'satker.nama_satker', 'berita.judul_berita','berita.foto','berita.tanggal','berita.id_satker')->where('is_active',1)->orderBy('berita.tanggal','desc')->limit('10');
        
        if($uuid != null){
            $beritadaerahs = $beritadaerah->limit('6')->get();
            $beritasatker = $beritadaerah->where('berita.id_satker',$satker->id_satker)->limit('6')->get();
        }else{
            $beritadaerahs = $beritadaerah->limit('6')->get();
            $beritasatker = '';
        }

        $lelang = Lelang::join('satker','satker.id_satker','=','lelang.id_satker')->select('lelang.*','satker.nama_satker')->orderBy('lelang.created_at','desc')->limit('4')->get();
        $slideratas = Sliders::where('is_active',1)->where('tipe_slider',1)->get();
        $slidersamping = Sliders::where('is_active',1)->where('tipe_slider',2)->get();
        $sliderbawah = Sliders::where('is_active',1)->where('tipe_slider',3)->get();
        $kegiatan = DB::table('master_kegiatan')->where('is_active',1)->get();

        //chart
        $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            $data_pengaduan_perbulan = 
                DB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',1)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });

            $pengaduanmcount = [];
            $pengaduanArr = [];
        
            foreach ($data_pengaduan_perbulan as $key => $value) {
                $pengaduanmcount[(int)$key] = count($value);
            }
        
            for ($i = 1; $i <= 12; $i++) {
                if (!empty($pengaduanmcount[$i])) {
                    $pengaduanArr[$i]['count'] = $pengaduanmcount[$i];
                } else {
                    $pengaduanArr[$i]['count'] = 0;
                }
                $pengaduanArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_pengaduan_perbulan =  response()->json(array_values($pengaduanArr));

            $data_layanan_perbulan = 
                DB::table('master_layanans')
                ->leftjoin('permohonan_layanan','permohonan_layanan.jenis_layanan','=','master_layanans.id_layanan')
                ->where('master_layanans.tipe_layanan','layanan')
                ->where('master_layanans.aktif',1)
                ->where('master_layanans.is_lapdu',null)
                ->select('permohonan_layanan.created_at as created_at')
                ->get()
                ->groupBy(function ($date) {
                    return Carbon::parse($date->created_at)->format('m');
                });

            $layananmcount = [];
            $layananArr = [];
        
            foreach ($data_layanan_perbulan as $key => $jumlah) {
                $layananmcount[(int)$key] = count($jumlah);
            }
        
            for ($i = 1; $i <= 12; $i++) {
                if (!empty($layananmcount[$i])) {
                    $layananArr[$i]['count'] = $layananmcount[$i];
                } else {
                    $layananArr[$i]['count'] = 0;
                }
                $layananArr[$i]['month'] = $month[$i - 1];
                }

            $hitung_data_layanan_perbulan =  response()->json(array_values($layananArr));

        return view('landingpage.index', compact('images', 'hitung_data_pengaduan_perbulan', 'hitung_data_layanan_perbulan', 'lelang', 'kegiatan', 'datatamu','satker', 'ruangDiskusi', 'menuLayanan', 'menuInformasi','menuPengaduan','berita','beritadaerahs','beritasatker','slideratas','slidersamping','sliderbawah'));
    }
    

    public function indexlayanan()
    {
        $images = [];
        $masterLayanan = MasterLayanan::where('tipe_layanan', 'layanan')->where('aktif',1)->where('is_lapdu', null)->orderBy('order_by','asc')->get();
        foreach ($masterLayanan as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.create', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            // if ($value->id_layanan == 11)
            //     $is_blank = false;
            // else
                $is_blank = true;

            $images[] = [
                'image' => asset('landingpage/images/listmenu') . '/' . str_replace(public_path('landingpage/images/listmenu/'), '', $value->image),
                'menu' => $value->tipe_layanan,
                'is_blank' => $is_blank,
                'url' => $url,
                'id_layanan' => $value->id_layanan,
                'nama_layanan' => ucwords($value->nama_layanan)
            ];
        }

        $images = json_encode($images);
        return $images;
    }
    
    public function informasi()
    {
        $images = [];
        $masterLayanan = MasterLayanan::where('tipe_layanan', '<>', 'layanan')->where('aktif',1)->where('is_lapdu', null)->orderBy('order_by','asc')->get();
        foreach ($masterLayanan as $key => $value) {
            if ($value->id_layanan == 19 || $value->id_layanan == 17|| $value->id_layanan == 20|| $value->id_layanan == 21 || $value->id_layanan == 13 || $value->id_layanan == 14  || $value->id_layanan == 42  || $value->id_layanan == 43 || $value->id_layanan == 44)
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.create', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            $is_blank = true;

            $images[] = [
                'image' => asset('landingpage/images/listmenu') . '/' . str_replace(public_path('landingpage/images/listmenu/'), '', $value->image),
                'menu' => $value->tipe_layanan,
                'is_blank' => $is_blank,
                'url' => $url,
                'nama_layanan' => ucwords($value->nama_layanan)
            ];
        }

        $images = json_encode($images);
        $ruangDiskusi = RuangDiskusi::where('is_answer', 1)->orderBy('created_at','desc')->limit('5')->get();
        return $images;
    }

    public function pengaduan()
    {
        $images = [];
        $masterLayanan = MasterLayanan::where('tipe_layanan', 'layanan')->where('aktif',1)->where('is_lapdu', 1)->orderBy('order_by','asc')->get();
        foreach ($masterLayanan as $key => $value) {
            if (
                $value->id_layanan == 19
                || $value->id_layanan == 17
                || $value->id_layanan == 20
                || $value->id_layanan == 21
            )
                $url = route('permohonan.indexpublic', ['idLayanan' => $value->id_layanan]);
            else
                $url = route('permohonan.create', ['idLayanan' => $value->id_layanan]);

            if ($value->type == 'hyperlink' && $value->link_url)
                $url = $value->link_url;

            // if ($value->id_layanan == 11)
            //     $is_blank = false;
            // else
                $is_blank = true;

        $images[] = [
                'image' => asset('landingpage/images/listmenu') . '/' . str_replace(public_path('landingpage/images/listmenu/'), '', $value->image),
                'menu' => $value->tipe_layanan,
                'is_blank' => $is_blank,
                'url' => $url,
                'id_layanan' => $value->id_layanan,
                'nama_layanan' => ucwords($value->nama_layanan)
            ];
        }

        $images = json_encode($images);
        return $images;
    }
    

    public function scanqr()
    {
        return view('landingpage.scanqr');
    }

    public function pendaftaran()
    {
        return view('landingpage.pendaftaran');
    }

    public function validateqr($unique_key)
{     
    // return $unique_key;          
    $data = Tamu::where('unique_key',$unique_key)->first();        
    if(!$data){            
        return redirect()->back()->withInput()->with(['error' => 'Data tidak ditemukan harap Scan kembali QRCode Anda']);
    }
    $url = '?uuid='.Crypt::encrypt($data->uuid);
    return redirect(url($url));               
}

    public function sendEmail(Request $request)
    {
        try {
            $name = jsEscape($request->name);
            $email = jsEscape($request->email);
            $phone = jsEscape($request->phone);
            $username = $name;
            $emailname = $email;
            $message = jsEscape($request->message);
            
            $saveData = new RuangDiskusi();
            $saveData->name = $username;
            $saveData->email = $emailname;
            $saveData->phone = $phone;
            $saveData->message = $message;
            $saveData->save();

            return response()->json([
                'error' => false,
                'message' => 'Data berhasil disimpan'
            ], 200);
        } catch (\Throwable $th) {
            dd($th);
            return response()->json([
                'error' => true,
                'message' => 'Proses sedang tidak bisa dilakukan'
            ], 200);
        }
    }

    public function saveRuangBerbagi($request) {

    }

    public function doSendEmail($request) {
        try {
            //code...
            $username = $request->name;
            $emailname = $request->email;
            $phone = $request->phone;
            $email = 'humas.puspenkum@kejaksaan.go.id';
            // $email = 'danang.hermanto77@gmail.com';
            $data = array(
                "name" => $username,
                "phone" => $phone,
                "email" => $emailname,
                "body" => $request->message
            );

            Mail::send('emails.mail', $data, function ($message) use ($username, $email) {
                $message->to($email, $username)
                    ->subject('Masalah dari Pengaduan Masyarakat');
            });

            $saveData = new SendEmailForm();
            $saveData->nama = $request->name;
            $saveData->email = $request->email;
            $saveData->phone_number = $request->phone;
            $saveData->message = $request->message;
            $saveData->save();
            // dd($request->all());
            return response()->json([
                'error' => false,
                'message' => 'Email berhasil dikirim'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => true,
                'message' => 'Proses sedang tidak bisa dilakukan'
            ], 200);
        }
    }

    public function satker()
    {

        $images = [];

        $satkers = Satker::where('parent_id',1)->get();

        foreach ($satkers as $satker) {
            array_push($images, ['image' => '', 'menu' => $satker->nama_satker, 'url' => route('satkerChild', ['id_satker' => $satker->id_satker])]);
        }
        $images = json_encode($images);
        return view('landingpage.satker', compact('images'));
    }

    public function satkerChild(Request $request){
        $images = [];

        $satkers = Satker::where('parent_id',$request->id_satker)->get();
       
        foreach ($satkers as $satker) {
            array_push($images, ['image' => '', 'menu' => $satker->nama_satker, 'url' => route('status-tamu', ['kode_satker' => $satker->kode_satker])]);
        }
        $images = json_encode($images);
        return view('landingpage.satker', compact('images'));
    }

    public function statusTamu(Request $request)
    {
        // dd($request->kode_satker);
        $images = [
            [
                "image" => "",
                "menu"  => "Tamu Umum",
                'url'   => route('identitas', [
                            'kode_satker' => $request->kode_satker,
                            'status_tamu'      => 'umum'])
            ],
            [
                "image" => "",
                "menu"  => "Saksi/Terdakwa",
                'url'   => route('identitas', [
                            'kode_satker' => $request->kode_satker,
                            'status_tamu'      => 'saksi_terdakwa'])
            ],
            [
                "image" => "",
                "menu"  => "Pers",
                'url'   => route('identitas', [
                            'kode_satker' => $request->kode_satker,
                            'status_tamu'      => 'pers'])
            ]
        ];
        $images = json_encode($images);

        return view('landingpage.status-tamu', compact('images'));
    }



}
