<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

class PermohonanLayananEditForm extends Form
{
    public function buildForm()
    {
        $idLayanan = $this->getData('idLayanan');
        $parameterSurat = $this->getData('parameterSurat');
        $satker = $this->getData('satker');
        $satker = $satker->mapWithKeys(function($item) {
            return [$item->id_satker => $item->nama_satker];
        })->all();
        $jenisPengaduan = $this->getData('jenisPengaduan');
        $asalPengaduan = $this->getData('asalPengaduan');
        $optionPps = $this->getData('optionPps');
        $jenisLayanan = $this->getData('jenisLayanan');
        $jenisBidang = $this->getData('jenisBidang');
        $data = $this->getData('data')->konten;

        foreach($parameterSurat as $key => $value) {
            $choices = [];
            switch ($key) {
                case "satker" :
                    $choices = $satker;
                    break;
                case "jenis" :
                    $choices = $jenisPengaduan;
                    break;
                case "asal" :
                    $choices = $asalPengaduan;
                    break;
                case "pps" :
                    $choices = $optionPps;
                    break;
                case "jenis_layanan" :
                    $choices = $jenisLayanan;
                    break;
                case "jenis_bidang" :
                    $choices = $jenisBidang;
                    break;
            }
            switch ($value['type']) {
                case "text" :
                    $this->add($key, 'text', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'value' => isset($data[$key]) ? $data[$key] : "",
                        'attr' => [
                            'class' => 'w-100 form-control mb-5 mt-3',
                            'rows' => 5
                        ],
                    ]);
                    break;

                case "hidden" :
                    $this->add($key, 'hidden', [
                        'value' => isset($value['value']) ? $value['value'] : ''
                    ]);
                    break;

                case "textarea" :
                    $this->add($key, 'textarea', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'value' => isset($data[$key]) ? $data[$key] : "",
                        'attr' => [
                            'class' => 'w-100 form-control mb-5 mt-3',
                            'rows' => 5
                        ],
                    ]);
                    break;

                case "wysiwyg" :
                    $this->add($key, 'textarea', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'value' => isset($data[$key]) ? $data[$key] : "",
                        'attr' => [
                            'class' => 'w-100 form-control mb-5 mt-3'
                        ],
                    ]);
                    break;

                case "date" :
                    $this->add($key, 'text', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'attr' => [
                            'class' => 'w-100 form-control mb-5 mt-3'
                        ],
                        'value' => isset($data[$key]) ? $data[$key] : ""
                    ]);
                    break;

                case "select" :
                    $this->add($key, 'select', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'attr' => [
                            'class' => 'w-100 form-control mb-5 mt-3',
                            'data-control' => 'select2',
                        ],
                        'choices'   => $choices,
                        'empty_value' => '--Pilih--',
                        'selected' => isset($data[$key]) ? $data[$key] : ""
                    ]);
                    break;

                case "multiselect" :
                    $this->add($key."[]", 'select', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'attr' => [
                            'class' => 'w-100 select2 form-control mb-5 mt-3',
                            'multiple' => "",
                        ],
                        'choices'   => $choices,
                        'selected'  => isset($data[$key]) ? $data[$key] : ""
                    ]);
                    break;

                case "file" :
                    if (!isset($key)) {
                        $srcUrl = asset('storage/permohonan/'.$data[$key]);
                        $this->add($key, 'file', [
                            'label' => $value['title'],
                            'rules' => [
                                $value['required'] ? 'required' : '',
                                "max:{$value['max']}",
                                "mimes:{$value['mimes']}",
                                'attr' => [
                                    'class' => 'form-control mb-5 mt-3'
                                ],
                            ]
                        ]);

                        if (Str::containsAll($value['mimes'], ['pdf', 'doc', 'docx'])) {
                            $srcUrl = route('permohonan.downloadfile', ['filename' => $data[$key]]);
                            if ($data[$key] != null) {
                                $this->add($key.'_current', 'static', [
                                    'value' => new HtmlString('<a href="'.$srcUrl.'"> Download file '.$key.' </a>')
                                ]);
                            }
                        }
                        else {
                            $this->add($key.'_current', 'static', [
                                'value' => new HtmlString('<img src="'.$srcUrl.'" width="200" />')
                            ]);
                        }
                        break;
                    }   else {
                        $this->add($key, 'file', [
                            'label' => $value['title'],
                            'rules' => [
                                $value['required'] ? 'required' : '',
                                "max:{$value['max']}",
                                "mimes:{$value['mimes']}"
                            ]
                        ]);
                    }
            }
        }

        $this->add('idLayanan', 'hidden',
            [
                'value' => $idLayanan
            ]
        );

        $this->add('g_token', 'hidden');

        $this->add('submit', 'submit',
            [
                'attr' => ['class' => 'btn btn-primary waves-effect waves-classic w-100 mt-5', 'name' => 'submit'],
                'label' => 'Edit'
            ]
        );
    }
}
