<?php

namespace App\Forms;

use App\Helpers\OrganisasiHelper;
use Kris\LaravelFormBuilder\Form;

class PermohonanLayananForm extends Form
{
    public function buildForm()
    {
        $idLayanan = $this->getData('idLayanan');
        $parameterSurat = $this->getData('parameterSurat');
        $satker = $this->getData('satker');
        $dataTamu = $this->getData('dataTamu');
        $satker = $satker->mapWithKeys(function($item) {
            return [$item->id_satker => $item->nama_satker];
        })->all();


        $jenisPengaduan = $this->getData('jenisPengaduan');
        $asalPengaduan = $this->getData('asalPengaduan');
        $optionPps = $this->getData('optionPps');
        $jenisLayanan = $this->getData('jenisLayanan');
        $jenisBidang = $this->getData('jenisBidang');

        foreach($parameterSurat as $key => $value) {
            $choices = [];
            switch ($key) {
                case "satker" :
                    $choices = $satker;
                    break;
                case "jenis" :
                    $choices = $jenisPengaduan;
                    break;
                case "asal" :
                    $choices = $asalPengaduan;
                    break;
                case "pps" :
                    $choices = $optionPps;
                    break;
                case "jenis_layanan" :
                    $choices = $jenisLayanan;
                    break;
                case "jenis_bidang" :
                    $choices = $jenisBidang;
                    break;
                case "nik":
                    $value['value'] = isset($dataTamu->nik) ? $dataTamu->nik : "";
                    break;
                case "nama":
                    $value['value'] = isset($dataTamu->name) ? $dataTamu->name : "";
                    break;
                case "alamat":
                    $value['value'] = isset($dataTamu->address) ? $dataTamu->address : "";
                    break;
                case "telp":
                    $value['value'] = isset($dataTamu->phone)  ? $dataTamu->phone : "";
                    break;
            }

            switch($value['type']) {
                case "text" :
                    $this->add($key, 'text', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'value' => isset($value['value']) ? $value['value'] : '',
                        'attr' => [
                            'class' => 'w-100 form-control mb-5 mt-3'
                        ],
                    ]);
                    break;

                case "hidden" :
                    $this->add($key, 'hidden', [
                        'value' => isset($value['value']) ? $value['value'] : ''
                    ]);
                    break;

                case "textarea" :
                    $this->add($key, 'textarea', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'value' => isset($value['value']) ? $value['value'] : '',
                        'attr' => [
                            'class' => 'w-100 form-control mb-5 mt-3',
                            'rows' => 5
                        ],
                    ]);
                    break;

                case "wysiwyg" :
                        $this->add($key, 'textarea', [
                            'label' => $value['title'],
                            'rules' => $value['required'] ? 'required' : '',
                            'value' => isset($value['value']) ? $value['value'] : '',
                            'attr' => [
                                'class' => 'w-100 form-control mb-5 mt-3',
                                'rows' => 5
                            ],
                        ]);
                        break;

                case "date" :
                    $this->add($key, 'text', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'attr' => [
                            'class' => "form-control date mb-5 mt-3"
                        ]
                    ]);
                    break;

                case "select" :
                    $this->add($key, 'select', [
                        'label' => $value['title'],
                        'rules' => $value['required'] ? 'required' : '',
                        'attr' => [
                            'class' => 'w-100 form-control mb-5 mt-3 ',
                            'data-control' => 'select2',
                        ],
                        'choices'   => $choices,
                        'empty_value' => '--Pilih--'
                    ]);
                    break;

                case "multiselect" :
                        $this->add($key."[]", 'select', [
                            'label' => $value['title'],
                            'rules' => $value['required'] ? 'required' : '',
                            'attr' => [
                                'class' => 'w-100 select2 form-control mb-5 mt-3'
                            ],
                            'attr' => [
                                'class' => 'w-100 select2 form-control mb-5 mt-3',
                                'multiple' => ""
                            ],
                            'choices'   => $choices
                        ]);
                        break;

                case "file" :
                    $this->add($key, 'file', [
                        'label' => $value['title'],
                        'rules' => [
                            $value['required'] ? 'required' : '',
                            'attr' => [
                                'class' => 'form-control mb-5 mt-3'
                            ],
                            "max:{$value['max']}",
                            "mimes:{$value['mimes']}"
                        ]
                    ]);
                    break;
            }
        }

        $this->add('idLayanan', 'hidden', [
            'value' => $idLayanan
        ]);

        $this->add('g_token', 'hidden');

        if (count($parameterSurat) > 0) {
            $this->add('submit', 'submit', [
                'attr' => ['class' => 'btn btn-primary waves-effect waves-classic w-100 mt-5', 'name' => 'submit'],
                'label' => 'Create'
            ]);
        }
    }
}
