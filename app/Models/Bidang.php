<?php

namespace App\Models;

use App\Models\Organisasi;
use App\Helpers\OrganisasiHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bidang extends Model
{
    use HasFactory;

    protected $table = 'bidang';
    protected $primaryKey = 'id_bidang';
    protected $guarded = [];

    public function scopeOrder($query) 
    {
        return $query->orderBy('parent_id')
                        ->orderBy('nama_bidang');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id_bidang')
                    ->with('parent')
                    ->withDefault();
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id_bidang')
                    ->with(['children' => function($q) {
                            $q->orderBy('parent_id');
                        }]);
    }

    public function childrenWithoutMain()
    {
        return $this->children()->where('main', '<>', 1);
    }

}
