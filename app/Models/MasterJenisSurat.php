<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterJenisSurat extends Model
{
    use HasFactory;

    protected $table = 'master_jenis_surat';
    protected $primaryKey = 'id_surat';
    protected $guarded = [];
    public $timestamps = false;

    protected $casts = [
        'parameter' => 'array'
    ];
}
