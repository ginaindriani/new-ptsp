<?php

namespace App\Models;

use App\Models\Organisasi;
use Illuminate\Support\Str;
use App\Models\PermohonanLayananLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pengumuman extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "pengumuman";
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $guarded = [];
    
}
