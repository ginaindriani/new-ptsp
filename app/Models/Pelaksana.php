<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pelaksana extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "pelaksana";
    public $incrementing = false;
    protected $primaryKey = 'id_pelaksana';
    protected $guarded = [];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }

    public function organisasi()
    {
        return $this->belongsTo(Organisasi::class, 'id_organisasi', 'id_organisasi')->withDefault();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

    public function pelaksanaLog()
    {
        return $this->hasMany(PelaksanaLog::class, 'id_pelaksana', 'id_pelaksana');
    }
}
