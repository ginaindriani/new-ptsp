<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class KunjunganTilangDetail extends Model
{
    use HasFactory, Uuid;

    protected $table = 'kunjungan_tilang_detail';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'alamat',
        'bb',
        'bp',
        'denda',
        'form',
        'kode_ins',
        'nama',
        'no_briva',
        'no_ranmor',
        'no_reg_tilang',
        'pasal',
        'subsider',
        'tgl_bayar',
        'tgl_sidang',
        'uang_titipan',
        'tgl_ambil',
        'nama_petugas',
        'jenis_kendaraan',
        'nama_hakim',
        'nama_panitera',
        'kunjungan_tilang_id'
    ];
	public $timestamps=true;
}
