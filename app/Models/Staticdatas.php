<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staticdatas extends Model
{
    use HasFactory;

    //because we dont have this table, and not sure should I create one?
    public static function education() {
        $list = [];
        $list[1] = 'Tidak Sekolah';
        $list[2] = 'Sekolah Dasar / Sederajat';
        $list[3] = 'Sekolah Menengah Pertama / Sederajat';
        $list[4] = 'Sekolah Lanjut Tingkat Atas / Sederajat';
        $list[5] = 'Akademi / Diploma';
        $list[6] = 'Diploma / Strata I';
        $list[7] = 'Strata II';
        $list[8] = 'Strata III';

        return $list;
    }

    //because we dont have this table, and not sure should I create one?
    public static function citizenship() {
        $list = [];
        $list[1] = 'WNI';
        $list[2] = 'WNA';

        return $list;
    }

    //because we dont have this table, and not sure should I create one?
    public static function gender() {
        $list = [];
        $list[1] = 'Laki-Laki';
        $list[2] = 'Perempuan';

        return $list;
    }

    //because we dont have this table, and not sure should I create one?
    public static function identity_type() {
        $list = [];
        // $list[1] = 'KTP (Kartu Tanda Penduduk)';
        // $list[2] = 'SIM (Surat Izin Mengemudi)';
        // $list[3] = 'Passport';
        $list[1] = 'KTP';
        $list[2] = 'SIM';
        $list[3] = 'Passport';

        return $list;
    }

    //because we dont have this table, and not sure should I create one?
    public static function satker_type() {
        $list = [];
        $list[1] = 'Kejaksaan Agung';
        $list[2] = 'Kejaksaan Tinggi';
        $list[3] = 'Kejaksaan Negeri';
        $list[4] = 'Cabang Kejaksaan Negeri';

        return $list;
    }

    //because we dont have this table, and not sure should I create one?
    public static function default_status() {
        $list = [];
        $list[0] = 'Inactive';
        $list[1] = 'Active';
        $list[2] = 'Deleted';

        return $list;
    }

    //because we dont have this table, and not sure should I create one?
    public static function kunjungan_status() {
        $list = [];
        $list[0] = 'Waiting for Approval';
        $list[1] = 'On Progress';
        $list[2] = 'Deleted';
        $list[3] = 'Finished';
        $list[4] = 'Rejected';

        return $list;
    }

    //because we dont have this table, and not sure should I create one?
    public static function tamu_type() {
        $list = [];
        $list[1] = 'VVIP';
        $list[3] = 'Employee';
        $list[2] = 'Visitor';
        $list[4] = 'Bad Guy';

        return $list;
    }

    //because we dont have this table, and not sure should I create one?
    public static function employee_flag() {
        $list = [];
        $list[1] = 'Aktif';
        $list[2] = 'Pensiun';
        $list[3] = 'Dikembalikan ke Instasi';
        $list[4] = 'Dipecat';

        return $list;
    }
}
