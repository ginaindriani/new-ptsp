<?php

namespace App\Models;

use App\Models\Organisasi;
use Illuminate\Support\Str;
use App\Models\PermohonanLayananLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PermohonanLayanan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "permohonan_layanan";
    public $incrementing = false;
    protected $primaryKey = 'id_permohonan_layanan';
    protected $guarded = [];

    protected $casts = [
        'konten' => 'array'
    ];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }
    
    public function PermohonanLayananLog()
    {
        return $this->hasMany(PermohonanLayananLog::class, 'id_permohonan_layanan', 'id_permohonan_layanan');
    }
}
