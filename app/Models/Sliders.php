<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sliders extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "sliders";
    protected $primaryKey = 'id';
    protected $guarded = [];
    
}
