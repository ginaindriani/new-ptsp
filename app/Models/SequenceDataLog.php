<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SequenceDataLog extends Model
{
    use HasFactory;

    protected $table = "sequence_data_log";
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $guarded = [];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }
}
