<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class TamuPengikut extends Model
{
    use HasFactory, Uuid;

    protected $table = 'tamu_pengikut';


    protected $fillable = [
        'uuid',
        'id_kunjungan',
        'nama_pengikut',
        'jenis_kelamin',
        'is_anak',
        'urutan_tamu'
    ];
    

}
