<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TujuanPenerusan extends Model
{
    use HasFactory;

    protected $table = "tujuan_penerusan";
    protected $primaryKey = 'id_penerusan';
    public $incrementing = false;
    protected $guarded = [];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }

    public function user()
    {
        return $this->hasMany(User::class, 'id_organisasi', 'id_tujuan_penerusan');
    }

    public function scopeWithUser($query) {
        return $query->with('user:id,id_organisasi,nama');
    }
}
