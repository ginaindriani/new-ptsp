<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    use HasFactory;

    protected $table = 'jabatan';
    protected $primaryKey = 'id_jabatan';
    protected $guarded = [];

    protected $appends = ['label_type_jabatan', 'label_is_admin'];
    
    public function scopeOrder($query) 
    {
        return $query->orderBy('id_jabatan');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id_jabatan')
                    ->with('parent')
                    ->withDefault();
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id_jabatan')
                    ->with(['children' => function($q) {
                            $q->orderBy('parent_id');
                        }]);
    }

    public function getLabelTypeJabatanAttribute()
    {
        switch ($this->eselon) {
            case 1:
                return "Eselon 1";
                break;
            case 2:
                return "Eselon 2";
                break;
            case 3:
                return "Eselon 3";
                break;
            case 4:
                return "Eselon 4";
                break;
            case 5:
                return "Eselon 5";
                break;
        }
    }

    public function getLabelIsAdminAttribute()
    {
        switch ($this->is_admin) {
            case 0:
                return '<span class="badge badge-secondary px-2 py-1">Tidak</span>';
                break;
            case 1:
                return '<span class="badge badge-success px-2 py-1">Ya</span>';
                break;
        }
    }
}
