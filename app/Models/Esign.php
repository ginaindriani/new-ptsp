<?php

namespace App\Models;

use App\Models\City;
use App\Models\User;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Esign extends Model
{
    use HasFactory;

    protected $table = 'esign';
    protected $primaryKey = 'id_esign';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user', 'id')->withDefault();
    }

    public function provinsi()
    {
        return $this->belongsTo(Province::class, 'id_provinsi', 'id');
    }

    public function kota()
    {
        return $this->belongsTo(City::class, 'id_kota', 'id');
    }
    
}
