<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Kunjungan extends Model
{
    use HasFactory, Uuid;

    protected $table = 'kunjungan';

    protected $primaryKey = 'id';

    protected $fillable = [
        'uuid',
        'id_tamu',
        'id_satker',
        'id_organisasi',
        'id_pegawai',
        'no_kendaraan',
        'jabatan_pegawai',
        'unique_id',
        'massgate_id',
        'evolv_mark',
        'evolv_alarm',
        'entry_status',
        'temperature',
        'evolv_scan_id',
        'no_berkas',
        'guest_no',
        'detail',
        'photo',
        'photo_kendaraan',
        'pdf_file',
        'qr_image',
        'jumlah_tamu',
        'tipe_tamu',
        'in',
        'out',
        'tanggal_expired',
        'reject_reason',
        'massgate_active',
        'status',
        'no_polisi',
        'no_hp',
        'email',
        'alamat',
        'no_identitas',
        'nama',
        'name',
        'jabatan',
        'barang_bawaan',
        'barang_ditinggal',
        'nama_tahanan',
        'tipe_pelayanan'
    ];
	public $timestamps=true;

}
