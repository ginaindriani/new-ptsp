<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class SequenceData extends Model
{
    protected $table = "sequence_data";
    public $incrementing = false;
    protected $primaryKey = 'sequence_name';
    protected $guarded = [];
    public $timestamps = false;

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }
}