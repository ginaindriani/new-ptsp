<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MasterIntruksi extends Model
{
    use HasFactory;

    protected $table = "master_intruksi";
    protected $primaryKey = 'id_intruksi';
    public $incrementing = false;
    protected $guarded = [];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }
}
