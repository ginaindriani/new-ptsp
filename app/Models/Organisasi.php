<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organisasi extends Model
{
    use HasFactory;

    protected $table = 'organisasi';
    protected $primaryKey = 'id_organisasi';
    protected $guarded = [];

    public function scopeOrder($query) 
    {
        return $query->orderBy('id_satker')
                        ->orderBy('id_bidang')
                        ->orderBy('id_jabatan')
                        ->orderByDesc('created_at');
    }

    public function satker()
    {
        return $this->belongsTo(Satker::class, 'id_satker', 'id_satker')->withDefault();
    }

    public function bidang()
    {
        return $this->belongsTo(Bidang::class, 'id_bidang', 'id_bidang')->withDefault();
    }

    public function jabatan()
    {
        return $this->belongsTo(Jabatan::class, 'id_jabatan', 'id_jabatan')->withDefault();
    }

    public function user()
    {
        return $this->hasMany(User::class, 'id_organisasi', 'id_organisasi');
    }

    public function scopeWithData($query) {
        return $query->with('satker:id_satker,nama_satker,tipe_satker','bidang:id_bidang,nama_bidang','jabatan:id_jabatan,nama_jabatan', 'user:id,id_organisasi,nama');
    }
}
