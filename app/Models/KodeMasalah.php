<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodeMasalah extends Model
{
    use HasFactory;

    protected $table = "kode_masalah";
    public $incrementing = false;
    protected $primaryKey = 'kode';
    protected $guarded = [];
}
