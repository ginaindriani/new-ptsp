<?php

namespace App\Models;

use App\Models\SuratKeluar;
use Illuminate\Support\Str;
use App\Models\SuratMasukLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SuratMasuk extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = "surat_masuk";
    public $incrementing = false;
    protected $primaryKey = 'id_surat_masuk';
    protected $guarded = [];

    protected $casts = [
        'konten' => 'array'
    ];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }

    public function suratkeluar()
    {
        return $this->belongsTo(SuratKeluar::class, 'id_surat_keluar', 'id_surat_keluar')->withDefault();
    }

    public function suratMasukLog()
    {
        return $this->hasMany(SuratMasukLog::class, 'id_surat_masuk', 'id_surat_masuk');
    }
}
