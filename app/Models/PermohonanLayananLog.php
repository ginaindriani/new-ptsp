<?php

namespace App\Models;

use App\Models\PermohonanLayanan;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PermohonanLayananLog extends Model
{
    use HasFactory;

    protected $table = "permohonan_layanan_log";
    public $incrementing = false;
    protected $guarded = [];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }

    public function PermohonanLayanan()
    {
        return $this->belongsTo(PermohonanLayanan::class, 'id_permohonan_layanan', 'id_permohonan_layanan')->withDefault();
    }
}
