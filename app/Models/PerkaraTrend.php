<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerkaraTrend extends Model
// {
//     use HasFactory;
//     protected $connection = 'dbtrend_analysis';
//     protected $table = 'new_data_perkaras';
// }

{
    use HasFactory, SoftDeletes;
    protected $table = "new_data_perkaras";
}
