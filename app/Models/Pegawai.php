<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Pegawai extends Model
{
    use HasFactory, Uuid;

    protected $table = 'pegawai';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'nik',
        'nrp',
        'nip',
        'name',
        'jk',
        'nama_tmpt_lahir',
        'tmpt_lahir',
        'tgl_lahir',
        'usia',
        'alamat_domisili',
        'kota_alamat',
        'id_kota',
        'alamat_ktp',
        'kota_alamat_ktp',
        'id_kota_ktp',
        'agama',
        'pernikahan',
        'no_kk',
        'no_karpeg',
        'no_bpjs',
        'no_npwp',
        'no_taspen',
        'no_hp',
        'jabatan',
        'eselon',
        'golpang',
        'kode_satker',
        'nama_satker',
        'email_dinas',
        'email_alternatif',
        'foto',
        'foto_mobile',
        'foto_alternatif',
        'gol',
        'pangkat',
        'status_pegawai',
        'id_organisasi',
        'status',
        'created_at',
        'updated_at'
    ];

    public function organisasi() {
        return $this->belongsTo(Organisasi::class, 'id_organisasi', 'id_organisasi');
    }

}
