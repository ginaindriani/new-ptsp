<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TamuTambahan extends Model
{
    use HasFactory;

    protected $table = 'tamu_tambahan';

    protected $guarded = [];
}
