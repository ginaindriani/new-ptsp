<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Satker extends Model
{
    use HasFactory;

    protected $table = "satker";
    public $timestamps = false;
    protected $primaryKey = 'id_satker';
    protected $guarded = [];
    protected $appends = ['label_type_satker'];
    
    public function scopeOrder($query) 
    {
        return $query->orderBy('tipe_satker')
                        ->orderBy('parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id_satker')
                    ->with('parent')
                    ->withDefault();
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id_satker')
                    ->with(['children' => function($q) {
                            $q->orderBy('parent_id');
                        }]);
    }

    public function getLabelTypeSatkerAttribute()
    {
        switch ($this->tipe_satker) {
            case 1:
                return "Kejaksaan Agung";
                break;
            case 2:
                return "Kejaksaan Tinggi";
                break;
            case 3:
                return "Kejaksaan Negeri";
                break;
            case 4:
                return "Cabang Kejaksaan Negeri";
                break;
        }
    }
}
