<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class KunjunganTilang extends Model
{
    use HasFactory, Uuid;

    protected $table = 'kunjungan_tilang';

    protected $primaryKey = 'id';

    protected $fillable = [
        'uuid',
        'id_satker',
        'unique_id',
        'guest_no',
        'photo',
        'qr_image',
        'nama_tamu',
        'nomor_berkas_tilang',
        'alamat'
    ];
	public $timestamps=true;
}
