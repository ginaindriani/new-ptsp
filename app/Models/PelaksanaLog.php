<?php

namespace App\Models;

use App\Models\Pelaksana;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PelaksanaLog extends Model
{
    use HasFactory;

    protected $table = "pelaksana_log";
    public $incrementing = false;
    protected $guarded = [];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }

    public function pelaksana()
    {
        return $this->belongsTo(Pelaksana::class, 'id_pelaksana', 'id_pelaksana')->withDefault();
    }
}
