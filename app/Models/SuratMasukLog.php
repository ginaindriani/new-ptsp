<?php

namespace App\Models;

use App\Models\User;
use App\Models\SuratMasuk;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SuratMasukLog extends Model
{
    use HasFactory;

    protected $table = "surat_masuk_log";
    public $incrementing = false;
    protected $guarded = [];

    protected static function boot()
    {
            
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });

    }

    public function suratMasuk()
    {
        return $this->hasMany(SuratMasuk::class, 'id_surat_masuk', 'id_surat_masuk')->withDefault();
    }

    public function user()
    {
        return $this->hasMany(User::class, 'id_organisasi', 'id_organisasi');
    }

    public function scopeWithUser($query) {
        return $query->with('user:id,id_organisasi,nama');
    }

}
