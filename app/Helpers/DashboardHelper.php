<?php

namespace App\Helpers;

use App\Models\Bidang;
use App\Models\SuratMasuk;
use App\Models\SuratKeluar;
use App\Models\SuratMasukLog;
use App\Models\SuratKeluarLog;
use App\Helpers\OrganisasiHelper;
use Illuminate\Support\Facades\DB;

class DashboardHelper {

    public static function blmDisposisi($tahun)
    {
        $data = SuratMasuk::join('surat_masuk_log', function ($join) {
            $join->on('surat_masuk_log.id_surat_masuk', '=', 'surat_masuk.id_surat_masuk')
            ->where('surat_masuk_log.id_organisasi', auth()->user()->id_organisasi);
        })
        ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_masuk.jenis_surat')
        ->whereYear('surat_masuk.created_at', '=', $tahun)
        ->get();

        $totalNonDisposisi = $data->filter(function ($item, $key) {
            return $item['created_by'] == null && $item['checked_at'] == null && $item['status'] == null 
                    && $item['type'] != 'tembusan' && !$item['is_archive'];
        })->count();

        return $totalNonDisposisi;
    }

    public static function tembusan($tahun)
    {
        $data = SuratMasuk::join('surat_masuk_log', function ($join) {
            $join->on('surat_masuk_log.id_surat_masuk', '=', 'surat_masuk.id_surat_masuk')
            ->where('surat_masuk_log.id_organisasi', auth()->user()->id_organisasi);
        })
        ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_masuk.jenis_surat')
        ->whereYear('surat_masuk.created_at', '=', $tahun)
        ->get();

        $totalTembusan = $data->filter(function ($item, $key) {
            return $item['type'] == 'tembusan';
        })->count();

        return $totalTembusan;
    }

    public static function terkirim($tahun)
    {
        $latestLogs = SuratKeluarLog::select('id_surat_keluar', 'id_organisasi', DB::raw('MAX(created_at) created_at'))
        ->groupBy('id_surat_keluar', 'id_organisasi');

        $data = SuratKeluar::joinSub($latestLogs, 'latest_logs', function ($join) {
            $join->on('surat_keluar.id_surat_keluar', '=', 'latest_logs.id_surat_keluar')
            ->where('latest_logs.id_organisasi', auth()->user()->id_organisasi);
        })
        ->join('surat_keluar_log', function ($join) {
            $join->on('surat_keluar_log.id_organisasi', '=', 'latest_logs.id_organisasi')
            ->whereRaw('surat_keluar_log.created_at = latest_logs.created_at');
        })
        ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_keluar.jenis_surat')
        ->whereYear('surat_keluar.created_at', '=', $tahun);

        $totalTerkirim = $data->whereNotNull('surat_keluar_log.checked_at')->count();

        return $totalTerkirim;
    }

    public static function persetujuan($tahun)
    {
        $latestLogs = SuratKeluarLog::select('id_surat_keluar', 'id_organisasi', DB::raw('MAX(created_at) created_at'))
        ->groupBy('id_surat_keluar', 'id_organisasi');

        $data = SuratKeluar::joinSub($latestLogs, 'latest_logs', function ($join) {
            $join->on('surat_keluar.id_surat_keluar', '=', 'latest_logs.id_surat_keluar');
        })
        ->join('surat_keluar_log', function ($join) {
            $join->on('surat_keluar_log.id_organisasi', '=', 'latest_logs.id_organisasi')
            ->whereRaw('surat_keluar_log.created_at = latest_logs.created_at');
        })
        ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_keluar.jenis_surat')
        ->leftJoin('pelaksana', function ($join) {
            $join->on('pelaksana.id_organisasi', '=', 'surat_keluar_log.id_organisasi')
                ->whereRaw('DATE(now()) BETWEEN pelaksana.start_date AND pelaksana.end_date')
                ->orWhereRaw('(DATE(now()) >= start_date  AND end_date IS NULL)');
        })
        ->where(function($q) {
            $q->where('latest_logs.id_organisasi', auth()->user()->id_organisasi)
            ->orWhere(function($q) {
                $q->where('pelaksana.user_id', auth()->user()->id)
                ->where('master_jenis_surat.pelaksana', 1);
            });
        })
        ->whereYear('surat_keluar.created_at', '=', $tahun);

        $totalTerkirim = $data->whereNull('surat_keluar_log.checked_at')->count();

        return $totalTerkirim;
    }

    public static function bidangNonKejagungMasuk($id_bidang, $level, $tahun)
    {
        $totalMasuk = '~';

        if ($level == 1) {
            $totalMasuk = SuratMasukLog::join('organisasi', 'surat_masuk_log.id_organisasi', '=', 'organisasi.id_organisasi')
                        ->join('bidang', 'organisasi.id_bidang', '=', 'bidang.id_bidang')
                        ->join('surat_masuk', 'surat_masuk.id_surat_masuk', '=', 'surat_masuk_log.id_surat_masuk')
                        ->where('organisasi.id_satker', auth()->user()->organisasi->id_satker)
                        ->where('bidang.id_bidang', $id_bidang)
                        ->whereYear('surat_masuk.created_at', '=', $tahun)
                        ->distinct('surat_masuk.id_surat_masuk')->count('surat_masuk.id_surat_masuk');

        } else {
            $children = OrganisasiHelper::getChildrenBidangWithoutMain($id_bidang);
            if ($children->parent_id != null) {
                $childrenId = OrganisasiHelper::getIdParentBidang($children);
            } else {
                $childrenId[] = $id_bidang;
            }

            $totalMasuk = SuratMasuk::join('surat_masuk_log', 'surat_masuk_log.id_surat_masuk', '=', 'surat_masuk.id_surat_masuk')
                    ->join('organisasi', 'organisasi.id_organisasi', '=', 'surat_masuk_log.id_organisasi')
                    ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_masuk.jenis_surat')
                    ->whereIn('organisasi.id_bidang', $childrenId)
                    ->where('organisasi.id_satker', auth()->user()->organisasi->id_satker)
                    ->whereYear('surat_masuk.created_at', '=', $tahun)
                    ->distinct('surat_masuk.id_surat_masuk')->count('surat_masuk.id_surat_masuk');
        }

        return $totalMasuk;
    }

    public static function bidangKejagungMasuk($id_bidang, $level, $tahun)
    {
        $totalMasuk = '~';

        if ($id_bidang == 1) {
            $IdOrganisasiAdmin = OrganisasiHelper::getIdAdminTuPusat();
            $totalMasuk = SuratMasukLog::join('organisasi', 'surat_masuk_log.id_organisasi', '=', 'organisasi.id_organisasi')
                            ->join('bidang', 'organisasi.id_bidang', '=', 'bidang.id_bidang')
                            ->join('surat_masuk', 'surat_masuk.id_surat_masuk', '=', 'surat_masuk_log.id_surat_masuk')
                            ->whereNull('from')
                            ->where('organisasi.id_satker', '1')
                            ->where('surat_masuk_log.id_organisasi', $IdOrganisasiAdmin)
                            ->whereYear('surat_masuk.created_at', '=', $tahun)
                            ->count();
        }
        else {

            $children = OrganisasiHelper::getChildrenBidangWithoutMain($id_bidang);
            if ($children->main == 1) {
                $totalMasuk = SuratMasukLog::join('organisasi', 'surat_masuk_log.id_organisasi', '=', 'organisasi.id_organisasi')
                        ->join('bidang', 'organisasi.id_bidang', '=', 'bidang.id_bidang')
                        ->join('surat_masuk', 'surat_masuk.id_surat_masuk', '=', 'surat_masuk_log.id_surat_masuk')
                        ->where('organisasi.id_satker', '1')
                        ->where('bidang.id_bidang', $id_bidang)
                        ->whereYear('surat_masuk.created_at', '=', $tahun)
                        ->distinct('surat_masuk.id_surat_masuk')->count('surat_masuk.id_surat_masuk');
            } else {
                $childrenId = OrganisasiHelper::getIdParentBidang($children);

                $totalMasuk = SuratMasukLog::join('organisasi', 'surat_masuk_log.id_organisasi', '=', 'organisasi.id_organisasi')
                            ->join('bidang', 'organisasi.id_bidang', '=', 'bidang.id_bidang')
                            ->join('surat_masuk', 'surat_masuk.id_surat_masuk', '=', 'surat_masuk_log.id_surat_masuk')
                            ->where('organisasi.id_satker', '1')
                            ->whereIn('bidang.id_bidang', $childrenId)
                            ->whereYear('surat_masuk.created_at', '=', $tahun)
                            ->distinct('surat_masuk.id_surat_masuk')->count('surat_masuk.id_surat_masuk');
            }
        }

        return $totalMasuk;
    }

    public static function bidangNonKejagungKeluar($id_bidang, $level, $tahun)
    {

        $totalKeluar = '~';

        $children = OrganisasiHelper::getChildrenBidangWithoutMain($id_bidang);
        if ($children->parent_id != null) {
            $childrenId = OrganisasiHelper::getIdParentBidang($children);
        } else {
            $childrenId[] = $id_bidang;
        }

        $totalKeluar = SuratKeluar::join('organisasi', 'organisasi.id_organisasi', '=', 'surat_keluar.id_organisasi')
            ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_keluar.jenis_surat')
            ->whereIn('organisasi.id_bidang', $childrenId)
            ->where('organisasi.id_satker', auth()->user()->organisasi->id_satker)
            ->whereYear('surat_keluar.created_at', '=', $tahun)
            ->count();
        
        return $totalKeluar;
    }

    public static function bidangKejagungKeluar($id_bidang, $level, $tahun)
    {
        $totalKeluar = '~';
        if ($id_bidang == 1) {
            $IdOrganisasiAdmin = OrganisasiHelper::getIdAdminTuPusat();
            $idBidang = [$id_bidang, $IdOrganisasiAdmin];

            $totalKeluar = SuratKeluar::join('organisasi', 'organisasi.id_organisasi', '=', 'surat_keluar.id_organisasi')
                ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_keluar.jenis_surat')
                ->whereIn('organisasi.id_bidang', $idBidang)
                ->where('organisasi.id_satker', '1')
                ->whereNull('deleted_at')
                ->whereYear('surat_keluar.created_at', '=', $tahun)
                ->count();
        } else {
            $children = OrganisasiHelper::getChildrenBidangWithoutMain($id_bidang);
            $childrenId = OrganisasiHelper::getIdParentBidang($children);

            $totalKeluar = SuratKeluar::join('organisasi', 'organisasi.id_organisasi', '=', 'surat_keluar.id_organisasi')
            ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_keluar.jenis_surat')
            ->whereIn('organisasi.id_bidang', $childrenId)
            ->where('organisasi.id_satker', '1')
            ->whereNull('deleted_at')
            ->whereYear('surat_keluar.created_at', '=', $tahun)
            ->count();
        }

        return $totalKeluar;
    }
}

?>