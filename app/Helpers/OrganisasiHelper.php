<?php

namespace App\Helpers;

use App\Models\Bidang;
use App\Models\Satker;
use App\Models\Jabatan;
use App\Models\Organisasi;
use Illuminate\Support\Str;

class OrganisasiHelper {

    public static function getParentSatker($idSatker) 
    {
        $satker = Satker::with('parent')->where('id_satker', $idSatker)->first();
        return $satker;
    }

    public static function getChildrenSatker($idSatker) 
    {
        $satker = Satker::with('children')->where('id_satker', $idSatkers)->first();
        return $satker;
    }

    public static function getParentBidang($idBidang) 
    {
        $bidang = Bidang::with('parent')->where('id_bidang', $idBidang)->first();
        return $bidang;
    }

    public static function getChildrenBidang($idBidang) 
    {
        $bidang = Bidang::with('children')->where('id_bidang', $idBidang)->first();
        return $bidang;
    }

    public static function getChildrenBidangWithoutMain($idBidang) 
    {
        $bidang = Bidang::with('childrenWithoutMain')->where('id_bidang', $idBidang)->first();
        return $bidang;
    }

    public static function getParentJabatan($idJabatan) 
    {
        $jabatan = Jabatan::with('parent')->where('id_jabatan', $idJabatan)->first();
        return $jabatan;
    }

    public static function getChildrenJabatan($idJabatan) 
    {
        $jabatan = Jabatan::with('children')->where('id_jabatan', $idJabatan)->first();
        return $jabatan;
    }

    public static function getIdChildSatker($idSatker)
    {
        $tree = array();
        if (!empty($idSatker)) {
            $tree = Satker::where('parent_id', $idSatker)->pluck('id_satker')->toArray();
            foreach ($tree as $key => $val) {
                $ids = self::getIdChildSatker($val);
                if(!empty($ids)){
                    if(count($ids)>0) $tree = array_merge($tree, $ids);
                }
            }
        }
        return $tree;
    }

    public static function getIdParentBidang($data) {
        $id = [$data->id_bidang];
        $flatten = self::flatten($data->toArray());
        $idParent = collect($flatten)->pluck('id_bidang');
        $idMerge = array_merge($id, $idParent->toArray());
        return $idMerge;
    }

    public static function getFlattenParentBidang($data) {
        $id = [collect($data)->except(['parent'])->toArray()];
        $flatten = self::flatten($data->toArray());
        $idMerge = array_merge($id, $flatten);
        return $idMerge;
    }

    public static function getFlattenChildrenBidang($data) {
        $id = [collect($data)->except(['children'])->toArray()];
        $flatten = self::flatten($data->toArray());
        $idMerge = array_merge($id, $flatten);
        return $idMerge;
    }

    public static function flatten($array) {
        $result = array();
        foreach ($array as $item) {
            if (is_array($item)) {
                $result[] = array_filter($item, function($array) {
                    return ! is_array($array);
                });
                $result = array_merge($result, self::flatten($item));
            } 
        }
        return array_filter($result);
    }

    public static function getSatkerBidangName($idOrganisasi) 
    {
        $organisasi = Organisasi::with('satker', 'bidang', 'jabatan')->findOrFail($idOrganisasi);
        return $organisasi->satker->nama_satker . " | " .$organisasi->bidang->nama_bidang;
    }

    public static function getSatkerBidangNameArr($idOrganisasi) 
    {
        $result = "";
        $organisasi = Organisasi::with('satker', 'bidang', 'jabatan')
                                    ->whereIn('id_organisasi', $idOrganisasi)
                                    ->get();
        foreach ($organisasi as $key => $org) {
            $result .= $org->satker->nama_satker . " | " .$org->bidang->nama_bidang.", ";
        }
        return rtrim($result, ", ");
    }

    public static function getIdAdminOrganisasiArr($idTujuan) 
    {
        $IdOrganisasiAdmin = collect();
        foreach ($idTujuan as $key => $value) {
            $detailOrganisasi = self::getDetailOrganisasi($value);
            if ($detailOrganisasi->bidang->nama_bidang == 'JAKSA AGUNG') {
                $IdOrganisasiAdmin = $IdOrganisasiAdmin->merge(collect([self::getIdAdminTuPusat()]));
            }
            else {
                if ($detailOrganisasi->id_satker == auth()->user()->organisasi->id_satker) {
                    $IdOrganisasiAdmin = $IdOrganisasiAdmin->merge(collect([(int) $value]));
                } else {
                    $idBidangChild = self::getIdChildBidang($detailOrganisasi->id_bidang);
                    $idBidang = array_merge([$detailOrganisasi->id_bidang], $idBidangChild);
                    $organisasi = Organisasi::whereHas('jabatan', function($q) {
                                                $q->where('is_admin', true);
                                            })
                                            ->whereIdSatker($detailOrganisasi->id_satker)
                                            ->whereIn('id_bidang', $idBidang)
                                            ->first();
                    if (!$organisasi) {
                        $parentBidang = self::getParentBidang($detailOrganisasi->id_bidang);
                        $idBidangParent = self::getIdParentBidang($parentBidang);
                        $organisasi = Organisasi::whereHas('jabatan', function($q) {
                                                    $q->where('is_admin', true);
                                                })
                                                ->whereIdSatker($detailOrganisasi->id_satker)
                                                ->whereIn('id_bidang', $idBidangParent)
                                                ->first();
                        if ($organisasi) {
                            $IdOrganisasiAdmin = $IdOrganisasiAdmin->merge(collect([$organisasi->id_organisasi])); 
                        }
                        else {
                            $IdOrganisasiAdmin = $IdOrganisasiAdmin->merge(collect([(int) $value]));
                        }
                    }
                    else {
                        $IdOrganisasiAdmin = $IdOrganisasiAdmin->merge(collect([$organisasi->id_organisasi]));
                    }
                }
            }
        }

        return $IdOrganisasiAdmin->unique()->values()->all();
    }

    public static function getIdAdminTuPusat() 
    {
        $organisasi = Organisasi::whereHas('satker', function($q) {
                                    $q->whereIdSatker(1);
                                })
                                ->whereHas('bidang', function($q) {
                                    $q->where('nama_bidang', 'like', '%BAGIAN TATA USAHA UMUM DAN PIMPINAN%');
                                })
                                ->whereHas('jabatan', function($q) {
                                    $q->where('nama_jabatan', 'like', '%KEPALA BAGIAN TATA USAHA UMUM DAN PIMPINAN%')
                                        ->orWhere('id_jabatan', 30);
                                })
                                ->first();
        if ($organisasi) {
            return $organisasi->id_organisasi;
        }
        return null;
    }

    public static function getDetailOrganisasi($idOrganisasi) 
    {
        $organisasi = Organisasi::with('satker', 'bidang', 'jabatan')
                                ->whereIdOrganisasi($idOrganisasi)
                                ->first();
        
        return $organisasi;
    }

    public static function checkIsAdmin($idOrganisasi) 
    {
        $organisasi = Organisasi::whereHas('jabatan', function($q) {
                                    $q->where('is_admin', true);
                                })
                                ->whereIdOrganisasi($idOrganisasi)
                                ->exists();
        
        return $organisasi;
    }

    public static function getIdChildBidang($idBidang)
    {
        $tree = array();
        if (!empty($idBidang)) {
            $tree = Bidang::where('parent_id', $idBidang)->pluck('id_bidang')->toArray();
            foreach ($tree as $key => $val) {
                $ids = self::getIdChildBidang($val);
                if(!empty($ids)){
                    if(count($ids)>0) $tree = array_merge($tree, $ids);
                }
            }
        }
        return $tree;
    }
}

?>