<?php

namespace App\Helpers;

use App\Models\Bidang;
use App\Models\Satker;
use App\Models\Jabatan;
use App\Models\Pelaksana;
use App\Models\Organisasi;
use App\Models\SuratMasuk;
use Illuminate\Support\Str;

class StatistikSuratHelper {
    
    public static function suratMasukOrganisasi()
    {
        $data = SuratMasuk::join('surat_masuk_log', 'surat_masuk_log.id_surat_masuk', '=', 'surat_masuk.id_surat_masuk')
                ->leftJoin('pelaksana', function($join) {
                        $join->on('pelaksana.id_organisasi', '=', 'surat_masuk_log.id_organisasi')
                                ->where('pelaksana.user_id', auth()->user()->id)
                                ->whereNotNull('pelaksana.deleted_at');
                })
                ->join('master_jenis_surat', 'master_jenis_surat.id_surat', '=', 'surat_masuk.jenis_surat')
                ->where(function($q) {
                        $q->where('surat_masuk_log.id_organisasi', auth()->user()->id_organisasi)
                        ->orWhereNotNull('pelaksana.id_pelaksana');
                })
                ->select('created_by', 'checked_at', 'status', 'id_surat_keluar', 'type', 'is_archive')
                ->get();
        
        $totalManual = $data->filter(function ($item, $key) {
                    return $item['created_by'] == null && $item['checked_at'] == null && $item['status'] == null 
                            && $item['id_surat_keluar'] == null && $item['type'] != 'tembusan' && !$item['is_archive'];
                })->count();
        
        $totalDisposisi = $data->filter(function ($item, $key) {
                    return $item['created_by'] != null && $item['checked_at'] != null && $item['status'] != null 
                            && $item['type'] != 'tembusan' && !$item['is_archive'];
                })->count();
        
        $totalNonDisposisi = $data->filter(function ($item, $key) {
                    return $item['created_by'] == null && $item['checked_at'] == null && $item['status'] == null 
                            && $item['type'] != 'tembusan' && !$item['is_archive'];
                })->count();

        $totalTembusan = $data->filter(function ($item, $key) {
                    return $item['type'] == 'tembusan';
                })->count();

        $totalArsip = $data->filter(function ($item, $key) {
                    return $item['type'] != 'tembusan' && $item['is_archive'];
                })->count();
        
        return compact('totalManual', 'totalDisposisi', 'totalNonDisposisi', 'totalTembusan', 'totalArsip');
    }
}

?>