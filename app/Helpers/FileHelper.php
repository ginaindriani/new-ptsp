<?php

namespace App\Helpers;
use DB;
use File;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileHelper {

    public static function uploadFile(UploadedFile $file, $path, $prefixname)
    {
        $filename = "";
        try {
            $filename = $prefixname.'_'.Carbon::now()->timestamp.uniqid().'.'.$file->getClientOriginalExtension();
            Storage::disk('public')->put($path.$filename, File::get($file));
        }
        catch (\Exception $ex) {
        }
        return $filename;
    }


    public static function uploadFileBase64($file, $path, $prefixname)
    {
        $filename = null;
        try {
            $extension = explode('/', mime_content_type($file))[1] ?? 'png';
            if (preg_match('/^data:image\/(\w+);base64,/', $file)) {
                $fileDecode = substr($file, strpos($file, ',') + 1);
                $fileDecode = base64_decode($fileDecode);
                if ($fileDecode) {
                    $filename = $prefixname.'_'.Carbon::now()->timestamp.uniqid().'.'.$extension;
					//Upload to Disk Folder
                    //Storage::disk('public')->put($path.$filename, $fileDecode);
					
					//Upload To Another Server
					Storage::disk('sftp')->put($path.$filename, $fileDecode);
                }
            }
            return $filename;
        }
        catch (\Exception $ex) {
            return $filename;
        }
    }
    
    public static function deleteFile($path)
    {
		//Delete from Disk Folder
        //Storage::disk('public')->delete($path);
		
		//Delete from Disk sftp
        Storage::disk('sftp')->delete($path);
    }

    public static function createFile($content, $mime, $filename)
    {
        $response = Response::make($content, 200);
        $response->withHeaders("Content-Type", $mime);
        $response->withHeaders("Content-Disposition", 'attachment; filename='.$filename);

        return $response;
    }
}

?>