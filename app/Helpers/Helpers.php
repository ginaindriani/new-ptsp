<?php

use App\Models\LayananCounter;
use App\Models\SuratKeluar;
use Illuminate\Support\Str;
use App\Models\SequenceData;
use Illuminate\Support\Facades\DB;

function getCounter($idLayanan) {
	$layananCounter = LayananCounter::where('id_layanan', $idLayanan)
		->first();

	if (!$layananCounter) {
		$layananCounter = new LayananCounter();
	}
	$layananCounter->id_layanan = $idLayanan;
	$layananCounter->counter += 1;
	$layananCounter->save();

	return $layananCounter->counter;
}

function getSuratPermohonan($counter, $idLayanan, $kode_satker) {
	// 450-IX/15/21
	$romawiNumber = [1 => "I", 2 => 'II' ,3 => 'III', 4 => 'IV', 5 => 'V', 6 => 'VI', 7 => 'VII', 8 => 'VIII', 9 => 'IX', 10 => 'X', 11 => 'XI', 12 => 'XII'];
	return 'B-' . ($counter < 10 ? '0' . $counter : $counter) . '/' . $idLayanan . '/PTSP/' .$kode_satker.'/'. $romawiNumber[(int) date('m')] . '/' . date('y');
}

function tglIndo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);

	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function textareaToArray($value){
    return preg_split('/\r\n|[\r\n]/', $value);
}

function hariIndonesia($tanggal){
	$date = date ("D", strtotime($tanggal));

	switch($date){
		case 'Sun':
			$hari = "Minggu";
		break;

		case 'Mon':
			$hari = "Senin";
		break;

		case 'Tue':
			$hari = "Selasa";
		break;

		case 'Wed':
			$hari = "Rabu";
		break;

		case 'Thu':
			$hari = "Kamis";
		break;

		case 'Fri':
			$hari = "Jumat";
		break;

		case 'Sat':
			$hari = "Sabtu";
		break;

		default:
			$hari = "Tidak di ketahui";
		break;
	}

	return $hari;

}

function bulanIndonesia($tanggal){
    $month = date ("m", strtotime($tanggal));

    $bulan = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',
    );

	return $bulan[$month];

}

function optionJenisPengaduan() {
    return [
        "Pengaduan Tentang Pegawai"   => "Pengaduan Tentang Pegawai",
        "Pengaduan Tentang Perkara"    => "Pengaduan Tentang Perkara",
        "Pengaduan Lain-lain"    => "Pengaduan Lain-lain"
    ];
}

function optionAsal() {
    return [
        "Organisasi"   => "Organisasi",
        "Pemerintah"    => "Pemerintah",
        "BUMN"    => "BUMN",
        "Masyarakat"    => "Masyarakat"
    ];
}

function optionPps() {
    return [
        "Proyek Strategis Nasional (PSN)"   => "Proyek Strategis Nasional (PSN)",
        "Proyek Strategis Daerah (PSD)"    => "Proyek Strategis Daerah (PSD)",
        "Proyek Prioritas Kementerian"    => "Proyek Prioritas Kementerian",
        "Proyek Prioritas Lembaga"    => "Proyek Prioritas Lembaga",
		"Proyek Prioritas BUMN"    => "Proyek Prioritas BUMN",
		"Proyek Prioritas BUMD"    => "Proyek Prioritas BUMD",
    ];
}

function optionJenisLayanan($idPelayanan) {
	if ($idPelayanan == 6) {
		$datas = [
			"Jaksa Masuk Rutan" => "Jaksa Masuk Rutan",
			"Jaksa Masuk Sekolah/Kampus" => "Jaksa Masuk Sekolah/Kampus",
			"Jaksa Masuk Pesantren" => "Jaksa Masuk Pesantren",
			"Jaksa Preneurship" => "Jaksa Preneurship"
		];
	} else if ($idPelayanan == 7) {
		$datas = [
			"Legal Opinion" => "Legal Opinion",
			"Legal Assistance" => "Legal Assistance",
			"Legal Audit" => "Legal Audit"
		];
	} else {
		$datas = [
			"Pidum"   => "Pidum",
			"Pidsus"    => "Pidsus",
			"Pidsus"    => "Pidsus",
			"Datun"    => "Datun",
			"Militer"    => "Militer"
		];
	}

	return $datas;
}

function optionJenisBidang($idPelayanan)
{
    if($idPelayanan == 2) {
        return [
            "PIDUM"     => "PIDUM"
        ];
    } else if($idPelayanan == 23 || $idPelayanan == 25 || $idPelayanan == 6) {
        return [
            "INTEL"     => "INTEL"
        ];
    } else if($idPelayanan == 7 || $idPelayanan == 8) {
        return [
            "DATUN"     => "DATUN"
        ];
    } else if($idPelayanan == 10 || $idPelayanan == 11) {
        return [
            "PENGAWASAN" => "PENGAWASAN"
        ];
    } else if($idPelayanan == 12) {
        return [
            "PEMBINAAN" => "PEMBINAAN"
        ];
    } else {
        return [
            "PIDUM"     => "PIDUM",
            "PIDSUS"    => "PIDSUS"
        ];
    }
}

function optionSifatSurat() {
    return [
        "SR"   => "Sangat Rahasia",
        "R"    => "Rahasia",
        "T"    => "Terbatas",
		"B"    => "Biasa",
		"KEP"  => "Keputusan",
		"PRIN" => "Perintah"
    ];
}

function htmlListToArray($html) {
    $doc = new DOMDocument();
    $doc->loadHTML($html);
    $liList = $doc->getElementsByTagName('li');
    $liValues = array();
    foreach ($liList as $li) {
        $liValues[] = $li->nodeValue;
    }

    return $liValues;
}

function getSeqMasuk($sifat, $id_bidang = null, $id_jabatan = null) {
	$seq = SequenceData::where('tipe', 2)
			->where('id_satker', auth()->user()->organisasi->satker->id_satker)
			->when($id_bidang, function ($query, $id_bidang) {
				return $query->where('id_bidang', $id_bidang);
			})
			->when($id_jabatan, function ($query, $id_jabatan) {
				return $query->where('id_jabatan', $id_jabatan);
			})
			->where('sifat_surat', $sifat)
			->where('tahun', date('Y'))
			->first();

	if ($seq == null) {
		$seq = SequenceData::create([
				'id_satker' => auth()->user()->organisasi->satker->id_satker,
				'id_bidang' => $id_bidang,
				'id_jabatan' => $id_jabatan,
				'sifat_surat' => $sifat,
				'tipe' => 2,
				'tahun' => date('Y'),
				'sequence_increment' => 1,
				'sequence_min_value' => 1,
				'sequence_max_value' => 999999999999,
				'sequence_cur_value' => 1,
				'sequence_cycle' => 0,
			]);
	}

	return $seq;
}

function getSeqKeluar($jenis_surat, $bidang_main = null) {

	$seq = SuratKeluar::join('organisasi', 'organisasi.id_organisasi', '=', 'surat_keluar.id_organisasi')
				->where('organisasi.id_satker', auth()->user()->organisasi->id_satker)
				->when($bidang_main, function ($query, $bidang_main) {
					return $query->where('surat_keluar.id_bidang_main', $bidang_main);
				})
				->whereYear('surat_keluar.created_at', date('Y'))
				->where('jenis_surat', $jenis_surat)
				->max('seq');

	return $seq + 1;


	// $seq = SequenceData::where('tipe', 1)
	// 		->where('id_satker', auth()->user()->organisasi->satker->id_satker)
	// 		->when($id_bidang, function ($query, $id_bidang) {
	// 			return $query->where('id_bidang', $id_bidang);
	// 		})
	// 		->when($id_jabatan, function ($query, $id_jabatan) {
	// 			return $query->where('id_jabatan', $id_jabatan);
	// 		})
	// 		->where('sifat_surat', $content['sifat'])
	// 		->where('tahun', date('Y', strtotime($content['tanggal'])))
	// 		->first();

	// if ($seq == null) {
	// 	$seq = SequenceData::create([
	// 			'id_satker' => auth()->user()->organisasi->satker->id_satker,
	// 			'id_bidang' => $id_bidang,
	// 			'id_jabatan' => $id_jabatan,
	// 			'sifat_surat' => $content['sifat'],
	// 			'tipe' => 1,
	// 			'tahun' => date('Y', strtotime($content['tanggal'])),
	// 			'sequence_increment' => 1,
	// 			'sequence_min_value' => 1,
	// 			'sequence_max_value' => 999999999999,
	// 			'sequence_cur_value' => 1,
	// 			'sequence_cycle' => 0,
	// 		]);
	// }

	// return $seq;
}

function seqNo($seq) {
	$sequense = "00" . $seq;
	if (strlen($sequense) <= 6) {
		$sequense = substr($sequense, -2);
	} else {
		$sequense = substr($sequense, -strlen($seq));
	}

	return $sequense;
}

function buildNomor($content, $seq, $penandatangan = null) {
	if (array_key_exists('nomor', $content)) {
		if (array_key_exists('sifat', $content) && array_key_exists('kode_masalah', $content)) {
			return $content['sifat'].'-'.seqNo($seq).'/'.$penandatangan->kode_pejabat.'/'.$content['kode_masalah'].'/'.date('m', strtotime($content['tanggal'])).'/'.date('Y', strtotime($content['tanggal']));
		} else {
			return seqNo($seq);
		}
	}
}
