<?php

$master = array(
    'title'      => 'Master',
    'icon'       => array(
        'svg'  => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com006.svg", "svg-icon-2"),
        'font' => '<i class="bi bi-person fs-2"></i>',
    ),
    'classes'    => array('item' => 'menu-accordion'),
    'attributes' => array(
        "data-kt-menu-trigger" => "click",
    ),
    'sub'        => array(
        'class' => 'menu-sub-accordion menu-active-bg',
        'items' => array(
            array(
                'title'  => 'Master Layanan',
                'path'   => 'master/master_layanan',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title'  => 'Data Satker',
                'path'   => 'master/satker',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title'  => 'Data Bidang',
                'path'   => 'master/bidang',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title'  => 'Data Jabatan',
                'path'   => 'master/jabatan',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title'  => 'Relasi Organisasi',
                'path'   => 'master/relasiorganisasi',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title'  => 'Slider',
                'path'   => 'master/slider',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),

        ),
    ),
);

$pengaturan = array(
    'title' => 'Pengaturan',
    'icon' => array(
        'svg' => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com006.svg", "svg-icon-2"),
        'font' => '<i class="bi bi-person fs-2"></i>',
    ),
    'classes' => array('item' => 'menu-accordion'),
    'attributes' => array(
        "data-kt-menu-trigger" => "click",
    ),
    'sub' => array(
        'class' => 'menu-sub-accordion menu-active-bg',
        'items' => array(
            array(
                'title' => 'Manajemen Akun',
                'path' => 'manajemen/user',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
        ),
    ),
);

$layanan = array(
    'title' => 'Data Layanan',
    'icon' => array(
        'svg' => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com006.svg", "svg-icon-2"),
        'font' => '<i class="bi bi-person fs-2"></i>',
    ),
    'classes' => array('item' => 'menu-accordion'),
    'attributes' => array(
        "data-kt-menu-trigger" => "click",
    ),
    'sub' => array(
        'class' => 'menu-sub-accordion menu-active-bg',
        'items' => array(
            array(
                'title' => 'Permintaan Hari Ini',
                'path'  => '/layanan/permohonan/hari-ini',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Permintaan Belum Diverifikasi',
                'path'  => '/layanan/permohonan/belum-diverifikasi',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Daftar Semua Permintaan',
                'path'  => '/layanan/permohonan/daftar',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Menu Layanan',
                'path'  => '/backoffice/layanan',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),


            
        ),
        
    ),
);
$pengaduan = array(
    'title' => 'Data Pengaduan',
    'icon' => array(
        'svg' => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com006.svg", "svg-icon-2"),
        'font' => '<i class="bi bi-person fs-2"></i>',
    ),
    'classes' => array('item' => 'menu-accordion'),
    'attributes' => array(
        "data-kt-menu-trigger" => "click",
    ),
    'sub' => array(
        'class' => 'menu-sub-accordion menu-active-bg',
        'items' => array(
            array(
                'title' => 'Pengaduan Hari Ini',
                'path'  => '/layanan/permohonan/pengaduan/hari-ini',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Pengaduan Belum Diverifikasi',
                'path'  => '/layanan/permohonan/pengaduan/belum-diverifikasi',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Daftar Semua Pengaduan',
                'path'  => '/layanan/permohonan/pengaduan/daftar',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Menu Pengaduan',
                'path'  => '/backoffice/pengaduan',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),


            
        ),
        
    ),
);

$informasi = array(
    'title' => 'Data Informasi',
    'icon' => array(
        'svg' => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com006.svg", "svg-icon-2"),
        'font' => '<i class="bi bi-person fs-2"></i>',
    ),
    'classes' => array('item' => 'menu-accordion'),
    'attributes' => array(
        "data-kt-menu-trigger" => "click",
    ),
    'sub' => array(
        'class' => 'menu-sub-accordion menu-active-bg',
        'items' => array(
            array(
                'title' => 'Berita',
                'path'  => 'master/berita',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Kegiatan',
                'path'  => 'master/kegiatan',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Pengumuman',
                'path'  => 'master/pengumuman',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'DPO',
                'path'  => 'master/dpo',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Lelang',
                'path'  => 'master/lelang',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            array(
                'title' => 'Jadwal Sidang',
                'path'  => 'master/jadwalsidang',
                'bullet' => '<span class="bullet bullet-dot"></span>',
            ),
            


            
        ),
        
    ),
);


$ruangDiskusi = array(
    'title' => 'Ruang Diskusi',
    'path'  => 'ruang-diskusi',
    'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com012.svg", "svg-icon-2"),
);

$dataTamu = array(
    'title' => 'Data Tamu',
    'path'  => 'master/datatamu',
    'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com012.svg", "svg-icon-2"),
);

$Tilang = array(
    'title' => 'Data Tilang',
    'path'  => 'master/tilang',
    'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com012.svg", "svg-icon-2"),
);


$datas = array(
    'main' => array(
        array(
            'title' => 'Dashboard',
            'path'  => '/backoffice/dashboard',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/art/art002.svg", "svg-icon-2"),
        ),
        array(
            'classes' => array('content' => 'pt-8 pb-2'),
            'path' => 'modules',
            'content' => '<span class="menu-section text-muted text-uppercase fs-8 ls-1">Modules</span>',
        ),
        array(
            'title' => 'Pencarian Data',
            'path'  => 'dashboard',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/art/art002.svg", "svg-icon-2"),
        ),
        // array(
        //     'title' => 'Big Data Kejaksaan',
        //     'path'  => '/dashboard/big-data',
        //     'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/art/art002.svg", "svg-icon-2"),
        // ),
    )
);
$datas['main'][] = $layanan;
$datas['main'][] = $pengaduan;
$datas['main'][] = $informasi;
$datas['main'][] = $master;
$datas['main'][] = $pengaturan;
$datas['main'][] = $ruangDiskusi;
$datas['main'][] = $dataTamu;
$datas['main'][] = $Tilang;


$datas['main'][] = array(
    'path' => 'separator',
    'content' => '<div class="separator mx-1 my-4"></div>',
);

return $datas;
