<?php

return [
    /*
     * KEJAGUNG API
     * */
    'kejagung_api'    => env('KEJAGUNG_API', 'http://infocovid19.kejaksaan.go.id/api/kejaksaan/pegawai'),
    'kejagung_token'    => env('KEJAGUNG_TOKEN', '9623A7E5376164AC8B5388775AA49E2170E165A54DA702FF726DF5334DF1EDD6'),
    
    /*
     * BSRE API
     * */
    'bsre_api'    => env('BSRE_API', 'http://103.147.3.229:3000'),
    'bsre_client_id' => env('BSRE_CLIENT_ID', 'admin'),
    'bsre_client_secret' => env('BSRE_CLIENT_SECRET', 'qwerty'),
	
	/*
     * MEDIA SERVER
     * */
    'host_media'    => env('FTP_HOST', '103.147.3.229'),
];
