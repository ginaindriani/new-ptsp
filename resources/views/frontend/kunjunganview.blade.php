@extends('layout.frontend.index')  
@push('plugins-css')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<style type="text/css"></style>
 @endpush 
 @section('content')
 <div>
	<div class="registration-form-section">
	<div class="section-title reg-header animated fadeInDown" data-animation="fadeInDown">
		<div class="row">
			<div class="col-md-3">
				<center>
					<br/><br/>
					<a id="button_download_pdf"  data-unique="{{ $data_personal->unique_id }}" class="btn btn-primary btn-lg btn-block">PRINT GATE PASS</a>
				</center>
			</div>
			<div class="col-md-6">
				<center>
					<h3>BUKU TAMU</h3>
					<h5>{{ $data_personal->nama_satker }}</h5>
					<h4 style="font-family: Arial, Helvetica, sans-serif;">
						<strong>Selamat Datang {{ $data_personal->nama_tamu }}</strong>
					</h4>
					
				</center>
			</div>
			<div class="col-md-3">
				<br/><br/>
					<a id="print_kunjungan" onclick="PrintQR('print-container','e-bukutamu')"  data-unique="{{ $data_personal->unique_id }}" class="btn btn-primary btn-lg btn-block">PRINT QR</a>
			</div>
			
		</div>
		<hr />
		<center>
		<strong><h4>SIMPAN QR CODE DIBAWAH INI UNTUK MELAKUKAN CHECKIN DAN CHECKOUT</h4></strong>
		<img src="{{ asset($data_personal->qr_image)}}">
		<p>Valid sampai dengan</p>
			{{ day_idn(date('w', strtotime($data_personal->valid_sampai))) }},
			{{ date('d', strtotime($data_personal->valid_sampai)) }}
			{{ month_idn(date('n', strtotime($data_personal->valid_sampai))) }}
			{{ date('Y', strtotime($data_personal->valid_sampai)) }}
			{{ date('h:i a', strtotime($data_personal->valid_sampai)) }}
			<br/>
		<strong>Nomor Antrian Anda : {{ $data_personal->nomor_antrian }}</strong>
			<hr />
		</center>
		<div class="row">
			<center><strong><h4>DATA KUNJUNGAN</h4></strong></center>
			<div class="col-md-6 table-responsive">
				<table class="data-kunjungan data-kunjungan-date" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
					<tr>
						<th>
							<strong>No Identitas Anda</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->ktp_tamu }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Nama Anda</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->nama_tamu }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Jenis Kelamin</strong>
						</th>
						<td>  :  </td>
						<td>
							@if($data_personal->jenis_kelamin == 1)
								Laki-Laki
							@elseif($data_personal->jenis_kelamin == 2)
								Perempuan
							@else
								-
							@endif
						</td>
					</tr>

					<tr>
						<th>
							<strong>Plat Kendaraan Anda</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->plat_tamu }}
						</td>
					</tr>

					<tr>
						<th>
							<strong>No HP / Whatsapp Anda</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->no_hp_tamu }}
						</td>
					</tr>

					<tr>
						<th>
							<strong>Email Anda</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->email_tamu }}
						</td>
					</tr>

					<tr>
						<th>
							<strong>Alamat Anda</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->alamat_tamu }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Jumlah Tamu</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->jumlah_tamu }} Tamu
						</td>
					</tr>
					<tr>
						<th>
							<strong>Datang Sebagai</strong>
						</th>
						<td>  :  </td>
						<td>
							@if ($data_personal->tipe_tamu == 1)
							Tamu Biasa
							@elseif ($data_personal->tipe_tamu == 2)
							Saksi
							@elseif ($data_personal->tipe_tamu == 3)
							Tersangka/Terdakwa
							@elseif ($data_personal->tipe_tamu == 4)
							Ahli
							@elseif ($data_personal->tipe_tamu == 5)
							Tamu VVIP
							@else
							Tamu Biasa
							@endif
						</td>
					</tr>

					
				</table>
			</div>
			<div class="col-md-6 table-responsive">
				<table class="data-kunjungan data-kunjungan-date" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
					<tr>
						<th>
							<strong>Nama Tujuan</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->nama_tujuan }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Jabatan Tujuan</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->jabatan_tujuan }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Tanggal Berkunjung</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ day_idn(date('w', strtotime($data_personal->tanggal_berkunjung))) }},
							{{ date('d', strtotime($data_personal->tanggal_berkunjung)) }}
							{{ month_idn(date('n', strtotime($data_personal->tanggal_berkunjung))) }}
							{{ date('Y', strtotime($data_personal->tanggal_berkunjung)) }}
							{{ date('h:i a', strtotime($data_personal->tanggal_berkunjung)) }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Jam Masuk</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->jam_masuk != null ? date('h:i a', strtotime($data_personal->jam_masuk)) : '00:00' }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Jam Keluar</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_personal->jam_keluar != null ? date('h:i a', strtotime($data_personal->jam_keluar)) : '00:00' }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Durasi Kedatangan</strong>
						</th>
						<td>  :  </td>
						<td>
							@if ($data_personal->jam_masuk != null && $data_personal->jam_keluar != null)
								{{ durasi_kunjungan($data_personal->jam_masuk, $data_personal->jam_keluar) }}
							@else
								{{ '0 menit' }}
							@endif
						</td>
					</tr>
					
				</table>
			</div>
		</div>
		<div>
			<hr />
			<center><strong><h4>DATA PENGIKUT</h4></strong></center>
			<div class="table-responsive">
				<table id="" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
					<thead>
						<tr>
							<th width="10%">No</th>
							<th width="40%">Nama Pengikut</th>
							<th width="30%">Jenis Kelamin</th>
							<th width="20%">Kategori</th>
						</tr>
					</thead>
					<tbody>
						@forelse ($pengikut as $key => $pengikut )
						<tr>
							<td>{{ $key + 1 }}</td>
							<td>{{ $pengikut->nama_pengikut }}</td>
							<td>
								@if($pengikut->jenis_kelamin == 1)
									Laki-Laki
								@else
									Perempuan
								@endif
							</td>
							<td>
								@if($pengikut->is_anak == 1)
								Anak-anak
							@else
								Dewasa
							@endif</td>
						</tr>
						@empty

						@endforelse
					</tbody>
				</table>
			</div>
		</div>
		<div>
			<hr />
			<center><strong><h4>RIWAYAT KUNJUNGAN</h4></strong></center>
			<div class="table-responsive">
				<table id="" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
					<thead>
						<tr>
							<th width="20%">Tanggal Kunjungan</th>
							<th width="20%">Satuan Kerja</th>
							<th width="20%">Bertemu dengan</th>
							<th width="15%">Keterangan</th>
							<th width="15%">Durasi Kunjungan</th>
							<th width="10%">Aksi</th>
						</tr>
					</thead>
					<tbody>
						@forelse ($riwayat_kunjungan as $key => $riwayat_kunjungan )
						<tr>
							<td>{{ day_idn(date('w', strtotime($data_personal->tanggal_berkunjung))) }},
								{{ date('d', strtotime($data_personal->tanggal_berkunjung)) }}
								{{ month_idn(date('n', strtotime($data_personal->tanggal_berkunjung))) }}
								{{ date('Y', strtotime($data_personal->tanggal_berkunjung)) }}
								{{ date('h:i a', strtotime($data_personal->tanggal_berkunjung)) }}</td>
							<td>{{ $riwayat_kunjungan->nama_satker }}</td>
							<td>{{ $riwayat_kunjungan->nama_tujuan }}</td>
							<td>{{ $riwayat_kunjungan->keterangan_tujuan }}</td>
							<td>
								@if ($riwayat_kunjungan->jam_masuk != null && $riwayat_kunjungan->jam_keluar != null)
									{{ durasi_kunjungan($riwayat_kunjungan->jam_masuk, $riwayat_kunjungan->jam_keluar) }}
								@else
									{{ '0 menit' }}
								@endif
							</td>
							<td><a href="javascript:;" data-id="{{$riwayat_kunjungan->kunjungan_id}}" class="btn btn-default view-survey">
								Lihat Survey
							</a></td>
						</tr>
						@empty

						@endforelse
					</tbody>
				</table>
			</div>
		</div>
		<div class="registration-form-action clearfix animated fadeInLeftBig" data-animation="fadeInLeftBig" data-animation-delay=".15s" style="animation-delay: 0.15s;">
			<input type="file" id="file_ktp" style="display: none;" />
			<div class="pull-left">
			  <a href = "{{ url('/ptsp',$data_personal->slug_satker) }}" type="button" id="loads" class="btn btn-danger"><i class="fa fa-chevron-left"> KEMBALI KE MENU UTAMA</i></a>
			</div>
		</div>
		
		


	</div>
	<div class="clearfix">
		<div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
		</div>
	</div>
	</div>
	<div class="modal fade" id="view-survey" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Data Survey</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body" id="modal-body">
				<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
					<thead>
						<tr>
							<th width="70%">Pertanyaan</th>
							<th width="30%">Jawaban</th>
						</tr>
					</thead>
						
					<tbody id="data-survey">
					</tbody>
					
				</table>
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
			</div>
		</div>
		</div>
	</div>
</div>

<div id="print-container" style="display:none;">
	<div style="width:80mm;">
		<p style="text-align:center;">
			Selamat datang di
			<br />
			{{ ($data_personal->nama_satker) }}
		</p>

		<hr />

		<p style="text-align:center;"><strong>QR CODE</strong></p>

		<?php
			$qr_image = asset($data_personal->qr_image);
			
		?>

		<p style="text-align:center;">
			<img src="{{ $qr_image }}" style="width:100%;" />
		</p>

		<p style="text-align:center;">
			@if ( !empty($qr_image) )
				<strong style="font-size:16px;">Valid sampai dengan:</strong>
				<br />
				{{ day_idn(date('w', strtotime($data_personal->valid_sampai))) }},
				{{ date('d', strtotime($data_personal->valid_sampai)) }}
				{{ month_idn(date('n', strtotime($data_personal->valid_sampai))) }}
				{{ date('Y', strtotime($data_personal->valid_sampai)) }}
				{{ date('h:i a', strtotime($data_personal->valid_sampai)) }}
			@else
				Maaf, QR Code Anda <strong style="font-size:16px;">tidak valid</strong>.
				<br />
				Silahkan hubungi customer service kami di lobby.
			@endif
		</p>

		{{-- <hr /> --}}

		<p style="text-align:center;"><strong>
			NO ANTRIAN :
			<br />
			<span style="font-size: 16px;">{{ $data_personal->nomor_antrian }}</span>
		</strong></p>
	</div>
</div>
@endsection 
@push('modal') 
@endpush 
@push('plugins-js')
<script type="text/javascript">

function PrintQR(divid,title) {
        $('#print_kunjungan').html('Loading...');
        var contents = document.getElementById(divid).innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame1";
        // frame1.style.position = "absolute";
        // frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write(`<html><head><title>${title}</title>`);
        frameDoc.document.write('<style>body { margin: 0px auto; background-color: #ffffff; }</style>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            document.body.removeChild(frame1);
            $('#print_kunjungan').html('Cetak QR Code');
        }, 500);
        setTimeout(function () {
            window.location = baseUrl;
        }, 600);
        return false;
    }
// var base_url = window.location.origin + "/qr/reader"
var direct_url = "{{ url('ptsp', $data_personal->slug) }}";
$(document).ready(function () {
    // Handler for .ready() called.
    window.setTimeout(function () {
        location.href = direct_url;
    }, 30000);
});


$('.view-survey').on("click", function(event) {
	document.getElementById("loading");
    
    var survey_id = $(this).data('id');

	const xhttp = new XMLHttpRequest();
	xhttp.open("GET", "/kunjungan/survey/" + survey_id);
	xhttp.send();
	xhttp.onreadystatechange = function() {
		if (xhttp.status == 200) {
			var trHTML = ''; 
			const objects = JSON.parse(this.responseText)
			for (let object of objects) {
				trHTML += '<tr>';
				trHTML += '<td>'+object['question']+'</td>';
				trHTML += '<td>'+object['response']+'</td>';
				trHTML += "</tr>";
			}
			document.getElementById("data-survey").innerHTML = trHTML;
			$("#view-survey").modal('show');
		}else {
			Swal.fire({
				type: 'error',
				title: '<i>Survey Tidak dilakukan oleh tamu ini pada saat kunjungan</i>'
			})
		}
	}
});

var url_download = "{{ url('/kunjungan/pdf', $data_personal->unique_id)}}"
var url_awal = "{{ url('/kunjungan', $data_personal->unique_id)}}"

$('#button_download_pdf').on('click', function (e) {
        $(this).html('Downloading...');
        $.get(url_download, function (data) {
            window.location = url_download;
			console.log(url_download, 'disini cuyy')
            $('#button_download_pdf').html('Download PDF');
        });
        window.location = url_awal;
    });

</script>
@endpush

