@extends('layout.frontend.index')
@push('plugins-css')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<style type="text/css">
 #my_camera{
     width: 320px;
     height: 240px;
     border: 1px solid black;
}

.d-none {
  display: none !important;
}


</style>
<link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2/dist/sweetalert2.min.css') }}">
 @endpush 
 @section('content')
<div class="registration-form-section">
  <div class="section-title reg-header animated fadeInDown" data-animation="fadeInDown">
    <center>
      <h3>BUKU TAMU</h3>
    </center>
    <center>
      <h5>{{ $satker->nama_satker }}</h5>
    </center>
    <hr />
    <center>
      <h4 style="font-family: Arial, Helvetica, sans-serif;">
        <strong>Form Pendaftaran Pengambilan Tilang {{ $satker->nama_satker }} </strong>
      </h4>
      <button id="cameraFlip" class="btn btn-success">Ganti Kamera</button>
      <form action="{{ route('simpantamutilang') }}" method="post" id="formsimpan">
        @csrf 
        <input name="id_satker" value="{{ $satker->id_satker }}" hidden />
        <div class="form-group">
          <span>Ambil Foto Tamu </span>
          <label style="color: red;">*</label>
          <div id="bx-cam">
            <video id="webcam" autoplay playsinline></video>
            <canvas id="canvas" class="d-none"></canvas>
            <div id="flash-cam" class="flash-cam"></div>
            <input type="hidden" name="photo" id="download-photo" required/>
          </div>
          <div id="bx-cam-icon">
            <div class="row-icon">
              <a id="take-photo" class="cam-icon">
              <i class="fa fa-camera"></i>
              </a>
              <a id="retake-photo" class="d-none cam-icon">
              <i class="fa fa-repeat"></i>
              </a>
            </div>
          </div>
        </div>
    </center>
        <div class="form-group">
            <span>Nama Tamu </span>
            <label style="color: red;">*</label>
            <input type="text"  class="form-control" name="nama_tamu" placeholder="Masukan Nama Tamu" value="" required />
        </div>
        <div class="form-group">
          <span>Alamat Tamu </span>
          <label style="color: red;">*</label>
          <textarea placeholder="Masukan alamat Anda" name="alamat" class="form-control" rows="3" id="alamat" required></textarea>
        </div>
        <div class="form-group">
            <span>Masukkan Nomor Berkas Tilang</span>
            <label style="color: red;">*</label>
            <input type="text" class="form-control" name="nomor_berkas" placeholder="Masukan No Berkas Tilang" value="" required />
        </div>
        <input type="checkbox" id="checkme"/> Dengan ini, saya setuju untuk memberikan data
        <div class="registration-form-action clearfix animated fadeInLeftBig" data-animation="fadeInLeftBig" data-animation-delay=".15s" style="animation-delay: 0.15s;">
          
          <div class="pull-left">
            <a href = {{ url($satker->slug) }} type="button" id="loads" class="btn btn-danger"><i class="fa fa-chevron-left"> KEMBALI KE MENU AKSI</i></a>
          </div>
          <div class="pull-right">
            <button type="submit" onclick="simpandata()" id="tombol"name="tombol"  class="btn btn-success">Simpan <i class="fa fa-chevron-right"></i></button>
          </div>
        </div>
        
      </form>
    
  </div>
  <div class="clearfix">
    <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
      <div class="reg-content">
        <div class="registration-form-action clearfix link1 animated fadeInRightBig" data-animation="fadeInRightBig" data-animation-delay=".2s" style="animation-delay: 0.2s;">
          <div class="row text-center">
            <span>
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script>
              &copy; KEJAKSAAN AGUNG RI
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection 
@push('modal') 
@endpush 
@push('plugins-js')
<script src="{{ asset('assets/backend/plugins/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script type="text/javascript">
var checker = document.getElementById('checkme');
  var sendbtn = document.getElementById('tombol');
  sendbtn.disabled = true;
  // when unchecked or checked, run the function
  checker.onchange = function(){
  if(this.checked){
      sendbtn.disabled = false;
  } else {
      sendbtn.disabled = true;
  }}

  function simpandata() {
    sendbtn.disabled = true;
    $('#tombol').html('Data sedang disimpan Mohon Tunggu');
    document.getElementById("formsimpan").submit();
  }
  
$(document).ready(function() {
  $.ajaxSetup({
    headers: {
      'csrftoken': '{{ csrf_token() }}'
    }
  });
  const webcamElement = document.getElementById('webcam');
  const canvasElement = document.getElementById('canvas');
  const snapSoundElement = document.getElementById('snapSound');
  const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
  $('#cameraFlip').click(function() {
    webcam.flip();
    webcam.start();  
  });
  webcam.start()
    .then(result =>{
        console.log("webcam started");
    })
    .catch(err => {
        console.log(err);
    }
  );

  $("#take-photo").click(function () {
    beforeTakePhoto();
    let picture = webcam.snap();
    $("#download-photo").val(picture);
    Swal.fire({
      type: 'success',
      title: 'Berhasil mengambil foto!',
      html: '<em>'+'Lanjutkan dengan klik Simpan'+'</em>',
      showCancelButton: false,
      showConfirmButton: true
    });
    // document.querySelector('#download-photo').href = picture;
    afterTakePhoto();
  });

  $("#retake-photo").click(function () {
    webcam.stream()
        .then(facingMode =>{
            removeCapture();
        }
    );
  });

  function beforeTakePhoto(){
    $('#flash-cam')
        .show()
        .animate({opacity: 0.3}, 500)
        .fadeOut(500)
        .css({'opacity': 0.7});
    // window.scrollTo(0, 0);
    $('#webcam-control').addClass('d-none');
    $('#cameraControls').addClass('d-none');
  };

  function afterTakePhoto(){
    webcam.stop();
    $('#canvas').removeClass('d-none');
    $('#take-photo').addClass('d-none');
    $('#retake-photo').removeClass('d-none');
    // $('#download-photo').removeClass('d-none');
    // $('#resume-camera').removeClass('d-none');
    // $('#cameraControls').removeClass('d-none');
  }

  function removeCapture(){
    $('#canvas').addClass('d-none');
    $('#take-photo').removeClass('d-none');
    $('#retake-photo').addClass('d-none');
  }

  const webcamElementKendaraan = document.getElementById('webcamkendaraan');
  const canvasElementKendaraan = document.getElementById('canvaskendaraan');
  const snapSoundElementKendaraan = document.getElementById('snapSound');
  const webcamKendaraan = new Webcam(webcamElementKendaraan, 'user', canvasElementKendaraan, snapSoundElementKendaraan);
  webcamKendaraan.start().then(result => {
    console.log("webcam kendaraan started");
  }).catch(err => {
    console.log(err);
  });
  $("#take-photo-kendaraan").click(function() {
    beforeTakePhotoKendaraan();
    let pictureKendaraan = webcamKendaraan.snap();
    $("#download-photo-kendaraan").val(pictureKendaraan);
    Swal.fire({
      type: 'success',
      title: 'Sukses!',
      html: '<em>'+'Mantap'+'</em>',
      showCancelButton: false,
      showConfirmButton: true
    });
    // document.querySelector('#download-photo').href = pictureKendaraan;
    afterTakePhotoKendaraan();
  });
  $("#retake-photo-kendaraan").click(function() {
    webcamKendaraan.stream().then(facingMode => {
      removeCapture();
    });
  });

  function beforeTakePhotoKendaraan() {
    $('#flash-cam-kendaraan').show().animate({
      opacity: 0.3
    }, 500).fadeOut(500).css({
      'opacity': 0.7
    });
    // window.scrollTo(0, 0);
    // $('#webcam-control').addClass('d-none');
    // $('#cameraControls').addClass('d-none');
  };

  function afterTakePhotoKendaraan() {
    webcamKendaraan.stop();
    $('#canvaskendaraan').removeClass('d-none');
    $('#take-photo-kendaraan').addClass('d-none');
    $('#retake-photo-kendaraan').removeClass('d-none');
    // $('#download-photo').removeClass('d-none');
    // $('#resume-camera').removeClass('d-none');
    // $('#cameraControls').removeClass('d-none');
  }

  function removeCaptureKendaraan() {
    $('#canvaskendaraan').addClass('d-none');
    $('#take-photo-kendaraan').removeClass('d-none');
    $('#retake-photo-kendaraan').addClass('d-none');
  }

  
});

</script>
@endpush

