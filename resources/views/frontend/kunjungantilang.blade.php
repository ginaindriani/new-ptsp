@extends('layout.frontend.index')  
@push('plugins-css')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<style type="text/css"></style>
 @endpush 
 @section('content')
 <div>
	<div class="registration-form-section">
	<div class="section-title reg-header animated fadeInDown" data-animation="fadeInDown">
		<div class="row">
			<div class="col-md-3">
				<br/><br/>
					<a id="print_kunjungan" onclick="PrintQR('print-container','e-bukutamu')"  data-unique="{{ $data_personal->unique_id }}" class="btn btn-primary btn-lg btn-block">PRINT QR</a>
			</div>
			<div class="col-md-6">
				<center>
					<h3>BUKU TAMU</h3>
					<h5>{{ $data_personal->nama_satker }}</h5>
					<h4 style="font-family: Arial, Helvetica, sans-serif;">
						<strong>Selamat Datang {{ $data_personal->nama_tamu }}</strong>
					</h4>
				</center>
			</div>
			<div class="col-md-3">
				<br/><br/>
					<a href="https://tilang.kejaksaan.go.id/" class="btn btn-primary btn-lg btn-block">BAYAR ONLINE</a>
			</div>
			
		</div>
		<hr />
		<center>
		<strong><h4>SIMPAN QR CODE DIBAWAH INI UNTUK MELAKUKAN CHECKIN DAN CHECKOUT</h4></strong>
		<img src="{{ asset($data_personal->qr_image)}}">
		<p>Valid sampai dengan</p>
			{{ day_idn(date('w', strtotime($data_personal->created_at))) }},
			{{ date('d', strtotime($data_personal->created_at)) }}
			{{ month_idn(date('n', strtotime($data_personal->created_at))) }}
			{{ date('Y', strtotime($data_personal->created_at)) }}
			{{ date('h:i a', strtotime($data_personal->created_at)) }}
			<br/>
		<strong>Nomor Antrian Anda : {{ $data_personal->guest_no }}</strong>
			<hr />
		</center>
		<div class="row">
			<center><strong><h4>DATA TILANG</h4></strong></center>
			<hr />
			<div class="col-md-6 table-responsive">
				<table class="data-kunjungan data-kunjungan-date" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
					<tr>
						<th>
							<strong>Nama</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->nama }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Alamat</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->alamat }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>No Ranmor</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->no_ranmor }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Jenis Kendaraan</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->jenis_kendaraan }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>No Reg Tilang</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->no_reg_tilang }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Pasal</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->pasal }}
						</td>
					</tr>
				</table>
			</div>
			<div class="col-md-6 table-responsive">
				<table class="data-kunjungan data-kunjungan-date" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
					<tr>
						<th>
							<strong>Barang Bukti</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->bb }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Denda</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->denda }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Biaya Perkara</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->bp }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Uang Titipan</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->uang_titipan }}
						</td>
					</tr>
					<tr>
						<th>
							<strong>Subsider</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->subsider }}
						</td>
					</tr>
					
					<tr>
						<th>
							<strong>Petugas</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ $data_tilang->nama_petugas }}
						</td>
					</tr>
					{{-- <tr>
						<th>
							<strong>Tanggal Sidang</strong>
						</th>
						<td>  :  </td>
						<td>
							{{ day_idn(date('w', strtotime($data_tilang->tgl_sidang))) }},
							{{ date('d', strtotime($data_tilang->tgl_sidang)) }}
							{{ month_idn(date('n', strtotime($data_tilang->tgl_sidang))) }}
							{{ date('Y', strtotime($data_tilang->tgl_sidang)) }}
						</td>
					</tr> --}}
				</table>
			</div>
			&nbsp;&nbsp;
			<center><strong><h4>TOTAL BIAYA : {{ $data_tilang->denda + $data_tilang->bp + $data_tilang->uang_titipan }}</h4></strong></center>
		</div>
		<hr />
		<div class="row">
			<center><strong><h4>FOTO TAMU</h4></strong>
			<hr />
			<div class="col-md-12">
				<img src="{{ asset($data_personal->photo) }}" width="300" height="300">
			</div>
			</center>
		</div>

	</div>
</div>

<div id="print-container" style="display:none;">
	<div style="width:80mm;">
		<p style="text-align:center;">
			{{ ($data_personal->nama_satker) }}
		</p>

		<hr />

		<p style="text-align:center;"><strong>QR CODE</strong></p>

		<?php
			$qr_image = asset($data_personal->qr_image);
			
		?>

		<p style="text-align:center;">
			<img src="{{ $qr_image }}" style="width:100; height:100;" />
		</p>

		<p style="text-align:center;">
			@if ( !empty($qr_image) )
				<strong style="font-size:16px;">Valid sampai dengan:</strong>
				<br />
				{{ day_idn(date('w', strtotime($data_personal->created_at))) }},
				{{ date('d', strtotime($data_personal->created_at)) }}
				{{ month_idn(date('n', strtotime($data_personal->created_at))) }}
				{{ date('Y', strtotime($data_personal->created_at)) }}
				{{ date('h:i a', strtotime($data_personal->created_at)) }}
			@else
				Maaf, QR Code Anda <strong style="font-size:16px;">tidak valid</strong>.
				<br />
				Silahkan hubungi customer service kami di lobby.
			@endif
		</p>

		<p style="text-align:center;"><strong>
			NO ANTRIAN :
			<br />
			<span style="font-size: 16px;">{{ $data_personal->guest_no }}</span>
		</strong>
		</p>

		<p style="text-align:center;"><strong>
			TOTAL TAGIHAN :
			<br />
			<span style="font-size: 16px;">{{ $data_tilang->denda + $data_tilang->bp + $data_tilang->uang_titipan }}</span>
		</strong>
		</p>
		<div style="font-size: 4px">
			<table cellspacing="0" width="100%" style="width:100%">
				<tr>
					<th>
						Nama
					</th>
					<td>  :  </td>
					<td>
						{{ $data_tilang->nama }}
					</td>
				</tr>
				<tr>
					<th>
						No Ranmor
					</th>
					<td>  :  </td>
					<td>
						{{ $data_tilang->no_ranmor }}
					</td>
				</tr>
				<tr>
					<th>
						No Reg Tilang
					</th>
					<td>  :  </td>
					<td>
						{{ $data_tilang->no_reg_tilang }}
					</td>
				</tr>
				<tr>
					<th>
						Denda
					</th>
					<td>  :  </td>
					<td>
						{{ $data_tilang->denda }}
					</td>
				</tr>
				<tr>
					<th>
						Biaya Perkara
					</th>
					<td>  :  </td>
					<td>
						{{ $data_tilang->bp }}
					</td>
				</tr>
				<tr>
					<th>
						Uang Titipan
					</th>
					<td>  :  </td>
					<td>
						{{ $data_tilang->uang_titipan }}
					</td>
				</tr>
				<tr>
					<th>
						Subsider
					</th>
					<td>  :  </td>
					<td>
						{{ $data_tilang->subsider }}
					</td>
				</tr>
				
				<tr>
					<th>
						Petugas
					</th>
					<td>  :  </td>
					<td>
						{{ $data_tilang->nama_petugas }}
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection 
@push('modal') 
@endpush 
@push('plugins-js')
<script type="text/javascript">

function PrintQR(divid,title) {
        $('#print_kunjungan').html('Loading...');
        var contents = document.getElementById(divid).innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame1";
        // frame1.style.position = "absolute";
        // frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write(`<html><head><title>${title}</title>`);
        frameDoc.document.write('<style>body { margin: 0px auto; background-color: #ffffff; }</style>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            document.body.removeChild(frame1);
            $('#print_kunjungan').html('Cetak QR Code');
        }, 500);
        setTimeout(function () {
            window.location = baseUrl;
        }, 600);
        return false;
    }
// var base_url = window.location.origin + "/qr/reader"
var base_url = "{{ url('qr/reader') }}";


$('.view-survey').on("click", function(event) {
	document.getElementById("loading");
    
    var survey_id = $(this).data('id');

	const xhttp = new XMLHttpRequest();
	xhttp.open("GET", "survey/" + survey_id);
	xhttp.send();
	xhttp.onreadystatechange = function() {
		if (xhttp.status == 200) {
			var trHTML = ''; 
			const objects = JSON.parse(this.responseText)
			for (let object of objects) {
				trHTML += '<tr>';
				trHTML += '<td>'+object['question']+'</td>';
				trHTML += '<td>'+object['response']+'</td>';
				trHTML += "</tr>";
			}
			document.getElementById("data-survey").innerHTML = trHTML;
			$("#view-survey").modal('show');
		}else {
			Swal.fire({
				type: 'error',
				title: '<i>Survey Tidak dilakukan oleh tamu ini pada saat kunjungan</i>'
			})
		}
	}
});

var url_download = "{{ url('/kunjungan/pdf', $data_personal->unique_id)}}"
var url_awal = "{{ url('/kunjungan', $data_personal->unique_id)}}"

$('#button_download_pdf').on('click', function (e) {
        $(this).html('Downloading...');
        $.get(url_download, function (data) {
            window.location = url_download;
			console.log(url_download, 'disini cuyy')
            $('#button_download_pdf').html('Download PDF');
        });
        window.location = url_awal;
    });

</script>
@endpush

