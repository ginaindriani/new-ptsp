@extends('layout.frontend.index') 
@push('plugins-css')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<style type="text/css">
 #my_camera{
     width: 320px;
     height: 240px;
     border: 1px solid black;
}
</style>
 @endpush 
 @section('content')
<div class="registration-form-section">
  <div class="section-title reg-header animated fadeInDown" data-animation="fadeInDown">
    <center>
      <h3>BUKU TAMU</h3>
    </center>
    <center>
      <h5>{{ $satker->nama_satker }}</h5>
    </center>
    <hr />
    <center>
      <h4 style="font-family: Arial, Helvetica, sans-serif;">
        <strong>Pilih Kendaraan Anda</strong>
      </h4>
      <p hidden>{{ $urlsatker = ('/update/'.$satker->slug); }}</p>
      <div class="row">
        
        @forelse ($data as $key=>$data)
          <a href="{{ url($urlsatker, $data->unique_id) }}"><img src="{{ url($data->photo_kendaraan) }}" width='300' id="loads" height='200' style="border-radius: 1.00rem; margin:10px"></a>
        @empty
        <h5>TIDAK ADA TAMU DATANG MELALUI KAMDAL</h5>
        @endforelse
      </div>
      <div id="loading"></div>
      
        <div class="registration-form-action clearfix animated fadeInLeftBig" data-animation="fadeInLeftBig" data-animation-delay=".15s" style="animation-delay: 0.15s;">
          <input type="file" id="file_ktp" style="display: none;" />
          <div class="pull-left">
            <a href = {{ url($satker->slug) }} type="button" id="loads" class="btn btn-danger"><i class="fa fa-chevron-left"> KEMBALI KE MENU AKSI</i></a>
          </div>
          <div class="pull-right">
            <a href = {{ url('/satker',$satker->slug) }} type="button" id="loads" class="btn btn-success">SAYA TIDAK MEMBAWA KENDARAAN PRIBADI <i class="fa fa-chevron-right"></i></a>
          </div>
        </div>
        
      </form>
    </center>
  </div>
  <div class="clearfix">
    <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
      <div class="reg-content">
        <div class="registration-form-action clearfix link1 animated fadeInRightBig" data-animation="fadeInRightBig" data-animation-delay=".2s" style="animation-delay: 0.2s;">
          <div class="row text-center">
            <span>
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script>
              &copy; KEJAKSAAN AGUNG RI
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection 
@push('modal') 
@endpush 
@push('plugins-js')
<script type="text/javascript">
$(document).ready(function() {

  setTimeout(function(){
    window.location.reload(1);
  }, 60000);
  $.ajaxSetup({
    headers: {
      'csrftoken': '{{ csrf_token() }}'
    }
  });
  const webcamElement = document.getElementById('webcam');
  const canvasElement = document.getElementById('canvas');
  const snapSoundElement = document.getElementById('snapSound');
  const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);

  webcam.start()
    .then(result =>{
        console.log("webcam started");
    })
    .catch(err => {
        console.log(err);
    }
  );

  $("#take-photo").click(function () {
    beforeTakePhoto();
    let picture = webcam.snap();
    $("#download-photo").val(picture);
    Swal.fire({
      type: 'success',
      title: 'Berhasil mengambil foto!',
      html: '<em>'+'Lanjutkan dengan klik Simpan'+'</em>',
      showCancelButton: false,
      showConfirmButton: true
    });
    // document.querySelector('#download-photo').href = picture;
    afterTakePhoto();
  });

  $("#retake-photo").click(function () {
    webcam.stream()
        .then(facingMode =>{
            removeCapture();
        }
    );
  });

  function beforeTakePhoto(){
    $('#flash-cam')
        .show()
        .animate({opacity: 0.3}, 500)
        .fadeOut(500)
        .css({'opacity': 0.7});
    // window.scrollTo(0, 0);
    $('#webcam-control').addClass('d-none');
    $('#cameraControls').addClass('d-none');
  };

  function afterTakePhoto(){
    webcam.stop();
    $('#canvas').removeClass('d-none');
    $('#take-photo').addClass('d-none');
    $('#retake-photo').removeClass('d-none');
    // $('#download-photo').removeClass('d-none');
    // $('#resume-camera').removeClass('d-none');
    // $('#cameraControls').removeClass('d-none');
  }

  function removeCapture(){
    $('#canvas').addClass('d-none');
    $('#take-photo').removeClass('d-none');
    $('#retake-photo').addClass('d-none');
  }

  const webcamElementKendaraan = document.getElementById('webcamkendaraan');
  const canvasElementKendaraan = document.getElementById('canvaskendaraan');
  const snapSoundElementKendaraan = document.getElementById('snapSound');
  const webcamKendaraan = new Webcam(webcamElementKendaraan, 'user', canvasElementKendaraan, snapSoundElementKendaraan);
  webcamKendaraan.start().then(result => {
    console.log("webcam kendaraan started");
  }).catch(err => {
    console.log(err);
  });
  $("#take-photo-kendaraan").click(function() {
    beforeTakePhotoKendaraan();
    let pictureKendaraan = webcamKendaraan.snap();
    $("#download-photo-kendaraan").val(pictureKendaraan);
    Swal.fire({
      type: 'success',
      title: 'Berhasil mengambil foto!',
      html: '<em>'+'Lanjutkan dengan klik Simpan'+'</em>',
      showCancelButton: false,
      showConfirmButton: true
    });
    // document.querySelector('#download-photo').href = pictureKendaraan;
    afterTakePhotoKendaraan();
  });
  $("#retake-photo-kendaraan").click(function() {
    webcamKendaraan.stream().then(facingMode => {
      removeCapture();
    });
  });

  function beforeTakePhotoKendaraan() {
    $('#flash-cam-kendaraan').show().animate({
      opacity: 0.3
    }, 500).fadeOut(500).css({
      'opacity': 0.7
    });
    // window.scrollTo(0, 0);
    // $('#webcam-control').addClass('d-none');
    // $('#cameraControls').addClass('d-none');
  };

  function afterTakePhotoKendaraan() {
    webcamKendaraan.stop();
    $('#canvaskendaraan').removeClass('d-none');
    $('#take-photo-kendaraan').addClass('d-none');
    $('#retake-photo-kendaraan').removeClass('d-none');
    // $('#download-photo').removeClass('d-none');
    // $('#resume-camera').removeClass('d-none');
    // $('#cameraControls').removeClass('d-none');
  }

  function removeCaptureKendaraan() {
    $('#canvaskendaraan').addClass('d-none');
    $('#take-photo-kendaraan').removeClass('d-none');
    $('#retake-photo-kendaraan').addClass('d-none');
  }
});

</script>
@endpush

