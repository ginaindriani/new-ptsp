@extends('layout.frontend.index') 
@push('plugins-css')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
 @endpush 
 @section('content')
<div class="registration-form-section">
  <div class="section-title reg-header animated fadeInDown" data-animation="fadeInDown">
    <center>
      <h3>BUKU TAMU</h3>
    </center>
    <center>
      <h5>{{ $data[0]->nama_satker }}</h5>
    </center>
    <hr />
    <center>
      <h4 style="font-family: Arial, Helvetica, sans-serif;">
        <strong>Tujuan dan Biodata Tamu</strong>
      </h4>
    </center>
  </div>
  <div class="clearfix">
    <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
      <div class="reg-content">
        <form action="{{ route('updatetamu', $unique->unique_id) }}" method="post" id="formsimpan">
          @csrf 
          @method('PUT')
          <input name="id_satker" value="{{ $data[0]->id_satker }}" hidden />
          <input name="id_organisasi" value="{{ $data[0]->id_organisasi }}" hidden />
          <input name="id_kunjungan" value="{{ $unique->id_kunjungan }}" hidden />
          <div class="textbox-wrap" id="">
            <fieldset class="scheduler-border">
              <legend class="scheduler-border">Data Kunjungan</legend>
              <div class="form-group">
                <center>
                <span>Kendaraan Anda</span>
                <label style="color: red;">*</label>
                <br/>
                <img src="{{ url($unique->photo_kendaraan) }}" width='300' height='200'>
                <br/>
                <h5>ID KUNJUNGAN ANDA : {{ $unique->unique_id }}</h5>
              </center>

              </div>

            </fieldset>
          </div>
          <div class="textbox-wrap" id="">
            <fieldset class="scheduler-border">
              <div class="form-group">
                <span>Tipe Pelayanan</span>
                <label style="color: red;">*</label>
                <input type="text" class="form-control" id="status" name="status" value="Pelayanan Pertemuan Tatap Muka (Datang langsung saat ini, anda sudah berada di Kejaksaan)" required />
              </div>
              
              <div id="tujuan">
                <legend class="scheduler-border">Tujuan</legend>
                <div class="form-group">
                  <span>Pilih Tujuan</span>
                  <label style="color: red;">*</label>
                  <div class="row">
                    <div class="col-sm-12">
                      <select name="id_pegawai" class="form-control select2" id="kejari" required>
                        <option value="tamu-tidak-mengunjungi-siapapun">===PILIH TUJUAN===</option>
                        @forelse ($data_tujuan as $key => $data_tujuan )
                        <option value="{{ $data_tujuan->id_pegawai }}">{{ $data_tujuan->tujuan }}</option>
                        @empty TIDAK ADA DATA @endforelse
                      </select>
                    </div>
                    {{-- <div class="col-sm-3">
                      <a href="javascript:;" data-id="{{ $data[0]->kode_satker }}" class="btn btn-primary btn-block view-pegawai">
                        Lihat Daftar Hadir Pegawai
                      </a>
                    </div> --}}
                  </div>
                </div>
              </div>
              <div class="form-group">
                <span>Tujuan Anda </span>
                <label style="color: red;">*</label>
                <textarea name="kunjungan_detail" class="form-control" rows="3" required></textarea>
              </div>
              @if ($data[0]->id_satker == 540)
                <div class="form-group">
                  <span>Tipe Tamu</span>
                  <label style="color: red;">*</label>
                  <select name="tamu_tipe_tamu" onchange="tamu_tipe_tamu_view(this);" id="tamu_tipe_tamu" class="form-control select2" required>
                    <option value="">Pilih Tipe Tamu</option>
                    <option value="1">Tamu Biasa</option>
                    <option value="2">Saksi</option>
                    <option value="3">Tersangka/Terdakwa</option>
                    <option value="4">Ahli</option>
                    <option value="5">Tamu VVIP</option>
                  </select>
                </div>
              @else
              <div class="form-group">
                <span>Tipe Tamu</span>
                <label style="color: red;">*</label>
                <select name="tamu_tipe_tamu" id="tamu_tipe_tamu" class="form-control select2" required>
                  <option value="">Pilih Tipe Tamu</option>
                  <option value="1">Tamu Biasa</option>
                  <option value="2">Saksi</option>
                  <option value="3">Tersangka/Terdakwa</option>
                  <option value="4">Ahli</option>
                  <option value="5">Tamu VVIP</option>
                </select>
              </div>
              @endif
              
            
            </fieldset>
            <fieldset class="scheduler-border">
              <legend class="scheduler-border">Data Pribadi</legend>
              <div class="form-group">
                <span>Tipe Identitas </span>
                <label style="color: red;">*</label>
                <select name="tamu_tipe_identitas" id="tamu_tipe_identitas" class="form-control select2" required>
                  <option value="">Pilih Tipe Identitas</option>
                  <option value="1">KTP</option>
                  <option value="2">SIM</option>
                  <option value="3">Passport</option>
                </select>
              </div>
              <div class="row">
                <div class="form-group col-sm-10">
                  <span>No Identitas </span>
                  <label style="color: red;">*</label>
                  <input type="text" id="tamu_no_identitas" class="form-control" name="tamu_no_identitas" placeholder="Masukan no Identitas Anda" value="" required />
                </div>
                <div class="col-sm-2">
                  <span>Aksi</span>
                  <label style="color: red;">*</label>
                  <a id="searchnik" class="btn btn-success">Lanjutkan <i class="fa fa-chevron-right"></i></a>
                </div>
              </div>
              <div id="loading"></div>
              <div id ="divIDBio">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <span>Nama Anda </span>
                      <label style="color: red;">*</label>
                      <input type="text" class="form-control" name="tamu_nama" placeholder="Masukan nama sesuai KTP Anda" value="" id="nama" required />
                    </div>
                    <div class="form-group">
                      <span>Email Anda </span>
                      <label style="color: red;">*</label>
                      <input type="text" class="form-control" name="tamu_email" placeholder="Masukan email Anda" value="" id="email" required />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <span>No HP / Whatsapp </span>
                      <label style="color: red;">*</label>
                      <input type="text" class="form-control" name="tamu_no_hp" placeholder="Masukan no HP atau whatsapp" value="" id="no_hp" required />
                    </div>
                    <div class="form-group">
                      <span>Plat Kendaraan </span>
                      <input type="text" class="form-control" name="tamu_no_polisi" placeholder="Masukan no plat kendaraan Anda" value="" id="no_polisi" />
                      <span class="form-text text-muted">Kosongkan apabila Anda tidak memiliki kendaraan.</span>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <span>Jenis Kelamin</span>
                      <label style="color: red;">*</label>
                      <td>
                        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control select2" required>
                          <option value="1">Laki-laki</option>
                          <option value="2">Perempuan</option>
                        </select>
                      </td>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <span>Alamat Anda </span>
                      <label style="color: red;">*</label>
                      <textarea placeholder="Masukan alamat sesuai KTP Anda" name="tamu_alamat" class="form-control" rows="3" id="alamat" required></textarea>
                    </div>
                  </div>
                  <div id="barang" style="display: none;">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <span>Barang bawaan yang dibawa pada saat menemui tamu</span>
                        <textarea placeholder="Masukan barang yang dibawa" name="barang_bawaan" class="form-control" rows="3" id="barang_bawaan" ></textarea>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <span>Barang bawaan yang ditinggal diloket pada saat menemui tamu</span>
                        <textarea placeholder="Masukan barang yang tidak dibawa" name="barang_ditinggal" class="form-control" rows="3" id="barang_ditinggal" ></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <center>
                  <div class="col-sm-12">
                    <span>Klik tombol dibawah ini jika anda tidak datang sendiri</span>
                  </div>
                  <div class="col-sm-12">
                    <a id="buka_pengikut" class="btn btn-warning btn-block">Saya membawa pengikut</a>
                  </div>
                </center>
                {!! "&nbsp;" !!}
                <div id ="pengikut">
                  <div class="form-group">
                    <span>Jumlah Pengikut</span>
                    <label style="color: red;">*</label>
                    <input type="number" class="form-control" name="jumlah_tamu" placeholder="Masukan Jumlah Pengikut" value="" id="jumlah_tamu" />
                  </div>
                  <table class="table table-bordered" id="dynamicAddRemove">
                    <tr>
                        <th>Nama Pengikut</th>
                        <th>Jenis Kelamin</th>
                        <th>Kategori Pengikut</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td><input type="text" name="nama_pengikut[]" placeholder="Masukkan Nama" class="form-control" />
                        </td>
                        <td>
                          <select name="jenis_kelamin_pengikut[]" id="jenis_kelamin" class="form-control" >
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="1">Laki-laki</option>
                            <option value="2">Perempuan</option>
                          </select>
                        </td>
                        <td>
                          <select name="is_anak[]" id="kategori" class="form-control">
                            <option value="">Pilih Kategori Pengikut</option>
                            <option value="2">Dewasa</option>
                            <option value="1">Anak-anak</option>
                          </select>
                        </td>
                        <td><button type="button" name="add" id="dynamic-ar" class="btn btn-primary">Tambah</button></td>
                    </tr>
                  </table>
                </div>
                <legend class="scheduler-border">Foto</legend>
                <div class="form-group">
                  <span>Ambil foto diri Anda </span>
                  <label style="color: red;">*</label>
                  <center><a id="cameraFlip" class="btn btn-success">Ganti Kamera</a></center>
                  <div id="bx-cam">
                    <video id="webcam" autoplay playsinline></video>
                    <canvas id="canvas" class="d-none"></canvas>
                    <div id="flash-cam" class="flash-cam"></div>
                    <input type="hidden" name="photo" id="download-photo" />
                  </div>
                  <div id="bx-cam-icon">
                    <div class="row-icon">
                      <a id="take-photo" class="cam-icon">
                      <i class="fa fa-camera"></i>
                      </a>
                      <a id="retake-photo" class="d-none cam-icon">
                      <i class="fa fa-repeat"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </fieldset>
            <span style="color: red;">* Wajib diisi atau dipilih</span>
          </div>
          <input type="checkbox" id="checkme"/> Dengan ini, saya setuju untuk memberikan data
          <div class="registration-form-action clearfix animated fadeInLeftBig" data-animation="fadeInLeftBig" data-animation-delay=".15s" style="animation-delay: 0.15s;">
            <input type="file" id="file_ktp" style="display: none;" />
            <div class="pull-left">
              <a href = {{ url($data[0]->slug) }} type="button" id="loads" class="btn btn-danger"><i class="fa fa-chevron-left"> KEMBALI KE MENU AKSI</i></a>
            </div>
            <div class="pull-right">
              <button type="submit" onclick="simpandata()" id="tombol"name="tombol" class="btn btn-success">SIMPAN <i class="fa fa-chevron-right"></i></button>
            </div>
          </div>
        </form>
        <div class="registration-form-action clearfix link1 animated fadeInRightBig" data-animation="fadeInRightBig" data-animation-delay=".2s" style="animation-delay: 0.2s;">
          <div class="row text-center">
            <span>
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script>
              &copy; KEJAKSAAN AGUNG RI
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection 
@push('modal') 
<div class="modal fade" id="view-pegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Data pegawai</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body" id="modal-body">
      <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
          <tr>
            <th width="40%">Nama</th>
            <th width="30%">Status Kehadiran</th>
            <th width="30%">Keterangan</th>
          </tr>
        </thead>
        <tbody id="data-pegawai">
        </tbody>
        
      </table>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
    </div>
  </div>
  </div>
</div>
@endpush 
@push('plugins-js')
<script type="text/javascript">
  $('.view-pegawai').on("click", function(event) {
    document.getElementById("loading");
      
    var kode_satker = $(this).data('id');
    var date = new Date().toISOString().slice(0, 10);

    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://43.231.129.16/absen/api/status_ketersediaan_pegawai_per_satker?kode_satker=" + kode_satker + "&tanggal=" + date);
    xhttp.send();
    xhttp.onreadystatechange = function() {
      if (xhttp.status == 200) {
        var trHTML = ''; 
        const objects = JSON.parse(this.responseText)
        for (let object of objects['data']) {
          trHTML += '<tr>';
          trHTML += '<td>'+object['nama']+'</td>';
          trHTML += '<td>'+object['status_sekarang']+'</td>';
          trHTML += '<td>'+object['status_terakhir']+'</td>';
          trHTML += "</tr>";
        }
        document.getElementById("data-pegawai").innerHTML = trHTML;
        $("#view-pegawai").modal('show');
      }else {
        Swal.fire({
          type: 'error',
          title: '<i>Pegawai tidak ada</i>'
        })
      }
    }
  });
  var checker = document.getElementById('checkme');
  var sendbtn = document.getElementById('tombol');
  sendbtn.disabled = true;
  // when unchecked or checked, run the function
  checker.onchange = function(){
  if(this.checked){
      sendbtn.disabled = false;
  } else {
      sendbtn.disabled = true;
  }}

  function simpandata() {
    sendbtn.disabled = true;
    $('#tombol').html('Data sedang disimpan Mohon Tunggu');
    document.getElementById("formsimpan").submit();
  }
  

  
  $(document).ready(function() {
    var xy = 0;
    $("#dynamic-ar").click(function () {
        ++xy;
        $("#dynamicAddRemove").append('<tr id="row'+xy+'"><td><input type="text" name="nama_pengikut[]" placeholder="Masukkan Nama" class="form-control" /></td><td><select name="jenis_kelamin_pengikut[]" id="jenis_kelamin" class="form-control" required><option value="">Pilih Jenis Kelamin</option><option value="1">Laki-laki</option><option value="2">Perempuan</option></select></td><td><select name="is_anak[]" id="kategori" class="form-control" required><option value="">Pilih Kategori Pengikut</option><option value="1">Dewasa</option><option value="2">Anak-anak</option></select></td><td><button type="button" class="btn btn-danger remove-input-field">Hapus</button></td></tr>');
    });
    $(document).on('click', '.remove-input-field', function () {
        $(this).parents('tr').remove();
    });


    var x = document.getElementById("divIDBio");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }

    var p = document.getElementById("pengikut");
    if (p.style.display === "none") {
      p.style.display = "block";
    } else {
      p.style.display = "none";
    }
    
    $(".select2").select2({
      placeholder: "Pilih Data",
      width: '100%',
      theme: "bootstrap"
    });
    $.ajaxSetup({
      headers: {
        'csrftoken': '{{ csrf_token() }}'
      }
    });
    $('#tamu_no_identitas').on('keyup', function(){
      $value_no_identitas = $(this).val();
    });
    $('#buka_pengikut').on('click', function() {
      displayLoading()
      p.style.display = "block"
    });
    $('#searchnik').on('click', function() {
      displayLoading()
      $value = $value_no_identitas;
      var data = $.ajax({
        type: 'get',
        url: '{{URL::to('searchnik')}}',
        data: {
          'nik': $value
        },
        success: function(response, textStatus, request) {
          $('#nama').val(response.data.nama).removeClass('is-invalid').addClass('is-valid');
          $('#alamat').val(response.data.alamat).removeClass('is-invalid').addClass('is-valid');
          $('#no_polisi').val(response.data.no_polisi).removeClass('is-invalid').addClass('is-valid');
          $('#email').val(response.data.email).removeClass('is-invalid').addClass('is-valid');
          $('#no_hp').val(response.data.no_hp).removeClass('is-invalid').addClass('is-valid');
          $('#no_identitas').val(response.data.no_identitas).removeClass('is-invalid').addClass('is-valid');
          $('#jenis_kelamin').val(response.data.jenis_kelamin).removeClass('is-invalid').addClass('is-valid');
          hideLoading();
        },
      });
      x.style.display = "block"
    });

    const webcamElement = document.getElementById('webcam');
    const canvasElement = document.getElementById('canvas');
    const snapSoundElement = document.getElementById('snapSound');
    const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
    $('#cameraFlip').click(function() {
      webcam.flip();
      webcam.start();  
    });
    webcam.start()
        .then(result =>{
            console.log("webcam started");
        })
        .catch(err => {
            console.log(err);
        }
    );

    function refreshCamera(){
      webcam.start()
          .then(result =>{
              console.log("webcam started");
          })
          .catch(err => {
              console.log(err);
          }
      )
    }

    $("#take-photo").click(function () {
        beforeTakePhoto();
        let picture = webcam.snap();
        $("#download-photo").val(picture);
        Swal.fire({
          type: 'success',
          title: 'Berhasil mengambil foto!',
          html: '<em>'+'Lanjutkan dengan klik Simpan'+'</em>',
          showCancelButton: false,
          showConfirmButton: true
        });
        // document.querySelector('#download-photo').href = picture;
        afterTakePhoto();
    });

    $("#retake-photo").click(function () {
        webcam.stream()
            .then(facingMode =>{
                removeCapture();
            }
        );
    });


    function beforeTakePhoto(){
        $('#flash-cam')
            .show()
            .animate({opacity: 0.3}, 500)
            .fadeOut(500)
            .css({'opacity': 0.7});
        // window.scrollTo(0, 0);
        $('#webcam-control').addClass('d-none');
        $('#cameraControls').addClass('d-none');
    };

    function afterTakePhoto(){
        webcam.stop();
        $('#canvas').removeClass('d-none');
        $('#take-photo').addClass('d-none');
        $('#retake-photo').removeClass('d-none');
        // $('#download-photo').removeClass('d-none');
        // $('#resume-camera').removeClass('d-none');
        // $('#cameraControls').removeClass('d-none');
    }

    function removeCapture(){
        $('#canvas').addClass('d-none');
        $('#take-photo').removeClass('d-none');
        $('#retake-photo').addClass('d-none');
    }

    const webcamElementKendaraan = document.getElementById('webcamkendaraan');
    const canvasElementKendaraan = document.getElementById('canvaskendaraan');
    const snapSoundElementKendaraan = document.getElementById('snapSound');
    const webcamKendaraan = new Webcam(webcamElementKendaraan, 'user', canvasElementKendaraan, snapSoundElementKendaraan);
    webcamKendaraan.start().then(result => {
      console.log("webcam kendaraan started");
    }).catch(err => {
      console.log(err);
    });
    $("#take-photo-kendaraan").click(function() {
      beforeTakePhotoKendaraan();
      let pictureKendaraan = webcamKendaraan.snap();
      $("#download-photo-kendaraan").val(pictureKendaraan);
      Swal.fire({
        type: 'success',
        title: 'Berhasil mengambil foto!',
        html: '<em>'+'Lanjutkan dengan klik Simpan'+'</em>',
        showCancelButton: false,
        showConfirmButton: true
      });
      // document.querySelector('#download-photo').href = pictureKendaraan;
      afterTakePhotoKendaraan();
    });
    $("#retake-photo-kendaraan").click(function() {
      webcamKendaraan.stream().then(facingMode => {
        removeCapture();
      });
    });

    function beforeTakePhotoKendaraan() {
      $('#flash-cam-kendaraan').show().animate({
        opacity: 0.3
      }, 500).fadeOut(500).css({
        'opacity': 0.7
      });
      // window.scrollTo(0, 0);
      // $('#webcam-control').addClass('d-none');
      // $('#cameraControls').addClass('d-none');
    };

    function afterTakePhotoKendaraan() {
      webcamKendaraan.stop();
      $('#canvaskendaraan').removeClass('d-none');
      $('#take-photo-kendaraan').addClass('d-none');
      $('#retake-photo-kendaraan').removeClass('d-none');
      // $('#download-photo').removeClass('d-none');
      // $('#resume-camera').removeClass('d-none');
      // $('#cameraControls').removeClass('d-none');
    }

    function removeCaptureKendaraan() {
      $('#canvaskendaraan').addClass('d-none');
      $('#take-photo-kendaraan').removeClass('d-none');
      $('#retake-photo-kendaraan').addClass('d-none');
    }
    $(function() {
      $(".form-control").focus(function() {
        $(this).closest(".textbox-wrap").addClass("focused");
      }).blur(function() {
        $(this).closest(".textbox-wrap").removeClass("focused");
      });
    });
    $("#txt_username").focus();
    $("#txt_username").keypress(function(e) {
      if (e.which == 13) {
        validate();
        e.preventDefault();
      }
    });
    $("#txt_password").keypress(function(e) {
      if (e.which == 13) {
        validate();
        e.preventDefault();
      }
    });
    $("#btn_login").click(function() {
      // validate();
      window.location = "{{ url('tujuan') }}";
    });
  });
</script>
@endpush