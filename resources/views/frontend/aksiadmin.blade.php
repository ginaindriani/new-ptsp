@extends('layout.frontend.index') 
@push('plugins-css')
<style type="text/css"></style>
 @endpush 
 @section('content')
<div class="registration-form-section">
  <div class="section-title reg-header animated fadeInDown" data-animation="fadeInDown">
    <center>
      <h3>BUKU TAMU</h3>
    </center>
    <center>
      <h5>{{ $satker->nama_satker }}</h5>
    </center>
    <hr />
    <center>
      <h4 style="font-family: Arial, Helvetica, sans-serif;">
        <strong>Pilih Aksi Anda Sebagai : </strong>
      </h4>
      <div class="row">
        <div class="col-sm-4">
          <a href="{{ url('/kamdal', $satker->slug) }}"><img src=" {{ asset('/img/kamdal.png') }}" width="200" height="300"></a>
        </div>
        <div class="col-sm-4">
          <a href="{{ url('/qr/reader') }}"><img src=" {{ asset('/img/qr-reader.png') }} " width="200" height="300"></a>
        </div>
        <div class="col-sm-4">
          <a href="{{ url('/panel') }}"><img src=" {{ asset('/img/admin2.png') }} " width="200" height="300"></a>
        </div>
        {{-- <div class="col-sm-3">
          <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/ptsp.png') }} " width="200" height="300"></a>
        </div>
        
        <div class="col-sm-3">
          <a href="{{ url('/tilang', $satker->slug) }}"><img src=" {{ asset('/img/TILANG.png') }} " width="200" height="300"></a>
        </div> --}}
      </div>
    </center>
  </div>
  <div class="clearfix">
    <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
      <div class="reg-content">
        <div class="registration-form-action clearfix link1 animated fadeInRightBig" data-animation="fadeInRightBig" data-animation-delay=".2s" style="animation-delay: 0.2s;">
          <div class="row text-center">
            <span>
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script>
              &copy; KEJAKSAAN AGUNG RI
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection 
@push('modal') 
@endpush 
@push('plugins-js')
<script type="text/javascript">
</script>
@endpush

