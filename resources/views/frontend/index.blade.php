@extends('layout.frontend.index')

@push('plugins-css')
<style type="text/css">

</style>
@endpush

@section('content')
<div class="login-form-section">
    <div class="login-content  animated bounceIn" data-animation="bounceIn">
        {{-- <form action="{{ route('pilihsatker') }}" method="post">
			@csrf --}}
            <center>
                <div class="section-title">
                    <h3>Buku Tamu - Kejaksaan RI</h3>
                    <h5>Silahkan pilih Satuan Kerja yang anda tuju</h5>
                    <hr/>
                </div>
            </center>
            <div class="textbox-wrap focused">
                <div class="input-group">
	                <select id="pilihsatker" name="pilihsatker" class="form-control select2">
							<option disable value="">===Pilih Satker===</option>
						@forelse ($satker as $key => $satker)
							<option value="{{ $satker->slug }}">{{ $satker->nama_satker }}</option>
						@empty
							<option value="">DATA TIDAK TERSEDIA</option>
						@endforelse
                	</select>
	            </div>
            </div>
            <div class="login-form-action clearfix">
            	<div class="pull-left">
            		
            	</div>
            	<div class="pull-right">
					<a id="link_combo" name="link_combo" type="button" class="btn btn-success" href="{{ url('/kamdal', $satker->slug) }}">Pilih<a>
            	</div>                
            </div>
        {{-- </form> --}}
    </div>
    {{--
    <div class="login-form-links link1  animated fadeInRightBig" data-animation="fadeInRightBig" data-animation-delay=".2s" style="animation-delay: 0.2s;">
        <h4 class="green">Masuk KAMDAL?</h4>
        <a href="{{ url('admin') }}" class="green">Klik di sini</a>
        <span>untuk ADMINSTRATOR</span>
    </div>
    --}}
    <div class="login-form-links link1  animated fadeInLeftBig" data-animation="fadeInLeftBig" data-animation-delay=".2s" style="animation-delay: 0.2s;">
    	<div class="row text-center">
    		<span class="blue"><script type='text/javascript'>document.write(new Date().getFullYear());</script> &copy; KEJAKSAAN REPUBLIK INDONESIA</span>
    	</div>
    </div>
</div>
@endsection

@push('modal')

@endpush

@push('plugins-js')
<script type="text/javascript">
$("#pilihsatker").change(function () {
	var admin= "http://localhost/new-ptsp/public/new/";
	var link= this.value;
	var con = admin.concat(link);

	console.log(con,"kondisi");
  $("#link_combo").attr('href',con);
});


$(document).ready(function(){
	$(".select2").select2({
        placeholder: "Pilih Satker",
        width: '100%',
        theme: "bootstrap"
    });

    $(function () {
        $(".form-control").focus(function () {
            $(this).closest(".textbox-wrap").addClass("focused");
        }).blur(function () {
            $(this).closest(".textbox-wrap").removeClass("focused");
        });
    });
    
    $("#txt_username").focus();
	
	$("#txt_username").keypress(function(e) {
	    if(e.which == 13) {
	        validate();
	        e.preventDefault();
	    }
	});

	$("#txt_password").keypress(function(e) {
	    if(e.which == 13) {
	        validate();
	        e.preventDefault();
	    }
	});

	$("#btn_login").click(function() {
		// validate();
		window.location = "{{ url('tujuan') }}";
	});
});
</script>
@endpush