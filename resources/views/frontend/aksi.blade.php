@extends('layout.frontend.index') 
@push('plugins-css')
<style type="text/css"></style>
 @endpush 
 @section('content')
<div class="registration-form-section">
  <div class="section-title reg-header animated fadeInDown" data-animation="fadeInDown">
    <center>
      <h3>LAYANAN</h3>
    </center>
    <center>
      <h5>{{ $satker->nama_satker }}</h5>
    </center>
    <hr />
    <center>
      <h4 style="font-family: Arial, Helvetica, sans-serif;">
        <strong>Tujuan Kunjungan Anda </strong>
      </h4>
      <div class="row">
        @if($satker->tipe_satker == '3' || $satker->tipe_satker == '4')
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/tatapmuka.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="col-sm-2">
          </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/tatapmukadenganjanji.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="clearfix">
          <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
            <div class="reg-content">
            </div>
          </div>
        </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/mengantarkansurat.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="col-sm-2">
          </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/terpadusatupintu.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="clearfix">
          <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
            <div class="reg-content">
            </div>
          </div>
        </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/mengantarkantahanan.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="col-sm-2">
          </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/menemuitahanan.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          
        @else
        <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/tatapmuka.png') }} " width="450" height="200" border-radius="0.63rem"></a>
          </div>
          <div class="col-sm-2">
          </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/tatapmukadenganjanji.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="clearfix">
          <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
            <div class="reg-content">
            </div>
          </div>
        </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/mengantarkansurat.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="col-sm-2">
          </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/terpadusatupintu.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="clearfix">
          <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
            <div class="reg-content">
            </div>
          </div>
        </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/mengantarkantahanan.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
          <div class="col-sm-2">
          </div>
          <div class="col-sm-4">
            <a href="{{ url('/ptsp', $satker->slug) }}"><img src=" {{ asset('/img/menemuitahanan.png') }} " width="450" height="200" style="border-radius: 0.95rem;"></a>
          </div>
        @endif
      </div>
    </center>
  </div>
  <div class="clearfix">
    <div class="col-sm-12 registration-left-section animated fadeInUp" data-animation="fadeInUp">
      <div class="reg-content">
        <div class="registration-form-action clearfix link1 animated fadeInRightBig" data-animation="fadeInRightBig" data-animation-delay=".2s" style="animation-delay: 0.2s;">
          <div class="row text-center">
            <span>
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script>
              &copy; KEJAKSAAN AGUNG RI
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection 
@push('modal') 
@endpush 
@push('plugins-js')
<script type="text/javascript">
</script>
@endpush

