<?php
    function tgl_indo($tanggal){
    $substr = substr($tanggal, 0, -9);
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $substr);

    if($substr == NULL){
        return NULL;
    }
    
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    function tgl_indo2($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        if($tanggal == NULL){
            return NULL;
        }
        
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
        content="Pengaduan Masyarakat Adalah Layanan Interaksi Dan Komunikasi Masyarakat Yang Disediakan Oleh Kejaksaan RI Serta Kolaborasi Data Antar Kementrian Lembaga.">
    <meta name="keywords"
        content="portfolio, agency, business, clean, company, corporate, creative, gallery, rtl, modern, photography, vue, vuejs, responsive, sass">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">
    <!-- title of the browser tab -->
    <title>Kejaksaan Agung Republik Indonesia</title>

    <!-- favicon -->
    <link rel="icon" href="{{asset('assets/logo-kejaksaan.png')}}">

    {{-- <script src="{{ asset('landingpage/scripts/main.js') }}"></script> --}}

    <!-- fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Roboto&family=Saira+Stencil+One&text=Nafie&display=swap">
    <!-- chartist CSS -->

    <!-- css libraries file -->
    <link rel="stylesheet" href="{{ asset('landingpage/styles/libraries.min.css') }}">
    <!--boxicon CDN-->
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>

    {{-- <!-- Bootstrap file -->
    <link rel="stylesheet" href="{{ asset('styles/bootstrap.min.css') }}"> --}}
    <!-- main style file -->
    <link rel="stylesheet" href="{{ asset('landingpage/styles/main.css') }}">
    <link rel="stylesheet" href="{{ asset('landingpage/styles/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landingpage/styles/custom.css') }}">

    {{-- <link href="{{url('/')}}/demo1/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" /> --}}
    <link href="{{url('/')}}/demo1/plugins/global/plugins-custom.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/demo1/css/style.bundle.css" rel="stylesheet" type="text/css" />

    <style>
        .logo-title {
            margin-left: 69px !important;
        }

        .logo-title-header {
            justify-content: center;
        }

        .text-desc {
            font-size: 1.5rem;
        }

        @media (min-width: 992px) {
            .portfolio-section .section-content .row {
                -webkit-column-count: 4;
                -moz-column-count: 4;
                column-count: 4;
            }
            .section .text-box .subtitle:before {
                width: 50px !important;
            }
        }

        @media only screen and (max-width: 400px) {
            .logo-title {
                font-size: 20px !important;
                margin-left: 0 !important;
            }

            .logo-title-header {
                justify-content: start;
            }

            .text-desc {
                font-size: 1rem;
            }
        }

        @media only screen and (max-width: 600px) {
            .logo-title {
                font-size: 24px !important;
                margin-left: 0 !important;
            }

            .text-desc {
                font-size: 1rem;
            }
        }

        .menu-custom {
            -webkit-box-shadow: 10px 10px 5px 0px rgb(0 0 0 / 75%);
            -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 1px 2px 4px 0px rgb(0 0 0 / 75%);
            text-transform: uppercase;
            font-weight: bold;
            padding: 0.5rem 1.5rem;
            color: rgb(255, 255, 255);
        }

        #card-search-nik {
            background-color: #313131;
            -webkit-box-shadow: 11px 10px 38px rgb(0 0 0 / 38%);
            box-shadow: 11px 10px 38px rgb(0 0 0 / 38%);
            border-radius: 0.63rem;
        }

        .contact-section .section-content .contact-form {
            padding: 25px;
            border-radius: 0.63rem;
            width: 100%;
            float: right;
        }

        check:active + .btn.btn-warning, .btn.btn-warning:focus:not(.btn-active), .btn.btn-warning:hover:not(.btn-active), .btn.btn-warning:active:not(.btn-active), .btn.btn-warning.active, .btn.btn-warning.show, .show > .btn.btn-warning {
            background-color: rgb(177 107 60) !important;
        }

        ::-moz-selection {
            background-color: #e38443;
        }

        ::selection {
            background-color: #e38443;
        }

        .about-section .section-content > * {
            margin-bottom: 0px;
        }

        .contact-section .section-content {
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: normal;
            margin-bottom: -50px;
        }

        #container-chat {
            padding: 25px;
            border-radius: 0.63rem;
            margin-bottom: 20px;
            background-color: white;
            -webkit-box-shadow: 11px 10px 38px rgb(0 0 0 / 38%);
            box-shadow: 11px 10px 38px rgb(0 0 0 / 38%);
            color: #424242;
        }

        .body-chat {
            margin-bottom: 15px;
            font-style: italic;
        }

        .identity-chat span {
            font-style: italic;
        }
        .identity-chat small {
            font-weight: 400;
        }
        .identity-chat {
            margin-bottom: 10px;
        }

        .content-chat {
            padding: 16px;
            background-color: #fafafa;
            border-radius: 0.63rem;
            font-weight: bold;
            max-height: 400px;
            overflow: scroll;
        }

        .content-answer {
            padding-left: 30px;
            margin-left: 10px;
            border-left: 2px solid #c8c7c7;
        }
    </style>
</head>

<body style="position: relative;" class="loader">
    <!-- start preloader-->
    <div class="preloader" id="loading">
        <div class="circle">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
    <!-- end preloader-->
    <div id="app">
        <div id="app-inner" ref="appRef" :class="[savedTheme, {'enable-focus-style': isAnyFocus}]"
            @click="isAnyFocus = false" @keyup.tab="isAnyFocus = true">

            <div class="circle-cursor circle-cursor-outer" ref="circleCursorOuter"></div>
            <div class="circle-cursor circle-cursor-inner" ref="circleCursorInner"></div>

            <!-- start preloader-->
            {{-- <transition name="preloader">
                <div class="preloader" id="loading">
                    <div class="circle">
                        <div class="double-bounce1"></div>
                        <div class="double-bounce2"></div>
                    </div>
                </div>
            </transition> --}}
            <!-- end preloader-->
            <header class="fixed-top">
                <div class="header-social">
                    <div class="container px-md-0 px-3 py-0">
                        <div class="d-flex align-items-center justify-content-end py-2">
                            <div class="social">
                                <a href="https://www.facebook.com/Kejaksaan.Republik.Indonesia" target="_blank" rel="noreferrer">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <a href="https://twitter.com/Kejaksaan_RI" target="_blank" rel="noreferrer">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                                <a href="https://www.youtube.com/channel/UCy9IA8V2Wo2pxHHXDVVtuhA" target="_blank" rel="noreferrer">
                                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="navbar navbar-expand-lg rz-header navbar-blue">
                    <div class="container px-md-0 px-3 py-0">
                        <div class="col-2">
                            <a class="navbar-brand" href="/">
                                <img src="{{ asset('landingpage/images/hero-img.png') }}" alt="User Name">
                            </a>
                        </div>
                        <div class="col-md-10 py-0 bordered-bottom">
                            <div class="collapse mb-0 navbar-collapse flex-grow-1 text-right" id="navbarSupportedContent">
                                <ul class="navbar-nav ms-auto text-center flex-nowrap mb-2 mb-lg-0">
                                    <li class="nav-item hvr-inline">
                                        <a class="nav-link border-bottom-sm active" aria-current="page" href="#">Beranda</a>
                                    </li>
                                    <li class="nav-item hvr-inline dropdown">
                                        <a class="dropdown-toggle nav-link border-bottom-sm" id="navbarDropdown" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Layanan
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @foreach ($menuLayanan as $item)
                                                <a class="dropdown-item" href="{{ucwords($item['url'])}}&uuid={{request()->uuid}}">{{ucwords($item['nama'])}}</a>
                                            @endforeach
                                            {{-- <a class="dropdown-item" href="/profil/latarbelakang">Latar Belakang</a>
                                            <a class="dropdown-item" href="/profil/legalitas">Legalitas</a>
                                            <a class="dropdown-item" href="/profil/struktur">Struktur Organisasi</a> --}}
                                        </div>
                                    </li>
                                    <li class="nav-item hvr-inline dropdown">
                                        <a class="dropdown-toggle nav-link border-bottom-sm" id="navbarDropdown" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Informasi
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @foreach ($menuInformasi as $item)
                                                <a class="dropdown-item" href="{{ucwords($item['url'])}}&uuid={{request()->uuid}}">{{ucwords($item['nama'])}}</a>
                                            @endforeach
                                        </div>
                                    </li>
                                    <li class="nav-item hvr-inline dropdown">
                                        <a class="dropdown-toggle nav-link border-bottom-sm" id="navbarDropdown" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @foreach ($kegiatan as $item)
                                                <a class="dropdown-item" href="{{url('kegiatan/'.$item->slug)}}">{{ucwords($item->judul_kegiatan)}}</a>
                                            @endforeach
                                        </div>
                                    </li>
                                    <li class="nav-item hvr-inline dropdown">
                                        <a class="dropdown-toggle nav-link border-bottom-sm" id="navbarDropdown" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pengaduan
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @foreach ($menuPengaduan as $item)
                                                <a class="dropdown-item" href="{{ucwords($item['url'])}}&uuid={{request()->uuid}}">{{ucwords($item['nama'])}}</a>
                                            @endforeach
                                        </div>
                                    </li>
                                    {{-- <li class="nav-item hvr-inline">
                                        <a class="nav-link border-bottom-sm" href="#">Proses Pelayanan</a>
                                    </li> --}}
                                    
                                </ul>
                            </div>
                            <a class="navbar-toggler collapsed float-end mb-4" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <div class="wrap">
                                    <div class="burger">
                                        <div class="strip burger-strip">
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </nav>
            </header>
            <div style="height:55px; width:100%; clear: both; display:block;">
            </div>
            <div class="box-spacer">
                <div class="container px-md-0 px-3">
                    <div>
                        <div class="swiper swipermain">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                @foreach ($slideratas as $slideratas)
                                    <div class="swiper-slide">
                                        <div class="row">
                                            <div class="col-md-5 d-flex align-items-center">
                                                <div class="content-title">
                                                    <h1>
                                                        {{$slideratas->judul_slider}}
                                                    </h1>
                                                    <p>
                                                        {{$slideratas->deskripsi_slider}}
                                                    </p>
                                                    @if($slideratas->url_slider != null)
                                                    <div class="content-title-footer">
                                                        <a href="{{$slideratas->url_slider}}" class="btn btn-primary">Lihat Detail</a>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                            </div>
                                            <div class="col-md-6 py-4">
                                                <!-- Slides -->
                                                <img src="{{ asset($slideratas->gambar) }}" alt="Gambar" width="625" height="300">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev">
                            <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        </div>
                        <div class="swiper-button-next">
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-sec bg-green">
                <div class="position-relative" id="title-single-page">
                    @if($datatamu != null)
                        <div class="container mb-3">
                            <div class="container px-0 ">
                                <div class="header-content px-3 py-5 pt-md-5 pb-md-4 mx-auto text-center">
                                    <h1 class="title">Selamat Datang {{$datatamu->name}}</h1>
                                    <p class="sub-title">
                                        Jika anda pernah melakukan permohonan pelayanan, anda dapat mengecek status pelayanan anda dengan klik tombol Cari pada kolom dibawah ini
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="px-md-5 pb-md-5 pt-0 d-flex align-items-center">
                            <div class="container col-md-11 list-card-news">
                                <div class="row">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-8">
                                                <div id="card-search-nik">
                                                    <div class="input-group p-4">
                                                        <input type="text" class="form-control" name="nik" value="{{$datatamu->nik}}" placeholder="Cari NIK" aria-label="Search" aria-describedby="basic-addon2">
                                                        <div class="input-group-append">
                                                            <button style="background-color: rgba(68,103,232, 1);" class="input-group-text btn btn-warning cari-nik" id="basic-addon2">
                                                                <i class="fa fa-search fs-2x"></i> Cari
                                                            </button>
                                                            {{-- <span class="input-group-text" id="basic-addon2"></span> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>&nbsp;
                                            <center><link><a href="{{url('/')}}"><p style="color: white">Jika anda telah selesai melakukan pelayanan silahkan klik tulisan ini untuk melakukan reset sesi (logout)</p></a></link></center>
                                            <div class="col-lg-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="container mb-3">
                            <div class="container px-0 ">
                                <div class="header-content px-3 py-5 pt-md-5 pb-md-4 mx-auto text-center">
                                    <h1 class="title">Cari NIK anda sekarang</h1>
                                    <p class="sub-title">
                                        Jika anda pernah melakukan permohonan pelayanan, anda dapat mengecek status pelayanan anda dengan mencari NIK anda melalui kolom dibawah ini
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="px-md-5 pb-md-5 pt-0 d-flex align-items-center">
                            <div class="container col-md-11 list-card-news">
                                <div class="row">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-8">
                                                <div id="card-search-nik">
                                                    <div class="input-group p-4">
                                                        <input type="text" class="form-control" name="nik" placeholder="Cari NIK" aria-label="Search" aria-describedby="basic-addon2">
                                                        <div class="input-group-append">
                                                            <button style="background-color: rgba(68,103,232, 1);" class="input-group-text btn btn-warning cari-nik" id="basic-addon2">
                                                                <i class="fa fa-search fs-2x"></i> Cari
                                                            </button>
                                                            {{-- <span class="input-group-text" id="basic-addon2"></span> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    
                </div>
            </div>

            <div class="content-sec bg-grey">
                <!-- start of contact section -->
                <div id="contact" class="contact-section section" tabindex="-1">
                    <div class="container mb-3">
                        <div class="container px-0 ">
                            <div class="header-content px-3 py-5 pt-md-5 pb-md-4 mx-auto text-center">
                                <h2>Layanan & Informasi Kejaksaan Republik Indonesia</h2>
                                <p class="sub-title">
                                    Layanan Interaksi Dan Komunikasi Masyarakat Yang Disediakan Oleh
                                    Kejaksaan Republik Indonesia
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="container px-md-0 px-3">
                        <div class="section-content row">
                            <div class="col-lg-8">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a href="?type=layanan" class="fw-bolder nav-link active" id="layanan-tab" data-bs-toggle="tab" data-bs-target="#home" role="tab" aria-controls="home" aria-selected="true">
                                            Layanan
                                        </a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a href="?type=informasi" class="fw-bolder nav-link" id="informasi-tab" data-bs-toggle="tab" data-bs-target="#profile" role="tab" aria-controls="profile" aria-selected="false">
                                            Informasi
                                        </a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a href="?type=kegiatan" class="fw-bolder nav-link" id="kegiatan-tab" data-bs-toggle="tab" data-bs-target="#kegiatan" role="tab" aria-controls="profile" aria-selected="false">
                                            Kegiatan
                                        </a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a href="?type=pengaduan" class="fw-bolder nav-link" id="pengaduan-tab" data-bs-toggle="tab" data-bs-target="#pengaduan" role="tab" aria-controls="pengaduan" aria-selected="false">
                                            Pengaduan
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="layanan-tab">
                                        <div class="section-content row">
                                            <div id="portfolio" class="portfolio-section section" tabindex="-1">
                                                <div class="container">
                                                    <div class="section-content">
                                                        <!-- portfolio items -->
                                                        <div class="row justify-content-center" ref="portfolioItems"
                                                            data-no-more-works-msg="No more to load">
                                                            <!-- start items list -->
                                                            <div class="col-md-4 col-sm-6 col-xs-12 my-3 portfolio-item"
                                                                v-for="work in portfolioItems" :key="work.title.en"
                                                                v-show="worksFilter === 'all' || worksFilter === work.category.slug">
                                                                <a :href="work.url+'&uuid={{request()->uuid}}'">
                                                                    {{-- :target="work.isBlank ? '_blank' : ''" rel="noopener"> --}}
                                                                    <div class="item-img">
                                                                        <img :src="work.imgUrl" :alt="work.title.en"
                                                                            style="width: 100%; height: 210px;  border-radius: 0.63rem;">
                                                                    </div>
                                                                    <div class="item-details">
                                                                        <h3 class="title">@{{ work.title.en }}</h3>
                                                                        {{-- <div class="desc">@{{ work.desc.en }}</div> --}}
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <!-- end items list -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="informasi-tab">
                                        <div class="section-content row">
                                            <div id="portfolio" class="portfolio-section section" tabindex="-1">
                                                <div class="container">
                                                    <div class="section-content">
                                                        <!-- portfolio items -->
                                                        <template v-if="!reRender">
                                                            <div class="row justify-content-center" ref="portfolioItems"
                                                                data-no-more-works-msg="No more to load">
                                                                <!-- start items list -->
                                                                <div class="col-md-4 col-sm-6 col-xs-12 my-3 portfolio-item"
                                                                    v-for="work in portfolioItems" :key="imgUrl"
                                                                    v-show="worksFilter === 'all' || worksFilter === work.category.slug">
                                                                    <a :href="work.url+'&uuid={{request()->uuid}}'">
                                                                        {{-- :target="work.isBlank ? '_blank' : ''" rel="noopener"> --}}
                                                                        <div class="item-img">
                                                                            <img :src="work.imgUrl" :alt="work.title.en"
                                                                                style="width: 100%; height: 210px;  border-radius: 0.63rem;">
                                                                        </div>
                                                                        <div class="item-details">
                                                                            <h3 class="title">@{{ work.title.en }}</h3>
                                                                            {{-- <div class="desc">@{{ work.desc.en }}</div> --}}
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <!-- end items list -->
                                                            </div>
                                                        </template>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
                                        <div class="section-content row">
                                            <div id="portfolio" class="portfolio-section section" tabindex="-1">
                                                <div class="container">
                                                    <div class="section-content">
                                                        <!-- portfolio items -->
                                                        <template v-if="!reRender">
                                                            <div class="row justify-content-center" ref="portfolioItems"
                                                                data-no-more-works-msg="No more to load">
                                                                @foreach($kegiatan as $items)
                                                                    <div class="col-md-4 col-sm-6 col-xs-12 my-3 portfolio-item">
                                                                        <a href="{{url('kegiatan/'.$items->slug)}}">
                                                                            <div class="item-img">
                                                                                <img src="{{asset('landingpage/images/listmenu/'.$items->foto)}}" alt="{{ucwords($items->judul_kegiatan)}}"
                                                                                    style="width: 100%; height: 210px;  border-radius: 0.63rem;">
                                                                            </div>
                                                                            <div class="item-details">
                                                                                <h3 class="title">{{ucwords($items->judul_kegiatan)}}</h3>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                @endforeach
                                                                <!-- end items list -->
                                                            </div>
                                                        </template>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pengaduan" role="tabpanel" aria-labelledby="pengaduan-tab">
                                        <div class="section-content row">
                                            <div id="portfolio" class="portfolio-section section" tabindex="-1">
                                                <div class="container">
                                                    <div class="section-content">
                                                        <!-- portfolio items -->
                                                        <template v-if="!reRender">
                                                            <div class="row justify-content-center" ref="portfolioItems"
                                                                data-no-more-works-msg="No more to load">
                                                                <!-- start items list -->
                                                                <div class="col-md-4 col-sm-6 col-xs-12 my-3 portfolio-item"
                                                                    v-for="work in portfolioItems" :key="imgUrl"
                                                                    v-show="worksFilter === 'all' || worksFilter === work.category.slug">
                                                                    <a :href="work.url+'&uuid={{request()->uuid}}'">
                                                                        {{-- :target="work.isBlank ? '_blank' : ''" rel="noopener"> --}}
                                                                        <div class="item-img">
                                                                            <img :src="work.imgUrl" :alt="work.title.en"
                                                                                style="width: 100%; height: 210px;  border-radius: 0.63rem;">
                                                                        </div>
                                                                        <div class="item-details">
                                                                            <h3 class="title">@{{ work.title.en }}</h3>
                                                                            {{-- <div class="desc">@{{ work.desc.en }}</div> --}}
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <!-- end items list -->
                                                            </div>
                                                        </template>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- text box -->
                            <div class="col-lg-4">
                                <div id="contactus-discuss">
                                    <div class="header-content px-0 py-5 pt-md-5 pb-md-4 text-left">
                                        <h1>FAQ</h1>
                                        <p class="sub-title">
                                            Anda dapat melihat keseluruhan pertanyaan dan anda dapat mengajukan pertanyaan melalui fitur dibawah ini
                                        </p>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="container-chat">
                                                @if (count($ruangDiskusi) > 0)
                                                    <div class="content-chat">
                                                        @foreach ($ruangDiskusi as $item)
                                                            <div class="identity-chat">
                                                                <strong>{{ucwords($item->name)}}</strong>, <span>{{ucwords($item->email)}}</span>
                                                                <div><small class="muted-text">{{date('d F Y, H:i', strtotime($item->created_at))}}</small></div>
                                                            </div>
                                                            <div class="body-chat fw-bold">{{ucwords($item->message)}}</div>
                                                            <div class="content-answer">
                                                                <div class="identity-chat">
                                                                    <strong>Admin Kejaksaan</strong>
                                                                    <div><small class="muted-text">{{date('d F Y, H:i', strtotime($item->created_at))}}</small></div>
                                                                </div>
                                                                <div class="body-chat fw-bold">
                                                                    {{ucwords($item->answer)}}
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        @endforeach
                                                    </div>
                                                @endif
                                                <div class="gap-2 mt-5">
                                                    <button style="background-color: rgba(68,103,232, 1);" class="btn btn-primary full-block mt-5 fw-bolder tanya-diskusi" >
                                                        Tanya Sesuatu ?
                                                    </button>
                                                    {{-- <a href="{{route('login')}}" class="btn btn-primary full-block mt-5 fw-bolder">Tanya Sesuatu ?</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-5">
                                    <div class="header-content px-0 py-5 pt-md-5 pb-md-4 text-left">
                                        <h1>Link Sistem Terkait</h1>
                                        <p class="sub-title">
                                            Berikut ini adalah sistem terkait yang berhubungan dengan PTSP, masyarakat dapat melakukan klik pada gambar dibawah ini untuk mengetahui informasi lebih lanjut
                                        </p>
                                    </div>
                                    <div class="swiperexternal swiper">
                                        <!-- Additional required wrapper -->
                                        
                                        <div class="swiper-wrapper">
                                            @foreach($slidersamping as $slidersamping)
                                            <div class="swiper-slide">
                                                <div class="">
                                                    <a href="{{url($slidersamping->url_slider)}}"><img class="p-0" src="{{ asset($slidersamping->gambar) }}" alt="Gambar"></a>
                                                    {{-- <img src="{{ asset('landingpage/images/listmenu/MENU-03.jpg') }}" alt="User Name"> --}}
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="swiper-pagination-external" style="text-align: center"></div>
                                    </div>
                                </div>
                                {{-- <div class="mt-5"> --}}
                                    {{-- <div class="header-content px-0 py-5 pt-md-5 pb-md-4 text-left">
                                        <h1>Link Sistem Internal</h1>
                                        <p class="sub-title">
                                            Berikut ini adalah sistem internal yang berhubungan dengan PTSP
                                        </p>
                                    </div>
                                    <div class="swiperinternal swiper">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <a target="_blank" href="http://survey.kejaksaan.go.id">
                                                    <h4 class="clients-icon-text">E-Survey</h4>
                                                </a>
                                            </div>
                                            <div class="swiper-slide">
                                                <a target="_blank" href="https://bukutamu.kejaksaan.go.id">
                                                    <h4 class="clients-icon-text">BukuTamu</h4>
                                                </a>
                                            </div>
                                            <div class="swiper-slide">
                                                <a target="_blank" href="https://sipede.kejaksaan.go.id">
                                                    <h4 class="clients-icon-text">Sipede</h4>
                                                </a>
                                            </div>
                                            <div class="swiper-slide">
                                                <a target="_blank" href="http://43.231.129.18/">
                                                    <h4 class="clients-icon-text">E-Kinerja</h4>
                                                </a>
                                            </div>
                                            <div class="swiper-slide">
                                                <a target="_blank" href="https://sicana.kejaksaan.go.id">
                                                    <h4 class="clients-icon-text">Sicana</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div> --}}
                                {{-- </div> --}}
                            </div>
                            <div class="col-lg-12 text-center">
                                <a href="https://t.me/ruangberbagi29_bot"><img style = "box-shadow: 5px 5px 5px #333; height:auto; width:100%; margin:auto; max-height:150px;" src="{{asset('landingpage/images/listmenu/rdiskusi.png')}}"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of contact section -->
            </div>
            <div class="content-sec">
                <div class="position-relative" id="title-single-page">
                    <div class="container mb-3">
                        <div class="container px-0 ">
                            <div class="header-content px-3 py-5 pt-md-5 pb-md-4 mx-auto text-center">
                                <h1 class="title">BERITA.</h1>
                                <p class="sub-title">
                                    Berbagai daftar berita kegiatan yang dilakukan.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="section-content row">
                        {{-- <div class="container mb-3">
                            <div class="container px-0 ">
                                <ul class="nav nav-tabs" id="tabBerita" role="tablist">
                                    {{-- <li class="nav-item" role="presentation">
                                        <a href="?type=beritaNasonal" class="fw-bolder nav-link active" id="berita-nasional" data-bs-toggle="tab" data-bs-target="#beritaNasional" role="tab" aria-controls="profile" aria-selected="true">
                                            Berita Nasional
                                        </a>
                                    </li> --}}
                                    {{-- <li class="nav-item" role="presentation">
                                        <a href="?type=beritaDaerah" class="fw-bolder nav-link active" id="berita-daerah" data-bs-toggle="tab" data-bs-target="#beritaDaerah" role="tab" aria-controls="profile" aria-selected="true">
                                            Berita Keseluruhan Satuan Kerja
                                        </a>
                                    </li>
                                    @if($datatamu != null)
                                        <li class="nav-item" role="presentation">
                                            <a href="?type=beritaSatker" class="fw-bolder nav-link" id="berita-satker" data-bs-toggle="tab" data-bs-target="#beritaSatker" role="tab" aria-controls="profile" aria-selected="true">
                                                Berita {{ucwords(strtolower($satker->nama_satker))}}
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-12 text-center"> --}} --}}
                            <div class="tab-content" id="tabBeritaContent">
                                {{-- <div class="tab-pane fade show active" id="beritaNasional" role="tabpanel" aria-labelledby="berita-nasional">
                                    <div class="px-md-5 pb-md-5 pt-0 d-flex align-items-center">
                                        <div class="container col-md-11 px-md-0 list-card-news">
                                            <div class="row">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="swipernews swiper">
                                                            <!-- Additional required wrapper -->
                                                            <div class="swiper-wrapper">
                                                                @forelse($berita as $berita)
                                                                    <div class="swiper-slide full-height">
                                                                        <div class="card">
                                                                            <div class="card-image">
                                                                                <a href="{{url('layanan/permohonan/berita/'.$berita['id'])}}">
                                                                                    <img class="img-gallery" src="{{$berita['cover']}}" alt="...">
                                                                                </a>
                                                                            </div>
                                                                            <div class="card-body">
                                                                                <a href="{{url('layanan/permohonan/berita/'.$berita['id'])}}">
                                                                                    <p class="card-text text-align-left">
                                                                                        <small class="">{{$berita['title']}}</small>
                                                                                    </p>
                                                                                    <small class="text-muted text-align-left"><i class="fa fa-calendar"></i>&nbsp;{{$berita['created_at']}}</small>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @empty
                                                                SERVER SEDANG MAINTENANCE
                                                                @endforelse
                                                            </div>
                                                            <div class="swiper-pagination-news"></div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                {{-- <div class="tab-pane fade show active" id="beritaDaerah" role="tabpanel" aria-labelledby="berita-daerah"> --}}
                                    <div class="px-md-5 pb-md-5 pt-0 d-flex align-items-center">
                                        <div class="container col-md-11 list-card-news">
                                            <div class="row">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="swipernews2 swiper">
                                                            <!-- Additional required wrapper -->
                                                            <div class="swiper-wrapper">
                                                                @forelse($beritadaerahs as $berita)
                                                                    <div class="swiper-slide">
                                                                        <div class="card">
                                                                            <div class="card-image">
                                                                                <a href="{{url('beritadaerah/'.$berita->id)}}">
                                                                                    <img class="img-gallery" src="{{asset($berita->foto)}}" alt="..."></a>
                                                                            </div>
                                                                            <div class="card-body">
                                                                                <a href="{{url('beritadaerah/'.$berita->id)}}">

                                                                                    <p class="card-text text-align-left">
                                                                                        <h6 class="text-align-left"><strong>{{$berita['judul_berita']}}</strong></h6>
                                                                                    </p>
                                                                                    <br>
                                                                                    <small class="text-muted text-align-left mt-4">
                                                                                        <i class="fa fa-calendar"></i>&nbsp;
                                                                                        {{tgl_indo($berita['tanggal'])}}
                                                                                    </small>
                                                                                    <small class="text-muted text-align-left mt-1 d-flex">
                                                                                        <i class="fa fa-map-marker me-2"></i>
                                                                                        {{$berita['nama_satker']}}
                                                                                    </small>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @empty
                                                                @endforelse
                                                            </div>
                                                            <div class="swiper-pagination-news2"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="{{url('layanan/permohonan/create?idLayanan=32')}}" class="btn btn-primary btn-block">BERITA SELENGKAPNYA</a>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                {{-- </div> --}}
                                {{-- @if($datatamu != null)
                                    <div class="tab-pane fade show" id="beritaSatker" role="tabpanel" aria-labelledby="berita-satker">
                                        <div class="px-md-5 pb-md-5 pt-0 d-flex align-items-center">
                                            <div class="container col-md-11 list-card-news">
                                                <div class="row">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="swipernews3 swiper">
                                                                <!-- Additional required wrapper -->
                                                                <div class="swiper-wrapper">
                                                                    @forelse($beritasatker as $berita)
                                                                        <div class="swiper-slide">
                                                                            <div class="card">
                                                                                <div class="card-image">
                                                                                    <a href="{{url('beritadaerah/'.$berita->id)}}">
                                                                                        <img class="img-gallery" src="{{asset($berita->foto)}}" alt="...">
                                                                                    </a>
                                                                                </div>
                                                                                
                                                                                <div class="card-body">
                                                                                    <a href="{{url('beritadaerah/'.$berita->id)}}">
                                                                                        <p class="card-text text-align-left">
                                                                                            <h6 class="text-align-left"><strong>{{$berita['judul_berita']}}</strong></h6>
                                                                                        </p>
                                                                                        <br>
                                                                                        <small class="text-muted text-align-left mt-4"><i class="fa fa-calendar"></i>&nbsp;{{tgl_indo($berita['tanggal'])}}</small>
                                                                                        <small class="text-muted text-align-left mt-1 d-flex">
                                                                                            <i class="fa fa-map-marker me-2"></i>
                                                                                            {{$berita['nama_satker']}}
                                                                                        </small>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @empty
                                                                    @endforelse
                                                                </div>
                                                                <div class="swiper-pagination-news3"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif --}}
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-sec">
                <div class="position-relative" id="title-single-page">
                    <div class="container mb-3">
                        <div class="container px-0 ">
                            <div class="header-content px-3 py-5 pt-md-5 pb-md-4 mx-auto text-center">
                                <h1 class="title">INFORMASI LELANG BARANG SITAAN</h1>
                                <p class="sub-title">
                                    Berbagai informasi barang sitaan yang dilelang.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="px-md-5 pb-md-5 pt-0 d-flex align-items-center">
                    <div class="container col-md-11 list-card-news">
                        <div class="row">
                            <div class="container">
                                <center>
                                    <div class="row">
                                        @forelse($lelang as $l)
                                        <div class="col-lg-3 col-md-4 col-sm-6">
                                            <div class="card card-news">
                                                <div class="img-tgl">
                                                    <a href="" id="editCompany" data-toggle="modal" data-target='{{'#practice_modal'.$l->id}}' data-id="{{ $l->id }}">
                                                        <img src="{{asset($l->foto)}}" alt="...">
                                                    </a>
                                                    @if($l->status_laku == '1')
                                                        <div class="tag-tanggal orange">
                                                            BELUM TERLELANG
                                                        </div>
                                                    @else 
                                                        <div class="tag-tanggal green">
                                                            SUDAH TERLELANG
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="card-body">
                                                    <a href="" id="editCompany" data-toggle="modal" data-target='{{'#practice_modal'.$l->id}}' data-id="{{ $l->id }}">
                                                        <p class="card-text text-align-left">
                                                            <h6 class="text-align-left">{{$l->nama_barang_lelang}}</h6>
                                                            <h2 class="text-align-left"><strong>Rp.{{$l->harga_barang}}</strong></h2>
                                                        </p>
                                                        @if($l->tanggal_lelang !== null)
                                                            <small class="text-muted text-align-left d-flex align-items-flex-start">
                                                                <i class="fa fa-calendar me-2 min-width-15 text-center"></i>{{tgl_indo2($l->tanggal_lelang)}}</small>
                                                        @else
                                                        <small class="text-muted text-align-left">&nbsp;</small>
                                                        @endif
                                                        <small class="text-muted text-align-left d-flex align-items-flex-start">
                                                            <i class="fa fa-map-marker me-2 min-width-15 text-center"></i>
                                                            {{$l->nama_satker}}</small>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        @empty
                                        SERVER SEDANG MAINTENANCE
                                        @endforelse
                                    </div>
                                </center>
                            </div>
                        </div>
                        <center><a href="{{url('layanan/permohonan/lelang')}}" class="btn btn-primary btn-block mt-3">INFORMASI LELANG SELENGKAPNYA</a></center>
                    </div>
                </div>
            </div>
            <!-- MODAL LELANG -->
            @foreach($lelang as $l)
                <div class="modal fade" id="{{'practice_modal'.$l->id}}">
                    <div class="modal-dialog">
                    <form id="companydata">
                            <div class="modal-content">
                                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-close"></i>
                                </div>
                                <div class="img-tgl">
                                    <img src="{{asset($l->foto)}}" id="foto" width="200" height="200">
                                </div>
                                <div class="modal-body">
                                    <center>
                                        <h2 class="text-align-left">{{$l->nama_barang_lelang}}</h2>
                                        <div class="text-align-left">
                                            <p class="text-align-left">{!! $l->deskripsi_barang !!}</p>
                                        </div>
                                        <h1 class="text-align-left"> Rp.{{$l->harga_barang}}</h1>
                                        <br>&nbsp;
                                        @if($l->status_laku == 1)
                                        <div class="p-3 tag-purchase-tanggal orange">
                                            <h2 style="color: #fff">Belum Terlelang</h2>
                                        </div>
                                        @else
                                        <div class="p-3 tag-purchase-tanggal green">
                                            <h2 style="color: #fff">Sudah Terlelang</h2>
                                            <p>Tanggal Lelang : {{ tgl_indo2($l->tanggal_lelang) }}</p>
                                        </div>
                                        @endif
                                        {{-- @if($l->status_laku == 1)
                                        <h2>Belum Terlelang</h2>
                                        @else
                                        <h2>Sudah Terlelang</h2>
                                        @endif --}}
                                        @if(!empty($l->link_lelang))
                                        <p>Link Lelang : <a href="{{$l->link_lelang}}"><u>{{$l->link_lelang}}</u></a></p>
                                        @endif
                                        <p><i class="fa fa-map-marker me-2"></i>&nbsp;{{ $l->nama_satker }}</p>
                                    </center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endforeach
            <div class="content-sec">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <center>
                            <h1 class="title">Grafik Permohonan Pelayanan</h1>
                            <div class="card">
                                <div class="chart-area">
                                    <canvas id="layananLayananChart"></canvas>
                                </div>
                            </div>
                        </center>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <center>
                            <h1 class="title">Grafik Laporan Pengaduan</h1>
                            <div class="card">
                                <div class="chart-area">
                                    <canvas id="layananPengaduanChart"></canvas>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
            <div class="content-sec bg-footer">
                <div class="container px-md-0 px-3">
                    <div id="contact" class="contact-section" tabindex="-1">
                        <div class="section-content row">
                            <div class="contact-text">
                                <div class="row contact-info">
                                    <div class="col-md-6">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.175136803006!2d106.79499561486645!3d-6.24063446284725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f13f6aaaaaab%3A0x38b77dea508d4f3f!2sKejaksaan%20Agung%20Republik%20Indonesia!5e0!3m2!1sid!2sid!4v1661748631490!5m2!1sid!2sid%22" width="380" height="250" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" style="border: 0px;border-radius: 16px;"></iframe>
                                    </div>
                                    <div class="col-md-3">
                                        <h3 class="mb-5">
                                            KONTAK
                                        </h3>
                                        <div class="d-flex align-items-start my-5">
                                            <i class="fa fa-map-marker fa-lg me-5" aria-hidden="true"></i>
                                            <div>
                                                Jl. Sultan Hasanuddin No.1 Kebayoran Baru Jakarta Selatan -
                                                Indonesia
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-start my-5">
                                            <i class="fa fa-envelope fa-lg me-4" aria-hidden="true"></i>
                                            <a href="mailto:humas.puspenkum@kejaksaan.go.id" class="contact-text">humas.puspenkum@kejaksaan.go.id</a>
                                        </div>
                                        <div class="d-flex align-items-start my-5">
                                            <i class="fa fa-phone fa-lg me-4" aria-hidden="true"></i>
                                            <a style="text-decoration: none;" class="contact-text" href="tel:+62217221269">+62217221269</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="text-box">
                                            <h3>
                                                Apakah anda memiliki masalah?
                                                <br>
                                                PUSAT PENERANGAN HUKUM
                                                <br>
                                                KEJAKSAAN AGUNG R.I
                                            </h3>
                                            <p></p>
                                        </div>
                                        <div class="d-flex justify-content-start mt-5">
                                            <ul class="social">
                                                <li>
                                                    <a href="https://www.facebook.com/Kejaksaan.Republik.Indonesia" target="_blank" rel="noreferrer"
                                                        v-tooltip="{text: 'Facebook', dir: 'top'}">
                                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://twitter.com/Kejaksaan_RI" target="_blank" rel="noreferrer"
                                                        v-tooltip="{text: 'Twitter', dir: 'top'}">
                                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.youtube.com/channel/UCy9IA8V2Wo2pxHHXDVVtuhA" target="_blank" rel="noreferrer"
                                                        v-tooltip="{text: 'YouTube', dir: 'top'}">
                                                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- start of footer -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 mx-auto text-center">
                            &copy;
                            copyright
                            <a href="#" target="_blank" rel="noopener"> Kejaksaan Republik Indonesia</a>
                        </div>
                    </div>
                </div>
            </footer>
            {{-- <div id="visitor-counter" style="position: fixed; right: 0px; bottom: 100px;">
                <script type="text/javascript" src="https://free-hit-counters.net/count/9dhu"></script><br>
                <a id="private-krankenvers" href='http://www.versicherungen.at/private-krankenversicherung/'>private Krankenversicherung</a>
                <script type='text/javascript' src='https://www.whomania.com/ctr?id=18abb0801b9a40501edc9ce38b124d35a220bb5f'></script>
            </div> --}}
            <!-- end of footer -->

            <!-- scroll to top button -->
            {{-- <button ref="scrollTopBtn" :class="['scroll-to-top', isScrollTopBtnDisplayed && 'show-scrollTop']"
                title="Back To Top" tabindex="0" @click="scrollToTop">
                <i class="fa fa-angle-up" aria-hidden="true"></i>
            </button> --}}

            <!-- toast notifications -->
            <ul class="notifications-container">
                <transition-group name="notify">
                    <li v-for="notify of notifications" :key="notify.id" :id="notify.id"
                        :class="['notification', 'show', notify.className, notify.time && 'timer']">
                        @{{ notify.msg }}
                        <i class="fa fa-times link-hover" aria-hidden="true" @click="dismissNotify(notify.id)"></i>
                        <span v-if="notify.time" class="disappearing-time"
                            :style="{ 'animation-duration': notify.time + 'ms' }"></span>
                    </li>
                </transition-group>
            </ul>

            <!-- ajax loading -->
            {{-- <div v-show="isAjaxLoading" class="ajax-loading">
                <span></span>
            </div> --}}
        </div>
        @if($sliderbawah != null)
        <div class="marquee">
            <div class="data-marquee">
                @foreach($sliderbawah as $sliderbawah)
                    <span class="labelmarquee" data-marquee="{{$sliderbawah->judul_slider}}">{{$sliderbawah->judul_slider}}</span>
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                @endforeach
            </div>
        </div>
        @endif
        <div class="modal fade" tabindex="-1" id="modal-container-diskusi" aria-modal="true" role="dialog">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Ruang diskusi untuk pelayanan Kejaksaan RI </h5>
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <span class="svg-icon svg-icon-2x">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                                    <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form
                            ref="contactForm" class="contact-form form-styled"
                            action="{{ route('send-email') }}"
                            data-success-msg="Message sent successfully!"
                            data-err-msg="Oops! something went wrong, please try again."
                        >
                            @csrf
                            <div class="group">
                                <label>Nama</label>
                                <div class="control has-prefix-icon">
                                    <input type="text" name="name" placeholder="Silahkan isi nama anda"
                                        minlength="3" required>
                                    <i class="fa fa-user-circle prefix-icon" aria-hidden="true"></i>
    
                                    <!-- validation errors messages -->
                                    <div class="errors-msgs">
                                        <input class="required" type="hidden" value="Name is required">
                                        <input class="minLength" type="hidden"
                                            value="Name must be at least 3 characters">
                                    </div>
                                </div>
                            </div>
                            <div class="group">
                                <label>Email</label>
                                <div class="control has-prefix-icon">
                                    <input class="ltr-dir" type="email" inputmode="email" name="email"
                                        placeholder="Silahkan isi email anda" required>
                                    <i class="fa fa-envelope prefix-icon" aria-hidden="true"></i>
    
                                    <!-- validation errors messages -->
                                    <div class="errors-msgs">
                                        <input class="required" type="hidden" value="Email is required">
                                        <input class="invalid" type="hidden"
                                            value="Please enter a valid email address">
                                    </div>
                                </div>
                            </div>
                            <div class="group phone-number">
                                <label>Phone <span class="optional">(Optional)</span></label>
                                <div class="control has-prefix-icon">
                                    <input type="tel" inputmode="tel" name="phone"
                                        placeholder="Silahkan isi no telp anda">
                                    <i class="fa fa-phone prefix-icon" aria-hidden="true"></i>
    
                                    <!-- validation errors messages -->
                                    <div class="errors-msgs">
                                        <input class="invalid" type="hidden"
                                            value="Please enter a valid Phone Number">
                                    </div>
                                </div>
                            </div>
                            <div class="group">
                                <label>Message</label>
                                <div class="control has-prefix-icon">
                                    <textarea name="message" placeholder="Tulis pesan anda..." required></textarea>
                                    <i class="fa fa-comments prefix-icon" aria-hidden="true"></i>
    
                                    <!-- validation errors messages -->
                                    <div class="errors-msgs">
                                        <input class="required" type="hidden" value="Message is required">
                                    </div>
                                </div>
                            </div>
                            <div class="group">
                                <div class="control">
                                    <button class="submit-btn btn btn-primary full-block fw-bolder" type="button"
                                        @click="contactFormValidation">Send &nbsp;
                                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-container-custom"></div>
    <!-- js plugins file -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.4.1/chart.min.js" integrity="sha512-5vwN8yor2fFT9pgPS9p9R7AszYaNn0LkQElTXIsZFCL7ucT8zDCAqlQXDdaqgA1mZP47hdvztBMsIoFxq/FyyQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('landingpage/scripts/plugins.min.js') }}"></script>
    {{-- <script src="{{ asset('demo1/js/custom/widgets.js') }}"></script> --}}
    {{-- <script src="{{ asset('demo1/plugins/global/plugins.bundle.js') }}"></script> --}}
    {{-- <script type="module" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.1.1/chart.min.js"></script> --}}
    

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- main script file -->
    <script>
        let imageList = JSON.parse("{{$images}}".replace(/&quot;/g,'"'));
        let loading = false;
    </script>
    <script src="{{ asset('landingpage/scripts/main.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <!-- unused for landing page -->
    <script src="{{url('/')}}/demo1/plugins/custom/datatables/datatables.bundle.js"></script>
    <script type="module">
        
        $(document).ready(function(){

            $('body').on('click', '#editCompany', function (event) {
                        event.preventDefault();
                        var id = $(this).data('id');
                        $.get('layanan/permohonan/lelang/lelang/detail/' + id, function (data) {
                            console.log(data);
                            $('#practice_modal' + id).modal('show');
                            
                        })
                    });

            var pengaduan = {!! json_encode($hitung_data_pengaduan_perbulan) !!};
            console.log(pengaduan,'data pengaduan');
            // 
            var arrPengaduan = [];
            for(var i in pengaduan.original){
                arrPengaduan.push(pengaduan.original[i].count)
            }
            // 
            var laypengaduan = document.getElementById("layananPengaduanChart");
            var pengaduanChart = new Chart(laypengaduan, {
            type: 'line',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                label: "Jumlah",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: arrPengaduan,
                }],
            },
            options: {
                maintainAspectRatio: false,
                layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
                },
                scales: {
                xAxes: [{
                    time: {
                    unit: 'date'
                    },
                    gridLines: {
                    display: false,
                    drawBorder: false
                    },
                    ticks: {
                    maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                    maxTicksLimit: 5,
                    padding: 10,
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return '$' + number_format(value);
                    }
                    },
                    gridLines: {
                    color: "rgb(234, 236, 244)",
                    zeroLineColor: "rgb(234, 236, 244)",
                    drawBorder: false,
                    borderDash: [2],
                    zeroLineBorderDash: [2]
                    }
                }],
                },
                legend: {
                display: false
                },
                tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
                    }
                }
                }
            }
            });


            var layanan = {!! json_encode($hitung_data_layanan_perbulan) !!};
            console.log(layanan,'data layanan');
            // 
            var arrLayanan = [];
            for(var i in layanan.original){
                arrLayanan.push(layanan.original[i].count)
            }
            // 
            var layLayanan = document.getElementById("layananLayananChart");
            var layananChart = new Chart(layLayanan, {
            type: 'line',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                label: "Jumlah",
                lineTension: 0.3,
                backgroundColor: "rgba(78, 115, 223, 0.05)",
                borderColor: "rgba(78, 115, 223, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(78, 115, 223, 1)",
                pointBorderColor: "rgba(78, 115, 223, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: arrLayanan,
                }],
            },
            options: {
                maintainAspectRatio: false,
                layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
                },
                scales: {
                xAxes: [{
                    time: {
                    unit: 'date'
                    },
                    gridLines: {
                    display: false,
                    drawBorder: false
                    },
                    ticks: {
                    maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                    maxTicksLimit: 5,
                    padding: 10,
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return '$' + number_format(value);
                    }
                    },
                    gridLines: {
                    color: "rgb(234, 236, 244)",
                    zeroLineColor: "rgb(234, 236, 244)",
                    drawBorder: false,
                    borderDash: [2],
                    zeroLineBorderDash: [2]
                    }
                }],
                },
                legend: {
                display: false
                },
                tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                intersect: false,
                mode: 'index',
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
                    }
                }
                }
            }
            });
            // changeColor();
        });
        $(document).on('click', '.action-custom', function(){
            $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
            $('.action-custom').removeClass('show');

            $(this).addClass('show');
            $(this).parent().find('.menu-sub-dropdown').addClass('show');
        });
        $(document).on('click', function (e) {
            if ($(e.target).closest(".action-custom").length === 0) {
                $('.action-custom').removeClass('show');
                $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
            }
        });
        $(document).on('click', '#changeMode', function(){
            changeColor();
        });
        $(document).on('click', '.cari-nik', function(){
            $('.cari-nik').attr("disabled", true);
            $.get( "{{ route('permohonan.dataModal') }}", {
                // _token: "{{csrf_token()}}",
                nik: $('[name="nik"]').val(),
            })
            .done(function(response) {
                $('#modal-container-custom').html(response);
                $('#kt_modal_1').modal('show');
            })
            .fail(function() {
                $('.cari-nik').attr("disabled", false);
            })
            .always(function() {
                $('.cari-nik').attr("disabled", false);
            });
        });

        $(document).on('click', '.tanya-diskusi', function(){
            $('#modal-container-diskusi').modal('show');
        });
        $(document).on('click', '#layanan-tab', function(){
            console.log('clicked layanan tab');
            loading = true;
            vm.portfolioItems = [];
            $.get( "{{ route('indexlayanan') }}")
            .done(function(response) {
                // console.log('---->', app);
                console.log('====>', app)
                imageList = JSON.parse(response.replace(/&quot;/g,'"'));
                vm.setImages();
                vm.getPortfolioItems();
                // this.$refs.myTable.$forceUpdate()
            })
            .fail(function() {
                console.log('----> false');
            })
        });
        $(document).on('click', '#informasi-tab', function(){
            console.log('clicked informasi tab');
            // loading = true;
            vm.portfolioItems = [];
            $.get( "{{ route('indexinformasi') }}")
            .done(function(response) {
                imageList = JSON.parse(response.replace(/&quot;/g,'"'));
                vm.setImages();
                vm.getPortfolioItems();
                // loading = true;
                // app.$set(app, 'stockTrendingDatas' ,res.data);
            })
            .fail(function() {
                console.log('----> false');
            })
        });
        $(document).on('click', '#pengaduan-tab', function(){
            console.log('clicked pengaduan tab');
            // loading = true;
            vm.portfolioItems = [];
            $.get( "{{ route('indexpengaduan') }}")
            .done(function(response) {
                imageList = JSON.parse(response.replace(/&quot;/g,'"'));
                vm.setImages();
                vm.getPortfolioItems();
                // loading = true;
                // app.$set(app, 'stockTrendingDatas' ,res.data);
            })
            .fail(function() {
                console.log('----> false');
            })
        });
        // var VUEApp = new Vue({
        //     el: '#app',
        //     data() {
        //         return {
        //             allPortfolioItems:[]
        //         }
        //     },
        //     methods: {
        //         setImages(){
        //             this.allPortfolioItems = [];
        //             imageList.forEach(element => {
        //                 this.allPortfolioItems.push({
        //                 url: element.url,
        //                 isBlank: element.is_blank,
        //                 imgUrl: element.image,
        //                 title: { en: element.nama_layanan, ar: 'Ruang' },
        //                 desc: { en: 'Pengaduan Masyarakat', ar: 'Pengaduan Masyarakat' },
        //                 category: { slug: element.menu, name: element.menu.toUpperCase() },
        //                 });
        //             });
        //         },
        //         getPortfolioItems(){

        //         }
        //     }
        // })
    </script>
    <script>
        function changeColor() {
            if ($('#changeMode').attr('class') == 'light_theme') {
                $('#card-search-nik').css("background-color", "#fff");
                $('.contact-text').css("color", "#5f5f5f");
                // $('#private-krankenvers').css("color", "#e6e6e6");
            } else {
                $('#card-search-nik').css("background-color", "#313131");
                $('.contact-text').css("color", "#9f9f9f");
                // $('#private-krankenvers').css("color", "#1f1f1f");
            }
        }

    </script>
    <script>
        window.replainSettings = { id: 'c9f245ab-eb4f-4307-ae7b-00f71e05a50f' };
        (function(u){var s=document.createElement('script');s.async=true;s.src=u;
        var x=document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);
        })('https://widget.replain.cc/dist/client.js');
    </script>
    
    <script type="module">
        var heightwindow = $(window).height();
        var widthWindow = $(window).width();
        var str = $('.labelmarquee').data("marquee");
        if(str != undefined && str != null){
            if(str.length >= 250){
                $('.labelmarquee').css('width', '250%')
            }
        }
        // setTimeout(function(){
        //  }, 2000);
        // setTimeout(() => {
        //     $('#private-krankenvers').css('display', 'none');
        // }, 5000);
        import Swiper from 'https://unpkg.com/swiper@8/swiper-bundle.esm.browser.min.js';
      
        const swiper = new Swiper('.swipermain', {
          // Optional parameters
          direction: 'horizontal',
          loop: true,
          autoplay: {
            delay: 2500,
            disableOnInteraction: false,
            },
      
          // If we need pagination
          pagination: {
            el: '.swiper-pagination',
            clickable: true
          },
      
          // Navigation arrows
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
      
          // And if we need scrollbar
          scrollbar: {
            el: '.swiper-scrollbar',
          },
        });

        const swiperExternal = new Swiper('.swiperexternal', {
          // Optional parameters
          direction: 'horizontal',
          loop: true,
          autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
        //   effect: "fade",
      
          // If we need pagination
          pagination: {
            el: '.swiper-pagination-external',
            clickable: true
          },
      
          // Navigation arrows
          navigation: {
            nextEl: '.swiper-button-next-external',
            prevEl: '.swiper-button-prev-external',
          },
      
          // And if we need scrollbar
          scrollbar: {
            el: '.swiper-scrollbar-external',
          },
        });

        const swiperInternal = new Swiper('.swiperinternal', {
          // Optional parameters
          direction: 'horizontal',
          loop: true,
          autoplay: {
            delay: 2500,
            disableOnInteraction: false,
            },
        //   effect: "fade",
        });

        const swiperNews = new Swiper('.swipernews', {
          // Optional parameters
            direction: 'horizontal',
            loop: true,
            // slidesPerView: slidesPerViewWindow,
            // spaceBetween: spaceBetweenWindow,
            // spaceBetween: 70,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
        //   effect: "fade",
      
          // If we need pagination
          pagination: {
            el: '.swiper-pagination-news',
            clickable: true
          },
      
          // Navigation arrows
          navigation: {
            nextEl: '.swiper-button-next-news',
            prevEl: '.swiper-button-prev-news',
          },
      
          // And if we need scrollbar
          scrollbar: {
            el: '.swiper-scrollbar-news',
          },
          breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
                // spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 2,
                // spaceBetween: 10
            },
            // when window width is >= 640px
            640: {
                slidesPerView: 2,
                // spaceBetween: 10
            },

            720: {
                slidesPerView: 2,
                // spaceBetween: 10
            },

            1024: {
                slidesPerView: 3,
                // spaceBetween: 10
            },

            1280: {
                slidesPerView: 4,
                // spaceBetween: 20
            }
        }
        });

        const swiperNews2 = new Swiper('.swipernews2', {
          // Optional parameters
            direction: 'horizontal',
            loop: true,
            // slidesPerView: slidesPerViewWindow,
            // spaceBetween: spaceBetweenWindow,
            // spaceBetween: 70,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
        //   effect: "fade",
      
          // If we need pagination
          pagination: {
            el: '.swiper-pagination-news2',
            clickable: true
          },
          scrollbar: {
            el: '.swiper-scrollbar-news2',
          },

          breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 2,
            },
            // when window width is >= 640px
            640: {
                slidesPerView: 2,
            },

            720: {
                slidesPerView: 2,
            },

            1024: {
                slidesPerView: 3,
            },

            1280: {
                slidesPerView: 4,
            }
        }
        });
        const swiperNews3 = new Swiper('.swipernews3', {
          // Optional parameters
            direction: 'horizontal',
            loop: true,
            // slidesPerView: slidesPerViewWindow,
            // spaceBetween: spaceBetweenWindow,
            spaceBetween: 70,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
        //   effect: "fade",
      
          // If we need pagination
          pagination: {
            el: '.swiper-pagination-news3',
            clickable: true
          },
          scrollbar: {
            el: '.swiper-scrollbar-news3',
          },

          breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            // when window width is >= 640px
            640: {
                slidesPerView: 2,
                spaceBetween: 20
            },

            720: {
                slidesPerView: 2,
                spaceBetween: 20
            },

            1024: {
                slidesPerView: 3,
                spaceBetween: 30
            },

            1280: {
                slidesPerView: 4,
                spaceBetween: 50
            }
        }
        });

        $('#berita-nasional').on('click', function(){
            swiperNews.autoplay.start();
        });
        $('#berita-daerah').on('click', function(){
            console.log('ini lah diclick',swiperNews2.autoplay)
            setTimeout(() => {
                swiperNews2.autoplay.start();
            }, 1000);
        });
        $('#berita-satker').on('click', function(){
            setTimeout(() => {
                swiperNews3.autoplay.start();
            }, 1000);
        });
    </script>
    <script>
        $(window).on('load', function () {
            $('#loading').addClass("hideloader");
            setTimeout(() => {
                $('#loading').addClass("delete");
            }, 400);
        }) 
      </script>
</body>

</html>
