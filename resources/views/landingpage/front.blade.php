<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pengaduan Masyarakat Adalah Layanan Interaksi Dan Komunikasi Masyarakat Yang Disediakan Oleh Kejaksaan RI Serta Kolaborasi Data Antar Kementrian Lembaga.">
    <meta name="keywords" content="portfolio, agency, business, clean, company, corporate, creative, gallery, rtl, modern, photography, vue, vuejs, responsive, sass">

    <!-- title of the browser tab -->
    <title>Kejaksaan Agung Republik Indonesia</title>

    <!-- favicon -->
    <link rel="icon" href="{{ asset('assets/logo-kejaksaan.png') }}">

    <!-- fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Roboto&family=Saira+Stencil+One&text=Nafie&display=swap">
    <!-- chartist CSS -->

    <!-- css libraries file -->
    <link rel="stylesheet" href="{{ asset('landingpage/styles/libraries.min.css') }}">
    <!--boxicon CDN-->
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <!-- main style file -->
    <link rel="stylesheet" href="{{ asset('landingpage/styles/main.css') }}">

    <link href="{{ url('/') }}/demo1/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/demo1/plugins/global/plugins-custom.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/demo1/css/style.bundle.css" rel="stylesheet" type="text/css" />

    <style>
        .logo-title {
            margin-left: 69px !important;
        }

        .logo-title-header {
            justify-content: center;
        }

        .text-desc {
            font-size: 1.5rem;
        }

        @media (min-width: 992px) {
            .portfolio-section .section-content .row {
                -webkit-column-count: 4;
                -moz-column-count: 4;
                column-count: 4;
            }

            .section .text-box .subtitle:before {
                width: 50px !important;
            }
        }

        @media only screen and (max-width: 400px) {
            .logo-title {
                font-size: 20px !important;
                margin-left: 0 !important;
            }

            .logo-title-header {
                justify-content: start;
            }

            .text-desc {
                font-size: 1rem;
            }
        }

        @media only screen and (max-width: 600px) {
            .logo-title {
                font-size: 24px !important;
                margin-left: 0 !important;
            }

            .text-desc {
                font-size: 1rem;
            }
        }

        .menu-custom {
            background-color: #fff;
            -webkit-box-shadow: 10px 10px 5px 0px rgb(0 0 0 / 75%);
            -moz-box-shadow: 10px 10px 5px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 1px 2px 4px 0px rgb(0 0 0 / 75%);
            text-transform: uppercase;
            font-weight: bold;
            padding: 1rem 3.5rem;
            color: #000;
            margin-left: 10px;
        }

        #card-search-nik {
            background-color: #313131;
            -webkit-box-shadow: 11px 10px 38px rgb(0 0 0 / 38%);
            box-shadow: 11px 10px 38px rgb(0 0 0 / 38%);
            border-radius: 0.63rem;
        }

        .contact-section .section-content .contact-form {
            padding: 25px;
            border-radius: 0.63rem;
            width: 100%;
            float: right;
        }

        check:active+.btn.btn-warning,
        .btn.btn-warning:focus:not(.btn-active),
        .btn.btn-warning:hover:not(.btn-active),
        .btn.btn-warning:active:not(.btn-active),
        .btn.btn-warning.active,
        .btn.btn-warning.show,
        .show>.btn.btn-warning {
            background-color: rgb(177 107 60) !important;
        }

        ::-moz-selection {
            background-color: #e38443;
        }

        ::selection {
            background-color: #e38443;
        }

        .about-section .section-content>* {
            margin-bottom: 0px;
        }

        .contact-section .section-content {
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: normal;
            margin-bottom: -50px;
        }

        #container-chat {
            font-family: "Poppins", sans-serif;
            padding: 25px;
            border-radius: 0.63rem;
            margin-bottom: 20px;
            background-color: #313131;
            -webkit-box-shadow: 11px 10px 38px rgb(0 0 0 / 38%);
            box-shadow: 11px 10px 38px rgb(0 0 0 / 38%);
            color: #c9c9c9;
        }

        .body-chat {
            margin-bottom: 15px;
            font-style: italic;
        }

        .identity-chat span {
            font-style: italic;
        }

        .identity-chat {
            margin-bottom: 10px;
        }

        .content-chat {
            padding: 16px;
            background-color: #292929;
            border-radius: 0.63rem;
            font-weight: bold;
            max-height: 400px;
            overflow: scroll;
        }

        .content-answer {
            margin-left: 40px;
        }

    </style>
</head>

<body style="position: relative;">
    <div id="app">
        <div id="app-inner" ref="appRef" :class="['light_theme', {'enable-focus-style': isAnyFocus}]" @click="isAnyFocus = false" @keyup.tab="isAnyFocus = true">

            <div class="circle-cursor circle-cursor-outer" ref="circleCursorOuter"></div>
            <div class="circle-cursor circle-cursor-inner" ref="circleCursorInner"></div>

            <!-- start preloader-->
            <transition name="preloader">
                <div class="preloader" v-if="isPreloading">
                    <div class="circle">
                        <div class="double-bounce1"></div>
                        <div class="double-bounce2"></div>
                    </div>
                </div>
            </transition>
            <!-- end preloader-->

            <!-- start of header -->
            <header :class="{
                'big-header': isHeaderBig,
                'small-header': !isHeaderBig,
                'header-hidden': isHeaderHidden
              }">
                <div class="container">
                    <div class="d-flex w-100 logo-title-header">
                        <div class="logo" title="kejaksaan republik indonesia">
                            <h1 class="logo-title">Kejaksaan Republik Indonesia</h1>
                        </div>
                    </div>

                    <ul class="options-icons">
                        {{-- <li class="theme-switcher">
                            <button :class="savedTheme" id="changeMode" title="Change Mode"
                                @click="changeAppTheme"></button>
                        </li> --}}
                        @if (!auth()->user())
                            <li>
                                <a href="{{ route('login') }}" class="btn btn-warning btn-sm">Login</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </header>
            <!-- end of header -->

            <!-- start of hero section -->
            <div id="hero" class="hero-section" ref="heroSection">
                <div class="hero-img">
                    <div class="layer" v-clone>
                        <img src="{{ asset('landingpage/images/hero-img.png') }}" alt="User Name">
                    </div>
                </div>

                <div class="hero-text">
                    <h2>
                        Layanan & Informasi Kejaksaan Tinggi Sumatera Utara
                        <br>
                        <span class="text-desc">Layanan Interaksi Dan Komunikasi Masyarakat Yang Disediakan Oleh
                            Kejaksaan Tinggi Sumatera Utara</span>
                    </h2>
                    <ul class="row" ref="portfolioItems" data-no-more-works-msg="No more to load">
                        <li>
                            <div class="text-box">
                                <div class="works-filter"><a href="#portfolio"><button class="" onclick="location.href='{{ route('satker') }}';">
                                            <div class="menu-custom">Layanan</div>
                                        </button></a><a href="#portfolio"><button class="" onclick="location.href='{{ route('informasi') }}';">
                                            <div class="menu-custom">Informasi</div>
                                        </button></a></div>
                            </div>
                        </li>
                    </ul>
                </div>

                {{-- <div class="social">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/Kejaksaan.Republik.Indonesia" target="_blank" rel="noreferrer" v-tooltip="{text: 'Facebook', dir: 'right'}">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/Kejaksaan_RI" target="_blank" rel="noreferrer" v-tooltip="{text: 'Twitter', dir: 'right'}">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCy9IA8V2Wo2pxHHXDVVtuhA" target="_blank" rel="noreferrer" v-tooltip="{text: 'Youtube', dir: 'right'}">
                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div> --}}

                <div class="scroll-down">
                    <a href="#about" class="contact-text" title="Scroll Down">Scroll</a>
                </div>
            </div>
            <!-- end of hero section -->

            <!-- start of about section -->
            <div id="about" class="about-section section" tabindex="-1">
                <div class="container">
                    <div class="section-content row">
                        <div id="portfolio" class="portfolio-section section" tabindex="-1">
                            <div class="container">
                                <div class="section-content">
                                    <!-- portfolio items -->
                                    <div class="row justify-content-center" ref="portfolioItems" data-no-more-works-msg="No more to load">
                                        <!-- start items list -->
                                        <div class="col-md-3 col-sm-6 col-xs-12 my-3 portfolio-item" v-for="work in portfolioItems" :key="work.title.en" v-show="worksFilter === 'all' || worksFilter === work.category.slug">
                                            <a :href="work.url" :target="work.isBlank ? '_blank' : ''" rel="noopener">
                                                <div class="item-img">
                                                    <img :src="work.imgUrl" :alt="work.title.en" style="width: 100%; height: 210px;  border-radius: 0.63rem;">
                                                </div>
                                                <div class="item-details">
                                                    <h3 class="title">@{{ work . title . en }}</h3>
                                                    <div class="desc">@{{ work . desc . en }}</div>
                                                </div>
                                            </a>
                                        </div>
                                        <!-- end items list -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of about section -->
            <!-- start of footer -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6"></div>
                        <div class="col-lg-6">
                            &copy;
                            copyright
                            <a href="#" target="_blank" rel="noopener"> Kejaksaan Republik Indonesia</a>
                        </div>
                    </div>
                </div>
            </footer>
            {{-- <div id="visitor-counter" style="position: fixed; right: -41px; bottom: 100px;">
                <script type="text/javascript" src="https://free-hit-counters.net/count/9dhu"></script><br>
                <a id="private-krankenvers" href='http://www.versicherungen.at/private-krankenversicherung/'>private
                    Krankenversicherung</a>
                <script type='text/javascript' src='https://www.whomania.com/ctr?id=18abb0801b9a40501edc9ce38b124d35a220bb5f'></script>
            </div> --}}
            <!-- end of footer -->

            <!-- scroll to top button -->
            <button ref="scrollTopBtn" :class="['scroll-to-top', isScrollTopBtnDisplayed && 'show-scrollTop']" title="Back To Top" tabindex="0" @click="scrollToTop">
                <i class="fa fa-angle-up" aria-hidden="true"></i>
            </button>

            <!-- toast notifications -->
            <ul class="notifications-container">
                <transition-group name="notify">
                    <li v-for="notify of notifications" :key="notify.id" :id="notify.id" :class="['notification', 'show', notify.className, notify.time && 'timer']">
                        @{{ notify . msg }}
                        <i class="fa fa-times link-hover" aria-hidden="true" @click="dismissNotify(notify.id)"></i>
                        <span v-if="notify.time" class="disappearing-time" :style="{ 'animation-duration': notify.time + 'ms' }"></span>
                    </li>
                </transition-group>
            </ul>

            <!-- ajax loading -->
            <div v-show="isAjaxLoading" class="ajax-loading">
                <span></span>
            </div>
        </div>
    </div>
    <div id="modal-container-custom"></div>
    <!-- js plugins file -->
    <script src="{{ asset('landingpage/scripts/plugins.min.js') }}"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- main script file -->
    <script>
        let imageList = [];
    </script>
    <script src="{{ asset('landingpage/scripts/main2.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ url('/') }}/demo1/plugins/custom/datatables/datatables.bundle.js"></script>
    <script>
        $(document).ready(function() {
            changeColor();
        });
        $(document).on('click', '.action-custom', function() {
            $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
            $('.action-custom').removeClass('show');

            $(this).addClass('show');
            $(this).parent().find('.menu-sub-dropdown').addClass('show');
        });
        $(document).on('click', function(e) {
            if ($(e.target).closest(".action-custom").length === 0) {
                $('.action-custom').removeClass('show');
                $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
            }
        });
        $(document).on('click', '#changeMode', function() {
            changeColor();
        });
        $(document).on('click', '.cari-nik', function() {
            $('.cari-nik').attr("disabled", true);
            $.get("{{ route('permohonan.dataModal') }}", {
                    // _token: "{{ csrf_token() }}",
                    nik: $('[name="nik"]').val(),
                })
                .done(function(response) {
                    $('#modal-container-custom').html(response);
                    $('#kt_modal_1').modal('show');
                })
                .fail(function() {
                    $('.cari-nik').attr("disabled", false);
                })
                .always(function() {
                    $('.cari-nik').attr("disabled", false);
                });
        });
    </script>
    <script>
        function changeColor() {
            if ($('#changeMode').attr('class') == 'light_theme') {
                $('#card-search-nik').css("background-color", "#fff");
                $('.contact-text').css("color", "#5f5f5f");
                $('#private-krankenvers').css("color", "#e6e6e6");
            } else {
                $('#card-search-nik').css("background-color", "#313131");
                $('.contact-text').css("color", "#9f9f9f");
                $('#private-krankenvers').css("color", "#1f1f1f");
            }
        }
    </script>

</body>

</html>
