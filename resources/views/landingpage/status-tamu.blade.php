<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pengaduan Masyarakat Adalah Layanan Interaksi Dan Komunikasi Masyarakat Yang Disediakan Oleh Kejaksaan RI Serta Kolaborasi Data Antar Kementrian Lembaga.">
    <!-- title of the browser tab -->
    <title>Kejaksaan Agung Republik Indonesia</title>

    <!-- favicon -->
    <link rel="icon" href="{{ asset('assets/favicon.ico') }}">

    <!-- fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Roboto&family=Saira+Stencil+One&text=Nafie&display=swap">
    <!-- chartist CSS -->

    <!-- css libraries file -->
    <link rel="stylesheet" href="{{ asset('landingpage/styles/libraries.min.css') }}">
    <!--boxicon CDN-->
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <!-- main style file -->
    <link rel="stylesheet" href="{{ asset('landingpage/styles/main.css') }}">

    <style>
        .logo-title {
            margin-left: 69px !important;
        }

        .logo-title-header {
            justify-content: center;
        }

        .text-desc {
            font-size: 2rem;
        }

        @media only screen and (max-width: 400px) {
            .logo-title {
                font-size: 20px !important;
                margin-left: 0 !important;
            }

            .logo-title-header {
                justify-content: start;
            }

            .text-desc {
                font-size: 1rem;
            }
        }

        @media only screen and (max-width: 600px) {
            .logo-title {
                font-size: 24px !important;
                margin-left: 0 !important;
            }

            .text-desc {
                font-size: 1rem;
            }
        }

        .menu-custom {
            -webkit-box-shadow: 10px 10px 5px 0px rgb(0 0 0 / 75%);
            -moz-box-shadow: 10px 10px 5px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 1px 2px 4px 0px rgb(0 0 0 / 75%);
            text-transform: uppercase;
            font-weight: bold;
            padding: 0.5rem 1.5rem;
            color: rgb(255, 255, 255);
        }

    </style>
</head>

<body>
    <div id="app">
        <div id="app-inner" ref="appRef" :class="[savedTheme, {'enable-focus-style': isAnyFocus}]" @click="isAnyFocus = false" @keyup.tab="isAnyFocus = true">

            <div class="circle-cursor circle-cursor-outer" ref="circleCursorOuter"></div>
            <div class="circle-cursor circle-cursor-inner" ref="circleCursorInner"></div>

            <!-- start preloader-->
            <transition name="preloader">
                <div class="preloader" v-if="isPreloading">
                    <div class="circle">
                        <div class="double-bounce1"></div>
                        <div class="double-bounce2"></div>
                    </div>
                </div>
            </transition>
            <!-- end preloader-->

            <!-- start of header -->
            <header :class="{
                'big-header': isHeaderBig,
                'small-header': !isHeaderBig,
                'header-hidden': isHeaderHidden
              }">
                <div class="container">
                    <div class="d-flex w-100 logo-title-header">
                        <div class="logo" title="kejaksaan republik indonesia">
                            <h1 class="logo-title">Kejaksaan Republik Indonesia</h1>
                        </div>
                    </div>


                </div>
            </header>
            <!-- end of header -->

            <!-- start of hero section -->
            <div id="hero" class="hero-section" ref="heroSection">
                <div class="hero-img">
                    <div class="layer" v-clone>
                        <img src="{{ asset('landingpage/images/hero-img.png') }}" alt="User Name">
                    </div>
                </div>

                <div class="hero-text">
                    <h2>
                        Layanan & Informasi Kejaksaan Republik Indonesia
                        <br>
                        <span class="text-desc">Layanan Interaksi Dan Komunikasi Masyarakat Yang Disediakan Oleh
                            Kejaksaan Republik Indonesia</span>
                    </h2>
                    {{-- <ul class="portfolio-items" ref="portfolioItems" data-no-more-works-msg="No more to load">
                        <li>
                            <div class="text-box">
                                <div class="works-filter">
                                    <a href="#portfolio" v-for="filter in ['Layanan', 'Sistem']" :key="filter">
                                        <button :class="{'active': worksFilter === filter.toLowerCase()}"
                                            @click="worksFilter = filter.toLowerCase()">
                                            <div class="menu-custom">
                                                @{{ filter == 'Sistem' ? 'Informasi' : filter }}
                                            </div>
                                        </button>
                                    </a>
                                    <a href="https://t.me/ruangberbagi29_bot" target="_blank">
                                        <button class="menu-custom">Ruang Diskusi</button>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul> --}}
                </div>



                <div class="scroll-down">
                    <a href="#about" title="Scroll Down">Scroll</a>
                </div>
            </div>
            <!-- end of hero section -->

            <!-- start of about section -->
            <div id="about" class="about-section section" tabindex="-1">
                <div class="container">
                    <h2 style="text-align: center">Pilih Status Tamu</h2>
                    <div class="section-content row">
                        <div id="portfolio" class="portfolio-section section" tabindex="-1">
                            <div class="container">
                                <div class="section-content">
                                    <!-- portfolio items -->
                                    <div class="row justify-content-center" ref="portfolioItems" data-no-more-works-msg="No more to load">
                                        <!-- start items list -->
                                        <div class="col-md-3 col-sm-6 col-xs-12 my-3 portfolio-item" style="" v-for="(work, index) in portfolioItems" :key="work.category.slug" v-show="worksFilter === 'all' || worksFilter === work.category.slug">
                                            <a :href="work.url" style=" height: 100px; display: flex; align-items: center; justify-content: center; border: 2px solid rgb(235, 106, 55); padding: 10px; border-radius: 7px;">
                                                <div class="item-img">
                                                    <h5 class="title mb-0 text-center">@{{ work . category . name }}</h5>
                                                </div>
                                            </a>
                                        </div>
                                        <!-- end items list -->
                                        {{-- <div class="col-md-3 col-sm-6 col-xs-12 my-3 portfolio-item" style=""
                                            :key="load_more">
                                            <a href="javascript:void(0)"
                                                style="height: 100px; display: flex; align-items: center; justify-content: center;"
                                                @click="getPortfolioItems">
                                                <div class="item-img">
                                                    <h5 class="title mb-0 text-center">Load more</h5>
                                                </div>
                                            </a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of about section -->



            <!-- start of footer -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6"></div>
                        <div class="col-lg-6">
                            &copy;
                            copyright
                            <a href="#" target="_blank" rel="noopener"> Kejaksaan Republik Indonesia</a>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end of footer -->

            <!-- scroll to top button -->
            <button ref="scrollTopBtn" :class="['scroll-to-top', isScrollTopBtnDisplayed && 'show-scrollTop']" title="Back To Top" tabindex="0" @click="scrollToTop">
                <i class="fa fa-angle-up" aria-hidden="true"></i>
            </button>

            <!-- toast notifications -->
            <ul class="notifications-container">
                <transition-group name="notify">
                    <li v-for="notify of notifications" :key="notify.id" :id="notify.id" :class="['notification', 'show', notify.className, notify.time && 'timer']">
                        @{{ notify . msg }}
                        <i class="fa fa-times link-hover" aria-hidden="true" @click="dismissNotify(notify.id)"></i>
                        <span v-if="notify.time" class="disappearing-time" :style="{ 'animation-duration': notify.time + 'ms' }"></span>
                    </li>
                </transition-group>
            </ul>

            <!-- ajax loading -->
            <div v-show="isAjaxLoading" class="ajax-loading">
                <span></span>
            </div>
        </div>
    </div>
    <script src="{{ asset('elite/assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <!-- js plugins file -->
    <script src="{{ asset('landingpage/scripts/plugins.min.js') }}"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- main script file -->
    <script>
        let imageList = JSON.parse("{{ $images }}".replace(/&quot;/g, '"'));
    </script>
    <script src="{{ asset('landingpage/scripts/main.js') }}"></script>
</body>

</html>
