@section('styles')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
    }
    body {
        /* background-image: url("{{ asset('assets/bg-form.jpg') }}"); */
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;

        /* background: linear-gradient(0deg, rgba(13,35,91,1) 27%, rgba(68,103,232,1) 100%) no-repeat center center fixed; */
        height:100%;
        /* background-repeat: repeat; */
        background-size: contain ;
        /* background-color: red; */
        /* background: linear-gradient(0deg, rgba(13,35,91,1) 27%, rgba(68,103,232,1) 100%); */
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Scan QR</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            Scan QR
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">Scan QR Code</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <form method='get' class="lacak_berkas_1 mb-2 mb-md-0" action="">
                <div class="form-row justify-content-center">
                    <div class="col-lg-12 col-md-12">
                        <h3>Pilih sumber kamera : </h3>
                        <select class="form-control" id="pilihKamera" style="max-width:400px">
                        </select>
                        <div class="box-center">
                            <div class="header-content px-3 py-5 pt-md-5 pb-md-4 mx-auto text-center">
                                <h4 class="title">Anda diwajibkan mempunyai QR untuk melakukan pelayanan, Sejajarkan kode QR di dalam bingkai agar dapat dipindai</h4>
                                <p class="sub-title">
                                    Jika anda tidak mempunyai QR Code Silahkan <a href="{{url('layanan/daftar')}}"><link>Klik Disini</link><a> mendaftar terlebih dahulu untuk mendapatkan layanan
                                </p>
                            </div>
                            <br>
                            <div class="video-wrapper">
                                <div class="box-border">
                                    <video id="previewKamera"></video>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    <br>
                </div>
            </form>
        </div>
        <!--end::Card body-->
    </div>
    
    <!--end::Card-->
    @section('scripts')
    <script type="text/javascript">
        let selectedDeviceId = null;
            const codeReader = new ZXing.BrowserMultiFormatReader();
            const sourceSelect = $("#pilihKamera");
            // var base_url = window.location.origin
            var base_url = "{{ url('') }}";
            
            $(document).on('change','#pilihKamera',function(){
                selectedDeviceId = $(this).val();
                if(codeReader){
                    codeReader.reset()
                    initScanner()
                }
            })
            
            function initScanner() {
                codeReader
                .listVideoInputDevices()
                .then(videoInputDevices => {
                    videoInputDevices.forEach(device =>
                        console.log(`${device.label}, ${device.deviceId}`)
                    );
            
                    if(videoInputDevices.length > 0){
                        
                        if(selectedDeviceId == null){
                            if(videoInputDevices.length > 1){
                                selectedDeviceId = videoInputDevices[1].deviceId
                            } else {
                                selectedDeviceId = videoInputDevices[0].deviceId
                            }
                        }
                        
                        
                        if (videoInputDevices.length >= 1) {
                            sourceSelect.html('');
                            videoInputDevices.forEach((element) => {
                                const sourceOption = document.createElement('option')
                                sourceOption.text = element.label
                                sourceOption.value = element.deviceId
                                if(element.deviceId == selectedDeviceId){
                                    sourceOption.selected = 'selected';
                                }
                                sourceSelect.append(sourceOption)
                            })
                    
                        }
            
                        codeReader
                            .decodeOnceFromVideoDevice(selectedDeviceId, 'previewKamera')
                            .then(result => {
            
                                    //hasil scan
                                    console.log(result.text)
                                    $("#hasilscan").val(result.text);
                                    window.location.href = base_url + "/validate/qr/" + result.text ;
                                
                                    if(codeReader){
                                        codeReader.reset()
                                    }
                            })
                            .catch(err => console.error(err));
                        
                    } else {
                        alert("Camera not found!")
                    }
                })
                .catch(err => console.error(err));
            }
            
            
            if (navigator.mediaDevices) {
                
            
                initScanner()
                
            
            } else {
                alert('Cannot access camera.');
            }

       </script>
    @endsection

</x-base-layout>
