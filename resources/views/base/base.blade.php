<!DOCTYPE html>
{{--
Product Name: {{ theme()->getOption('product', 'description') }}
Author: KeenThemes
Purchase: {{ theme()->getOption('product', 'purchase') }}
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: {{ theme()->getOption('product', 'license') }}
--}}
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {!! theme()->printHtmlAttributes('html') !!}
{{ theme()->printHtmlClasses('html') }}>
{{-- begin::Head --}}

<head>
    
    <meta charset="utf-8" />
    <title>{{ ucfirst(theme()->getOption('meta', 'title')) }} | PELAYANAN TERPADU</title>
    <meta name="description" content="Pelayanan Terpadu Kejaksaan RI Adalah Layanan Interaksi Dan Komunikasi Masyarakat Yang Disediakan Oleh Kejaksaan RI Serta Kolaborasi Data Antar Kementrian Lembaga" />
    <meta name="keywords" content="{{ theme()->getOption('meta', 'keywords') }}" />
    <link rel="canonical" href="{{ ucfirst(theme()->getOption('meta', 'canonical')) }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="{{asset('assets/logo-kejaksaan.png')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! csrf_field() !!}

    {{-- begin::Fonts --}}
    {{ theme()->includeFonts() }}
    {{-- end::Fonts --}}

    @if (theme()->hasOption('page', 'assets/vendors/css'))
    {{-- begin::Page Vendor Stylesheets(used by this page) --}}
    @foreach (theme()->getOption('page', 'assets/vendors/css') as $file)
    <link href="{{ assetCustom($file) }}" rel="stylesheet" type="text/css" />
    @endforeach
    {{-- end::Page Vendor Stylesheets --}}
    @endif

    @if (theme()->hasOption('page', 'assets/custom/css'))
    {{-- begin::Page Custom Stylesheets(used by this page) --}}
    @foreach (theme()->getOption('page', 'assets/custom/css') as $file)
    <link href="{{ assetCustom($file) }}" rel="stylesheet" type="text/css" />
    @endforeach
    {{-- end::Page Custom Stylesheets --}}
    @endif

    @if (theme()->hasOption('assets', 'css'))
    {{-- begin::Global Stylesheets Bundle(used by all pages) --}}
    @foreach (theme()->getOption('assets', 'css') as $file)
    <link href="{{ assetCustom($file) }}" rel="stylesheet" type="text/css" />
    @endforeach
    {{-- end::Global Stylesheets Bundle --}}
    @endif

    @if (theme()->getViewMode() === 'preview')
    {{ theme()->getView('partials/trackers/_ga-general') }}
    {{ theme()->getView('partials/trackers/_ga-tag-manager-for-head') }}
    @endif
    <link rel="stylesheet" href="{{ asset('styles/custom.css') }}">

    <style>
        @media (min-width: 1400px) {

            .container-xxl,
            .container-xl,
            .container-lg,
            .container-md,
            .container-sm,
            .container {
                max-width: none !important;
            }
        }
    </style>
    @yield('styles')
    @yield('head_js')
</head>
{{-- end::Head --}}

{{-- begin::Body --}}

<body {!! theme()->printHtmlAttributes('body') !!} {!! theme()->printHtmlClasses('body') !!} {!!
    theme()->printCssVariables('body') !!}>

    @if (theme()->getOption('layout', 'loader/display') === true)
    {{ theme()->getView('layout/_loader') }}
    @endif

    @yield('content')
    <div id="modal-container-custom"></div>
    {{-- begin::Javascript --}}
    @if (theme()->hasOption('assets', 'js'))
    {{-- begin::Global Javascript Bundle(used by all pages) --}}
    @foreach (theme()->getOption('assets', 'js') as $file)
    <script src="{{ asset(theme()->getDemo() . '/' .$file) }}"></script>
    @endforeach
    {{-- end::Global Javascript Bundle --}}
    @endif

    @if (theme()->hasOption('page', 'assets/vendors/js'))
    {{-- begin::Page Vendors Javascript(used by this page) --}}
    @foreach (theme()->getOption('page', 'assets/vendors/js') as $file)
    <script src="{{ asset(theme()->getDemo() . '/' .$file) }}"></script>
    @endforeach
    {{-- end::Page Vendors Javascript --}}
    @endif

    @if (theme()->hasOption('page', 'assets/custom/js'))
    {{-- begin::Page Custom Javascript(used by this page) --}}
    @foreach (theme()->getOption('page', 'assets/custom/js') as $file)
    <script src="{{ asset(theme()->getDemo() . '/' .$file) }}"></script>
    @endforeach
    {{-- end::Page Custom Javascript --}}
    @endif
    {{-- end::Javascript --}}

    @if (theme()->getViewMode() === 'preview')
    {{ theme()->getView('partials/trackers/_ga-tag-manager-for-body') }}
    @endif

    <script>
        window.replainSettings = { id: 'c9f245ab-eb4f-4307-ae7b-00f71e05a50f' };
        (function(u){var s=document.createElement('script');s.async=true;s.src=u;
        var x=document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);
        })('https://widget.replain.cc/dist/client.js');
    </script>
        
        

    <script>
        
        $(document).on('click', '.action-custom', function(){
                $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
                $('.action-custom').removeClass('show');

                $(this).addClass('show');
                $(this).parent().find('.menu-sub-dropdown').addClass('show');
            });
            $(document).on('click', function (e) {
                if ($(e.target).closest(".action-custom").length === 0) {
                    $('.action-custom').removeClass('show');
                    $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
                }
            });

            var target = document.querySelector("#container-card");
            var blockUI = new KTBlockUI(target, {
                message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
            });
            function Verif(event) {
                event.preventDefault();
                const id = event.currentTarget.getAttribute('data-id');
                let url = "{{ route('permohonan.proses', ':id') }}";
                url = url.replace(':id', id);
                Swal.fire({
                    title: 'Anda yakin Akan memverifikasi permohonan ini?',
                    text: 'Permohonan akan terkirim otomatis ke sipede satker tujuan ',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Kirim Permohonan!'
                })
                .then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'GET', //THIS NEEDS TO BE GET
                            url: url,
                            dataType: 'json',
                            beforeSend: function() {
                                // blockUI.block();
                                $('#verification-button').html('Loading... <i class="fa fa-spinner" aria-hidden="true"></i>');
                            },
                            success: function (data) {
                                // blockUI.release();
                                if (data['status'] == 201) {
                                    Swal.fire(
                                        'Berhasil!',
                                        'Permohonan berhasil terkirim ke sipede.',
                                        'success'
                                    )
                                    window.location.reload();
                                    $('#verification-button').html('Verifikasi');
                                }else {
                                    Swal.fire(
                                        'Gagal!',
                                        'Permohonan gagal terkirim ke sipede.',
                                        'error'
                                    )
                                    $('#verification-button').html('Verifikasi');
                                }
                            },error:function(){
                                blockUI.release();
                                $('#verification-button').html('Verifikasi');
                                Swal.fire(
                                    'Gagal!',
                                    'Permohonan gagal terkirim ke sipede.',
                                    'error'
                                )
                            }
                        });
                    }
                });
            }
            function deleteData(event)
            {
                event.preventDefault();
                const id = event.currentTarget.getAttribute('data-id');
                console.log(id);
                let url = "{{ route('permohonan.destroy', ':id') }}";
                url = url.replace(':id', id);
                Swal.fire({
                    title: 'Anda yakin?',
                    text: 'Akan menghapus permohonan ini',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                .then((result) => {
                    if (result.value) {
                        let submit = $("#deleteForm").attr('action', url);
                        submit.submit();
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    }
                });
            }
    </script>

    @yield('scripts')
</body>
{{-- end::Body --}}

</html>
