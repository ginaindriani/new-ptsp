
@section('styles')
<style>
    .grecaptcha-badge {
        bottom: 100px !important;
    }
</style>
@endsection
@section('head_js')
<script type="text/javascript">
    function tokenValidation(token) {
        document.getElementsByName('g_token')[0].value = token;
    }
</script>   

{!! htmlScriptTagJsApi([
    'action' => 'submit',
    'custom_validation' => 'tokenValidation'
]) !!} 
@endsection
<x-auth-layout>

    <!--begin::Signin Form-->
    <form method="POST" action="{{ theme()->getPageUrl('login') }}" class="form w-100" novalidate="novalidate" autocomplete="off"
        id="kt_sign_in_form">
        @csrf
        

        <!--begin::Heading-->
        <div class="text-center mb-10">
            <!--begin::Title-->
            <h1 class="text-dark mb-3">
                {{ __('Layanan Kejaksaan') }}
            </h1>
            <!--end::Title-->

            <!--begin::Link-->
            <div class="text-gray-400 fw-bold fs-4">
            {{ __('Layanan & Informasi Kejaksaan RI') }}

            </div>
            <!--end::Link-->
        </div>
        <!--begin::Heading-->

        <!--begin::Input group-->
        <div class="fv-row mb-10">
            <!--begin::Label-->
            <label class="form-label fs-6 fw-bolder text-dark">{{ __('Username') }}</label>
            <!--end::Label-->

            <!--begin::Input-->
            <input class="form-control form-control-lg form-control-solid" type="text" name="username" autocomplete="new-password"
                value="{{ old('username', '') }}" required autofocus />
            <!--end::Input-->
            <input type="hidden" name="g_token">
        </div>
        <!--end::Input group-->

        <!--begin::Input group-->
        <div class="fv-row mb-10">
            <!--begin::Wrapper-->
            <div class="d-flex flex-stack mb-2">
                <!--begin::Label-->
                <label class="form-label fw-bolder text-dark fs-6 mb-0">{{ __('Password') }}</label>
                <!--end::Label-->

                <!--begin::Link-->
                {{-- @if (Route::has('password.request'))
                <a href="{{ theme()->getPageUrl('password.request') }}" class="link-primary fs-6 fw-bolder">
                    {{ __('Forgot Password ?') }}
                </a>
                @endif --}}
                <!--end::Link-->
            </div>
            <!--end::Wrapper-->

            <!--begin::Input-->
            <input class="form-control form-control-lg form-control-solid" type="password" name="password"
                autocomplete="new-password" value="" required />
            <!--end::Input-->
        </div>
        <!--end::Input group-->

        <!--begin::Input group-->
        <div class="fv-row mb-10">
            {{-- <label class="form-check form-check-custom form-check-solid">
                <input class="form-check-input" type="checkbox" name="remember" />
                <span class="form-check-label fw-bold text-gray-700 fs-6">{{ __('Remember me') }}
                </span>
            </label> --}}
        </div>
        <!--end::Input group-->

        <!--begin::Actions-->
        <div class="text-center">
            <!--begin::Submit button-->
            <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
                @include('partials.general._button-indicator', ['label' => __('Continue')])
            </button>
            <!--end::Submit button-->


        </div>
        <!--end::Actions-->
    </form>
    <!--end::Signin Form-->
    @section('scripts')
        <script>
        //     const _token = $('meta[name="csrf-token"]').attr('content');
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': _token
        //     }
        // });
            let secretKey = "{{$secretKey}}";
            // let secretKey = "6LfcN0AjAAAAANa4xiISYq6Z6K4uLs23H4F6doJb";
            console.log('suntala', secretKey)
        </script>
    @endsection
</x-auth-layout>
