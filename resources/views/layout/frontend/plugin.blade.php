<script src="{{ asset('assets/backend/plugins/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/webcam-easy-1.0.5.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/placeholders.js') }}"></script>
<script src="{{ asset('assets/frontend/plugin/bootstrap-3.3.4/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/frontend/plugin/select2/js/select2.full.min.js')}}"></script>
{{-- <script src="{{ asset('assets/frontend/js/qrreader/webqr.js') }}"></script>
<script src="{{ asset('assets/frontend/js/qrreader/llqrcode.js') }}"></script>
<script src="{{ asset('assets/frontend/js/qrreader/plusone.js') }}"></script> --}}
<script type="text/javascript">
    const loader = document.querySelector("#loading");
        // showing loading
    function displayLoading() {
        loader.classList.add("display");
        // to stop loading after some time
        setTimeout(() => {
            loader.classList.remove("display");
        }, 5000);
    }
    // hiding loading 
    function hideLoading() {
        loader.classList.remove("display");
    }

    $('#loads').on('click', function() {
      displayLoading()
    })
</script>
@stack('plugins-js')