<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Layanan || KEJAKSAAN AGUNG RI</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="stylesheet" href="{{ asset('assets/frontend/css/font2.css') }}" type="text/css"/>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Favicon -->
<link rel="icon" href="{{ asset('img/logo.png') }}" type="image/x-icon"/>
<link href="{{ asset('assets/frontend/plugin/bootstrap-3.3.4/css/bootstrap.css') }}" rel="stylesheet">
<!-- font-awesome icons -->
<link href="{{ asset('assets/frontend/plugin/Font-Awesome-4.6.3/css/font-awesome.css') }}" rel="stylesheet"> 
<!-- //font-awesome icons -->

<script src="{{ asset('assets/frontend/js/jquery-2.1.4.min.js') }}"></script>

<script src="{{ asset('assets/frontend/js/toastr.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/frontend/css/toastr.min.css') }}">
<script src="{{ asset('assets/frontend/js/vanilla-masker.js') }}"></script>


<link href="{{ asset('assets/frontend/css/preview.css') }}" rel="stylesheet">
<link href="{{ asset('assets/frontend/plugin/select2/css/select2.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/frontend/plugin/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/frontend/css/webcam-style.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('assets/backend/plugins/sweetalert2/dist/sweetalert2.min.css') }}">
{{-- <link href="{{ asset('assets/frontend/css/qr-reader.css') }}" rel="stylesheet" /> --}}
<style>
	html {
		height: 100%;
	}
	/*.modal {
	  width: 600px;
	  margin: 0 auto; 
	}*/
</style>
<style type="text/css">
	#loading {
		width: 5rem;
		height: 5rem;
		border: 5px solid #f3f3f3;
		border-top: 6px solid #9c41f2;
		border-radius: 100%;
		margin: auto;
		visibility: hidden;
		animation: spin 1s infinite linear;
	}
	#loading.display {
		visibility: visible;
	}
	@keyframes spin {
		from {
			transform: rotate(0deg);
		}
		to {
			transform: rotate(360deg);
		}
	}
	
	.d-none {
	  display: none !important;
	}
</style>
@stack('plugins-css')