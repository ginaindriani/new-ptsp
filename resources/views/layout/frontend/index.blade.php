<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="robots" content="noindex">
	@include('layout.frontend.head')
</head>
<body class="eternity-form scroll-animations-activated signup-body" style="background-image:url({{ asset('/img/landing.jpeg') }})">
	
	<section class="colorBg" id="demo1" data-panel="first">
        <div class=" container auth">
            
            @include('sweetalert::alert')
        	@yield('content')
        </div>
    </section>
    <!-- Basic modal -->
    @stack('modal')
    <!-- End Basic modal -->

    <!-- plugins -->
	@include('layout.frontend.plugin')
	<!-- end plugins -->
	
</body>
</html>