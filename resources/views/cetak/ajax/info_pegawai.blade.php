<div class="row justify-content-center">
    @if ($request->search_by == 'nama')
        @foreach ($result as $item)
            <div class="col-md-2 col-lg-2 col-md-3 col-sm-4 col-xs-6 mb-5">
                <div class="user_column p-5" style="height: 480px; justify-content: unset; overflow: scroll;">
                    <div style="height: 300px; margin: 0 auto; overflow: hidden;" class="mb-5">
                        <img src="https://images.weserv.nl?url={{rawurlencode($item->foto_alternatif ?: $item->foto)}}" onerror="javascript:this.src='{{asset('assets/default.jpg')}}'" alt="image" style="border-radius: 8px; width: 100%; height: 100%; object-fit: cover;">
                    </div>
                    <div href="#" class="fs-7 text-gray-800 text-hover-primary fw-bolder mb-3 text-center">{{$item->nama}}</div>
                    <div href="#" class="fs-7 text-gray-800 text-hover-primary fw-bolder mb-3 text-center">{{$item->jabatan}}</div>
                    <div class="mb-9 text-center">
                        <div class="badge badge-lg badge-light-info" style="white-space: normal;">{{$item->nama_satker}}</div>
                    </div>
                </div>
            </div>
        @endforeach
        <ul class="pagination mt-5">
            <li class="page-item previous previous-run {{$page > 1 ? '' : 'disabled'}}">
                <a href="#" class="page-link">
                    <i class="previous"></i>
                </a>
            </li>
            @for ($i = 1; $i <= $totalPage; $i++)
                <li class="page-item page-run {{$page == $i ? 'active' : ''}}" data-page="{{$i}}">
                    <a href="#" class="page-link">{{$i}}</a>
                </li>
            @endfor
            <li class="page-item next next-run {{$page < $totalPage ? '' : 'disabled'}}" data-total-page="{{$totalPage}}">
                <a href="#"  class="page-link">
                    <i class="next"></i>
                </a>
            </li>
        </ul>
    @else
        <div class="col-lg-2 mb-5">
            <div class="user_column p-5" style="height: 480px; justify-content: unset; overflow: scroll;">
                <div style="height: 300px; margin: 0 auto; overflow: hidden;" class="mb-5">
                    <img src="https://images.weserv.nl?url={{rawurlencode($result->foto_alternatif ?: $result->foto)}}" onerror="javascript:this.src='{{asset('assets/default.jpg')}}'" alt="image" style="border-radius: 8px; width: 100%; height: 100%; object-fit: cover;">
                </div>
                <div href="#" class="fs-7 text-gray-800 text-hover-primary fw-bolder mb-3 text-center">{{$result->nama}}</div>
                <div href="#" class="fs-7 text-gray-800 text-hover-primary fw-bolder mb-3 text-center">{{$result->jabatan}}</div>
                <div class="mb-9 text-center">
                    <div class="badge badge-lg badge-light-info" style="white-space: normal;">{{$result->nama_satker}}</div>
                </div>
            </div>
        </div>
    @endif
</div>