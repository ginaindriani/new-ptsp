<html>
<head>
	<title>Surat Permohonan</title>
	{{-- <link rel="stylesheet" href="{{asset('demo1/css/bootstrap.min.css')}}"> --}}
    <style>
        *, ::after, ::before {
            box-sizing: border-box;
        }
        .mb-3, .my-3 {
            margin-bottom: 1rem!important;
        }
        .mb-5, .my-5 {
            margin-bottom: 3rem!important;
        }
        .table {
            width: 100%;
        }
        table {
            border-collapse: collapse;
        }
        .table td, .table th {
            padding-top: 10px;
            padding-bottom: 10px;
            /* vertical-align: top; */
            border-top: 1px solid #dee2e6;
        }
        th {
            text-align: inherit;
        }
        body {
            margin: 0;
            font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }
        .h6, h6 {
            font-size: 1rem;
        }
        .h4, h4 {
            font-size: 1.5rem;
        }
        h1, h2, h3, h4, h5, h6 {
            margin-top: 0;
            margin-bottom: .5rem;
        }
        .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
            margin-bottom: .5rem;
            font-weight: 500;
            line-height: 1.2;
        }
        b, strong {
            font-weight: bolder;
        }
        table tr td,
		table tr th{
			font-size: 11px;
		}
    </style>
</head>
<body>
    <div>
        <div style="float: left">
            <h4 class="mb-3">{{$masterLayanan->nama_layanan}}</h4>
            <h6 class="mb-5">Nomor Permohonan: {{$permohonan->nomor_permohonan}}</h6>
        </div>
        <div style="float: right; margin-bottom: 25px; margin-top: 25px;">
            <img src="data:image/png;base64, {!! base64_encode($qrcode) !!} ">
        </div>
        <div style="clear: both;"></div>
        <div>
            <table class="table">
                <tbody>  
                    @foreach ($kontens as $item => $result)
                        @if ($item != 'idLayanan')
                            <tr>
                                <th><b>{{strtoupper(str_replace('_', ' ', $item))}}</b></th>
                                <td>:</td>
                                <td>{!!$result!!}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <br/>
        <div class="col-md-12">
            <div class="table-responsive ">
                <table class="table table-striped" style="width:100%;" id="tableuser">
                    <thead align="center">
                        <tr>
                            <th style="text-align: center;">No</th>
                            <th style="text-align: center;">Diproses Oleh</th>
                            <th style="text-align: center;">Status Akhir</th>
                            <th style="text-align: center;">Tanggal Tindak Lanjut</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                        @if($logStatus == '200')
                            @php $i=1 @endphp
                            @foreach($log as $l)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $l->nama_jabatan }}</td>
                                <td>{{ $l->status }}</td>
                                <td>{{ $l->checked_at }}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">
                                
                                DATA BELUM DIPROSES
                                
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>