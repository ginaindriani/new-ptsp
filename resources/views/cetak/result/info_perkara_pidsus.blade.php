@if (!auth()->user())
@section('styles')
<link rel="stylesheet" href="{{asset('demo1/css/style.bundle.css')}}">
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endsection
@endif
@if (auth()->user())
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3"></h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            <a href="{{route('permohonan.index')}}?idLayanan={{$masterLayanan->id_layanan}}"
                class="text-muted text-hover-primary">
                {{$masterLayanan->nama_layanan}}
            </a>
        </li>
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            View Data
        </li>
    </ul>
</div>
@endsection
@endif
<x-base-layout>
    @include('components.notification')
    <!--begin::Card-->
    <div class="card" id="container-card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <div class="row">
                <div class="col-md-12 mb-5">
                    <h3>Nomor Permohonan: {{$permohonan->nomor_permohonan}}</h3>
                </div>
                <div class="col-md-6 mt-5">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                
                                @foreach ($kontens as $item => $result)
                                @if ($item != 'idLayanan')
                                <tr>
                                    <th><b>{{strtoupper(str_replace('_', ' ', $item))}}</b></th>
                                    <td>{!!$result!!}</td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="text-center" style="margin-top: 30px;">
                        {!! $qrcode !!}
                    </div>
                </div>
<div class="col-md-12">
                    <div class="table-responsive ">
                        <table class="table table-striped" style="width:100%;" id="tableuser">
                            <thead align="center">
                                <tr>
                                    <th style="text-align: center;">No</th>
                                    <th style="text-align: center;">Diproses Oleh</th>
                                    <th style="text-align: center;">Status Akhir</th>
                                    <th style="text-align: center;">Tanggal Tindak Lanjut</th>
                                </tr>
                            </thead>
                            <tbody align="center">
                                @if($logStatus == '200')
                                    @php $i=1 @endphp
                                    @foreach($log as $l)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $l->nama_jabatan }}</td>
                                        <td>{{ $l->status }}</td>
                                        <td>{{ $l->checked_at }}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                        DATA BELUM DIPROSES
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="text-center mt-5">
                        <a target="__blank"
                            href="{{route('permohonan.downloadfile', ['id' => $permohonan->id_permohonan_layanan])}}"
                            type="button" class="btn btn-secondary btn-sm me-3">
                            <i class="fa fa-file-pdf"></i> Download
                        </a>
                        @if($permohonan->status == null)
                        @if (auth()->user())
                        <a href="javascript:void(0);" class="btn btn-warning btn-sm" id="verification-button"
                            data-id="{{$permohonan->id_permohonan_layanan}}" onclick="Verif(event)"
                            class="menu-link px-3" data-kt-customer-table-filter="delete_row">Verifikasi</a>
                        @endif
                        @else
                        <a href="javascript:void(0);" class="btn btn-success btn-sm" class="menu-link px-3"
                            data-kt-customer-table-filter="delete_row">Sudah Verifikasi</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

    {{-- Inject Scripts --}}
    @section('scripts')
    <script src="{{asset('demo1/js/scripts.bundle.js')}}"></script>

    @endsection
</x-base-layout>
