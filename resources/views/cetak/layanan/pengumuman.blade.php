<?php
    function tgl_indo($tanggal){
    $substr = substr($tanggal, 0, -9);
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $substr);

    if($substr == NULL){
        return NULL;
    }
    
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
?>
@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
    .card-img-left{
        width: 300px;
        height: 300px;
        margin: auto;
    }
    .card-img-left img {
        object-fit: cover;
        width: 300px !important;
        height: 300px !important;
        filter: brightness(0.7);
        border-radius: 0 !important;
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">{{$masterLayanan->nama_layanan}}</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            {{$masterLayanan->nama_layanan}}
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <div class="row">
                <div class="row">
                    @forelse($pengumuman as $pengumuman)
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="m-2 card">
                            <div class="card-img">
                                <img  height="matchparent" width="100%"src="{{asset($pengumuman->foto)}}">
                            </div>
                            <div class="py-5 px-3">
                                <h6 class="text-primary text-align-left">{{tgl_indo($pengumuman['tanggal'])}}</h6>
                                <h5 class="card-title text-align-left">{{$pengumuman['judul_pengumuman']}}</h5>
                                <p class="text-muted text-align-left">{{$pengumuman['nama_satker']}}</p>
                                <a class="btn btn-dark float-right" target="_blank" href="{{url('pengumuman/'.$pengumuman->id)}}">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                    @empty
                    DATA TIDAK ADA
                    @endforelse
                </div>
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->
    @section('scripts')
      <script>
        const newsdetails = document.getElementById("newsdetails");
        const api_data = "https://ptsp.kejaksaan.go.id/api/listnews";
        console.log('adadisiniii nih')
        window.onload = function() {
          fetchData();
          console.log('adadisiniii nih')
        };

        const fetchData = async () => {
          const response = await fetch(api_data);
          newsDataArr = [];
          if(response.status >=200 && response.status < 300) {
              const myJson = await response.json();
              newsDataArr = myJson.data;
          } else {
              // handle errors
              console.log(response.status, response.statusText);
              newsdetails.innerHTML = "<h5>No data found.</h5>"
              return;
          }
          $('#beritaNasional').removeClass('show');
          $('#beritaNasional').addClass('active');
          $('#beritaNasional').addClass('show');

          displayNews();
        }

        function displayNews() {

        newsdetails.innerHTML = "";

        // if(newsDataArr.length == 0) {
        //     newsdetails.innerHTML = "<h5>No data found.</h5>"
        //     return;
        // }

        newsDataArr.forEach(news => {

            
            var col = document.createElement('div');
            col.className="col-lg-3 col-md-4 col-sm-6";

            var card = document.createElement('div');
            card.className = "m-2 card";

            var imgleft = document.createElement('div');
            imgleft.className = "card-img";

            var image = document.createElement('img');
            image.setAttribute("height","matchparent");
            image.setAttribute("width","100%");
            image.src=news.cover;

            var image = document.createElement('img');
            image.setAttribute("height","matchparent");
            image.setAttribute("width","100%");
            image.src=news.cover;

            var cardBody = document.createElement('div');
            cardBody.className = "py-5 px-3";

            var newsHeading = document.createElement('h5');
            newsHeading.className = "card-title text-align-left";
            newsHeading.innerHTML = news.title;

            var dateHeading = document.createElement('h6');
            dateHeading.className = "text-primary text-align-left";
            dateHeading.innerHTML = news.created_at;

            var discription = document.createElement('p');
            discription.className="text-muted text-align-left";
            discription.innerHTML = news.title;

            var link = document.createElement('a');
            var berita = "berita/"
            link.className="btn btn-dark float-right";
            link.setAttribute("target", "_blank");
            link.href = berita.concat(news.id);
            link.innerHTML="Read more";

            cardBody.appendChild(dateHeading);
            cardBody.appendChild(newsHeading);
            cardBody.appendChild(discription);
            cardBody.appendChild(link);

            imgleft.append(image);
            card.appendChild(imgleft);
            // card.appendChild(image);
            card.appendChild(cardBody);

            col.appendChild(card);

            newsdetails.appendChild(col);
        });

        // newsDataArr.forEach(news => {

            
        //     var col = document.createElement('div');
        //     col.className="card my-5 no-border";

        //     var card = document.createElement('div');
        //     card.className = "card-horizontal d-md-flex align-items-center";

        //     var imgleft = document.createElement('div');
        //     imgleft.className = "card-img-left";

        //     var image = document.createElement('img');
        //     // image.setAttribute("height","matchparent");
        //     // image.setAttribute("width","100%");
        //     image.src=news.cover;

        //     var cardBody = document.createElement('div');
        //     cardBody.className = "card-body px-3 py-0 bg-white";

        //     var newsHeading = document.createElement('h5');
        //     newsHeading.className = "card-title my-2";
        //     newsHeading.innerHTML = news.title;

        //     var dateHeading = document.createElement('h6');
        //     dateHeading.className = "text-primary";
        //     dateHeading.innerHTML = news.created_at;

        //     var discription = document.createElement('p');
        //     discription.className="text-muted";
        //     discription.innerHTML = news.title;

        //     var link = document.createElement('a');
        //     var berita = "berita/"
        //     link.className="btn btn-dark";
        //     link.setAttribute("target", "_blank");
        //     link.href = berita.concat(news.id);
        //     link.innerHTML="Baca Selengkapnya";

        //     cardBody.appendChild(newsHeading);
        //     cardBody.appendChild(dateHeading);
        //     cardBody.appendChild(discription);
        //     cardBody.appendChild(link);
        //     imgleft.append(image);

        //     card.appendChild(imgleft);
        //     card.appendChild(cardBody);

        //     col.appendChild(card);

        //     newsdetails.appendChild(col);
        //     });

        }

      </script>
    @endsection

</x-base-layout>
