@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }
    .title-image{
        width: 100%;
        /* max-height:550px; */
        text-align: center;
    }
    .title-image img{
        height: auto;
        width: 60%;
        object-fit: cover;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">BERITA</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            BERITA
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">BERITA</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <div class="px-md-5 pb-md-2 mt-3 pt-0 d-flex align-items-center">
                <div class="container col-md-11 px-0">
                    <div class="title-image mb-5">
                        <img src="{{$berita['cover']}}" alt="cover" class="img img-responsive img-fluid">
                    </div>
                    <hr>
                    <h1 class="title">
                        <strong>{{$berita['title']}}
                        </strong>
                    </h1>
                    <div class="d-flex align-items-center justify-content-between mt-3 control">
                        <div>
                            <p class="card-text">
                                <small class="text-muted"><i class="fa fa-user mr-2"></i>&nbsp;&nbsp;<strong>Superadmin</strong></small>
                            </p>
                            <p class="card-text mt-3">
                                <small class="text-muted"><i class="fa fa-calendar mr-2"></i>&nbsp;&nbsp;{{$berita['Date']}}</small>
                            </p>
                        </div>
                    </div>
                    <p class="mt-4">
                        {{$berita['description']}}
                    </p>
                </div>
            </div>
            {{-- <img src="{{$berita['cover']}}" alt="cover" class="img img-responsive img-fluid">
            <div class="content">
                <br><br>
                <div class="title"><h1>{{$berita['title']}}</h1></div>
                
                <div class="sub-title">
                    <p class="m-0">
                        <i class="icofont-user mr-2"></i>Superadmin
                        &nbsp;&nbsp;
                        <i class="icofont-wall-clock mr-2"></i>{{$berita['Date']}}
                    </p>
                </div>
                <br><br>
                <h5><p align="justify">{{$berita['description']}} </p></h5>
            </div> --}}
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

</x-base-layout>
