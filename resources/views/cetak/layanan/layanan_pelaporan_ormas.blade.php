@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .grecaptcha-badge {
        bottom: 100px !important;
    }
    @media (min-width: 1400px) {
        .container-xxl, .container-xl, .container-lg, .container-md, .container-sm, .container {
            max-width: 900px !important;
        }
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }

    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-container { 
        z-index: 100000 !important; 
        background-color: #FFF;
        position: fixed;
        display: inline-block;
        float: left;
    }

    .modal{
        z-index: 1000;   
    }
    .modal-backdrop{
        z-index: 10;        
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input-update,
    #pac-input,
    #pac-input-clockout {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
        z-index: 100000 !important;
    }

    #pac-input-update:focus,
    #pac-input:focus,
    #pac-input-clockout:focus {
        border-color: #4d90fe;
    }

    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }

    #target {
        width: 345px;
    }
</style>
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
@endsection
@section('head_js')
<script type="text/javascript">
    function tokenValidation(token) {
        document.getElementsByName('g_token')[0].value = token;
    }
</script>    
{!! htmlScriptTagJsApi([
    'action' => 'submit',
    'custom_validation' => 'tokenValidation'
]) !!} 
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">{{$masterLayanan->nama_layanan}}</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        @if (auth()->user())
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{route('permohonan.index')}}?idLayanan={{$masterLayanan->id_layanan}}" class="text-muted text-hover-primary">
                    {{$masterLayanan->nama_layanan}}
                </a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
        @endif
        <li class="breadcrumb-item text-dark">
            Data
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <div class="modal fade" tabindex="-1" id="kt_modal_1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Lokasi</h5>
    
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                            </svg>
                        </span>
                    </div>
                    <!--end::Close-->
                </div>
    
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nomor" class="control-label required">Lokasi</label>
                                <div id="container-input">
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Box" />
                                </div>
                                <div class="location-map">
                                    <div style="width: 100%; height: 400px; border: 6px solid #dedede; border-radius: 10px;" id="lokasi"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Pilih</button>
                </div>
            </div>
        </div>
    </div>
    @include('components.notification')
     <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            {!! form($form) !!}
        </div>
    </div>
    <input type="hidden" name="idLayanan" value="{{ request()->query('idLayanan') }}">

    @section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiGp0jrIfFDYobAJfooazOFxtQcAodl0s&callback=initMap&libraries=places"></script>
        <script>
            $('.select2').select2();
            $('.select2').addClass('mb-5').addClass('mt-3');
            $('.date').datepicker({ dateFormat: "yy-mm-dd"}).datepicker("setDate", new Date());
            $('.dateedit').datepicker({ dateFormat: "yy-mm-dd"});

            let wysiwyg = {!! json_encode($wysiwyg) !!}
                wysiwyg.forEach(function(val, index, arr) {
                CKEDITOR.replace(val, {
                    customConfig : 'config.js',
                    toolbar : 'simple',
                    tabSpaces : 12
                })
            })

            $(document).on('focus', '#peta', function(){
                $('#kt_modal_1').modal('show');
            });
        </script>
        <script>
            var maps = [];
            var markers = [];
            let lat = 0;
            let long = 0;

            function showPosition(position) {
                let idElement = "lokasi";
                let dataLat = $('[name="peta"]').val();

                if (dataLat) {
                    lat = dataLat.split(",")[0];
                    long = dataLat.split(",")[1];
                } else {
                    lat = position.coords.latitude;
                    long = position.coords.longitude;
                }

                // $("input[name='peta']").val(lat + ',' + long);
                initializeGMap(lat, long, idElement, true, "pac-input", "pac-input");
            }

            function initMapData() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    alert("Geolocation is not supported by this browser.");
                }
            }
    
            function initializeGMap(
                lat,
                lng,
                element,
                isUpdate,
                idForm,
                nameForm
            ) {
                var peta = new google.maps.LatLng(lat, lng);
    
                var myOptions = {
                    zoom: 15,
                    zoomControl: true,
                    center: peta,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
    
                maps[nameForm] = new google.maps.Map(document.getElementById(element), myOptions);
                maps[nameForm].setCenter(peta);
    
                markers[nameForm] = new google.maps.Marker({
                    position: peta,
                    draggable: false
                });
                markers[nameForm].setMap(maps[nameForm]);

                if (isUpdate) {
                    const input = document.getElementById(idForm);
                    const searchBox = new google.maps.places.SearchBox(input);
                    maps[nameForm].controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                    maps[nameForm].addListener("bounds_changed", () => {
                        searchBox.setBounds(maps[nameForm].getBounds());
                    });
                    searchBox.addListener("places_changed", () => {
                        console.log(searchBox);
                        console.log(nameForm);
                        searchData(searchBox, nameForm);
                    });
    
                    google.maps.event.addListener(maps[nameForm], "click", (event) => {
                        if (markers[nameForm]) {
                            markers[nameForm].setMap(null);
                        }
                        addMarker(event.latLng, nameForm);
                    });
                }
            }
    
            function searchData(searchBox, nameForm) {
                const places = searchBox.getPlaces();
    
                if (places.length == 0) {
                    return;
                }
                // For each place, get the icon, name and location.
                const bounds = new google.maps.LatLngBounds();
                places.forEach((place) => {
                    if (!place.geometry || !place.geometry.location) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    const icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25),
                    };
    
                    addMarker(place.geometry.location, nameForm);
    
                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                maps[nameForm].fitBounds(bounds);
            }
    
            function addMarker(location, nameForm) {
                var latLong = location.toJSON();
                $("input[name='peta']").val(latLong.lat + ',' + latLong.lng);
    
                markers[nameForm] = new google.maps.Marker({
                    position: location,
                    map: maps[nameForm],
                });
            }

            $('#kt_modal_1').on('show.bs.modal', function(event) {
                initMapData();
                $('#container-input').html('<input id="pac-input" class="controls" type="text" placeholder="Search Box" />');
            });
        </script>
    @endsection
</x-base-layout>

