@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">BERITA</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            Data Tilang
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">Data Tilang</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <div class="content">
                <table class="table table-striped" align="center">
                    <tbody>
                      <tr>
                        <td align="center">1</td>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{$nama}}</td>
                      </tr>
                      <tr>
                        <td align="center">2</td>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{$alamat}}</td>
                      </tr>
                      <tr>
                        <td align="center">3</td>
                        <td>Pasal</td>
                        <td>:</td>
                        <td>{{$pasal}}</td>
                      </tr>
                      <tr>
                        <td align="center">4</td>
                        <td>Barang Bukti</td>
                        <td>:</td>
                        <td>{{$barang_bukti}}</td>
                      </tr>
                      <tr>
                        <td align="center">5</td>
                        <td>Biaya Perkara</td>
                        <td>:</td>
                        <td>{{$biaya_perkara}}</td>
                      </tr>
                      <tr>
                        <td align="center">5</td>
                        <td>Denda</td>
                        <td>:</td>
                        <td>{{$denda}}</td>
                      </tr>
                      <tr>
                        <td align="center">6</td>
                        <td>Nomor Kendaraan Bermotor</td>
                        <td>:</td>
                        <td>{{$no_ranmor}}</td>
                      </tr>
                      <tr>
                        <td align="center">7</td>
                        <td>Nomor Register Tilang</td>
                        <td>:</td>
                        <td>{{$no_reg_tilang}}</td>
                      </tr>
                      <tr>
                        <td align="center">8</td>
                        <td>Subsider</td>
                        <td>:</td>
                        <td>{{$subsider}}</td>
                      </tr>
                      <tr>
                        <td align="center">9</td>
                        <td>Tanggal Bayar</td>
                        <td>:</td>
                        <td>{{$tgl_bayar}}</td>
                      </tr>
                      <tr>
                        <td align="center">10</td>
                        <td>Tanggal Sidang</td>
                        <td>:</td>
                        <td>{{$tgl_sidang}}</td>
                      </tr>
                      <tr>
                        <td align="center">11</td>
                        <td>Uang Titipan</td>
                        <td>:</td>
                        <td>{{$uang_titipan}}</td>
                      </tr>
                      <tr>
                        <td align="center">12</td>
                        <td>Tanggal Ambil</td>
                        <td>:</td>
                        <td>{{$tgl_ambil}}</td>
                      </tr>
                      <tr>
                        <td align="center">13</td>
                        <td>Nama Petugas</td>
                        <td>:</td>
                        <td>{{$nama_petugas}}</td>
                      </tr>
                      <tr>
                        <td align="center">14</td>
                        <td>Jenis Kendaraan</td>
                        <td>:</td>
                        <td>{{$jenis_kendaraan}}</td>
                      </tr>
                      <tr>
                        <td align="center">15</td>
                        <td>Kode Satker PN</td>
                        <td>:</td>
                        <td>{{$kode_satker_pn}}</td>
                      </tr>
                      <tr>
                        <td align="center">16</td>
                        <td>Nama Hakim</td>
                        <td>:</td>
                        <td>{{$nama_hakim}}</td>
                      </tr>
                      <tr>
                        <td align="center">17</td>
                        <td>Nama Panitera</td>
                        <td>:</td>
                        <td>{{$nama_panitera}}</td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

</x-base-layout>
