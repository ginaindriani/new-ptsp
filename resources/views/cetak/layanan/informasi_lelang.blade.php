<?php
    function tgl_indo($tanggal){
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    if($tanggal == NULL){
        return NULL;
    }
    
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
?>
@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
    .select2-container--bootstrap5 .select2-selection {
        height: 42.5px;
    }
    .select2-container--bootstrap5 .select2-selection--single.form-select-solid .select2-selection__rendered {
        color: #181C32;
    }
        
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }
    .select2-container--bootstrap5 .select2-selection {
        height: 42.5px;
    }
    .select2-container--bootstrap5 .select2-selection--single.form-select-solid .select2-selection__rendered {
        color: #181C32;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Informasi Lelang Barang Sitaan</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            Informasi Lelang Barang Sitaan
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <br>
            <div class="card-title col-12">
                <h3 class="fw-bolder">Informasi Lelang Barang Sitaan</h3>
            </div>
            <br>&nbsp;
            <div class="col-12">
                <form action="{{ url('layanan/permohonan/lelang/search') }}" method="POST">  @csrf
                    <div class="col-12 row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Pilih Satuan Kerja</label>
                                <select class="form-control select2-container" id="satker" name="satker">
                                <option value="">-- Semua --</option>
                                @foreach($datasatker as $ds)
                                    <option value="{{$ds->id_satker}}">{{$ds->nama_satker}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2-container" id="status_laku" name="status_laku">
                                <option value="">-- Pilih --</option>
                                <option value="1" >BELUM TERLELANG</option>
                                <option value="2">SUDAH TERLELANG</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary btn-block waves-effect waves-light"  type="submit" style="color:white;width:100%;margin-top:2%">
                                    Filter
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <br>
        </div>
        <div class="card-body pt-6">
            <div class="table-responsive ">
                <table class="table table-striped" style="width:100%;" id="tableuser">
                  <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th style="text-align: center;">Satuan Kerja</th>
                        <th style="text-align: center;">Nama Barang</th>
                        <th style="text-align: center;">Harga Barang</th>
                        <th style="text-align: center;">Status Lelang</th>
                        <th style="text-align: center;">Tanggal Lelang</th>
                        <th style="text-align: center;">Foto Barang</th>
                        <th style="text-align: center;">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                      @php $i=1 @endphp
                      @foreach($lelang as $l)
                      <tr>
                          <td>{{ $i++ }}</td>
                          <td>{{ $l->nama_satker }}</td>
                          <td>{{ $l->nama_barang_lelang }}</td>
                          <td>{{ $l->harga_barang }}</td>
                            @if($l->status_laku == 1)
                            <td>Belum Terlelang</td>
                            @else
                            <td>Sudah Terlelang</td>
                            @endif
                          <td>{{ tgl_indo($l->tanggal_lelang) }}</td>
                          <td><img src="{{ asset($l->foto) }}" width="50" height="50"></td>
                          <td><a href="" class="btn btn-primary btn-block" id="editCompany" data-toggle="modal" data-target='{{'#practice_modal'.$l->id}}' data-id="{{ $l->id }}">Detail</a></td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
        </div>
        <!--end::Card body-->
        @foreach($lelang as $l)
        <div class="modal fade" id="{{'practice_modal'.$l->id}}">
            <div class="modal-dialog">
               <form id="companydata">
                    <div class="modal-content">
                    <div class="modal-body">
                        <center>
                            <img src="{{asset($l->foto)}}" id="foto" width="200" height="200">
                            <h1>{{$l->nama_barang_lelang}}</h1>
                            <h2>{{$l->harga_barang}}</h2>
                            <p>{!! $l->deskripsi_barang !!}</p>
                            <br>&nbsp;
                            @if($l->status_laku == 1)
                            <h2>Belum Terlelang</h2>
                            @else
                            <h2>Sudah Terlelang</h2>
                            @endif
                            <p>Tanggal Lelang : {{ tgl_indo($l->tanggal_lelang) }}</p>
                            <p>Link Lelang : <a href="{{$l->link_lelang}}"><u>{{$l->link_lelang}}</u></a></p>
                            <p>{{ $l->nama_satker }}</p>
                        </center>
                    </div>
                </div>
               </form>
            </div>
        </div>
        @endforeach
    </div>
    <!--end::Card-->


    

    @section('scripts')
        <script type="text/javascript">
            $(document).ready(function() {
                $(".select2-container").select2();
                $('body').on('click', '#editCompany', function (event) {
                        event.preventDefault();
                        var id = $(this).data('id');
                        $.get('lelang/detail/' + id, function (data) {
                            console.log(data);
                            $('#practice_modal' + id).modal('show');
                            
                        })
                    });
            });
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
        
            //var table = $('#tableuser').DataTable();
            var loadTable =  function () {    
            var table = $('#tableuser').DataTable({
                scrollX: true,
                bDestroy: true,
                paging: true,
                responsive: true,
                processing: true,
                language: {
                searchPlaceholder: 'Search...',
                sEmptyTable:   'Tidak ada data yang tersedia pada tabel ini',
                sProcessing:   'Sedang memproses...',
                sLengthMenu:   'Tampilkan _MENU_ entri',
                sZeroRecords:  'Tidak ditemukan data yang sesuai',
                sInfo:         'Menampilkan _START_ sampai _END_ dari _TOTAL_ entri',
                sInfoEmpty:    'Menampilkan 0 sampai 0 dari 0 entri',
                sInfoFiltered: '(disaring dari _MAX_ entri keseluruhan)',
                sInfoPostFix:  '',
                sSearch:       '',
                sUrl:          '',
                oPaginate: {
                    sFirst:    'Pertama',
                    sPrevious: 'Sebelumnya',
                    sNext:     'Selanjutnya',
                    sLast:     'Terakhir'
                }
                },
                columnDefs : [
                    {
                    targets: 0, // no
                    className: 'text-center'
                    },
                    {
                    targets: 1, // no
                    className: 'text-center'
                    },
                    {
                    targets: 2, // no
                    className: 'text-center'
                    },
                            {
                    targets: 3, // no
                    className: 'text-center'
                    }
                            
                ],
                autoWidth: true
            });
        
                    $('input[type="search"]').unbind();
                    $('input[type="search"]').bind('keyup', function(e) {
                    if (e.keyCode == 13) {
                        table.search(this.value).draw();
                    }
                    });
            
        
            };
        
            $(document).ready(function() {
                    loadTable();      
            // show the alert
                    setTimeout(function() {
                    $("#alert-msg").slideUp(100);
                    }, 500);

                    

                });
        
            $(document).on('click', '.btnDelete[data-remove]', function (e) { 
            e.preventDefault();
            var id  = $(this).data('id');
            var url = $(this).data('remove');
            // confirm then
            swal({
                title: "Apakah Kamu Benar-benar ingin Menghapus Data?",
                text: "You will not be able to recover this imaginary file!",
                icon: 'warning',
                dangerMode: true,
                buttons: {
                cancel: 'Tidak, Batalkan!',
                delete: 'Ya, Hapus Data ini!'
                }
            }).then(function (willDelete) { // <--
                if (willDelete) { // <-- if confirmed
                    $.ajax({
                    // url : $(this).data('remove'),
                    url : url,
                    type: 'POST',
                    dataType : 'json',
                    headers: { 'CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data : { 'id': id, method : '_DELETE' , submit : true},
                    success:function(data){
                    console.log(data);
                    if (data == 'Success') {
                        swal("Terhapus!", "Data Berhasil di Hapus, Silahkan Refresh Halaman Ini", "success").then(function() {
                            location.reload();
                        });
                    }
                    }
                });
                } else {
                swal("Data berhasil dipertahankan", {
                    title: 'Dibatalkan',
                    icon: "error",
                });
                }
            });
            
            });
        </script>
        
    @endsection
</x-base-layout>
