@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">BERITA</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            Data Tilang
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">Data Info Tahanan</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <div class="content">
                <table class="table table-striped" align="center">
                    <tbody>
                      <tr>
                        <td align="center">1</td>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{$nama}}</td>
                      </tr>
                      <tr>
                        <td align="center">2</td>
                        <td>No Identitas</td>
                        <td>:</td>
                        <td>{{$nik}}</td>
                      </tr>
                      <tr>
                        <td align="center">3</td>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>{{$alamat}}</td>
                      </tr>
                      <tr>
                        <td align="center">4</td>
                        <td>Tempat Lahir</td>
                        <td>:</td>
                        <td>{{$tempat_lahir}}</td>
                      </tr>
                      <tr>
                        <td align="center">5</td>
                        <td>Tanggal Lahir</td>
                        <td>:</td>
                        <td>{{$tanggal_lahir}}</td>
                      </tr>
                      <tr>
                        <td align="center">6</td>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{$jk}}</td>
                      </tr>
                      <tr>
                        <td align="center">7</td>
                        <td>No Hp</td>
                        <td>:</td>
                        <td>{{$no_hp}}</td>
                      </tr>
                      <tr>
                        <td align="center">8</td>
                        <td>Nama Kejaksaan</td>
                        <td>:</td>
                        <td>{{$nama_satker}}</td>
                      </tr>
                      <tr>
                        <td align="center">9</td>
                        <td>Ditahan Sejak</td>
                        <td>:</td>
                        <td>{{$ditahan_sejak}}</td>
                      </tr>
                      <tr>
                        <td align="center">10</td>
                        <td>Jaksa</td>
                        <td>:</td>
                        <td>{{$jaksa}}</td>
                      </tr>
                      <tr>
                        <td align="center">11</td>
                        <td>Penyidik</td>
                        <td>:</td>
                        <td>{{$polres}}</td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

</x-base-layout>
