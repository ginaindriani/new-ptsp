@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background-color: #e6fff5;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }

    .highcharts-figure,
    .highcharts-data-table table {
    min-width: 360px;
    max-width: 800px;
    margin: 1em auto;
    }

    .highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #ebebeb;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
    }

    .highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
    }

    .highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
    padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
    background: #f1f7ff;
    }
</style>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
@endsection

<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')

    <div class="card card-xxl-stretch mb-5">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4 card-container">
                    <label for="nomor" class="control-label required">Satker</label>
                    <select class="form-control form-select-solid" onchange="findChartData()" data-kt-select2="true" id="satker_select_option">
                                <option value="0" selected>KEJAKSAAN RI</option>
                        @foreach ($satker as $list)
                                 <option value="{{$list->id_satker}}">{{$list->nama_satker}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4 card-container">
                    <label for="nomor" class="control-label required" >Tahun</label>
                    <select class="form-control form-select-solid" onchange="findChartData()" data-kt-select2="true" id="year_select_option">

                        @php
                            $dataTahun = array();
                            foreach($tahun as $list){
                                array_push($dataTahun,$list->tahun);
                            }
                            if ($dataTahun) {
                                $maxTahun = max($dataTahun);
                            }else{
                                $maxTahun = max([2020,2021,2022,2019]);
                            }
                        @endphp

                        @foreach ($tahun as $list)
                            @if (date('Y') == $list->tahun)
                                <option value="{{$list->tahun}}" selected>{{$list->tahun}}</option>
                            @elseif ($maxTahun == $list->tahun)
                                <option value="{{$list->tahun}}" selected>{{$list->tahun}}</option>
                            @else
                                <option value="{{$list->tahun}}">{{$list->tahun}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <div class="col-md">
                <figure class="highcharts-figure">
                    <div id="container"></div>
                </figure>
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

@section('scripts')
<script>

    satker = $("#satker_select_option").val();
    tahun = $("#year_select_option").val();
    $(document).ready(function(){
        findChartData(satker,tahun);
    })




const _token = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': _token
                }
            });

function findChartData(satker,tahun){

    satker = $("#satker_select_option").val();
    tahun = $("#year_select_option").val();


    url = "{{route('permohonan.chartPerkara')}}"

    $.ajax({
        url : url,
        data: {satker : satker,
                tahun : tahun},
        type: "POST",
        dataType: "JSON",
        success: function(data){
            chartCanvas(data)
        },
        error: function(){

        }
    })

}

function chartCanvas(data){

    Highcharts.chart('container', {

        title: {
        text: 'Informasi Trend Laporan Perkara Kejaksaan RI'
        },

        subtitle: {
        text: 'Source: Kejaksaan RI'
        },

        yAxis: {
        title: {
            text: 'Number of People'
        }
        },

        xAxis: {
            categories:["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sept","Okt","Nov","Des"]
        },

        legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
        },


        series: data,
        responsive: {
        rules: [{
            condition: {
            maxWidth: 500
            },
            chartOptions: {
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            }
            }
        }]
        }

    });

}
</script>
@endsection
</x-base-layout>

