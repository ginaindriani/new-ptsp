@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
<style>
    #map {
        min-height: 25px;
        border: 6px solid #f1f1f1;
        border-radius: 6px;
    }

    .leaflet-popup {
        width: 600px;
    }

    .info {
        padding: 6px 8px;
        font: 14px/16px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgb(0 0 0 / 20%);
        border-radius: 5px;
    }

    .legend {
        text-align: left;
        line-height: 18px;
        color: #555;
    }

    .legend i {
        width: 18px;
        height: 18px;
        float: left;
        margin-right: 8px;
        opacity: 0.7;
    }

    .info-color {
        width: 130px;
        padding: 6px 8px;
        font: 14px/16px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgb(0 0 0 / 20%);
        border-radius: 5px;
    }

    .color-legend {
        width: 20px;
        height: 20px;
        margin-right: 5px;
    }

    .number-legend {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .number-legend:last-child {
        margin-bottom: 0px;
    }

    .citizen-data {
        font-size: 16px;
        font-weight: 700;
    }

    #data-penduduk {
        margin: 20px;
        padding: 5px;
        text-align: right;
        float: right;
        font-size: 20px;
    }

</style>
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">{{$masterLayanan->nama_layanan}}</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        @if (auth()->user())
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{route('permohonan.index')}}?idLayanan={{$masterLayanan->id_layanan}}" class="text-muted text-hover-primary">
                    {{$masterLayanan->nama_layanan}}
                </a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
        @endif
        <li class="breadcrumb-item text-dark">
            Data
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    @include('components.notification')
     <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3> 
            </div>
        </div>
        <div class="card-body pt-6">
            <div id="map" style="height: 100vh;"></div>
            <hr>
        </div>
    </div>

    @section('scripts')
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
        <script>
            const loading =
                '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>';
    
            var info = L.control();
    
            function initMap() {
                // Creating map options
                const latLimit1 = 12.017059633688106;
                const langLimit1 = 88.10764483812892;
                const latLimit2 = -11.786986468956854;
                const langLimit2 = 148.1235953344222;
                var mapOptions = {
                    center: [-0.7693592346777363, 113.56961917231527],
                    zoom: 6,
                    minZoom: 5
                }
                // Creating a map object
                var map = new L.map('map', mapOptions);
                // Creating a Layer object
                var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
                // Adding layer to the map
                map.addLayer(layer);
                // set geoJson
                var prov = "{{ $provs }}";
                var provJson = JSON.parse(prov.replace(/&quot;/g, '"'));
                for (const key in provJson) {
                    if (Object.hasOwnProperty.call(provJson, key)) {
                        const element = provJson[key];
                        setGeojson(map, element.id);
                    }
                }
                // set legend
                setLegend(map);
                // info
                info.onAdd = function(map) {
                    this._div = L.DomUtil.create('div', 'info');
                    this.update();
                    return this._div;
                };
                info.update = function(props) {
                    this._div.innerHTML = '<h4>Tahun {{ $yearRangeName }}</h4>' + (props ?
                        props.nama_wilayah + '<br /><h6>' + props.jumlah_perkara + ' Perkara</h6>' :
                        'Provinsi');
                };
                info.addTo(map);
                // Set limit
                // L.marker([latLimit1, langLimit1]).addTo(map);
                // L.marker([latLimit2, langLimit2]).addTo(map);
                setMarker(map);
                // set marker
                setLimitBound(
                    map,
                    latLimit1,
                    langLimit1,
                    latLimit2,
                    langLimit2,
                );
            }
    
            function setMarker(map) {
                var datas = "{{ $datas }}";
                var resultJson = JSON.parse(datas.replace(/&quot;/g, '"'));
    
                // Creating a marker
                var markers = [];
                for (const key in resultJson) {
                    if (Object.hasOwnProperty.call(resultJson, key)) {
                        const element = resultJson[key];
                        const lat = parseFloat(element.lat.replace(',', '.'));
                        const long = parseFloat(element.long.replace(',', '.'));
    
                        // Creating Marker Options
                        const markerOptions = {
                            title: element.satker,
                            satkerId: element.id,
                            clickable: true
                        }
    
                        // let htmlPopup = `<div class="popup-custom" data-id="${element.id}"></div>`;
    
                        var shelter = L.marker([lat, long], markerOptions)
                        // .bindPopup(htmlPopup, {
                        //     maxWidth: 'auto'
                        // })
                        ;
                        markers.push(shelter);
                    }
                }
    
                var shelterMarkers = new L.FeatureGroup();
                for (const key in markers) {
                    if (Object.hasOwnProperty.call(markers, key)) {
                        const element = markers[key];
                        shelterMarkers.addLayer(element);
                    }
                }
    
                shelterMarkers.on("click", function(event) {
                    var clickedMarker = event.layer;
                    dataPerkara(event.layer.options.satkerId);
                });
    
                map.on('zoomend', function() {
                    if (map.getZoom() < 8) {
                        map.removeLayer(shelterMarkers);
                    } else {
                        map.addLayer(shelterMarkers);
                    }
                });
            }
    
            function setLimitBound(
                map,
                latLimit1,
                langLimit1,
                latLimit2,
                langLimit2
            ) {
                var corner1 = L.latLng(latLimit1, langLimit1),
                    corner2 = L.latLng(latLimit2, langLimit2),
                    bounds = L.latLngBounds(corner1, corner2);
    
                map.setMaxBounds(bounds);
                map.on('drag', function() {
                    map.panInsideBounds(bounds, {
                        animate: false
                    });
                });
            }
    
            function dataPerkara(satkerId) {
                $.ajax({
                        url: `{{ url('layanan/permohonan/${satkerId}/detail-map') }}`,
                        beforeSend: function() {
                            // blockPage();
                        }
                    }).done(function(res) {
                        console.log(res);
                        $('#modal-container-custom').html(res);
                        $('#kt_modal_1').modal('show');
                    })
                    .fail(function(err) {
                        // $('.popup-custom').html('<p>Gagal mengambil data</p>');
                    })
                    .always(function(res) {
                        // unblockPage();
                    });
            }
    
            function getColor(d) {
                return d <= 500 ? '#4fff55' :
                    d > 500 && d <= 1500 ? '#ecff4f' :
                    d > 1500 && d <= 3000 ? '#f44336' :
                    d > 3000 ? '#b30c00' :
                    '#4fff55';
            }
    
            function style(feature) {
                return {
                    weight: 2,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.7,
                    fillColor: getColor(feature.properties.jumlah_perkara)
                };
            }
    
            function highlightFeature(e) {
                var layer = e.target;
    
                layer.setStyle({
                    weight: 5,
                    color: '#666',
                    dashArray: '',
                    fillOpacity: 0.7
                });
    
                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    layer.bringToFront();
                }
    
                info.update(layer.feature.properties);
            }
    
            var geojson;
    
            function resetHighlight(e) {
                geojson.resetStyle(e.target);
                info.update();
            }
    
            var mapGlobal;
    
            function zoomToFeature(e) {
                mapGlobal.fitBounds(e.target.getBounds());
            }
    
            function onEachFeature(feature, layer) {
                layer.on({
                    mouseover: highlightFeature,
                    mouseout: resetHighlight,
                    click: zoomToFeature
                });
            }
    
            let repeat = 3;
            let limit = 0;
    
            function setGeojson(map, provId) {
                $.ajax({
                        url: "{{url('layanan/permohonan/data-geo-province')}}/" + provId + "?dates={{ str_replace(' ', '', $yearRangeName) }}",
                        beforeSend: function() {
                            // $('.popup-custom').html(loading);
                        }
                    }).done(function(res) {
                        mapGlobal = map;
                        var statesData = res.data;
                        // get color depending on population density value
                        geojson = L.geoJson(statesData, {
                            style: style,
                            onEachFeature: onEachFeature
                        }).addTo(map);
                    })
                    .fail(function(err) {
                        if (limit < repeat) {
                            setGeojson(map, provId);
                            limit++;
                        } else {
                            limit = 0;
                        }
                    })
                    .always(function(res) {});
            }
    
            function setLegend(map) {
                var legend = L.control({
                    position: 'bottomright'
                });
    
                legend.onAdd = function(map) {
                    var div = L.DomUtil.create('div', 'info-color legend'),
                        grades = [0, 500, 1500, 3000],
                        labels = [],
                        numbers = [],
                        from, to;
    
    
                    for (var i = 0; i < grades.length; i++) {
                        from = grades[i];
                        to = grades[i + 1];
    
                        labels.push(
                            '<div class="number-legend"><div class="color-legend" style="background:' + getColor(from +
                                1) + '"></div>' +
                            '<div>' +
                            from + (to ? '&ndash;' + to : '+') + '</div></div>');
                    }
    
                    let dataHtml = labels.join('');
                    div.innerHTML = dataHtml;
                    return div;
                };
    
                legend.addTo(map);
            }
    
            initMap();
        </script>
    @endsection
</x-base-layout>

