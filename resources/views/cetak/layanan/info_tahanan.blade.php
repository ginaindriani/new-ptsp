@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">{{$masterLayanan->nama_layanan}}</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            {{$masterLayanan->nama_layanan}}
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <form method='get' class="lacak_berkas_1 mb-2 mb-md-0" action="">
                <div class="form-row justify-content-center">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-group mr-0 mr-lg-2">
                            <input class="form-control form-control-solid rounded-pill"
                                name="text" type="text" id="text"
                                placeholder="Masukkan NIK Tahanan">
                        </div>
                    </div>
                    <br>
                    <div class="col-lg-12 col-md-12"><center><button type="button" id="btn" 
                        class="btn btn-primary btn-block btn-marketing rounded-pill"
                        >Cari</button></center>
                    </div>
                </div>
            </form>
        </div>
        <!--end::Card body-->
    </div>
    
    <!--end::Card-->
    @section('scripts')
      <script>
        var base_url = "{{ url('') }}";
        $("#btn").click( function() {
            var url = base_url +  "/layanan/permohonan/tahanan/" + $("#text").val();
            window.location.assign(url);
        });
      </script>
    @endsection

</x-base-layout>
