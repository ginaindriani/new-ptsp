@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
<style>
  .card-header.py-3{
      align-items: center !important;
  }
  @-webkit-keyframes spinner-left {
      0% {
          -webkit-transform: rotate(0deg);
                  transform: rotate(0deg);
      }
      100% {
          -webkit-transform: rotate(360deg);
                  transform: rotate(360deg);
      }
      }
      @keyframes spinner-left {
      0% {
          -webkit-transform: rotate(0deg);
                  transform: rotate(0deg);
      }
      100% {
          -webkit-transform: rotate(360deg);
                  transform: rotate(360deg);
      }
      }

      @-webkit-keyframes spinner-right {
      0% {
          -webkit-transform: rotate(0deg);
                  transform: rotate(0deg);
      }
      100% {
          -webkit-transform: rotate(-360deg);
                  transform: rotate(-360deg);
      }
  }
  .ajax-loading {
      position: relative;
      z-index: 2000;
      bottom: 20px;
      padding: 3px;
      border-radius: 50%;
  }

      html[dir="ltr"] .ajax-loading {
          left: 20px;
      }

      .light_theme .ajax-loading {
          background-color: #fff;
          -webkit-box-shadow: 11px 10px 38px rgba(0, 0, 0, 0.12);
                  box-shadow: 11px 10px 38px rgba(0, 0, 0, 0.12);
      }

      .ajax-loading span {
          display: block;
          height: 29px;
          width: 29px;
          border-width: 3px;
          border-style: solid;
          border-radius: 50%;
          margin: auto;
          margin-top:20px;
      }

      .ajax-loading span {
          -webkit-animation: spinner-left 0.6s linear infinite;
              animation: spinner-left 0.6s linear infinite;
      }
      .ajax-loading span {
          border-color: transparent #009EF7;
      }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">{{$masterLayanan->nama_layanan}}</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            {{$masterLayanan->nama_layanan}}
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <form method='get' class="lacak_berkas_1 mb-2 mb-md-0" action="">
                <div class="form-row justify-content-center">
                    <div class="col-lg-12 col-md-12">
                        <div class="chart-area">
                            <canvas id="layananPengaduanChart"></canvas>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-body pt-6">
            <div class="table-responsive ">
                <table class="table table-striped" style="width:100%;" id="tableuser">
                <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th style="text-align: center;">Jenis Pengaduan</th>
                        <th style="text-align: center;">Jumlah Pengaduan</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1 @endphp
                    @foreach($data_pengaduan as $dp)
                    <tr>
                        <td style="text-align: center;">{{ $i++ }}</td>
                        <td style="text-align: center;">{{ $dp->nama_layanan }}</td>
                        <td style="text-align: center;">{{ $dp->jumlah }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
        <!--end::Card body-->
    </div>
    
    <!--end::Card-->
    @section('scripts')
      <script>
        var pengaduan = {!! json_encode($hitung_data_pengaduan_perbulan) !!};
        console.log(pengaduan,'data pengaduan');
        // 
        var arrPengaduan = [];
        for(var i in pengaduan.original){
            arrPengaduan.push(pengaduan.original[i].count)
        }
        // 
        var laypengaduan = document.getElementById("layananPengaduanChart");
        var pengaduanChart = new Chart(laypengaduan, {
        type: 'line',
        data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
            label: "Jumlah",
            lineTension: 0.3,
            backgroundColor: "rgba(78, 115, 223, 0.05)",
            borderColor: "rgba(78, 115, 223, 1)",
            pointRadius: 3,
            pointBackgroundColor: "rgba(78, 115, 223, 1)",
            pointBorderColor: "rgba(78, 115, 223, 1)",
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
            pointHoverBorderColor: "rgba(78, 115, 223, 1)",
            pointHitRadius: 10,
            pointBorderWidth: 2,
            data: arrPengaduan,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
            padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
            }
            },
            scales: {
            xAxes: [{
                time: {
                unit: 'date'
                },
                gridLines: {
                display: false,
                drawBorder: false
                },
                ticks: {
                maxTicksLimit: 7
                }
            }],
            yAxes: [{
                ticks: {
                maxTicksLimit: 5,
                padding: 10,
                // Include a dollar sign in the ticks
                callback: function(value, index, values) {
                    return '$' + number_format(value);
                }
                },
                gridLines: {
                color: "rgb(234, 236, 244)",
                zeroLineColor: "rgb(234, 236, 244)",
                drawBorder: false,
                borderDash: [2],
                zeroLineBorderDash: [2]
                }
            }],
            },
            legend: {
            display: false
            },
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            titleMarginBottom: 10,
            titleFontColor: '#6e707e',
            titleFontSize: 14,
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            intersect: false,
            mode: 'index',
            caretPadding: 10,
            callbacks: {
                label: function(tooltipItem, chart) {
                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
                }
            }
            }
        }
        });
      </script>
    @endsection

</x-base-layout>
