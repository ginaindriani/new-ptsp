@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
    iframe body{
        overflow: hidden !important;
    }
    .card-frame{
        height: 100%;

    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">BERITA</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            Pendaftaran Layanan
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card card-frame" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
          <div class="card-title">
              <h3 class="fw-bolder">Pendaftaran Layanan</h3>
          </div>
        </div>
        <div class="card-body pt-6">
          <form>
            @csrf
              <div class="form-row justify-content-center">
                <center><iframe src="https://bukutamu.kejaksaan.go.id/ptsp/pilihsatker" style="border:0px #ffffff none;" name="myiFrame" scrolling="yes" frameborder="1" marginheight="0px" marginwidth="0px" height="500px" width="1000px"  allow="camera; microphone" allowfullscreen></iframe></center>
              </div>
            </form>
            <h4 class="title">Jika anda telah terdaftar dan memiliki QR Code untuk melakukan pelayanan</h4>
            <p class="sub-title">
                Silahkan <a href="{{url('scanqr')}}"><link>Klik Disini</link><a> untuk mendapatkan pelayanan
            </p>
        </div>
    </div>
    <!--end::Card-->

</x-base-layout>
