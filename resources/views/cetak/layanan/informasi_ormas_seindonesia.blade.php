@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">{{$masterLayanan->nama_layanan}}</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            {{$masterLayanan->nama_layanan}}
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <div class="table-responsive">
                @include('cetak.layanan.table._info_table')
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->
    <div class="modal fade" tabindex="-1" id="kt_modal_1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Lokasi</h5>
    
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-2x">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                            </svg>
                        </span>
                    </div>
                    <!--end::Close-->
                </div>
    
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 modal_body_map">
                                <div class="location-map" id="location-map">
                                    <div style="width: 600px; height: 400px;" id="map_canvas"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</x-base-layout>
