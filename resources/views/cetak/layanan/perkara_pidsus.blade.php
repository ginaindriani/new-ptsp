@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
    .select2-container--bootstrap5 .select2-selection {
        height: 42.5px;
    }
    .select2-container--bootstrap5 .select2-selection--single.form-select-solid .select2-selection__rendered {
        color: #181C32;
    }
        
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }
    .select2-container--bootstrap5 .select2-selection {
        height: 42.5px;
    }
    .select2-container--bootstrap5 .select2-selection--single.form-select-solid .select2-selection__rendered {
        color: #181C32;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
</style>
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Data Perkara Tindak Pidana Khusus</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            Data Perkara Tindak Pidana Khusus
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
            <br>
            <div class="card-title col-12">
                <h3 class="fw-bolder">Data Perkara Tindak Pidana Khusus</h3>
            </div>
            <br><br>
            <div class="col-12">
                <form action="{{ url('layanan/permohonan/perkara-pidsus/search') }}" method="POST">  @csrf
                    <div class="col-12 row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Satuan Kerja</label>
                                <select class="form-control select2-container" id="satker" name="satker">
                                <option value="">-- Pilih --</option>
                                @foreach($datasatker as $ds)
                                    <option value="{{$ds->kode_satker_cms}}" <?php echo $ds->kode_satker_cms == $satker ? 'selected' : ''; ?>>{{$ds->nama_satker}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Jenis Pidana</label>
                                <select class="form-control select2-container" id="jenis" name="jenis">
                                <option value="">-- Pilih --</option>
                                <option value="1" <?php echo $jenis == '1' ? 'selected' : ''; ?>>Tindak Pidana Korupsi & TPPU</option>
                                <option value="2" <?php echo $jenis == '2' ? 'selected' : ''; ?>>Tindak Pidana Perpajakan & TPPU</option>
                                <option value="3" <?php echo $jenis == '3' ? 'selected' : ''; ?>>Tindak Pidana Kepabeanan, Cukai & TPPU</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select class="form-control select2-container" id="tahun" name="tahun">
                                <option value="">-- Pilih --</option>
                                <option value="2020" <?php echo $tahun == '2020' ? 'selected' : ''; ?>>2020</option>
                                <option value="2021" <?php echo $tahun == '2021' ? 'selected' : ''; ?>>2021</option>
                                <option value="2022" <?php echo $tahun == '2022' ? 'selected' : ''; ?>>2022</option>
                                <option value="2023" <?php echo $tahun == '2023' ? 'selected' : ''; ?>>2023</option>
                                <option value="2024" <?php echo $tahun == '2024' ? 'selected' : ''; ?>>2024</option>
                                <option value="2025" <?php echo $tahun == '2025' ? 'selected' : ''; ?>>2025</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary btn-block waves-effect waves-light"  type="submit" style="color:white;width:100%;margin-top:2%">
                                    Filter
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <br>
        </div>
        <div class="card-body pt-6">
            <div class="table-responsive ">
                <table class="table table-striped" style="width:100%;" id="tableuser">
                  <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th style="text-align: center;">Nama Satker</th>
                        <th style="text-align: center;">No SPDP</th>
                        <th style="text-align: center;">Tanggal SPDP</th>
                        <th style="text-align: center;">Nama Tersangka</th>
                        <th style="text-align: center;">Jenis Pidana</th>
                        <th style="text-align: center;">Warga negara</th>
                    </tr>
                  </thead>
                  <tbody>
                      @php $i=1 @endphp
                      @foreach($data as $d)
                      <tr>
                          <td>{{ $i++ }}</td>
                          <td>{{ $d->nama_satker }}</td>
                          <td>{{ $d->no_spdp }}</td>
                          <td>{{ $d->tgl_spdp }}</td>
                          <td>{{ $d->nama_tsk }}</td>
                          <td>{{ $d->jenis_pidana }}</td>
                          <td>{{ $d->warganegara }}</td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->




    @section('scripts')
        <script type="text/javascript">
            $(document).ready(function() {
                $(".select2-container").select2();
            });
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
        
            //var table = $('#tableuser').DataTable();
            var loadTable =  function () {    
            var table = $('#tableuser').DataTable({
                scrollX: true,
                bDestroy: true,
                paging: true,
                responsive: true,
                processing: true,
                language: {
                searchPlaceholder: 'Search...',
                sEmptyTable:   'Tidak ada data yang tersedia pada tabel ini',
                sProcessing:   'Sedang memproses...',
                sLengthMenu:   'Tampilkan _MENU_ entri',
                sZeroRecords:  'Tidak ditemukan data yang sesuai',
                sInfo:         'Menampilkan _START_ sampai _END_ dari _TOTAL_ entri',
                sInfoEmpty:    'Menampilkan 0 sampai 0 dari 0 entri',
                sInfoFiltered: '(disaring dari _MAX_ entri keseluruhan)',
                sInfoPostFix:  '',
                sSearch:       '',
                sUrl:          '',
                oPaginate: {
                    sFirst:    'Pertama',
                    sPrevious: 'Sebelumnya',
                    sNext:     'Selanjutnya',
                    sLast:     'Terakhir'
                }
                },
                columnDefs : [
                    {
                    targets: 0, // no
                    className: 'text-center'
                    },
                    {
                    targets: 1, // no
                    className: 'text-center'
                    },
                    {
                    targets: 2, // no
                    className: 'text-center'
                    },
                            {
                    targets: 3, // no
                    className: 'text-center'
                    }
                            
                ],
                autoWidth: true
            });
        
                    $('input[type="search"]').unbind();
                    $('input[type="search"]').bind('keyup', function(e) {
                    if (e.keyCode == 13) {
                        table.search(this.value).draw();
                    }
                    });
            
        
            };
        
            $(document).ready(function() {
                    loadTable();      
            // show the alert
                    setTimeout(function() {
                    $("#alert-msg").slideUp(100);
                    }, 500);
                });
        
            $(document).on('click', '.btnDelete[data-remove]', function (e) { 
            e.preventDefault();
            var id  = $(this).data('id');
            var url = $(this).data('remove');
            // confirm then
            swal({
                title: "Apakah Kamu Benar-benar ingin Menghapus Data?",
                text: "You will not be able to recover this imaginary file!",
                icon: 'warning',
                dangerMode: true,
                buttons: {
                cancel: 'Tidak, Batalkan!',
                delete: 'Ya, Hapus Data ini!'
                }
            }).then(function (willDelete) { // <--
                if (willDelete) { // <-- if confirmed
                    $.ajax({
                    // url : $(this).data('remove'),
                    url : url,
                    type: 'POST',
                    dataType : 'json',
                    headers: { 'CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data : { 'id': id, method : '_DELETE' , submit : true},
                    success:function(data){
                    console.log(data);
                    if (data == 'Success') {
                        swal("Terhapus!", "Data Berhasil di Hapus, Silahkan Refresh Halaman Ini", "success").then(function() {
                            location.reload();
                        });
                    }
                    }
                });
                } else {
                swal("Data berhasil dipertahankan", {
                    title: 'Dibatalkan',
                    icon: "error",
                });
                }
            });
            
            });
        </script>
        
    @endsection
</x-base-layout>
