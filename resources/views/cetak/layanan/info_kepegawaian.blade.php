@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="{{asset('demo1/css/style.bundle.css')}}">
<style>
    .grecaptcha-badge {
        bottom: 100px !important;
    }
    .user_column {
        border: 1px solid #e4e6ef; border-radius: 10px;
    }

    /* Hide scrollbar for Chrome, Safari and Opera */
    .user_column::-webkit-scrollbar {
        display: none;
    }

    /* Hide scrollbar for IE, Edge and Firefox */
    .user_column {
    -ms-overflow-style: none;  /* IE and Edge */
        scrollbar-width: none;  /* Firefox */
    }
    /* @media (min-width: 1400px) {
        .container-xxl, .container-xl, .container-lg, .container-md, .container-sm, .container {
            max-width: 900px !important;
        }
    } */
</style>
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">{{$masterLayanan->nama_layanan}}</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        @if (auth()->user())
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{route('permohonan.index')}}?idLayanan={{$masterLayanan->id_layanan}}" class="text-muted text-hover-primary">
                    {{$masterLayanan->nama_layanan}}
                </a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
        @endif
        <li class="breadcrumb-item text-dark">
            Data
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    @include('components.notification')
    <div class="card" id="container-card-info">
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group d-flex">
                        <div class="form-check form-check-custom form-check-solid form-check-sm me-5">
                            <input class="form-check-input" type="radio" value="nip" name="search_by" id="Nip"  checked/>
                            <label class="form-check-label" for="Nip">
                                Nip
                            </label>
                        </div>
                        <div class="form-check form-check-custom form-check-solid form-check-sm">
                            <input class="form-check-input" type="radio" value="nama" name="search_by" id="Nama"  />
                            <label class="form-check-label" for="Nama">
                                Nama
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-5">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Cari ">
                            <button class="input-group-text btn btn-primary cari-nip" id="basic-addon2">
                                <i class="bi bi-search fs-2x"></i> Cari
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12">
                    <div id="alert_searching" style="display: none;">
                        <div class="alert alert-dismissible bg-light-info d-flex flex-center flex-column py-10 px-10 px-lg-20 mb-10">
                            <!--begin::Icon-->
                            <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                            <span class="svg-icon svg-icon-5tx svg-icon-info mb-5">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                                    <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"></rect>
                                    <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"></rect>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                            <!--end::Icon-->
                            <!--begin::Content-->
                            <div class="text-center text-dark">
                                <h1 class="fw-bolder mb-5" id="message-alert">Data tidak ditemukan</h1>
                            </div>
                            <!--end::Content-->
                        </div>
                    </div>
                    <div id="result_data"></div>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
        <script>
            let page = 1;
            let limit = 6;
            let targetInfoPegawai = document.querySelector("#container-card-info");
            let blockUIInfo = new KTBlockUI(targetInfoPegawai, {
                message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Loading...</div>',
            });
        </script>
        <script>
            $(document).on('click', '.cari-nip', function(){
                page = 1;
                getDataPegawai();
            });

            $(document).on('click', '.previous-run', function(){
                if (page > 1) 
                    page = page - 1;
                else 
                    page = 1;

                getDataPegawai();
            });

            $(document).on('click', '.next-run', function(){
                let totalPage = $(this).attr('data-total-page');
                if (page < totalPage) 
                    page = page + 1;
                    
                getDataPegawai();
            });

            $(document).on('click', '.page-run', function(){
                page = parseInt($(this).attr('data-page'));
                getDataPegawai();
            });

            function getDataPegawai() {
                let searchBy = $('input[name="search_by"]:checked').val();
                let search = $('[name="search"]').val();
                $.ajax({
                    method: "post",
                    url: "{{ route('permohonan.info-pegawai') }}",
                    data: { 
                        _token: "{{csrf_token()}}",
                        search: $('[name="search"]').val(), 
                        search_by: $('input[name="search_by"]:checked').val(),
                        page: page,
                        limit: limit
                    }, beforeSend: function() {
                        blockUIInfo.block();
                        $('#alert_searching').hide();
                        $('.cari-nip').html('<i class="fa fa-spinner" aria-hidden="true"></i> Cari');
                        $('.cari-nip').attr('disabled', true);
                    }
                })
                .done(function(response) {
                    if (!response.error) {
                        $('#result_data').html(response.data);
                    } else {
                        $('#result_data').html("");
                        $('#alert_searching').show();
                        $('.cari-nip').html('<i class="bi bi-search fs-2x"></i> Cari');
                        $('.cari-nip').attr('disabled', false);
                        $('#message-alert').html(response.message);
                    }
                })
                .fail(function() {
                    $('.cari-nip').html('<i class="bi bi-search fs-2x"></i> Cari');
                    $('.cari-nip').attr('disabled', false);
                    blockUIInfo.release();
                })
                .always(function() {
                    $('.cari-nip').html('<i class="bi bi-search fs-2x"></i> Cari');
                    $('.cari-nip').attr('disabled', false);
                    blockUIInfo.release();
                });
            }
        </script>
    @endsection
</x-base-layout>

