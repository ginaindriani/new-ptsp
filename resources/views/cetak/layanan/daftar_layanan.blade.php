@section('head_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link href="https://bukutamu.kejaksaan.go.id/assets/frontend/css/webcam-style.css" rel="stylesheet" />
    <script>
        function tokenValidation(token) {
            document.getElementsByName('g_token')[0].value = token;
        }
    </script>
@endsection
@section('styles')
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
        padding-top:0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
    #my_camera{
     width: 320px;
     height: 240px;
     border: 1px solid black;
    }

    .d-none {
    display: none !important;
    }
</style>
@endif
<style>
    .buttons-reset {
        padding: 5px 10px 5px 10px !important;
    }

    @media only screen and (max-width: 600px) {
        .menu-action {
            right: 0px;
        }
    }
    iframe body{
        overflow: hidden !important;
    }
    .card-frame{
        height: 100%;

    }
    #my_camera{
     width: 320px;
     height: 240px;
     border: 1px solid black;
    }

    .d-none {
    display: none !important;
    }
</style>
@endsection

@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">BERITA</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        <li class="breadcrumb-item text-muted">
            <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
        </li>
        {{-- <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-muted">
            Master
        </li> --}}
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-200 w-5px h-2px"></span>
        </li>
        <li class="breadcrumb-item text-dark">
            Pendaftaran Layanan
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    <!--begin::Card-->
    @include('components.notification')
    <div class="card card-frame" id="container-card">
        <!--begin::Card body-->
        <div class="card-header">
          <div class="card-title">
              <h3 class="fw-bolder">Pendaftaran Layanan</h3>
          </div>
        </div>
        <div class="card-body pt-6">
            <center><h4 class="title">Anda diwajibkan untuk mendaftarkan diri anda sebelum melakukan pelayanan/pengaduan</h4></center>
            <hr/>
            <br/>
            <form action="{{ route('permohonan.simpantamu') }}" method="post" id="formsimpan">
            @csrf
                <input type="hidden" class="g_token"  name="g_token"  id="g_token" />
                <div class="form-group mb-7">
                    <label for="satker" class="control-label required">PILIH SATUAN KERJA KEJAKSAAN YANG AKAN MENANGANI LAYANAN/PENGADUAN ANDA</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" id="satker" name="satker" required>
                        <option value="">==PILIH SATKER==</option>
                        @foreach ($satker as $s)
                            <option value="{{ $s->id_satker }}">{{ $s->nama_satker }}</option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('satker') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="type" class="control-label required">TIPE PENDAFTARAN TAMU</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" id="type" name="type" required>
                        <option value="">==PILIH TIPE PENDAFTARAN TAMU==</option>
                        <option value="1">TAMU BIASA</option>
                        <option value="2">SAKSI</option>
                        <option value="3">TERDAKWA</option>
                    </select>
                    <p class="text-danger">{{ $errors->first('type') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="no_identitas" class="control-label required">NOMOR IDENTITAS (KTP)</label>
                    <input class="form-control" type="number" pattern="[^()/><\][\\\x22,;|]+" required="required" name="no_identitas" type="no_identitas" placeholder="MASUKKAN NOMOR IDENTITAS (KTP)"
                        id="no_identitas">
                    <p class="text-danger">{{ $errors->first('no_identitas') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nama" class="control-label required">NAMA LENGKAP</label>
                    <input class="form-control" required="required" pattern="[^/><\][\\\x22,;|]+" name="nama" type="nama" placeholder="MASUKKAN NAMA LENGKAP"
                        id="nama">
                    <p class="text-danger">{{ $errors->first('nama') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="jenis_kelamin" class="control-label required">JENIS KELAMIN</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" id="jenis_kelamin" name="jenis_kelamin" required>
                        <option value="">==PILIH JENIS KELAMIN==</option>
                        <option value="1">LAKI-LAKI</option>
                        <option value="2">PEREMPUAN</option>
                    </select>
                    <p class="text-danger">{{ $errors->first('jenis_kelamin') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="email" class="control-label required">EMAIL</label>
                    <input class="form-control" type="email" pattern="[^()/><\][\\\x22,;|]+" required="required" name="email" type="email" placeholder="MASUKKAN EMAIL"
                        id="email">
                    <p class="text-danger">{{ $errors->first('email') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="no_hp" class="control-label required">NOMOR HANDPHONE (UTAMAKAN YANG TERDAFTAR OLEH WHATSAPP, KARENA QR-CODE AKAN DIKIRIM MELALUI WHATSAPP)</label>
                    <input class="form-control" type="number" pattern="[^()/><\][\\\x22,;|]+" required="required" name="no_hp" type="no_hp" placeholder="MASUKKAN NOMOR HANDPHONE"
                        id="no_hp">
                    <p class="text-danger">{{ $errors->first('no_hp') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="alamat" class="control-label required">ALAMAT</label>
                    <input class="form-control" required="required" pattern="[^/><\][\\\x22,;|]+" name="alamat" type="alamat" placeholder="MASUKKAN ALAMAT"
                        id="alamat">
                    <p class="text-danger">{{ $errors->first('alamat') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="tujuan" class="control-label required">TUJUAN MENDAFTAR</label>
                    <input class="form-control" required="required" name="tujuan" pattern="[^/><\][\\\x22,;|]+" type="tujuan" placeholder="MASUKKAN ALASAN MENDAFTAR PADA SISTEM PELAYANAN KEJAKSAAN INI"
                        id="tujuan">
                    <p class="text-danger">{{ $errors->first('tujuan') }}</p>
                </div>
                <legend class="scheduler-border">Foto</legend>
                <div class="form-group mb-7">
                    <span>AMBIL FOTO DIRI ANDA</span>
                    <label style="color: red;">*</label>
                    <center><a id="cameraFlip" class="btn btn-success">GANTI KAMERA</a></center>
                    <div id="bx-cam">
                        <video id="webcam" autoplay playsinline></video>
                        <canvas id="canvas" class="d-none"></canvas>
                        <div id="flash-cam" class="flash-cam"></div>
                        <input type="hidden" name="photo" id="download-photo" />
                    </div>
                    <div id="bx-cam-icon">
                        <div class="row-icon">
                            <a id="take-photo" class="cam-icon">
                            <i class="fa fa-camera"></i>
                            </a>
                            <a id="retake-photo" class="d-none cam-icon">
                            <i class="fa fa-repeat"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <center>
                <div class="form-group mb-7">
                    <input type="checkbox" id="checkme"/> DENGAN INI, SAYA SETUJU UNTUK MEMBERIKAN DATA PRIBADI SAYA
                    <div class="registration-form-action clearfix animated fadeInLeftBig" data-animation="fadeInLeftBig" data-animation-delay=".15s" style="animation-delay: 0.15s;">
                        <input type="file" id="file_ktp" style="display: none;" />
                        <button type="submit" onclick="simpandata()" id="tombol" name="tombol" class="btn btn-primary btn-block">SIMPAN DATA SAYA DAN LANJUTKAN</button>
                    </div>
                </div>
                </center>
            </form>
            <center>
                <h4 class="title">DATA PRIBADI ANDA YANG DISIMPAN PADA APLIKASI PELAYANAN TERPADU KEJAKSAAN REPUBLIK INDONESIA BERSIFAT RAHASIA DAN AMAN</h4>
            </center>
            <hr/>
            <br/>
            <br/>
            <center>
                <p class="sub-title">
                    Jika anda sudah pernah mendaftar dan masih memiliki QR-Code Silahkan <a href="{{url('scanqr')}}"><link>Klik Disini</link><a> untuk langsung mendapatkan pelayanan.<br/> Namun jika QR-Code hilang lakukan pendaftaran kembali pada form diatas
                </p>
            </center>
        </div>
    </div>
    <!--end::Card-->
@section('scripts')
<script src="https://bukutamu.kejaksaan.go.id/assets/frontend/js/webcam-easy-1.0.5.min.js"></script>
<script type="text/javascript">
    var checker = document.getElementById('checkme');
    var sendbtn = document.getElementById('tombol');
    sendbtn.disabled = true;
    // when unchecked or checked, run the function
    checker.onchange = function(){
    if(this.checked){
        sendbtn.disabled = false;
    } else {
        sendbtn.disabled = true;
    }}
    function simpandata() {
        sendbtn.disabled = true;
        $('#tombol').html('Data sedang disimpan Mohon Tunggu');
        document.getElementById("formsimpan").submit();
    }
    $(document).ready(function() {
        const webcamElement = document.getElementById('webcam');
        const canvasElement = document.getElementById('canvas');
        const snapSoundElement = document.getElementById('snapSound');
        const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
        $('#cameraFlip').click(function() {
        webcam.flip();
        webcam.start();  
        });
        webcam.start()
            .then(result =>{
                console.log("webcam started");
            })
            .catch(err => {
                console.log(err);
            }
        );

        function refreshCamera(){
        webcam.start()
            .then(result =>{
                console.log("webcam started");
            })
            .catch(err => {
                console.log(err);
            }
        )
        }

        $("#take-photo").click(function () {
            beforeTakePhoto();
            let picture = webcam.snap();
            $("#download-photo").val(picture);
            Swal.fire({
            type: 'success',
            title: 'Berhasil mengambil foto!',
            html: '<em>'+'Lanjutkan dengan klik Simpan'+'</em>',
            showCancelButton: false,
            showConfirmButton: true
            });
            // document.querySelector('#download-photo').href = picture;
            afterTakePhoto();
        });

        $("#retake-photo").click(function () {
            webcam.stream()
                .then(facingMode =>{
                    removeCapture();
                }
            );
        });


        function beforeTakePhoto(){
            $('#flash-cam')
                .show()
                .animate({opacity: 0.3}, 500)
                .fadeOut(500)
                .css({'opacity': 0.7});
            // window.scrollTo(0, 0);
            $('#webcam-control').addClass('d-none');
            $('#cameraControls').addClass('d-none');
        };

        function afterTakePhoto(){
            webcam.stop();
            $('#canvas').removeClass('d-none');
            $('#take-photo').addClass('d-none');
            $('#retake-photo').removeClass('d-none');
            // $('#download-photo').removeClass('d-none');
            // $('#resume-camera').removeClass('d-none');
            // $('#cameraControls').removeClass('d-none');
        }

        function removeCapture(){
            $('#canvas').addClass('d-none');
            $('#take-photo').removeClass('d-none');
            $('#retake-photo').addClass('d-none');
        }


        

    });
</script>
@endsection
</x-base-layout>
