<x-base-layout> 
    <div class="card card-xxl-stretch mb-5">
        <div class="card-body">
            <div class="row">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="nik" placeholder="Cari NIK" aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="input-group-text btn btn-primary cari-nik" id="basic-addon2">
                            <i class="bi bi-search fs-2x"></i> Cari
                        </button>
                        {{-- <span class="input-group-text" id="basic-addon2"></span> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-xxl-stretch">
        <div class="card-header">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark" id="title-menu">SEMUA</span>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">   
                @foreach ($masterLayanan as $layanan)  
                    <div class="col-lg-3 col-md-4 col-sm-6 mb-5 list-image" data-tipe-layanan="{{$layanan->tipe_layanan}}">
                        <div class="card overlay overflow-hidden">
                        <div class="card-body p-0">
                                <div class="overlay-wrapper">
                                    <img src="{{asset('landingpage/images/listmenu/') . '/' . $layanan->image}}" alt="" class="w-100 rounded" style="width: 365.25px; height: 260.97px;"/>
                                </div>
                                <div class="overlay-layer bg-dark bg-opacity-25 align-items-end justify-content-center">
                                    <div class="d-flex flex-grow-1 flex-center  py-5">
                                        @if ($layanan->type == 'hyperlink' && $layanan->link_url)
                                            <a target="{{$layanan->is_blank ? '_blank' : ''}}" href="{{$layanan->link_url}}" class="btn btn-light-primary btn-sm btn-shadow ms-2">Pilih Layanan</a>
                                        @else
                                            <a href="{{$layanan->url_target}}" class="btn btn-light-primary btn-sm btn-shadow ms-2">Pilih Layanan</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer"></div>
    </div>
    <script>
        document.getElementById("filter_menu_layanan").addEventListener("click", function(){
            var menu = document.querySelector('input[name="menu"]:checked').value;
            var arrows = document.getElementsByClassName("list-image");
            document.getElementById("title-menu").innerHTML = menu.toUpperCase();

            for(var i = 0; i < arrows.length; i++){
                if (menu != 'all') {
                    if (arrows[i].getAttribute("data-tipe-layanan") == menu) {
                        arrows[i].style.display = "block";
                    } else {
                        arrows[i].style.display = "none";
                    }
                } else {
                    arrows[i].style.display = "block";
                }
            }
        });
    </script>
    @section('scripts')
        <script>
            $(document).on('click', '.action-custom', function(){
                $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
                $('.action-custom').removeClass('show');
                
                $(this).addClass('show');
                $(this).parent().find('.menu-sub-dropdown').addClass('show');
            });
            $(document).on('click', function (e) {
                if ($(e.target).closest(".action-custom").length === 0) {
                    $('.action-custom').removeClass('show');
                    $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
                }
            });

            $(document).on('click', '.cari-nik', function(){
                $.post( "{{ route('permohonan.dataModal') }}", { 
                    _token: "{{csrf_token()}}",
                    nik: $('[name="nik"]').val(), 
                })
                .done(function(response) {
                    $('#modal-container-custom').html(response);
                    $('#kt_modal_1').modal('show');
                })
                .fail(function() {
                    
                })
                .always(function() {
                });
            });
        </script>
    @endsection
</x-base-layout>
