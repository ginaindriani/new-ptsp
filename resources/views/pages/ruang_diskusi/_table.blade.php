<!--begin::Table-->
{{ $dataTable->table(['id' => 'ruang-diskusi-table'], true) }}
<!--end::Table-->

{{-- Inject Scripts --}}
@section('scripts')
{{ $dataTable->scripts() }}
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
<script>
    function deleteData(event) {
        event.preventDefault();
        const id = event.currentTarget.getAttribute('data-id');
        let url = "{{ route('ruang-diskusi.destroy', ':id') }}";
        url = url.replace(':id', id);
        Swal.fire({
                title: 'Anda yakin?',
                text: 'Akan menghapus data ini',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {
                if (result.value) {
                    let submit = $("#deleteForm").attr('action', url);
                    submit.submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
    }
    
    function getAnswer(event) {
        event.preventDefault();
        const url = event.currentTarget.getAttribute('href');
        
        $.ajax({
            type: "GET",
            url: url,
            data: {},
            success: function (data) {
                $('#modal-container-custom').html(data);
                $('#kt_modal_1').modal('show');
            }
        });
    }

    $(document).on('click', '#save-data', function(){
        const url = $(this).attr('href');
        $.ajax({
            type: "POST",
            url: url,
            data: {
                answer: $('[name="answer"]').val(),
                _token: "{{ csrf_token() }}"
            },
            success: function (data) {
                if (!data.error) {
                    Swal.fire(
                        'Berhasil!',
                        data.message,
                        'success'
                    )
                } else {
                    Swal.fire(
                        '',
                        data.message,
                        'info'
                    )
                }
                $('#kt_modal_1').modal('hide');
                window.location.reload();
            }
        });
    });
</script>
@endsection
