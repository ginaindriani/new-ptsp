<x-base-layout>
    @include('components.notification')
    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">Ruang Diskusi</h3>
            </div>
        </div>
        <!--begin::Card body-->
        <div class="card-body pt-6">
            <div class="table-responsive">
                @include('pages.ruang_diskusi._table')
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

</x-base-layout>
