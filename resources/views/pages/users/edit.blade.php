<x-base-layout>
    @section('styles')
        <style>
            .select2-container--bootstrap5 .select2-selection {
                height: 42.5px;
            }
            .select2-container--bootstrap5 .select2-selection--single.form-select-solid .select2-selection__rendered {
                color: #181C32;
            }
        </style>
    @endsection
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Manajemen User
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Pengaturan
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('user.index')}}" class="text-muted text-hover-primary">
                    Data User
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Edit Data User
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection
    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('user.update', $user->id) }}" method="post">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Edit Data User</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                @method('PUT')
                <div class="form-group mb-7">
                    <label for="nip" class="control-label required">NIP</label>
                    <div class="input-group">
                        <input class="form-control" required="required" name="nip" type="text"
                            id="nip" value="{{ $user->nip }}">
                        <span class="input-group-append">
                            <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" id="cek_nip" title="Cari NIP Pada Sistem Kepegawaian Kejaksaan RI" type="button"><i class="fa fa-search mr-1"></i>Cari NIP</button>
                        </span>
                    </div>
                    <p class="text-danger">{{ $errors->first('nip') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nama" class="control-label required">Nama</label>
                    <input class="form-control" required="required" name="nama" type="text"
                        id="nama" value="{{ $user->nama }}">
                    <p class="text-danger">{{ $errors->first('nama') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nik" class="control-label required">NIK</label>
                    <input class="form-control" required="required" name="nik" type="text"
                        id="nik" value="{{ $user->nik }}">
                    <p class="text-danger">{{ $errors->first('nik') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="pangkat" class="control-label required">Pangkat</label>
                    <input class="form-control" required="required" name="pangkat" type="text"
                        id="pangkat" value="{{ $user->pangkat }}">
                    <p class="text-danger">{{ $errors->first('pangkat') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="username" class="control-label required">Username</label>
                    <input class="form-control" required="required" name="username" type="text"
                        id="username" value="{{ $user->username }}">
                    <p class="text-danger">{{ $errors->first('name') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="email" class="control-label required">Email</label>
                    <input class="form-control" required="required" name="email" type="email"
                        id="email" value="{{ $user->email }}">
                    <p class="text-danger">{{ $errors->first('email') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="password" class="control-label required">Password</label>
                    <input class="form-control" name="password" type="password"
                        id="password">
                    <p class="text-primary">Kosongkan jika tidak ingin mengganti</p>
                    <p class="text-danger">{{ $errors->first('password') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="role" class="control-label required">Role User</label>
                    <select class="form-control form-select-solid select2-container" name="role" id="role" required="required">
                        <option value="">None</option>
                        @foreach ($role as $row)
                            <option value="{{ $row->name }}" {{ $row->name == $user->role ? 'selected' : '' }}> {{ $row->name }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('role') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="id_satker" class="control-label required">Pilih Satker</label>
                    <select class="form-control form-select-solid select2-container" name="id_satker" id="id_satker" required="required">
                        <option value="">--Pilih Satker--</option>
                        @foreach ($satker as $row)
                        <option value="{{ $row['id'] }}" {{ $row['id'] == $user->organisasi->satker->id_satker ? 'selected' : '' }}> {{ $row['text'] }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('id_satker') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nomor" class="control-label required">Pilih Bidang</label>
                    <select class="form-control form-select-solid select2-container" name="id_bidang" id="id_bidang" required="required">
                        <option value="{{ $user->organisasi->bidang->id_bidang }}" selected> {{ $user->organisasi->bidang->nama_bidang }} </option>
                    </select>
                    <p class="text-danger">{{ $errors->first('id_bidang') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="id_jabatan" class="control-label required">Pilih Jabatan</label>
                    <select class="form-control form-select-solid select2-container" name="id_jabatan" id="id_jabatan" required="required">
                    <option value="{{ $user->organisasi->jabatan->id_jabatan }}" selected> {{ $user->organisasi->jabatan->nama_jabatan }} </option>
                    </select>
                    <p class="text-danger">{{ $errors->first('id_jabatan') }}</p>
                </div>
                <input type="hidden" id="photo" name="photo"/>
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->

    @section('scripts')
        <script>
            const _token = $('meta[name="csrf-token"]').attr('content');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': _token
                }
            });
            
            $( document ).ready(function() {
                $(".select2-container").select2();
                $('#id_satker').on('change', function() {
                    $.ajax({
                        url: "{{ route('user.getbidang') }}",
                        type: "GET",
                        data: { id_satker: $(this).val() },
                        success: function(data){
                            $('#id_bidang').empty()
                            $('#id_bidang').append('<option value="">--Pilih Bidang--</option>')
                            $.each(data, function(key, item) {
                                $('#id_bidang').append('<option value="'+item.id+'">'+item.text+'</option>')
                            })
                        }
                    });
                })

                $('#id_bidang').on('change', function() {
                    $.ajax({
                        url: "{{ route('user.getjabatan') }}",
                        type: "GET",
                        data: { id_bidang: $(this).val() },
                        success: function(data){
                            $('#id_jabatan').empty()
                            $('#id_jabatan').append('<option value="">--Pilih Jabatan--</option>')
                            $.each(data, function(key, item) {
                                $('#id_jabatan').append('<option value="'+item.id+'">'+item.text+'</option>')
                            })
                        }
                    });
                })
            });
            
            //Script NIP
            $("input[name='nip']").on('input', function (e) {
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
            });

            $("#nip").keypress(function (e) {
                var length = this.value.length;
                if (length >= 18) {
                    e.preventDefault();
                    alert("Input NIP Hanya 18 character");
                }
            });

        $('#cek_nip').on('click', function (e) {
            e.preventDefault();
            var nip = $("input[name='nip']").val();
            $.ajax({
                type: "POST",
                url: "{{ route('user.ceknip') }}",
                data: {
                    nip: nip
                },
                success: function (data) {
                    if (data['status'] == 200) {
                        $("input[name='nama']").empty();
                        $("input[name='nik']").empty();
                        $("input[name='pangkat']").empty();
                        $("input[name='photo']").empty();

                        Swal.fire({
                            title: "Success!",
                            text: "NIP Ditemukan",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: false
                        });
                        $("input[name='nama']").val(data['nama']);
                        $("input[name='pangkat']").val(data['golpang']);
                        $("input[name='nik']").val(data['nik']);
                        $("input[name='photo']").val(data['foto']);
                        $("input[name='username']").setfocus();

                    } else {
                        swal({
                            title: "Invalid!",
                            text: "NIP Tidak Ditemukan",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonClass: "btn-warning",
                            confirmButtonText: 'OK',
                            closeOnConfirm: false
                        });
                        $("input[name='nama']").val(null);
                        $("input[name='pangkat']").val(null);
                        $("input[name='nik']").val(null);
                        $("input[name='photo']").val(null);
                    }
                }
            });
        });
        </script>
    @endsection
</x-base-layout>
