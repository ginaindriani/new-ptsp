<x-base-layout> 
    @section('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/4.1.4/css/sb-admin-2.min.css" integrity="sha512-Mk4n0eeNdGiUHlWvZRybiowkcu+Fo2t4XwsJyyDghASMeFGH6yUXcdDI3CKq12an5J8fq4EFzRVRdbjerO3RmQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    @endsection
    @section('breadcrumb')
        <div class="d-flex align-items-center me-3">
            <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3"></h1>
            <span class="h-20px border-gray-200 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">
                    <a href="{{route('backoffice.dashboard')}}" class="text-muted text-hover-primary">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">
                    <a href="#" class="text-dark text-hover-primary">
                        Dashboard
                    </a>
                </li>
            </ul>
        </div>
    @endsection
    
    <div class="card card-xxl-stretch">
        <div class="card-header align-items-center">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark" id="title-menu">Dashboard</span>
            </h3>
            @if(auth()->user()->kode_satker == null)
                <div class="col-5">
                    <div class="d-flex align-items-center justify-content-end">
                        <label style="width: 150px" class="mr-4 mb-0">Pilih Satuan Kerja : </label>
                        <div class="form-group m-0" style="width: 70%">
                            <select class="form-control select2-container" id="pilihsatker" name="pilihsatker">
                                <option value="0">-- Semua --</option>
                                @foreach($datasatker as $ds)
                                    <option value="{{$ds->id_satker}}">{{$ds->nama_satker}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="card-body" id="content-dashboard">
            <div class="row">
                {{-- @if(auth()->user()->kode_satker == null)
                    <div class="col-12">
                        <div class="form-group">
                            <label>Satuan Kerja</label>
                            <select class="form-control select2-container" id="pilihsatker" name="pilihsatker">
                                <option value="">-- Semua --</option>
                                @foreach($datasatker as $ds)
                                    <option value="{{$ds->id_satker}}">{{$ds->nama_satker}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="">
                            <button type="button" id="btn"  type="button" class="btn btn-success btn-block">Pilih</button>
                        </div>
                    </div>  
                    <div class="col-12">
                        <br> 
                        <br>
                    </div>  
                @endif --}}
                <div class="col-xl-6 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Total Permohonan Layanan</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pelayanan}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                        Total Pengaduan</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pengaduan}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Permohonan Layanan</h6>
                        </div>
                        <div class="card-body">
                            @foreach($data_layanan as $dp)
                                <h5 class="font-weight-bold">{{substr($dp->nama_layanan,7,50)}}</h5>
                                <span>Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                                <div class="progress mb-4">
                                    <div class="progress-bar 
                                    @if($dp->percentage == '100%')
                                        bg-success
                                    @elseif($dp->percentage <= 21)
                                        bg-danger
                                    @elseif($dp->percentage <= 41)
                                        bg-warning
                                    @elseif($dp->percentage <= 61)

                                    @elseif($dp->percentage <= 81)
                                        bg-info
                                    @elseif($dp->percentage <= 101)
                                        bg-success
                                    @else

                                    @endif
                                    " 
                                    role="progressbar" style="width: {{$dp->percentage}}"
                                    aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @if(!$data_pengaduan->isEmpty())
                <div class="col-lg-6 mb-4">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Total Pengaduan</h6>
                        </div>
                        <div class="card-body">
                            @foreach($data_pengaduan as $dp)
                                <h5 class="font-weight-bold">{{substr($dp->nama_layanan,9,50)}}</h5>
                                <span>Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                                <div class="progress mb-4">
                                    <div class="progress-bar 
                                    @if($dp->percentage == '100%')
                                        bg-success
                                    @elseif($dp->percentage <= 21)
                                        bg-danger
                                    @elseif($dp->percentage <= 41)
                                        bg-warning
                                    @elseif($dp->percentage <= 61)

                                    @elseif($dp->percentage <= 81)
                                        bg-info
                                    @elseif($dp->percentage <= 101)
                                        bg-success
                                    @else

                                    @endif
                                    " 
                                    role="progressbar" style="width: {{$dp->percentage}}"
                                    aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-xl-6 col-lg-6">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div
                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Jumlah Layanan Perbulan</h6>
                            <div class="dropdown no-arrow">
                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                </a>
                            </div>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="chart-area">
                                <canvas id="layananPerbulanChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div
                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Jumlah Pengaduan Perbulan</h6>
                            <div class="dropdown no-arrow">
                                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                </a>
                            </div>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="chart-area">
                                <canvas id="layananPengaduanChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                @if(auth()->user()->kode_satker == null)
                    <div class="col-lg-6 mb-4">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">TOP 10 Satker Permohonan Pelayanan Terbanyak</h6>
                            </div>
                            <div class="card-body">
                                @foreach($top_10_satker_data_layanan as $dp)
                                    <h5 class="font-weight-bold">{{$dp->nama_satker}}</h5>
                                    <span>Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                                    <div class="progress mb-4">
                                        <div class="progress-bar
                                        @if($dp->percentage == '100%')
                                            bg-success
                                        @elseif($dp->percentage <= 21)
                                            bg-danger
                                        @elseif($dp->percentage <= 41)
                                            bg-warning
                                        @elseif($dp->percentage <= 61)

                                        @elseif($dp->percentage <= 81)
                                            bg-info
                                        @elseif($dp->percentage <= 101)
                                            bg-success
                                        @else

                                        @endif
                                        " 
                                        role="progressbar" style="width: {{$dp->percentage}}"
                                        aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">TOP 10 Satker Pengaduan Masyarakat Terbanyak</h6>
                            </div>
                            <div class="card-body">
                                @foreach($top_10_satker_data_pengaduan as $dp)
                                    <h5 class="font-weight-bold">{{$dp->nama_satker}} </h5>
                                    <span>Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                                    <div class="progress mb-4">
                                        <div class="progress-bar
                                        @if($dp->percentage == '100%')
                                            bg-success
                                        @elseif($dp->percentage <= 21)
                                            bg-danger
                                        @elseif($dp->percentage <= 41)
                                            bg-warning
                                        @elseif($dp->percentage <= 61)

                                        @elseif($dp->percentage <= 81)
                                            bg-info
                                        @elseif($dp->percentage <= 101)
                                            bg-success
                                        @else

                                        @endif
                                        " 
                                        role="progressbar" style="width: {{$dp->percentage}}"
                                        aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">TOP 10 Satker Penyelesaian Pelayanan Terbanyak</h6>
                            </div>
                            <div class="card-body">
                                @foreach($top_10_satker_persentase_layanan as $dp)
                                    <h5 class="font-weight-bold">{{$dp->nama_satker}}</h5>
                                    <span>Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                                    <div class="progress mb-4">
                                        <div class="progress-bar
                                        @if($dp->percentage == '100%')
                                            bg-success
                                        @elseif($dp->percentage <= 21)
                                            bg-danger
                                        @elseif($dp->percentage <= 41)
                                            bg-warning
                                        @elseif($dp->percentage <= 61)

                                        @elseif($dp->percentage <= 81)
                                            bg-info
                                        @elseif($dp->percentage <= 101)
                                            bg-success
                                        @else

                                        @endif
                                        " 
                                        role="progressbar" style="width: {{$dp->percentage}}"
                                        aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">TOP 10 Satker Penyelesaian Pengaduan Masyarakat Terbanyak</h6>
                            </div>
                            <div class="card-body">
                                @foreach($top_10_satker_persentase_pengaduan as $dp)
                                    <h5 class="font-weight-bold">{{$dp->nama_satker}}</h5>
                                    <span>Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                                    <div class="progress mb-4">
                                        <div class="progress-bar
                                        @if($dp->percentage == '100%')
                                            bg-success
                                        @elseif($dp->percentage <= 21)
                                            bg-danger
                                        @elseif($dp->percentage <= 41)
                                            bg-warning
                                        @elseif($dp->percentage <= 61)

                                        @elseif($dp->percentage <= 81)
                                            bg-info
                                        @elseif($dp->percentage <= 101)
                                            bg-success
                                        @else

                                        @endif
                                        " 
                                        role="progressbar" style="width: {{$dp->percentage}}"
                                        aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="card-footer"></div>
    </div>
    @section('scripts')
    <script>
        var base_url = "{{ url('') }}";
        $("#btn").click( function() {
            var url =  base_url + "/backoffice/dashboard/" + $("#pilihsatker").val();
            window.location.assign(url);
        });


        $(document).ready(function(){
            // 
            $('#pilihsatker').select2();

            $('#pilihsatker').change(function(){
                //value 
                console.log($(this).val());
                var url =  base_url + "/backoffice/filterdashboard/" + $("#pilihsatker").val();
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: url,
                    dataType: 'json',
                    success: function (data) {
                        // console.log('yo filter',data, $("#content-dashboard"));
                        // $("#content-dashboard").replaceWith("<p></p>");
                    },error:function(){
                        
                    },complete:function(data){
                        console.log('yo complete',data);
                        $("#content-dashboard").html(data.responseText);
                        initGenerate();
                    }
                });
            });

            function initGenerate(){
                var layanan = {!! json_encode($hitung_data_layanan_perbulan) !!};
                console.log(layanan,'data layanan');
                // 
                var arrLayanan = [];
                for(var i in layanan.original){
                    arrLayanan.push(layanan.original[i].count)
                }
                // 
                var ctx = document.getElementById("layananPerbulanChart");
                var myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    datasets: [{
                        label: "Jumlah",
                        lineTension: 0.3,
                        backgroundColor: "rgba(78, 115, 223, 0.05)",
                        borderColor: "rgba(78, 115, 223, 1)",
                        pointRadius: 3,
                        pointBackgroundColor: "rgba(78, 115, 223, 1)",
                        pointBorderColor: "rgba(78, 115, 223, 1)",
                        pointHoverRadius: 3,
                        pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                        pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                        pointHitRadius: 10,
                        pointBorderWidth: 2,
                        data: arrLayanan,
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    layout: {
                    padding: {
                        left: 10,
                        right: 25,
                        top: 25,
                        bottom: 0
                    }
                    },
                    scales: {
                    xAxes: [{
                        time: {
                        unit: 'date'
                        },
                        gridLines: {
                        display: false,
                        drawBorder: false
                        },
                        ticks: {
                        maxTicksLimit: 7
                        }
                    }],
                    yAxes: [{
                        ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return '$' + number_format(value);
                        }
                        },
                        gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                        }
                    }],
                    },
                    legend: {
                    display: false
                    },
                    tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    titleMarginBottom: 10,
                    titleFontColor: '#6e707e',
                    titleFontSize: 14,
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    intersect: false,
                    mode: 'index',
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
                        }
                    }
                    }
                }
                });

                var pengaduan = {!! json_encode($hitung_data_pengaduan_perbulan) !!};
                console.log(pengaduan,'data pengaduan');
                // 
                var arrPengaduan = [];
                for(var i in pengaduan.original){
                    arrPengaduan.push(pengaduan.original[i].count)
                }
                // 
                var laypengaduan = document.getElementById("layananPengaduanChart");
                var pengaduanChart = new Chart(laypengaduan, {
                type: 'line',
                data: {
                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    datasets: [{
                    label: "Jumlah",
                    lineTension: 0.3,
                    backgroundColor: "rgba(78, 115, 223, 0.05)",
                    borderColor: "rgba(78, 115, 223, 1)",
                    pointRadius: 3,
                    pointBackgroundColor: "rgba(78, 115, 223, 1)",
                    pointBorderColor: "rgba(78, 115, 223, 1)",
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                    pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                    pointHitRadius: 10,
                    pointBorderWidth: 2,
                    data: arrPengaduan,
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    layout: {
                    padding: {
                        left: 10,
                        right: 25,
                        top: 25,
                        bottom: 0
                    }
                    },
                    scales: {
                    xAxes: [{
                        time: {
                        unit: 'date'
                        },
                        gridLines: {
                        display: false,
                        drawBorder: false
                        },
                        ticks: {
                        maxTicksLimit: 7
                        }
                    }],
                    yAxes: [{
                        ticks: {
                        maxTicksLimit: 5,
                        padding: 10,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return '$' + number_format(value);
                        }
                        },
                        gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                        }
                    }],
                    },
                    legend: {
                    display: false
                    },
                    tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    titleMarginBottom: 10,
                    titleFontColor: '#6e707e',
                    titleFontSize: 14,
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    intersect: false,
                    mode: 'index',
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
                        }
                    }
                    }
                }
                });
            }
            initGenerate();
        });
        
       

        
    </script>
    @endsection
</x-base-layout>
