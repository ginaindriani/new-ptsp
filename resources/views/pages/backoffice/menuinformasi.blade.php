<x-base-layout> 
    @section('breadcrumb')
        <div class="d-flex align-items-center me-3">
            <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3"></h1>
            <span class="h-20px border-gray-200 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">
                    <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-200 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">
                    <a href="#" class="text-dark text-hover-primary">
                        Informasi
                    </a>
                </li>
            </ul>
        </div>
    @endsection
    
    <div class="card card-xxl-stretch">
        <div class="card-header">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark" id="title-menu">Menu Informasi</span>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">   
                @foreach ($masterLayanan as $layanan)  
                    <div class="col-lg-3 col-md-4 col-sm-6 mb-5 list-image" data-tipe-layanan="{{$layanan->tipe_layanan}}">
                        <div class="card overlay overflow-hidden">
                        <div class="card-body p-0">
                                <div class="overlay-wrapper">
                                    <img src="{{asset('landingpage/images/listmenu/') . '/' . $layanan->image}}" alt="" class="w-100 rounded" style="width: 365.25px; height: 260.97px;"/>
                                </div>
                                <div class="overlay-layer bg-dark bg-opacity-25 align-items-end justify-content-center">
                                    <div class="d-flex flex-grow-1 flex-center  py-5">
                                        @if ($layanan->type == 'hyperlink' && $layanan->link_url)
                                            <a target="{{$layanan->is_blank ? '_blank' : ''}}" href="{{$layanan->link_url}}" class="btn btn-light-primary btn-sm btn-shadow ms-2">Pilih Layanan</a>
                                        @else
                                            <a href="{{$layanan->url_target}}" class="btn btn-light-primary btn-sm btn-shadow ms-2">Pilih Layanan</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer"></div>
    </div>
    <script>
        document.getElementById("filter_menu_layanan").addEventListener("click", function(){
            var menu = document.querySelector('input[name="menu"]:checked').value;
            var arrows = document.getElementsByClassName("list-image");
            document.getElementById("title-menu").innerHTML = menu.toUpperCase();

            for(var i = 0; i < arrows.length; i++){
                if (menu != 'all') {
                    if (arrows[i].getAttribute("data-tipe-layanan") == menu) {
                        arrows[i].style.display = "block";
                    } else {
                        arrows[i].style.display = "none";
                    }
                } else {
                    arrows[i].style.display = "block";
                }
            }
        });
    </script>
    @section('scripts')
        <script>
            $(document).on('click', '.action-custom', function(){
                $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
                $('.action-custom').removeClass('show');
                
                $(this).addClass('show');
                $(this).parent().find('.menu-sub-dropdown').addClass('show');
            });
            $(document).on('click', function (e) {
                if ($(e.target).closest(".action-custom").length === 0) {
                    $('.action-custom').removeClass('show');
                    $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
                }
            });

            $(document).on('click', '.cari-nik', function(){
                $.post( "{{ route('permohonan.dataModal') }}", { 
                    _token: "{{csrf_token()}}",
                    nik: $('[name="nik"]').val(), 
                })
                .done(function(response) {
                    $('#modal-container-custom').html(response);
                    $('#kt_modal_1').modal('show');
                })
                .fail(function() {
                    
                })
                .always(function() {
                });
            });
        </script>
    @endsection
</x-base-layout>
