{{-- <div class="row">
    <div class="col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <h6 class="font-weight-bold text-primary text-uppercase mb-1">
                            Total Permintaan Layanan</h6>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pelayanan}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <h6 class="font-weight-bold text-danger text-uppercase mb-1">
                            Total Pengaduan</h6>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pengaduan}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 mb-4">
        @if(!$data_layanan->isEmpty())
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Permintaan Layanan</h6>
            </div>
            <div class="card-body">
                @foreach($data_layanan as $dp)
                <div class="py-1">
                    <h5 class="font-weight-bold">{{substr($dp->nama_layanan,7,50)}}</h5>
                    <span class="text-muted">Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase : <strong>{{$dp->percentage}}</strong></span>
                    <div class="progress mb-4">
                        <div class="progress-bar 
                        @if($dp->percentage == '100%')
                            bg-success
                        @elseif($dp->percentage <= 21)
                            bg-danger
                        @elseif($dp->percentage <= 41)
                            bg-warning
                        @elseif($dp->percentage <= 61)

                        @elseif($dp->percentage <= 81)
                            bg-info
                        @elseif($dp->percentage <= 101)
                            bg-success
                        @else

                        @endif
                        " 
                        role="progressbar" style="width: {{$dp->percentage}}"
                        aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <div class="col-lg-6 mb-4">
        @if(!$data_pengaduan->isEmpty())
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Total Pengaduan</h6>
                </div>
                <div class="card-body">
                    @foreach($data_pengaduan as $dp)
                    <div class="py-1">
                        <h5 class="font-weight-bold">{{substr($dp->nama_layanan,9,50)}}</h5>
                        <span class="text-muted">Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase : <strong>{{$dp->percentage}}</strong></span>    
                        </h4>
                        <div class="progress mb-4">
                            <div class="progress-bar 
                            @if($dp->percentage == '100%')
                                bg-success
                            @elseif($dp->percentage <= 21)
                                bg-danger
                            @elseif($dp->percentage <= 41)
                                bg-warning
                            @elseif($dp->percentage <= 61)

                            @elseif($dp->percentage <= 81)
                                bg-info
                            @elseif($dp->percentage <= 101)
                                bg-success
                            @else

                            @endif
                            " 
                            role="progressbar" style="width: {{$dp->percentage}}"
                            aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <div class="col-xl-6 col-lg-6">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Jumlah Layanan Perbulan</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="layananPerbulanChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-lg-6">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Jumlah Pengaduan Perbulan</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="layananPengaduanChart"></canvas>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="row">
    {{-- @if(auth()->user()->kode_satker == null)
        <div class="col-12">
            <div class="form-group">
                <label>Satuan Kerja</label>
                <select class="form-control select2-container" id="pilihsatker" name="pilihsatker">
                    <option value="">-- Semua --</option>
                    @foreach($datasatker as $ds)
                        <option value="{{$ds->id_satker}}">{{$ds->nama_satker}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-12">
            <div class="">
                <button type="button" id="btn"  type="button" class="btn btn-success btn-block">Pilih</button>
            </div>
        </div>  
        <div class="col-12">
            <br> 
            <br>
        </div>  
    @endif --}}
    <div class="col-xl-3 col-md-3 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <h6 class="font-weight-bold text-primary text-uppercase mb-1">
                            Permintaan Layanan Hari Ini</h6>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pelayanan_hari_ini}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-3 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <h6 class="font-weight-bold text-primary text-uppercase mb-1">
                            Permintaan Layanan Belum Diverifikasi</h6>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pelayanan_belum}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-3 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <h6 class="font-weight-bold text-danger text-uppercase mb-1">
                            Pengaduan Hari Ini</h6>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pengaduan_hari_ini}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-3 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <h6 class="font-weight-bold text-danger text-uppercase mb-1">
                            Pengaduan Belum Diverifikasi</h6>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pengaduan_belum}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <h6 class="font-weight-bold text-primary text-uppercase mb-1">
                            Total Permintaan Layanan</h6>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pelayanan}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <h6 class="font-weight-bold text-danger text-uppercase mb-1">
                            Total Pengaduan</h6>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_pengaduan}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 mb-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Permintaan Layanan</h6>
            </div>
            <div class="card-body">
                @foreach($data_layanan as $dp)
                    <h5 class="font-weight-bold">{{substr($dp->nama_layanan,7,50)}}</h5>
                    <span class="text-muted">Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                    <div class="progress mb-4">
                        <div class="progress-bar 
                        @if($dp->percentage == '100%')
                            bg-success
                        @elseif($dp->percentage <= 21)
                            bg-danger
                        @elseif($dp->percentage <= 41)
                            bg-warning
                        @elseif($dp->percentage <= 61)

                        @elseif($dp->percentage <= 81)
                            bg-info
                        @elseif($dp->percentage <= 101)
                            bg-success
                        @else

                        @endif
                        " 
                        role="progressbar" style="width: {{$dp->percentage}}"
                        aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @if(!$data_pengaduan->isEmpty())
    <div class="col-lg-6 mb-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Total Pengaduan</h6>
            </div>
            <div class="card-body">
                @foreach($data_pengaduan as $dp)
                    <h5 class="font-weight-bold">{{substr($dp->nama_layanan,9,50)}}</h5>
                    <span class="text-muted">Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>    
                    <div class="progress mb-4">
                        <div class="progress-bar 
                        @if($dp->percentage == '100%')
                            bg-success
                        @elseif($dp->percentage <= 21)
                            bg-danger
                        @elseif($dp->percentage <= 41)
                            bg-warning
                        @elseif($dp->percentage <= 61)

                        @elseif($dp->percentage <= 81)
                            bg-info
                        @elseif($dp->percentage <= 101)
                            bg-success
                        @else

                        @endif
                        " 
                        role="progressbar" style="width: {{$dp->percentage}}"
                        aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
    <div class="col-xl-6 col-lg-6">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Jumlah Layanan Perbulan</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="layananPerbulanChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-lg-6">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Jumlah Pengaduan Perbulan</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="layananPengaduanChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    @if(auth()->user()->kode_satker == null)
        @if($tempid == 0)
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">TOP 10 Satker Permintaan Pelayanan Terbanyak</h6>
                </div>
                <div class="card-body">
                    @foreach($top_10_satker_data_layanan as $dp)
                        <h5 class="font-weight-bold">{{$dp->nama_satker}}</h5>
                        <span class="text-muted">Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                        <div class="progress mb-4">
                            <div class="progress-bar
                            @if($dp->percentage == '100%')
                                bg-success
                            @elseif($dp->percentage <= 21)
                                bg-danger
                            @elseif($dp->percentage <= 41)
                                bg-warning
                            @elseif($dp->percentage <= 61)

                            @elseif($dp->percentage <= 81)
                                bg-info
                            @elseif($dp->percentage <= 101)
                                bg-success
                            @else

                            @endif
                            " 
                            role="progressbar" style="width: {{$dp->percentage}}"
                            aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        @if($tempid == 0)
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">TOP 10 Satker Pengaduan Masyarakat Terbanyak</h6>
                </div>
                <div class="card-body">
                    @foreach($top_10_satker_data_pengaduan as $dp)
                        <h5 class="font-weight-bold">{{$dp->nama_satker}} </h5>
                        <span class="text-muted">Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                        <div class="progress mb-4">
                            <div class="progress-bar
                            @if($dp->percentage == '100%')
                                bg-success
                            @elseif($dp->percentage <= 21)
                                bg-danger
                            @elseif($dp->percentage <= 41)
                                bg-warning
                            @elseif($dp->percentage <= 61)

                            @elseif($dp->percentage <= 81)
                                bg-info
                            @elseif($dp->percentage <= 101)
                                bg-success
                            @else

                            @endif
                            " 
                            role="progressbar" style="width: {{$dp->percentage}}"
                            aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        @if($tempid == 0)
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">TOP 10 Satker Penyelesaian Pelayanan Terbanyak</h6>
                </div>
                <div class="card-body">
                    @foreach($top_10_satker_persentase_layanan as $dp)
                        <h5 class="font-weight-bold">{{$dp->nama_satker}}</h5>
                        <span class="text-muted">Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                        <div class="progress mb-4">
                            <div class="progress-bar
                            @if($dp->percentage == '100%')
                                bg-success
                            @elseif($dp->percentage <= 21)
                                bg-danger
                            @elseif($dp->percentage <= 41)
                                bg-warning
                            @elseif($dp->percentage <= 61)

                            @elseif($dp->percentage <= 81)
                                bg-info
                            @elseif($dp->percentage <= 101)
                                bg-success
                            @else

                            @endif
                            " 
                            role="progressbar" style="width: {{$dp->percentage}}"
                            aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        @if($tempid == 0)
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">TOP 10 Satker Penyelesaian Pengaduan Masyarakat Terbanyak</h6>
                </div>
                <div class="card-body">
                    @foreach($top_10_satker_persentase_pengaduan as $dp)
                        <h5 class="font-weight-bold">{{$dp->nama_satker}}</h5>
                        <span class="text-muted">Diproses : <strong>{{$dp->sudahdiproses}}</strong>, Data Masuk : <strong>{{$dp->jumlah}}</strong>, Persentase penyelesaian : <strong>{{$dp->percentage}}</strong></span>
                        <div class="progress mb-4">
                            <div class="progress-bar
                            @if($dp->percentage == '100%')
                                bg-success
                            @elseif($dp->percentage <= 21)
                                bg-danger
                            @elseif($dp->percentage <= 41)
                                bg-warning
                            @elseif($dp->percentage <= 61)

                            @elseif($dp->percentage <= 81)
                                bg-info
                            @elseif($dp->percentage <= 101)
                                bg-success
                            @else

                            @endif
                            " 
                            role="progressbar" style="width: {{$dp->percentage}}"
                            aria-valuenow="{{$dp->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
    @endif
</div>