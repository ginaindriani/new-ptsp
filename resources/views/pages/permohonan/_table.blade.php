<!--begin::Table-->
{{ $dataTable->table(['id' => 'permohonan-table'], true) }}
<!--end::Table-->

{{-- Inject Scripts --}}
@section('scripts')
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
{{ $dataTable->scripts() }}
<script>
    $(document).on('click', '.action-custom', function(){
        $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
        $('.action-custom').removeClass('show');
        
        $(this).addClass('show');
        $(this).parent().find('.menu-sub-dropdown').addClass('show');
    });
    $(document).on('click', function (e) {
        if ($(e.target).closest(".action-custom").length === 0) {
            $('.action-custom').removeClass('show');
            $('.action-custom').parent().find('.menu-sub-dropdown').removeClass('show');
        }
    });
    function deleteData(event)
    {
        event.preventDefault();
        const id = event.currentTarget.getAttribute('data-id');
        console.log(id);
        let url = "{{ route('permohonan.destroy', ':id') }}";
        url = url.replace(':id', id);
        Swal.fire({
            title: 'Anda yakin?',
            text: 'Akan menghapus permohonan ini',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
        .then((result) => {
            if (result.value) {
                let submit = $("#deleteForm").attr('action', url);
                submit.submit();
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        });
    }
</script>
@endsection
