@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
@endsection
@section('breadcrumb')
<div class="d-flex align-items-center me-3">
    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">{{$masterLayanan->nama_layanan}}</h1>
    <span class="h-20px border-gray-200 border-start mx-4"></span>
    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
        @if (auth()->user())
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <li class="breadcrumb-item text-muted">
                <a href="{{route('permohonan.index')}}?idLayanan={{$masterLayanan->id_layanan}}" class="text-muted text-hover-primary">
                    {{$masterLayanan->nama_layanan}}
                </a>
            </li>
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
        @endif
        <li class="breadcrumb-item text-dark">
            Tambah Data
        </li>
    </ul>
</div>
@endsection
<x-base-layout>
    @include('components.notification')
     <div class="card">
        <div class="card-body pt-6">
            <div class="card-header p-0">
                <div class="card-title">
                    <h3 class="fw-bolder">{{$masterLayanan->nama_layanan}}</h3>
                </div>
            </div>
            {!! form($form) !!}
        </div>
    </div>
    <input type="hidden" name="idLayanan" value="{{ request()->query('idLayanan') }}">

    @section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
        <script>
            // $('.select2').select2();
            // $('.select2').addClass('mb-5').addClass('mt-3');
            $('.date').datepicker({ dateFormat: "yy-mm-dd"}).datepicker("setDate", new Date());
            $('.dateedit').datepicker({ dateFormat: "yy-mm-dd"});

            let wysiwyg = {!! json_encode($wysiwyg) !!}
            wysiwyg.forEach(function(val, index, arr) {
            CKEDITOR.replace(val, {
                customConfig : 'config.js',
                toolbar : 'simple',
                tabSpaces : 12
            })
            })
        </script>
    @endsection
</x-base-layout>

