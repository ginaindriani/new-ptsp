<div class="modal fade" tabindex="-1" id="kt_modal_1" aria-modal="true" role="dialog">
    
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="svg-icon svg-icon-2x">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <h4>{{ $datas->data->satker->satker }}</h4>
                    <div>
                        <i>{{ $datas->data->satker->address }}</i>
                    </div>
                    <p style="font-weight: bold;">PERINGKAT PERKARA DI {{ $datas->data->satker->satker }}</p>
                </div>
                <div id="data-penduduk" style="margin: 0;">
                    <div>
                        <span class="citizen-data">DATA PENDUDUK
                            {{ ucwords($datas->data->totalPenduduk->provinsi) }} :
                            <b>{{ $datas->data->totalPenduduk->total }}</b> ORANG</span>
                    </div>
                    <div>
                        <span class="citizen-data">JAKSA :
                            <b>{{ $datas->data->pegawaiSatker }}</b> ORANG</span>
                    </div>
                    <div>
                        <span class="citizen-data">PERBANDINGAN
                            <b>1 :
                                {{ number_format(round($datas->data->totalPenduduk->total_int / $datas->data->pegawaiSatker), 0, ',', '.') }}
                                ORANG</b>
                        </span>
                    </div>
                    <div>
                        <span class="citizen-data">JUMLAH TAHANAN
                            <b> : {{ $jumlahTahanan }} ORANG</b>
                        </span>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Perkara</th>
                                        @foreach ($datas->data->years as $item)
                                            <th>{{ $item }}</th>
                                        @endforeach
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($datas->data->dataTable)
                                        {!! $datas->data->dataTable->table !!}
                                        <tr>
                                            <th colspan="2">Total</th>
                                            @foreach ($datas->data->dataTable->year_total as $item)
                                                <th>{{ $item }}</th>
                                            @endforeach
                                            <th>{{ $datas->data->dataTable->all_total }}</th>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="{{ count($datas->data->years) + 2 }}">
                                                <p class="text-center">
                                                    Data tidak ditemukan
                                                </p>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>