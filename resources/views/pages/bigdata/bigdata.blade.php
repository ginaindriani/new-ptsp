@section('styles')
<style>
    .title-bigdata {
        color: #535252;
        text-align: center;
        margin-top: 10px;
        margin-bottom: 30px;
    }

    .card-title {
        color:#777777;
    }

    .row {
        margin-top: 30px;
        margin-bottom: 30px;
    }

    .card-container {
        padding: 0 20px;
    }

    .clickable-container:hover {
        cursor: pointer;
    }

    .clickable-container:hover h3 {
        color: #09a1ff;
    }

    .text-right {
        text-align: right !important;
    }

    @media (min-width: 992px) {
        .modal-lg {
            max-width: 1250px;
        }
    }

    .custom-radio-inline {
        display: inline-block;
        margin-right: 20px;
    }
</style>
<link href="{{url('')}}/demo1/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection
<x-base-layout>
    <h1 class="title-bigdata">DASHBOARD BIG DATA KEJAKSAAN RI</h1>
    <div class="container" style="margin-top: 20px;">
        @if (Auth::user()->role == "superadmin")
            <div class="row">
                <div class="col-sm-4 card-container">
                    <label for="nomor" class="control-label required">Satker</label>
                    <select class="form-control form-select-solid" onchange="satker()" data-kt-select2="true" id="satker_select_option">
                                <option value="0" selected>KEJAKSAAN RI</option>
                        @foreach ($satker as $list)
                                 <option value="{{$list->id_satker}}">{{$list->nama_satker}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-4 card-container">
                <div class="card shadow-sm rounded clickable-container" id="card_pidum">
                    <div class="card-body">
                        <h3 class="card-title">PIDUM</h3>
                        <h1> <div class="total-pidum">{{$totalPidum}} </div>Perkara</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 card-container">
                <div class="card shadow-sm rounded clickable-container" id="card_pidsus">
                    <div class="card-body">
                        <h3 class="card-title">PIDSUS</h3>
                        <h1><div class="total-pidsus">{{$totalPidsus}} </div> Perkara</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 card-container">
                <a href="{{ route('permohonan.info-ormas') }}">
                    <div class="card shadow-sm rounded clickable-container">
                        <div class="card-body">
                            <h3 class="card-title">TOTAL ORMAS</h3>
                            <h1><div class="total-ormas">{{$totalOrmas}} </div></h1>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 card-container">
                <a href="{{ route('permohonan.data_layanan') }}">
                    <div class="card shadow-sm rounded clickable-container">
                        <div class="card-body">
                            <h3 class="card-title">JUMLAH TAMU</h3>
                            <h1><div class="total-tamu">{{$totalTamu}} </div></h1>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 card-container">
                <div class="card shadow-sm rounded clickable-container" id="card_pegawai">
                    <div class="card-body">
                        <h3 class="card-title">TOTAL PEGAWAI</h3>
                        <h1><div class="total-pegawai">{{$totalPegawai}} </div></h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 card-container">
                <a href="{{ route('permohonan.info-pakem') }}">
                    <div class="card shadow-sm rounded clickable-container">
                        <div class="card-body">
                            <h3 class="card-title">TOTAL PAKEM</h3>
                            <h1><div class="total-pakem">{{$totalPakem}} </div></h1>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    @include('pages.bigdata.modal_data_pegawai')
    @section('scripts')
        <script src="{{url('')}}/demo1/plugins/custom/datatables/datatables.bundle.js"></script>
        <script>
            var filtered = 0;
            var id_satker = 0;

            $(document).ready(function() {
                $.fn.dataTable.ext.search.push(
                function( settings, searchData, index, rowData, counter ) {
                    if (filtered == 0 || filtered == searchData[5]) {
                        return true;
                    }
                    return false;
                }
            );

            $(document).on('change', '.input-status_pegawai', function() {
                var val = $(this).val();
                var checked = $(this).prop('checked');
                filtered = val;

                //console.log(filtered);
                _detailTable.draw();
            })
        });
        </script>
        <script>

            function satker(){
                kode_satker = $("#satker_select_option").val();
                text = $("#satker_select_option option:selected").text();
                $('.title-bigdata').contents().filter(function(){
                    return this.nodeType === 3;
                }).remove();
                $('.title-bigdata').append("DASHBOARD BIG DATA "+text);
                id_satker = kode_satker;
                $.ajax({
                    url: "{{route('dashboard.big_data_calculate')}}",
                    data: {satker:kode_satker},
                    type: "GET",
                    dataType: "JSON",
                    success: function(data) {
                        $('.total-pidum, .total-pidsus,.total-ormas,.total-tamu,.total-pegawai,.total-pakem').contents().filter(function(){
                            return this.nodeType === 3;
                        }).remove();
                        $(".total-pidum").append(data.totalPidum)
                        $(".total-pidsus").append(data.totalPidsus)
                        $(".total-ormas").append(data.totalOrmas)
                        $(".total-tamu").append(data.totalTamu)
                        $(".total-pegawai").append(data.totalPegawai)
                        $(".total-pakem").append(data.totalPakem)
                    },
                    error:function(data){
                        console.log("error");
                    }


                })

            }

            $(document).on('click', '#card_pidum', function(){
                modalPidum();
            });

            $(document).on('click', '#card_pidsus', function(){
                modalPidsus();
            });

            $(document).on('click', '#card_pegawai', function(){
                showModalPegawai();
            });

            function modalPidsus() {
                url = "{{ route('dashboard.pidsus',':satker') }}";
                url = url.replace(':satker',id_satker)
                $.ajax({
                    url: url,
                    beforeSend: function() {
                        $('#overlay').show();
                    }
                }).done(function(res) {
                    $('#modal-container-custom').html(res);
                    $('#kt_modal_1').modal('show');
                })
                .fail(function(err) {
                    $('#overlay').hide();
                })
                .always(function(res) {
                    $('#overlay').hide();
                });
            }

            function modalPidum() {
                url = "{{ url(route('dashboard.pidum',':satker')) }}"
                url = url.replace(':satker',id_satker)
                $.ajax({
                    url: url,
                    beforeSend: function() {
                        $('#overlay').show();
                    }
                }).done(function(res) {
                    $('#modal-container-custom').html(res);
                    $('#kt_modal_1').modal('show');
                })
                .fail(function(err) {
                    $('#overlay').hide();
                })
                .always(function(res) {
                    $('#overlay').hide();
                });
            }
        </script>
        <script>
            function blockPage() {
                $(".modal-loader").fadeIn(100);
            }

            function unblockPage() {
                $(".modal-loader").fadeOut(100);
            }

            // modal data pegawai dan datatablenya
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let _detailTable = null;

            function showModalPegawai() {
                $.ajax({
                    method: "GET",
                    url: "https://absensi.kejaksaan.go.id/absen/api/get_pegawai_mataelang_dan_satker_turunan/02",
                    async: true,
                    beforeSend: function() {
                        blockPage();
                    },
                success: function(result) {
                    if(result.status == "200") {
                        _detailData = result.data;
                        if(_detailTable) {
                            _detailTable.clear();
                            _detailTable.rows.add(_detailData);
                            _detailTable.draw();
                        } else {
                            _detailTable = $("#pegawai-table").DataTable({
                                "orderCellsTop": true,
                                "data" : _detailData,
                                "buttons": [],
                                "dom": "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-6 pb-2'B>>\r\n                          <'row'<'col-sm-12'tr>>\r\n                      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                                "order": [0, "asc"],
                                columnDefs : [
                                    { targets : [5],
                                        render : function (data, type, row) {
                                            switch(data) {
                                                case 1 : return 'Aktif'; break;
                                                case 2 : return 'Diperbantukan'; break;
                                                case 3 : return 'Meninggal'; break;
                                                case 4 : return 'Mengundurkan Diri'; break;
                                                case 5 : return 'Dikaryakan'; break;
                                                case 6 : return 'Diberhentikan'; break;
                                                case 7 : return 'Tidak Aktif'; break;
                                                case 10 : return 'Purnaja'; break;
                                                default  : return 'N/A';
                                            }
                                        }
                                    }
                                ],
                                "columns" : [
                                    {
                                        "data": "DT_RowIndex",
                                        "render": function ( data, type, row, meta ) {
                                            return meta.row + 1;
                                        }
                                    },
                                    { "data" : "nama" },
                                    { "data" : "nip" },
                                    { "data" : "nama_satker" },
                                    { "data" : "eselon" },
                                    { "data" : "status_pegawai" },
                                ],
                                "initComplete": function() {
                                    $('#pegawai-table thead tr:eq(0) th').each(function(i) {
                                        if (i > 0) {
                                            var input = document.createElement('input');
                                            input.className = 'form-control form-control-sm form-search-' + i;
                                            $(document).find('#pegawai-table thead tr:eq(1) th').eq(i).html(input).on('change',
                                                function() {
                                                    var _value = $('#pegawai-table thead tr:eq(1) th').eq(i)
                                                        .find("input").val();
                                                    $("#pegawai-table").DataTable()
                                                        .column(i)
                                                        .search(_value)
                                                        .draw();
                                                });
                                        }
                                    });
                                }
                            });
                        }
                        $('#modalDataPegawai').modal("show");
                    } else {
                        swal("Gagal", result.message, "error");
                    }
                    unblockPage();
                }
            }).done().fail(function(data) {
                if (data.status == 419 || data.status == 401) {
                    location.reload();
                }
                unblockPage();
            });
            }
        </script>
    @endsection
</x-base-layout>
