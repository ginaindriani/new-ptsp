<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="modalDataPegawai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title w-100 text-center mb-5">Pegawai Kejaksaan Sumatera Utara</h1>
        </div>
        <div class="modal-body">
          <div class="col-md-12 text-right">
            <div class="form-group">
                <div class="custom-radio-inline">
                    <input type="radio" id="semuaInline" name="status_pegawai" checked
                        class="custom-control-input input-status_pegawai" value="0">
                    <label class="custom-control-label" style="cursor: pointer;" for="semuaInline">Semua</label>
                </div>
                <div class="custom-radio-inline">
                    <input type="radio" id="aktifInline" name="status_pegawai"
                        class="custom-control-input input-status_pegawai" value="Aktif">
                    <label class="custom-control-label" style="cursor: pointer;" for="aktifInline">Aktif</label>
                </div>
                <div class="custom-radio-inline">
                    <input type="radio" id="diperbantukanInline" name="status_pegawai"
                        class="custom-control-input input-status_pegawai" value="Diperbantukan">
                    <label class="custom-control-label" style="cursor: pointer;" for="diperbantukanInline">Diperbantukan</label>
                </div>
                <div class="custom-radio-inline">
                    <input type="radio" id="meninggalInline" name="status_pegawai"
                        class="custom-control-input input-status_pegawai" value="Meninggal">
                    <label class="custom-control-label" style="cursor: pointer;" for="meninggalInline">Meninggal</label>
                </div>
                <div class="custom-radio-inline">
                    <input type="radio" id="mengundurkanDiriInline" name="status_pegawai"
                        class="custom-control-input input-status_pegawai" value="Mengundurkan Diri">
                    <label class="custom-control-label" style="cursor: pointer;" for="mengundurkanDiriInline">Mengundurkan Diri</label>
                </div>
                <div class="custom-radio-inline">
                    <input type="radio" id="dikaryakanInline" name="status_pegawai"
                        class="custom-control-input input-status_pegawai" value="Dikaryakan">
                    <label class="custom-control-label" style="cursor: pointer;" for="dikaryakanInline">Dikaryakan</label>
                </div>
                <div class="custom-radio-inline">
                    <input type="radio" id="diberhentikanInline" name="status_pegawai"
                        class="custom-control-input input-status_pegawai" value="Diberhentikan">
                    <label class="custom-control-label" style="cursor: pointer;" for="diberhentikanInline">Diberhentikan</label>
                </div>
                <div class="custom-radio-inline">
                    <input type="radio" id="purnajaInline" name="status_pegawai"
                        class="custom-control-input input-status_pegawai" value="Purnaja">
                    <label class="custom-control-label" style="cursor: pointer;" for="purnajaInline">Purnaja</label>
                </div>
                <div class="custom-radio-inline">
                    <input type="radio" id="tidakAktifInline" name="status_pegawai"
                        class="custom-control-input input-status_pegawai" value="Tidak Aktif">
                    <label class="custom-control-label" style="cursor: pointer;" for="tidakAktifInline">Tidak Aktif</label>
                </div>
            </div>
        </div>
          <div class="row">
            <div class="table-responsive">
              <table class='table table-bordered text-center w-100' id='pegawai-table'>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIP</th>
                        <th>Satker</th>
                        <th>Eselon</th>
                        <th>Status</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->