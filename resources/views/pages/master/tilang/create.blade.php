<x-base-layout>
    @section('head_js')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <link href="https://bukutamu.kejaksaan.go.id/assets/frontend/css/webcam-style.css" rel="stylesheet" />
    <script>
        function tokenValidation(token) {
            document.getElementsByName('g_token')[0].value = token;
        }
    </script>
    @endsection
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Tilang
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('tilang.index')}}" class="text-muted text-hover-primary">
                    Data Tilang
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Tambah Data Tilang
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection

    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form id="formsimpan" action="{{ route('tilang.store') }}" method="post" enctype="multipart/form-data" novalidate>
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Tambah Data Tilang</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                <input type="hidden" class="g_token"  name="g_token"  id="g_token" />
                <div class="form-group mb-7">
                    <label for="satker" class="control-label required">PILIH SATUAN KERJA KEJAKSAAN YANG AKAN MENANGANI LAYANAN/PENGADUAN ANDA</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" id="satker" name="satker" required>
                        <option value="">==PILIH SATKER==</option>
                        @foreach ($satker as $s)
                            <option value="{{ $s->id_satker }}">{{ $s->nama_satker }}</option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('satker') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="type" class="control-label required">TIPE PENDAFTARAN TAMU</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" id="type" name="type" required>
                        <option value="">==PILIH TIPE PENDAFTARAN TAMU==</option>
                        <option value="1">TAMU BIASA</option>
                        <option value="2">SAKSI</option>
                        <option value="3">TERDAKWA</option>
                    </select>
                    <p class="text-danger">{{ $errors->first('type') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="no_identitas" class="control-label required">NOMOR IDENTITAS (KTP)</label>
                    <input class="form-control" type="number" pattern="[^()/><\][\\\x22,;|]+" required="required" name="no_identitas" type="no_identitas" placeholder="MASUKKAN NOMOR IDENTITAS (KTP)"
                        id="no_identitas">
                    <p class="text-danger">{{ $errors->first('no_identitas') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nama" class="control-label required">NAMA LENGKAP</label>
                    <input class="form-control" required="required" pattern="[^/><\][\\\x22,;|]+" name="nama" type="nama" placeholder="MASUKKAN NAMA LENGKAP"
                        id="nama">
                    <p class="text-danger">{{ $errors->first('nama') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="jenis_kelamin" class="control-label required">JENIS KELAMIN</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" id="jenis_kelamin" name="jenis_kelamin" required>
                        <option value="">==PILIH JENIS KELAMIN==</option>
                        <option value="1">LAKI-LAKI</option>
                        <option value="2">PEREMPUAN</option>
                    </select>
                    <p class="text-danger">{{ $errors->first('jenis_kelamin') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="email" class="control-label required">EMAIL</label>
                    <input class="form-control" type="email" pattern="[^()/><\][\\\x22,;|]+" required="required" name="email" type="email" placeholder="MASUKKAN EMAIL"
                        id="email">
                    <p class="text-danger">{{ $errors->first('email') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="no_hp" class="control-label required">NOMOR HANDPHONE (UTAMAKAN YANG TERDAFTAR OLEH WHATSAPP, KARENA QR-CODE AKAN DIKIRIM MELALUI WHATSAPP)</label>
                    <input class="form-control" type="number" pattern="[^()/><\][\\\x22,;|]+" required="required" name="no_hp" type="no_hp" placeholder="MASUKKAN NOMOR HANDPHONE"
                        id="no_hp">
                    <p class="text-danger">{{ $errors->first('no_hp') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="alamat" class="control-label required">ALAMAT</label>
                    <input class="form-control" required="required" pattern="[^/><\][\\\x22,;|]+" name="alamat" type="alamat" placeholder="MASUKKAN ALAMAT"
                        id="alamat">
                    <p class="text-danger">{{ $errors->first('alamat') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="tujuan" class="control-label required">TUJUAN MENDAFTAR</label>
                    <input class="form-control" required="required" name="tujuan" pattern="[^/><\][\\\x22,;|]+" type="tujuan" placeholder="MASUKKAN ALASAN MENDAFTAR PADA SISTEM PELAYANAN KEJAKSAAN INI"
                        id="tujuan">
                    <p class="text-danger">{{ $errors->first('tujuan') }}</p>
                </div>
                <legend class="scheduler-border">Foto</legend>
                <div class="form-group mb-7">
                    <span>AMBIL FOTO DIRI ANDA</span>
                    <label style="color: red;">*</label>
                    <center><a id="cameraFlip" class="btn btn-success">GANTI KAMERA</a></center>
                    <div id="bx-cam">
                        <video id="webcam" autoplay playsinline></video>
                        <canvas id="canvas" class="d-none"></canvas>
                        <div id="flash-cam" class="flash-cam"></div>
                        <input type="hidden" name="photo" id="download-photo" />
                    </div>
                    <div id="bx-cam-icon">
                        <div class="row-icon">
                            <a id="take-photo" class="cam-icon">
                            <i class="fa fa-camera"></i>
                            </a>
                            <a id="retake-photo" class="d-none cam-icon">
                            <i class="fa fa-repeat"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <center>
                <div class="form-group mb-7">
                    <input type="checkbox" id="checkme"/> DENGAN INI, SAYA SETUJU UNTUK MEMBERIKAN DATA PRIBADI SAYA
                    <div class="registration-form-action clearfix animated fadeInLeftBig" data-animation="fadeInLeftBig" data-animation-delay=".15s" style="animation-delay: 0.15s;">
                        <input type="file" id="file_ktp" style="display: none;" />
                        <button type="submit" onclick="simpandata()" id="tombol" name="tombol" class="btn btn-primary btn-block">SIMPAN DATA SAYA DAN LANJUTKAN</button>
                    </div>
                </div>
                </center>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->
    @section('scripts')
        <script type="text/javascript">
         ClassicEditor
            .create( document.querySelector( '#ckeditor' ) )
            .then( ckeditor => {
                    console.log( ckeditor );
            } )
            .catch( error => {
                    console.error( error );
            } );

        </script>
        <script src="https://bukutamu.kejaksaan.go.id/assets/frontend/js/webcam-easy-1.0.5.min.js"></script>
        <script type="text/javascript">
            var checker = document.getElementById('checkme');
            var sendbtn = document.getElementById('tombol');
            sendbtn.disabled = true;
            // when unchecked or checked, run the function
            checker.onchange = function(){
            if(this.checked){
                sendbtn.disabled = false;
            } else {
                sendbtn.disabled = true;
            }}
            function simpandata() {
                sendbtn.disabled = true;
                $('#tombol').html('Data sedang disimpan Mohon Tunggu');
                document.getElementById("formsimpan").submit();
            }
            $(document).ready(function() {
                const webcamElement = document.getElementById('webcam');
                const canvasElement = document.getElementById('canvas');
                const snapSoundElement = document.getElementById('snapSound');
                const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
                $('#cameraFlip').click(function() {
                webcam.flip();
                webcam.start();  
                });
                webcam.start()
                    .then(result =>{
                        console.log("webcam started");
                    })
                    .catch(err => {
                        console.log(err);
                    }
                );
        
                function refreshCamera(){
                webcam.start()
                    .then(result =>{
                        console.log("webcam started");
                    })
                    .catch(err => {
                        console.log(err);
                    }
                )
                }
        
                $("#take-photo").click(function () {
                    beforeTakePhoto();
                    let picture = webcam.snap();
                    $("#download-photo").val(picture);
                    Swal.fire({
                    type: 'success',
                    title: 'Berhasil mengambil foto!',
                    html: '<em>'+'Lanjutkan dengan klik Simpan'+'</em>',
                    showCancelButton: false,
                    showConfirmButton: true
                    });
                    // document.querySelector('#download-photo').href = picture;
                    afterTakePhoto();
                });
        
                $("#retake-photo").click(function () {
                    webcam.stream()
                        .then(facingMode =>{
                            removeCapture();
                        }
                    );
                });
        
        
                function beforeTakePhoto(){
                    $('#flash-cam')
                        .show()
                        .animate({opacity: 0.3}, 500)
                        .fadeOut(500)
                        .css({'opacity': 0.7});
                    // window.scrollTo(0, 0);
                    $('#webcam-control').addClass('d-none');
                    $('#cameraControls').addClass('d-none');
                };
        
                function afterTakePhoto(){
                    webcam.stop();
                    $('#canvas').removeClass('d-none');
                    $('#take-photo').addClass('d-none');
                    $('#retake-photo').removeClass('d-none');
                    // $('#download-photo').removeClass('d-none');
                    // $('#resume-camera').removeClass('d-none');
                    // $('#cameraControls').removeClass('d-none');
                }
        
                function removeCapture(){
                    $('#canvas').addClass('d-none');
                    $('#take-photo').removeClass('d-none');
                    $('#retake-photo').addClass('d-none');
                }
        
        
                
        
            });
        </script>
    @endsection
</x-base-layout>
