<x-base-layout>
    @section('head_js')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    @endsection
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Lelang
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Master
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('lelang.index')}}" class="text-muted text-hover-primary">
                    Data Lelang
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Tambah Data Lelang
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection

    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('lelang.store') }}" method="post" enctype="multipart/form-data" novalidate>
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Tambah Data Lelang</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                <div class="form-group mb-7">
                    <label for="id_satker" class="control-label required">Satuan Kerja</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="id_satker" id="id_satker">
                        <option value="">==PILIH SATKER==</option>
                        @foreach ($satker as $satkers)
                            <option value="{{$satkers->id_satker}}">{{$satkers->nama_satker}}</option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('id_satker') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nama_barang_lelang" class="control-label required">Nama Barang</label>
                    <input class="form-control" required="required" name="nama_barang_lelang" type="text"
                        id="nama_barang_lelang">
                    <p class="text-danger">{{ $errors->first('nama_barang_lelang') }}</p>
                </div>
                {{-- <div class="form-group mb-7">
                    <label for="deskripsi_barang" class="control-label required">Deskripsi Barang</label>
                    <input class="form-control" required="required" name="deskripsi_barang" type="text"
                        id="deskripsi_barang">
                    <p class="text-danger">{{ $errors->first('deskripsi_barang') }}</p>
                </div> --}}
                <div class="form-group mb-7">
                    <label for="deskripsi_barang" class="control-label required">Deskripsi Barang</label>
                    <textarea class="form-control ckeditor" required="required" name="deskripsi_barang" id="ckeditor"></textarea>
                    <p class="text-danger">{{ $errors->first('deskripsi_barang') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="harga_barang" class="control-label required">Harga Barang</label>
                    <input class="form-control" required="required" name="harga_barang" type="text"
                        id="harga_barang">
                    <p class="text-danger">{{ $errors->first('harga_barang') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="status_laku" class="control-label required">Status</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="status_laku" id="status_laku">
                        <option value="1"> Belum Terlelang</option>
                        <option value="2"> Sudah Terlelang</option>
                    </select>
                    <p class="text-danger">{{ $errors->first('status_laku') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="tanggal_lelang" class="control-label required">Tanggal Lelang</label>
                    <input class="form-control" required="required" name="tanggal_lelang" type="date"
                        id="tanggal_lelang">
                    <p class="text-danger">{{ $errors->first('tanggal_lelang') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="link_lelang" class="control-label required">Link Lelang (Contohnya : https://lelang.go.id/lot-lelang/detail/612894/PTPNM-Tangerang-Tanahbangunan-SHM-No1356Nerogtog-LT-75-m2-Kel-NerogtogKec-Pinang-Kota-Tangerang-Banten.html)</label>
                    <input class="form-control" required="required" name="link_lelang" type="text"
                        id="link_lelang">
                    <p class="text-danger">{{ $errors->first('link_lelang') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="foto" class="control-label required">Gambar Utama</label>
                    
                    <input class="form-control" required="required" name="foto" type="file"
                        id="foto">
                    <p class="text-danger">{{ $errors->first('foto') }}</p>
                </div>
                
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->
    @section('scripts')
        <script type="text/javascript">
         ClassicEditor
            .create( document.querySelector( '#ckeditor' ) )
            .then( ckeditor => {
                    console.log( ckeditor );
            } )
            .catch( error => {
                    console.error( error );
            } );

        </script>
    @endsection
</x-base-layout>
