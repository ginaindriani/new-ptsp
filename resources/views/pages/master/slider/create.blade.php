<x-base-layout>
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Slider
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Master
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('slider.index')}}" class="text-muted text-hover-primary">
                    Data Slider
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Tambah Data Slider
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection

    <!--begin::Card-->
    @include('components.notification')
    <div class="card">
        <form action="{{ route('slider.store') }}" method="post" enctype="multipart/form-data">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Tambah Data Slider</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                <div class="form-group mb-7">
                    <label for="tipe_slider" class="control-label required">Tipe Slider</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="tipe_slider" id="tipe_slider">
                        <option value="1">Slider Atas (Main Slider)</option>
                        <option value="2">Slider Kanan (External Link)</option>
                        <option value="3">Slider Bawah (Tulisan Berjalan)</option>
                    </select>
                    <p class="text-danger">{{ $errors->first('tipe_slider') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="judul_slider" class="control-label required">Judul Slider</label>
                    <input class="form-control" required="required" name="judul_slider" type="text"
                        id="judul_slider">
                    <p class="text-danger">{{ $errors->first('judul_slider') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="deskripsi_slider" class="control-label required">Deskripsi Slider</label>
                    <input class="form-control" required="required" name="deskripsi_slider" type="text"
                        id="deskripsi_slider">
                    <p class="text-danger">{{ $errors->first('deskripsi_slider') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="url_slider" class="control-label">Url Slider</label>
                    <input class="form-control"  name="url_slider" type="text"
                        id="url_slider">
                    <p class="text-danger">{{ $errors->first('url_slider') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="gambar" class="control-label required">Gambar</label>
                    <input class="form-control" required="required" name="gambar" type="file"
                        id="gambar">
                    <p class="text-danger">{{ $errors->first('gambar') }}</p>
                </div>
                
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->

</x-base-layout>
