<x-base-layout>
    @section('head_js')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    @endsection
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Berita
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Master
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('berita.index')}}" class="text-muted text-hover-primary">
                    Data Berita
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Tambah Data Berita
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection

    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('berita.update', $berita->id) }}" method="post" enctype="multipart/form-data" novalidate>
            
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Tambah Data Berita</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                @method('PUT') 
                <div class="form-group mb-7">
                    <label for="id_satker" class="control-label required">Satuan Kerja</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="id_satker" id="id_satker">
                            <option value="{{$selfsatker->id_satker}}" selected>{{$selfsatker->nama_satker}}</option>
                        @foreach ($satker as $satkers)
                            <option value="{{$satkers->id_satker}}">{{$satkers->nama_satker}}</option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('id_satker') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="judul_berita" class="control-label required">Judul Berita</label>
                    <input class="form-control" required="required" value="{{$berita->judul_berita}}" name="judul_berita" type="text"
                        id="judul_berita">
                    <p class="text-danger">{{ $errors->first('judul_berita') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="deskripsi_berita" class="control-label required">Deskripsi Berita</label>
                    <textarea class="form-control ckeditor" required="required" value="{{$berita->deskripsi_berita}}" name="deskripsi_berita" id="ckeditor">{{$berita->deskripsi_berita}}</textarea>
                    <p class="text-danger">{{ $errors->first('deskripsi_berita') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="fotoold" class="control-label "> Gambar</label>
                    <img src="{{ asset($berita->foto)}}" width="100" height="100">
                </div>
                <div class="form-group mb-7">
                    <label for="foto" class="control-label "> Upload Gambar (Jika ingin mengganti Gambar)</label>
                    
                    <input class="form-control" required="required" name="foto"  type="file"
                        id="foto">
                    <p class="text-danger">{{ $errors->first('foto') }}</p>
                </div>
                
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->
    @section('scripts')
        <script type="text/javascript">
         ClassicEditor
            .create( document.querySelector( '#ckeditor' ) )
            .then( ckeditor => {
                    console.log( ckeditor );
            } )
            .catch( error => {
                    console.error( error );
            } );

        </script>
    @endsection
</x-base-layout>
