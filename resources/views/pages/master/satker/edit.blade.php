<x-base-layout>
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Satker
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Master
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('satker.index')}}" class="text-muted text-hover-primary">
                    Data Satker
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Edit Data Satker
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection

    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('satker.update', $data->id_satker) }}" method="post">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Edit Data Satker</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                @method('PUT')
                <div class="form-group mb-7">
                    <label for="nama_satker" class="control-label required">Nama Satker</label>
                    <input class="form-control" required="required" name="nama_satker" type="text"
                        id="nama_satker" value="{{ $data->nama_satker }}">
                    <p class="text-danger">{{ $errors->first('nama_satker') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nomor" class="control-label required">Parent Satker</label>
                    <select class="parent_id-control form-control form-select-solid" data-kt-select2="true" name="parent_id" id="parent_id">
                        @foreach ($satker as $d)
                        <option value="{{ $d['id_satker'] }}" {{ $d['id_satker'] == $data['parent_id'] ? 'selected' : '' }}> {{ $d['nama_satker'] }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('parent_id') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="tipe_satker" class="control-label required">Tipe Satker</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="tipe_satker" id="tipe_satker" required="required">
                        @foreach ($tipeSatker as $key => $d)
                        <option value="{{ $key }}" {{ $key == $data['tipe_satker'] ? 'selected' : '' }}> {{ $d }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('tipe_satker') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="alamat_satker" class="control-label required">Alamat</label>
                    <input class="form-control" name="alamat_satker" type="text"
                        id="alamat_satker" value="{{ $data->alamat_satker }}">
                    <p class="text-danger">{{ $errors->first('alamat_satker') }}</p>
                    <small id="alamatHelp" class="form-text text-muted">Contoh: Jl. Sultan Hasanuddin Nomor 1, Kebayoran Baru, Jakarta Selatan</small>
                </div>
                <div class="form-group mb-7">
                    <label for="telp_satker" class="control-label required">Telepon</label>
                    <input class="form-control" name="telp_satker" type="text"
                        id="telp_satker" value="{{ $data->telp_satker }}">
                    <p class="text-danger">{{ $errors->first('telp_satker') }}</p>
                    <small id="alamatHelp" class="form-text text-muted">Contoh: Telp. (021) 7203061 – 63 (hunting) fax. (021) …………….</small>
                </div>
                <div class="form-group mb-7">
                    <label for="website_satker" class="control-label required">Website</label>
                    <input class="form-control" name="website_satker" type="text"
                        id="website_satker" value="{{ $data->website_satker }}">
                    <p class="text-danger">{{ $errors->first('website_satker') }}</p>
                    <small id="alamatHelp" class="form-text text-muted">Contoh: www.kejaksaan.go.id</small>
                </div>
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->

</x-base-layout>
