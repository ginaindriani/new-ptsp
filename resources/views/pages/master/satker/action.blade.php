<form id="deleteForm" action="{{ route('satker.destroy', $data->id_satker) }}" method="post">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <a href="{{ route('satker.edit', $data->id_satker) }}" class="btn btn-icon btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
        <i class="fas fa-pencil-alt text-white"></i>
    </a>
    <button onclick="deleteData(event, {{ $data->id_satker }})" data-id="{{ $data->id_satker }}" class="btn btn-icon btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
        <i class="fas fa-trash text-white"></i>
    </button>
</form>