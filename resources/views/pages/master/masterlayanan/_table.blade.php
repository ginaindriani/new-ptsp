<!--begin::Table-->
{{ $dataTable->table(['id' => 'MasterLayanan-table'], true) }}
<!--end::Table-->

{{-- Inject Scripts --}}
@section('scripts')
{{ $dataTable->scripts() }}
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
<script>
    function deleteData(event)
    {
        event.preventDefault();
        const id = event.currentTarget.getAttribute('data-id');
        
        let url = "{{ route('master_layanan.destroy', ':id') }}";
        url = url.replace(':id', id);
        Swal.fire({
                title: 'Anda yakin?',
                text: 'Akan menghapus layanan ini',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {
                if (result.value) {
                    let submit = $("#deleteForm").attr('action', url);
                    submit.submit();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
    }
    
</script>
@endsection
