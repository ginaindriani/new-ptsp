<form id="deleteForm" action="{{ route('master_layanan.destroy', $data->id_layanan) }}" method="post">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <a href="{{ route('master_layanan.edit', $data->id_layanan) }}" class="btn btn-icon btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
        <i class="fas fa-pencil-alt text-white"></i>
    </a>
    <button onclick="deleteData(event, {{ $data->id_layanan }})" data-id="{{ $data->id_layanan }}" class="btn btn-icon btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
        <i class="fas fa-trash text-white"></i>
    </button>
</form>