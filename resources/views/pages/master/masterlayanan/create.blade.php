<x-base-layout>
    @include('components.notification')
    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('master_layanan.store') }}" method="post">
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                <div class="form-group mb-7">
                    <label for="nama_layanan" class="control-label required">Nama Layanan</label>
                    <input class="form-control" required="required" name="nama_layanan" type="text" value="{{old('nama_layanan')}}"
                        id="nama_layanan">
                    <p class="text-danger">{{ $errors->first('nama_layanan') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="parameter" class="control-label required">Parameter</label>
                    <textarea class="form-control" required="required" name="parameter" type="text"
                        id="parameter">{{old('parameter')}}</textarea>
                    <p class="text-danger">{{ $errors->first('parameter') }}</p>
                    <small id="parameterHelp" class="form-text text-muted">Masukkan Json Parameter</small>
                </div>
                <div class="form-group mb-7">
                    <label for="blade" class="control-label required">Nama Blade Print</label>
                    <input class="form-control" required="required" name="blade" type="text" value="{{old('blade')}}"
                        id="blade">
                    <p class="text-danger">{{ $errors->first('blade') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="blade" class="control-label required">Nama Blade Result</label>
                    <input class="form-control" required="required" name="blade_result" type="text" value="{{old('blade_result')}}"
                        id="blade_result">
                    <p class="text-danger">{{ $errors->first('blade_result') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="tipe_layanan" class="control-label required">Tipe Layanan</label>
                    <select class="form-control form-select-solid" required="required" data-kt-select2="true" name="tipe_layanan" id="tipe_layanan">
                        <option value=""> --Pilih Tipe-- </option>
                        <option {{old('tipe_layanan') == 'layanan' ? 'selected' : ''}} value="layanan"> Layanan </option>
                        <option {{old('tipe_layanan') == 'sistem' ? 'selected' : ''}} value="sistem"> Informasi </option>
                    </select>
                    <p class="text-danger">{{ $errors->first('tipe_layanan') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="type" class="control-label required">Jenis Layanan</label>
                    <select class="form-control form-select-solid" required="required" data-kt-select2="true" name="type" id="type">
                        <option value=""> --Pilih Jenis-- </option>
                        <option {{old('type') == 'in_system' ? 'selected' : ''}} value="in_system"> in_system </option>
                        <option {{old('type') == 'hyperlink' ? 'selected' : ''}} value="hyperlink"> hyperlink </option>
                    </select>
                    <p class="text-danger">{{ $errors->first('type') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="aktif" class="control-label required">Status Aktif</label>
                    <select class="form-control form-select-solid" required="required" data-kt-select2="true" name="aktif" id="aktif">
                        <option value=""> --Pilih Status-- </option>
                        <option {{old('aktif') == '1' ? 'selected' : ''}} value="1"> Aktif </option>
                        <option {{old('aktif') == '0' ? 'selected' : ''}} value="0"> Tidak Aktif </option>
                    </select>
                </div>
                <div class="form-group mb-7">
                    <label for="order_by" class="control-label required">Urutan</label>
                    <input class="form-control" required="required" name="order_by" type="text" value="{{old('order_by')}}"
                        id="order_by">
                    <p class="text-danger">{{ $errors->first('order_by') }}</p>
                    <small id="order_byHelp" class="form-text text-muted">Urutan Layanan yang akan tampil pada landing page halaman publik</small>
                </div>
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->

    @section('scripts')
        <script>

            //Script NIP
            $("input[name='order_by']").on('input', function (e) {
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
            });
        </script>
    @endsection
</x-base-layout>
