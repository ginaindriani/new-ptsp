@section('button_add')
    <a href="{{ route('master_layanan.create') }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Tambah Layanan">
        Tambah Layanan
    </a>
@endsection

<x-base-layout>
    @include('components.notification')
    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body pt-6">
            <div class="table-responsive">
                @include('pages.master.masterlayanan._table')
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

</x-base-layout>
