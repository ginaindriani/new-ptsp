<x-base-layout>
    @include('components.notification')
    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('master_layanan.update', $data->id_layanan) }}" method="post">
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                @method('PUT')
                <div class="form-group mb-7">
                    <label for="nama_layanan" class="control-label required">Nama Layanan</label>
                    <input class="form-control" required="required" name="nama_layanan" type="text"
                    id="nama_layanan" value="{{ $data->nama_layanan }}">
                    <p class="text-danger">{{ $errors->first('nama_layanan') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="parameter" class="control-label required">Parameter</label>
                    <textarea class="form-control" required="required" name="parameter" type="text"
                    id="parameter">{{ json_encode($data->parameter) }}</textarea>
                    <p class="text-danger">{{ $errors->first('parameter') }}</p>
                    <small id="parameterHelp" class="form-text text-muted">Masukkan Json Parameter</small>
                </div>
                <div class="form-group mb-7">
                    <label for="blade" class="control-label required">Nama Blade Print</label>
                    <input class="form-control" required="required" name="blade" type="text"
                        id="blade" value="{{ $data->blade }}">
                    <p class="text-danger">{{ $errors->first('blade') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="blade" class="control-label required">Nama Blade Result</label>
                    <input class="form-control" required="required" name="blade_result" type="text"
                        id="blade_result" value="{{ $data->blade_result }}">
                    <p class="text-danger">{{ $errors->first('blade_result') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="tipe_layanan" class="control-label required">Tipe Layanan</label>
                    <select class="form-control form-select-solid" required="required" data-kt-select2="true" name="tipe_layanan" id="tipe_layanan">
                        <option value=""> --Pilih Tipe-- </option>
                        <option value="layanan" {{ "layanan" == $data->tipe_layanan ? 'selected' : '' }}> Layanan </option>
                        <option value="sistem" {{ "sistem" == $data->tipe_layanan ? 'selected' : '' }}> Informasi </option>
                    </select>
                    <p class="text-danger">{{ $errors->first('tipe_layanan') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="type" class="control-label required">Jenis Layanan</label>
                    <select class="form-control form-select-solid" required="required" data-kt-select2="true" name="type" id="type">
                        <option value=""> --Pilih Jenis-- </option>
                        <option value="in_system" {{ "in_system" == $data->type ? 'selected' : '' }}> in_system </option>
                        <option value="hyperlink" {{ "hyperlink" == $data->type ? 'selected' : '' }}> hyperlink </option>
                    </select>
                    <p class="text-danger">{{ $errors->first('type') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="aktif" class="control-label required">Status Aktif</label>
                    <select class="form-control form-select-solid" required="required" data-kt-select2="true" name="aktif" id="aktif">
                        <option value=""> --Pilih Status-- </option>
                        <option value="1" {{ "1" == $data->aktif ? 'selected' : '' }}> Aktif </option>
                        <option value="0" {{ "0" == $data->aktif ? 'selected' : '' }}> Tidak Aktif </option>
                    </select>
                </div>
                <div class="form-group mb-7">
                    <label for="order_by" class="control-label required">Urutan</label>
                    <input class="form-control" required="required" name="order_by" type="text"
                        id="order_by" value="{{ $data->order_by }}">
                    <p class="text-danger">{{ $errors->first('order_by') }}</p>
                    <small id="order_byHelp" class="form-text text-muted">Urutan Layanan yang akan tampil pada landing page halaman publik</small>
                </div>
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->

</x-base-layout>
