@section('button_add')
    <a href="{{ route('datatamu.create') }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Tambah Satker">
        Tambah Data Tamu
    </a>
@endsection

<x-base-layout>
    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">Data Tamu</h3>
            </div>
        </div>
        <!--begin::Card body-->
        <div class="card-body pt-6">
            <div class="table-responsive">
                @include('pages.master.datatamu._table')
            </div>
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

</x-base-layout>
