<div class="modal fade" tabindex="-1" id="kt_modal_1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Data answer</h5>
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="svg-icon svg-icon-2x">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label class="mb-3">Nama</label>
                        <input type="text" name="name" class="form-control mb-5" disabled value="{{$ruangDiskusi->name}}">
                    </div>
                    <div class="col-md-12">
                        <label class="mb-3">Email</label>
                        <input type="text" name="email" class="form-control mb-5" disabled value="{{$ruangDiskusi->email}}">
                    </div>
                    <div class="col-md-12">
                        <label class="mb-3">Phone</label>
                        <input type="text" name="phone" class="form-control mb-5" disabled value="{{$ruangDiskusi->phone}}">
                    </div>
                    <div class="col-md-12">
                        <label class="mb-3">Pesan</label>
                        <textarea name="message" class="form-control mb-5" disabled rows="5">{{$ruangDiskusi->message}}</textarea>
                    </div>
                    <div class="col-md-12">
                        <label class="mb-3">Jawaban</label>
                        <textarea name="answer" placeholder="Silahkan isi jawaban anda disini." class="form-control mb-5" rows="5">{{$ruangDiskusi->answer}}</textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-block">
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" href="{{ route('ruang-diskusi.answer', ['id' => $ruangDiskusi->id]) }}" class="btn btn-success w-100" id="save-data">Save</button>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-light w-100" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>