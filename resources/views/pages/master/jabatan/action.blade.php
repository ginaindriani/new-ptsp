<form id="deleteForm" action="{{ route('jabatan.destroy', $data->id_jabatan) }}" method="post">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <a href="{{ route('jabatan.edit', $data->id_jabatan) }}" class="btn btn-icon btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
        <i class="fas fa-pencil-alt text-white"></i>
    </a>
	@role('superadmin')
    <button onclick="deleteData(event, {{ $data->id_jabatan }})" data-id="{{ $data->id_jabatan }}" class="btn btn-icon btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
        <i class="fas fa-trash text-white"></i>
    </button>
	@endrole
</form>