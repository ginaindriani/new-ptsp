<x-base-layout>
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Jabatan
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Master
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('jabatan.index')}}" class="text-muted text-hover-primary">
                    Data Jabatan
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Edit Data Jabatan
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection
    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('jabatan.update', $dataJabatan->id_jabatan) }}" method="post">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Edit Data Jabatan</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                @method('PUT')
                <div class="form-group mb-7">
                    <label for="nomor" class="control-label required">Nama Jabatan</label>
                    <input class="form-control" required="required" name="nama_jabatan" type="text"
                        id="nama_jabatan" value="{{ $dataJabatan->nama_jabatan }}">
                    <p class="text-danger">{{ $errors->first('nama_jabatan') }}</p>
                </div>
                <div class="form-group mb-7" @if($role != "superadmin") hidden @endif>
                    <label for="nomor" class="control-label required">Parent Jabatan</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="parent_id" id="parent_id">
                        @foreach ($jabatan as $d)
                        <option value="{{ $d['id_jabatan'] }}" {{ $d['id_jabatan'] == $dataJabatan['parent_id'] ? 'selected' : '' }}> {{ $d['nama_jabatan'] }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-7">
                    <label for="nomor" class="control-label required">Eselon</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="eselon" id="eselon" required="required">
                        @foreach ($tipeJabatan as $key => $d)
                        <option value="{{ $key }}" {{ $key == $dataJabatan['eselon'] ? 'selected' : '' }}> {{ $d }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('eselon') }}</p>
                </div>
                <div class="form-group mb-7"  @if($role != "superadmin") hidden @endif>
                    <label for="nomor" class="control-label required">Is Admin</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="is_admin" id="is_admin" required="required">
                        <option value="0" {{ $dataJabatan['is_admin'] == false ? 'selected' : '' }}> Tidak </option>
                        <option value="1" {{ $dataJabatan['is_admin'] == true ? 'selected' : '' }}> Ya </option>
                    </select>
                    <p class="text-danger">{{ $errors->first('is_admin') }}</p>
                </div>
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->

</x-base-layout>
