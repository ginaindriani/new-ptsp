<x-base-layout>
    @section('head_js')
    <script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    @endsection
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Jadwal Sidang
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Informasi
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('jadwalsidang.index')}}" class="text-muted text-hover-primary">
                    Data Jadwal Sidang
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Ubah Data Jadwal Sidang
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection

    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('jadwalsidang.update', $jadwalsidang->id) }}" method="post" enctype="multipart/form-data" novalidate>
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Ubah Data Jadwal Sidang</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                @method('PUT') 
                <div class="form-group mb-7">
                    <label for="id_satker" class="control-label required">Satuan Kerja</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="id_satker" id="id_satker">
                        <option value="{{$selfsatker->id_satker}}">{{$selfsatker->nama_satker}}</option>
                        @foreach ($satker as $satkers)
                            <option value="{{$satkers->id_satker}}">{{$satkers->nama_satker}}</option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('id_satker') }}</p>
                </div>
                
                
                <div class="form-group mb-7">
                    <label for="perkara" class="control-label required">Klasifikasi Perkara</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="perkara" id="perkara">
                        <option value="{{$jadwalsidang->perkara}}">{{$jadwalsidang->perkara}}</option>
                        <option value="OHARDA">OHARDA</option>
                        <option value="KAMNEGTIBUM DAN TPUL">KAMNEGTIBUM DAN TPUL</option>
                        <option value="NARKOTIKA">NARKOTIKA</option>
                        <option value="TERORISME">TERORISME</option>
                        <option value="TINDAK PIDANA KORUPSI & TPPU">TINDAK PIDANA KORUPSI & TPPU</option>
                        <option value="TINDAK PIDANA PERPAJAKAN & TPPU">TINDAK PIDANA PERPAJAKAN & TPPU</option>
                        <option value="TINDAK PIDANA KEPABEANAN, CUKAI & TPPU">TINDAK PIDANA KEPABEANAN, CUKAI & TPPU</option>
                    </select>
                    <p class="text-danger">{{ $errors->first('perkara') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="pasal" class="control-label required">Pasal</label>
                    <input class="form-control" value="{{$jadwalsidang->pasal}}" required="required" name="pasal" type="text"
                        id="pasal">
                    <p class="text-danger">{{ $errors->first('pasal') }}</p>
                </div>
                <hr/>
                <div class="form-group mb-7">
                    <label for="daftar_terdakwa" class="control-label required">Nama Terdakwa </label>
                    <input class="form-control" value="{{$jadwalsidang->daftar_terdakwa}}" required="required" type="text"
                        id="pasal" disabled>
                    
                    <table class="table table-bordered" id="dynamicAddRemoveTerdakwa">
                        <tr>
                            <th>Nama Terdakwa</th>
                            <th>Action</th>
                        </tr>
                        <tr>
                            <td><input type="text" name="daftar_terdakwa[]" placeholder="Masukkan Nama Terdakwa" class="form-control" /></td>
                            
                            <td><button type="button" name="add" id="dynamic-terdakwa" class="btn btn-primary btn-block">Tambah</button></td>
                        </tr>
                    </table>
                    <p class="text-danger">Jika anda ingin melakukan pengubahan terkait daftar Nama Terdakwa silahkan diisi ulang inputan diatas ini, jika tidak kosongkan saja</p>
                </div>
                <hr/>
                
                <div class="form-group mb-7">
                    <label for="daftar_jaksa" class="control-label required">Nama Jaksa </label>
                    <input class="form-control" value="{{$jadwalsidang->daftar_jaksa}}" required="required" type="text"
                        id="jaksa" disabled>
                    <table class="table table-bordered" id="dynamicAddRemoveJaksa">
                      <tr>
                          <th>Nama Jaksa</th>
                          <th>Action</th>
                      </tr>
                      <tr>
                        <td>
                            <select class="form-control form-select-solid" data-kt-select2="true" name="daftar_jaksa[]" id="jaksa">
                                <option value="">==PILIH JAKSA==</option>
                                @foreach ($pegawai as $pegawai)
                                    <option value="{{$pegawai->nip}}">{{$pegawai->nama}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td><button type="button" name="add" id="dynamic-jaksa" class="btn btn-primary btn-block">Tambah</button></td>
                      </tr>
                    </table>
                    <p class="text-danger">Jika anda ingin melakukan pengubahan terkait daftar Nama Jaksa silahkan diisi ulang inputan diatas ini, jika tidak kosongkan saja</p>
                </div>
                <hr/>
                <div class="form-group mb-7">
                    <label for="tahapan_sidang" class="control-label required">Tahapan Sidang</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="tahapan_sidang" id="tahapan_sidang">
                        <option value="{{$jadwalsidang->tahap_sidang}}">{{$jadwalsidang->tahap_sidang}}</option>
                        @foreach ($stepSidang as $stepSidang)
                            <option value="{{$stepSidang->step_sidang}}">{{$stepSidang->step_sidang}}</option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('tahapan_sidang') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="tanggal_sidang" class="control-label required">Tanggal Sidang</label>
                    <input class="form-control" required="required" name="tanggal_sidang" type="date"
                        id="tanggal_sidang" value="{{$jadwalsidang->tanggal_sidang}}">
                    <p class="text-danger">{{ $errors->first('tanggal_sidang') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="keterangan" class="control-label required">Keterangan Tambahan</label>
                    <textarea class="form-control" id="keterangan" name="keterangan"  required="required" value="{{$jadwalsidang->keterangan}}" rows="10" cols="70">{{$jadwalsidang->keterangan}}</textarea>
                    <p class="text-danger">{{ $errors->first('keterangan') }}</p>
                </div>
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->
    @section('scripts')
        <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var token = $("input[name='_token']").val();
        var result = "";
        $.ajax({
            url: "{{route('jadwalsidang.getNip')}}",
            method: 'POST',
            async:false,
            data: {_token:token},
            success: function(data) {                
                data.forEach(myFunction);
                
                function myFunction(item) {
                    result += `<option value="${item.nip}">${item.nama}</option>`
                }
            }
        });
       

         ClassicEditor
            .create( document.querySelector( '#ckeditor' ) )
            .then( ckeditor => {
                    
            } )
            .catch( error => {

            } );

            $(document).ready(function() {
                var terdakwa = 0;
                $("#dynamic-terdakwa").click(function () {
                    ++terdakwa;
                    $("#dynamicAddRemoveTerdakwa").append('<tr id="row'+terdakwa+'"><td><input type="text" name="daftar_terdakwa[]" placeholder="Masukkan Nama Terdakwa" class="form-control" /></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
                });
                $(document).on('click', '.remove-input-field', function () {
                    $(this).parents('tr').remove();
                });


                var jaksa = 0;
                $("#dynamic-jaksa").click(function () {
                    ++terdakwa;
                    $("#dynamicAddRemoveJaksa").append('<tr id="row'+jaksa+'"><td><select class="form-control form-select-solid" data-kt-select2="true" name="daftar_jaksa[]" id="jaksa"><option value="">==PILIH JAKSA==</option>'+result+'</select></td><td><button type="button" class="btn btn-danger btn-block remove-input-field">Hapus</button></td></tr>');
                    initSelect2();
                });
                $(document).on('click', '.remove-input-field', function () {
                    $(this).parents('tr').remove();
                });
            });
        </script>
    @endsection
</x-base-layout>
