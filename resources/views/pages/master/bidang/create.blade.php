<x-base-layout>
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Bidang
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Master
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('bidang.index')}}" class="text-muted text-hover-primary">
                    Data Bidang
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Tambah Data Bidang
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection

    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('bidang.store') }}" method="post">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Tambah Data Bidang</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                <div class="form-group mb-7">
                    <label for="nomor" class="control-label required">Nama Bidang</label>
                    <input class="form-control" required="required" name="nama_bidang" type="text"
                        id="nama_bidang">
                    <p class="text-danger">{{ $errors->first('nama_bidang') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nomor" class="control-label required">Nama Lengkap Bidang</label>
                    <input class="form-control" required="required" name="nama_bidang_full" type="text"
                        id="nama_bidang_full">
                    <p class="text-danger">{{ $errors->first('nama_bidang_full') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nomor" class="control-label required">Parent Bidang</label>
                    <select class="form-control form-select-solid"  data-kt-select2="true" name="parent_id" id="parent_id">
                        @foreach ($bidang as $data)
                        <option value="{{ $data['id_bidang'] }}"> {{ $data['nama_bidang'] }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('parent_id') }}</p>
                </div>
                <div class="form-group mb-7">
                    <label for="nomor" class="control-label required">Main Parent Bidang</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="main_parent_id" id="main_parent_id">
                        @foreach ($mainBidang as $data)
                        <option value="{{ $data['id_bidang'] }}"> {{ $data['nama_bidang'] }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('main_parent_id') }}</p>
                </div>
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->
    @section('scripts')
    <script>
        KTUtil.onDOMContentLoaded(function() {
                $(".select2").select2();
        });
    </script>
    @endsection
</x-base-layout>
