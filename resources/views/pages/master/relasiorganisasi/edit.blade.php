<x-base-layout>
    @section('styles')
        <style>
            .select2-container--bootstrap5 .select2-selection {
                height: 42.5px;
            }
        </style>
    @endsection
    @section('breadcrumb')
    <div data-kt-swapper="&quot;true&quot;" data-kt-swapper-mode="&quot;prepend&quot;" data-kt-swapper-parent="&quot;{default:" &#039;#kt_content_container&#039;,="" &#039;lg&#039;:="" &#039;#kt_toolbar_container&#039;}&quot;="" class="d-flex align-items-center me-3">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">
            Data Relasi Organisasi
        </h1>
        <!--end::Title-->
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('dashboard')}}" class="text-muted text-hover-primary">
                    Home
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                Master
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-muted">
                <a href="{{route('relasiorganisasi.index')}}" class="text-muted text-hover-primary">
                    Data Relasi Organisasi
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <span class="bullet bg-gray-200 w-5px h-2px"></span>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-dark">
                Edit Relasi Organisasi
            </li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    @endsection
    <!--begin::Card-->
@include('components.notification')
    <div class="card">
        <form action="{{ route('relasiorganisasi.update', $dataOrganisasi->id_organisasi) }}" method="post">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="fw-bolder">Edit Relasi Organisasi</h3>
                </div>
            </div>
            <!--begin::Card body-->
            <div class="card-body pt-6">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="nomor" class="control-label required">Pilih Satker</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="id_satker" id="id_satker" required="required">
                        @foreach ($satker as $d)
                        <option value="{{ $d['id_satker'] }}" {{ $d['id_satker'] == $dataOrganisasi['satker']['id_satker'] ? 'selected' : '' }}> {{ $d['nama_satker'] }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('id_satker') }}</p>
                </div>
                <div class="form-group">
                    <label for="nomor" class="control-label required">Pilih Bidang</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="id_bidang" id="id_bidang" required="required">
                        @foreach ($bidang as $d)
                        <option value="{{ $d['id_bidang'] }}" {{ $d['id_bidang'] == $dataOrganisasi['bidang']['id_bidang'] ? 'selected' : '' }}> {{ $d['nama_bidang'] }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('id_bidang') }}</p>
                </div>
                <div class="form-group">
                    <label for="nomor" class="control-label required">Pilih Jabatan</label>
                    <select class="form-control form-select-solid" data-kt-select2="true" name="id_jabatan" id="id_jabatan" required="required">
                        @foreach ($jabatan as $d)
                        <option value="{{ $d['id_jabatan'] }}" {{ $d['id_jabatan'] == $dataOrganisasi['jabatan']['id_jabatan'] ? 'selected' : '' }}> {{ $d['nama_jabatan'] }} </option>
                        @endforeach
                    </select>
                    <p class="text-danger">{{ $errors->first('id_jabatan') }}</p>
                </div>
                {{-- <div class="form-group">
                    <label for="kode_pejabat" class="control-label required">Kode Pejabat</label>
                    <input class="form-control" name="kode_pejabat" type="text" id="kode_pejabat" 
                    value="{{ $dataOrganisasi->kode_pejabat }}">
                    <p class="text-danger">{{ $errors->first('kode_pejabat') }}</p>
                </div> --}}
                <button class="btn btn-primary waves-effect waves-classic waves-effect waves-classic" type="submit">Simpan</button>
            </div>
            <!--end::Card body-->
        </form>
    </div>
    <!--end::Card-->

</x-base-layout>
