<form id="deleteForm" action="{{ route('relasiorganisasi.destroy', $data->id_organisasi) }}" method="post">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <a href="{{ route('relasiorganisasi.edit', $data->id_organisasi) }}" class="btn btn-icon btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
        <i class="fas fa-pencil-alt text-white"></i>
    </a>
    <button onclick="deleteData(event, {{ $data->id_organisasi }})" data-id="{{ $data->id_organisasi }}" class="btn btn-icon btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
        <i class="fas fa-trash text-white"></i>
    </button>
</form>