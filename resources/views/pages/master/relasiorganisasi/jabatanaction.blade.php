<a href="{{ route('kdjabatan.edit', $data->id_organisasi) }}" class="btn btn-icon btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
    <i class="fas fa-pencil-alt text-white"></i>
</a>