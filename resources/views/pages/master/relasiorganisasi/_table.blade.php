<!--begin::Table-->
{{ $dataTable->table(['id' => 'relasiorganisasi-table'], true) }}
<!--end::Table-->

{{-- Inject Scripts --}}
@section('scripts')
{{ $dataTable->scripts() }}
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
<script>
    function deleteData(event, id)
    {
        event.preventDefault();
        let url = "{{ route('relasiorganisasi.destroy', ':id') }}";
        url = url.replace(':id', id);
        var _this = this;
        Swal.fire({
                title: 'Anda yakin?',
                text: 'Akan menghapus relasiorganisasi ini',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {
                if (result.value) {
                    let submit = $("#deleteForm").attr('action', url);
                    submit.submit()
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
    }
</script>
@endsection
