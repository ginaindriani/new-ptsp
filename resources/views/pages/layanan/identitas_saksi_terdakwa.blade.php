@section('styles')
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
{{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> --}}

<link rel="stylesheet" href="{{url('')}}/demo1/css/application/webcam-style.css" />
<link href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<style>
    .grecaptcha-badge {
        bottom: 100px !important;
    }

    @media (min-width: 1400px) {

        .container-xxl,
        .container-xl,
        .container-lg,
        .container-md,
        .container-sm,
        .container {
            max-width: 1340px !important;
        }
    }

    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
@endsection
@section('head_js')
<script type="text/javascript">
    function tokenValidation(token) {
        document.getElementsByName('g_token')[0].value = token;
    }
</script>
{!! htmlScriptTagJsApi([
'action' => 'submit',
'custom_validation' => 'tokenValidation'
]) !!}
@endsection

<x-base-layout>
    @include('components.notification')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">Lengkapi Identitas</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <!--begin::Stepper-->
            <div class="stepper stepper-pills d-flex flex-column" id="kt_create_account_stepper" data-type="identitas_saksi_terdakwa">
                <!--begin::Nav-->
                <div class="stepper-nav flex-center flex-wrap mb-10">

                    <!--begin::Step 1-->
                    <div class="stepper-item mx-2 my-4 current" data-kt-stepper-element="nav">
                        <!--begin::Line-->
                        <div class="stepper-line w-40px"></div>
                        <!--end::Line-->

                        <!--begin::Icon-->
                        <div class="stepper-icon w-40px h-40px">
                            <i class="stepper-check fas fa-check"></i>
                            <span class="stepper-number">1</span>
                        </div>
                        <!--end::Icon-->

                        <!--begin::Label-->
                        <div class="stepper-label">
                            <h3 class="stepper-title">
                                Step 1
                            </h3>

                            <div class="stepper-desc">
                                Data Pribadi
                            </div>
                        </div>
                        <!--end::Label-->
                    </div>
                    <!--end::Step 1-->

                    <!--begin::Step 2-->
                    <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                        <!--begin::Line-->
                        <div class="stepper-line w-40px"></div>
                        <!--end::Line-->

                        <!--begin::Icon-->
                        <div class="stepper-icon w-40px h-40px">
                            <i class="stepper-check fas fa-check"></i>
                            <span class="stepper-number">2</span>
                        </div>
                        <!--end::Icon-->

                        <!--begin::Label-->
                        <div class="stepper-label">
                            <h3 class="stepper-title">
                                Step 2
                            </h3>

                            <div class="stepper-desc">
                                Data Biografi
                            </div>
                        </div>
                        <!--end::Label-->
                    </div>
                    <!--end::Step 2-->

                    <!--begin::Step 3-->
                    <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                        <!--begin::Line-->
                        <div class="stepper-line w-40px"></div>
                        <!--end::Line-->

                        <!--begin::Icon-->
                        <div class="stepper-icon w-40px h-40px">
                            <i class="stepper-check fas fa-check"></i>
                            <span class="stepper-number">3</span>
                        </div>
                        <!--begin::Icon-->

                        <!--begin::Label-->
                        <div class="stepper-label">
                            <h3 class="stepper-title">
                                Step 3
                            </h3>

                            <div class="stepper-desc">
                                Foto
                            </div>
                        </div>
                        <!--end::Label-->
                    </div>
                    <!--end::Step 3-->

                </div>
                <!--end::Nav-->
                <!--begin::Form-->
                <form class="mx-auto mw-1000px w-100 py-10" method="post" novalidate="novalidate"
                    id="kt_create_account_form" action="{{route('submit_identitas')}}" enctype="multipart/form-data">

                    <!--begin::Step 1-->
                    {{-- <form class="mx-auto mw-1000px w-100 py-10" method="post" novalidate="novalidate"
                    id="kt_create_account_form" action="{{route('submit_identitas')}}">
                    {{ csrf_field() }} --}}
                    <div class="current" data-kt-stepper-element="content">
                         <input type="hidden" class="g_token"  name="g_token"  id="g_token" />

                        {{-- {{ csrf_field() }} --}}
                        @csrf

                        <!--begin::Wrapper-->
                        <div class="w-100">
                            <div id="alert-bukutamu">
                                @if (session('alert-error-bukutamu'))
                                <div class="row justify-content-center">
                                    <div class="col-8 mb-10">
                                        <div class="alert alert-danger ">
                                            <strong>
                                                {{ session('alert-error-bukutamu') }}

                                                @if (session('errors-bukutamu'))
                                                <ul class="text-left mt-4 mb-0">
                                                    @foreach (session('errors-bukutamu') as $error)
                                                    <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <!--begin::Heading-->
                            <div class="pb-10 pb-lg-12">
                                <!--begin::Title-->
                                <h2 class="fw-bolder text-dark">Masukan No Kartu Identitas Anda</h2>
                                <!--end::Title-->

                            </div>
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Label-->
                                <label class="form-label required">Tipe No Identitas</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <select name="nik_type" id="nik_type"
                                    class="form-select form-select-lg form-select-solid" data-control="select2"
                                    data-placeholder="Pilih Tipe Identitas..." data-allow-clear="true"
                                    data-hide-search="true">
                                    @foreach ($identity_type as $kNT => $nik_type)
                                    <option {{ isset($old_input['nik_type']) && $old_input['nik_type']===$kNT
                                        ? 'selected' : '' }} value="{{ $kNT }}">{{
                                        $nik_type }}</option>
                                    @endforeach
                                </select>
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--end::Heading-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10" id="div_nik_ktp">
                                <!--begin::Label-->
                                <label class="form-label required">No KTP</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input name="nik_ktp" id="nik_ktp" name="nik_ktp" placeholder="Masukan no KTP Anda"
                                    value="{{ isset($old_input['nik_ktp']) ? $old_input['nik_ktp'] : '' }}"
                                    class="form-control form-control-lg form-control-solid" />
                                <!--end::Input-->
                            </div>
                            <div class="fv-row mb-10 d-none" id="div_nik_sim">
                                <!--begin::Label-->
                                <label class="form-label required">No SIM</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input name="nik_sim" id="nik_sim" placeholder="Masukan no SIM Anda"
                                    value="{{ isset($old_input['nik_sim']) ? $old_input['nik_sim'] : '' }}"
                                    class="form-control form-control-lg form-control-solid @error('nik_sim') is-invalid @enderror" />
                                <!--end::Input-->
                            </div>
                            <div class="fv-row mb-10 d-none" id="div_nik_passport">
                                <!--begin::Label-->
                                <label class="form-label required">No Passport</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input name="nik_passport" id="nik_passport" placeholder="Masukan no Passport Anda"
                                    value="{{ isset($old_input['nik_sim']) ? $old_input['nik_sim'] : '' }}"
                                    placeholder="Masukan no Passport Anda" class="form-control form-control-lg form-control-solid @error('nik_passport') is-invalid @enderror"/>
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->

                            <!--begin::Heading-->
                            <div class=" pb-10 pb-lg-15">
                                <!--begin::Title-->
                                <h2 class="fw-bolder text-dark">Masukan Data Pribadi Anda</h2>
                                <!--end::Title-->

                            </div>
                            <!--end::Heading-->
                            <div class="row mb-10">
                                <div class="col-md-6">
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                            <span class="required">Nama Anda</span>
                                        </label>
                                        <!--end::Label-->
                                        <input type="text" class="form-control form-control-solid" name="nama"
                                            placeholder="Masukan nama sesuai KTP Anda"
                                            value="{{ isset($old_input['nama']) ? $old_input['nama'] : '' }}"
                                            id="guest_nama" />

                                    </div>
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                            <span class="required">No HP / Whatsapp</span>
                                        </label>
                                        <!--end::Label-->
                                        <input type="text" class="form-control form-control-solid" name="telepon"
                                            placeholder="Masukan no HP atau whatsapp"
                                            value="{{ isset($old_input['telepon']) ? $old_input['telepon'] : '' }}"
                                            id="guest_telp" />

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                            <span class="">Email Anda</span>
                                        </label>
                                        <!--end::Label-->
                                        <input type="text" class="form-control form-control-solid" name="email"
                                            placeholder="Masukan email Anda"
                                            value="{{ isset($old_input['email']) ? $old_input['email'] : '' }}"
                                            id="guest_email" />
                                        <span class="form-text text-muted">Kosongkan apabila Anda tidak memiliki email.
                                        </span>
                                    </div>
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                            <span class="">Plat Kendaraan</span>
                                        </label>
                                        <!--end::Label-->
                                        <input type="text" class="form-control form-control-solid" name="plat_kendaraan"
                                            placeholder="Masukan no plat kendaraan Anda"
                                            value="{{ isset($old_input['plat_kendaraan']) ? $old_input['plat_kendaraan'] : '' }}"
                                            id="guest_plat_kendaraan" />
                                        <span class="form-text text-muted">Kosongkan apabila Anda tidak memiliki
                                            kendaraan.</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="fv-row mb-10">
                                        <label class="form-label required">Alamat Anda</label>
                                        <textarea type="text" class="form-control form-control-solid" name="alamat"
                                            placeholder="Masukan alamat sesuai KTP Anda" id="guest_alamat"
                                            style="min-height: 85px;">{{ isset($old_input['alamat']) ? $old_input['alamat'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="fv-row mb-10">
                                        <label class="form-label required">Tujuan</label>
                                        <textarea type="text" class="form-control form-control-solid" name="tujuan"
                                            placeholder="Masukan tujuan Anda" id="guest_tujuan"
                                            style="min-height: 85px;">{{ isset($old_input['tujuan']) ? $old_input['tujuan'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-3 fv-row">
                                    <label class="form-label required">Tanggal kedatangan</label>
                                    <input type="text" class="form-control form-control-solid form-control-tanggal" name="tanggal_kedatangan"
                                        id="tanggal_kedatangan" value="{{ old('dari_tgl') ?: date('Y-m-d') }}" required>
                                </div>
                                <div class="col-md-3 fv-row">
                                    <label class="form-label required">Waktu kedatangan</label>
                                    <input class="form-control timepicker-kedatangan form-control-solid" readonly required="required" name="waktu_kedatangan"
                                        type="text" id="waktu_kedatangan" value="{{ old('waktu_kedatangan') ?: date('H:i') }}">
                                </div>
                                <div class="col-md-12 mb-10">
                                    <div class="row">
                                        <div class="col-md-6 fv-row">
                                            <!--begin::Label-->
                                            <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                                <span class="required">Tempat Lahir</span>
                                            </label>
                                            <!--end::Label-->
                                            <input type="text" class="form-control form-control-solid" name="tempat_lahir"
                                                placeholder="Masukan tempat lahir sesuai KTP Anda"
                                                value="{{ isset($old_input['tempat_lahir']) ? $old_input['tempat_lahir'] : '' }}"
                                                id="tempat_lahir" />
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label class="form-label required">Tanggal Lahir</label>
                                            <input class="form-control form-control-tanggal form-control-solid" readonly required="required" name="tanggal_lahir"
                                                type="text" id="tanggal_lahir" value="{{ old('tanggal_lahir') ?: date('Y-m-d') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-10">
                                    <div class="row">
                                        <div class="col-md-6 fv-row">
                                            <label class="form-label required">Jenis Kelamin</label>
                                            <select name="jenis_kelamin" id="jenis_kelamin"
                                                class="form-control form-select form-select-lg form-select-solid" data-control="select2"
                                                data-placeholder="Pilih Jenis Kelamin..."
                                                data-hide-search="true">
                                                @foreach ($jenis_kelamin_data as $jenis_kelamin)
                                                <option {{ isset($old_input['jenis_kelamin']) && $old_input['jenis_kelamin']===$jenis_kelamin->key
                                                    ? 'selected' : '' }} value="{{ $jenis_kelamin->key }}">{{
                                                    $jenis_kelamin->text }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label class="form-label required">Bangsa / Suku</label>
                                            <select name="suku" id="suku"
                                                class="form-control form-select form-select-lg form-select-solid" data-control="select2"
                                                data-placeholder="Pilih Bangsa / Suku..."  data-allow-clear="true"
                                                data-hide-search="true">
                                                <option value=""></option>
                                                @foreach ($suku_data as $suku)
                                                <option {{ isset($old_input['suku']) && $old_input['suku']===$suku->key
                                                    ? 'selected' : '' }} value="{{ $suku->key }}">{{
                                                    $suku->text }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-10">
                                    <div class="row">
                                        <div class="col-md-6 fv-row">
                                            <label class="form-label required">Kewarganegaraan</label>
                                            <select name="kewarganegaraan" id="kewarganegaraan"
                                                class="form-control form-select form-select-lg form-select-solid" data-control="select2"
                                                data-placeholder="Pilih Kewarganegaraan..."
                                                data-hide-search="true">
                                                @foreach ($kewarganegaraan_data as $kewarganegaraan)
                                                <option {{ isset($old_input['kewarganegaraan']) && $old_input['kewarganegaraan']===$kewarganegaraan->key
                                                    ? 'selected' : '' }} value="{{ $kewarganegaraan->key }}">{{
                                                    $kewarganegaraan->text }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label class="form-label required">Agama / Kepercayaan</label>
                                            <select name="agama" id="agama"
                                                class="form-control form-select form-select-lg form-select-solid" data-control="select2"
                                                data-placeholder="Pilih Agama / Kepercayaan..."
                                                data-hide-search="true">
                                                @foreach ($agama_data as $agama)
                                                <option {{ isset($old_input['agama']) && $old_input['agama']===$agama->key
                                                    ? 'selected' : '' }} value="{{ $agama->key }}">{{
                                                    $agama->text }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-10">
                                    <div class="row">
                                        <div class="col-md-6 fv-row">
                                            <label class="form-label required">Pendidikan</label>
                                            <select name="pendidikan" id="pendidikan"
                                                class="form-control form-select form-select-lg form-select-solid" data-control="select2"
                                                data-placeholder="Pilih Pendidikan..." data-allow-clear="true" onchange="onChangePendidikan()"
                                                data-hide-search="true">
                                                @foreach ($pendidikan_data as $pendidikan)
                                                <option {{ isset($old_input['pendidikan']) && $old_input['pendidikan']===$pendidikan->key
                                                    ? 'selected' : '' }} value="{{ $pendidikan->key }}">{{
                                                    $pendidikan->text }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 fv-row">
                                            <label class="form-label required">Pekerjaan</label>
                                            <input type="text" class="form-control form-control-solid" name="pekerjaan"
                                                placeholder="Masukan Pekerjaan Anda"
                                                value="{{ isset($old_input['pekerjaan']) ? $old_input['pekerjaan'] : '' }}"
                                                id="pekerjaan" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-10">
                                    <div class="row">
                                        <div class="col-md-6 fv-row">
                                            <label class="form-label required">Status Perkawinan</label>
                                            <select name="status_perkawinan" id="status_perkawinan"
                                                class="form-control form-select form-select-lg form-select-solid" data-control="select2"
                                                data-placeholder="Pilih Status Perkawinan..." onchange="onChangeStatusPerkawinan()"
                                                data-hide-search="true">
                                                @foreach ($status_perkawinan_data as $status_perkawinan)
                                                <option {{ isset($old_input['status_perkawinan']) && $old_input['status_perkawinan']===$status_perkawinan->key
                                                    ? 'selected' : '' }} value="{{ $status_perkawinan->key }}">{{
                                                    $status_perkawinan->text }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-10">
                                    <div class="row">
                                        <div class="col-md-12 fv-row">
                                            <label class="form-label required">Alamat Kantor</label>
                                            <textarea type="text" class="form-control form-control-solid" name="alamat_kantor"
                                                placeholder="Masukan alamat kantor" id="alamat_kantor"
                                                style="min-height: 85px;">{{ isset($old_input['alamat_kantor']) ? $old_input['alamat_kantor'] : '' }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: none" class="pb-10 pb-lg-15" id="tambahTamuTitle">
                                <!--begin::Title-->
                                <h2 class="fw-bolder text-dark">Tambah Tamu</h2>
                                <!--end::Title-->
                            </div>
                            <button class="btn btn-lg btn-primary" onclick="tambahTamu(event)" id="tambahTamuButton">
                                Tambah Tamu
                            </button>
                            <div id="tambahTamuContainer">
                            </div>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Step 1-->

                    <!--begin::Step 2-->
                    {{-- <form class="mx-auto mw-1000px w-100 py-10" method="post" novalidate="novalidate"
                    id="kt_create_account_form" action="{{route('submit_identitas')}}">
                    {{ csrf_field() }} --}}
                    <div data-kt-stepper-element="content">
                        <input type="hidden" class="g_token"  name="g_token"  id="g_token" />

                       {{-- {{ csrf_field() }} --}}
                       @csrf

                       <!--begin::Wrapper-->
                       <div class="w-100">
                           <div id="alert-bukutamu">
                               @if (session('alert-error-bukutamu'))
                               <div class="row justify-content-center">
                                   <div class="col-8 mb-10">
                                       <div class="alert alert-danger ">
                                           <strong>
                                               {{ session('alert-error-bukutamu') }}

                                               @if (session('errors-bukutamu'))
                                               <ul class="text-left mt-4 mb-0">
                                                   @foreach (session('errors-bukutamu') as $error)
                                                   <li>{{ $error }}</li>
                                                   @endforeach
                                               </ul>
                                               @endif
                                           </strong>
                                       </div>
                                   </div>
                               </div>
                               @endif
                           </div>
                           <!--begin::Heading-->
                           <div class="pb-10 pb-lg-12">
                               <!--begin::Title-->
                               <h2 class="fw-bolder text-dark">Riwayat Hidup Singkat</h2>
                               <!--end::Title-->
                           </div>

                            <div class="row mb-10">
                                <div class="col-md-6">
                                    <label class="form-label">Pendidikan</label>
                                    <input type="text" class="form-control form-control-solid" name="pendidikan_clone"
                                        placeholder="Pilih Pendidikan"
                                        value="{{ isset($old_input['pendidikan']) ? $old_input['pendidikan'] : '' }}"
                                        id="pendidikan_clone" readonly/>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-label">Pekerjaan</label>
                                    <input type="text" class="form-control form-control-solid" name="pekerjaan_clone"
                                        placeholder="Masukan Pekerjaan Anda"
                                        value="{{ isset($old_input['pekerjaan']) ? $old_input['pekerjaan'] : '' }}"
                                        id="pekerjaan_clone" readonly/>
                                </div>
                            </div>

                            <div class="row mb-10">
                                <div class="col-md-6 fv-row">
                                    <label class="form-label">Kepartaian</label>
                                    <input type="text" class="form-control form-control-solid" name="kepartaian"
                                        placeholder="Masukan Kepartaian Anda"
                                        value="{{ isset($old_input['kepartaian']) ? $old_input['kepartaian'] : '' }}"
                                        id="kepartaian" />
                                </div>
                                <div class="col-md-6 fv-row">
                                    <label class="form-label">Ormas Lainnya</label>
                                    <input type="text" class="form-control form-control-solid" name="ormas_lainnya"
                                        placeholder="Masukan Ormas Lain Anda"
                                        value="{{ isset($old_input['ormas_lainnya']) ? $old_input['ormas_lainnya'] : '' }}"
                                        id="ormas_lainnya" />
                                </div>
                            </div>

                            <div class="pb-10 pb-lg-12">
                                <!--begin::Title-->
                                <h2 class="fw-bolder text-dark">Keluarga</h2>
                                <!--end::Title-->
                            </div>

                            <div class="row mb-10">
                                <div class="col-md-12 fv-row">
                                    <label class="form-label">Nama Istri / Suami</label>
                                    <input type="text" class="form-control form-control-solid" name="nama_pasangan"
                                        placeholder="Masukan Nama Istri / Suami Anda"
                                        value="{{ isset($old_input['nama_pasangan']) ? $old_input['nama_pasangan'] : '' }}"
                                        id="nama_pasangan" />
                                </div>
                            </div>

                            <div class="row mb-10" id="form-anak">
                                <div class="col-md-12 fv-row">
                                    <label class="form-label">Nama Anak</label>
                                    <input type="text" class="form-control form-control-solid" name="nama_anak_anak"
                                        placeholder="Masukan Nama Anak 1, Nama Anak 2"
                                        value="{{ isset($old_input['nama_anak_anak']) ? $old_input['nama_anak_anak'] : '' }}"
                                        id="nama_pasangan" />
                                </div>
                            </div>

                            <div class="row mb-10">
                                <div class="col-md-6 fv-row">
                                    <label class="form-label required">Nama Ayah Kandung</label>
                                    <input type="text" class="form-control form-control-solid" name="nama_ayah_kandung"
                                        placeholder="Masukan Nama Ayah Kandung Anda"
                                        value="{{ isset($old_input['nama_ayah_kandung']) ? $old_input['nama_ayah_kandung'] : '' }}"
                                        id="nama_ayah_kandung" />
                                </div>
                                <div class="col-md-6 fv-row">
                                    <label class="form-label required">Alamat Ayah Kandung</label>
                                    <textarea type="text" class="form-control form-control-solid" name="alamat_ayah_kandung"
                                        placeholder="Masukan Alamat Ayah Kandung Anda" id="alamat_ayah_kandung"
                                        style="min-height: 85px;">{{ isset($old_input['alamat_ayah_kandung']) ? $old_input['alamat_ayah_kandung'] : '' }}</textarea>
                                </div>
                            </div>

                            <div class="row mb-10">
                                <div class="col-md-6 fv-row">
                                    <label class="form-label required">Nama Ibu Kandung</label>
                                    <input type="text" class="form-control form-control-solid" name="nama_ibu_kandung"
                                        placeholder="Masukan Nama Ibu Kandung Anda"
                                        value="{{ isset($old_input['nama_ibu_kandung']) ? $old_input['nama_ibu_kandung'] : '' }}"
                                        id="nama_ibu_kandung" />
                                </div>
                                <div class="col-md-6 fv-row">
                                    <label class="form-label required">Alamat Ibu Kandung</label>
                                    <textarea type="text" class="form-control form-control-solid" name="alamat_ibu_kandung"
                                        placeholder="Masukan Alamat Ibu Kandung Anda" id="alamat_ibu_kandung"
                                        style="min-height: 85px;">{{ isset($old_input['alamat_ibu_kandung']) ? $old_input['alamat_ibu_kandung'] : '' }}</textarea>
                                </div>
                            </div>

                            <div id="form-keluarga-mertua" style="display: none;">
                                <div class="row mb-10">
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label required">Nama Ayah Mertua</label>
                                        <input type="text" class="form-control form-control-solid" name="nama_ayah_mertua"
                                            placeholder="Masukan Nama Ayah Mertua Anda"
                                            value="{{ isset($old_input['nama_ayah_mertua']) ? $old_input['nama_ayah_mertua'] : '' }}"
                                            id="nama_ayah_mertua" />
                                    </div>
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label required">Alamat Ayah Mertua</label>
                                        <textarea type="text" class="form-control form-control-solid" name="alamat_ayah_mertua"
                                            placeholder="Masukan Alamat Ayah Mertua Anda" id="alamat_ayah_mertua"
                                            style="min-height: 85px;">{{ isset($old_input['alamat_ayah_mertua']) ? $old_input['alamat_ayah_mertua'] : '' }}</textarea>
                                    </div>
                                </div>

                                <div class="row mb-10">
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label required">Nama Ibu Mertua</label>
                                        <input type="text" class="form-control form-control-solid" name="nama_ibu_mertua"
                                            placeholder="Masukan Nama Ibu Mertua Anda"
                                            value="{{ isset($old_input['nama_ibu_mertua']) ? $old_input['nama_ibu_mertua'] : '' }}"
                                            id="nama_ibu_mertua" />
                                    </div>
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label required">Alamat Ibu Mertua</label>
                                        <textarea type="text" class="form-control form-control-solid" name="alamat_ibu_mertua"
                                            placeholder="Masukan Alamat Ibu Mertua Anda" id="alamat_ibu_mertua"
                                            style="min-height: 85px;">{{ isset($old_input['alamat_ibu_mertua']) ? $old_input['alamat_ibu_mertua'] : '' }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <!--begin::Heading-->
                            <div class="pb-10 pb-lg-12">
                                <!--begin::Title-->
                                <h2 class="fw-bolder text-dark">Referensi / Kenalan</h2>
                                <!--end::Title-->
                            </div>
                            <div id="form-kenalan">
                                <div class="row mb-10">
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label">Nama</label>
                                        <input type="text" class="form-control form-control-solid" name="nama_kenalan1"
                                            placeholder="Masukan Nama Kenalan Anda"
                                            value="{{ isset($old_input['nama_kenalan1']) ? $old_input['nama_kenalan1'] : '' }}"
                                            id="nama_kenalan1" />
                                    </div>
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label">Alamat</label>
                                        <textarea type="text" class="form-control form-control-solid" name="alamat_kenalan1"
                                            placeholder="Masukan Alamat Kenalan Anda" id="alamat_kenalan1"
                                            style="min-height: 85px;">{{ isset($old_input['alamat_kenalan1']) ? $old_input['alamat_kenalan1'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-10">
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label">Nama</label>
                                        <input type="text" class="form-control form-control-solid" name="nama_kenalan2"
                                            placeholder="Masukan Nama Kenalan Anda"
                                            value="{{ isset($old_input['nama_kenalan2']) ? $old_input['nama_kenalan2'] : '' }}"
                                            id="nama_kenalan2" />
                                    </div>
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label">Alamat</label>
                                        <textarea type="text" class="form-control form-control-solid" name="alamat_kenalan2"
                                            placeholder="Masukan Alamat Kenalan Anda" id="alamat_kenalan2"
                                            style="min-height: 85px;">{{ isset($old_input['alamat_kenalan2']) ? $old_input['alamat_kenalan2'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-10">
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label">Nama</label>
                                        <input type="text" class="form-control form-control-solid" name="nama_kenalan3"
                                            placeholder="Masukan Nama Kenalan Anda"
                                            value="{{ isset($old_input['nama_kenalan3']) ? $old_input['nama_kenalan3'] : '' }}"
                                            id="nama_kenalan3" />
                                    </div>
                                    <div class="col-md-6 fv-row">
                                        <label class="form-label">Alamat</label>
                                        <textarea type="text" class="form-control form-control-solid" name="alamat_kenalan3"
                                            placeholder="Masukan Alamat Kenalan Anda" id="alamat_kenalan3"
                                            style="min-height: 85px;">{{ isset($old_input['alamat_kenalan3']) ? $old_input['alamat_kenalan3'] : '' }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-10">
                                <div class="col-md-6 fv-row">
                                    <label class="form-label required">Hobi / Kegemaran</label>
                                    <input type="text" class="form-control form-control-solid" name="hobi"
                                        placeholder="Masukan Hobi / Kegemaran Anda"
                                        value="{{ isset($old_input['hobi']) ? $old_input['hobi'] : '' }}"
                                        id="hobi" />
                                </div>
                                <div class="col-md-6 fv-row">
                                    <label class="form-label required">Kedudukan di masyarakat</label>
                                    <input type="text" class="form-control form-control-solid" name="kedudukan_di_masyarakat"
                                        placeholder="Masukan Kedudukan di masyarakat Anda"
                                        value="{{ isset($old_input['kedudukan_di_masyarakat']) ? $old_input['kedudukan_di_masyarakat'] : '' }}"
                                        id="kedudukan_di_masyarakat" />
                                </div>
                            </div>

                       </div>
                       <!--end::Wrapper-->
                   </div>
                   <!--end::Step 2-->

                    <!--begin::Step 3-->
                    <div   data-kt-stepper-element="content">
                        <!--begin::Wrapper-->
                        <div class="w-100">
                            <div>
                                <h4 class="mb-10 font-weight-bold text-dark">Upload atau ambil foto KTP Anda</h4>
                                <h6 style="color: crimson; display: none;" id="notifIsiKtp">Foto KTP tidak boleh kosong</h4>

                                <div class="col-md-4" id="photo-ktp-wrapper">
                                    <div class="form-group">
                                        <label for="photo-ktp" class="form-label required">Foto KTP</label>
                                        <input class="form-control" type="file" id="photo-ktp"
                                        name="photo-ktp"
                                        accept=".png, .jpg, .jpeg, .pdf, .xls, .xlsx, .doc, .docx" required>
                                    </div>
                                </div>
                                <br><br>
                                <div id="bx-cam-ktp">
                                    <video id="webcamktp" autoplay playsinline></video>
                                    <canvas id="canvasktp" class="d-none"></canvas>
                                    <div id="flash-cam-ktp" class="flash-cam"></div>
                                    <input type="hidden" name="photo-ktp-64" id="download-photo-ktp" />
                                </div>

                                <div id="bx-cam-ktp-icon">
                                    <div class="row-icon">
                                        <a id="take-photo-ktp" class="cam-icon">
                                            <i class="fas fa-camera"></i>
                                        </a>
                                        <a id="retake-photo-ktp" class="d-none cam-icon">
                                            <i class="fas fa-portrait"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div>
                                <h4 class="mb-10 font-weight-bold text-dark">Upload atau ambil foto diri Anda</h4>

                                <div class="col-md-4" id="photo-diri-wrapper">
                                    <div class="form-group">
                                        <label for="photo-diri" class="form-label">Foto Diri</label>
                                        <input class="form-control" type="file" id="photo-diri"
                                        name="photo-diri"
                                        accept=".png, .jpg, .jpeg, .pdf, .xls, .xlsx, .doc, .docx" required>
                                    </div>
                                </div>
                                <br><br>
                                <div id="bx-cam">
                                    <video id="webcam" autoplay playsinline></video>
                                    <canvas id="canvas" class="d-none"></canvas>
                                    <div id="flash-cam" class="flash-cam"></div>
                                    <input type="hidden" name="image" id="download-photo" />
                                </div>

                                <div id="bx-cam-icon">
                                    <div class="row-icon">
                                        <a id="take-photo" class="cam-icon">
                                            <i class="fas fa-camera"></i>
                                        </a>
                                        <a id="retake-photo" class="d-none cam-icon">
                                            <i class="fas fa-portrait"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-10 pt-5" style="border-top:1px solid #EBEDF3;">
                                <h4 class="mb-10 mt-20 font-weight-bold text-dark">Upload atau ambil foto kendaraan Anda</h4>

                                <div class="col-md-4" id="photo-kendaraan-wrapper">
                                    <div class="form-group">
                                        <label for="photo-kendaraan" class="form-label">Foto kendaraan</label>
                                        <input class="form-control" type="file" id="photo-kendaraan"
                                        name="photo-kendaraan"
                                        accept=".png, .jpg, .jpeg, .pdf, .xls, .xlsx, .doc, .docx" required>
                                    </div>
                                </div>
                                <br><br>
                                <div id="bx-cam-kendaraan">
                                    <video id="webcamkendaraan" autoplay playsinline></video>
                                    <canvas id="canvaskendaraan" class="d-none"></canvas>
                                    <div id="flash-cam-kendaraan" class="flash-cam"></div>
                                    <input type="hidden" name="image_plat_kendaraan" id="download-photo-kendaraan" />
                                </div>

                                <div id="bx-cam-kendaraan-icon">
                                    <div class="row-icon">
                                        <a id="take-photo-kendaraan" class="cam-icon">
                                            <i class="fas fa-camera"></i>
                                        </a>
                                        <a id="retake-photo-kendaraan" class="d-none cam-icon">
                                            <i class="fas fa-portrait"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Step 3-->
                    <!--begin::Actions-->
                    <div class="d-flex flex-stack pt-15">
                        <!--begin::Wrapper-->
                        <div class="mr-2">
                            <button type="button" class="btn btn-lg btn-light-primary me-3"
                                data-kt-stepper-action="previous">
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
                                <span class="svg-icon svg-icon-4 me-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
                                        <path
                                            d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z"
                                            fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->Back
                            </button>
                        </div>
                        <!--end::Wrapper-->
                        <!--begin::Wrapper-->
                        <div>
                            <input type="hidden" name="kode_satker" value="02" id="kode_satker"/>
                            <input type="hidden" name="status_tamu" value={{ $status_tamu }} id="status_tamu"/>
                            <button type="submit" class="btn btn-lg btn-primary me-3" data-kt-stepper-action="submit" id="realSubmit">
                                <span class="indicator-label">Submit
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                    <span class="svg-icon svg-icon-3 ms-2 me-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1"
                                                transform="rotate(-180 18 13)" fill="black" />
                                            <path
                                                d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                            <button class="btn btn-lg btn-primary me-3 d-none" id="submitButton">
                                <span class="indicator-label">Submit
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                    <span class="svg-icon svg-icon-3 ms-2 me-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1"
                                                transform="rotate(-180 18 13)" fill="black" />
                                            <path
                                                d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                            <button type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="next">Continue
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1"
                                            transform="rotate(-180 18 13)" fill="black" />
                                        <path
                                            d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                            fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Actions-->
                    <input type="hidden" name="id_layanan" value={{ $id_layanan }}>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Stepper-->
        </div>
    </div>
    <input type="hidden" name="idLayanan" value="{{ request()->query('idLayanan') }}">

    @section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script src="{{url("")}}/demo1/js/custom/identitas-wizard.js"></script>
    <script src="{{url("")}}/demo1/js/application/webcam-easy-1.0.5.min.js"></script>
    <script>
        // $('.select2').select2();
        // $('.select2').addClass('mb-5').addClass('mt-3');
        $('.form-control-tanggal').datepicker({
            dateFormat: "yyyy-mm-dd",
            autoclose: true,
            enableOnReadonly: true
        }).datepicker("setDate", new Date());

        const _token = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': _token
            }
        });

        $(document).ready(function() {
            $('#submitButton').click(function (e) {
                e.preventDefault()

                let photoKtpNotNull = $('#photo-ktp').val() || $('#download-photo-ktp').val()
                console.log('photoktp' + $('#photo-ktp').val())
                console.log('download-photo-ktp' + $('#download-photo-ktp').val())
                if(photoKtpNotNull) {
                    $('#realSubmit').click()
                } else {
                    $('#notifIsiKtp').show()
                    Swal.fire({
                            text: "Mohon maaf, mohon lengkapi data sebelum melanjutkan.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, baik!",
                            customClass: {
                                confirmButton: "btn btn-light",
                            },
                        }).then(function () {
                            KTUtil.scrollTop();
                        });
                }
            })

            $("#pendidikan_clone").val($("#pendidikan").val().toUpperCase());
            onChangeStatusPerkawinan();
            $('.select2-satker').select2({
                placeholder: "",
                ajax: {
                    url: "{{ route('layanan.get-satker') }}",
                    dataType: 'json',
                    delay: 250,
                    quietMillis: 500,
                    maximumSelectionLength: 30,
                    type: "POST",
                    multiple: false,
                    data: function(params) {
                        return {
                            keyword: params.term || "",
                            page: params.page || 1
                        }
                    },
                    processResults: function(data) {
                        return data;
                    },
                    cache: true
                }
            });

            $(".timepicker-kedatangan").timepicker({
                showMeridian: false,
                minuteStep: 1,
                icons: {
                    up: 'fas fa-chevron-up',
                    down: 'fas fa-chevron-down'
                }
            });

            // $('.select2-satker').trigger('changeSatker');

            // const changeSatker = () => {
            //     console.log('abc')
            // }


            //console.log(selectedSatker)
        })

        function tambahTamu(event) {
            event.preventDefault();
             $('#tambahTamuContainer').show();
             $('#tambahTamuTitle').show();
             $('#tambahTamuButton').remove();
             let jmlTamu = $('.nama-tamu-tambahan').length - 1
            let additionalForm = '<div class="row mb-10">' +
                                    '<div class="col-md-6">' +
                                        '<div class="fv-row mb-10">' +
                                            '<label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">' +
                                                '<span class="required">Nama Tamu</span>' +
                                            '</label>' +
                                            '<input type="text" class="form-control form-control-solid nama-tamu-tambahan" name="tamu_tambahan[][nama]"' +
                                                'placeholder="Masukan nama sesuai KTP Tamu"' +
                                                'value="{{ isset($old_input['nama_tamu_tambahan']) ? $old_input['nama_tamu_tambahan'] : '' }}"' +
                                                'id="nama_tamu_tambahan' + jmlTamu + '" required/>' +
                                        '</div>' +
                                        '<div class="fv-row mb-10">' +
                                            '<label class="form-label required">No KTP</label>' +
                                            '<input name="tamu_tambahan[][ktp]" placeholder="Masukan no KTP Tamu"' +
                                                'value="{{ isset($old_input['ktp_tamu_tambahan']) ? $old_input['ktp_tamu_tambahan'] : '' }}"' +
                                                'class="form-control form-control-lg form-control-solid ktp-tamu-tambahan"' +
                                                'id="ktp_tamu_tambahan' + jmlTamu + '" required/>' +
                                            '<button style="margin-top: 20px" class="btn btn-lg btn-primary" onclick="tambahTamu(event)" id="tambahTamuButton">' +
                                                'Tambah Tamu' +
                                            '</button>' +
                                        '</div>' +
                                   '</div>' +
                                    '<div class="col-md-6">' +
                                        '<div class="fv-row mb-10">' +
                                            '<label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">' +
                                                '<span class="required">No HP / Whatsapp</span>' +
                                            '</label>' +
                                            '<input type="text" class="form-control form-control-solid telepon-tamu-tambahan" name="tamu_tambahan[][telp]"' +
                                                'placeholder="Masukan no HP atau whatsapp"' +
                                                'value="{{ isset($old_input['telepon_tamu_tambahan']) ? $old_input['telepon_tamu_tambahan'] : '' }}"' +
                                                'id="telp_tamu_tambahan' + jmlTamu + '" required/>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>'
            $('#tambahTamuContainer').append(additionalForm);
            console.log($('.nama-tamu-tambahan').length)
            _validations_field["tamu_tambahan[][nama]"] =  {
                validators: {
                    notEmpty: {
                        message: "Nama tamu wajib diisi",
                    },
                    regexp: {
                    regexp: /^[a-zA-Z\s]*$/,
                    message:
                        "Nama hanya dapat diisi dengan alfabet dan spasi",
                    },
                }
            };
            _validations_field["tamu_tambahan[][ktp]"] =  {
                validators: {
                    notEmpty: {
                        message: "No KTP wajib diisi",
                    },
                    digits: {
                        message:
                            "No KTP hanya dapat diisi dengan angka",
                    },
                    stringLength: {
                        min: 16,
                        max: 16,
                        message: "No KTP harus terdiri dari 16 digit",
                    },
                }
            };
            _validations_field["tamu_tambahan[][telp]"] =  {
                validators: {
                    notEmpty: {
                        message: "No HP / Whatsapp wajib diisi",
                    },
                    regexp: {
                    regexp: /^(^\+62\s?|^0)(\d{2,4}-?){2}\d{3,5}$/,
                    message:
                        "Format no HP / whatsapp salah. Contoh no HP / whatsapp +628131234567 atau 08131234567",
                    }
                }
            };
            initValidations();
        }

        $('#photo-ktp').change(function (e){
            $('#bx-cam-ktp').hide();
            $('#bx-cam-ktp-icon').hide();
        })

        $('#photo-diri').change(function (e){
            $('#bx-cam').hide();
            $('#bx-cam-icon').hide();
        })

        $('#photo-kendaraan').change(function (e){
            $('#bx-cam-kendaraan').hide();
            $('#bx-cam-kendaraan-icon').hide();
        })

        // $('.select2-satker').on('select2:selected', function (e) {
        //         let selectedSatker = $('#kode_satker').val( $('.select2-satker').val() )
        //     );
        // })
    </script>
    <script>
        // ktp
                    const webcamElementKtp = document.getElementById('webcamktp');
                    const canvasElementKtp = document.getElementById('canvasktp');
                    const snapSoundElementKtp = document.getElementById('snapSound');
                    const webcamKtp = new Webcam(webcamElementKtp, 'user', canvasElementKtp, snapSoundElementKtp);

                    webcamKtp.start()
                        .then(result =>{
                            console.log("webcam ktp started");
                        })
                        .catch(err => {
                            console.log(err);
                        }
                    );

                    $("#take-photo-ktp").click(function () {
                        beforeTakePhotoKtp();
                        let pictureKtp = webcamKtp.snap();
                        $("#download-photo-ktp").val(pictureKtp);
                        // document.querySelector('#download-photo').href = pictureKtp;
                        afterTakePhotoKtp();
                    });

                    $("#retake-photo-ktp").click(function () {
                        webcamKtp.stream()
                            .then(facingMode =>{
                                removeCaptureKtp();
                            }
                        );
                    });

                    function beforeTakePhotoKtp(){
                        $('#flash-cam-ktp')
                            .show()
                            .animate({opacity: 0.3}, 500)
                            .fadeOut(500)
                            .css({'opacity': 0.7});
                        // window.scrollTo(0, 0);
                        //  $('#webcam-control').addClass('d-none');
                        //  $('#cameraControls').addClass('d-none');
                    };

                    function afterTakePhotoKtp(){
                        let a = webcamKtp.stop();
                        $('#photo-ktp-wrapper').hide();
                        $('#canvasktp').removeClass('d-none');
                        $('#take-photo-ktp').addClass('d-none');
                        $('#retake-photo-ktp').removeClass('d-none');
                        // $('#download-photo').removeClass('d-none');
                        // $('#resume-camera').removeClass('d-none');
                        // $('#cameraControls').removeClass('d-none');
                    }

                    function removeCaptureKtp(){
                        $('#canvasktp').addClass('d-none');
                        $('#take-photo-ktp').removeClass('d-none');
                        $('#retake-photo-ktp').addClass('d-none');
                    }
    </script>
    <script>
        // selfie
                    const webcamElement = document.getElementById('webcam');
                    const canvasElement = document.getElementById('canvas');
                    const snapSoundElement = document.getElementById('snapSound');
                    const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);

                    webcam.start()
                        .then(result =>{
                            console.log("webcam started");
                        })
                        .catch(err => {
                            console.log(err);
                        }
                    );

                    $("#take-photo").click(function () {
                        beforeTakePhoto();
                        let picture = webcam.snap();
                        $("#download-photo").val(picture);
                        // document.querySelector('#download-photo').href = picture;
                        afterTakePhoto();
                    });

                    $("#retake-photo").click(function () {
                        webcam.stream()
                            .then(facingMode =>{
                                removeCapture();
                            }
                        );
                    });

                    function beforeTakePhoto(){
                        $('#flash-cam')
                            .show()
                            .animate({opacity: 0.3}, 500)
                            .fadeOut(500)
                            .css({'opacity': 0.7});
                        // window.scrollTo(0, 0);
                        // $('#webcam-control').addClass('d-none');
                        // $('#cameraControls').addClass('d-none');
                    };

                    function afterTakePhoto(){
                        webcam.stop();
                        $('#photo-diri-wrapper').hide();
                        $('#canvas').removeClass('d-none');
                        $('#take-photo').addClass('d-none');
                        $('#retake-photo').removeClass('d-none');
                        // $('#download-photo').removeClass('d-none');
                        // $('#resume-camera').removeClass('d-none');
                        // $('#cameraControls').removeClass('d-none');
                    }

                    function removeCapture(){
                        $('#canvas').addClass('d-none');
                        $('#take-photo').removeClass('d-none');
                        $('#retake-photo').addClass('d-none');
                    }
    </script>

    <script>
        // kendaraan
                    const webcamElementKendaraan = document.getElementById('webcamkendaraan');
                    const canvasElementKendaraan = document.getElementById('canvaskendaraan');
                    const snapSoundElementKendaraan = document.getElementById('snapSound');
                    const webcamKendaraan = new Webcam(webcamElementKendaraan, 'user', canvasElementKendaraan, snapSoundElementKendaraan);

                    webcamKendaraan.start()
                        .then(result =>{
                            console.log("webcam kendaraan started");
                        })
                        .catch(err => {
                            console.log(err);
                        }
                    );

                    $("#take-photo-kendaraan").click(function () {
                        beforeTakePhotoKendaraan();
                        let pictureKendaraan = webcamKendaraan.snap();
                        $("#download-photo-kendaraan").val(pictureKendaraan);
                        // document.querySelector('#download-photo').href = pictureKendaraan;
                        afterTakePhotoKendaraan();
                    });

                    $("#retake-photo-kendaraan").click(function () {
                        webcamKendaraan.stream()
                            .then(facingMode =>{
                                removeCaptureKendaraan();
                            }
                        );
                    });

                    function beforeTakePhotoKendaraan(){
                        $('#flash-cam-kendaraan')
                            .show()
                            .animate({opacity: 0.3}, 500)
                            .fadeOut(500)
                            .css({'opacity': 0.7});
                        // window.scrollTo(0, 0);
                        //  $('#webcam-control').addClass('d-none');
                        //  $('#cameraControls').addClass('d-none');
                    };

                    function afterTakePhotoKendaraan(){
                        webcamKendaraan.stop();
                        $('#photo-kendaraan-wrapper').hide();
                        $('#canvaskendaraan').removeClass('d-none');
                        $('#take-photo-kendaraan').addClass('d-none');
                        $('#retake-photo-kendaraan').removeClass('d-none');
                        // $('#download-photo').removeClass('d-none');
                        // $('#resume-camera').removeClass('d-none');
                        // $('#cameraControls').removeClass('d-none');
                    }

                    function removeCaptureKendaraan(){
                        $('#canvaskendaraan').addClass('d-none');
                        $('#take-photo-kendaraan').removeClass('d-none');
                        $('#retake-photo-kendaraan').addClass('d-none');
                    }
    </script>
    <script>
        $("#pekerjaan").on("change", function() {
            $("#pekerjaan_clone").val($(this).val());
        })
        function onChangePendidikan() {
            $("#pendidikan_clone").val($("#pendidikan").val().toUpperCase());
        }
        function onChangeStatusPerkawinan() {
            if($("#status_perkawinan").val() == "kawin") {
                $("#form-keluarga-mertua").show();
            } else {
                $("#form-keluarga-mertua").hide();
            }
        }
    </script>
    @endsection
</x-base-layout>
