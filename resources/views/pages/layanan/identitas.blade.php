@section('styles')
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
{{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> --}}

<link rel="stylesheet" href="{{url('')}}/demo1/css/application/webcam-style.css" />
<link href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<style>
    .grecaptcha-badge {
        bottom: 100px !important;
    }

    @media (min-width: 1400px) {

        .container-xxl,
        .container-xl,
        .container-lg,
        .container-md,
        .container-sm,
        .container {
            max-width: 1340px !important;
        }
    }

    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@if (!auth()->user())
<style>
    .aside-enabled.aside-fixed .wrapper {
        padding-left: 0px;
    }
    body {
        background: url("{{ asset('assets/bg-newform.png') }}") repeat center center fixed, linear-gradient(0deg, rgb(2 12 46) 27%, #153094 100%) no-repeat center center fixed;
        height:100%;
        background-size: contain ;
    }
</style>
@endif
@endsection
@section('head_js')
<script type="text/javascript">
    function tokenValidation(token) {
        document.getElementsByName('g_token')[0].value = token;
    }
</script>
{!! htmlScriptTagJsApi([
'action' => 'submit',
'custom_validation' => 'tokenValidation'
]) !!}
@endsection

<x-base-layout>
    @include('components.notification')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <h3 class="fw-bolder">Lengkapi Identitas</h3>
            </div>
        </div>
        <div class="card-body pt-6">
            <!--begin::Stepper-->
            <div class="stepper stepper-pills d-flex flex-column" id="kt_create_account_stepper">
                <!--begin::Nav-->
                <div class="stepper-nav flex-center flex-wrap mb-10">

                    <!--begin::Step 2-->
                    <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                        <!--begin::Line-->
                        <div class="stepper-line w-40px"></div>
                        <!--end::Line-->

                        <!--begin::Icon-->
                        <div class="stepper-icon w-40px h-40px">
                            <i class="stepper-check fas fa-check"></i>
                            <span class="stepper-number">1</span>
                        </div>
                        <!--end::Icon-->

                        <!--begin::Label-->
                        <div class="stepper-label">
                            <h3 class="stepper-title">
                                Step 1
                            </h3>

                            <div class="stepper-desc">
                                Data Pribadi
                            </div>
                        </div>
                        <!--end::Label-->
                    </div>
                    <!--end::Step 2-->

                    <!--begin::Step 3-->
                    <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                        <!--begin::Line-->
                        <div class="stepper-line w-40px"></div>
                        <!--end::Line-->

                        <!--begin::Icon-->
                        <div class="stepper-icon w-40px h-40px">
                            <i class="stepper-check fas fa-check"></i>
                            <span class="stepper-number">2</span>
                        </div>
                        <!--begin::Icon-->

                        <!--begin::Label-->
                        <div class="stepper-label">
                            <h3 class="stepper-title">
                                Step 2
                            </h3>

                            <div class="stepper-desc">
                                Foto
                            </div>
                        </div>
                        <!--end::Label-->
                    </div>
                    <!--end::Step 3-->

                </div>
                <!--end::Nav-->
                <!--begin::Form-->
                <form class="mx-auto mw-1000px w-100 py-10" method="post" novalidate="novalidate"
                    id="kt_create_account_form" action="{{route('submit_identitas')}}">

                    <!--begin::Step 2-->
                    {{-- <form class="mx-auto mw-1000px w-100 py-10" method="post" novalidate="novalidate"
                    id="kt_create_account_form" action="{{route('submit_identitas')}}">
                    {{ csrf_field() }} --}}
                    <div class="current" data-kt-stepper-element="content">
                         <input type="hidden" class="g_token"  name="g_token"  id="g_token" />

                        {{-- {{ csrf_field() }} --}}
                        @csrf

                        <!--begin::Wrapper-->
                        <div class="w-100">
                            <div id="alert-bukutamu">
                                @if (session('alert-error-bukutamu'))
                                <div class="row justify-content-center">
                                    <div class="col-8 mb-10">
                                        <div class="alert alert-danger ">
                                            <strong>
                                                {{ session('alert-error-bukutamu') }}

                                                @if (session('errors-bukutamu'))
                                                <ul class="text-left mt-4 mb-0">
                                                    @foreach (session('errors-bukutamu') as $error)
                                                    <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <!--begin::Heading-->
                            <div class="pb-10 pb-lg-12">
                                <!--begin::Title-->
                                <h2 class="fw-bolder text-dark">Masukan No Kartu Identitas Anda</h2>
                                <!--end::Title-->

                            </div>
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Label-->
                                <label class="form-label required">Tipe No Identitas</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <select name="nik_type" id="nik_type"
                                    class="form-select form-select-lg form-select-solid" data-control="select2"
                                    data-placeholder="Pilih Tipe Identitas..." data-allow-clear="true"
                                    data-hide-search="true">
                                    @foreach ($identity_type as $kNT => $nik_type)
                                    <option {{ isset($old_input['nik_type']) && $old_input['nik_type']===$kNT
                                        ? 'selected' : '' }} value="{{ $kNT }}">{{
                                        $nik_type }}</option>
                                    @endforeach
                                </select>
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--end::Heading-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10" id="div_nik_ktp">
                                <!--begin::Label-->
                                <label class="form-label required">No KTP</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input name="nik_ktp" id="nik_ktp" name="nik_ktp" placeholder="Masukan no KTP Anda"
                                    value="{{ isset($old_input['nik_ktp']) ? $old_input['nik_ktp'] : '' }}"
                                    class="form-control form-control-lg form-control-solid" />
                                <!--end::Input-->
                            </div>
                            <div class="fv-row mb-10 d-none" id="div_nik_sim">
                                <!--begin::Label-->
                                <label class="form-label required">No KTP</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input name="nik_sim" id="nik_sim" placeholder="Masukan no SIM Anda"
                                    value="{{ isset($old_input['nik_sim']) ? $old_input['nik_sim'] : '' }}"
                                    class="form-control form-control-lg form-control-solid @error('nik_sim') is-invalid @enderror" />
                                <!--end::Input-->
                            </div>
                            <div class="fv-row mb-10 d-none" id="div_nik_passport">
                                <!--begin::Label-->
                                <label class="form-label required">No Passport</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input name="nik_passport" id="nik_passport" placeholder="Masukan no SIM Anda"
                                    value="{{ isset($old_input['nik_sim']) ? $old_input['nik_sim'] : '' }}"
                                    placeholder="Masukan no Passport Anda" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->

                            <!--begin::Heading-->
                            <div class=" pb-10 pb-lg-15">
                                <!--begin::Title-->
                                <h2 class="fw-bolder text-dark">Masukan Data Pribadi Anda</h2>
                                <!--end::Title-->

                            </div>
                            <!--end::Heading-->
                            <div class="row mb-10">
                                <div class="col-md-6">
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                            <span class="required">Nama Anda</span>
                                        </label>
                                        <!--end::Label-->
                                        <input type="text" class="form-control form-control-solid" name="nama"
                                            placeholder="Masukan nama sesuai KTP Anda"
                                            value="{{ isset($old_input['nama']) ? $old_input['nama'] : '' }}"
                                            id="guest_nama" />

                                    </div>
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                            <span class="required">No HP / Whatsapp</span>
                                        </label>
                                        <!--end::Label-->
                                        <input type="text" class="form-control form-control-solid" name="telepon"
                                            placeholder="Masukan no HP atau whatsapp"
                                            value="{{ isset($old_input['telepon']) ? $old_input['telepon'] : '' }}"
                                            id="guest_telp" />

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                            <span class="">Email Anda</span>
                                        </label>
                                        <!--end::Label-->
                                        <input type="text" class="form-control form-control-solid" name="email"
                                            placeholder="Masukan email Anda"
                                            value="{{ isset($old_input['email']) ? $old_input['email'] : '' }}"
                                            id="guest_email" />
                                        <span class="form-text text-muted">Kosongkan apabila Anda tidak memiliki email.
                                        </span>
                                    </div>
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
                                            <span class="">Plat Kendaraan</span>
                                        </label>
                                        <!--end::Label-->
                                        <input type="text" class="form-control form-control-solid" name="plat_kendaraan"
                                            placeholder="Masukan no plat kendaraan Anda"
                                            value="{{ isset($old_input['plat_kendaraan']) ? $old_input['plat_kendaraan'] : '' }}"
                                            id="guest_plat_kendaraan" />
                                        <span class="form-text text-muted">Kosongkan apabila Anda tidak memiliki
                                            kendaraan.</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="fv-row mb-10">
                                        <label class="form-label required">Alamat Anda</label>
                                        <textarea type="text" class="form-control form-control-solid" name="alamat"
                                            placeholder="Masukan alamat sesuai KTP Anda" id="guest_alamat"
                                            style="min-height: 85px;">{{ isset($old_input['alamat']) ? $old_input['alamat'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="fv-row mb-10">
                                        <label class="form-label required">Tujuan</label>
                                        <textarea type="text" class="form-control form-control-solid" name="tujuan"
                                            placeholder="Masukan tujuan Anda" id="guest_tujuan"
                                            style="min-height: 85px;">{{ isset($old_input['tujuan']) ? $old_input['tujuan'] : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="form-label required">Tanggal kedatangan</label>
                                    <input type="text" class="form-control form-control-solid form-control-tanggal" name="tanggal_kedatangan"
                                        id="tanggal_kedatangan" value="{{ old('dari_tgl') ?: date('Y-m-d') }}" required>
                                </div>
                                <div class="col-md-3">
                                    <label class="form-label required">Waktu kedatangan</label>
                                    <input class="form-control timepicker-kedatangan form-control-solid" readonly required="required" name="waktu_kedatangan"
                                        type="text" id="waktu_kedatangan" value="{{ old('waktu_kedatangan') ?: date('H:i') }}">
                                </div>
                            </div>

                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Step 2-->

                    <!--begin::Step 3-->
                    <div   data-kt-stepper-element="content">
                        <!--begin::Wrapper-->
                        <div class="w-100">
                            <div>
                                <h4 class="mb-10 font-weight-bold text-dark">Ambil foto diri Anda</h4>

                                <div id="bx-cam">
                                    <video id="webcam" autoplay playsinline></video>
                                    <canvas id="canvas" class="d-none"></canvas>
                                    <div id="flash-cam" class="flash-cam"></div>
                                    <input type="hidden" name="image" id="download-photo" />
                                </div>

                                <div id="bx-cam-icon">
                                    <div class="row-icon">
                                        <a id="take-photo" class="cam-icon">
                                            <i class="fas fa-camera"></i>
                                        </a>
                                        <a id="retake-photo" class="d-none cam-icon">
                                            <i class="fas fa-portrait"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-10 pt-5" style="border-top:1px solid #EBEDF3;">
                                <h4 class="mb-10 mt-20 font-weight-bold text-dark">Ambil foto kendaraan Anda</h4>

                                <div id="bx-cam-kendaraan">
                                    <video id="webcamkendaraan" autoplay playsinline></video>
                                    <canvas id="canvaskendaraan" class="d-none"></canvas>
                                    <div id="flash-cam-kendaraan" class="flash-cam"></div>
                                    <input type="hidden" name="image_plat_kendaraan" id="download-photo-kendaraan" />
                                </div>

                                <div id="bx-cam-kendaraan-icon">
                                    <div class="row-icon">
                                        <a id="take-photo-kendaraan" class="cam-icon">
                                            <i class="fas fa-camera"></i>
                                        </a>
                                        <a id="retake-photo-kendaraan" class="d-none cam-icon">
                                            <i class="fas fa-portrait"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Step 3-->
                    <!--begin::Actions-->
                    <div class="d-flex flex-stack pt-15">
                        <!--begin::Wrapper-->
                        <div class="mr-2">
                            <button type="button" class="btn btn-lg btn-light-primary me-3"
                                data-kt-stepper-action="previous">
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
                                <span class="svg-icon svg-icon-4 me-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
                                        <path
                                            d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z"
                                            fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->Back
                            </button>
                        </div>
                        <!--end::Wrapper-->
                        <!--begin::Wrapper-->
                        <div>
                            <input type="hidden" name="kode_satker" value="02" id="kode_satker"/>
                            <button type="submit" class="btn btn-lg btn-primary me-3" data-kt-stepper-action="submit">
                                <span class="indicator-label">Submit
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                    <span class="svg-icon svg-icon-3 ms-2 me-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1"
                                                transform="rotate(-180 18 13)" fill="black" />
                                            <path
                                                d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                                fill="black" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </span>
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                            <button type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="next">Continue
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none">
                                        <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1"
                                            transform="rotate(-180 18 13)" fill="black" />
                                        <path
                                            d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                            fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </button>
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Actions-->
                    <input type="hidden" name="id_layanan" value={{ $id_layanan }}>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Stepper-->
        </div>
    </div>
    <input type="hidden" name="idLayanan" value="{{ request()->query('idLayanan') }}">

    @section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script src="{{url("")}}/demo1/js/custom/identitas-wizard.js"></script>
    <script src="{{url("")}}/demo1/js/application/webcam-easy-1.0.5.min.js"></script>
    <script>
        // $('.select2').select2();
        // $('.select2').addClass('mb-5').addClass('mt-3');
        $('.form-control-tanggal').datepicker({
            dateFormat: "yyyy-mm-dd",
            autoclose: true,
            enableOnReadonly: true
        }).datepicker("setDate", new Date());

        const _token = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': _token
            }
        });

        $(document).ready(function() {
            $('.select2-satker').select2({
                placeholder: "",
                ajax: {
                    url: "{{ route('layanan.get-satker') }}",
                    dataType: 'json',
                    delay: 250,
                    quietMillis: 500,
                    maximumSelectionLength: 30,
                    type: "POST",
                    multiple: false,
                    data: function(params) {
                        return {
                            keyword: params.term || "",
                            page: params.page || 1
                        }
                    },
                    processResults: function(data) {
                        return data;
                    },
                    cache: true
                }
            });

            $(".timepicker-kedatangan").timepicker({
                showMeridian: false,
                minuteStep: 1,
                icons: {
                    up: 'fas fa-chevron-up',
                    down: 'fas fa-chevron-down'
                }
            });

            // $('.select2-satker').trigger('changeSatker');

            // const changeSatker = () => {
            //     console.log('abc')
            // }


            //console.log(selectedSatker)
        })

        // $('.select2-satker').on('select2:selected', function (e) {
        //         let selectedSatker = $('#kode_satker').val( $('.select2-satker').val() )
        //     );
        // })
    </script>
    <script>
        // selfie
                    const webcamElement = document.getElementById('webcam');
                    const canvasElement = document.getElementById('canvas');
                    const snapSoundElement = document.getElementById('snapSound');
                    const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);

                    webcam.start()
                        .then(result =>{
                            console.log("webcam started");
                        })
                        .catch(err => {
                            console.log(err);
                        }
                    );

                    $("#take-photo").click(function () {
                        beforeTakePhoto();
                        let picture = webcam.snap();
                        $("#download-photo").val(picture);
                        // document.querySelector('#download-photo').href = picture;
                        afterTakePhoto();
                    });

                    $("#retake-photo").click(function () {
                        webcam.stream()
                            .then(facingMode =>{
                                removeCapture();
                            }
                        );
                    });

                    function beforeTakePhoto(){
                        $('#flash-cam')
                            .show()
                            .animate({opacity: 0.3}, 500)
                            .fadeOut(500)
                            .css({'opacity': 0.7});
                        // window.scrollTo(0, 0);
                        $('#webcam-control').addClass('d-none');
                        $('#cameraControls').addClass('d-none');
                    };

                    function afterTakePhoto(){
                        webcam.stop();
                        $('#canvas').removeClass('d-none');
                        $('#take-photo').addClass('d-none');
                        $('#retake-photo').removeClass('d-none');
                        // $('#download-photo').removeClass('d-none');
                        // $('#resume-camera').removeClass('d-none');
                        // $('#cameraControls').removeClass('d-none');
                    }

                    function removeCapture(){
                        $('#canvas').addClass('d-none');
                        $('#take-photo').removeClass('d-none');
                        $('#retake-photo').addClass('d-none');
                    }
    </script>

    <script>
        // kendaraan
                    const webcamElementKendaraan = document.getElementById('webcamkendaraan');
                    const canvasElementKendaraan = document.getElementById('canvaskendaraan');
                    const snapSoundElementKendaraan = document.getElementById('snapSound');
                    const webcamKendaraan = new Webcam(webcamElementKendaraan, 'user', canvasElementKendaraan, snapSoundElementKendaraan);

                    webcamKendaraan.start()
                        .then(result =>{
                            console.log("webcam kendaraan started");
                        })
                        .catch(err => {
                            console.log(err);
                        }
                    );

                    $("#take-photo-kendaraan").click(function () {
                        beforeTakePhotoKendaraan();
                        let pictureKendaraan = webcamKendaraan.snap();
                        $("#download-photo-kendaraan").val(pictureKendaraan);
                        // document.querySelector('#download-photo').href = pictureKendaraan;
                        afterTakePhotoKendaraan();
                    });

                    $("#retake-photo-kendaraan").click(function () {
                        webcamKendaraan.stream()
                            .then(facingMode =>{
                                removeCapture();
                            }
                        );
                    });

                    function beforeTakePhotoKendaraan(){
                        $('#flash-cam-kendaraan')
                            .show()
                            .animate({opacity: 0.3}, 500)
                            .fadeOut(500)
                            .css({'opacity': 0.7});
                        // window.scrollTo(0, 0);
                        // $('#webcam-control').addClass('d-none');
                        // $('#cameraControls').addClass('d-none');
                    };

                    function afterTakePhotoKendaraan(){
                        webcamKendaraan.stop();
                        $('#canvaskendaraan').removeClass('d-none');
                        $('#take-photo-kendaraan').addClass('d-none');
                        $('#retake-photo-kendaraan').removeClass('d-none');
                        // $('#download-photo').removeClass('d-none');
                        // $('#resume-camera').removeClass('d-none');
                        // $('#cameraControls').removeClass('d-none');
                    }

                    function removeCaptureKendaraan(){
                        $('#canvaskendaraan').addClass('d-none');
                        $('#take-photo-kendaraan').removeClass('d-none');
                        $('#retake-photo-kendaraan').addClass('d-none');
                    }
    </script>
    @endsection
</x-base-layout>
