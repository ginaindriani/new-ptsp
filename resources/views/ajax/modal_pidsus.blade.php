<div class="modal fade" tabindex="-1" id="kt_modal_1">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PIDSUS</h5>
                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="svg-icon svg-icon-2x">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                        </svg>
                    </span>
                </div>
                <!--end::Close-->
            </div>
            <div class="modal-body">
                <div class="row" id="data-card-pidsus">
                    @foreach ($datas as $item)
                        <div class="col-sm-4 card-container mb-5 card-container-pidsus" data-satker="{{$item->kode_satker}}" data-nama-satker="{{$item->satker}}">
                            <div class="card shadow-sm rounded clickable-container">
                                <div class="card-body">
                                    <p class="card-title">{{$item->satker}}</p>
                                    <b>{{$item->total}} Perkara</b>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="p-5" id="data-card-table-pidsus">
                    <button class="btn btn-sm btn-success mb-5" type="button" id="back-pidsus">
                        <i class="fa fa-arrow-left"></i> BACK
                    </button>
                    <h2 id="title_satker_pidsus" class="text-center">Data Pidsus</h2>
                    <div class="table-responsive">
                        <table class="table dataTable" id="datatable-pidsus">
                            <thead>
                                <tr>
                                    <th>BERKAS</th>
                                    <th>Satker</th>
                                    <th>NO SPDP</th>
                                    <th>NAMA TERSANGKA</th>
                                    <th>TGL SPDP</th>
                                    <th>JENIS PIDANA</th>
                                    <th>TAHAPAN BERKAS</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <script>
        var kode_satker = null;
        var _tablePidsus = null;
        function datatableInitPidsus() {
            _tablePidsus = $("#datatable-pidsus").DataTable({
                autoWidth: false,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{route("dashboard.pidsusdata")}}' + '?kode_satker=' + kode_satker,
                    type: "GET",
                    data: function (data) {}
                },
                columns: [
                    {data: 'id_berkas', name: 'id_berkas'},
                    {data: 'satker', name: 'satker_long_lats.satker'},
                    {data: 'no_spdp', name: 'no_spdp'},
                    {data: 'nama_tsk', name: 'nama_tsk'},
                    {data: 'tgl_spdp', name: 'tgl_spdp'},
                    {data: 'jenis_pidana', name: 'jenis_pidana'},
                    {data: 'tahap_berkas', name: 'tahap_berkas'},
                ]
            });
        }

        $(document).ready(function(){
            // datatableInitPidsus();
            $('#data-card-table-pidsus').hide();
        });

        $(document).on('click', '#back-pidsus', function(){
            $('#data-card-pidsus').show();
            $('#data-card-table-pidsus').hide();
        });

        $(document).on('click', '.card-container-pidsus', function(){
            kode_satker = $(this).attr('data-satker');
            var title_satker = $(this).attr('data-nama-satker');

            $('#data-card-table-pidsus').show();
            $('#data-card-pidsus').hide();
            $('#title_satker_pidsus').html(title_satker);

            if (_tablePidsus) {
                _tablePidsus.destroy();
            }
            datatableInitPidsus();
        });
    </script>
</div>
