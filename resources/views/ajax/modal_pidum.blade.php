<div class="modal fade" tabindex="-1" id="kt_modal_1">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PIDUM</h5>
                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <span class="svg-icon svg-icon-2x">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                        </svg>
                    </span>
                </div>
                <!--end::Close-->
            </div>
            <div class="modal-body">
                <div class="row" id="data-card-pidum">
                    @foreach ($datas as $item)
                        <div class="col-sm-4 card-container mb-5 card-container-pidum" data-satker="{{$item->kode_satker}}" data-nama-satker="{{$item->satker}}">
                            <div class="card shadow-sm rounded clickable-container">
                                <div class="card-body">
                                    <p class="card-title">{{$item->satker}}</p>
                                    <b>{{$item->total}} Perkara</b>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="p-5" id="data-card-table-pidum">
                    <button class="btn btn-sm btn-success mb-5" type="button" id="back-pidum">
                        <i class="fa fa-arrow-left"></i> BACK
                    </button>
                    <h2 id="title_satker_pidum" class="text-center">Data Pidum</h2>
                    <div class="table-responsive">
                        <table class="table dataTable" id="datatable-pidum">
                            <thead>
                                <tr>
                                    <th>BERKAS</th>
                                    <th>NO SPDP</th>
                                    <th>ID PERKARA</th>
                                    <th>NAMA TERSANGKA</th>
                                    <th>TGL SPDP</th>
                                    <th>JENIS PIDANA</th>
                                    <th>JENIS PERKARA</th>
                                    <th>TAHAPAN BERKAS</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <script>
        var kode_satker_pidum = null;
        var _tablePidum = null;
        function datatableInitPidum() {
            _tablePidum = $("#datatable-pidum").DataTable({
                autoWidth: false,
                responsive: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{route("dashboard.pidumdata")}}' + '?kode_satker=' + kode_satker_pidum,
                    type: "GET",
                    data: function (data) {}
                },
                columns: [
                    {data: 'id_berkas', name: 'id_berkas'},
                    {data: 'no_spdp', name: 'no_spdp'},
                    {data: 'id_perkara', name: 'id_perkara'},
                    {data: 'nama_tsk', name: 'nama_tsk'},
                    {data: 'tgl_spdp', name: 'tgl_spdp'},
                    {data: 'jenis_pidana', name: 'jenis_pidana'},
                    {data: 'jenis_perkara', name: 'jenis_perkara'},
                    {data: 'tahap_berkas', name: 'tahap_berkas'},
                ]
            });
        }

        $(document).ready(function(){
            // datatableInitPidsus();
            $('#data-card-table-pidum').hide();
        });

        $(document).on('click', '#back-pidum', function(){
            $('#data-card-pidum').show();
            $('#data-card-table-pidum').hide();
        });

        $(document).on('click', '.card-container-pidum', function(){
            kode_satker_pidum = $(this).attr('data-satker');
            var title_satker = $(this).attr('data-nama-satker');

            $('#data-card-table-pidum').show();
            $('#data-card-pidum').hide();
            $('#title_satker_pidum').html(title_satker);

            console.log(_tablePidum);
            if (_tablePidum) {
                _tablePidum.destroy();
            }
            datatableInitPidum();
        });
    </script>
</div>
