<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pengaduan Masyarakat Kejaksaan</title>
</head>

<body>
    <p>Email: {{ $email }}</p>
    <p>Nama: {{ ucwords($name) }}</p>
    @if ($phone)
        <p>No Hp: {{ $phone }}</p>
    @endif
    <p>Pesan: </p>
    <p>{{ $body }}</p>
    <p>Terimakasih</p>
</body>

</html>
