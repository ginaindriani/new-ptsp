checkAccess();

function checkAccess() {
	var v_accepted_domains=new Array("localhost","kejaksaan.go.id")
	 
	var v_domaincheck=document.location.href //retrieve the current URL of user browser
	var v_accepted_ok=false //set acess to false by default
	 
	if (v_domaincheck.indexOf("http")!=-1){ //if this is a http request
	    for (r=0;r<v_accepted_domains.length;r++){
	        if (v_domaincheck.indexOf(v_accepted_domains[r])!=-1){ //if a match is found
	            v_accepted_ok=true //set access to true, and break out of loop
	            break
	        }
	    }
	}
	else
	    v_accepted_ok=true
	 
	if (!v_accepted_ok){
	    //alert("You\'re not allowed to directly link to this .js file on our server!")
	    history.back(-1)
	}
}

function setBitFormat(p_data) {
	var v_data = "0";
	if ((p_data == "0") || (p_data == "1")) {
		v_data = p_data;
	}
	
	return v_data;
}

function setIntFormat(p_data) {
	var v_data = 0;
	if (Number.isInteger(parseInt(p_data))) {
		v_data = p_data;
	}
	
	return v_data;
}

function setNumFormat(p_data) {
	var v_data = 0;
	if (Number.isInteger(parseInt(p_data))) {
		v_data = p_data;
	}
	
	return v_data;
}

Number.prototype.setDecFormat = function (n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

function reverseDateFormat(p_data) {
	var v_data = p_data.split("-");
	var v_formatted = '';
	
	if (p_data == '' || p_data == '0000-00-00' || p_data == '1000-01-01') {
		v_formatted = '';
	}
	else if (isPickerDate(p_data) == false) {
		v_formatted = '';
	}
	else {
		v_formatted = parseInt(v_data[1]) + '/' + parseInt(v_data[2]) + '/' + parseInt(v_data[0]);
	}
	
	return v_formatted;
}

function reverseDateFormatToDMY(p_data) {
	var v_data = p_data.split("-");
	var v_formatted = '';
	
	if (p_data == '' || p_data == '0000-00-00' || p_data == '1000-01-01') {
		v_formatted = '';
	}
	else if (isPickerDate(p_data) == false) {
		v_formatted = '';
	}
	else {
		v_formatted = ((v_data[2].length == 2) ? v_data[2] : '0' + v_data[2]) + '-' + ((v_data[1].length == 2) ? v_data[1] : '0' + v_data[1]) + '-' + v_data[0];
	}
	
	return v_formatted;
}

function setDateFormat(p_data) {
	var v_data = p_data.split("/");
	var v_formatted = '';
	
	if (p_data == '') {
		v_formatted = '1000-01-01'; //'0000-00-00';
	}
	else if (isDate(p_data) == false) {
		v_formatted = '1000-01-01'; //'0000-00-00';
	}
	else {
		v_formatted = v_data[2] + '-' + ((v_data[0].length == 2) ? v_data[0] : '0' + v_data[0]) + '-' + ((v_data[1].length == 2) ? v_data[1] : '0' + v_data[1]);
	}
	
	return v_formatted;
}

function setDateFormatFromDMY(p_data) {
	var v_data = p_data.split("-");
	var v_formatted = '';
	
	if (p_data == '') {
		v_formatted = '1000-01-01'; //'0000-00-00';
	}
	else if (isDateDMY(p_data) == false) {
		v_formatted = '1000-01-01'; //'0000-00-00';
	}
	else {
		v_formatted = v_data[2] + '-' + ((v_data[1].length == 2) ? v_data[1] : '0' + v_data[1]) + '-' + ((v_data[0].length == 2) ? v_data[0] : '0' + v_data[0]);
	}
	
	return v_formatted;
}

function isDate(p_data) {
	var v_data = p_data.split("/");
	var v_year = 0;
	var v_month = 0;
	var v_date = 0;
	
	if (v_data.length < 3) {
		return false;
	}
	else {
		if (Number.isInteger(parseInt(v_data[0])) && Number.isInteger(parseInt(v_data[1])) && Number.isInteger(parseInt(v_data[2]))) {
			v_year = parseInt(v_data[2]);
			v_month = parseInt(v_data[0]);
			v_date = parseInt(v_data[1]);
			
			if (v_month < 1 && v_month > 12) {
				//alert('v_month'+v_month);
				return false;
			}
			else if (v_year < 1800 && v_year > 9999) {
				//alert('v_month'+v_month);
				return false;
			}
			else if (v_date < 1) {
				//alert('v_date'+v_date);
				return false;
			}
			else {
				if ((v_month == '1' || v_month == '3' || v_month == '5' || v_month == '7' || v_month == '8' || v_month == '10' || v_month == '12') && v_date > 31) {
					//alert('1.v_month'+v_month+'-v_date'+v_date);
					return false;
				}
				else if ((v_month == '4' || v_month == '6' || v_month == '9' || v_month == '11') && v_date > 30) {
					//alert('2.v_month'+v_month+'-v_date'+v_date);
					return false;
				}
				else if (v_month == '2') {
					if  (((v_year % 4) == 0 && v_date > 29) || ((v_year % 4) != 0 && v_date > 28)) {
						//alert('3.kabisat'+(v_year % 4)+'-v_date'+v_date);
						return false;
					}
				}
			}
		}
		else {
			//alert('not number'+v_data[0]+'-'+v_data[1]+'-'+v_data[2]);
			return false;
		}
	}
	
	return true;
}

function isDateDMY(p_data) {
	var v_data = p_data.split("-");
	var v_year = 0;
	var v_month = 0;
	var v_date = 0;
	
	if (v_data.length < 3) {
		return false;
	}
	else {
		if (Number.isInteger(parseInt(v_data[0])) && Number.isInteger(parseInt(v_data[1])) && Number.isInteger(parseInt(v_data[2]))) {
			v_year = parseInt(v_data[2]);
			v_month = parseInt(v_data[1]);
			v_date = parseInt(v_data[0]);
			
			if (v_month < 1 && v_month > 12) {
				//alert('v_month'+v_month);
				return false;
			}
			else if (v_year < 1800 && v_year > 9999) {
				//alert('v_month'+v_month);
				return false;
			}
			else if (v_date < 1) {
				//alert('v_date'+v_date);
				return false;
			}
			else {
				if ((v_month == '1' || v_month == '3' || v_month == '5' || v_month == '7' || v_month == '8' || v_month == '10' || v_month == '12') && v_date > 31) {
					//alert('1.v_month'+v_month+'-v_date'+v_date);
					return false;
				}
				else if ((v_month == '4' || v_month == '6' || v_month == '9' || v_month == '11') && v_date > 30) {
					//alert('2.v_month'+v_month+'-v_date'+v_date);
					return false;
				}
				else if (v_month == '2') {
					if  (((v_year % 4) == 0 && v_date > 29) || ((v_year % 4) != 0 && v_date > 28)) {
						//alert('3.kabisat'+(v_year % 4)+'-v_date'+v_date);
						return false;
					}
				}
			}
		}
		else {
			//alert('not number'+v_data[0]+'-'+v_data[1]+'-'+v_data[2]);
			return false;
		}
	}
	
	return true;
}

function isPickerDate(p_data) {
	if (p_data == null || p_data == 'null') {
		return false;
	}
	var v_data = p_data.split("-");
	var v_year = 0;
	var v_month = 0;
	var v_date = 0;
	
	if (v_data.length < 3) {
		return false;
	}
	else {
		if (Number.isInteger(parseInt(v_data[1])) && Number.isInteger(parseInt(v_data[2])) && Number.isInteger(parseInt(v_data[0]))) {
			v_year = parseInt(v_data[0]); //2
			v_month = parseInt(v_data[1]);//0
			v_date = parseInt(v_data[2]); //1
			
			if (v_month < 1 || v_month > 12) {
				//alert('v_month'+v_month);
				return false;
			}
			else if (v_year < 1800 || v_year > 9999) {
				//alert('v_month'+v_month);
				return false;
			}
			else if (v_date < 1) {
				//alert('v_date'+v_date);
				return false;
			}
			else {
				if ((v_month == '1' || v_month == '3' || v_month == '5' || v_month == '7' || v_month == '8' || v_month == '10' || v_month == '12') && v_date > 31) {
					//alert('1.v_month'+v_month+'-v_date'+v_date);
					return false;
				}
				else if ((v_month == '4' || v_month == '6' || v_month == '9' || v_month == '11') && v_date > 30) {
					//alert('2.v_month'+v_month+'-v_date'+v_date);
					return false;
				}
				else if (v_month == '2') {
					if  (((v_year % 4) == 0 && v_date > 29) || ((v_year % 4) != 0 && v_date > 28)) {
						//alert('3.kabisat'+(v_year % 4)+'-v_date'+v_date);
						return false;
					}
				}
			}
		}
		else {
			//alert('not number'+v_data[0]+'-'+v_data[1]+'-'+v_data[2]);
			return false;
		}
	}
	
	return true;
}

function isPickerDateDMY(p_data) {
	if (p_data == null || p_data == 'null') {
		return false;
	}
	var v_data = p_data.split("-");
	var v_year = 0;
	var v_month = 0;
	var v_date = 0;
	
	if (v_data.length < 3) {
		return false;
	}
	else {
		if (Number.isInteger(parseInt(v_data[1])) && Number.isInteger(parseInt(v_data[2])) && Number.isInteger(parseInt(v_data[0]))) {
			v_year = parseInt(v_data[0]); //2
			v_month = parseInt(v_data[1]);//0
			v_date = parseInt(v_data[2]); //1
			
			if (v_month < 1 || v_month > 12) {
				//alert('v_month'+v_month);
				return false;
			}
			else if (v_year < 1800 || v_year > 9999) {
				//alert('v_month'+v_month);
				return false;
			}
			else if (v_date < 1) {
				//alert('v_date'+v_date);
				return false;
			}
			else {
				if ((v_month == '1' || v_month == '3' || v_month == '5' || v_month == '7' || v_month == '8' || v_month == '10' || v_month == '12') && v_date > 31) {
					//alert('1.v_month'+v_month+'-v_date'+v_date);
					return false;
				}
				else if ((v_month == '4' || v_month == '6' || v_month == '9' || v_month == '11') && v_date > 30) {
					//alert('2.v_month'+v_month+'-v_date'+v_date);
					return false;
				}
				else if (v_month == '2') {
					if  (((v_year % 4) == 0 && v_date > 29) || ((v_year % 4) != 0 && v_date > 28)) {
						//alert('3.kabisat'+(v_year % 4)+'-v_date'+v_date);
						return false;
					}
				}
			}
		}
		else {
			//alert('not number'+v_data[0]+'-'+v_data[1]+'-'+v_data[2]);
			return false;
		}
	}
	
	return true;
}

function showPagination(p_functionBindData, p_dataVariable, p_data, p_dataPerPage, p_totalPageConfig, p_page) {
	var v_rows = '';
	var v_dataPerPage = p_dataPerPage;
	var v_totalPageConfig = p_totalPageConfig;
	var v_totalData = p_data.length;
	var v_totalPageData = Math.ceil(v_totalData / v_dataPerPage);
	var v_totalPage = v_totalPageConfig;
	var v_pageFirst = 1;
	
	if (v_totalPage > Math.ceil(v_totalData / v_dataPerPage)) {
		v_totalPage = Math.ceil(v_totalData / v_dataPerPage);
	}
	
	v_pageFirst = Math.floor(v_totalPageConfig / 2);

	if ((p_page + v_pageFirst) > v_totalPageData) {
		v_pageFirst = v_totalPageData - (v_totalPageConfig - 1);
	}
	else {
		v_pageFirst = (p_page + v_pageFirst) - v_totalPageConfig;
	}
	
	if (v_pageFirst <= 0) {
		v_pageFirst = 1;
	}
	
	if (((v_pageFirst + v_totalPageConfig) - 1) < v_totalPageData) {
		v_totalPage = (v_pageFirst + v_totalPageConfig) - 1;
	}
	else {
		v_totalPage = v_totalPageData;
	}
	
	if (v_totalData > v_dataPerPage) {
		v_rows = v_rows + '<nav>';
		v_rows = v_rows + '<ul class="pagination pagination-sm">';
        if (p_page > 1) {
			v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', 1, false);">First</a></li>';
			v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+ (p_page - 1) +', false);">Prev</a>';
		}
        else {
			v_rows = v_rows + '<li class="disabled"><span aria-hidden="true">First</span></li>';
			v_rows = v_rows + '<li class="disabled"><span aria-hidden="true">Prev</span></li>';
		}
		
		for (i = v_pageFirst; i <= v_totalPage; i++) {
			if (i == p_page) {
				v_rows = v_rows + '<li class="active"><span aria-hidden="true">'+i+'</span></li>';
			}
			else {
				if (i == v_totalPage) {
					v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+i+', false);">'+i+'</a></li>';
				}
				else {
					v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+i+', false);">'+i+'</a></li>';
				}
			}
		}

        if (p_page < v_totalPageData) {
			v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+ (p_page + 1) +', false);">Next</a></li>';
			v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+ v_totalPageData +', false);">Last</a></li>';
		}
        else {
			v_rows = v_rows + '<li class="disabled"><span aria-hidden="true">Next</span></li>';
			v_rows = v_rows + '<li class="disabled"><span aria-hidden="true">Last</span></li>';
		}
		v_rows = v_rows + '</ul>';
		v_rows = v_rows + '</nav>';
	}
	return v_rows;
}

function showPagination2(p_functionBindData, p_dataVariable, p_data, p_dataPerPage, p_totalPageConfig, p_page) {
	var v_rows = '';
	var v_dataPerPage = p_dataPerPage;
	var v_totalPageConfig = p_totalPageConfig;
	var v_totalData = p_data.length;
	var v_totalPageData = Math.ceil(v_totalData / v_dataPerPage);
	var v_totalPage = v_totalPageConfig;
	var v_pageFirst = 1;
	
	if (v_totalPage > Math.ceil(v_totalData / v_dataPerPage)) {
		v_totalPage = Math.ceil(v_totalData / v_dataPerPage);
	}
	
	v_pageFirst = Math.floor(v_totalPageConfig / 2);

	if ((p_page + v_pageFirst) > v_totalPageData) {
		v_pageFirst = v_totalPageData - (v_totalPageConfig - 1);
	}
	else {
		v_pageFirst = (p_page + v_pageFirst) - v_totalPageConfig;
	}
	
	if (v_pageFirst <= 0) {
		v_pageFirst = 1;
	}
	
	if (((v_pageFirst + v_totalPageConfig) - 1) < v_totalPageData) {
		v_totalPage = (v_pageFirst + v_totalPageConfig) - 1;
	}
	else {
		v_totalPage = v_totalPageData;
	}
	
	if (v_totalData > v_dataPerPage) {
		v_rows = v_rows + '<nav>';
		v_rows = v_rows + '<ul class="pagination pagination-sm">';
        if (p_page > 1) {
			v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', 1);">First</a></li>';
			v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+ (p_page - 1) +');">Prev</a>';
		}
        else {
			v_rows = v_rows + '<li class="disabled"><span aria-hidden="true">First</span></li>';
			v_rows = v_rows + '<li class="disabled"><span aria-hidden="true">Prev</span></li>';
		}
		
		for (i = v_pageFirst; i <= v_totalPage; i++) {
			if (i == p_page) {
				v_rows = v_rows + '<li class="active"><span aria-hidden="true">'+i+'</span></li>';
			}
			else {
				if (i == v_totalPage) {
					v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+i+');">'+i+'</a></li>';
				}
				else {
					v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+i+');">'+i+'</a></li>';
				}
			}
		}

        if (p_page < v_totalPageData) {
			v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+ (p_page + 1) +');">Next</a></li>';
			v_rows = v_rows + '<li><a href="javascript:'+p_functionBindData+'('+p_dataVariable+', '+ v_totalPageData +');">Last</a></li>';
		}
        else {
			v_rows = v_rows + '<li class="disabled"><span aria-hidden="true">Next</span></li>';
			v_rows = v_rows + '<li class="disabled"><span aria-hidden="true">Last</span></li>';
		}
		v_rows = v_rows + '</ul>';
		v_rows = v_rows + '</nav>';
	}
	return v_rows;
}
	
function generatePickerDate(p_pickerId, p_targetId) {
	$('#'+p_pickerId).monthly({
		mode: 'picker',
		target: '#'+p_targetId,
		setWidth: '225px',
		startHidden: true,
		showTrigger: '#'+p_targetId,
		stylePast: false,
		disablePast: false
	});
}

function sortJSON(data, key, way) {
    return data.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        if (way === 'asc' ) { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
        if (way === 'desc') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
    });
}

function predicate() {
    var fields = [],
      	n_fields = arguments.length,
      	field, name, reverse, cmp;

    var default_cmp = function (a, b) {
    if (a === b) return 0;
        return a < b ? -1 : 1;
	},
    getCmpFunc = function (primer, reverse) {
        var dfc = default_cmp,
          	// closer in scope
          	cmp = default_cmp;
        if (primer) {
          	cmp = function (a, b) {
            	return dfc(primer(a), primer(b));
        	};
    	}
    	if (reverse) {
        	return function (a, b) {
            	return -1 * cmp(a, b);
        	};
    	}
    	return cmp;
    };
	
    // preprocess sorting options
    for (var i = 0; i < n_fields; i++) {
      	field = arguments[i];
      	if (typeof field === 'string') {
        	name = field;
        	cmp = default_cmp;
      	}
      	else {
        	name = field.name;
        	cmp = getCmpFunc(field.primer, field.reverse);
      	}
      	fields.push({
        	name: name,
        	cmp: cmp
      	});
    }
	
    // final comparison function
    return function (A, B) {
      	var a, b, name, result;
      	for (var i = 0; i < n_fields; i++) {
	        result = 0;
        	field = fields[i];
        	name = field.name;
			
        	result = field.cmp(A[name], B[name]);
        	if (result !== 0) break;
      	}
      	return result;
    };
}

function loadJS(file) {
    // DOM: Create the script element
    var jsElm = document.createElement("script");
    // set the type attribute
    jsElm.type = "application/javascript";
    // make the script element load file
    jsElm.src = file;
    // finally insert the element to the body element in order to load the script
    document.body.appendChild(jsElm);
}

function createjscssfile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    return fileref
}
 
function replacejscssfile(oldfilename, newfilename, filetype){
    var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist using
    var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
    var allsuspects=document.getElementsByTagName(targetelement)
    for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
        if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename)!=-1){
            var newelement=createjscssfile(newfilename, filetype)
            allsuspects[i].parentNode.replaceChild(newelement, allsuspects[i])
        }
    }
}

function validateEmail(p_email) {
	var v_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return v_regex.test(p_email);
}



function escapeHtml(p_value) {
	if (p_value === undefined || p_value === null) {
	    return '';
	}
	else if (Number.isInteger(parseInt(p_value))) {
	    return p_value;
	}
	else {
	    return p_value
	         .replace(/&/g, "&amp;")
	         .replace(/</g, "&lt;")
	         .replace(/>/g, "&gt;")
	         .replace(/"/g, "&quot;")
	         .replace(/'/g, "&#039;");
	}
}

function validateHtmlTag(p_value) {
	var v_return = true;
	if(p_value.match(/([\<])([^\>]{1,})*([\>])/i)!=null) {
		v_return = false;
	}
	
	return v_return;
}

function validateNPWP(p_value) {
	var v_regex = /^\d{2}.\d{3}.\d{3}.\d{1}-\d{3}.\d{3}/;
	var v_return = false;
	if (v_regex.test(p_value) == true) {
		if (p_value.length == 20) {
			v_return = true;
		}
	}
	return v_return;
}
//https://stackoverflow.com/questions/9804777/how-to-test-if-a-string-is-json-or-not
function isJson(item) {
    item = typeof item !== "string"
        ? JSON.stringify(item)
        : item;

    try {
        item = JSON.parse(item);
    } catch (e) {
        return false;
    }

    if (typeof item === "object" && item !== null) {
        return true;
    }

    return false;
}

function isNumber(evt) {
  	var theEvent = evt || window.event;
  	var key = theEvent.keyCode || theEvent.which;
  	key = String.fromCharCode(key);
  	if (key.length == 0) return;
  	var regex = /^[0-9.,\b]+$/;
  	if (!regex.test(key)) {
	    theEvent.returnValue = false;
	    if (theEvent.preventDefault) theEvent.preventDefault();
  	}
}

function enterNumbers(event) {
	if ((event.code == 'ArrowLeft') || (event.code == 'ArrowRight') ||
	     (event.code == 'ArrowUp') || (event.code == 'ArrowDown') || 
	     (event.code == 'Tab') || (event.code == 'Enter') || 
	     (event.code == 'Delete') || (event.code == 'Backspace')) {
	     return;
	}
	//else if (event.key.search(/\d/) == -1) {
	else if (event.key.search(/^[0-9,\b]+$/) == -1) {
	    event.preventDefault();
	}
}

function enterNumberOnly(event) {
	if ((event.code == 'ArrowLeft') || (event.code == 'ArrowRight') ||
	     (event.code == 'ArrowUp') || (event.code == 'ArrowDown') || 
	     (event.code == 'Tab') || (event.code == 'Enter') || 
	     (event.code == 'Delete') || (event.code == 'Backspace')) {
	     return;
	}
	//else if (event.key.search(/\d/) == -1) {
	else if (event.key.search(/^[0-9\b]+$/) == -1) {
	    event.preventDefault();
	}
}

function addPeriod(p_value)
{
	if (!Number.isInteger(parseInt(p_value))) {
	    return p_value;
	}
	else {
		p_value = p_value.replace(/\./g, '');
	    p_value += '';
	    x = p_value.split('.');
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + '.' + '$2');
	    }
	    return x1 + x2;
	}
}

function removePeriod(p_value)
{
	if (!Number.isInteger(parseInt(p_value))) {
	    return p_value;
	}
	else {
		p_value = p_value.replace(/\./g, '');
	    return p_value;
	}
}

/*replacejscssfile("oldscript.js", "newscript.js", "js") //Replace all occurences of "oldscript.js" with "newscript.js"
replacejscssfile("oldstyle.css", "newstyle", "css") //Replace all occurences "oldstyle.css" with "newstyle.css"*/