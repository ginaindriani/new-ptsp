$(document).ready(function () {

    $('#terimaTamu').on('click', function (e) {
        e.preventDefault();
        var unique = $(this).data('unique');
        $(this).html('loading...');
        $.ajax({
            url: baseUrl + '/ajax/terima-tamu',
            type: 'POST',
            data: {
                '_token': cToken,
                'unique': unique
            },
            success: function (response, textStatus, request) {
                $(this).html('Sukses Menerima Tamu');
                window.location = baseUrl+'/terima-kunjungan/'+unique;
            },
            error: function (response, textStatus, request) {
                $(this).html('Ya');
                swal.fire({
                    text: response.responseJSON.message,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                });
            }
        });
    });

    $('#tolakTamu').on('click', function (e) {
        e.preventDefault();
        swal.fire({
            html: `
                <div class="form-group">
                    <textarea class="form-control" id="rejectReason" style="min-height:100px;" placeholder="Masukan alasan Anda menolak tamu"></textarea>
                    <a id="submitAlasanTolakTamu" class="btn btn-primary font-weight-bolder">Kirim Alasan Menolak Tamu</a>
                </div>
            `,
            icon: 'warning',
            showCancelButton: false,
            showConfirmButton: false,
        });

        $('#submitAlasanTolakTamu').on('click', function (e) {
            var unique = $('#tolakTamu').data('unique');
            var reason = $('#rejectReason').val();
            $('#tolakTamu').html('loading...');
            $.ajax({
                url: baseUrl + '/ajax/tolak-tamu',
                type: 'POST',
                data: {
                    '_token': cToken,
                    'unique': unique,
                    'reason': reason
                },
                success: function (response, textStatus, request) {
                    $('#tolakTamu').html('Sukses Menolak Tamu');
                    window.location = baseUrl+'/tolak-kunjungan/'+unique;
                },
                error: function (response, textStatus, request) {
                    $('#tolakTamu').html('Tidak');
                    swal.fire({
                        text: response.responseJSON.message,
                        icon: 'error',
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                }
            });
        });
    });
});
