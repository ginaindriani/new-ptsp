
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = new Date().getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ':' + seconds + ' ' + ampm;
    return strTime;
}

$(document).ready(function () {
    // $("#jam").html(formatAMPM(new Date));
    setInterval(function () {
        var seconds = new Date().getSeconds();
        $("#OSec").html((seconds < 10 ? "0" : "") + seconds);
    }, 1000);

    setInterval(function () {
        var minutes = new Date().getMinutes();
        $("#OMin").html((minutes < 10 ? "0" : "") + minutes);
    }, 1000);

    setInterval(function () {
        var hours = new Date().getHours();
        $("#OHours").html((hours < 10 ? "0" : "") + hours);
    }, 1000);

    setInterval(function () {
        var hours = new Date().getHours();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        $("#OAMPM").html(ampm);
    }, 1000);
    
    setInterval(function () {
        var hours = new Date().getHours();
        // hours = hours + hours;
        var greet = 'pagi';
        if (hours >= 19) {
            greet = 'malam';
        } else if (hours >= 12 && hours <= 15) {
            greet = 'siang';
        } else if (hours >= 15 && hours <= 19) {
            greet = 'sore';
        };
        $("#OGreet").html(greet);
    }, 1000);
});