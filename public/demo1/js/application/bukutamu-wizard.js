'use strict';

var KTWizardBukutamu = function () {
    var wizardEl = document.querySelector('#kt_wizard_v3');
    var _wizardEBukutamu;
    var _formBukutamu;
    var _wizard;
    var _validations = [];

    var initWizard = function () {
        _wizard = new KTWizard(_wizardEBukutamu, {
            startStep: 1,
            clickableSteps: false,
            navigation: false
        });

        var _wizardGetstep = _wizard.getStep();

        // Wizard's navigation buttons
        // Custom navigation handlers
        var nextButtonStep1 = wizardEl.querySelector('[id="action-next-step1"]');
        nextButtonStep1.addEventListener('click', function () {
            var isStep1Valid = _validations[_wizardGetstep - 1];
            initValidator(isStep1Valid, 1);
        });

        var nextButtonStep2 = wizardEl.querySelector('[id="action-next-step2"]');
        nextButtonStep2.addEventListener('click', function () {
            var isStep2Valid = _validations[_wizardGetstep - 1];
            initValidator(isStep2Valid, 2);
        });

        var prevButtonStep2 = wizardEl.querySelector('[id="action-prev-step2"]');
        prevButtonStep2.addEventListener('click', function () {
            _wizard.goPrev();
            KTUtil.scrollTop();
        });

        var prevButtonStep3 = wizardEl.querySelector('[id="action-prev-step3"]');
        prevButtonStep3.addEventListener('click', function () {
            _wizard.goPrev();
            KTUtil.scrollTop();
            $('#alert-bukutamu').fadeIn();
        });


        _wizard.on('beforeChange', function (wizard) {
            var _wizardGetstep = wizard.getStep();
            _wizard.stop();

            var validator = _validations[_wizardGetstep - 1];

            if (validator) {
                validator.validate().then(function (status) {
                    console.log(status);
                    if (status == 'Valid') {
                        _wizard.goNext();
                        KTUtil.scrollTop();
                    } else {
                        Swal.fire({
                            html: 'Anda memiliki kesalahan input atau data belum lengkap.<br />Silahkan periksa langkah lainnya dan ulangi lagi.',
                            icon: 'error',
                            buttonsStyling: false,
                            confirmButtonText: 'Tutup',
                            customClass: {
                                confirmButton: 'btn font-weight-bold btn-light'
                            }
                        }).then(function () {
                            KTUtil.scrollTop();
                            $('#alert-bukutamu').html(`
                                <div class='row justify-content-center'>
                                    <div class='col-8 mb-10'>
                                        <div class='alert alert-danger text-center'>
                                            <strong>
                                                Anda memiliki kesalahan input atau data belum lengkap.
                                                <br />
                                                Silahkan periksa langkah lainnya dan ulangi lagi.
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            `);
                        });
                    }
                });
            }
        });

        _wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }

    var initValidator = function (isValid, stepNumber) {
        if (isValid) {
            isValid.validate().then(function (status) {
                if (status == 'Valid') {
                    _wizard.goNext();
                    KTUtil.scrollTop();
                    if(stepNumber === 2) {
                        $('#alert-bukutamu').fadeOut();
                    }
                } else {
                    Swal.fire({
                        html: 'Anda memiliki kesalahan input atau data belum lengkap.<br />Silahkan periksa langkah lainnya dan ulangi lagi.',
                        icon: 'error',
                        buttonsStyling: false,
                        confirmButtonText: 'Tutup',
                        customClass: {
                            confirmButton: 'btn font-weight-bold btn-light'
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                        $('#alert-bukutamu').html(`
                                <div class='row justify-content-center'>
                                    <div class='col-8 mb-10'>
                                        <div class='alert alert-danger text-center'>
                                            <strong>
                                                Anda memiliki kesalahan input atau data belum lengkap.
                                                <br />
                                                Silahkan periksa langkah lainnya dan ulangi lagi.
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            `);
                    });
                }
            });
        }
    };

    var initValidation = function () {
        _validations.push(FormValidation.formValidation(
            _formBukutamu,
            {
                fields: {
                    bidang: {
                        validators: {
                            notEmpty: {
                                message: 'Bidang tujuan wajib dipilih'
                            }
                        }
                    },
                    jabatan: {
                        validators: {
                            notEmpty: {
                                message: 'Jabatan tujuan wajib dipilih'
                            }
                        }
                    },
                    // jumlah_tamu: {
                    //     validators: {
                    //         notEmpty: {
                    //             message: 'Anda wajib memberitahukan jumlah tamu yang datang'
                    //         },
                    //         digits: {
                    //             message: 'Jumlah tamu hanya dapat diisi dengan angka'
                    //         },
                    //     }
                    // },
                    perihal: {
                        validators: {
                            notEmpty: {
                                message: 'Anda wajib memberitahukan keterangan kedatangan Anda'
                            },
                            stringLength: {
                                min: 10,
                                message: 'Keterangan kedatangan minimal 10 karakter'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z,.\s]*$/,
                                message: 'Keterangan kedatangan hanya dapat diisi dengan alfabet, titik (.), koma (,) dan spasi'
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));

        _validations.push(FormValidation.formValidation(
            _formBukutamu,
            {
                fields: {
                    nik_ktp: {
                        validators: {
                            callback: {
                                message: 'No KTP wajib diisi',
                                callback: function(value, validator, $field) {
                                    if(document.getElementById('nik_type').value == 1){
                                        return true;
                                    }
                                    return false;
                                }
                            },
                            digits: {
                                message: 'No KTP hanya dapat diisi dengan angka'
                            },
                            stringLength: {
                                min: 16,
                                max: 16,
                                message: 'No KTP harus terdiri dari 16 digit'
                            },
                        }
                    },
                    nik_sim: {
                        validators: {
                            callback: {
                                message: 'No SIM wajib diisi',
                                callback: function(value, validator, $field) {
                                    if(document.getElementById('nik_type').value == 2){
                                        return true;
                                    }
                                    return false;
                                }
                            },
                            digits: {
                                message: 'No SIM hanya dapat diisi dengan angka'
                            },
                            stringLength: {
                                min: 14,
                                max: 14,
                                message: 'No SIM harus terdiri dari 14 digit'
                            },
                        }
                    },
                    nik_passport: {
                        validators: {
                            callback: {
                                message: 'No Passport wajib diisi',
                                callback: function(value, validator, $field) {
                                    if(document.getElementById('nik_type').value == 3){
                                        return true;
                                    }
                                    return false;
                                }
                            },
                            regexp: {
                                regexp: /^[A-Z0-9\s]*$/,
                                message: 'Nama hanya dapat diisi dengan alfabet dan angka'
                            },
                            stringLength: {
                                min: 8,
                                message: 'No Passport minimal terdiri dari 8 karakter'
                            },
                        }
                    },
                    nama: {
                        validators: {
                            notEmpty: {
                                message: 'Nama wajib diisi sesuai KTP'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z\s]*$/,
                                message: 'Nama hanya dapat diisi dengan alfabet dan spasi'
                            },
                            stringLength: {
                                min: 3,
                                message: 'Nama minimal terdiri dari 3 karakter'
                            }
                        }
                    },
                    telepon: {
                        validators: {
                            notEmpty: {
                                message: 'No HP / Whatsapp wajib diisi'
                            },
                            regexp: {
                                regexp: /^(^\+62\s?|^0)(\d{2,4}-?){2}\d{3,5}$/,
                                message: 'Format no HP / whatsapp salah. Contoh no HP / whatsapp +628131234567 atau 08131234567'
                            }
                        }
                    },
                    plat_kendaraan: {
                        validators: {
                            regexp: {
                                regexp: /^[a-zA-Z0-9-_,.\s]*$/,
                                message: 'Plat Kendaraan hanya dapat diisi dengan alfabet, angka dan spasi. Contoh B 1234 ABC'
                            }
                        }
                    },
                    email: {
                        validators: {
                            regexp: {
                                regexp: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                                message: 'Format email salah. Contoh email nama_kamu@email.com'
                            }
                        }
                    },
                    alamat: {
                        validators: {
                            notEmpty: {
                                message: 'Masukan alamat sesuai KTP Anda'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9-_,.\s]*$/,
                                message: 'Alamat hanya dapat diisi dengan alfabet, angka, dash (-), underscores (_), titik (.), koma (,) dan spasi'
                            },
                            stringLength: {
                                min: 5,
                                message: 'Alamat minimal terdiri dari 5 karakter'
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        ));

        // _validations.push(FormValidation.formValidation(
        //     _formBukutamu,
        //     {
        //         fields: {
        //             image: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: 'Silahkan melakukan pengambilan foto terlebih dahulu'
        //                     }
        //                 }
        //             },
        //         },
        //         // plugins: {
        //         //     trigger: new FormValidation.plugins.Trigger(),
        //         //     bootstrap: new FormValidation.plugins.Bootstrap()
        //         // }
        //     }
        // ));
    }

    return {
        init: function () {
            _wizardEBukutamu = KTUtil.getById('kt_wizard_v3');
            _formBukutamu = KTUtil.getById('kt_form');

            initWizard();
            initValidation();
        }
    };
}();

jQuery(document).ready(function () {
    if ($('#kt_wizard_v3').length > 0) {
        KTWizardBukutamu.init();
    }
});
