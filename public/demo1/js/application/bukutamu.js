$(document).ready(function () {
    var typingTimer; //timer identifier
    var doneTypingInterval = 500; //time in ms, 0.5 second
    let ktp_length = 16;
    let sim_length = 14;
    let passport_min_length = 7;

    if ($('#kt_select_satker').length > 0) {
        $('#kt_select_satker').select2({
            placeholder: "Select Satker"
        });

        $('#kt_select_satker').on('select2:select', function (e) {
            let gosatker = e.params.data.id;
            window.location.replace(gosatker);
        });
    }

    $('#nik_type').on('change', function (e) {
        let nik_type = $(this).val();
        $('#data-tamu, #detail-tamu').fadeOut();

        if (nik_type == 1) {
            $('#div_nik_ktp').removeClass('d-none').addClass('d-block');
            $('#div_nik_sim').removeClass('d-block').addClass('d-none');
            $('#div_nik_passport').removeClass('d-block').addClass('d-none');
        } else if (nik_type == 2) {
            $('#div_nik_ktp').removeClass('d-block').addClass('d-none');
            $('#div_nik_sim').removeClass('d-none').addClass('d-block');
            $('#div_nik_passport').removeClass('d-block').addClass('d-none');
        } else if (nik_type == 3) {
            $('#div_nik_ktp').removeClass('d-block').addClass('d-none');
            $('#div_nik_sim').removeClass('d-block').addClass('d-none');
            $('#div_nik_passport').removeClass('d-none').addClass('d-block');
        }
    });

    $('#nik_ktp').on('keypress', function (e) {
        $('#nik_sim').val('');
        $('#nik_passport').val('');
        var nik = $(this).val();
        if (nik.length == ktp_length) {
            $('#alert-wait-nik').removeClass('d-none').addClass('d-block');
        } else {
            $('#action-next-step2').fadeOut();
            $('#data-tamu, #detail-tamu').fadeOut();
        }
    });

    $('#nik_ktp').on('keyup', function (e) {
        clearTimeout(typingTimer);
        var nik = $(this).val();
        typingTimer = setTimeout(doneStepNIK(nik), doneTypingInterval);
    });

    $('#nik_sim').on('keypress', function (e) {
        $('#nik_ktp').val('');
        $('#nik_passport').val('');
        var nik = $(this).val();
        if (nik.length == sim_length) {
            $('#alert-wait-nik').removeClass('d-none').addClass('d-block');
        } else {
            $('#action-next-step2').fadeOut();
            $('#data-tamu, #detail-tamu').fadeOut();
        }
    });

    $('#nik_sim').on('keyup', function (e) {
        clearTimeout(typingTimer);
        var nik = $(this).val();
        typingTimer = setTimeout(doneStepNIK(nik), doneTypingInterval);
    });

    $('#nik_passport').on('keypress', function (e) {
        $('#nik_ktp').val('');
        $('#nik_sim').val('');
        var nik = $(this).val();
        if (nik.length > passport_min_length) {
            $('#alert-wait-nik').removeClass('d-none').addClass('d-block');
        } else {
            $('#action-next-step2').fadeOut();
            $('#data-tamu, #detail-tamu').fadeOut();
        }
    });

    $('#nik_passport').on('keyup', function (e) {
        clearTimeout(typingTimer);
        var nik = $(this).val();
        typingTimer = setTimeout(doneStepNIK(nik), doneTypingInterval);
    });

    $('#nik_ktp, #nik_sim, #nik_passport').on('keypress', function (e) {
        $('#action-next-step2').fadeOut();
        $('#data-tamu, #detail-tamu').fadeOut();
    });

    $('#nik_ktp, #nik_sim, #nik_passport').on('keydown', function (e) {
        clearTimeout(typingTimer);
    });

    function doneStepNIK (nik) {
        $('#action-next-step2').fadeOut();
        $('#data-tamu, #detail-tamu').fadeOut();
        var nik_type = $('#nik_type').val();
        var nik_length = 0;
        if (nik_type == 1) {
            nik_length = ktp_length;
        } else if (nik_type == 2) {
            nik_length = sim_length;
        } else if (nik_type == 3) {
            nik_length = passport_min_length;
        }
        if ( (nik_type == 3 && nik.length > nik_length) || (nik_type != 3 && nik.length == nik_length) ) {
            $.ajax({
                url: baseUrl + '/ajax/check-tamu',
                type: 'POST',
                data: '_token=' + cToken + '&nik_type=' + nik_type + '&nik=' + nik,
                success: function (response, textStatus, request) {
                    $('#action-next-step2').fadeIn();
                    $('#data-tamu, #detail-tamu').fadeIn();
                    $('#data-tamu p').html('Data Anda');
                    $('#guest_nama').attr('readonly', true).val(response.datas.name).removeClass('is-invalid').addClass('is-valid');
                    $('#guest_telp').attr('readonly', true).val(response.datas.phone).removeClass('is-invalid').addClass('is-valid');
                    $('#guest_email').attr('readonly', true).val(response.datas.email).removeClass('is-invalid').addClass('is-valid');
                    $('#guest_plat_kendaraan').attr('readonly', true).val(response.datas.plat_kendaraan).removeClass('is-invalid').addClass('is-valid');
                    $('#guest_alamat').attr('readonly', true).val(response.datas.address).removeClass('is-invalid').addClass('is-valid');
                    $('#alert-wait-nik').removeClass('d-block').addClass('d-none');
                    $('#data-tamu .fv-plugins-message-container').fadeOut();
                },
                error: function (response, textStatus, request) {
                    if (response.status == 404 && ( (nik_type == 1 && nik.length == ktp_length) || (nik_type == 2 && nik.length == sim_length) || (nik_type == 3 && nik.length > passport_min_length) )) {
                        $('#alert-wait-nik').removeClass('d-block').addClass('d-none');
                        $('#guest_nama').removeAttr('readonly').val('').removeClass('is-invalid').removeClass('is-valid');
                        $('#guest_telp').removeAttr('readonly').val('').removeClass('is-invalid').removeClass('is-valid');
                        $('#guest_email').removeAttr('readonly').val('').removeClass('is-invalid').removeClass('is-valid');
                        $('#guest_plat_kendaraan').removeAttr('readonly').val('').removeClass('is-invalid').removeClass('is-valid');
                        $('#guest_alamat').removeAttr('readonly').val('').removeClass('is-invalid').removeClass('is-valid');
                        $('#data-tamu, #detail-tamu').fadeIn();
                        $('#data-tamu p').html('Masukan data Anda sesuai No Identitas');
                    } else {
                        $('#data-tamu, #detail-tamu').fadeOut();
                    }
                }
            });
        }
    }

    $('#guest_nama, #guest_telp, #guest_alamat').on('keyup', function (e) {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneStep2, doneTypingInterval);
    });

    $('#guest_nama, #guest_telp, #guest_alamat').on('keydown', function (e) {
        clearTimeout(typingTimer);
    });

    function doneStep2 () {
        var guest_nama = $('#guest_nama').val();
        var guest_telp = $('#guest_telp').val();
        var guest_alamat = $('#guest_alamat').val();
        if (guest_nama.length > 2 && guest_telp.length > 4 && guest_alamat.length > 4) {
            $('#action-next-step2').fadeIn();
        } else {
            $('#action-next-step2').fadeOut();
        }
    }

    $('#guest_perihal').on('keyup', function (e) {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneStep3, doneTypingInterval);
    });

    $('#guest_perihal').on('keydown', function (e) {
        clearTimeout(typingTimer);
    });

    function doneStep3 () {
        var guest_perihal = $('#guest_perihal').val();
        if (guest_perihal.length > 10) {
            $('#action-next-step1').fadeIn();
        } else {
            $('#action-next-step1').fadeOut();
        }
    }

    $('#pilih_bidang').on('change', function (e) {
        e.preventDefault();
        var satker = $('#pilih_satker').val();
        var bidang = $(this).val();
        $('#pilih_jabatan').select2({
            placeholder: "loading..."
        });
        $.ajax({
            url: baseUrl + '/ajax/bukutamu-jabatan',
            type: 'POST',
            data: {
                '_token': cToken,
                'bidang': bidang,
                'satker': satker
            },
            success: function (response, textStatus, request) {
                var data_jabatan = '';
                    data_jabatan += '<option value="">--</option>';
                for (var jabatan = 0; jabatan < response.datas.length; jabatan++) {
                    data_jabatan += '<option value="' + response.datas[jabatan].random_id + '">' + response.datas[jabatan].nama + '</option>';
                }
                $('#pilih_jabatan').html(data_jabatan).removeAttr('disabled');
                $('#pilih_jabatan').select2({
                    placeholder: "Pilih Jabatan Tujuan"
                });
                // $('#action-next-step1').fadeIn();
            },
            error: function (response, textStatus, request) {
                swal.fire({
                    text: response.responseJSON.message,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                });
                // $('#action-next-step1').fadeOut();
            }
        });
    });

    $('#pilih_satker').on('change', function (e) {
        e.preventDefault();
        var satker = $(this).val();
        $('#pilih_bidang').select2({
            placeholder: "loading..."
        });
        $.ajax({
            url: baseUrl + '/ajax/bukutamu-satker',
            type: 'POST',
            data: {
                '_token': cToken,
                'satker': satker
            },
            success: function (response, textStatus, request) {
                var data_bidang = '';
                    data_bidang += '<option value="">--</option>';
                for (var bidang = 0; bidang < response.datas.length; bidang++) {
                    data_bidang += '<option value="' + response.datas[bidang].random_id + '">' + response.datas[bidang].nama + '</option>';
                }
                $('#pilih_bidang').html(data_bidang).removeAttr('disabled');
                $('#pilih_bidang').select2({
                    placeholder: "Pilih Bidang Tujuan"
                });
            },
            error: function (response, textStatus, request) {
                swal.fire({
                    text: response.responseJSON.message,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                });
            }
        });
    });

    $('#pilih_jabatan').on('change', function (e) {
        e.preventDefault();
        doneStep3();
    });

    if ($('#pilih_satker').length > 0) {
        $('#pilih_satker').select2({
            placeholder: "Pilih Satker Tujuan"
        });
    }

    if ($('#pilih_bidang').length > 0) {
        $('#pilih_bidang').select2({
            // placeholder: "(Pilih Satker Terlebih Dahulu)"
        });
    }

    if ($('#pilih_jabatan').length > 0) {
        $('#pilih_jabatan').select2({
            // placeholder: "(Pilih Bidang Terlebih Dahulu)"
        });
    }

    $('#button_download_pdf').on('click', function (e) {
        $(this).html('Downloading...');
        var unique = $(this).data('unique');
        $.get(baseUrl, function (data) {
            window.location = baseUrl + '/kunjungan/pdf/' + unique;
            $('#button_download_pdf').html('Download PDF');
        });
        window.location = baseUrl + '/kunjungan/' + unique;
    });

    $('#print_kunjungan').on('click', function (e) {
        // $('#print_kunjungan').html('Loading...');
        // var unique = $('#print_kunjungan').data('unique');
        // $.ajax({
        //     url: baseUrl + '/ajax/print-kunjungan/' + unique,
        //     type: 'GET',
        //     success: function (response, textStatus, request) {
        //         swal.fire({
        //             text: 'QR Code sudah tercetak',
        //             icon: 'success',
        //             showCancelButton: false,
        //             showConfirmButton: false,
        //         });
        //         $('#print_kunjungan').html('Cetak QR Code');
        //     },
        //     error: function (response, textStatus, request) {
        //         swal.fire({
        //             text: response.responseJSON.message,
        //             icon: 'error',
        //             showCancelButton: false,
        //             showConfirmButton: false,
        //         });
        //         $('#print_kunjungan').html('Cetak QR Code');
        //     }
        // });
    });
});
