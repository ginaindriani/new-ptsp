"use strict";

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const statusTamu = urlParams.get('status_tamu');
var validations = [];
var form;
var _validations_field = {
    nik_type: {
        validators: {
            notEmpty: {
                message: "Tipe No Identitas tidak boleh kosong",
            },
        },
    },
    nama: {
        validators: {
            notEmpty: {
                message: "Nama wajib diisi sesuai KTP",
            },
            regexp: {
                regexp: /^[a-zA-Z\s]*$/,
                message:
                    "Nama hanya dapat diisi dengan alfabet dan spasi",
            },
            stringLength: {
                min: 3,
                message: "Nama minimal terdiri dari 3 karakter",
            },
        },
    },
    telepon: {
        validators: {
            notEmpty: {
                message: "No HP / Whatsapp wajib diisi",
            },
            regexp: {
                regexp: /^(^\+62\s?|^0)(\d{2,4}-?){2}\d{3,5}$/,
                message:
                    "Format no HP / whatsapp salah. Contoh no HP / whatsapp +628131234567 atau 08131234567",
            },
        },
    },
    tempat_lahir: {
        validators: {
            notEmpty: {
                message:
                    "Tempat lahir tidak boleh kosong",
            },
        },
    },
    jenis_kelamin: {
        validators: {
            notEmpty: {
                message:
                    "Silahkan pilih jenis kelamin kosong",
            },
        },
    },
    plat_kendaraan: {
        validators: {
            regexp: {
                regexp: /^[a-zA-Z0-9-_,.\s]*$/,
                message:
                    "Plat Kendaraan hanya dapat diisi dengan alfabet, angka dan spasi. Contoh B 1234 ABC",
            },
        },
    },
    email: {
        validators: {
            regexp: {
                regexp: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                message:
                    "Format email salah. Contoh email nama_kamu@email.com",
            },
        },
    },
    alamat: {
        validators: {
            notEmpty: {
                message: "Masukan alamat sesuai KTP Anda",
            },
            regexp: {
                regexp: /^[a-zA-Z0-9-_,.\s]*$/,
                message:
                    "Alamat hanya dapat diisi dengan alfabet, angka, dash (-), underscores (_), titik (.), koma (,) dan spasi",
            },
            stringLength: {
                min: 5,
                message:
                    "Alamat minimal terdiri dari 5 karakter",
            },
        },
    },
    tujuan: {
        validators: {
            notEmpty: {
                message: "Masukan tujuan Anda",
            },
            regexp: {
                regexp: /^[a-zA-Z0-9-_,.\s]*$/,
                message:
                    "Tujuan hanya dapat diisi dengan alfabet, angka, dash (-), underscores (_), titik (.), koma (,) dan spasi",
            },
            stringLength: {
                min: 5,
                message:
                    "Tujuan minimal terdiri dari 5 karakter",
            },
        },
    },
    tanggal_kedatangan: {
        validators: {
            notEmpty: {
                message:
                    "Tanggal Kedatangan tidak boleh kosong",
            },
        },
    },
    waktu_kedatangan: {
        validators: {
            notEmpty: {
                message: "Waktu Kedatangan tidak boleh kosong",
            },
        },
    },
};

// Class definition
var KTCreateAccount = (function () {
    // Elements
    var modal;
    var modalEl;

    var stepper;
    var formSubmitButton;
    var formContinueButton;

    // Variables
    var stepperObj;

    // Private Functions
    var initStepper = function () {
        // Initialize Stepper
        stepperObj = new KTStepper(stepper);

        // Stepper change event
        stepperObj.on("kt.stepper.changed", function (stepper) {
            console.log(stepperObj.getCurrentStepIndex())
            if (statusTamu != 'umum') {
                if (stepperObj.getCurrentStepIndex() === 3) {
                    //formSubmitButton.classList.remove("d-none");
                    //formSubmitButton.classList.add("d-inline-block");
                    $('#submitButton').removeClass("d-none");
                    $('#submitButton').addClass("d-inline-block");
                    formContinueButton.classList.add("d-none");
                } else if (stepperObj.getCurrentStepIndex() === 1) {
                    formSubmitButton.classList.add("d-none");
                    formContinueButton.classList.remove("d-none");
                } else {
                    formSubmitButton.classList.remove("d-inline-block");
                    formSubmitButton.classList.remove("d-none");
                    formContinueButton.classList.remove("d-none");
                }
            } else {
                if (stepperObj.getCurrentStepIndex() === 2) {
                    console.log('step 2')
                    //formSubmitButton.classList.remove("d-none");
                    //formSubmitButton.classList.add("d-inline-block");
                    $('#submitButton').removeClass("d-none");
                    $('#submitButton').addClass("d-inline-block");
                    formContinueButton.classList.add("d-none");
                } else if (stepperObj.getCurrentStepIndex() === 1) {
                    formSubmitButton.classList.add("d-none");
                    formContinueButton.classList.remove("d-none");
                } else {
                    formSubmitButton.classList.remove("d-inline-block");
                    formSubmitButton.classList.remove("d-none");
                    formContinueButton.classList.remove("d-none");
                }
            }
        });

        // Validation before going to next page
        stepperObj.on("kt.stepper.next", function (stepper) {
            console.log("stepper.next");

            // Validate form before change stepper step
            var validator = validations[stepper.getCurrentStepIndex() - 1]; // get validator for currnt step

            if (validator) {
                validator.validate().then(function (status) {
                    console.log("validated!");

                    if (status == "Valid") {
                        stepper.goNext();

                        KTUtil.scrollTop();
                    } else {
                        Swal.fire({
                            text: "Mohon maaf, mohon lengkapi data sebelum melanjutkan.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, baik!",
                            customClass: {
                                confirmButton: "btn btn-light",
                            },
                        }).then(function () {
                            KTUtil.scrollTop();
                        });
                    }
                });
            } else {
                stepper.goNext();

                KTUtil.scrollTop();
            }
        });

        // Prev event
        stepperObj.on("kt.stepper.previous", function (stepper) {
            console.log("stepper.previous");

            stepper.goPrevious();
            KTUtil.scrollTop();
        });
    };

    var handleForm = function () {
        formSubmitButton.addEventListener("click", function (e) {
            // Validate form before change stepper step
            var validator = validations[3]; // get validator for last form

            validator.validate().then(function (status) {
                console.log("validated!");

                if (status == "Valid") {
                    // Prevent default button action
                    e.preventDefault();

                    // Disable button to avoid multiple click
                    formSubmitButton.disabled = true;

                    // Show loading indication
                    formSubmitButton.setAttribute("data-kt-indicator", "on");

                    // Simulate form submission
                    setTimeout(function () {
                        // Hide loading indication
                        formSubmitButton.removeAttribute("data-kt-indicator");

                        // Enable button
                        formSubmitButton.disabled = false;

                        stepperObj.goNext();
                        //KTUtil.scrollTop();
                    }, 2000);
                } else {
                    Swal.fire({
                        text: "Mohon maaf, mohon lengkapi data sebelum melanjutkan.",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-light",
                        },
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }
            });
        });

        // Expiry month. For more info, plase visit the official plugin site: https://select2.org/
        $(form.querySelector('[name="card_expiry_month"]')).on(
            "change",
            function () {
                // Revalidate the field when an option is chosen
                validations[3].revalidateField("card_expiry_month");
            }
        );

        // Expiry year. For more info, plase visit the official plugin site: https://select2.org/
        $(form.querySelector('[name="card_expiry_year"]')).on(
            "change",
            function () {
                // Revalidate the field when an option is chosen
                validations[3].revalidateField("card_expiry_year");
            }
        );

        // Expiry year. For more info, plase visit the official plugin site: https://select2.org/
        $(form.querySelector('[name="business_type"]')).on(
            "change",
            function () {
                // Revalidate the field when an option is chosen
                validations[2].revalidateField("business_type");
            }
        );
    };

    var initValidation = function () {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        onChangeTipeIdentitas();
        // Step 3
        // validations.push(
        //     FormValidation.formValidation(form, {
        //         fields: {
        //             nik_type: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Tipe No Identitas tidak boleh kosong",
        //                     },
        //                 },
        //             },
        //             nik_ktp: {
        //                 validators: {
        //                     callback: {
        //                         message: "No KTP wajib diisi",
        //                         callback: function (value, validator, $field) {
        //                             if (
        //                                 document.getElementById("nik_type")
        //                                     .value == 1
        //                             ) {
        //                                 return true;
        //                             }
        //                             return false;
        //                         },
        //                     },
        //                     notEmpty: {
        //                         message: "No KTP tidak boleh kosong",
        //                     },
        //                     digits: {
        //                         message:
        //                             "No KTP hanya dapat diisi dengan angka",
        //                     },
        //                     stringLength: {
        //                         min: 16,
        //                         max: 16,
        //                         message: "No KTP harus terdiri dari 16 digit",
        //                     },
        //                 },
        //             },
        //             // nik_sim: {
        //             //     validators: {
        //             //         callback: {
        //             //             message: "No SIM wajib diisi",
        //             //             callback: function (value, validator, $field) {
        //             //                 if (
        //             //                     document.getElementById("nik_type")
        //             //                         .value == 2
        //             //                 ) {
        //             //                     return true;
        //             //                 }
        //             //                 return false;
        //             //             },
        //             //         },
        //             //         digits: {
        //             //             message:
        //             //                 "No SIM hanya dapat diisi dengan angka",
        //             //         },
        //             //         stringLength: {
        //             //             min: 14,
        //             //             max: 14,
        //             //             message: "No SIM harus terdiri dari 14 digit",
        //             //         },
        //             //     },
        //             // },
        //             // nik_passport: {
        //             //     validators: {
        //             //         callback: {
        //             //             message: "No Passport wajib diisi",
        //             //             callback: function (value, validator, $field) {
        //             //                 if (
        //             //                     document.getElementById("nik_type")
        //             //                         .value == 3
        //             //                 ) {
        //             //                     return true;
        //             //                 }
        //             //                 return false;
        //             //             },
        //             //         },
        //             //         regexp: {
        //             //             regexp: /^[A-Z0-9\s]*$/,
        //             //             message:
        //             //                 "Nama hanya dapat diisi dengan alfabet dan angka",
        //             //         },
        //             //         stringLength: {
        //             //             min: 8,
        //             //             message:
        //             //                 "No Passport minimal terdiri dari 8 karakter",
        //             //         },
        //             //     },
        //             // },
        //             nama: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Nama wajib diisi sesuai KTP",
        //                     },
        //                     regexp: {
        //                         regexp: /^[a-zA-Z\s]*$/,
        //                         message:
        //                             "Nama hanya dapat diisi dengan alfabet dan spasi",
        //                     },
        //                     stringLength: {
        //                         min: 3,
        //                         message: "Nama minimal terdiri dari 3 karakter",
        //                     },
        //                 },
        //             },
        //             telepon: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "No HP / Whatsapp wajib diisi",
        //                     },
        //                     regexp: {
        //                         regexp: /^(^\+62\s?|^0)(\d{2,4}-?){2}\d{3,5}$/,
        //                         message:
        //                             "Format no HP / whatsapp salah. Contoh no HP / whatsapp +628131234567 atau 08131234567",
        //                     },
        //                 },
        //             },
        //             plat_kendaraan: {
        //                 validators: {
        //                     regexp: {
        //                         regexp: /^[a-zA-Z0-9-_,.\s]*$/,
        //                         message:
        //                             "Plat Kendaraan hanya dapat diisi dengan alfabet, angka dan spasi. Contoh B 1234 ABC",
        //                     },
        //                 },
        //             },
        //             email: {
        //                 validators: {
        //                     regexp: {
        //                         regexp: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        //                         message:
        //                             "Format email salah. Contoh email nama_kamu@email.com",
        //                     },
        //                 },
        //             },
        //             alamat: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Masukan alamat sesuai KTP Anda",
        //                     },
        //                     regexp: {
        //                         regexp: /^[a-zA-Z0-9-_,.\s]*$/,
        //                         message:
        //                             "Alamat hanya dapat diisi dengan alfabet, angka, dash (-), underscores (_), titik (.), koma (,) dan spasi",
        //                     },
        //                     stringLength: {
        //                         min: 5,
        //                         message:
        //                             "Alamat minimal terdiri dari 5 karakter",
        //                     },
        //                 },
        //             },
        //             tujuan: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Masukan tujuan Anda",
        //                     },
        //                     regexp: {
        //                         regexp: /^[a-zA-Z0-9-_,.\s]*$/,
        //                         message:
        //                             "Tujuan hanya dapat diisi dengan alfabet, angka, dash (-), underscores (_), titik (.), koma (,) dan spasi",
        //                     },
        //                     stringLength: {
        //                         min: 5,
        //                         message:
        //                             "Tujuan minimal terdiri dari 5 karakter",
        //                     },
        //                 },
        //             },
        //             tanggal_kedatangan: {
        //                 validators: {
        //                     notEmpty: {
        //                         message:
        //                             "Tanggal Kedatangan tidak boleh kosong",
        //                     },
        //                 },
        //             },
        //             waktu_kedatangan: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Waktu Kedatangan tidak boleh kosong",
        //                     },
        //                 },
        //             },
        //         },
        //         plugins: {
        //             trigger: new FormValidation.plugins.Trigger(),
        //             // Bootstrap Framework Integration
        //             bootstrap: new FormValidation.plugins.Bootstrap5({
        //                 rowSelector: ".fv-row",
        //                 eleInvalidClass: "",
        //                 eleValidClass: "",
        //             }),
        //         },
        //     })
        // );

        // Step 4
        // validations.push(
        //     FormValidation.formValidation(form, {
        //         fields: {
        //             card_name: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Name on card is required",
        //                     },
        //                 },
        //             },
        //             card_number: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Card member is required",
        //                     },
        //                     creditCard: {
        //                         message: "Card number is not valid",
        //                     },
        //                 },
        //             },
        //             card_expiry_month: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Month is required",
        //                     },
        //                 },
        //             },
        //             card_expiry_year: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "Year is required",
        //                     },
        //                 },
        //             },
        //             card_cvv: {
        //                 validators: {
        //                     notEmpty: {
        //                         message: "CVV is required",
        //                     },
        //                     digits: {
        //                         message: "CVV must contain only digits",
        //                     },
        //                     stringLength: {
        //                         min: 3,
        //                         max: 4,
        //                         message: "CVV must contain 3 to 4 digits only",
        //                     },
        //                 },
        //             },
        //         },

        //         plugins: {
        //             trigger: new FormValidation.plugins.Trigger(),
        //             // Bootstrap Framework Integration
        //             bootstrap: new FormValidation.plugins.Bootstrap5({
        //                 rowSelector: ".fv-row",
        //                 eleInvalidClass: "",
        //                 eleValidClass: "",
        //             }),
        //         },
        //     })
        // );
        console.log(validations);
    };

    var handleFormSubmit = function () {};

    return {
        // Public Functions
        init: function () {
            // Elements
            modalEl = document.querySelector("#kt_modal_create_account");
            if (modalEl) {
                modal = new bootstrap.Modal(modalEl);
            }

            stepper = document.querySelector("#kt_create_account_stepper");
            form = stepper.querySelector("#kt_create_account_form");
            formSubmitButton = stepper.querySelector(
                '[data-kt-stepper-action="submit"]'
            );
            formContinueButton = stepper.querySelector(
                '[data-kt-stepper-action="next"]'
            );

            initStepper();
            initValidation();
            // handleForm();
        },
    };
})();

// On document ready
KTUtil.onDOMContentLoaded(function () {
    KTCreateAccount.init();
});

var fvgeneral;
function onChangeTipeIdentitas() {
    initValidations();
}

function initValidations() {
    try {
        fvgeneral.destroy();
    } catch (exception) {}
    var _nikType = $("#nik_type").val();
    var _type = $("#kt_create_account_stepper").attr("data-type");
    validations = [];
    if(_nikType == 2) {
        $("#div_nik_ktp").addClass("d-none");
        $("#div_nik_passport").addClass("d-none");
        $("#div_nik_sim").removeClass("d-none");
        delete _validations_field["nik_ktp"];
        delete _validations_field["nik_passport"];
        _validations_field["nik_sim"] =  {
            validators: {
                callback: {
                    message: "No SIM wajib diisi",
                    callback: function (value, validator, $field) {
                        if (
                            document.getElementById("nik_type")
                                .value == 2
                        ) {
                            return true;
                        }
                        return false;
                    },
                },
                notEmpty: {
                    message: "No SIM tidak boleh kosong",
                },
                digits: {
                    message:
                        "No SIM hanya dapat diisi dengan angka",
                },
                stringLength: {
                    min: 14,
                    max: 14,
                    message: "No SIM harus terdiri dari 14 digit",
                },
            }
        };
    } else if(_nikType == 3) {
        $("#div_nik_ktp").addClass("d-none");;
        $("#div_nik_sim").addClass("d-none");;
        $("#div_nik_passport").removeClass("d-none");
        delete _validations_field["nik_ktp"];
        delete _validations_field["nik_sim"];
        _validations_field["nik_passport"] =  {
            validators: {
                callback: {
                    message: "No Passport wajib diisi",
                    callback: function (value, validator, $field) {
                        if (
                            document.getElementById("nik_type")
                                .value == 3
                        ) {
                            return true;
                        }
                        return false;
                    },
                },
                notEmpty: {
                    message: "No Passport tidak boleh kosong",
                },
                regexp: {
                    regexp: /^[A-Z0-9\s]*$/,
                    message:
                        "Nama hanya dapat diisi dengan alfabet dan angka",
                },
                stringLength: {
                    min: 8,
                    message:
                        "No Passport minimal terdiri dari 8 karakter",
                },
            }
        };
    } else {
        $("#div_nik_sim").addClass("d-none");;
        $("#div_nik_passport").addClass("d-none");;
        $("#div_nik_ktp").removeClass("d-none");
        delete _validations_field["nik_sim"];
        delete _validations_field["nik_passport"];
        _validations_field["nik_ktp"] = {
            validators: {
                callback: {
                    message: "No KTP wajib diisi",
                    callback: function (value, validator, $field) {
                        if (
                            document.getElementById("nik_type")
                                .value == 1
                        ) {
                            return true;
                        }
                        return false;
                    },
                },
                notEmpty: {
                    message: "No KTP tidak boleh kosong",
                },
                digits: {
                    message:
                        "No KTP hanya dapat diisi dengan angka",
                },
                stringLength: {
                    min: 16,
                    max: 16,
                    message: "No KTP harus terdiri dari 16 digit",
                },
            },
        };
    }
    
    if (_type == 'identitas_umum') {
        validations.push(
            fvgeneral = FormValidation.formValidation(form, {
                fields: _validations_field,
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: ".fv-row",
                        eleInvalidClass: "",
                        eleValidClass: "",
                    }),
                },
            })
        );
    } else if (_type == 'identitas_saksi_terdakwa' || _type == 'identitas_pers') {
        _validations_field["suku"] =  {
            validators: {
                notEmpty: {
                    message: "Bangsa / Suku tidak boleh kosong",
                }
            }
        };
        _validations_field["kewarganegaraan"] =  {
            validators: {
                notEmpty: {
                    message: "Kewarganegaraan tidak boleh kosong",
                }
            }
        };
        _validations_field["agama"] =  {
            validators: {
                notEmpty: {
                    message: "Agama / Kepercayaan tidak boleh kosong",
                }
            }
        };
        _validations_field["pendidikan"] =  {
            validators: {
                notEmpty: {
                    message: "Pendidikan tidak boleh kosong",
                }
            }
        };
        _validations_field["pekerjaan"] =  {
            validators: {
                notEmpty: {
                    message: "Pekerjaan tidak boleh kosong",
                }
            }
        };
        _validations_field["status_perkawinan"] =  {
            validators: {
                notEmpty: {
                    message: "Status Perkawinan tidak boleh kosong",
                }
            }
        };
        _validations_field["alamat_kantor"] =  {
            validators: {
                notEmpty: {
                    message: "Alamat Kantor tidak boleh kosong",
                }
            }
        };
        validations.push(
            FormValidation.formValidation(form, {
                fields: _validations_field,
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: ".fv-row",
                        eleInvalidClass: "",
                        eleValidClass: "",
                    }),
                },
            })
        );
        var validation_intelijen = {
            // kepartaian: {
            //     validators: {
            //         notEmpty: {
            //             message: "Kepartaian tidak boleh kosong",
            //         },
            //     },
            // },
            // ormas_lainnya: {
            //     validators: {
            //         notEmpty: {
            //             message: "Ormas Lainnya tidak boleh kosong",
            //         },
            //     },
            // },
            // nama_pasangan: {
            //     validators: {
            //         // notEmpty: {
            //         //     message: "Nama Pasangan tidak boleh kosong",
            //         // },
            //     },
            // },
            // nama_anak_anak: {
            //     validators: {
            //         // notEmpty: {
            //         //     message: "Nama Pasangan tidak boleh kosong",
            //         // },
            //     },
            // },
            nama_ayah_kandung: {
                validators: {
                    notEmpty: {
                        message: "Ayah Kandung tidak boleh kosong",
                    },
                },
            },
            alamat_ayah_kandung: {
                validators: {
                    notEmpty: {
                        message: "Alamat Ayah Kandung tidak boleh kosong",
                    },
                },
            },
            nama_ibu_kandung: {
                validators: {
                    notEmpty: {
                        message: "Nama Ibu Kandung tidak boleh kosong",
                    },
                },
            },
            alamat_ibu_kandung: {
                validators: {
                    notEmpty: {
                        message: "Alamat Ibu Kandung tidak boleh kosong",
                    },
                },
            },
            nama_ayah_mertua: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            alamat_ayah_mertua: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            nama_ibu_mertua: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            alamat_ibu_mertua: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            nama_kenalan1: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            alamat_kenalan1: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            nama_kenalan2: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            alamat_kenalan2: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            nama_kenalan3: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            alamat_kenalan3: {
                validators: {
                    // notEmpty: {
                    //     message: "Nama Pasangan tidak boleh kosong",
                    // },
                },
            },
            hobi: {
                validators: {
                    notEmpty: {
                        message: "Hobi / Kegemaran tidak boleh kosong",
                    },
                },
            },
            kedudukan_di_masyarakat: {
                validators: {
                    notEmpty: {
                        message: "Kedudukan di masyarakat tidak boleh kosong",
                    },
                },
            },
        };
        if(_type == 'identitas_pers') {
            validation_intelijen["media_menaungi"] =  {
                validators: {
                    notEmpty: {
                        message: "Media Yang Menaungi tidak boleh kosong",
                    }
                }
            };
            validation_intelijen["website_media"] =  {
                validators: {
                    notEmpty: {
                        message: "Website Media tidak boleh kosong",
                    }
                }
            };
            validation_intelijen["nomor_kta"] =  {
                validators: {
                    notEmpty: {
                        message: "Nomor KTA tidak boleh kosong",
                    }
                }
            };
            validation_intelijen["nama_atasan_langsung"] =  {
                validators: {
                    notEmpty: {
                        message: "Nama Atasan Langsung tidak boleh kosong",
                    }
                }
            };
            validation_intelijen["no_telp_kantor"] =  {
                validators: {
                    notEmpty: {
                        message: "Nomor Telpon Kantor tidak boleh kosong",
                    }
                }
            };
            validation_intelijen["jabatan"] =  {
                validators: {
                    notEmpty: {
                        message: "Jabatan tidak boleh kosong",
                    }
                }
            };
        }
        validations.push(
            FormValidation.formValidation(form, {
                fields: validation_intelijen,
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: ".fv-row",
                        eleInvalidClass: "",
                        eleValidClass: "",
                    }),
                },
            })
        );
    }
}